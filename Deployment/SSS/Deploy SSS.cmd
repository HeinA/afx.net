@echo Server Deployment
@xcopy C:\Dev\afx.net\Source\Clients\MoneyMarket\MoneyMarket.Service\*.svc \\10.20.0.10\indilinga\Server\. /d /y
@xcopy C:\Dev\afx.net\Source\Clients\MoneyMarket\MoneyMarket.Service\App_Code\*.cs \\10.20.0.10\indilinga\Server\App_Code\. /d /y
@xcopy C:\Dev\afx.net\Source\Clients\MoneyMarket\MoneyMarket.Service\bin\*.dll \\10.20.0.10\indilinga\Server\bin\. /d /y


@echo Client Deployment
@xcopy C:\Dev\afx.net\Source\Clients\MoneyMarket\MoneyMarket.Test\bin\x86\Debug\*.dll \\10.20.0.10\indilinga\Client\Source\. /d /y /s
@xcopy C:\Dev\afx.net\Source\Clients\MoneyMarket\MoneyMarket.Test\bin\x86\Debug\*.exe \\10.20.0.10\indilinga\Client\Source\. /d /y /s
@del \\10.20.0.10\indilinga\Client\Source\*.vshost.exe
@del \\10.20.0.10\indilinga\Client\Source\*.vshost.exe.config

@echo Optional Server Deployment
@rem @xcopy C:\Dev\afx.net\Source\Clients\MoneyMarket\MoneyMarket.Service\Web.config \\10.20.0.10\indilinga\Server\. /d /y
@rem @xcopy C:\Dev\afx.net\Source\Clients\MoneyMarket\MoneyMarket.Service\*.asax \\10.20.0.10\indilinga\Server\. /d /y

@rem @xcopy C:\Dev\afx.net\Source\Clients\MoneyMarket\MoneyMarket.Service\bin\*.pdb \\10.20.0.10\indilinga\Server\bin\. /d /y

@echo Optional Client Deployment
@rem @xcopy C:\Dev\afx.net\Source\Clients\MoneyMarket\MoneyMarket.Test\bin\x86\Debug\server.xml \\10.20.0.10\indilinga\Client\Source\. /d /y /s
@rem @xcopy C:\Dev\afx.net\Source\Clients\MoneyMarket\MoneyMarket.Test\bin\x86\Debug\*.config \\10.20.0.10\indilinga\Client\Source\. /d /y /s

@rem @xcopy C:\Dev\Afx.net\Source\Clients\Imberbe\IFMS.Imberbe\bin\x86\Release\*.pdb \\10.20.0.10\indilinga\Client\Source\. /d /y /s

@pause
@echo Server Deployment
@xcopy C:\Dev\Afx.net\Source\Clients\IFMS\IFMS.Master\*.svc \\IS-IFMS\Indilinga\Server\. /d /y
@xcopy C:\Dev\Afx.net\Source\Clients\IFMS\IFMS.Master\App_Code\*.cs \\IS-IFMS\Indilinga\Server\App_Code\. /d /y
@xcopy C:\Dev\Afx.net\Source\Clients\IFMS\IFMS.Master\bin\*.dll \\IS-IFMS\Indilinga\Server\bin\. /d /y
@del \\IS-IFMS\Indilinga\Server\bin\ScheduleManagement.*
@del \\IS-IFMS\Indilinga\Server\ScheduleManagementService.svc

@echo Client Deployment
@xcopy C:\Dev\Afx.net\Source\Clients\IFMS\IFMS.Test\bin\x86\Debug\*.dll \\IS-IFMS\Indilinga\Client\Source\. /d /y /s
@xcopy C:\Dev\Afx.net\Source\Clients\IFMS\IFMS.Test\bin\x86\Debug\*.exe \\IS-IFMS\Indilinga\Client\Source\. /d /y /s
@del \\IS-IFMS\Indilinga\Client\Source\ScheduleManagement.*
@del \\IS-IFMS\Indilinga\Client\Source\*.vshost.exe
@del \\IS-IFMS\Indilinga\Client\Source\*.vshost.exe.config

@echo Optional Server Deployment
@rem @xcopy C:\Dev\Afx.net\Source\Clients\IFMS\IFMS.Master\Web.config \\IS-IFMS\Indilinga\Server\. /d /y
@rem @xcopy C:\Dev\Afx.net\Source\Clients\IFMS\IFMS.Master\*.asax \\IS-IFMS\Indilinga\Server\. /d /y

@rem @xcopy C:\Dev\Afx.net\Source\Clients\IFMS\IFMS.Master\bin\*.pdb \\IS-IFMS\Indilinga\Server\bin\. /d /y

@echo Optional Client Deployment
@rem @xcopy C:\Dev\Afx.net\Source\Clients\IFMS\IFMS.Test\bin\x86\Debug\server.xml \\IS-IFMS\Indilinga\Client\Source\. /d /y /s
@rem @xcopy C:\Dev\Afx.net\Source\Clients\IFMS\IFMS.Test\bin\x86\Debug\*.config \\IS-IFMS\Indilinga\Client\Source\. /d /y /s

@rem @xcopy C:\Dev\Afx.net\Source\Clients\Imberbe\IFMS.Imberbe\bin\x86\Debug\*.pdb \\IS-IFMS\Indilinga\Client\Source\. /d /y /s

@pause
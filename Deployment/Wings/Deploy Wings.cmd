@echo Server Deployment
@xcopy C:\dev\Afx.net\Source\Clients\Wings\IFMS.Master.Wings\*.svc \\wings-app\indilinga\ifms2\Server\. /d /y
@xcopy C:\dev\Afx.net\Source\Clients\Wings\IFMS.Master.Wings\App_Code\*.cs \\wings-app\indilinga\ifms2\Server\App_Code\. /d /y
@xcopy C:\dev\Afx.net\Source\Clients\Wings\IFMS.Master.Wings\bin\*.dll \\wings-app\indilinga\ifms2\Server\bin\. /d /y
@del \\wings-app\indilinga\ifms2\Server\bin\ScheduleManagement.*
@del \\wings-app\indilinga\ifms2\Server\ScheduleManagementService.svc


@echo Client Deployment
@xcopy C:\dev\Afx.net\Source\Clients\Wings\IFMS.Wings\bin\x86\Debug\*.dll \\wings-app\indilinga\ifms2\Client\Source\. /d /y /s
@xcopy C:\dev\Afx.net\Source\Clients\Wings\IFMS.Wings\bin\x86\Debug\*.exe \\wings-app\indilinga\ifms2\Client\Source\. /d /y /s
@del \\wings-app\indilinga\ifms2\Client\Source\ScheduleManagement.*
@del \\wings-app\indilinga\ifms2\Client\Source\*.vshost.exe
@del \\wings-app\indilinga\ifms2\Client\Source\*.vshost.exe.config

@echo Optional Server Deployment
@rem @xcopy C:\dev\Afx.net\Source\Clients\Wings\IFMS.Master.Wings\Web.config \\wings-app\indilinga\ifms2\Server\. /d /y
@rem @xcopy C:\dev\Afx.net\Source\Clients\Wings\IFMS.Master.Wings\*.asax \\wings-app\indilinga\ifms2\Server\. /d /y

 @xcopy C:\dev\Afx.net\Source\Clients\Wings\IFMS.Master.Wings\bin\*.pdb \\wings-app\indilinga\ifms2\Server\bin\. /d /y

@echo Optional Client Deployment
@rem @xcopy C:\dev\Afx.net\Source\Clients\Wings\IFMS.Wings\bin\x86\Debug\server.xml \\wings-app\indilinga\ifms2\Client\Source\. /d /y /s
@rem @xcopy C:\dev\Afx.net\Source\Clients\Wings\IFMS.Wings\bin\x86\Debug\*.config \\wings-app\indilinga\ifms2\Client\Source\. /d /y /s

@rem @xcopy C:\Dev\Afx.net\Source\Clients\Imberbe\IFMS.Imberbe\bin\x86\Release\*.pdb \\wings-app\indilinga\ifms2\Client\Source\. /d /y /s

@pause
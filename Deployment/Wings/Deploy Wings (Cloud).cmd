@echo Server Deployment
@xcopy C:\dev\Afx.net\Source\Clients\Wings\IFMS.Master.Wings\*.svc \\indilinga01.local\Wings\Server\. /d /y
@xcopy C:\dev\Afx.net\Source\Clients\Wings\IFMS.Master.Wings\App_Code\*.cs \\indilinga01.local\Wings\Server\App_Code\. /d /y
@xcopy C:\dev\Afx.net\Source\Clients\Wings\IFMS.Master.Wings\bin\*.dll \\indilinga01.local\Wings\Server\bin\. /d /y


@echo Client Deployment
@xcopy C:\dev\Afx.net\Source\Clients\Wings\IFMS.Wings\bin\x86\Debug\*.dll \\indilinga01.local\Wings\Client\Source\. /d /y /s
@xcopy C:\dev\Afx.net\Source\Clients\Wings\IFMS.Wings\bin\x86\Debug\*.exe \\indilinga01.local\Wings\Client\Source\. /d /y /s
@del \\indilinga01.local\Wings\Client\Source\*.vshost.exe
@del \\indilinga01.local\Wings\Client\Source\*.vshost.exe.config

@echo Optional Server Deployment
@rem @xcopy C:\dev\Afx.net\Source\Clients\Wings\IFMS.Master.Wings\Web.config \\indilinga01.local\Wings\Server\. /d /y
@rem @xcopy C:\dev\Afx.net\Source\Clients\Wings\IFMS.Master.Wings\*.asax \\indilinga01.local\Wings\Server\. /d /y

@rem @xcopy C:\dev\Afx.net\Source\Clients\Wings\IFMS.Master.Wings\bin\*.pdb \\indilinga01.local\Wings\Server\bin\. /d /y

@echo Optional Client Deployment
@rem @xcopy C:\dev\Afx.net\Source\Clients\Wings\IFMS.Wings\bin\x86\Debug\server.xml \\indilinga01.local\Wings\Client\Source\. /d /y /s
@rem @xcopy C:\dev\Afx.net\Source\Clients\Wings\IFMS.Wings\bin\x86\Debug\*.config \\indilinga01.local\Wings\Client\Source\. /d /y /s

@rem @xcopy C:\Dev\Afx.net\Source\Clients\Imberbe\IFMS.Imberbe\bin\x86\Release\*.pdb \\indilinga01.local\Wings\Client\Source\. /d /y /s

@pause
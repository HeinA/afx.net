@echo Server Deployment
@xcopy C:\Dev\Afx.net\Source\Clients\IFMS\IFMS.Master\*.svc \\sbs-server.sbs.local\indilinga\"ifms 2"\Server\. /d /y
@xcopy C:\Dev\Afx.net\Source\Clients\IFMS\IFMS.Master\App_Code\*.cs \\sbs-server.sbs.local\indilinga\"ifms 2"\Server\App_Code\. /d /y
@xcopy C:\Dev\Afx.net\Source\Clients\IFMS\IFMS.Master\bin\*.dll \\sbs-server.sbs.local\indilinga\"ifms 2"\Server\bin\. /d /y
@del \\sbs-server.sbs.local\indilinga\"ifms 2"\Server\bin\ScheduleManagement.*
@del \\sbs-server.sbs.local\indilinga\"ifms 2"\Server\ScheduleManagementService.svc

@echo Client Deployment
@xcopy C:\Dev\Afx.net\Source\Clients\IFMS\IFMS.Test\bin\x86\Debug\*.dll \\sbs-server.sbs.local\indilinga\"ifms 2"\ClientInstaller\Source\. /d /y /s
@xcopy C:\Dev\Afx.net\Source\Clients\IFMS\IFMS.Test\bin\x86\Debug\*.exe \\sbs-server.sbs.local\indilinga\"ifms 2"\ClientInstaller\Source\. /d /y /s
@del \\sbs-server.sbs.local\indilinga\"ifms 2"\ClientInstaller\Source\ScheduleManagement.*
@del \\sbs-server.sbs.local\indilinga\"ifms 2"\ClientInstaller\Source\*.vshost.exe
@del \\sbs-server.sbs.local\indilinga\"ifms 2"\ClientInstaller\Source\*.vshost.exe.config

@echo Optional Server Deployment
@rem @xcopy C:\Dev\Afx.net\Source\Clients\IFMS\IFMS.Master\Web.config \\sbs-server.sbs.local\indilinga\"ifms 2"\Server\. /d /y
@rem @xcopy C:\Dev\Afx.net\Source\Clients\IFMS\IFMS.Master\*.asax \\sbs-server.sbs.local\indilinga\"ifms 2"\Server\. /d /y

 @xcopy C:\Dev\Afx.net\Source\Clients\IFMS\IFMS.Master\bin\*.pdb \\sbs-server.sbs.local\indilinga\"ifms 2"\Server\bin\. /d /y

@echo Optional Client Deployment
@rem @xcopy C:\Dev\Afx.net\Source\Clients\IFMS\IFMS.Test\bin\x86\Debug\server.xml \\sbs-server.sbs.local\indilinga\"ifms 2"\ClientInstaller\Source\. /d /y /s
@rem @xcopy C:\Dev\Afx.net\Source\Clients\IFMS\IFMS.Test\bin\x86\Debug\*.config \\sbs-server.sbs.local\indilinga\"ifms 2"\ClientInstaller\Source\. /d /y /s

@rem @xcopy C:\Dev\Afx.net\Source\Clients\Imberbe\IFMS.Imberbe\bin\x86\Debug\*.pdb \\sbs-server.sbs.local\indilinga\"ifms 2"\ClientInstaller\Source\. /d /y /s

@pause
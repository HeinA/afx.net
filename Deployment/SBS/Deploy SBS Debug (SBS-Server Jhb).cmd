@echo Server Deployment
@xcopy C:\Dev\Afx.net\Source\Clients\IFMS\IFMS.Master\*.svc \\10.0.0.100\indilinga\Server\. /d /y
@xcopy C:\Dev\Afx.net\Source\Clients\IFMS\IFMS.Master\App_Code\*.cs \\10.0.0.100\indilinga\Server\App_Code\. /d /y
@xcopy C:\Dev\Afx.net\Source\Clients\IFMS\IFMS.Master\bin\*.dll \\10.0.0.100\indilinga\Server\bin\. /d /y
@del \\10.0.0.100\indilinga\Server\bin\ScheduleManagement.*
@del \\10.0.0.100\indilinga\Server\ScheduleManagementService.svc

@echo Client Deployment
@xcopy C:\Dev\Afx.net\Source\Clients\IFMS\IFMS.Test\bin\x86\Debug\*.dll \\10.0.0.100\indilinga\Client\Source\. /d /y /s
@xcopy C:\Dev\Afx.net\Source\Clients\IFMS\IFMS.Test\bin\x86\Debug\*.exe \\10.0.0.100\indilinga\Client\Source\. /d /y /s
@del \\10.0.0.100\indilinga\Client\Source\ScheduleManagement.*
@del \\10.0.0.100\indilinga\Client\Source\*.vshost.exe
@del \\10.0.0.100\indilinga\Client\Source\*.vshost.exe.config

@echo Optional Server Deployment
@rem @xcopy C:\Dev\Afx.net\Source\Clients\IFMS\IFMS.Master\Web.config \\10.0.0.100\indilinga\Server\. /d /y
@rem @xcopy C:\Dev\Afx.net\Source\Clients\IFMS\IFMS.Master\*.asax \\10.0.0.100\indilinga\Server\. /d /y

 @xcopy C:\Dev\Afx.net\Source\Clients\IFMS\IFMS.Master\bin\*.pdb \\10.0.0.100\indilinga\Server\bin\. /d /y

@echo Optional Client Deployment
@rem @xcopy C:\Dev\Afx.net\Source\Clients\IFMS\IFMS.Test\bin\x86\Debug\server.xml \\10.0.0.100\indilinga\Client\Source\. /d /y /s
@rem @xcopy C:\Dev\Afx.net\Source\Clients\IFMS\IFMS.Test\bin\x86\Debug\*.config \\10.0.0.100\indilinga\Client\Source\. /d /y /s

@rem @xcopy C:\Dev\Afx.net\Source\Clients\Imberbe\IFMS.Imberbe\bin\x86\Debug\*.pdb \\10.0.0.100\indilinga\Client\Source\. /d /y /s

@pause
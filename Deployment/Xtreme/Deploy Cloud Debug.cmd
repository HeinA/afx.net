@echo Server Deployment
@xcopy C:\Dev\Afx.net\Source\Clients\IFMS\IFMS.Master\*.svc \\indilinga01.local\Xtreme\Server\. /d /y
@xcopy C:\Dev\Afx.net\Source\Clients\IFMS\IFMS.Master\App_Code\*.cs \\indilinga01.local\Xtreme\Server\App_Code\. /d /y
@xcopy C:\Dev\Afx.net\Source\Clients\IFMS\IFMS.Master\bin\*.dll \\indilinga01.local\Xtreme\Server\bin\. /d /y
@del \\indilinga01.local\Xtreme\Server\bin\ScheduleManagement.*
@del \\indilinga01.local\Xtreme\Server\ScheduleManagementService.svc

@echo Client Deployment
@xcopy C:\Dev\Afx.net\Source\Clients\IFMS\IFMS.Test\bin\x86\Debug\*.dll \\indilinga01.local\Xtreme\Client\Source\. /d /y /s
@xcopy C:\Dev\Afx.net\Source\Clients\IFMS\IFMS.Test\bin\x86\Debug\*.exe \\indilinga01.local\Xtreme\Client\Source\. /d /y /s
@del \\indilinga01.local\Xtreme\ClientInstaller\Source\ScheduleManagement.*
@del \\indilinga01.local\Xtreme\ClientInstaller\Source\*.vshost.exe
@del \\indilinga01.local\Xtreme\ClientInstaller\Source\*.vshost.exe.config

@echo Optional Server Deployment
@rem @xcopy C:\Dev\Afx.net\Source\Clients\IFMS\IFMS.Master\Web.config \\indilinga01.local\Xtreme\Server\. /d /y
@rem @xcopy C:\Dev\Afx.net\Source\Clients\IFMS\IFMS.Master\*.asax \\indilinga01.local\Xtreme\Server\. /d /y

@rem @xcopy C:\Dev\Afx.net\Source\Clients\IFMS\IFMS.Master\bin\*.pdb \\indilinga01.local\Xtreme\Server\bin\. /d /y

@echo Optional Client Deployment
@rem @xcopy C:\Dev\Afx.net\Source\Clients\IFMS\IFMS.Test\bin\x86\Debug\server.xml \\indilinga01.local\Xtreme\ClientInstaller\Source\. /d /y /s
@rem @xcopy C:\Dev\Afx.net\Source\Clients\IFMS\IFMS.Test\bin\x86\Debug\*.config \\indilinga01.local\Xtreme\ClientInstaller\Source\. /d /y /s

@rem @xcopy C:\Dev\Afx.net\Source\Clients\Imberbe\IFMS.Imberbe\bin\x86\Debug\*.pdb \\indilinga01.local\Xtreme\ClientInstaller\Source\. /d /y /s

@pause
@echo Server Deployment
@xcopy C:\Dev\Afx.net\Source\Clients\IFMS\IFMS.Master\*.svc \\192.168.1.201\Indilinga\IFMS\Server\. /d /y
@xcopy C:\Dev\Afx.net\Source\Clients\IFMS\IFMS.Master\App_Code\*.cs \\192.168.1.201\Indilinga\IFMS\Server\App_Code\. /d /y
@xcopy C:\Dev\Afx.net\Source\Clients\IFMS\IFMS.Master\bin\*.dll \\192.168.1.201\Indilinga\IFMS\Server\bin\. /d /y
@del \\192.168.1.201\Indilinga\IFMS\Server\bin\ScheduleManagement.*
@del \\192.168.1.201\Indilinga\IFMS\Server\ScheduleManagementService.svc

@echo Client Deployment
@xcopy C:\Dev\Afx.net\Source\Clients\IFMS\IFMS.Test\bin\x86\Debug\*.dll \\192.168.1.201\Indilinga\IFMS\Client\Source\. /d /y /s
@xcopy C:\Dev\Afx.net\Source\Clients\IFMS\IFMS.Test\bin\x86\Debug\*.exe \\192.168.1.201\Indilinga\IFMS\Client\Source\. /d /y /s
@del \\192.168.1.201\Indilinga\IFMS\Client\Source\ScheduleManagement.*
@del \\192.168.1.201\Indilinga\IFMS\Client\Source\*.vshost.exe
@del \\192.168.1.201\Indilinga\IFMS\Client\Source\*.vshost.exe.config

@echo Optional Server Deployment
@rem @xcopy C:\Dev\Afx.net\Source\Clients\IFMS\IFMS.Master\Web.config \\192.168.1.201\Indilinga\IFMS\Server\. /d /y
@rem @xcopy C:\Dev\Afx.net\Source\Clients\IFMS\IFMS.Master\*.asax \\192.168.1.201\Indilinga\IFMS\Server\. /d /y

@rem @xcopy C:\Dev\Afx.net\Source\Clients\IFMS\IFMS.Master\bin\*.pdb \\192.168.1.201\Indilinga\IFMS\Server\bin\. /d /y

@echo Optional Client Deployment
@rem @xcopy C:\Dev\Afx.net\Source\Clients\IFMS\IFMS.Test\bin\x86\Debug\server.xml \\192.168.1.201\Indilinga\IFMS\Client\Source\. /d /y /s
@rem @xcopy C:\Dev\Afx.net\Source\Clients\IFMS\IFMS.Test\bin\x86\Debug\*.config \\192.168.1.201\Indilinga\IFMS\Client\Source\. /d /y /s

@rem @xcopy C:\Dev\Afx.net\Source\Clients\Imberbe\IFMS.Imberbe\bin\x86\Debug\*.pdb \\192.168.1.201\Indilinga\IFMS\Client\Source\. /d /y /s

@pause
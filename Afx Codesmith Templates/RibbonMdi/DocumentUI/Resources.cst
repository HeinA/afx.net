﻿<%-- 
Name:
Author: 
Description: 
--%>
<%@ Template Language="C#" TargetLanguage="XAML" %>
<%@ Property Name="AssemblyNamespace" Optional="False" Type="System.String" %>
<%@ Property Name="Namespace" Optional="False" Type="System.String" %>
<%@ Property Name="TypeName" Optional="False" Type="System.String" %>
<%@ Assembly Name="Afx.Codesmith" Path="..\..\Common" %>
<%@ Import Namespace="Afx.Codesmith" %>
<% NamespaceHelper.Initialize(AssemblyNamespace); %>
<ResourceDictionary xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
                    xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
                    xmlns:afx="http://openafx.net/Prism"
                    xmlns:prism="http://www.codeplex.com/prism"
                    xmlns:local="clr-namespace:<% =NamespaceHelper.Instance.ModuleName %>.Prism.Documents.<% =TypeName %>"
                    xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
                    xmlns:ignore="http://openafx.net/Ignore"
                    mc:Ignorable="ignore">

  <ResourceDictionary.MergedDictionaries>
    <ResourceDictionary Source="pack://application:,,,/Afx.Prism.RibbonMdi;Component/MdiDialogResources.xaml" />
    <ResourceDictionary Source="pack://application:,,,/Afx.Prism.RibbonMdi;component/Documents/Resources.xaml" />
  </ResourceDictionary.MergedDictionaries>

  <DataTemplate DataType="{x:Type local:<% =TypeName %>SearchViewModel}">
    <DockPanel>
      <Grid DockPanel.Dock="Top">
        <Grid.ColumnDefinitions>
          <ColumnDefinition Width="200" />
          <ColumnDefinition Width="*" />
        </Grid.ColumnDefinitions>
        <Grid.RowDefinitions>
          <RowDefinition Height="Auto" />
          <RowDefinition Height="Auto" />
          <RowDefinition Height="Auto" />
          <RowDefinition Height="Auto" />
          <RowDefinition Height="Auto" />
          <RowDefinition Height="Auto" />
          <RowDefinition Height="Auto" />
        </Grid.RowDefinitions>

        <TextBlock TextAlignment="Right"
                   VerticalAlignment="Center"
                   Margin="3,3,3,3"
                   Grid.Column="0"
                   Grid.Row="0">Document Number</TextBlock>
        <afx:AfxTextBox Grid.Column="1"
                        Grid.Row="0"
                        Margin="3,3,3,3"
                        Text="{Binding DocumentNumber, UpdateSourceTrigger=PropertyChanged}" />

        <TextBlock TextAlignment="Right"
                   VerticalAlignment="Center"
                   Margin="3,3,3,3"
                   Grid.Column="0"
                   Grid.Row="1">State</TextBlock>
        <ComboBox Grid.Column="1"
                  Grid.Row="1"
                  ItemsSource="{Binding DocumentTypeStates}"
                  SelectedValue="{Binding DocumentTypeState, Mode=TwoWay}"
                  DisplayMemberPath="Name"
                  Margin="3,3,3,3" />

        <StackPanel Orientation="Horizontal"
                    Grid.Column="1"
                    Grid.Row="2"
                    VerticalAlignment="Center"
                    HorizontalAlignment="Left">
          <StackPanel.Resources>
            <Style TargetType="{x:Type RadioButton}">
              <Setter Property="Margin"
                      Value="6,3,6,3" />
            </Style>
          </StackPanel.Resources>
          <RadioButton VerticalAlignment="Center"
                       GroupName="G1"
                       Content="Date"
                       IsChecked="{Binding UseSingleDate, Mode=TwoWay, UpdateSourceTrigger=PropertyChanged}" />

          <RadioButton VerticalAlignment="Center"
                       GroupName="G1"
                       Content="Date Range"
                       IsChecked="{Binding UseDateRange, Mode=TwoWay, UpdateSourceTrigger=PropertyChanged}" />
        </StackPanel>

        <TextBlock TextAlignment="Right"
                   VerticalAlignment="Center"
                   Margin="3,3,3,3"
                   Grid.Column="0"
                   Grid.Row="3"
                   Visibility="{Binding UseSingleDate, Converter={StaticResource BooleanVisibilityConverter}}">Date</TextBlock>
        <DatePicker Grid.Column="1"
                    Grid.Row="3"
                    Width="200"
                    HorizontalAlignment="Left"
                    Margin="3,3,3,3"
                    SelectedDate="{Binding Date1}"
                    Visibility="{Binding UseSingleDate, Converter={StaticResource BooleanVisibilityConverter}}" />

        <TextBlock TextAlignment="Right"
                   VerticalAlignment="Center"
                   Margin="3,3,3,3"
                   Grid.Column="0"
                   Grid.Row="4"
                   Visibility="{Binding UseDateRange, Converter={StaticResource BooleanVisibilityConverter}}">Start Date</TextBlock>
        <DatePicker Grid.Column="1"
                    Grid.Row="4"
                    Width="200"
                    HorizontalAlignment="Left"
                    Margin="3,3,3,3"
                    SelectedDate="{Binding Date1}"
                    Visibility="{Binding UseDateRange, Converter={StaticResource BooleanVisibilityConverter}}" />

        <TextBlock TextAlignment="Right"
                   VerticalAlignment="Center"
                   Margin="3,3,3,3"
                   Grid.Column="0"
                   Grid.Row="5"
                   Visibility="{Binding UseDateRange, Converter={StaticResource BooleanVisibilityConverter}}">End Date</TextBlock>
        <DatePicker Grid.Column="1"
                    Grid.Row="5"
                    Width="200"
                    HorizontalAlignment="Left"
                    Margin="3,3,3,3"
                    SelectedDate="{Binding Date2}"
                    Visibility="{Binding UseDateRange, Converter={StaticResource BooleanVisibilityConverter}}" />

        <TextBlock TextAlignment="Right"
                   VerticalAlignment="Center"
                   Margin="3,3,3,3"
                   Grid.Column="0"
                   Grid.Row="6">Organizational Unit</TextBlock>
        <ComboBox Grid.Column="1"
                  Grid.Row="6"
                  ItemsSource="{Binding OrganizationalUnits}"
                  SelectedValue="{Binding OrganizationalUnit, Mode=TwoWay}"
                  DisplayMemberPath="Name"
                  Margin="3,3,3,3" />

      </Grid>
      
      <Button DockPanel.Dock="Top"
              Command="{Binding SearchCommand}"
              IsDefault="True"
              Margin="0,3,0,3">Search</Button>

      <afx:AfxSearchResultListView Results="{Binding Results}"
                                   Margin="0,3,0,0"
                                   Command="{Binding ItemActivatedCommand}"
                                   LastActivatedResult="{Binding LastActivatedResult}">
        <ListView.Resources>
          <Style TargetType="{x:Type ListViewItem}">
            <Setter Property="IsSelected"
                    Value="{Binding IsSelected, Mode=TwoWay}" />
            <Setter Property="afx:FocusExtension.IsFocused"
                    Value="{Binding IsFocused}" />
            <Setter Property="HorizontalContentAlignment"
                    Value="Left" />
            <Setter Property="VerticalContentAlignment"
                    Value="Center" />
          </Style>
        </ListView.Resources>
      </afx:AfxSearchResultListView>
    </DockPanel>
  </DataTemplate>


  <DataTemplate DataType="{x:Type local:<% =TypeName %>ViewModel}">
    <ContentControl Template="{StaticResource ResourceKey={x:Static afx:MdiResourceKeys.Document}}" />
  </DataTemplate>

  <DataTemplate DataType="{x:Type local:<% =TypeName %>HeaderViewModel}"
                x:Shared="False">
    <afx:AfxStateContentControl>
      <afx:AfxStateContentControl.EditableContentTemplate>
        <DataTemplate>
            <afx:ErrorProvider IgnoreProperties="Title">
              <DockPanel Margin="0,0,0,0">
                <Grid Margin="0,0,0,3"
                      DockPanel.Dock="Top">
                  <Grid.ColumnDefinitions>
                    <ColumnDefinition Width="200" />
                    <ColumnDefinition Width="250" />
                  </Grid.ColumnDefinitions>
                  <Grid.RowDefinitions>
                    <RowDefinition Height="Auto" />
                    <RowDefinition Height="Auto" />
                    <RowDefinition Height="Auto" />
                  </Grid.RowDefinitions>

                    <TextBlock TextAlignment="Right"
                               VerticalAlignment="Center"
                               Margin="3,3,3,3"
                               Grid.Column="0"
                               Grid.Row="0"
                               Text="Document Number" />
                    <DockPanel Margin="3,3,3,3"
                               Grid.Row="0"
                               Grid.Column="1">
                      <TextBlock Foreground="Red"
                                 Margin="3,3,0,0"
                                 DockPanel.Dock="Right"
                                 Text="{Binding DocumentNumberError}"
                                 Visibility="{Binding DocumentNumberError, Converter={StaticResource StringToVisibilityConverter}}" />

                      <afx:AfxTextBox afx:FocusExtension.IsFocused="True"
                                      Text="{Binding Model.DocumentNumber, Mode=TwoWay}" />
                    </DockPanel>

                    <TextBlock TextAlignment="Right"
                               VerticalAlignment="Center"
                               Margin="3,3,3,0"
                               Grid.Column="0"
                               Grid.Row="1"
                               Text="Document Date" />
                    <DatePicker Grid.Column="1"
                                Grid.Row="1"
                                KeyboardNavigation.TabNavigation="Continue"
                                afx:FocusExtension.IsFocused="True"
                                Margin="3,3,3,3"
                                SelectedDateFormat="Long"
                                SelectedDate="{Binding Model.DocumentDate}" />

                    <TextBlock TextAlignment="Right"
                               VerticalAlignment="Center"
                               Margin="3,3,3,0"
                               Grid.Column="0"
                               Grid.Row="2">Organizational Unit</TextBlock>
                    <ComboBox Grid.Column="1"
                              Grid.Row="2"
                              Margin="3,3,3,3"
                              ItemsSource="{Binding OrganizationalUnits}"
                              SelectedItem="{Binding Model.OrganizationalUnit}"
                              DisplayMemberPath="Name" />

                </Grid>
              </DockPanel>
            </afx:ErrorProvider>        
        </DataTemplate>
      </afx:AfxStateContentControl.EditableContentTemplate>      
      <afx:AfxStateContentControl.ReadOnlyContentTemplate>
        <DataTemplate>
              <Grid DockPanel.Dock="Top">
                <Grid.ColumnDefinitions>
                  <ColumnDefinition Width="200" />
                  <ColumnDefinition Width="250" />
                </Grid.ColumnDefinitions>
                <Grid.RowDefinitions>
                  <RowDefinition Height="Auto" />
                  <RowDefinition Height="Auto" />
                  <RowDefinition Height="Auto" />
                </Grid.RowDefinitions>

                <TextBlock TextAlignment="Right"
                           VerticalAlignment="Center"
                           Margin="3,3,3,3"
                           Grid.Column="0"
                           Grid.Row="0"
                           Text="Document Number" />
                <afx:AfxTextBox Grid.Column="1"
                                Grid.Row="0"
                                Margin="3,3,3,3"
                                IsReadOnly="True"
                                afx:FocusExtension.IsFocused="True"
                                Text="{Binding Model.DocumentNumber}" />

                <TextBlock TextAlignment="Right"
                           VerticalAlignment="Center"
                           Margin="3,3,3,0"
                           Grid.Column="0"
                           Grid.Row="1"
                           Text="Document Date" />
                <TextBox Grid.Column="1"
                         Grid.Row="1"
                         Margin="3,3,3,3"
                         Text="{Binding Model.DocumentDate, StringFormat={}{0:D}}"
                         IsReadOnly="True" />

                <TextBlock TextAlignment="Right"
                           VerticalAlignment="Center"
                           Margin="3,3,3,0"
                           Grid.Column="0"
                           Grid.Row="2">Organizational Unit</TextBlock>
                <TextBox Grid.Column="1"
                         Grid.Row="2"
                         Margin="3,3,3,3"
                         Text="{Binding Model.OrganizationalUnit.Name}"
                         IsReadOnly="True" />

              </Grid>
        </DataTemplate>
      </afx:AfxStateContentControl.ReadOnlyContentTemplate>
    </afx:AfxStateContentControl>
  </DataTemplate>

</ResourceDictionary>
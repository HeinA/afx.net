﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Text;
using System.Threading.Tasks;

namespace Afx.ServiceModel
{
  public interface IDisposableWrapper<T> : IDisposable
  {
    T Instance { get; }
  }

  internal class DisposableWrapper<T> : IDisposableWrapper<T> where T : class
  {
    protected T InternalInstance { get; private set; }
    public T Instance { get; private set; }
    protected ChannelFactory<T> Factory { get; set; }

    public DisposableWrapper(ChannelFactory<T> factory)
    {
      Factory = factory;
      CreateInstance();
    }

    void CreateInstance()
    {
      InternalInstance = Factory.CreateChannel(Factory.Endpoint.Address);
      IClientChannel channel = InternalInstance as IClientChannel;
      Instance = InternalInstance;
    }

    protected virtual void Dispose(bool disposing)
    {
      if (disposing)
      {
        (InternalInstance as IDisposable).Dispose();
      }
    }

    public void Dispose()
    {
      try
      {
        Dispose(true);
        GC.SuppressFinalize(this);
      }
      catch
      {
      }
      InternalInstance = Instance = default(T);
    }
  }

  internal class ClientWrapper<TProxy> : DisposableWrapper<TProxy>
      where TProxy : class
  {
    public ClientWrapper(ChannelFactory<TProxy> proxy) : base(proxy) { }
    protected override void Dispose(bool disposing)
    {
      if (disposing)
      {
        if (this.InternalInstance != null)
        {
          IClientChannel channel = this.InternalInstance as IClientChannel;
          if (channel.State == CommunicationState.Faulted)
          {
            channel.Abort();
          }
          else
          {
            channel.Close();
          }
        }
      }

      base.Dispose(disposing);
    }
  }

  static class WCFExtensions
  {
    public static IDisposableWrapper<TProxy> Wrap<TProxy>(
        this ChannelFactory<TProxy> proxy)
        where TProxy : class
    {

      return new ClientWrapper<TProxy>(proxy);
    }
  }

  public static class ServiceFactory
  {
    static object mLock = new object();

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
    public static IDisposableWrapper<T> GetService<T>(string serviceName = null)
      where T : class
    {
      lock (mLock)
      {
        ChannelFactory<T> factory = null;
        if (serviceName == null) factory = new ChannelFactory<T>(typeof(T).FullName);
        else factory = new ChannelFactory<T>(string.Format("{0}::{1}", serviceName, typeof(T).FullName));
        factory.Open();
        IDisposableWrapper<T> wrapper = WCFExtensions.Wrap<T>(factory);
        return wrapper;
      }
    }
  }
}

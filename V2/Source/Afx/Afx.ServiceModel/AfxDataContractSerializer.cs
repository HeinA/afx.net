﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using Afx.ComponentModel;

namespace Afx.ServiceModel
{
  public class AfxDataContractSerializer : XmlObjectSerializer
  {
    System.Runtime.Serialization.DataContractSerializer mSerializer;

    public AfxDataContractSerializer(System.Runtime.Serialization.DataContractSerializer serializer)
    {
      mSerializer = serializer;
    }

    public override bool IsStartObject(System.Xml.XmlDictionaryReader reader)
    {
      return mSerializer.IsStartObject(reader);
    }

    public override object ReadObject(System.Xml.XmlDictionaryReader reader, bool verifyObjectName)
    {
      return mSerializer.ReadObject(reader, verifyObjectName);
    }

    public override void WriteEndObject(System.Xml.XmlDictionaryWriter writer)
    {
      mSerializer.WriteEndObject(writer);
    }

    public override void WriteObjectContent(System.Xml.XmlDictionaryWriter writer, object graph)
    {
      mSerializer.WriteObjectContent(writer, graph);
    }

    public override void WriteStartObject(System.Xml.XmlDictionaryWriter writer, object graph)
    {
      mSerializer.WriteStartObject(writer, graph);
      if (graph is BusinessObject)
      {
        int i = 1;
        foreach (var ns in KnownTypesProvider.KnownNamespaces)
        {
          writer.WriteAttributeString("xmlns", string.Format("ns{0}", i++), null, ns);
        }
      }
    }  
  }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel.Channels;
using System.ServiceModel.Dispatcher;
using System.ServiceModel.Description;

namespace Afx.ServiceModel.Description
{
  [AttributeUsage(AttributeTargets.Method)]
  public class SerializeCachedTypesAttribute : Attribute, IOperationBehavior
  {
    Type[] mAllowedTypes;

    public SerializeCachedTypesAttribute(params Type[] allowedTypes)
    {
      mAllowedTypes = allowedTypes;
    }

    public void AddBindingParameters(OperationDescription operationDescription, BindingParameterCollection bindingParameters)
    {
    }

    public void ApplyClientBehavior(OperationDescription operationDescription, ClientOperation clientOperation)
    {
    }

    public void ApplyDispatchBehavior(OperationDescription operationDescription, DispatchOperation dispatchOperation)
    {
    }

    public void Validate(OperationDescription operationDescription)
    {
      var dcsob = operationDescription.Behaviors.Find<System.ServiceModel.Description.DataContractSerializerOperationBehavior>();
      //dcsob.DataContractSurrogate = new AllowSpecifiedCachedTypesSurrogate(mAllowedTypes);
    }
  }
}

﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.ServiceModel.Channels;
using System.ServiceModel.Dispatcher;
using Afx.Runtime.Serialization;

namespace Afx.ServiceModel.Description
{
  [AttributeUsage(AttributeTargets.Interface)]
  public class AutoKnownTypesAttribute : Attribute, IContractBehavior
  {
    public virtual void AddBindingParameters(ContractDescription contractDescription, ServiceEndpoint endpoint, BindingParameterCollection bindingParameters)
    {
    }

    public virtual void ApplyClientBehavior(ContractDescription contractDescription, ServiceEndpoint endpoint, ClientRuntime clientRuntime)
    {
    }

    public virtual void ApplyDispatchBehavior(ContractDescription contractDescription, ServiceEndpoint endpoint, DispatchRuntime dispatchRuntime)
    {
    }

    public virtual void Validate(ContractDescription contractDescription, ServiceEndpoint endpoint)
    {
      #region Apply Known Types & Fault Contracts

      foreach (OperationDescription opDesc in endpoint.Contract.Operations)
      {
        foreach (Type t in KnownTypesProvider.KnownTypes)
        {
          if (!opDesc.KnownTypes.Contains(t)) opDesc.KnownTypes.Add(t);
        }

        foreach (Type t in KnownTypesProvider.KnownFaults)
        {
          FaultDescription faultDescription = new FaultDescription(string.Format(CultureInfo.CurrentCulture, "{0}/{1}/{2}_{3}", opDesc.DeclaringContract.Namespace, opDesc.DeclaringContract.Name, opDesc.Name, t.Name));
          FaultContractAttribute fca = t.GetCustomAttribute<FaultContractAttribute>();
          faultDescription.Namespace = fca.Namespace;
          faultDescription.DetailType = t;
          faultDescription.Name = t.Name;
          if (opDesc.Faults.FirstOrDefault(fd => fd.Name == t.Name) == null) opDesc.Faults.Add(faultDescription);
        }
      }

      #endregion
    }
  }
}

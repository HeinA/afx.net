﻿using Afx.ServiceModel.Dispatcher;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using System.Text;
using System.Threading.Tasks;

namespace Afx.ServiceModel.Description
{
  [AttributeUsage(AttributeTargets.Interface)]
  public class DefaultServiceBehaviorAttribute : AutoKnownTypesAttribute
  {
    public override void ApplyClientBehavior(ContractDescription contractDescription, ServiceEndpoint endpoint, ClientRuntime clientRuntime)
    {
      if (clientRuntime.MessageInspectors.FirstOrDefault(o => o.GetType() == typeof(AfxClientMessageInspector)) == null)
        clientRuntime.MessageInspectors.Add(new AfxClientMessageInspector());

      this.ReplaceSerializerOperationBehavior(contractDescription);  

      base.ApplyClientBehavior(contractDescription, endpoint, clientRuntime);
    }

    public override void ApplyDispatchBehavior(ContractDescription contractDescription, ServiceEndpoint endpoint, DispatchRuntime dispatchRuntime)
    {
      if (dispatchRuntime.MessageInspectors.FirstOrDefault(o => o.GetType() == typeof(AfxDispatchMessageInspector)) == null)
        dispatchRuntime.MessageInspectors.Add(new AfxDispatchMessageInspector());

      this.ReplaceSerializerOperationBehavior(contractDescription);  

      base.ApplyDispatchBehavior(contractDescription, endpoint, dispatchRuntime);
    }

    public override void Validate(ContractDescription contractDescription, ServiceEndpoint endpoint)
    {
      CustomBinding b = new CustomBinding(endpoint.Binding);
      BinaryMessageEncodingBindingElement bme = b.Elements.OfType<BinaryMessageEncodingBindingElement>().FirstOrDefault();
      if (bme != null)
      {
        bme.CompressionFormat = CompressionFormat.GZip;
        endpoint.Binding = b;
      }

      base.Validate(contractDescription, endpoint);
    }

    private void ReplaceSerializerOperationBehavior(ContractDescription contract)
    {
      foreach (OperationDescription od in contract.Operations)
      {
        var dcsob = od.Behaviors.Find<System.ServiceModel.Description.DataContractSerializerOperationBehavior>();
        var dcsobNew = new AfxSerializerOperationBehavior(od);
        //dcsobNew.DataContractSurrogate = dcsob.DataContractSurrogate;
        od.Behaviors[od.Behaviors.IndexOf(dcsob)] = dcsobNew;
        //if (dcsobNew.DataContractSurrogate == null) dcsobNew.DataContractSurrogate = new DefaultDataContractSurrogate();
      }
    }
  }
}

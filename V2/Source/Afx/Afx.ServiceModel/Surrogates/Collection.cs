﻿using Afx.ComponentModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Afx.ServiceModel.Surrogates
{
  [DataContract(Namespace = Afx.ServiceModel.Constants.SurrogateNamespace)]
  public class Collection
  {
    #region BusinessObject Owner

    public const string OwnerProperty = "Owner";
    [DataMember(Name = "Owner")]
    BusinessObject mOwner;
    public BusinessObject Owner
    {
      get { return mOwner; }
      set { mOwner = value; }
    }

    #endregion

    #region object[] Items

    public const string ItemsProperty = "Items";
    [DataMember(Name = "Items")]
    System.Collections.ObjectModel.Collection<object> mItems = new System.Collections.ObjectModel.Collection<object>();
    public System.Collections.ObjectModel.Collection<object> Items
    {
      get { return mItems; }
      set { mItems = value; }
    }

    #endregion
  }
}

﻿using Afx.ComponentModel;
using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Afx.ServiceModel.Surrogates
{
  public abstract class DataContractSurrogateBase : IDataContractSurrogate
  {
    public virtual object GetCustomDataToExport(Type clrType, Type dataContractType)
    {
      return null;
    }

    public virtual object GetCustomDataToExport(MemberInfo memberInfo, Type dataContractType)
    {
      return null;
    }

    public virtual Type GetDataContractType(Type type)
    {
      return type;
    }

    public virtual object GetDeserializedObject(object obj, Type targetType)
    {
      try
      {
        //Type genericType = GetGenericSubClass(typeof(Afx.ComponentModel.Collections.Collection<>), targetType);
        //if (genericType != null)
        //{
        //  Afx.ComponentModel.Collections.ICollection ic = (Afx.ComponentModel.Collections.ICollection)Activator.CreateInstance(targetType);
        //  Collection c = obj as Collection;
        //  ic.Owner = c.Owner;
        //  foreach (object o in c.Items)
        //  {
        //    ic.Add((BusinessObject)GetDeserializedObject(o, genericType.GetGenericArguments()[0]));
        //  }
        //  return ic;
        //}

        return obj;
      }
      catch
      {
        throw;
      }
    }

    public virtual void GetKnownCustomDataTypes(System.Collections.ObjectModel.Collection<Type> customDataTypes)
    {
    }

    public virtual object GetObjectToSerialize(object obj, Type targetType)
    {
      try
      {
        //Type genericType = GetGenericSubClass(typeof(Afx.ComponentModel.Collections.Collection<>), targetType);
        //if (genericType != null)
        //{
        //  Afx.ComponentModel.Collections.ICollection ic = obj as Afx.ComponentModel.Collections.ICollection;
        //  Collection c = new Collection();
        //  c.Owner = ic.Owner;
        //  foreach (object o in ic)
        //  {
        //    c.Items.Add(GetObjectToSerialize(o, genericType.GetGenericArguments()[0]));
        //  }
        //  return c;
        //}

        return obj;
      }
      catch
      {
        throw;
      }
    }

    public virtual Type GetReferencedTypeOnImport(string typeName, string typeNamespace, object customData)
    {
      return null;
    }

    public virtual CodeTypeDeclaration ProcessImportedType(CodeTypeDeclaration typeDeclaration, CodeCompileUnit compileUnit)
    {
      return null;
    }

    protected Type GetGenericSubClass(Type generic, Type toCheck)
    {
      while (toCheck != null && toCheck != typeof(object))
      {
        var cur = toCheck.IsGenericType ? toCheck.GetGenericTypeDefinition() : toCheck;
        if (generic == cur)
        {
          return toCheck;
        }
        toCheck = toCheck.BaseType;
      }
      return null;
    }
  }
}

﻿using Afx.ServiceModel.Surrogates;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Afx.ServiceModel.Surrogates
{
  class DefaultDataContractSurrogate : DataContractSurrogateBase
  {
    public override Type GetDataContractType(Type type)
    {
      //if (type == typeof(CacheReference))
      //{
      //  Type t = Type.GetType("Test.Business.TestItem, Test.Business");
      //  return t;
      //}

      return base.GetDataContractType(type);
    }

    public override object GetDeserializedObject(object obj, Type targetType)
    {
      if (targetType.Name == "TestItem")
      {
        return Activator.CreateInstance(targetType);
      }

      return base.GetDeserializedObject(obj, targetType);
    }

    public override object GetObjectToSerialize(object obj, Type targetType)
    {
      if (targetType.Name == "TestItem")
      {
        return new CacheReference(Guid.NewGuid());
      }

      return base.GetObjectToSerialize(obj, targetType);
    }
  }
}

﻿using Afx.ComponentModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.ServiceModel.Surrogates
{
  internal interface ICollection
  {
    BusinessObject Owner { get; set; }
    BusinessObject[] Items { get; set; }
  }
}

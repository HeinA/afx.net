﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace Afx.ServiceModel.Surrogates
{
  [DataContract(Namespace = Afx.ServiceModel.Constants.SurrogateNamespace, IsReference = true)]
  public class CacheReference //: IXmlSerializable
  {
    public CacheReference()
    {
    }

    public CacheReference(Guid reference)
    {
      Reference = reference;
    }

    #region Guid Reference

    public const string ReferenceProperty = "Reference";
    [DataMember(Name = "Reference")]
    Guid mReference;
    public Guid Reference
    {
      get { return mReference; }
      private set { mReference = value; }
    }

    #endregion

    //public XmlSchema GetSchema()
    //{
    //  XmlSchema s = new XmlSchema();
    //  s.Namespaces = new XmlSerializerNamespaces(new XmlQualifiedName[] { new XmlQualifiedName(Afx.Constants.AssemblyNamespace) });
    //  return s;
    //}

    //public void ReadXml(XmlReader reader)
    //{
    //  Reference = Guid.Parse(reader.GetAttribute(ReferenceProperty));
    //}

    //public void WriteXml(XmlWriter writer)
    //{
    //  writer.WriteAttributeString(ReferenceProperty, Reference.ToString());
    //}
  }
}

﻿using Afx.Runtime.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using Afx.ComponentModel;
using Afx.Utilities;

namespace Afx.ServiceModel
{
   class AfxSerializer : XmlObjectSerializer
  {
    Type mType;
    XmlElementAdapter mXmlElementAdapter;

    public AfxSerializer(Type type)
    {
      mType = type;
    }

     public override bool IsStartObject(System.Xml.XmlDictionaryReader reader)
    {
      return true;
    }

    public override object ReadObject(System.Xml.XmlDictionaryReader reader, bool verifyObjectName)
    {
      return null;
    }

    public override void WriteEndObject(System.Xml.XmlDictionaryWriter writer)
    {
      writer.WriteEndElement();
    }

    public override void WriteObjectContent(System.Xml.XmlDictionaryWriter writer, object graph)
    {
      if (IsSimpleType(graph))
      {
        writer.WriteAttributeString(KnownTypesProvider.GetNamespacePrefix(Afx.Constants.AssemblyNamespace), "Type", Afx.Constants.AssemblyNamespace, KnownTypesProvider.GetUri(graph.GetType().GetTypeInfo()).ToString());
        writer.WriteAttributeString(KnownTypesProvider.GetNamespacePrefix(Afx.Constants.AssemblyNamespace), "Value", Afx.Constants.AssemblyNamespace, SimpleValueConverter.ConvertToString(graph.GetType(), graph));
      }
      else
      {
        XmlConverter converter = XmlConverter.GetConverter(graph.GetType().GetTypeInfo());
        mXmlElementAdapter = new XmlElementAdapter(converter);
        mXmlElementAdapter.WriteXml(writer, (BusinessObject)graph);
      }
    }

    public override void WriteStartObject(System.Xml.XmlDictionaryWriter writer, object graph)
    {
      writer.WriteStartElement(KnownTypesProvider.GetNamespacePrefix(Afx.Constants.AssemblyNamespace), "Parameter", Afx.Constants.AssemblyNamespace);
    }

    bool IsSimpleType(object obj)
    {
      if (obj.GetType().IsValueType || obj.GetType().Equals(typeof(string)))
      {
        return true;
      }
      return false;
    }
  }
}

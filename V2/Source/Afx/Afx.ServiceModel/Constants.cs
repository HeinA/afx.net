﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.ServiceModel
{
  internal static class Constants
  {
    public const string SurrogateNamespace = "http://openafx.net/Business/Surrogates/";

    public const string Collection = "Collection";
  }
}

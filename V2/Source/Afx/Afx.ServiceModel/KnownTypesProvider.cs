﻿using Afx.ComponentModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Afx.ServiceModel
{
  static class KnownTypesProvider
  {
    static List<Type> mKnownTypes = new List<Type>();
    static List<Type> mKnownFaults = new List<Type>();

    static List<string> mKnownNamespaces = new List<string>();
    static Dictionary<Uri, Type> mKnownTypeLookup = new Dictionary<Uri, Type>();
    static Dictionary<Type, Uri> mUriLookup = new Dictionary<Type, Uri>();
    static Dictionary<Type, string> mNamespaceLookup = new Dictionary<Type, string>();

    static object mLock = new object();

    static KnownTypesProvider()
    {
      PreLoadDeployedAssemblies();

      foreach (var assembly in AppDomain.CurrentDomain.GetAssemblies())
      {
        foreach (var t in assembly.DefinedTypes.Where(t => typeof(BusinessObject).IsAssignableFrom(t) && t.IsDefined(typeof(DataContractAttribute)) && !t.IsGenericTypeDefinition))
        {
          RegisterKnownType(t);
        }

        //RegisterKnownType(typeof(CacheReference));
        //RegisterKnownType(typeof(Collection));

        foreach (var t in assembly.DefinedTypes.Where(t => t.IsDefined(typeof(FaultContractAttribute)) && !t.IsGenericTypeDefinition))
        {
          mKnownFaults.Add(t);
        }
      }
    }

    static void RegisterKnownType(Type knownType)
    {
      try
      {
        DataContractAttribute dca = knownType.GetCustomAttribute<DataContractAttribute>();
        if (mKnownTypes.Contains(knownType)) return;
        mKnownTypes.Add(knownType);
        if (!mKnownNamespaces.Contains(dca.Namespace)) mKnownNamespaces.Add(dca.Namespace);
        mNamespaceLookup.Add(knownType, dca.Namespace);
        Uri uri = new Uri(string.Format("{0}{1}", dca.Namespace, knownType.Name));
        mKnownTypeLookup.Add(uri, knownType);
        mUriLookup.Add(knownType, uri);
      }
      catch
      {
        throw;
      }
    }

    public static IEnumerable<Type> KnownTypes
    {
      get { return mKnownTypes; }
    }

    public static IEnumerable<Type> KnownFaults
    {
      get { return mKnownFaults; }
    }

    public static IEnumerable<string> KnownNamespaces
    {
      get { return mKnownNamespaces; }
    }

    public static string GetNamespace(Type knownType)
    {
      if (!mNamespaceLookup.ContainsKey(knownType)) throw new InvalidOperationException("Type is unknown"); //TODO: int
      return mNamespaceLookup[knownType];
    }

    public static Type GetKnownType(Uri uri)
    {
      if (!mKnownTypeLookup.ContainsKey(uri)) throw new InvalidOperationException("Uri is unknown"); //TODO: int
      return mKnownTypeLookup[uri];
    }

    public static Uri GetUri(Type knownType)
    {
      if (!mUriLookup.ContainsKey(knownType)) throw new InvalidOperationException("Type is unknown"); //TODO: int
      return mUriLookup[knownType];
    }

    #region void PreLoadDeployedAssemblies()

    static bool mAssembliesLoaded = false;
    public static void PreLoadDeployedAssemblies()
    {
      if (mAssembliesLoaded) return;
      foreach (var path in GetBinFolders())
      {
        PreLoadAssembliesFromPath(path);
      }
      mAssembliesLoaded = true;
    }

    private static IEnumerable<string> GetBinFolders()
    {
      List<string> toReturn = new List<string>();
      if (HttpContext.Current != null)
      {
        toReturn.Add(HttpRuntime.BinDirectory);
      }
      else
      {
        toReturn.Add(AppDomain.CurrentDomain.BaseDirectory);
      }

      return toReturn;
    }

    private static void PreLoadAssembliesFromPath(string p)
    {
      //S.O. NOTE: ELIDED - ALL EXCEPTION HANDLING FOR BREVITY

      //get all .dll files from the specified path and load the lot
      FileInfo[] files = null;
      //you might not want recursion - handy for localised assemblies 
      //though especially.
      files = new DirectoryInfo(p).GetFiles("*.dll",
          SearchOption.AllDirectories);

      AssemblyName a = null;
      string s = null;
      foreach (var fi in files)
      {
        s = fi.FullName;
        //now get the name of the assembly you've found, without loading it
        //though (assuming .Net 2+ of course).
        a = AssemblyName.GetAssemblyName(s);
        //sanity check - make sure we don't already have an assembly loaded
        //that, if this assembly name was passed to the loaded, would actually
        //be resolved as that assembly.  Might be unnecessary - but makes me
        //happy :)
        if (!AppDomain.CurrentDomain.GetAssemblies().Any(assembly =>
          AssemblyName.ReferenceMatchesDefinition(a, assembly.GetName())))
        {
          //crucial - USE THE ASSEMBLY NAME.
          //in a web app, this assembly will automatically be bound from the 
          //Asp.Net Temporary folder from where the site actually runs.
          Assembly.Load(a);
        }
      }
    }

    #endregion
  }
}

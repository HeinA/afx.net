﻿using Afx.ComponentModel;
using Afx.ComponentModel.Collections;
using Afx.ComponentModel.Markup;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Afx.Runtime.Serialization
{
  class AfxTypeSerializer
  {
    static Dictionary<Type, AfxTypeSerializer> mTypeSerializerDictionary = new Dictionary<Type, AfxTypeSerializer>();

    Collection<Type> mInheritanceList = new Collection<Type>();
    Collection<SimpleMemberAccessor> mSimpleMembers = new Collection<SimpleMemberAccessor>();
    Collection<ComplexMemberAccessor> mComplexMembers = new Collection<ComplexMemberAccessor>();

    AfxTypeSerializer(Type objectType)
    {
      ObjectType = objectType;
      Uri = KnownTypesProvider.GetTypeUri(ObjectType);

      BuildInheritanceList(ObjectType);
      Initialize();
    }

    #region Type ObjectType

    Type mObjectType;
    public Type ObjectType
    {
      get { return mObjectType; }
      private set { mObjectType = value; }
    }

    #endregion

    #region Uri Uri

    Uri mUri;
    public Uri Uri
    {
      get { return mUri; }
      private set { mUri = value; }
    }

    #endregion

    #region void ParseNamespaces(...)

    public void ParseNamespaces(object graph, SerializationContext context)
    {
      BusinessObject bo = graph as BusinessObject;
      if (bo != null)
      {
        if (context.ContainsObject(bo.Id))
        {
          return;
        }

        context.GetPrefix(Uri.ToString());
        context.AddObject(bo);
      }

      IBusinessObjectCollection boc = graph as IBusinessObjectCollection;
      if (boc != null)
      {
        string ns = KnownTypesProvider.GetTypeUri(boc.GetType()).ToString();
        context.GetPrefix(ns);
        foreach (var item in boc)
        {
          Type type = item.GetType();
          context.GetPrefix(KnownTypesProvider.GetTypeUri(type).ToString());
          AfxTypeSerializer ts = AfxTypeSerializer.GetTypeSerializer(type);
          ts.ParseNamespaces(item, context);
        }
      }

      foreach (var a in mComplexMembers)
      {
        context.GetPrefix(a.Namespace);
        object obj = a.GetValue(graph);
        if (obj == null) continue;

        AfxTypeSerializer ts = AfxTypeSerializer.GetTypeSerializer(obj.GetType());
        ts.ParseNamespaces(obj, context);
      }
    }

    #endregion

    #region void Write(...)

    public void Write(XmlDictionaryWriter writer, object graph, SerializationContext context)
    {
      try
      {
        BusinessObject bo = graph as BusinessObject;
        if (bo != null)
        {
          if (context.ContainsObject(bo.Id))
          {
            writer.WriteAttributeString(Constants.SerializationPrefix, Constants.ReferenceAttribute, Constants.SerializationNamespace, bo.Id.ToString());
            return;
          }

          context.AddObject(bo);
        }

        foreach (var a in mSimpleMembers)
        {
          string val = a.GetValue(graph);
          if (val == null) continue;
          writer.WriteAttributeString(context.GetPrefix(a.Namespace), a.Name, a.Namespace, val);
        }

        foreach (var a in mComplexMembers)
        {
          object obj = a.GetValue(graph);
          if (obj == null) continue;

          WriteNode(writer, obj, a.MemberType, a.Namespace, a.Name, context);
        }

        IBusinessObjectCollection boc = graph as IBusinessObjectCollection;
        if (boc != null)
        {
          string ns = KnownTypesProvider.GetTypeUri(boc.GetType()).ToString();
          foreach (var item in boc)
          {
            WriteNode(writer, item, boc.ItemType, ns, Constants.ItemElement, context);
          }
          foreach (var item in boc.DeletedItems)
          {
            WriteNode(writer, item, typeof(Guid), ns, Constants.DeletedItemElement, context);
          }
        }
      }
      catch
      {
        throw;
      }
    }

    #endregion

    #region void Read(...)

    public void Read(XmlDictionaryReader reader, object graph, SerializationContext context)
    {
      BusinessObject bo = graph as BusinessObject;

      IBusinessObjectCollection boc = graph as IBusinessObjectCollection;
      IEventStateAware esa = graph as IEventStateAware;
      if (esa != null) esa.SuppressEventState = true;

      foreach (var a in mSimpleMembers)
      {
        string val = reader.GetAttribute(a.Name, a.Namespace);
        if (val != null) a.SetValue(graph, val);

        if (bo != null && a.IsIdentifier)
        {
          context.AddObject(bo);
        }
      }

      string ns = reader.NamespaceURI;
      while (reader.Read())
      {
        if (reader.NodeType == XmlNodeType.EndElement)
        {
          if (reader.NamespaceURI == ns) break;
          continue;
        }

        var a = mComplexMembers.FirstOrDefault(a1 => a1.Namespace == reader.NamespaceURI && a1.Name == reader.LocalName);
        if (a != null)
        {
          object obj = ReadNode(reader, a.MemberType, context);
          a.SetValue(graph, obj);
        }
        else if (boc != null)
        {
          if (reader.LocalName == Constants.ItemElement)
          {
            object obj = ReadNode(reader, boc.ItemType, context);
            boc.Add(obj);
          }

          if (reader.LocalName == Constants.DeletedItemElement)
          {
            Guid id = (Guid)ReadNode(reader, typeof(Guid), context);
            boc.AddDeletedItem(id);
          }
        }
      }

      if (esa != null) esa.SuppressEventState = false;
    }

    #endregion

    #region AfxTypeSerializer GetTypeSerializer(...)

    static object mLock = new object();
    public static AfxTypeSerializer GetTypeSerializer(Type objectType)
    {
      lock (mLock)
      {
        if (mTypeSerializerDictionary.ContainsKey(objectType)) return mTypeSerializerDictionary[objectType];
        AfxTypeSerializer ts = new AfxTypeSerializer(objectType);
        mTypeSerializerDictionary.Add(objectType, ts);
        return ts;
      }
    }

    #endregion


    #region void WriteNode(...)

    void WriteNode(XmlDictionaryWriter writer, object graph, Type type, string ns, string name, SerializationContext context)
    {
      IBusinessObjectCollection boc = graph as IBusinessObjectCollection;
      if (boc == null || (boc != null && boc.Count > 0))
      {
        writer.WriteStartElement(context.GetPrefix(ns), name, ns);
        Type trueType = graph.GetType();
        if (trueType != type && !context.ContainsObject(((BusinessObject)graph).Id))
        {
          writer.WriteAttributeString(Constants.SerializationPrefix, Constants.TypeAttribute, Constants.SerializationNamespace, KnownTypesProvider.GetTypeUri(trueType).ToString());
        }
        if (trueType.IsValueType || trueType == typeof(string))
        {
          writer.WriteValue(graph);
        }
        else
        {
          AfxTypeSerializer ts = AfxTypeSerializer.GetTypeSerializer(trueType);
          ts.Write(writer, graph, context);
        }

        writer.WriteEndElement();
      }
    }

    #endregion

    #region void ReadNode(...)

    object ReadNode(XmlDictionaryReader reader, Type type, SerializationContext context)
    {
      if (type.IsValueType || type == typeof(string))
      {
        reader.Read();
        return SimpleValueConverter.ConvertFromString(type, reader.Value);
      }

      object obj = null;
      string id = reader.GetAttribute(Constants.ReferenceAttribute, Constants.SerializationNamespace);
      if (id != null)
      {
        obj = context.GetObject(Guid.Parse(id));
      }
      else
      {
        string tn = reader.GetAttribute(Constants.TypeAttribute, Constants.SerializationNamespace);
        if (tn != null) type = KnownTypesProvider.GetKnownType(new Uri(tn));
        obj = Activator.CreateInstance(type);
        AfxTypeSerializer ts = AfxTypeSerializer.GetTypeSerializer(type);
        ts.Read(reader, obj, context);

        IAfxSerializationAware sa = obj as IAfxSerializationAware;
        if (sa != null) sa.OnDeserialized();
      }

      return obj;
    }

    #endregion

    #region BuildInheritanceList(...)

    void BuildInheritanceList(Type objectType)
    {
      if (objectType == typeof(object)) return;
      BuildInheritanceList(objectType.BaseType);
      mInheritanceList.Add(objectType);
    }

    #endregion

    #region void Initialize(...)

    void Initialize()
    {
      foreach (Type type in mInheritanceList)
      {
        AfxTypeSerializer ts = null;
        if (type != ObjectType)
        {
          ts = GetTypeSerializer(type);
        }
        else
        {
          ts = this;
        }

        foreach (var i in type.GetFields(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.DeclaredOnly).Where(i1 => i1.IsDefined(typeof(DataMemberAttribute))))
        {
          DataMemberAttribute dma = i.GetCustomAttribute<DataMemberAttribute>();

          if (i.FieldType.IsValueType || i.FieldType == typeof(string))
          {
            mSimpleMembers.Add(new SimpleFieldAccessor(ts.Uri.ToString(), dma.Name ?? i.Name, i, dma.IsIdentifier));
          }
          else
          {
            mComplexMembers.Add(new ComplexFieldAccessor(ts.Uri.ToString(), dma.Name ?? i.Name, i));
          }
        }

        foreach (var i in type.GetProperties(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.DeclaredOnly).Where(i1 => i1.IsDefined(typeof(DataMemberAttribute))))
        {
          DataMemberAttribute dma = i.GetCustomAttribute<DataMemberAttribute>();

          if (i.PropertyType.IsValueType || i.PropertyType == typeof(string))
          {
            mSimpleMembers.Add(new SimplePropertyAccessor(ts.Uri.ToString(), dma.Name ?? i.Name, i, dma.IsIdentifier));
          }
          else
          {
            mComplexMembers.Add(new ComplexPropertyAccessor(ts.Uri.ToString(), dma.Name ?? i.Name, i));
          }
        }
      }
    }

    #endregion
  }
}

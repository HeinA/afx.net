﻿using Afx.ComponentModel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Afx.Runtime.Serialization
{
  class SimplePropertyAccessor : SimpleMemberAccessor
  {
    public SimplePropertyAccessor(string memberNamespace, string name, PropertyInfo propertyInfo)
      : base(memberNamespace, name, propertyInfo.PropertyType)
    {
      PropertyInfo = propertyInfo;
    }

    public SimplePropertyAccessor(string memberNamespace, string name, PropertyInfo propertyInfo, bool isIdentifier)
      : base(memberNamespace, name, propertyInfo.PropertyType, isIdentifier)
    {
      PropertyInfo = propertyInfo;
    }

    #region PropertyInfo PropertyInfo

    public const string PropertyInfoProperty = "PropertyInfo";
    PropertyInfo mPropertyInfo;
    public PropertyInfo PropertyInfo
    {
      get { return mPropertyInfo; }
      set { mPropertyInfo = value; }
    }

    #endregion

    public override void SetValue(object target, string value)
    {
      PropertyInfo.SetValue(target, ConvertFromString(PropertyInfo.PropertyType, value));
    }

    public override string GetValue(object source)
    {
      object val = PropertyInfo.GetValue(source);
      if (val == Default) return null;
      return ConvertToString(PropertyInfo.PropertyType, val);
    }
  }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Runtime.Serialization
{
  [AttributeUsage(AttributeTargets.Field | AttributeTargets.Parameter)]
  public class DataMemberAttribute : Attribute
  {
    #region string Name

    string mName;
    public string Name
    {
      get { return mName; }
      set { mName = value; }
    }

    #endregion

    #region bool IsReference

    bool mIsReference;
    public bool IsReference
    {
      get { return mIsReference; }
      set { mIsReference = value; }
    }

    #endregion
  }
}

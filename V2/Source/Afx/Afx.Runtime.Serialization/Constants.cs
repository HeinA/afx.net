﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Runtime.Serialization
{
  static class Constants
  {
    public const string SerializationNamespace = "http://openafx.net/Serialization";
    public const string SerializationPrefix = "s";

    public const string TypeAttribute = "type";
    public const string ReferenceAttribute = "ref";

    public const string ItemElement = "Item";
    public const string DeletedItemElement = "DeletedItem";
  }
}

﻿using Afx.ComponentModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Runtime.Serialization
{
  public class SerializationContext
  {
    Dictionary<Guid, BusinessObject> mObjectDictionary = new Dictionary<Guid, BusinessObject>();

    public bool ContainsObject(Guid id)
    {
      return mObjectDictionary.ContainsKey(id);
    }

    public BusinessObject GetObject(Guid id)
    {
      return mObjectDictionary[id];
    }

    public void AddObject(BusinessObject bo)
    {
      mObjectDictionary.Add(bo.Id, bo);
    }

    public void ClearObjects()
    {
      mObjectDictionary.Clear();
    }

    Collection<string> mNamespaces = new Collection<string>();
    public IEnumerable<string> Namespaces
    {
      get { return mNamespaces; }
    }

    int i = 1;
    Dictionary<string, string> mPrefixDictionary = new Dictionary<string, string>();

    public string GetPrefix(string ns)
    {
      if (mPrefixDictionary.ContainsKey(ns)) return mPrefixDictionary[ns];
      string p = string.Format("ns{0}", i++);
      mPrefixDictionary.Add(ns, p);
      mNamespaces.Add(ns);
      return p;
    }
  }
}

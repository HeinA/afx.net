﻿using Afx.ComponentModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Runtime.Serialization
{
  class ComplexFieldAccessor : ComplexMemberAccessor
  {
    public ComplexFieldAccessor(string memberNamespace, string name, FieldInfo fieldInfo)
      : base(memberNamespace, name)
    {
      FieldInfo = fieldInfo;
    }

    #region FieldInfo FieldInfo

    FieldInfo mFieldInfo;
    protected FieldInfo FieldInfo
    {
      get { return mFieldInfo; }
      private set { mFieldInfo = value; }
    }

    #endregion

    public override void SetValue(object target, object value)
    {
      FieldInfo.SetValue(target, value);
      //target.SetFieldValue(FieldInfo.Name, value, Flags.AnyVisibility | Flags.Instance);
    }

    public override object GetValue(object source)
    {
      //return source.GetFieldValue(FieldInfo.Name, Flags.AnyVisibility | Flags.Instance);
      return FieldInfo.GetValue(source);
    }

    public override Type MemberType
    {
      get { return FieldInfo.FieldType; }
    }
  }
}

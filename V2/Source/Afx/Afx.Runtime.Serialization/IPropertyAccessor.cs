﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Runtime.Serialization
{
  public interface IPropertyAccessor
  {
    object Get(object target);
    void Set(object target, object value);
  }
}

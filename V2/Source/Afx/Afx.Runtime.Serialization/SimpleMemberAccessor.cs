﻿using Afx.ComponentModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Afx.Runtime.Serialization
{
  abstract class SimpleMemberAccessor : MemberAccessor
  {
    protected SimpleMemberAccessor(string memberNamespace, string name, Type type)
      : base(memberNamespace, name)
    {
      if (type.IsValueType)
      {
        Default = Activator.CreateInstance(type);
      }
    }

    protected SimpleMemberAccessor(string memberNamespace, string name, Type type, bool isIdentifier)
      : base(memberNamespace, name)
    {
      IsIdentifier = isIdentifier;
      if (type.IsValueType)
      {
        Default = Activator.CreateInstance(type);
      }
    }

    #region object Default

    public const string DefaultProperty = "Default";
    object mDefault;
    protected object Default
    {
      get { return mDefault; }
      private set { mDefault = value; }
    }

    #endregion

    #region bool IsIdentifier

    bool mIsIdentifier;
    public bool IsIdentifier
    {
      get { return mIsIdentifier; }
      private set { mIsIdentifier = value; }
    }

    #endregion

    public abstract void SetValue(object target, string value);
    public abstract string GetValue(object source);

    protected string ConvertToString(Type valueType, object value)
    {
      return SimpleValueConverter.ConvertToString(valueType, value);
    }

    protected object ConvertFromString(Type valueType, string value)
    {
      return SimpleValueConverter.ConvertFromString(valueType, value);
    }
  }
}

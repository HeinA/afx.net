﻿using Afx.ComponentModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Runtime.Serialization
{
  class SimpleFieldAccessor : SimpleMemberAccessor
  {
    public SimpleFieldAccessor(string memberNamespace, string name, FieldInfo fieldInfo)
      : base(memberNamespace, name, fieldInfo.FieldType)
    {
      FieldInfo = fieldInfo;
    }

    public SimpleFieldAccessor(string memberNamespace, string name, FieldInfo fieldInfo, bool isIdentifier)
      : base(memberNamespace, name, fieldInfo.FieldType, isIdentifier)
    {
      FieldInfo = fieldInfo;
    }
    
    #region FieldInfo FieldInfo

    FieldInfo mFieldInfo;
    protected FieldInfo FieldInfo
    {
      get { return mFieldInfo; }
      private set { mFieldInfo = value; }
    }

    #endregion

    public override void SetValue(object target, string value)
    {
      //object o = ConvertFromString(FieldInfo.FieldType, value);
      //target.SetFieldValue(FieldInfo.Name, o, Flags.AnyVisibility | Flags.Instance);
      FieldInfo.SetValue(target, ConvertFromString(FieldInfo.FieldType, value));
    }

    public override string GetValue(object source)
    {
      //object val = source.GetFieldValue(FieldInfo.Name, Flags.AnyVisibility | Flags.Instance);
      object val = FieldInfo.GetValue(source);
      if (val == null) return null;
      if (val.Equals(Default)) return null;
      return ConvertToString(FieldInfo.FieldType, val);
    }

    //public override object GetObjectValue(object source)
    //{
    //  return FieldInfo.GetValue(source);
    //}
  }
}

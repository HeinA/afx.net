﻿using Afx.ComponentModel;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Afx.Runtime.Serialization
{
  public class AfxSerializer : System.Runtime.Serialization.XmlObjectSerializer
  {
    public AfxSerializer(Type type, string elementName)
    {
      ElementName = elementName;
      Type = type;
    }

    #region Type Type

    public const string TypeProperty = "Type";
    Type mType;
    public Type Type
    {
      get { return mType; }
      set { mType = value; }
    }

    #endregion

    #region string ElementName

    string mElementName;
    public string ElementName
    {
      get { return mElementName; }
      set { mElementName = value; }
    }

    #endregion


    public override bool IsStartObject(XmlDictionaryReader reader)
    {
      return true;
    }

    public override object ReadObject(XmlDictionaryReader reader, bool verifyObjectName)
    {
      try
      {
        Stopwatch sw = new Stopwatch();
        sw.Start();

        if (Type.IsValueType || Type == typeof(string))
        {
          reader.Read();
          object o = SimpleValueConverter.ConvertFromString(Type, reader.Value);
          reader.Read();
          reader.Read();
          return o;
        }
        else
        {
          Type = KnownTypesProvider.GetKnownType(new Uri(reader.GetAttribute(Constants.TypeAttribute, Constants.SerializationNamespace)));
          object o = Activator.CreateInstance(Type);
          SerializationContext context = new SerializationContext();
          AfxTypeSerializer ts = AfxTypeSerializer.GetTypeSerializer(Type);
          ts.Read(reader, o, context);
          reader.Read();

          IAfxSerializationAware sa = o as IAfxSerializationAware;
          if (sa != null) sa.OnDeserialized();

          sw.Stop();
          Debug.WriteLine("Deserialization {0}ms", sw.ElapsedMilliseconds);

          return o;
        }
      }
      catch
      {
        throw;
      }
    }

    public override void WriteEndObject(XmlDictionaryWriter writer)
    {
      try
      {
        writer.WriteEndElement();
      }
      catch
      {
        throw;
      }
    }

    public override void WriteObjectContent(XmlDictionaryWriter writer, object graph)
    {
      try
      {
        Stopwatch sw = new Stopwatch();
        sw.Start();

        Type type = graph.GetType();
        if (type.IsValueType || type == typeof(string))
        {
          writer.WriteValue(SimpleValueConverter.ConvertToString(type, graph));
        }
        else
        {
          if (type != Type)
          {
            writer.WriteAttributeString(Constants.SerializationPrefix, Constants.TypeAttribute, Constants.SerializationNamespace, KnownTypesProvider.GetTypeUri(type).ToString());
          }

          SerializationContext context = new SerializationContext();
          AfxTypeSerializer ts = AfxTypeSerializer.GetTypeSerializer(type);
          ts.ParseNamespaces(graph, context);
          foreach (var ns in context.Namespaces)
          {
            writer.WriteAttributeString("xmlns", context.GetPrefix(ns), null, ns);
          }
          context.ClearObjects();
          ts.Write(writer, graph, context);

          sw.Stop();
          Debug.WriteLine("Serialization {0}ms", sw.ElapsedMilliseconds);
        }
      }
      catch
      {
        throw;
      }
    }

    public override void WriteStartObject(XmlDictionaryWriter writer, object graph)
    {
      try
      {
        writer.WriteStartElement(ElementName);
      }
      catch
      {
        throw;
      }
    }
  }
}

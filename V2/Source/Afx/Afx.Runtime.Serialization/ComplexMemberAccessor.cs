﻿using Afx.ComponentModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Runtime.Serialization
{
  abstract class ComplexMemberAccessor : MemberAccessor
  {
    protected ComplexMemberAccessor(string memberNamespace, string name)
      : base(memberNamespace, name)
    {
    }
    public abstract Type MemberType { get; }
    public abstract void SetValue(object target, object value);
    public abstract object GetValue(object source);
  }
}

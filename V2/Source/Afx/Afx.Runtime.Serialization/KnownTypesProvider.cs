﻿using Afx.ComponentModel;
using Afx.ComponentModel.Collections;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Afx.Runtime.Serialization
{
  public class KnownTypesProvider
  {
    static List<Type> mKnownTypes = new List<Type>();
    static List<Type> mKnownGenerics = new List<Type>();
    static List<Type> mKnownFaults = new List<Type>();

    static KnownTypesProvider()
    {
      PreLoadDeployedAssemblies();

      foreach (var assembly in AppDomain.CurrentDomain.GetAssemblies())
      {
        foreach (var t in assembly.DefinedTypes.Where(t => typeof(BusinessObject).IsAssignableFrom(t) && !t.IsGenericTypeDefinition))
        {
          mKnownTypes.Add(t);
        }

        foreach (var t in assembly.DefinedTypes.Where(t => typeof(BusinessCollection<>).IsAssignableFrom(t)))
        {
          mKnownGenerics.Add(t);
        }

        foreach (var t in assembly.DefinedTypes.Where(t => t.IsDefined(typeof(FaultContractAttribute)) && !t.IsGenericTypeDefinition))
        {
          mKnownFaults.Add(t);
        }
      }
    }

    public static IEnumerable<Type> KnownTypes
    {
      get { return mKnownTypes; }
    }

    public static IEnumerable<Type> KnownFaults
    {
      get { return mKnownFaults; }
    }

    public static Uri GetTypeUri(Type knownType)
    {
      if (knownType.IsGenericType)
      {
        Collection<string> gtd = new Collection<string>();
        foreach (var t in knownType.GetGenericArguments())
        {
          gtd.Add(string.Format("{0}/{1}", t.Namespace, t.Name));
        }
        string s = string.Format("type://{0}/{1}#{2}", knownType.Namespace, knownType.Name.Substring(0, knownType.Name.IndexOf('`')), string.Join("+", gtd));
        Uri uri = new Uri(s);
        return uri;
      }
      else
      {
        Uri uri = new Uri(string.Format("type://{0}/{1}", knownType.Namespace, knownType.Name));
        return uri;
      }
    }

    public static Type GetKnownType(string ns, string elementName)
    {
      return GetKnownType(new Uri(new Uri(ns), elementName));
    }

    static Type[] GetKnownTypes(string fragment)
    {
      if (fragment.Substring(0, 1) == "#")
      {
        Collection<Type> types = new Collection<Type>();
        foreach (var s in fragment.Replace("#", "type://").Split('+'))
        {
          types.Add(GetKnownType(new Uri(s)));
        }
        return types.ToArray();
      }

      throw new InvalidOperationException(); //TODO:
    }

    public static Type GetKnownType(Uri uri)
    {
      string ns = null;
      string name = null;
      Type[] types = null;
      try
      {
        ns = uri.DnsSafeHost;
        name = uri.Segments[1];
        if (!string.IsNullOrWhiteSpace(uri.Fragment)) types = GetKnownTypes(uri.Fragment);
      }
      catch
      {
        throw new ArgumentException("uri");
      }

      if (types == null)
      {
        Type t = mKnownTypes.FirstOrDefault(t1 => t1.Namespace.ToLowerInvariant().Equals(ns) && t1.Name.Equals(name));
        if (t == null) throw new InvalidOperationException(); //TODO:
        return t;
      }
      else
      {
        Type t = mKnownGenerics.FirstOrDefault(t1 => t1.Namespace.ToLowerInvariant().Equals(ns) && t1.Name.Substring(0, name.Length).Equals(name));
        if (t == null) throw new InvalidOperationException(); //TODO:
        return t.MakeGenericType(types);
      }
    }

    #region void PreLoadDeployedAssemblies()

    static bool mAssembliesLoaded = false;
    public static void PreLoadDeployedAssemblies()
    {
      if (mAssembliesLoaded) return;
      foreach (var path in GetBinFolders())
      {
        PreLoadAssembliesFromPath(path);
      }
      mAssembliesLoaded = true;
    }

    private static IEnumerable<string> GetBinFolders()
    {
      List<string> toReturn = new List<string>();
      if (HttpContext.Current != null)
      {
        toReturn.Add(HttpRuntime.BinDirectory);
      }
      else
      {
        toReturn.Add(AppDomain.CurrentDomain.BaseDirectory);
      }

      return toReturn;
    }

    private static void PreLoadAssembliesFromPath(string p)
    {
      //S.O. NOTE: ELIDED - ALL EXCEPTION HANDLING FOR BREVITY

      //get all .dll files from the specified path and load the lot
      FileInfo[] files = null;
      //you might not want recursion - handy for localised assemblies 
      //though especially.
      files = new DirectoryInfo(p).GetFiles("*.dll",
          SearchOption.AllDirectories);

      AssemblyName a = null;
      string s = null;
      foreach (var fi in files)
      {
        s = fi.FullName;
        //now get the name of the assembly you've found, without loading it
        //though (assuming .Net 2+ of course).
        a = AssemblyName.GetAssemblyName(s);
        //sanity check - make sure we don't already have an assembly loaded
        //that, if this assembly name was passed to the loaded, would actually
        //be resolved as that assembly.  Might be unnecessary - but makes me
        //happy :)
        if (!AppDomain.CurrentDomain.GetAssemblies().Any(assembly =>
          AssemblyName.ReferenceMatchesDefinition(a, assembly.GetName())))
        {
          //crucial - USE THE ASSEMBLY NAME.
          //in a web app, this assembly will automatically be bound from the 
          //Asp.Net Temporary folder from where the site actually runs.
          Assembly.Load(a);
        }
      }
    }

    #endregion
  }
}

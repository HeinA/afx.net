﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Reflection;

namespace Afx.Runtime.Serialization
{
  internal static class SimpleValueConverter
  {
    public static string ConvertToString(Type valueType, object value)
    {
      #region Standard Primitives

      if (valueType == typeof(bool))
      {
        return XmlConvert.ToString((bool)value);
      }
      else if (valueType == typeof(byte))
      {
        return XmlConvert.ToString((byte)value);
      }
      else if (valueType == typeof(char))
      {
        return XmlConvert.ToString((char)value);
      }
      else if (valueType == typeof(DateTime))
      {
        return XmlConvert.ToString((DateTime)value, XmlDateTimeSerializationMode.Utc);
      }
      else if (valueType == typeof(DateTimeOffset))
      {
        return XmlConvert.ToString((DateTimeOffset)value);
      }
      else if (valueType == typeof(decimal))
      {
        return XmlConvert.ToString((decimal)value);
      }
      else if (valueType == typeof(double))
      {
        return XmlConvert.ToString((double)value);
      }
      else if (valueType == typeof(float))
      {
        return XmlConvert.ToString((float)value);
      }
      else if (valueType == typeof(Guid))
      {
        return XmlConvert.ToString((Guid)value);
      }
      else if (valueType == typeof(int))
      {
        return XmlConvert.ToString((int)value);
      }
      else if (valueType == typeof(long))
      {
        return XmlConvert.ToString((long)value);
      }
      else if (valueType == typeof(sbyte))
      {
        return XmlConvert.ToString((sbyte)value);
      }
      else if (valueType == typeof(short))
      {
        return XmlConvert.ToString((short)value);
      }
      else if (valueType == typeof(string))
      {
        return (string)value;
      }
      else if (valueType == typeof(TimeSpan))
      {
        return XmlConvert.ToString((TimeSpan)value);
      }
      else if (valueType == typeof(uint))
      {
        return XmlConvert.ToString((uint)value);
      }
      else if (valueType == typeof(ulong))
      {
        return XmlConvert.ToString((ulong)value);
      }
      else if (valueType == typeof(ushort))
      {
        return XmlConvert.ToString((ushort)value);
      }

      #endregion

      #region Nullable Primitives

      if (valueType == typeof(bool?))
      {
        object o = value;
        if (o != null) return XmlConvert.ToString((bool)o);
      }
      else if (valueType == typeof(byte?))
      {
        object o = value;
        if (o != null) return XmlConvert.ToString((byte)o);
      }
      else if (valueType == typeof(char?))
      {
        object o = value;
        if (o != null) return XmlConvert.ToString((char)o);
      }
      else if (valueType == typeof(DateTime?))
      {
        object o = value;
        if (o != null) return XmlConvert.ToString((DateTime)o, XmlDateTimeSerializationMode.Utc);
      }
      else if (valueType == typeof(DateTimeOffset?))
      {
        object o = value;
        if (o != null) return XmlConvert.ToString((DateTimeOffset)o);
      }
      else if (valueType == typeof(decimal?))
      {
        object o = value;
        if (o != null) return XmlConvert.ToString((decimal)o);
      }
      else if (valueType == typeof(double?))
      {
        object o = value;
        if (o != null) return XmlConvert.ToString((double)o);
      }
      else if (valueType == typeof(float?))
      {
        object o = value;
        if (o != null) return XmlConvert.ToString((float)o);
      }
      else if (valueType == typeof(Guid?))
      {
        object o = value;
        if (o != null) return XmlConvert.ToString((Guid)o);
      }
      else if (valueType == typeof(int?))
      {
        object o = value;
        if (o != null) return XmlConvert.ToString((int)o);
      }
      else if (valueType == typeof(long?))
      {
        object o = value;
        if (o != null) return XmlConvert.ToString((long)o);
      }
      else if (valueType == typeof(sbyte?))
      {
        object o = value;
        if (o != null) return XmlConvert.ToString((sbyte)o);
      }
      else if (valueType == typeof(short?))
      {
        object o = value;
        if (o != null) return XmlConvert.ToString((short)o);
      }
      else if (valueType == typeof(TimeSpan?))
      {
        object o = value;
        if (o != null) return XmlConvert.ToString((TimeSpan)o);
      }
      else if (valueType == typeof(uint?))
      {
        object o = value;
        if (o != null) return XmlConvert.ToString((uint)o);
      }
      else if (valueType == typeof(ulong?))
      {
        object o = value;
        if (o != null) return XmlConvert.ToString((ulong)o);
      }
      else if (valueType == typeof(ushort?))
      {
        object o = value;
        if (o != null) return XmlConvert.ToString((ushort)o);
      }

      #endregion

      #region Enum

      if (typeof(Enum).GetTypeInfo().IsAssignableFrom(valueType.GetTypeInfo()))
      {
        return Enum.GetName(valueType, value);
      }

      #endregion

      throw new InvalidOperationException("Could not convert to string"); //TODO: internationalization
    }

    public static object ConvertFromString(Type valueType, string value)
    {
      #region Standard Primitives

      if (valueType == typeof(bool))
      {
        return bool.Parse(value);
      }
      else if (valueType == typeof(byte))
      {
        return byte.Parse(value);
      }
      else if (valueType == typeof(char))
      {
        char c = (char)0;
        if (char.TryParse(value, out c))
        {
          return c;
        }
      }
      else if (valueType == typeof(DateTime))
      {
        return DateTime.Parse(value);
      }
      else if (valueType == typeof(DateTimeOffset))
      {
        return DateTimeOffset.Parse(value);
      }
      else if (valueType == typeof(decimal))
      {
        return decimal.Parse(value);
      }
      else if (valueType == typeof(double))
      {
        return double.Parse(value);
      }
      else if (valueType == typeof(float))
      {
        return float.Parse(value);
      }
      else if (valueType == typeof(Guid))
      {
        return Guid.Parse(value);
      }
      else if (valueType == typeof(int))
      {
        return int.Parse(value);
      }
      else if (valueType == typeof(long))
      {
        return long.Parse(value);
      }
      else if (valueType == typeof(sbyte))
      {
        return sbyte.Parse(value);
      }
      else if (valueType == typeof(short))
      {
        return short.Parse(value);
      }
      else if (valueType == typeof(string))
      {
        return value;
      }
      else if (valueType == typeof(TimeSpan))
      {
        return TimeSpan.Parse(value);
      }
      else if (valueType == typeof(uint))
      {
        return uint.Parse(value);
      }
      else if (valueType == typeof(ulong))
      {
        return ulong.Parse(value);
      }
      else if (valueType == typeof(ushort))
      {
        return ushort.Parse(value);
      }

      #endregion

      #region Nullable Primitives

      if (valueType == typeof(bool?))
      {
        return bool.Parse(value);
      }
      else if (valueType == typeof(byte?))
      {
        return byte.Parse(value);
      }
      else if (valueType == typeof(char?))
      {
        char c = (char)0;
        if (char.TryParse(value, out c))
        {
          return c;
        }
      }
      else if (valueType == typeof(DateTime?))
      {
        return DateTime.Parse(value);
      }
      else if (valueType == typeof(DateTimeOffset?))
      {
        return DateTimeOffset.Parse(value);
      }
      else if (valueType == typeof(decimal?))
      {
        return decimal.Parse(value);
      }
      else if (valueType == typeof(double?))
      {
        return double.Parse(value);
      }
      else if (valueType == typeof(float?))
      {
        return float.Parse(value);
      }
      else if (valueType == typeof(Guid?))
      {
        return Guid.Parse(value);
      }
      else if (valueType == typeof(int?))
      {
        return int.Parse(value);
      }
      else if (valueType == typeof(long?))
      {
        return long.Parse(value);
      }
      else if (valueType == typeof(sbyte?))
      {
        return sbyte.Parse(value);
      }
      else if (valueType == typeof(short?))
      {
        return short.Parse(value);
      }
      else if (valueType == typeof(TimeSpan?))
      {
        return TimeSpan.Parse(value);
      }
      else if (valueType == typeof(uint?))
      {
        return uint.Parse(value);
      }
      else if (valueType == typeof(ulong?))
      {
        return ulong.Parse(value);
      }
      else if (valueType == typeof(ushort?))
      {
        return ushort.Parse(value);
      }

      #endregion

      #region Enum

      if (typeof(Enum).GetTypeInfo().IsAssignableFrom(valueType.GetTypeInfo()))
      {
        return Enum.Parse(valueType, value);
      }

      #endregion

      throw new InvalidOperationException("Could not convert from string"); //TODO: internationalization
    }
  }
}

﻿using Afx.ComponentModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Afx.Runtime.Serialization
{
  internal static class KnownTypesProvider
  {
    class TypeNamespace
    {
      #region TypeInfo TypeInfo

      TypeInfo mTypeInfo;
      public TypeInfo TypeInfo
      {
        get { return mTypeInfo; }
        set { mTypeInfo = value; }
      }

      #endregion

      #region string Namespace

      string mNamespace;
      public string Namespace
      {
        get { return mNamespace; }
        set { mNamespace = value; }
      }

      #endregion

      #region string Prefix
      
      public const string PrefixProperty = "Prefix";
      string mPrefix;
      public string Prefix
      {
        get { return mPrefix; }
        set { mPrefix = value; }
      }
      
      #endregion

      #region Uri Uri

      public const string UriProperty = "Uri";
      Uri mUri;
      public Uri Uri
      {
        get { return mUri; }
        set { mUri = value; }
      }

      #endregion
    }

    static Dictionary<string, string> mNamespacePrefixDictionary = new Dictionary<string, string>();
    static List<TypeNamespace> mKnownTypes = new List<TypeNamespace>();

    static KnownTypesProvider()
    {
      PreLoadDeployedAssemblies();
      RegisterAssembly();
      foreach (var assembly in AppDomain.CurrentDomain.GetAssemblies())
      {
        RegisterAssembly(assembly);
      }
    }

    static void RegisterAssembly(Assembly assembly)
    {
      //foreach (var ti in typeof(Int32).Assembly.DefinedTypes.Where(t => t.IsValueType && t.IsDefined(typeof(DataContractAttribute))))
      //{
      //  DataContractAttribute dca = ti.GetCustomAttribute<DataContractAttribute>(false);
      //  mKnownTypes.Add(new TypeNamespace() { TypeInfo = ti, Namespace = dca.Namespace, Prefix = string.Format("ns{0}", mKnownTypes.Count + 1), Uri = new Uri(new Uri(dca.Namespace), ti.Name) });
      //}

      foreach (var ti in assembly.DefinedTypes.Where(t => t.IsDefined(typeof(DataContractAttribute)) && typeof(BusinessObject).GetTypeInfo().IsAssignableFrom(t)))
      {
        DataContractAttribute dca = ti.GetCustomAttribute<DataContractAttribute>(false);
        mKnownTypes.Add(new TypeNamespace() { TypeInfo = ti, Namespace = dca.Namespace, Prefix = string.Format("ns{0}", mKnownTypes.Count + 1), Uri = new Uri(new Uri(dca.Namespace), ti.Name) });
      }
    }

    static void RegisterAssembly()
    {
      RegisterType<BusinessObject>();
    }

    static void RegisterType<T>()
    {
      var ti = typeof(T).GetTypeInfo();
      mKnownTypes.Add(new TypeNamespace() { TypeInfo = ti, Namespace = Afx.Constants.AssemblyNamespace, Prefix = string.Format("ns{0}", mKnownTypes.Count + 1), Uri = new Uri(new Uri(Afx.Constants.AssemblyNamespace), ti.Name) });
    }

    public static IEnumerable<TypeInfo> KnownTypes
    {
      get { return mKnownTypes.Select<TypeNamespace, TypeInfo>(tn => tn.TypeInfo); }
    }

    public static string GetNamespacePrefix(string ns)
    {
      return mKnownTypes.FirstOrDefault(tn => tn.Namespace.Equals(ns)).Prefix; 
    }

    public static string GetNamespace(string prefix)
    {
      return mKnownTypes.FirstOrDefault(tn => tn.Prefix.Equals(prefix)).Namespace;
    }

    public static TypeInfo GetType(Uri uri)
    {
      TypeNamespace tn1 = mKnownTypes.FirstOrDefault(tn => tn.Uri.Equals(uri));
      return tn1.TypeInfo;
    }

    public static Uri GetUri(TypeInfo typeInfo)
    {
      return mKnownTypes.FirstOrDefault(tn => tn.TypeInfo.Equals(typeInfo)).Uri;
    }

    public static bool IsNamespaceRegistered(string ns)
    {
      return mKnownTypes.FirstOrDefault(tn => tn.Namespace == ns) != null;
    }

    #region void PreLoadDeployedAssemblies()

    static bool mAssembliesLoaded = false;
    public static void PreLoadDeployedAssemblies()
    {
      if (mAssembliesLoaded) return;
      foreach (var path in GetBinFolders())
      {
        PreLoadAssembliesFromPath(path);
      }
      mAssembliesLoaded = true;
    }

    private static IEnumerable<string> GetBinFolders()
    {
      List<string> toReturn = new List<string>();
      if (HttpContext.Current != null)
      {
        toReturn.Add(HttpRuntime.BinDirectory);
      }
      else
      {
        toReturn.Add(AppDomain.CurrentDomain.BaseDirectory);
      }

      return toReturn;
    }

    private static void PreLoadAssembliesFromPath(string p)
    {
      //S.O. NOTE: ELIDED - ALL EXCEPTION HANDLING FOR BREVITY

      //get all .dll files from the specified path and load the lot
      FileInfo[] files = null;
      //you might not want recursion - handy for localised assemblies 
      //though especially.
      files = new DirectoryInfo(p).GetFiles("*.dll",
          SearchOption.AllDirectories);

      AssemblyName a = null;
      string s = null;
      foreach (var fi in files)
      {
        s = fi.FullName;
        //now get the name of the assembly you've found, without loading it
        //though (assuming .Net 2+ of course).
        a = AssemblyName.GetAssemblyName(s);
        //sanity check - make sure we don't already have an assembly loaded
        //that, if this assembly name was passed to the loaded, would actually
        //be resolved as that assembly.  Might be unnecessary - but makes me
        //happy :)
        if (!AppDomain.CurrentDomain.GetAssemblies().Any(assembly =>
          AssemblyName.ReferenceMatchesDefinition(a, assembly.GetName())))
        {
          //crucial - USE THE ASSEMBLY NAME.
          //in a web app, this assembly will automatically be bound from the 
          //Asp.Net Temporary folder from where the site actually runs.
          Assembly.Load(a);
        }
      }
    }

    #endregion
  }
}

﻿using Afx.ComponentModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Runtime.Serialization
{
  class SimplePropertyAccessor : SimpleMemberAccessor
  {
    public SimplePropertyAccessor(string memberNamespace, string name, PropertyInfo propertyInfo)
      : base(memberNamespace, name)
    {
      PropertyInfo = propertyInfo;
    }

    public SimplePropertyAccessor(string memberNamespace, string name, PropertyInfo propertyInfo, bool isIdentifier)
      : base(memberNamespace, name, isIdentifier)
    {
      PropertyInfo = propertyInfo;
    }

    #region PropertyInfo PropertyInfo

    public const string PropertyInfoProperty = "PropertyInfo";
    PropertyInfo mPropertyInfo;
    public PropertyInfo PropertyInfo
    {
      get { return mPropertyInfo; }
      set { mPropertyInfo = value; }
    }

    #endregion

    public override void SetValue(BusinessObject target, string value)
    {
      PropertyInfo.SetValue(target, ConvertFromString(PropertyInfo.PropertyType, value));
    }

    public override string GetValue(BusinessObject source)
    {
      return ConvertToString(PropertyInfo.PropertyType, PropertyInfo.GetValue(source));
    }

    public override object GetObjectValue(BusinessObject source)
    {
      return PropertyInfo.GetValue(source);
    }
  }
}

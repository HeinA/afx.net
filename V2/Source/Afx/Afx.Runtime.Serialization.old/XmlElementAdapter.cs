﻿using Afx.ComponentModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Reflection;
using System.Collections;

namespace Afx.Runtime.Serialization
{
  class XmlElementAdapter
  {
    List<BusinessObject> mObjects = new List<BusinessObject>();
    List<XmlAttributeAdapter> mAttributes = new List<XmlAttributeAdapter>();
    List<XmlElementAdapter> mElements = new List<XmlElementAdapter>();
    object mObject = null;

    public XmlElementAdapter(XmlConverter converter)
    {
      Converter = converter;
    }

    public XmlElementAdapter(XmlConverter converter, XmlElementAdapter parent)
      : this(converter)
    {
      Parent = parent;
    }

    //public XmlElementAdapter(XmlElementAdapter parent)
    //{
    //  Parent = parent;
    //}

    public XmlElementAdapter(object obj)
    {
      mObject = obj;
    }

    XmlElementAdapter(XmlConverter converter, XmlElementAdapter parent, string name, object obj)
      : this(converter, parent)
    {
      mObject = obj;
      Name = name;
    }

    IEnumerable<XmlAttributeAdapter> AttributeAdapters
    {
      get { return mAttributes; }
    }

    object Value
    {
      get { return mObject; }
    }

    void RegisterAttribute(string ns, string name, string value)
    {
      mAttributes.Add(new XmlAttributeAdapter(ns, name, value));
    }

    IEnumerable<XmlElementAdapter> ElementAdapters
    {
      get { return mElements; }
    }

    XmlElementAdapter RegisterElement(string ns, string name, object value)
    {
      XmlConverter c = XmlConverter.GetConverter(value.GetType().GetTypeInfo());
      XmlElementAdapter xe = new XmlElementAdapter(c, this, name, value);
      xe.Namespace = ns;
      mElements.Add(xe);
      return xe;
    }

    XmlElementAdapter RegisterElement(XmlConverter converter)
    {
      XmlElementAdapter xe = new XmlElementAdapter(converter, this);
      mElements.Add(xe);
      return xe;
    }

    XmlElementAdapter RegisterElement(object obj)
    {
      XmlElementAdapter xe = new XmlElementAdapter(obj);
      mElements.Add(xe);
      return xe;
    }

    void RegisterObject(BusinessObject obj)
    {
      if (Parent != null) Parent.RegisterObject(obj);
      if (GetObject(obj.Id) != null) return;
      mObjects.Add(obj);
    }

    BusinessObject GetObject(Guid id)
    {
      if (Parent != null) return Parent.GetObject(id);
      return mObjects.FirstOrDefault(o => o.Id.Equals(id));
    }

    #region string Namespace

    public const string NamespaceProperty = "Namespace";
    string mNamespace;
    public string Namespace
    {
      get { return mNamespace ?? Converter.Namespace; }
      private set { mNamespace = value; }
    }

    #endregion

    #region string Name

    public const string NameProperty = "Name";
    string mName;
    public string Name
    {
      get { return mName; }
      set { mName = value; }
    }

    #endregion

    #region XmlConverter Converter

    XmlConverter mConverter;
    public XmlConverter Converter
    {
      get { return mConverter; }
      set { mConverter = value; }
    }

    #endregion

    #region XmlElementAdapter Parent

    XmlElementAdapter mParent;
    public XmlElementAdapter Parent
    {
      get { return mParent; }
      private set { mParent = value; }
    }

    #endregion

    void ParseObject()
    {
      try
      {
        BusinessObject obj = mObject as BusinessObject;
        if (obj != null)
        {
          RegisterObject(obj);

          SimpleMemberAccessor ida = Converter.IdMember;
          RegisterAttribute(ida.Namespace, ida.Name, ida.GetValue(obj));

          foreach (var a in Converter.SimpleMembers)
          {
            object v1 = Activator.CreateInstance(obj.GetType());
            object v2 = a.GetObjectValue(obj);
            if (v2 != null && !v2.Equals(v1))
            {
              RegisterAttribute(a.Namespace, a.Name, a.GetValue(obj));
            }
          }

          foreach (var a in Converter.ComplexMembers)
          {
            RegisterElement(a.Namespace, a.Name, a.GetValue(obj));
          }

          return;
        }

        if (GetGenericSubClass(typeof(Afx.ComponentModel.Collections.Collection<>), mObject.GetType()) != null)
        {
          IList list = (IList)mObject;

          foreach (BusinessObject bo in list)
          {
            RegisterElement(Afx.Constants.AssemblyNamespace, "Item", bo);
          }

          return;
        }

        throw new InvalidOperationException("Unable to serialize type"); //TODO: int
      }
      catch (Exception ex)
      {
        throw ex;
      }
    }

    Type GetGenericSubClass(Type generic, Type toCheck)
    {
      while (toCheck != null && toCheck != typeof(object))
      {
        var cur = toCheck.IsGenericType ? toCheck.GetGenericTypeDefinition() : toCheck;
        if (generic == cur)
        {
          return toCheck;
        }
        toCheck = toCheck.BaseType;
      }
      return null;
    }

    void ParseXml(XmlDictionaryReader reader)
    {
      while (reader.MoveToNextAttribute())
      {
        if (KnownTypesProvider.IsNamespaceRegistered(reader.NamespaceURI))
        {
          RegisterAttribute(reader.NamespaceURI, reader.LocalName, reader.Value);
        }
      }

      reader.MoveToElement();
      string en = string.Format("{0}:{1}", reader.Prefix, reader.LocalName);
      if (reader.IsEmptyElement) return;
      while (reader.Read())
      {
        if (reader.NodeType == XmlNodeType.EndElement)
        {
          string en1 = string.Format("{0}:{1}", reader.Prefix, reader.LocalName);
          if (en == en1) break;
        }

        if (KnownTypesProvider.IsNamespaceRegistered(reader.NamespaceURI))
        {
          if (reader.NamespaceURI == Afx.Constants.AssemblyNamespace && reader.LocalName == Afx.Constants.ItemElement)
          {
            reader.MoveToFirstAttribute();

            if (reader.NamespaceURI == Afx.Constants.AssemblyNamespace && reader.LocalName == Afx.Constants.TypeAttribute)
            {
              Uri uri = new Uri(reader.Value);
              TypeInfo ti = KnownTypesProvider.GetType(uri);
              XmlConverter converter = XmlConverter.GetConverter(ti);

              XmlElementAdapter xe = RegisterElement(converter);
              xe.ReadXml(reader);
            }
            else if (reader.NamespaceURI == Afx.Constants.AssemblyNamespace && reader.LocalName == Afx.Constants.IdAttribute)
            {
              Guid id = Guid.Parse(reader.Value);
              BusinessObject bo = GetObject(id);
              if (bo == null)
              {
                //TODO: Load From Cache
              }
              XmlElementAdapter xe = RegisterElement(bo);
              reader.Read();
            }
            else
            {
              throw new InvalidOperationException("First attribute must be the object type or id"); //TODO: internationalization
            }
          }
          else
          {
            ComplexMemberAccessor a = Converter.GetComplexMember(reader.NamespaceURI, reader.LocalName);
            object o = Activator.CreateInstance(a.MemberType);
            BusinessObject bo = o as BusinessObject;
            if (bo != null) RegisterObject(bo);
            XmlElementAdapter xe = RegisterElement(reader.NamespaceURI, reader.LocalName, o);
            xe.ReadXml(reader);
          }
        }
      }
    }

    public void WriteXml(XmlDictionaryWriter writer, object obj)
    {
      mObject = obj;
      ParseObject();

      foreach (var a in AttributeAdapters)
      {
        writer.WriteAttributeString(KnownTypesProvider.GetNamespacePrefix(a.Namespace), a.Name, a.Namespace, a.Value);
      }

      foreach (var a in ElementAdapters)
      {
        writer.WriteStartElement(a.Name, a.Namespace);

        BusinessObject bo = a.Value as BusinessObject;
        if (bo != null)
        {
          if (GetObject(bo.Id) != null)
          {
            writer.WriteAttributeString(KnownTypesProvider.GetNamespacePrefix(Afx.Constants.AssemblyNamespace), Afx.Constants.IdAttribute, Afx.Constants.AssemblyNamespace, bo.Id.ToString());
            writer.WriteEndElement();
            return;
          }

          Uri uri = new Uri(new Uri(a.Converter.Namespace), a.Converter.TypeName);
          writer.WriteAttributeString(KnownTypesProvider.GetNamespacePrefix(Afx.Constants.AssemblyNamespace), "Type", Afx.Constants.AssemblyNamespace, uri.ToString());
        }

        a.WriteXml(writer, a.Value);
        writer.WriteEndElement();
      }
    }

    public object ReadXml(XmlDictionaryReader reader)
    {
      if (mObject == null) mObject = Activator.CreateInstance(mConverter.TypeInfo.AsType());
      BusinessObject obj = mObject as BusinessObject;
      if (obj != null)
      {
        RegisterObject(obj);
      }

      ParseXml(reader);

      if (obj != null)
      {
        if (Converter == null) 
          return obj;

        foreach (var a in ElementAdapters)
        {
          Converter.GetComplexMember(a.Namespace, a.Name).SetValue(obj, a.Value);
        }

        foreach (var a in AttributeAdapters)
        {
          Converter.GetSimpleMember(a.Namespace, a.Name).SetValue(obj, a.Value);
        }

        return mObject;
      }

      if (GetGenericSubClass(typeof(Afx.ComponentModel.Collections.Collection<>), mObject.GetType()) != null)
      {
        IList list = mObject as IList;

        foreach (XmlElementAdapter ea in ElementAdapters)
        {
          list.Add(ea.Value);
        }

        return mObject;
      }

      return mObject;
    }
  }
}

﻿using Afx.ComponentModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Afx.Runtime.Serialization
{
  abstract class SimpleMemberAccessor : MemberAccessor
  {
    protected SimpleMemberAccessor(string memberNamespace, string name)
      : base(memberNamespace, name)
    {
    }

    protected SimpleMemberAccessor(string memberNamespace, string name, bool isIdentifier)
      : base(memberNamespace, name)
    {
      IsIdentifier = isIdentifier;
    }

    #region bool IsIdentifier

    bool mIsIdentifier;
    public bool IsIdentifier
    {
      get { return mIsIdentifier; }
      private set { mIsIdentifier = value; }
    }

    #endregion

    public abstract void SetValue(BusinessObject target, string value);
    public abstract string GetValue(BusinessObject source);
    public abstract object GetObjectValue(BusinessObject source);

    protected string ConvertToString(Type valueType, object value)
    {
      return SimpleValueConverter.ConvertToString(valueType, value);
    }

    protected object ConvertFromString(Type valueType, string value)
    {
      return SimpleValueConverter.ConvertFromString(valueType, value);
    }
  }
}

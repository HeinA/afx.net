﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using Afx.ComponentModel;
using Afx.Utilities;
using System.Xml;

namespace Afx.Runtime.Serialization
{
   class AfxSerializer : XmlObjectSerializer
  {
    Type mType;
    string mName;
    XmlElementAdapter mXmlElementAdapter;

    public AfxSerializer(Type type, string name)
    {
      mType = type;
      mName = name;
    }

     public override bool IsStartObject(XmlDictionaryReader reader)
    {
      return true;
    }

    public override object ReadObject(XmlDictionaryReader reader, bool verifyObjectName)
    {
      if (IsSimpleType(mType))
      {
        object o = SimpleValueConverter.ConvertFromString(mType, reader.ReadString());
        reader.Read();
        return o;
      }
      else
      {
        reader.MoveToFirstAttribute();

        if (reader.NamespaceURI == Afx.Constants.AssemblyNamespace && reader.LocalName == Afx.Constants.TypeAttribute)
        {
          Uri uri = new Uri(reader.Value);
          TypeInfo ti = KnownTypesProvider.GetType(uri);
          XmlConverter converter = XmlConverter.GetConverter(ti);
          mType = ti.AsType();

          mXmlElementAdapter = new XmlElementAdapter(converter);
          BusinessObject bo = (BusinessObject)mXmlElementAdapter.ReadXml(reader);

          reader.MoveToElement();
          if (!reader.IsEmptyElement) reader.Read();

          return bo;
        }

        if (reader.NamespaceURI == Afx.Constants.AssemblyNamespace && reader.LocalName == Afx.Constants.IdAttribute)
        {
        }

        throw new InvalidOperationException("First attribute must be the object type or id"); //TODO: internationalization
      }
    }

    public override void WriteEndObject(XmlDictionaryWriter writer)
    {
      writer.WriteEndElement();
    }

    public override void WriteObjectContent(XmlDictionaryWriter writer, object graph)
    {
      if (IsSimpleType(mType))
      {
        writer.WriteValue(graph);
      }
      else
      {
        XmlConverter converter = XmlConverter.GetConverter(mType.GetTypeInfo());

        if (graph is BusinessObject)
        {
          Uri uri = new Uri(new Uri(converter.Namespace), converter.TypeName);
          writer.WriteAttributeString(KnownTypesProvider.GetNamespacePrefix(Afx.Constants.AssemblyNamespace), Afx.Constants.TypeAttribute, Afx.Constants.AssemblyNamespace, uri.ToString());
        }

        mXmlElementAdapter = new XmlElementAdapter(converter);
        mXmlElementAdapter.WriteXml(writer, (BusinessObject)graph);
      }
    }

    public override void WriteStartObject(XmlDictionaryWriter writer, object graph)
    {
      writer.WriteStartElement(mName);
    }

    bool IsSimpleType(Type objectType)
    {
      if (objectType.IsValueType || objectType.Equals(typeof(string)))
      {
        return true;
      }
      return false;
    }
  }
}

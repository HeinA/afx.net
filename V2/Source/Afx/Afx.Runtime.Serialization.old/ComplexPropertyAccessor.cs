﻿using Afx.ComponentModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

namespace Afx.Runtime.Serialization
{
  class ComplexPropertyAccessor : ComplexMemberAccessor
  {
    public ComplexPropertyAccessor(string memberNamespace, string name, PropertyInfo propertyInfo)
      : base(memberNamespace, name)
    {
      PropertyInfo = propertyInfo;
    }

    #region PropertyInfo PropertyInfo

    public const string PropertyInfoProperty = "PropertyInfo";
    PropertyInfo mPropertyInfo;
    public PropertyInfo PropertyInfo
    {
      get { return mPropertyInfo; }
      set { mPropertyInfo = value; }
    }

    #endregion

    public override void SetValue(BusinessObject target, object value)
    {
      PropertyInfo.SetValue(target, value);
    }

    public override object GetValue(BusinessObject source)
    {
      return PropertyInfo.GetValue(source);
    }

    public override Type MemberType
    {
      get { return PropertyInfo.PropertyType; }
    }
  }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Runtime.Serialization
{
  [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
  public class DataMemberAttribute : Attribute
  {
    public DataMemberAttribute()
    {
    }

    internal DataMemberAttribute(string name, bool isIdentifier)
    {
      Name = name;
      IsIdentifier = isIdentifier;
    }

    #region string Name

    string mName = null;
    public string Name
    {
      get { return mName; }
      set { mName = value; }
    }

    #endregion

    #region bool Reference

    bool mReference = false;
    public bool Reference
    {
      get { return mReference; }
      set { mReference = value; }
    }

    #endregion

    #region bool IsIdentifier

    bool mIsIdentifier;
    internal bool IsIdentifier
    {
      get { return mIsIdentifier; }
      private set { mIsIdentifier = value; }
    }

    #endregion  
  }
}

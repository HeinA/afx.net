﻿using Afx.ComponentModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Runtime.Serialization
{
  class SimpleFieldAccessor : SimpleMemberAccessor
  {
    public SimpleFieldAccessor(string memberNamespace, string name, FieldInfo fieldInfo)
      : base(memberNamespace, name)
    {
      FieldInfo = fieldInfo;
    }

    public SimpleFieldAccessor(string memberNamespace, string name, FieldInfo fieldInfo, bool isIdentifier)
      : base(memberNamespace, name, isIdentifier)
    {
      FieldInfo = fieldInfo;
    }

    #region FieldInfo FieldInfo

    FieldInfo mFieldInfo;
    protected FieldInfo FieldInfo
    {
      get { return mFieldInfo; }
      private set { mFieldInfo = value; }
    }

    #endregion

    public override void SetValue(BusinessObject target, string value)
    {
      FieldInfo.SetValue(target, ConvertFromString(FieldInfo.FieldType, value));
    }

    public override string GetValue(BusinessObject source)
    {
      return ConvertToString(FieldInfo.FieldType, FieldInfo.GetValue(source));
    }

    public override object GetObjectValue(BusinessObject source)
    {
      return FieldInfo.GetValue(source);
    }
  }
}

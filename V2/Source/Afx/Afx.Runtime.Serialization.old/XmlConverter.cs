﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using Afx.ComponentModel;
using System.Xml;

namespace Afx.Runtime.Serialization
{
  class XmlConverter
  {
    static Dictionary<TypeInfo, XmlConverter> mConverterDictionary = new Dictionary<TypeInfo, XmlConverter>();

    List<TypeInfo> mInheritanceList = new List<TypeInfo>();
    List<MemberAccessor> mMemberAccessorList = new List<MemberAccessor>();

    XmlConverter(TypeInfo objectTypeInfo)
    {
      TypeInfo = objectTypeInfo;
      mConverterDictionary.Add(TypeInfo, this);
      
      DataContractAttribute dca = TypeInfo.GetCustomAttribute<DataContractAttribute>(false);
      if (!objectTypeInfo.Assembly.Equals(typeof(BusinessObject).GetTypeInfo().Assembly) && dca == null) throw new InvalidOperationException("Type does not have DataContract attribute"); //TODO: internationalization
      if (dca != null)
      {
        Namespace = dca.Namespace;
      }

      BuildInheritanceList();
      BuildMemberList();
    }

    XmlConverter(TypeInfo objectTypeInfo, string ns)
      :this(objectTypeInfo)
    {
      Namespace = ns;
    }

    #region string Namespace

    public const string NamespaceProperty = "Namespace";
    string mNamespace;
    public string Namespace
    {
      get { return mNamespace; }
      private set { mNamespace = value; }
    }

    #endregion

    #region TypeInfo TypeInfo

    TypeInfo mTypeInfo;
    public TypeInfo TypeInfo
    {
      get { return mTypeInfo; }
      private set { mTypeInfo = value; }
    }

    #endregion

    #region string TypeName

    public string TypeName
    {
      get { return TypeInfo.Name; }
    }

    #endregion

    public SimpleMemberAccessor GetSimpleMember(string ns, string name)
    {
      return mMemberAccessorList.OfType<SimpleMemberAccessor>().FirstOrDefault(a => a.Namespace == ns && a.Name == name);
    }

    public ComplexMemberAccessor GetComplexMember(string ns, string name)
    {
      return mMemberAccessorList.OfType<ComplexMemberAccessor>().FirstOrDefault(a => a.Namespace == ns && a.Name == name);
    }

    public SimpleMemberAccessor IdMember
    {
      get { return mMemberAccessorList.OfType<SimpleMemberAccessor>().FirstOrDefault(a => a.IsIdentifier); }
    }

    public IEnumerable<SimpleMemberAccessor> SimpleMembers
    {
      get { return mMemberAccessorList.OfType<SimpleMemberAccessor>().Where(a => !a.IsIdentifier); }
    }

    public IEnumerable<ComplexMemberAccessor> ComplexMembers
    {
      get { return mMemberAccessorList.OfType<ComplexMemberAccessor>(); }
    }

    void BuildInheritanceList()
    {
      TypeInfo ti = TypeInfo;
      while (ti != typeof(System.Object).GetTypeInfo())
      {
        mInheritanceList.Insert(0, ti);
        ti = ti.BaseType.GetTypeInfo();
      }
    }

    void BuildMemberList()
    {
      foreach (TypeInfo ti in mInheritanceList)
      {
        if (ti.Equals(typeof(BusinessObject).GetTypeInfo()))
        {
          FieldInfo fi = ti.GetField("mId", BindingFlags.NonPublic | BindingFlags.Instance);
          mMemberAccessorList.Add(new SimpleFieldAccessor(Afx.Constants.AssemblyNamespace, "Id", fi, true));
          continue;
        }

        DataContractAttribute dca = ti.GetCustomAttribute<DataContractAttribute>(false);

        foreach (FieldInfo fi in ti.DeclaredFields.Where(i => i.IsDefined(typeof(DataMemberAttribute))))
        {
          DataMemberAttribute dma = fi.GetCustomAttribute<DataMemberAttribute>();
          if (fi.FieldType.GetTypeInfo().IsValueType || fi.FieldType == typeof(string))
          {
            mMemberAccessorList.Add(new SimpleFieldAccessor(dca.Namespace, dma.Name, fi));
          }
          else
          {
            if (typeof(BusinessObject).IsAssignableFrom(fi.FieldType))
            {
              mMemberAccessorList.Add(new ComplexFieldAccessor(dca.Namespace, dma.Name, fi));
            }
            else
            {
              mMemberAccessorList.Add(new ComplexFieldAccessor(Afx.Constants.AssemblyNamespace, dma.Name, fi));
            }
          }
        }

        foreach (PropertyInfo pi in ti.DeclaredProperties.Where(i => i.IsDefined(typeof(DataMemberAttribute))))
        {
          DataMemberAttribute dma = pi.GetCustomAttribute<DataMemberAttribute>();
          if (pi.PropertyType.GetTypeInfo().IsValueType || pi.PropertyType == typeof(string))
          {
            mMemberAccessorList.Add(new SimplePropertyAccessor(dca.Namespace, dma.Name, pi));
          }
          else
          {
            if (typeof(BusinessObject).IsAssignableFrom(pi.PropertyType))
            {
              mMemberAccessorList.Add(new ComplexPropertyAccessor(dca.Namespace, dma.Name, pi));
            }
            else
            {
              mMemberAccessorList.Add(new ComplexPropertyAccessor(Afx.Constants.AssemblyNamespace, dma.Name, pi));
            }
          }
        }
      }
    }

    #region XmlConverter GetConverter(...)

    static object mLock = new object();

    public static XmlConverter GetConverter<T>()
    {
      return GetConverter(typeof(T).GetTypeInfo());
    }

    //public static XmlConverter GetConverter(TypeInfo objectTypeInfo)
    //{
    //  if (objectTypeInfo == null) throw new ArgumentException("objectTypeInfo"); //TODO: internationalization
    //  if (!objectTypeInfo.Assembly.Equals(typeof(BusinessObject).GetTypeInfo().Assembly) && !objectTypeInfo.IsDefined(typeof(DataContractAttribute))) throw new ArgumentException("objectTypeInfo"); //TODO: internationalization

    //  lock (mLock)
    //  {
    //    if (mConverterDictionary.ContainsKey(objectTypeInfo)) return mConverterDictionary[objectTypeInfo];
    //    XmlConverter converter = new XmlConverter(objectTypeInfo);
    //    return converter;
    //  }
    //}

    public static XmlConverter GetConverter(TypeInfo objectTypeInfo)
    {
      if (objectTypeInfo == null) throw new ArgumentException("objectTypeInfo"); //TODO: internationalization
      if (!objectTypeInfo.Assembly.Equals(typeof(BusinessObject).GetTypeInfo().Assembly) && !objectTypeInfo.IsDefined(typeof(DataContractAttribute))) throw new ArgumentException("objectTypeInfo"); //TODO: internationalization

      lock (mLock)
      {
        if (mConverterDictionary.ContainsKey(objectTypeInfo)) return mConverterDictionary[objectTypeInfo];
        string ns = Afx.Constants.AssemblyNamespace;
        DataContractAttribute dca = objectTypeInfo.GetCustomAttribute<DataContractAttribute>();
        if (dca != null) ns = dca.Namespace;
        XmlConverter converter = new XmlConverter(objectTypeInfo, ns);
        return converter;
      }
    }

    //public static XmlConverter GetConverter(XmlReader reader, XmlElementAdapter xe)
    //{
    //  if (reader == null) throw new ArgumentException("reader"); //TODO: internationalization
    //  reader.MoveToFirstAttribute();
    //  if (!(reader.NamespaceURI == Afx.Constants.AssemblyNamespace && (reader.LocalName == "Type" || reader.LocalName == "Id")))
    //  {
    //    throw new InvalidOperationException("First attribute must be the object type or id"); //TODO: internationalization
    //  }

    //  Uri uri = new Uri(reader.Value);
    //  TypeInfo ti = KnownTypesProvider.GetType(uri);
    //  return GetConverter(ti);
    //}

    #endregion
  }
}

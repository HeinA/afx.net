﻿using Afx.Prism.Controllers;
using Afx.Prism.Mdi.Ribbon;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.Mdi.ApplicationConfigurationModule.Ribbon
{
  [Export(typeof(IRibbonGroup))]
  class SetupGroup : RibbonGroup
  {
    public const string GroupName = "Setup";

    public override string TabName
    {
      get { return ApplicationConfigurationTab.TabName; }
    }

    public override string Name
    {
      get { return GroupName; }
    }
  }
}

﻿using Afx.Prism.Mdi.Ribbon;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.Mdi.ApplicationConfigurationModule.Ribbon
{
  [Export(typeof(IRibbonTab))]
  class ApplicationConfigurationTab : RibbonTab
  {
    public const string TabName = "Application Configuration";

    public override string Name
    {
      get { return TabName; }
    }
  }
}

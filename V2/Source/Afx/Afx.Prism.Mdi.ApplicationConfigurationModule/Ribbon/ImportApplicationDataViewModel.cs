﻿using Afx.Prism.Controllers;
using Afx.Prism.Mdi.Ribbon;
using Afx.Prism.Mdi.ViewModels;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.Mdi.ApplicationConfigurationModule.Ribbon
{
  [Export(typeof(IRibbonItem))]
  class ImportApplicationDataViewModel : RibbonItemViewModel
  {
    public const string Identifier = "{5D41D28F-53A6-4930-ADC8-9ED744CD2A79}";

    public override string TabName
    {
      get { return ApplicationConfigurationTab.TabName; }
    }

    public override string GroupName
    {
      get { return SetupGroup.GroupName; }
    }

    public override string ItemIdentifier
    {
      get { return Identifier; }
    }

    [Dependency]
    public ModuleController ModuleController { get; set; }

    #region DelegateCommand ClickCommand

    DelegateCommand mClickCommand;
    public DelegateCommand ClickCommand
    {
      get { return mClickCommand ?? (mClickCommand = new DelegateCommand(ExecuteClick)); }
    }

    void ExecuteClick()
    {
    }

    #endregion

  }
}

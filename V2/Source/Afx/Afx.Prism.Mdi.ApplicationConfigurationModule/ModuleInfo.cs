﻿using Afx.Prism;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.Mdi.ApplicationConfigurationModule
{
  [Export(typeof(IModuleInfo))]
  public class ModuleInfo : IModuleInfo
  {
    public const string Namespace = "http://openafx.net/ApplicationConfiguration";

    public string ModuleNamespace
    {
      get { return Namespace; }
    }

    public string[] GetModuleDependencies()
    {
      return null; 
    }

    public Type ModuleController
    {
      get { return typeof(ModuleController); }
    }
  }
}

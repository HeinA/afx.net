﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.ComponentModel.Collections
{
  internal interface IBusinessObjectCollection : IEnumerable, IEditNotifier
  {
    Type ItemType { get; }
    void Add(object obj);
    object Owner { get; set; }
    IEnumerable<Guid> DeletedItems { get; }
    void AddDeletedItem(Guid id);
    int Count { get; }
  }
}

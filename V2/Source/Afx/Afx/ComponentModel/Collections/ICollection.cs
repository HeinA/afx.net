﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.ComponentModel.Collections
{
  internal interface ICollection : System.Collections.ICollection
  {
    BusinessObject Owner { get; set; }
    void Add(BusinessObject obj);
  }
}

﻿using Afx.ComponentModel.Markup;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.Reflection;
using Afx.ComponentModel.Validation;

namespace Afx.ComponentModel.Collections
{
  public class BusinessCollection<T> : ObservableCollection<T>, IBusinessObjectCollection, IEventStateAware, IEditNotifier, IAfxSerializationAware, IErrorProvider, INotifyPropertyChanged
    where T : BusinessObject
  {
    #region Constructors

    public BusinessCollection()
    {
    }

    public BusinessCollection(BusinessObject owner)
      : this()
    {
      Owner = owner;
    }

    #endregion

    #region BusinessObject Owner

    protected const string OwnerProperty = "Owner";
    [DataMember(Name = OwnerProperty)]
    BusinessObject mOwner;
    protected BusinessObject Owner
    {
      get { return mOwner; }
      set { mOwner = value; }
    }

    #endregion

    #region bool SuppressEventState

    bool mSuppressEventState;
    protected bool SuppressEventState
    {
      get
      {
        if (!mSuppressEventState && mOwner != null) return mOwner.SuppressEventState;
        return mSuppressEventState;
      }
      set
      {
        mSuppressEventState = value;
        foreach (IEventStateAware item in this)
        {
          item.SuppressEventState = mSuppressEventState;
        }
      }
    }

    #endregion

    #region Items

    #region void InsertItem(...)

    protected override void InsertItem(int index, T item)
    {
      base.InsertItem(index, item);
      AddItemCore(item);
      OnObjectEdited();
    }

    #endregion

    #region void RemoveItem(...)

    protected override void RemoveItem(int index)
    {
      T item = this[index];
      base.RemoveItem(index);
      RemoveItemCore(item);
      OnObjectEdited();
    }

    #endregion

    #region void ClearItems(...)

    protected override void ClearItems()
    {
      foreach (T item in this)
      {
        RemoveItemCore(item);
      }
      base.ClearItems();
      OnObjectEdited();
    }

    #endregion

    #region void SetItem(...)

    protected override void SetItem(int index, T item)
    {
      T oldItem = this[index];
      base.SetItem(index, item);
      RemoveItemCore(oldItem);
      AddItemCore(item);
      OnObjectEdited();
    }

    #endregion

    #region void AddItemCore(...)

    protected virtual void AddItemCore(T item)
    {
      item.Owner = Owner;
      item.SuppressEventState = this.mSuppressEventState;
      item.PropertyChanged += Item_PropertyChanged;
      if (DeletedItems.Contains(item.Id)) DeletedItems.Remove(item.Id);
    }

    #endregion

    #region void RemoveItemCore(...)

    protected virtual void RemoveItemCore(T item)
    {
      item.Owner = null;
      item.SuppressEventState = false;
      item.PropertyChanged -= Item_PropertyChanged;
      if (SuppressEventState) return;
      if (!DeletedItems.Contains(item.Id)) DeletedItems.Add(item.Id);
    }

    #endregion

    #region void Item_PropertyChanged(...)

    void Item_PropertyChanged(object sender, PropertyChangedEventArgs e)
    {
      OnObjectEdited();
    }

    #endregion

    #endregion

    #region List<Guid> DeletedItems

    List<Guid> mDeletedItems;
    protected List<Guid> DeletedItems
    {
      get { return mDeletedItems ?? (mDeletedItems = new List<Guid>()); }
    }

    #endregion


    #region IBusinessObjectCollection

    void IBusinessObjectCollection.Add(object obj)
    {
      this.Add((T)obj);
    }

    Type IBusinessObjectCollection.ItemType
    {
      get { return typeof(T); }
    }

    object IBusinessObjectCollection.Owner
    {
      get { return mOwner; }
      set { mOwner = (BusinessObject)value; }
    }

    IEnumerable<Guid> IBusinessObjectCollection.DeletedItems
    {
      get { return DeletedItems; }
    }

    void IBusinessObjectCollection.AddDeletedItem(Guid id)
    {
      DeletedItems.Add(id);
    }

    #endregion

    #region IEventStateAware

    bool IEventStateAware.SuppressEventState
    {
      get { return SuppressEventState; }
      set { SuppressEventState = value; }
    }

    #endregion

    #region IEditNotifier

    public event EventHandler ObjectEdited;

    protected virtual void OnObjectEdited()
    {
      Validate();
      if (SuppressEventState) return;
      if (ObjectEdited != null) ObjectEdited.Invoke(this, EventArgs.Empty);
    }

    void IEditNotifier.RaiseObjectEdited()
    {
      OnObjectEdited();
    }

    #endregion

    #region IAfxSerializationAware

    //void IAfxSerializationAware.OnDeserializing()
    //{
    //  OnDeserializing();
    //}

    //protected virtual void OnDeserializing()
    //{
    //}

    void IAfxSerializationAware.OnDeserialized()
    {
      OnDeserialized();
    }

    protected virtual void OnDeserialized()
    {
    }

    #endregion

    #region IErrorProvider

    bool mValidatorsInitialized = false;
    void InitializeValidators()
    {
      if (mValidatorsInitialized) return;
      mValidatorsInitialized = true;
      PopulateValidators(this.GetType());
      Validate();
    }

    #region Collection<ObjectValidator> Validators

    Collection<ObjectValidator> mValidators;
    Collection<ObjectValidator> Validators
    {
      get { return mValidators ?? (mValidators = new Collection<ObjectValidator>()); }
    }

    #endregion

    #region PopulateValidators(...)

    void PopulateValidators(Type objectType)
    {
      foreach (var vi in ValidationHelper.GetValidators(objectType))
      {
        ObjectValidator ov = ValidationHelper.CreateValidator(this, vi);
        Validators.Add(ov);
      }

      OnPopulateValidators(Validators);
    }

    #endregion

    #region void OnPopulateValidators(...)

    protected virtual void OnPopulateValidators(Collection<ObjectValidator> validators)
    {
    }

    #endregion

    EventHandler<DataErrorsChangedEventArgs> mErrorsChanged;
    public event EventHandler<DataErrorsChangedEventArgs> ErrorsChanged
    {
      add { mErrorsChanged += value; }
      remove { mErrorsChanged -= value; }
    }

    public IEnumerable GetErrors(string propertyName)
    {
      InitializeValidators();
      Collection<string> errors = new Collection<string>();
      if (string.IsNullOrWhiteSpace(propertyName))
      {
        foreach (var pv in Validators.OfType<PropertyValidator>().Where(v => v.PropertyInfo.Name.Equals(propertyName)))
        {
          pv.Validate();
          if (!pv.IsValid) errors.Add(pv.ErrorMessage);
        }
      }
      else
      {
        foreach (var v in Validators)
        {
          v.Validate();
          if (!v.IsValid) errors.Add(v.ErrorMessage);
        }
      }

      return errors;
    }

    public void Validate()
    {
      InitializeValidators();
      bool hasErrors = false;
      Collection<string> errors = new Collection<string>();

      foreach (var v in Validators)
      {
        v.Validate();
        if (!v.IsValid)
        {
          hasErrors = true;
          errors.Add(v.ErrorMessage);
        }
      }

      HasErrors = hasErrors;
      Errors = string.Join("\n", errors);
    }

    #region string Errors

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1000:DoNotDeclareStaticMembersOnGenericTypes")]
    public const string ErrorsProperty = "Errors";
    string mErrors;
    public string Errors
    {
      get
      {
        InitializeValidators();
        return mErrors;
      }
      private set
      {
        if (mErrors != value)
        {
          mErrors = value;
          if (mErrorsChanged != null) mErrorsChanged(this, new DataErrorsChangedEventArgs(null));
          OnPropertyChanged(ErrorsProperty);
        }
      }
    }

    #endregion

    #region bool HasErrors

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1000:DoNotDeclareStaticMembersOnGenericTypes")]
    public const string HasErrorsProperty = "HasErrors";
    bool mIsValid;
    public bool HasErrors
    {
      get
      {

        InitializeValidators();
        return mIsValid;
      }
      private set
      {
        if (mIsValid != value)
        {
          mIsValid = value;
          OnPropertyChanged(HasErrorsProperty);
        }
      }
    }

    #endregion

    #endregion

    #region INotifyPropertyChanged

    public new event PropertyChangedEventHandler PropertyChanged;

    public virtual void OnPropertyChanged(string propertyName)
    {
      if (!SuppressEventState && PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
    }

    #endregion
  }
}

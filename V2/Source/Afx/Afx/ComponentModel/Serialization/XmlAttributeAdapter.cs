﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.ComponentModel.Serialization
{
  class XmlAttributeAdapter
  {
    public XmlAttributeAdapter(string ns, string name, string value)
    {
      Namespace = ns;
      Name = name;
      Value = value;
    }

    #region string Namespace

    public const string NamespaceProperty = "Namespace";
    string mNamespace;
    public string Namespace
    {
      get { return mNamespace; }
      private set { mNamespace = value; }
    }

    #endregion

    #region string Name

    public const string NameProperty = "Name";
    string mName;
    public string Name
    {
      get { return mName; }
      private set { mName = value; }
    }

    #endregion

    #region string Value

    public const string ValueProperty = "Value";
    string mValue;
    public string Value
    {
      get { return mValue; }
      private set { mValue = value; }
    }

    #endregion
  }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.ComponentModel.Serialization
{
  [AttributeUsage(AttributeTargets.Interface)]
  public class BusinessObjectSerializerFormat : Attribute, IContractBehavior
  {
  }
}

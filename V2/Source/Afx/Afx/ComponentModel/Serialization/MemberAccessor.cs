﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Afx.ComponentModel.Serialization
{
  abstract class MemberAccessor
  {
    protected MemberAccessor(string memberNamespace, string name)
    {
      Name = name;
      Namespace = memberNamespace;
    }

    #region string Namespace

    string mNamespace;
    public string Namespace
    {
      get { return mNamespace; }
      private set { mNamespace = value; }
    }

    #endregion

    #region string Name

    string mName;
    public string Name
    {
      get { return mName; }
      private set { mName = value; }
    }

    #endregion
  }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using Afx.ComponentModel.Markup;

namespace Afx.ComponentModel.Serialization
{
  class XmlConverter
  {
    static Dictionary<TypeInfo, XmlConverter> mConverterDictionary = new Dictionary<TypeInfo, XmlConverter>();

    List<TypeInfo> mInheritanceList = new List<TypeInfo>();
    List<MemberAccessor> mMemberAccessorList = new List<MemberAccessor>();

    XmlConverter(TypeInfo objectTypeInfo)
    {
      TypeInfo = objectTypeInfo;
      mConverterDictionary.Add(TypeInfo, this);
      
      DataContractAttribute dca = TypeInfo.GetCustomAttribute<DataContractAttribute>(false);
      if (dca == null) throw new InvalidOperationException("Type does not have DataContract attribute"); //TODO: internationalization
      Namespace = dca.Namespace;

      BuildInheritanceList();
      BuildMemberList();
    }

    #region string Namespace

    public const string NamespaceProperty = "Namespace";
    string mNamespace;
    public string Namespace
    {
      get { return mNamespace; }
      private set { mNamespace = value; }
    }

    #endregion

    #region TypeInfo TypeInfo

    TypeInfo mTypeInfo;
    TypeInfo TypeInfo
    {
      get { return mTypeInfo; }
      set { mTypeInfo = value; }
    }

    #endregion

    #region string TypeName

    public string TypeName
    {
      get { return TypeInfo.Name; }
    }

    #endregion

    public SimpleMemberAccessor IdMember
    {
      get { return mMemberAccessorList.OfType<SimpleMemberAccessor>().FirstOrDefault(a => a.IsIdentifier); }
    }

    public IEnumerable<SimpleMemberAccessor> SimpleMembers
    {
      get { return mMemberAccessorList.OfType<SimpleMemberAccessor>().Where(a => !a.IsIdentifier && typeof(SimpleMemberAccessor).GetTypeInfo().IsAssignableFrom(a.GetType().GetTypeInfo())); }
    }


    void BuildInheritanceList()
    {
      TypeInfo ti = TypeInfo;
      while (ti != typeof(System.Object).GetTypeInfo())
      {
        mInheritanceList.Insert(0, ti);
        ti = ti.BaseType.GetTypeInfo();
      }
    }

    void BuildMemberList()
    {
      foreach (TypeInfo ti in mInheritanceList)
      {
        DataContractAttribute dca = ti.GetCustomAttribute<DataContractAttribute>(false);

        foreach (FieldInfo fi in ti.DeclaredFields.Where(i => i.IsDefined(typeof(DataMemberAttribute))))
        {
          if (fi.FieldType.GetTypeInfo().IsValueType || fi.FieldType == typeof(string))
          {
            DataMemberAttribute dma = fi.GetCustomAttribute<DataMemberAttribute>();
            if (dma.IsIdentifier)
              mMemberAccessorList.Add(new SimpleFieldAccessor(dca.Namespace, dma.Name, fi, true));
            else
              mMemberAccessorList.Add(new SimpleFieldAccessor(dca.Namespace, dma.Name, fi));
          }
          else
          {
            //Complex Field
          }
        }

        foreach (PropertyInfo pi in ti.DeclaredProperties.Where(i => i.IsDefined(typeof(DataMemberAttribute))))
        {
          if (pi.PropertyType.GetTypeInfo().IsValueType || pi.PropertyType == typeof(string))
          {
            DataMemberAttribute dma = pi.GetCustomAttribute<DataMemberAttribute>();
            if (dma.IsIdentifier)
              mMemberAccessorList.Add(new SimplePropertyAccessor(dca.Namespace, dma.Name, pi, true));
            else
              mMemberAccessorList.Add(new SimplePropertyAccessor(dca.Namespace, dma.Name, pi));
          }
          else
          {
            //Complex Field
          }
        }
      }
    }

    #region XmlConverter GetConverter(...)

    static object mLock = new object();

    public static XmlConverter GetConverter<T>()
      where T : BusinessObject
    {
      return GetConverter(typeof(T).GetTypeInfo());
    }

    public static XmlConverter GetConverter(TypeInfo objectTypeInfo)
    {
      if (!typeof(BusinessObject).GetTypeInfo().IsAssignableFrom(objectTypeInfo)) throw new ArgumentException("objectType"); //TODO: internationalization
      if (!objectTypeInfo.IsDefined(typeof(DataContractAttribute))) throw new ArgumentException("objectType"); //TODO: internationalization

      lock (mLock)
      {
        if (mConverterDictionary.ContainsKey(objectTypeInfo)) return mConverterDictionary[objectTypeInfo];
        XmlConverter converter = new XmlConverter(objectTypeInfo);
        return converter;
      }
    }

    #endregion
  }
}

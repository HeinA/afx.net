﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Afx.ComponentModel.Serialization
{
  class XmlElementAdapter
  {
    Dictionary<Guid, BusinessObject> mObjectDictionary = new Dictionary<Guid, BusinessObject>();
    List<XmlAttributeAdapter> mAttributes = new List<XmlAttributeAdapter>();

    public XmlElementAdapter(XmlConverter converter)
    {
      Converter = converter;
    }

    public XmlElementAdapter(XmlConverter converter, XmlElementAdapter parent)
      : this(converter)
    {
      Parent = parent;
    }

    void RegisterObject(BusinessObject obj)
    {
      if (Parent != null) Parent.RegisterObject(obj);
      if (mObjectDictionary.ContainsKey(obj.Id)) return;
      mObjectDictionary.Add(obj.Id, obj);
    }

    BusinessObject GetObject(Guid id)
    {
      if (Parent != null) return Parent.GetObject(id);
      if (!mObjectDictionary.ContainsKey(id)) return null;
      return mObjectDictionary[id];
    }

    #region XmlConverter Converter

    XmlConverter mConverter;
    public XmlConverter Converter
    {
      get { return mConverter; }
      set { mConverter = value; }
    }

    #endregion

    #region XmlElementAdapter Parent

    XmlElementAdapter mParent;
    public XmlElementAdapter Parent
    {
      get { return mParent; }
      private set { mParent = value; }
    }

    #endregion

    void RegisterAttribute(string ns, string name, string value)
    {
      mAttributes.Add(new XmlAttributeAdapter(ns, name, value));
    }

    void ReadObject(BusinessObject obj)
    {
      try
      {
        RegisterObject(obj);

        Uri uri = new Uri(new Uri(Converter.Namespace), Converter.TypeName);
        RegisterAttribute(Constants.AssemblyNamespace, "Type", uri.ToString());

        SimpleMemberAccessor ida = Converter.IdMember;
        RegisterAttribute(ida.Namespace, ida.Name, ida.GetValue(obj));

        foreach (var a in Converter.SimpleMembers)
        {
          RegisterAttribute(a.Namespace, a.Name, a.GetValue(obj));
        }
      }
      catch (Exception ex)
      {
        throw ex;
      }
    }

    IEnumerable<XmlAttributeAdapter> AttributeAdapters
    {
      get { return mAttributes; }
    }

    public void WriteXml(XmlWriter writer, BusinessObject obj)
    {
      ReadObject(obj);

      foreach (var a in AttributeAdapters)
      {
        writer.WriteAttributeString(KnownTypesProvider.GetNamespacePrefix(a.Namespace), a.Name, a.Namespace, a.Value);
      }
    }
  }
}

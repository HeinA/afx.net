﻿using Afx.ComponentModel.Markup;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Afx.ComponentModel.Serialization
{
  public static class KnownTypesProvider
  {
    class TypeNamespace
    {
      #region TypeInfo TypeInfo

      TypeInfo mTypeInfo;
      public TypeInfo TypeInfo
      {
        get { return mTypeInfo; }
        set { mTypeInfo = value; }
      }

      #endregion

      #region string Namespace

      string mNamespace;
      public string Namespace
      {
        get { return mNamespace; }
        set { mNamespace = value; }
      }

      #endregion

      #region string Prefix
      
      public const string PrefixProperty = "Prefix";
      string mPrefix;
      public string Prefix
      {
        get { return mPrefix; }
        set { mPrefix = value; }
      }
      
      #endregion

      #region Uri Uri

      public const string UriProperty = "Uri";
      Uri mUri;
      public Uri Uri
      {
        get { return mUri; }
        set { mUri = value; }
      }

      #endregion
    }

    static Dictionary<string, string> mNamespacePrefixDictionary = new Dictionary<string, string>();
    static List<TypeNamespace> mKnownTypes = new List<TypeNamespace>();

    static KnownTypesProvider()
    {
      RegisterAssembly(typeof(KnownTypesProvider).GetTypeInfo().Assembly);
    }

    public static void RegisterAssembly(Assembly assembly)
    {
      foreach (var ti in assembly.DefinedTypes.Where(t => t.IsDefined(typeof(DataContractAttribute)) && typeof(BusinessObject).GetTypeInfo().IsAssignableFrom(t)))
      {
        DataContractAttribute dca = ti.GetCustomAttribute<DataContractAttribute>(false);
        mKnownTypes.Add(new TypeNamespace() { TypeInfo = ti, Namespace = dca.Namespace, Prefix = string.Format("ns{0}", mKnownTypes.Count + 1), Uri = new Uri(new Uri(dca.Namespace), ti.Name) });
      }
    }

    public static IEnumerable<TypeInfo> KnownTypes
    {
      get { return mKnownTypes.Select<TypeNamespace, TypeInfo>(tn => tn.TypeInfo); }
    }

    public static string GetNamespacePrefix(string ns)
    {
      return mKnownTypes.FirstOrDefault(tn => tn.Namespace.Equals(ns)).Prefix; 
    }

    public static string GetNamespace(string prefix)
    {
      return mKnownTypes.FirstOrDefault(tn => tn.Prefix.Equals(prefix)).Namespace;
    }

    public static TypeInfo GetType(Uri uri)
    {
      return mKnownTypes.FirstOrDefault(tn => tn.Uri.Equals(uri)).TypeInfo;
    }

    public static Uri GetUri(TypeInfo typeInfo)
    {
      return mKnownTypes.FirstOrDefault(tn => tn.TypeInfo.Equals(typeInfo)).Uri;
    }
  }
}

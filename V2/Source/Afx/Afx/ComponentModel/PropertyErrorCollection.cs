﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.ComponentModel
{
  class PropertyErrorCollection
  {
    #region string PropertyName

    string mPropertyName;
    public string PropertyName
    {
      get { return mPropertyName; }
      set { mPropertyName = value; }
    }

    #endregion

    #region Collection<string> Errors

    Collection<string> mErrors;
    public Collection<string> Errors
    {
      get { return mErrors ?? (mErrors = new Collection<string>()); }
    }

    #endregion
  }
}

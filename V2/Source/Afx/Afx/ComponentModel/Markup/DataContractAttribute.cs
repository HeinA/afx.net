﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.ComponentModel.Markup
{
  [AttributeUsage(AttributeTargets.Class)]
  public class DataContractAttribute : Attribute
  {
    public DataContractAttribute(string ns)
    {
      Namespace = ns;
    }

    #region string Namespace

    string mNamespace;
    public string Namespace
    {
      get { return mNamespace; }
      set
      {
        mNamespace = value;
        if (mNamespace.Substring(mNamespace.Length - 1) != "/")
        {
          mNamespace += "/";
        }
      }
    }

    #endregion
  }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.ComponentModel.Markup
{
  [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
  public sealed class DataMemberAttribute : Attribute
  {
    public DataMemberAttribute()
    {
    }

    internal DataMemberAttribute(string name, bool isIdentifier)
    {
      Name = name;
      IsIdentifier = isIdentifier;
    }

    #region string Name

    string mName = null;
    public string Name
    {
      get { return mName; }
      set { mName = value; }
    }

    #endregion

    #region bool IsReference

    bool mIsReference = false;
    public bool IsReference
    {
      get { return mIsReference; }
      set { mIsReference = value; }
    }

    #endregion

    #region bool IsIdentifier

    bool mIsIdentifier;
    internal bool IsIdentifier
    {
      get { return mIsIdentifier; }
      private set { mIsIdentifier = value; }
    }

    #endregion  
  }
}

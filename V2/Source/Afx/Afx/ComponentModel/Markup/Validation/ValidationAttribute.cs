﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.ComponentModel.Markup.Validation
{
  public abstract class ValidationAttribute : Attribute
  {
    protected ValidationAttribute(string errorMessage)
    {
      ErrorMessage = errorMessage;
    }

    #region string ErrorMessage

    public const string ErrorMessageProperty = "ErrorMessage";
    string mErrorMessage;
    public string ErrorMessage
    {
      get { return mErrorMessage; }
      private set { mErrorMessage = value; }
    }

    #endregion

    public abstract Type ValidatorType { get; }
  }
}

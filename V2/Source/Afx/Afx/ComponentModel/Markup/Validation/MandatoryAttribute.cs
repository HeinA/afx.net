﻿using Afx.ComponentModel.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.ComponentModel.Markup.Validation
{
  [AttributeUsage(AttributeTargets.Property)]
  public sealed class MandatoryAttribute : ValidationAttribute
  {
    public MandatoryAttribute(string errorMessage)
      : base(errorMessage)
    {
    }

    public override Type ValidatorType
    {
      get { return typeof(MandatoryPropertyValidator); }
    }
  }
}

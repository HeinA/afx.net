﻿using Afx.ComponentModel.Collections;
using Afx.ComponentModel.Markup;
using Afx.ComponentModel.Markup.Validation;
using Afx.ComponentModel.Validation;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace Afx.ComponentModel
{
  [System.Runtime.Serialization.DataContract(Namespace = Constants.AssemblyNamespace, IsReference = true)]
  public abstract class BusinessObject : INotifyPropertyChanged, IEventStateAware, IEditNotifier, IAfxSerializationAware, IErrorProvider
  {
    #region Constructors

    protected BusinessObject()
    {
    }

    protected BusinessObject(BusinessObject owner)
      : this()
    {
      Owner = owner;
    }

    #endregion  

    #region BusinessObject Owner

    const string OwnerProperty = "Owner";
    [DataMember(Name = OwnerProperty)]
    [System.Runtime.Serialization.DataMember(Name = OwnerProperty)]
    BusinessObject mOwner;
    protected internal BusinessObject Owner
    {
      get { return mOwner; }
      set { mOwner = value; }
    }

    #endregion

    #region bool SuppressEventState

    bool mSuppressEventState;
    protected internal bool SuppressEventState
    {
      get
      {
        if (!mSuppressEventState && Owner != null) return Owner.SuppressEventState;
        return mSuppressEventState;
      }
      set { mSuppressEventState = value; }
    }

    #endregion

    #region Guid Id

    public const string IdProperty = "Id";
    [DataMember(IdProperty, true)]
    [System.Runtime.Serialization.DataMember(Name = IdProperty)]
    Guid mId = IdGenerator.GetSqlSequentialId();
    [Mandatory("Id is mandatory.")]
    public Guid Id
    {
      get { return mId; }
      set { SetProperty<Guid>(ref mId, value); }
    }

    #endregion

    #region bool IsDirty

    public const string IsDirtyProperty = "IsDirty";
    [DataMember(Name = IsDirtyProperty)]
    [System.Runtime.Serialization.DataMember(Name = IsDirtyProperty)]
    bool mIsDirty;
    public bool IsDirty
    {
      get { return mIsDirty; }
      set
      {
        if (SuppressEventState) return;
        if (mIsDirty != value)
        {
          mIsDirty = value;
          OnObjectEdited();
        }
      }
    }

    #endregion

    #region int Revision

    public const string RevisionProperty = "Revision";
    [DataMember(Name = RevisionProperty)]
    [System.Runtime.Serialization.DataMember(Name = RevisionProperty)]
    int mRevision = 0;
    public int Revision
    {
      get { return mRevision; }
      set { mRevision = value; }
    }

    #endregion


    #region Equality

    public override bool Equals(object obj)
    {
      BusinessObject bo = obj as BusinessObject;
      if (bo == null) return false;

      return Id.Equals(bo.Id);
    }

    public override int GetHashCode()
    {
      return Id.GetHashCode();
    }

    #endregion

    #region INotifyPropertyChanged

    public event PropertyChangedEventHandler PropertyChanged;

    public virtual void OnPropertyChanged(string propertyName)
    {
      if (!SuppressEventState && PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
    }

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1045:DoNotPassTypesByReference", MessageId = "0#"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1026:DefaultParametersShouldNotBeUsed")]
    protected virtual bool SetProperty<T>(ref T field, T value, [System.Runtime.CompilerServices.CallerMemberName] string propertyName = null)
    {
      if ((typeof(T).GetTypeInfo().IsValueType || typeof(T) == typeof(string)) && Object.Equals(field, value)) return false;
      else if ((object)field == (object)value) return false;

      field = value;
      
      if (SuppressEventState) return true;
      IsDirty = true;
      OnObjectEdited();
      this.OnPropertyChanged(propertyName);

      return true;
    }

    #endregion

    #region IEventStateAware

    bool IEventStateAware.SuppressEventState
    {
      get { return SuppressEventState; }
      set { SuppressEventState = value; }
    }

    #endregion

    #region IEditNotifier

    public event EventHandler ObjectEdited;

    protected virtual void OnObjectEdited()
    {
      Validate();
      if (SuppressEventState) return;
      if (ObjectEdited != null) ObjectEdited(this, EventArgs.Empty);
      IEditNotifier owner = Owner as IEditNotifier;
      if (owner != null) owner.RaiseObjectEdited();
    }

    void IEditNotifier.RaiseObjectEdited()
    {
      OnObjectEdited();
    }

    #endregion

    #region IAfxSerializationAware

    void IAfxSerializationAware.OnDeserialized()
    {
      OnDeserialized();
    }

    protected virtual void OnDeserialized()
    {
    }

    #endregion

    #region IErrorProvider

    bool mValidatorsInitialized = false;
    void InitializeValidators()
    {
      if (mValidatorsInitialized) return;
      mValidatorsInitialized = true;
      PopulateValidators(this.GetType());
      Validate();
    }

    #region Collection<ObjectValidator> Validators

    Collection<ObjectValidator> mValidators;
    Collection<ObjectValidator> Validators
    {
      get { return mValidators ?? (mValidators = new Collection<ObjectValidator>()); }
    }

    #endregion

    #region PopulateValidators(...)

    void PopulateValidators(Type objectType)
    {
      foreach (var vi in ValidationHelper.GetValidators(objectType))
      {
        ObjectValidator ov = ValidationHelper.CreateValidator(this, vi);
        Validators.Add(ov);
      }

      OnPopulateValidators(Validators);
    }

    #endregion

    #region void OnPopulateValidators(...)

    protected virtual void OnPopulateValidators(Collection<ObjectValidator> validators)
    {
    }

    #endregion

    EventHandler<DataErrorsChangedEventArgs> mErrorsChanged;
    public event EventHandler<DataErrorsChangedEventArgs> ErrorsChanged
    {
      add { mErrorsChanged += value; }
      remove { mErrorsChanged -= value; }
    }

    public IEnumerable GetErrors(string propertyName)
    {
      InitializeValidators();
      Collection<string> errors = new Collection<string>();
      if (!string.IsNullOrWhiteSpace(propertyName))
      {
        foreach (var pv in Validators.OfType<PropertyValidator>().Where(v => v.PropertyInfo.Name.Equals(propertyName)))
        {
          pv.Validate();
          if (!pv.IsValid) errors.Add(pv.ErrorMessage);
        }
      }
      else
      {
        foreach (var v in Validators)
        {
          v.Validate();
          if (!v.IsValid) errors.Add(v.ErrorMessage);
        }
      }

      return errors;
    }

    public void Validate()
    {
      InitializeValidators();
      bool hasErrors = false;
      Collection<string> errors = new Collection<string>();

      foreach (var v in Validators)
      {
        v.Validate();
        if (!v.IsValid)
        {
          hasErrors = true;
          errors.Add(v.ErrorMessage);
        }
      }

      HasErrors = hasErrors;
      Errors = string.Join("\n", errors);
    }

    #region string Errors

    public const string ErrorsProperty = "Errors";
    string mErrors;
    public string Errors
    {
      get
      {
        InitializeValidators();
        return mErrors;
      }
      private set
      {
        if (SetProperty<string>(ref mErrors, value))
        {
          if (mErrorsChanged != null) mErrorsChanged(this, new DataErrorsChangedEventArgs(null));
        }
      }
    }

    #endregion

    #region bool HasErrors

    public const string HasErrorsProperty = "HasErrors";
    bool mHasErrors;
    public bool HasErrors
    {
      get
      {
        InitializeValidators();
        return mHasErrors;
      }
      private set { SetProperty<bool>(ref mHasErrors, value); }
    }

    #endregion

    #endregion
  }
}

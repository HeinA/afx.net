﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.ComponentModel
{
  public class PropertyChangingCancelEventArgs : PropertyChangingEventArgs
  {
    public bool Cancel { get; set; }
    public object OriginalValue { get; private set; }
    public object NewValue { get; private set; }

    public PropertyChangingCancelEventArgs(string propertyName, object originalValue, object newValue)
      : base(propertyName)
    {
      this.OriginalValue = originalValue;
      this.NewValue = newValue;
    }
  }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.ComponentModel
{
  public interface IErrorProvider : INotifyDataErrorInfo
  {
    void Validate();
    string Errors { get; }
  }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.ComponentModel
{
  public interface IAfxSerializationAware
  {
    //void OnDeserializing();
    void OnDeserialized();
  }
}

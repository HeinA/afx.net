﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.ComponentModel
{
  public sealed class EventStateSuppressor : IDisposable
  {
    public EventStateSuppressor(IEventStateAware root)
    {
      Root = root;
      Root.SuppressEventState = true;
    }

    #region IEventStateAware Root

    const string RootProperty = "Root";
    IEventStateAware mRoot;
    IEventStateAware Root
    {
      get { return mRoot; }
      set { mRoot = value; }
    }

    #endregion

    public void Dispose()
    {
      IErrorProvider ep = Root as IErrorProvider;
      if (ep != null) ep.Validate();

      Root.SuppressEventState = false;
    }
  }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.ComponentModel.Validation
{
  public abstract class ObjectValidator
  {
    protected ObjectValidator(IErrorProvider provider, string errorMessage)
    {
      Provider = provider;
      ErrorMessage = errorMessage;
    }

    #region IErrorProvider Provider

    public const string ProviderProperty = "Provider";
    IErrorProvider mProvider;
    public IErrorProvider Provider
    {
      get { return mProvider; }
      set { mProvider = value; }
    }

    #endregion

    #region string ErrorMessage

    public const string ErrorMessageProperty = "ErrorMessage";
    string mErrorMessage;
    public string ErrorMessage
    {
      get { return mErrorMessage; }
      private set { mErrorMessage = value; }
    }

    #endregion

    #region bool IsValid

    public const string IsValidProperty = "IsValid";
    bool mIsValid;
    public bool IsValid
    {
      get { return mIsValid; }
      private set
      {
        if (mIsValid != value)
        {
          mIsValid = value;
          //RaiseErrorsChanged();
        }
      }
    }

    #endregion

    public void Validate()
    {
      IsValid = OnValidate();
    }

    protected abstract bool OnValidate();

    //protected void RaiseErrorsChanged()
    //{
    //  Provider.Validate();
    //}
  }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Afx.ComponentModel.Validation
{
  public class MandatoryPropertyValidator : PropertyValidator
  {
    public MandatoryPropertyValidator(IErrorProvider provider, string errorMessage, PropertyInfo propertyInfo)
      : base(provider, errorMessage, propertyInfo)
    {
    }

    protected override bool OnValidate()
    {
      object value = PropertyInfo.GetValue(Provider);
      if (value == null) return false;
      string svalue = value as string;
      if (string.IsNullOrWhiteSpace(svalue)) return false;
      if (value.GetType().GetTypeInfo().IsValueType && value.Equals(Activator.CreateInstance(value.GetType()))) return false;
      BusinessObject bo = value as BusinessObject;
      if (bo != null && bo.Id == Guid.Empty) return false;
      IList list = value as IList;
      if (list != null && list.Count == 0) return false;
      return true;
    }
  }
}

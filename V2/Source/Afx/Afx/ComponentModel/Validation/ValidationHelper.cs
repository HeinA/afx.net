﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using Afx.ComponentModel.Markup.Validation;
using Afx.ComponentModel.Collections;

namespace Afx.ComponentModel.Validation
{
  static class ValidationHelper
  {
    static Dictionary<Type, Collection<ValidationIndex>> mValidationDictionary = new Dictionary<Type, Collection<ValidationIndex>>();
    static Dictionary<Type, ConstructorInfo> mValidatorConstructors = new Dictionary<Type, ConstructorInfo>();

    public static Collection<ValidationIndex> GetValidators(Type objectType)
    {
      if (!typeof(IErrorProvider).GetTypeInfo().IsAssignableFrom(objectType.GetTypeInfo()))
      {
        throw new ArgumentException("Type does not implement IErrorProvider");
      }

      if (mValidationDictionary.ContainsKey(objectType)) return mValidationDictionary[objectType];
      Collection<ValidationIndex> index = new Collection<ValidationIndex>();
      PopulateValidators(objectType, index);
      mValidationDictionary.Add(objectType, index);
      return index;
    }

    public static ObjectValidator CreateValidator(IErrorProvider targetObject, ValidationIndex vi)
    {
      if (mValidatorConstructors.ContainsKey(vi.ValidatorType))
      {
        return CreateValidator(targetObject, vi, mValidatorConstructors[vi.ValidatorType]);
      }
      ConstructorInfo ci = vi.ValidatorType.GetTypeInfo().DeclaredConstructors.FirstOrDefault();
      mValidatorConstructors.Add(vi.ValidatorType, ci);
      return CreateValidator(targetObject, vi, ci);
    }

    static ObjectValidator CreateValidator(IErrorProvider targetObject, ValidationIndex vi, ConstructorInfo ci)
    {
      if (vi.PropertyInfo != null)
      {
        return (ObjectValidator)ci.Invoke(new object[] { targetObject, vi.ErrorMessage, vi.PropertyInfo });
      }
      else
      {
        return (ObjectValidator)ci.Invoke(new object[] { targetObject, vi.ErrorMessage, vi.PropertyInfoCollection });
      }
    }

    static void PopulateValidators(Type objectType, Collection<ValidationIndex> index)
    {
      if (objectType == typeof(object)) return;
      PopulateValidators(objectType.GetTypeInfo().BaseType, index);
      foreach (var pi in objectType.GetTypeInfo().DeclaredProperties.Where(i => i.IsDefined(typeof(ValidationAttribute))))
      {
        ValidationAttribute va = pi.GetCustomAttribute<ValidationAttribute>();
        index.Add(new ValidationIndex(va.ValidatorType, va.ErrorMessage, pi));
      }

      Collection<PropertyInfo> collections = new Collection<PropertyInfo>();
      foreach (var pi in objectType.GetTypeInfo().DeclaredProperties.Where(i => typeof(IBusinessObjectCollection).GetTypeInfo().IsAssignableFrom(i.PropertyType.GetTypeInfo())))
      {
        collections.Add(pi);
      }
      if (collections.Count > 0)
      {
        index.Add(new ValidationIndex(typeof(CollectionValidator), "A referenced object has errors.", collections));
      }
    }
  }
}

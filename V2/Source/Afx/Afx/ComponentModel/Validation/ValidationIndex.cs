﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Afx.ComponentModel.Validation
{
  class ValidationIndex
  {
    public ValidationIndex(Type validatorType, string errorMessage, PropertyInfo propertyInfo)
    {
      ValidatorType = validatorType;
      ErrorMessage = errorMessage;
      PropertyInfo = propertyInfo;
    }

    public ValidationIndex(Type validatorType, string errorMessage, Collection<PropertyInfo> propertyInfoCollection)
    {
      ValidatorType = validatorType;
      ErrorMessage = errorMessage;
      PropertyInfoCollection = propertyInfoCollection;
    }

    #region Type ValidatorType

    public const string ValidatorTypeProperty = "ValidatorType";
    Type mValidatorType;
    public Type ValidatorType
    {
      get { return mValidatorType; }
      private set { mValidatorType = value; }
    }

    #endregion

    #region string ErrorMessage

    public const string ErrorMessageProperty = "ErrorMessage";
    string mErrorMessage;
    public string ErrorMessage
    {
      get { return mErrorMessage; }
      private set { mErrorMessage = value; }
    }

    #endregion

    #region PropertyInfo PropertyInfo

    public const string PropertyInfoProperty = "PropertyInfo";
    PropertyInfo mPropertyInfo;
    public PropertyInfo PropertyInfo
    {
      get { return mPropertyInfo; }
      private set { mPropertyInfo = value; }
    }

    #endregion

    #region Collection<PropertyInfo> PropertyInfoCollection

    Collection<PropertyInfo> mPropertyInfoCollection;
    public Collection<PropertyInfo> PropertyInfoCollection
    {
      get { return mPropertyInfoCollection; }
      private set { mPropertyInfoCollection = value; }
    }

    #endregion
  }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Afx.ComponentModel.Validation
{
  public abstract class PropertyValidator : ObjectValidator
  {
    protected PropertyValidator(IErrorProvider provider, string errorMessage, PropertyInfo propertyInfo)
      : base(provider, errorMessage)
    {
      PropertyInfo = propertyInfo;
    }

    #region PropertyInfo PropertyInfo

    PropertyInfo mPropertyInfo;
    public PropertyInfo PropertyInfo
    {
      get { return mPropertyInfo; }
      private set { mPropertyInfo = value; }
    }

    #endregion

  }
}

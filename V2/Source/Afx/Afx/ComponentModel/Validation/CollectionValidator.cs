﻿using Afx.ComponentModel.Collections;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Afx.ComponentModel.Validation
{
  class CollectionValidator : ObjectValidator
  {
    public CollectionValidator(IErrorProvider provider, string errorMessage, Collection<PropertyInfo> propertyInfoCollection)
      : base(provider, errorMessage)
    {
      PropertyInfoCollection = propertyInfoCollection;
    }

    #region Collection<PropertyInfo> PropertyInfoCollection

    Collection<PropertyInfo> mPropertyInfoCollection;
    public Collection<PropertyInfo> PropertyInfoCollection
    {
      get { return mPropertyInfoCollection; }
      private set { mPropertyInfoCollection = value; }
    }

    #endregion

    protected override bool OnValidate()
    {
      foreach (PropertyInfo pi in PropertyInfoCollection)
      {
        IBusinessObjectCollection col = pi.GetValue(Provider) as IBusinessObjectCollection;
        if (col == null) continue;
        IErrorProvider ep = col as IErrorProvider;
        if (ep != null)
        {
          ep.Validate();
          if (ep.HasErrors) return false;
        }
        foreach (IErrorProvider bo in col)
        {
          bo.Validate();
          if (bo.HasErrors) return false;
        }
      }
      return true;
    }
  }
}

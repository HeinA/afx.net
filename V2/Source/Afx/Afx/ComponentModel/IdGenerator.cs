﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.ComponentModel
{
  public static class IdGenerator
  {
    static Random mRandom = new Random();
    static DateTime mEpoch = new DateTime(2015, 1, 1, 0, 0, 0, DateTimeKind.Utc);
    static UInt64 mLastTicks = 0;
    static UInt16 mCount = 1;

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
    public static Guid GetSqlSequentialId()
    {
      UInt64 ticks = (UInt64)DateTime.Now.Subtract(mEpoch).Ticks;
      if (ticks == mLastTicks)
      {
        mCount++;
      }
      else
      {
        mLastTicks = ticks;
        mCount = 1;
      }

      byte[] tickBytes = BitConverter.GetBytes(ticks).Reverse().ToArray();
      byte[] sequenceBytes = BitConverter.GetBytes((UInt16)mCount);

      byte[] randomNumber = new byte[8];
      mRandom.NextBytes(randomNumber);
    
      byte[] val = new byte[16];
      System.Buffer.BlockCopy(randomNumber, 0, val, 0, 8);
      //System.Buffer.BlockCopy(sequenceBytes, 0, val, 6, 2);
      System.Buffer.BlockCopy(tickBytes, 1, val, 10, 6);
      System.Buffer.BlockCopy(tickBytes, 7, val, 8, 1);
      val[9] = sequenceBytes[0];
      val[7] = sequenceBytes[1];

      Guid g = new Guid(val);
      return g;
    }
  }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

namespace Afx.Utilities
{
  public static class TypeHelper
  {
    public static string GetTypeName(Type type)
    {
      TypeInfo ti = IntrospectionExtensions.GetTypeInfo(type);
      return string.Format("{0}, {1}", type.FullName, ti.Assembly.GetName().Name);
    }
  }
}

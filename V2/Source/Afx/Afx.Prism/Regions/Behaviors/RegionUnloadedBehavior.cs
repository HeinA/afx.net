﻿using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Prism.Regions.Behaviors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Afx.Prism.Regions.Behaviors
{
  public class RegionUnloadedBehavior : RegionBehavior, IHostAwareRegionBehavior
  {
    public const string BehaviorKey = "RegionUnloaded";

    protected override void OnAttach()
    {
      FrameworkElement fe = HostControl as FrameworkElement;
      if (fe != null) fe.Unloaded += Element_Unloaded;
    }

    void Element_Unloaded(object sender, RoutedEventArgs e)
    {
      FrameworkElement fe = (FrameworkElement)sender;
      fe.Unloaded -= Element_Unloaded;
      if (HostControl == null) return;

      string regionName = HostControl.GetValue(RegionManager.RegionNameProperty) as string;
      IRegionManager rm = HostControl.GetValue(RegionManager.RegionManagerProperty) as IRegionManager;
      if (rm != null && rm.Regions.ContainsRegionWithName(regionName))
      {
        rm.Regions.Remove(regionName);
      }
    }

    private WeakReference elementWeakReference;
    public DependencyObject HostControl
    {
      get { return this.elementWeakReference != null ? this.elementWeakReference.Target as DependencyObject : null; }
      set { this.elementWeakReference = new WeakReference(value); }
    }
  }
}

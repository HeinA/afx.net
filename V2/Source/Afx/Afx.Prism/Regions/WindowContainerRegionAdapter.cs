﻿using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Xceed.Wpf.Toolkit;
using Xceed.Wpf.Toolkit.Primitives;

namespace Afx.Prism.Regions
{
  public class WindowContainerRegionAdapter : RegionAdapterBase<WindowContainer>
  {
    [InjectionConstructor]
    public WindowContainerRegionAdapter(IRegionBehaviorFactory regionBehaviorFactory)
      : base(regionBehaviorFactory)
    {
    }

    Dictionary<object, WeakReference> mChildWindows = new Dictionary<object, WeakReference>();

    void RegisterChildWindow(object element, ChildWindow childWindow)
    {
      WeakReference wr = new WeakReference(childWindow);
      mChildWindows.Add(element, wr);
    }

    void RemoveChildWindow(object element)
    {
      if (!mChildWindows.ContainsKey(element)) return;
      mChildWindows.Remove(element);
    }

    ChildWindow GetChildWindow(object element)
    {
      if (!mChildWindows.ContainsKey(element)) return null;
      WeakReference wr = mChildWindows[element];
      if (!wr.IsAlive)
      {
        mChildWindows.Remove(element);
        return null;
      }
      return (ChildWindow)wr.Target;
    }

    protected override void Adapt(IRegion region, WindowContainer regionTarget)
    {
      if (region == null) throw new ArgumentNullException("region");
      if (regionTarget == null) throw new ArgumentNullException("regionTarget");

      region.Views.CollectionChanged += (s, e) =>
      {
        if (e.Action == NotifyCollectionChangedAction.Add)
          foreach (object element in e.NewItems)
          {
            ChildWindow cw = element as ChildWindow;
            if (cw == null)
            {
              DataTemplateKey dtk = new DataTemplateKey(element.GetType());
              DataTemplate dt = (DataTemplate)Application.Current.FindResource(dtk);
              cw = (ChildWindow)dt.LoadContent();
              cw.DataContext = element;
            }
            regionTarget.Children.Add(cw);
            RegisterChildWindow(element, cw);
          }

        if (e.Action == NotifyCollectionChangedAction.Remove)
          foreach (object element in e.OldItems)
          {
            ChildWindow cw = GetChildWindow(element);
            if (cw != null)
            {
              BindingOperations.ClearAllBindings(cw);
              cw.WindowState = Xceed.Wpf.Toolkit.WindowState.Closed;
              cw.Content = null;
              cw.DataContext = null;
              RemoveChildWindow(element);
              regionTarget.Children.Remove(cw);
            }
          }
      };
    }

    protected override IRegion CreateRegion()
    {
      return new SingleActiveRegion();
    }
  }
}

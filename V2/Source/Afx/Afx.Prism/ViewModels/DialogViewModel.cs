﻿using Afx.Prism.Controllers;
using Microsoft.Practices.Prism.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xceed.Wpf.Toolkit;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Prism.Regions;
using System.Windows;

namespace Afx.Prism.ViewModels
{
  public abstract class DialogViewModel : ViewModel
  {
    #region IRegionManager RegionManager

    IRegionManager mRegionManager;
    public IRegionManager RegionManager
    {
      get { return mRegionManager ?? (mRegionManager = Container.Resolve<IRegionManager>()); }
    }

    #endregion

    #region WindowState WindowState

    public const string WindowStateProperty = "WindowState";
    Xceed.Wpf.Toolkit.WindowState mWindowState = Xceed.Wpf.Toolkit.WindowState.Open;
    public Xceed.Wpf.Toolkit.WindowState WindowState
    {
      get { return mWindowState; }
      set
      {
        if (SetProperty<Xceed.Wpf.Toolkit.WindowState>(ref mWindowState, value))
        {
          if (WindowState == Xceed.Wpf.Toolkit.WindowState.Closed)
          {
            DialogController.Dispose();
          }
        }
      }
    }

    #endregion

    #region string Error

    public const string ErrorProperty = "Error";
    public string Error
    {
      get { return DialogController.Error; }
    }

    #endregion

    #region Visibility ErrorVisibility

    public const string ErrorVisibilityProperty = "ErrorVisibility";
    public Visibility ErrorVisibility
    {
      get { return string.IsNullOrWhiteSpace(Error) ? Visibility.Collapsed : Visibility.Visible; }
    }

    #endregion

    #region DelegateCommand OkCommand

    DelegateCommand mOkCommand;
    public DelegateCommand OkCommand
    {
      get { return mOkCommand ?? (mOkCommand = new DelegateCommand(ExecuteOk)); }
    }

    void ExecuteOk()
    {
      if (DialogController.Validate())
      {
        DialogController.OnApply();
        WindowState = Xceed.Wpf.Toolkit.WindowState.Closed;
      }
      else
      {
        System.Media.SystemSounds.Hand.Play();
      }
    }

    #endregion

    #region DelegateCommand CancelCommand

    DelegateCommand mCancelCommand;
    public DelegateCommand CancelCommand
    {
      get { return mCancelCommand ?? (mCancelCommand = new DelegateCommand(ExecuteCancel)); }
    }

    void ExecuteCancel()
    {
      WindowState = Xceed.Wpf.Toolkit.WindowState.Closed;
    }

    #endregion


    #region DialogController DialogController

    DialogController mDialogController;
    protected DialogController DialogController
    {
      get { return mDialogController ?? (mDialogController = Container.Resolve<DialogController>()); }
    }

    #endregion

    #region void OnLoaded()

    protected internal override void OnLoaded()
    {
      base.OnLoaded();

      DialogController.RaiseOnLoaded();
    }

    #endregion
  }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.ViewModels
{
  [Export(typeof(IResourceInfo))]
  class ResourceInfo : IResourceInfo
  {
    public Uri[] GetUriCollection()
    {
      return new Uri[] { new Uri("pack://application:,,,/Afx.Prism;component/ViewModels/Resources.xaml") }; 
    }

    public ResourceClass ResourceClass
    {
      get { return ResourceClass.Dialog; }
    }

    public decimal Priority
    {
      get { return 0; }
    }
  }
}

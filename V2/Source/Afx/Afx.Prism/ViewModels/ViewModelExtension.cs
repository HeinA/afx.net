﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace Afx.Prism.ViewModels
{
  public static class ViewModelExtension
  {
    public static bool GetIsAttached(DependencyObject obj)
    {
      if (obj == null) throw new ArgumentNullException("obj");

      return (bool)obj.GetValue(IsAttachedProperty);
    }

    public static void SetIsAttached(DependencyObject obj, bool value)
    {
      if (obj == null) throw new ArgumentNullException("obj");

      obj.SetValue(IsAttachedProperty, value);
    }

    public static readonly DependencyProperty IsAttachedProperty = DependencyProperty.RegisterAttached("IsAttached", typeof(bool), typeof(ViewModelExtension), new FrameworkPropertyMetadata(false, OnIsAttachedPropertyChanged));

    private static void OnIsAttachedPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
      FrameworkElement fe = d as FrameworkElement;
      if (fe != null && (bool)e.NewValue)
      {
        fe.Loaded += Element_Loaded;
      }
    }

    static void Element_Loaded(object sender, RoutedEventArgs e)
    {
      FrameworkElement fe = sender as FrameworkElement;
      if (fe != null)
      {
        ViewModel vm = fe.DataContext as ViewModel;
        if (vm != null)
        {
          //bool b = fe.MoveFocus(new TraversalRequest(FocusNavigationDirection.Next));
          vm.OnLoaded();
          fe.Loaded -= Element_Loaded;
        }
      }
    }
  }
}

﻿using Microsoft.Practices.ServiceLocation;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Prism.UnityExtensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using Microsoft.Practices.Prism.PubSubEvents;
using System.Reflection;
using Afx.Prism.ViewModels;
using Microsoft.Practices.Prism.Regions;
using System.Globalization;
using System.Windows;

namespace Afx.Prism.Controllers
{
  public abstract class ApplicationController : Controller
  {
    protected IRegionManager RegionManager { get; private set; }

    #region Dialogs

    Collection<DialogViewModel> mDialogs = new Collection<DialogViewModel>();
    public IEnumerable<DialogViewModel> Dialogs
    {
      get { return mDialogs; }
    }

    public int DialogCount
    {
      get { return mDialogs.Count; }
    }

    protected virtual void OnShowDialog(DialogViewModel viewModel)
    {
    }

    protected virtual void OnCloseDialog(DialogViewModel viewModel)
    {
    }

    internal void ShowDialog(DialogViewModel vm)
    {
      mDialogs.Add(vm);
      OnShowDialog(vm);
    }

    internal void CloseDialog(DialogViewModel vm)
    {
      if (Application.Current == null) return;
      mDialogs.Remove(vm);
      OnCloseDialog(vm);
    }

    #endregion

    #region ModuleController GetModuleController(...)

    public ModuleController GetModuleController(string ns)
    {
      ModuleController mc = ChildControllers.OfType<ModuleController>().FirstOrDefault(mc1 => mc1.ModuleNamespace.Equals(ns));
      if (mc == null) throw new InvalidOperationException(string.Format(CultureInfo.InvariantCulture, Properties.Resources.NoModuleForNamespace, ns));
      return mc;
    }

    public ModuleController GetModuleController(Assembly assembly)
    {
      ModuleController mc = ChildControllers.OfType<ModuleController>().FirstOrDefault(mc1 => mc1.GetType().Assembly.Equals(assembly));
      if (mc == null) throw new InvalidOperationException(string.Format(CultureInfo.InvariantCulture, Properties.Resources.NoModuleForAssembly, assembly.ToString()));
      return mc;
    }

    #endregion

    #region void ConfigureContainer(...)

    protected internal override void ConfigureContainer()
    {
      base.ConfigureContainer();

      Container.RegisterInstance<ApplicationController>(this);
      EventAggregator = Container.Resolve<IEventAggregator>();
      RegionManager = Container.Resolve<IRegionManager>();
    }

    #endregion
  }
}

﻿using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.Controllers
{
  internal class ControllerFactory
  {
    public static ModuleController CreateModuleController(IUnityContainer container, Type moduleType)
    {
      container = container.CreateChildContainer();
      ModuleController c = (ModuleController)container.Resolve(moduleType);
      container.BuildUp(moduleType, c);
      c.Container = container;
      c.ConfigureContainer();
      ApplicationController ac = container.Resolve<ApplicationController>();
      ac.AddModule(c.Namespace, c);
      container.RegisterInstance<ModuleController>(c);
      container.RegisterInstance(moduleType, c);      
      return c;
    }
  }
}

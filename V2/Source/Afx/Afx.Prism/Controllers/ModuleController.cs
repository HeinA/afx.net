﻿using Microsoft.Practices.ObjectBuilder2;
using Microsoft.Practices.Prism.Modularity;
using Microsoft.Practices.Prism.PubSubEvents;
using Microsoft.Practices.ServiceLocation;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.Controllers
{
  public abstract class ModuleController : Controller, IModule
  {
    public abstract string ModuleNamespace { get; }

    #region void ConfigureContainer(...)

    protected internal override void ConfigureContainer()
    {
      base.ConfigureContainer();

      EventAggregator = new EventAggregator();
      Container.RegisterInstance<IEventAggregator>(EventAggregator);

      Container.RegisterInstance<ModuleController>(this);
      Container.RegisterInstance(this.GetType(), this);      
    }

    #endregion
  }
}

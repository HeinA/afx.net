﻿using Microsoft.Practices.ServiceLocation;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Prism.UnityExtensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Collections.ObjectModel;
using Microsoft.Practices.Prism.PubSubEvents;
using System.Globalization;

namespace Afx.Prism.Controllers
{
  public abstract class Controller : INotifyPropertyChanged, IDisposable
  {
    #region Constructors & Destructor

    ~Controller()
    {
      Dispose(false);
    }

    #endregion

    public IEventAggregator EventAggregator { get; protected set; }

    protected internal IUnityContainer Container { get; internal set; }

    #region Guid Id

    public const string IdProperty = "Id";
    string mId = Guid.NewGuid().ToString();
    public virtual string Id
    {
      get { return mId; }
      set
      {
        if (Parent != null && value != Id && Parent.ChildControllers.FirstOrDefault(cc => cc.Id.Equals(value)) != null)
        {
          throw new InvalidOperationException(string.Format(CultureInfo.InvariantCulture, Properties.Resources.ControllerNameExistsInScope, value));
        }
        SetProperty<string>(ref mId, value);
      }
    }

    #endregion

    #region Controller Parent

    Controller mParent;
    public Controller Parent
    {
      get { return mParent ?? (mParent = Container == null ? null : Container.Parent == null ? null : Container.Parent.Resolve<Controller>()); }
    }

    #endregion

    #region bool IsDisposed

    bool mIsDisposed;
    public bool IsDisposed
    {
      get { return mIsDisposed; }
      private set { mIsDisposed = value; }
    }

    #endregion

    #region IEnumerable<IController> ChildControllers

    Collection<Controller> mChildControllers = new Collection<Controller>();
    public IEnumerable<Controller> ChildControllers
    {
      get { return mChildControllers; }
    }

    #endregion

    #region T GetCreateChildController<T>(...)

    public T GetCreateChildController<T>(string id)
      where T : Controller
    {
      if (string.IsNullOrWhiteSpace(id)) throw new ArgumentNullException("id");
      
      T controller = GetChildController<T>(id);
      if (controller != null) return controller;

      controller = Container.Resolve<T>();
      controller.Id = id;
      return controller;
    }

    #endregion

    #region GetChildController(...)

    public Controller GetChildController(string id)
    {
      return ChildControllers.FirstOrDefault(c => c.Id.Equals(id));
    }

    public T GetChildController<T>(string id)
    {
      object o = GetChildController(id);
      if (o != null && !typeof(T).Equals(o.GetType())) throw new InvalidOperationException(string.Format(CultureInfo.InvariantCulture, Properties.Resources.DifferentControllerHasName, id));
      return (T)o;
    }

    #endregion

    #region void Initialize(...)

    bool mIsInitialized = false;
    public void Initialize()
    {
      if (mIsInitialized)
      {
        Activate();
        return;
      }
      mIsInitialized = true;
      OnInitialized();
    }

    #endregion


    #region void OnInitialized(...)

    protected virtual void OnInitialized()
    {
    }

    #endregion

    #region Activation

    public void Activate()
    {
      OnActivate();
    }

    protected virtual void OnActivate()
    {
    }

    #endregion

    #region void OnLoaded(...)
    
    protected virtual void OnLoaded()
    {
    }

    internal void RaiseOnLoaded()
    {
      OnLoaded();
    }

    #endregion

    #region void ConfigureContainer(...)

    protected internal virtual void ConfigureContainer()
    {
      Container.RegisterInstance<Controller>(this);
    }

    #endregion


    #region void AddChildController(...)

    internal void AddChildController(Controller controller)
    {
      if (GetChildController(controller.Id) != null)
      {
        throw new InvalidOperationException(string.Format(CultureInfo.InvariantCulture, Properties.Resources.ControllerNameExistsInScope, controller.Id));
      }

      mChildControllers.Add(controller);
    }

    #endregion

    #region void RemoveChildController(...)

    internal void RemoveChildController(Controller controller)
    {
      mChildControllers.Remove(controller);
    }

    #endregion

    #region INotifyPropertyChanged 

    public event PropertyChangedEventHandler PropertyChanged;

    protected virtual void OnPropertyChanged(string propertyName)
    {
      if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
    }

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1026:DefaultParametersShouldNotBeUsed"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1045:DoNotPassTypesByReference", MessageId = "0#")]
    protected virtual bool SetProperty<T>(ref T field, T value, [System.Runtime.CompilerServices.CallerMemberName] string propertyName = null)
    {
      if (typeof(T).IsValueType && Object.Equals(field, value)) return false;
      else if ((object)field == (object)value) return false;

      field = value;
      this.OnPropertyChanged(propertyName);

      return true;
    }

    #endregion

    #region IDisposable

    public void Dispose()
    {
      Dispose(true);
      GC.SuppressFinalize(this);
    }

    protected virtual void Dispose(bool disposing)
    {
      if (IsDisposed) return;
      IsDisposed = true;

      if (disposing)
      {
        if (Parent != null)
        {
          Parent.RemoveChildController(this);
        }

        foreach (var c in ChildControllers)
        {
          c.Dispose();
        }

        Container.Dispose();
      }
    }

    #endregion
  }
}

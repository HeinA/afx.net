﻿using Microsoft.Practices.Prism.PubSubEvents;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.Controllers
{
  public interface IApplicationController
  {
    IEventAggregator EventAggregator { get; }
    IEnumerable<IModuleController> Modules { get; }

    IModuleController GetModuleController(string ns);
    IModuleController GetModuleController(Assembly assembly);
  }
}

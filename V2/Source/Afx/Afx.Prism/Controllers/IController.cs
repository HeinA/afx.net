﻿using Microsoft.Practices.ServiceLocation;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.Controllers
{
  public interface IController : IDisposable, INotifyPropertyChanged, INotifyPropertyChanging
  {
    Guid Id { get; set; }
    IController Parent { get; }
    IEnumerable<IController> ChildControllers { get; }
    T CreateChildController<T>() where T : class, IController;
    void Initialize();
  }
}

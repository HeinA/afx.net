﻿using Afx.Prism.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Prism.PubSubEvents;

namespace Afx.Prism.Controllers
{
  public abstract class DialogController : Controller
  {
    public IRegionManager RegionManager { get; private set; }

    DialogViewModel mDialogViewModel;

    #region string Error

    public const string ErrorProperty = "Error";
    string mError;
    public string Error
    {
      get { return mError; }
      set
      {
        if (SetProperty<string>(ref mError, value))
        {
          NotifyViewModelOfError();
        }
      }
    }

    void NotifyViewModelOfError()
    {
      if (mDialogViewModel == null) return;
      mDialogViewModel.OnPropertyChanged(DialogViewModel.ErrorProperty);
      mDialogViewModel.OnPropertyChanged(DialogViewModel.ErrorVisibilityProperty);
    }

    #endregion

    #region bool Validate()

    public virtual bool Validate()
    {
      return true;
    }

    #endregion


    protected abstract DialogViewModel ResolveDialogViewModel();

    #region ApplicationController ApplicationController

    ApplicationController mApplicationController;
    protected ApplicationController ApplicationController
    {
      get { return mApplicationController ?? (mApplicationController = Container.Resolve<ApplicationController>()); }
    }

    #endregion

    #region void ConfigureContainer(...)

    protected internal override void ConfigureContainer()
    {
      base.ConfigureContainer();

      IRegionManager rm = Container.Resolve<IRegionManager>();
      RegionManager = rm.CreateRegionManager();
      Container.RegisterInstance<IRegionManager>(RegionManager);

      EventAggregator = new EventAggregator();
      Container.RegisterInstance<IEventAggregator>(EventAggregator);

      Container.RegisterInstance<DialogController>(this);
      Container.RegisterInstance(this.GetType(), this);
    }

    #endregion

    #region void OnInitialized(...)

    protected override void OnInitialized()
    {
      base.OnInitialized();
      
      mDialogViewModel = ResolveDialogViewModel();
      if (mDialogViewModel == null) throw new InvalidOperationException(Properties.Resources.NoDialogViewModel);
      Container.RegisterInstance<DialogViewModel>(mDialogViewModel);

      ApplicationController.ShowDialog(mDialogViewModel);
    }

    #endregion

    #region void OnApply()

    protected internal virtual void OnApply()
    {
    }

    #endregion

    #region void Dispose(...)

    protected override void Dispose(bool disposing)
    {
      if (disposing)
      {
        ApplicationController.CloseDialog(mDialogViewModel);
        mDialogViewModel.Dispose();
      }

      base.Dispose(disposing);
    }

    #endregion
  }
}

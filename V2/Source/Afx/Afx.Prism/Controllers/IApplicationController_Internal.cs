﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.Controllers
{
  internal interface IApplicationController_Internal : IApplicationController
  {
    void AddModule(string ns, IModuleController module);
  }
}

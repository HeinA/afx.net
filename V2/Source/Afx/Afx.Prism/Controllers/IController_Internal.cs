﻿using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.Controllers
{
  internal interface IController_Internal : IController
  {
    void SetContainer(IUnityContainer container);
    void RegisterChildController(IController controller);
    void RemoveChildController(IController controller);
    void ConfigureContainer();
    IUnityContainer Container { get; }
  }
}

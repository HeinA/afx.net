﻿using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.ObjectBuilder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.ObjectBuilder2
{
  class AfxBuildExtension : UnityContainerExtension
  {
    protected override void Initialize()
    {
      Context.Strategies.AddNew<AfxBuildStrategy>(UnityBuildStage.Creation);
    }
  }
}

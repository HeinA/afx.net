﻿using Microsoft.Practices.ObjectBuilder2;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Prism.UnityExtensions;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Afx.Prism.Controllers;
using Afx.Prism.ViewModels;

namespace Afx.Prism.ObjectBuilder2
{
  class AfxBuildStrategy : BuilderStrategy
  {
    #region void PreBuildUp(...)

    public override void PreBuildUp(IBuilderContext context)
    {
      if (context == null) throw new ArgumentNullException("context");

      //Get the resolving Unity container
      ILifetimePolicy lifetime = context.Policies.Get<ILifetimePolicy>(NamedTypeBuildKey.Make<IUnityContainer>());
      IUnityContainer container = (IUnityContainer)lifetime.GetValue();

      Controller controller = context.Existing as Controller;
      if (controller != null)
      {
        ApplicationController ac = context.Existing as ApplicationController;
        if (ac == null)
        {
          // Create a child container for the controller
          container = container.CreateChildContainer();
        }

        // Apply the container to the controller
        controller.Container = container;
        controller.ConfigureContainer();
      }

      ViewModel vm = context.Existing as ViewModel;
      if (vm != null)
      {
        vm.Container = container; 
      }

      base.PreBuildUp(context);
    }

    #endregion

    #region void PostBuildUp(...)

    public override void PostBuildUp(IBuilderContext context)
    {
      if (context == null) throw new ArgumentNullException("context");

      Controller controller = context.Existing as Controller;
      if (controller != null)
      {
        if (controller.Parent != null)
        {
          controller.Parent.AddChildController(controller);
        }
      }

      base.PostBuildUp(context);
    }

    #endregion
  }
}

﻿using Afx.Prism.Controllers;
using Afx.Prism.ObjectBuilder2;
using Afx.Prism.Regions;
using Afx.Prism.Regions.Behaviors;
using Microsoft.Practices.Prism.Modularity;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Prism.UnityExtensions;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Xceed.Wpf.Toolkit.Primitives;
using Afx.Prism.Utilities;

namespace Afx.Prism
{
  public abstract class AfxBootstrapper : UnityBootstrapper
  {
    protected override void ConfigureModuleCatalog()
    {
      foreach (IModuleInfo mi in CompositionHelper.GetExportedValues<IModuleInfo>())
      {
        if (mi.GetModuleDependencies() == null)
        {
          ModuleCatalog.AddModule(new ModuleInfo(mi.ModuleNamespace, TypeHelper.GetTypeName(mi.ModuleController)));
        }
        else
        {
          ModuleCatalog.AddModule(new ModuleInfo(mi.ModuleNamespace, TypeHelper.GetTypeName(mi.ModuleController), mi.GetModuleDependencies()));
        }
      }

      base.ConfigureModuleCatalog();
    }

    protected override void InitializeModules()
    {
      base.InitializeModules();

      foreach (ResourceClass rc in Enum.GetValues(typeof(ResourceClass)))
      {
        ComposeResources(rc);
      }
    }

    protected override void ConfigureContainer()
    {
      base.ConfigureContainer();

      Container.AddNewExtension<BuilderAwareExtension>();
      Container.AddNewExtension<AfxBuildExtension>();
    }

    protected override RegionAdapterMappings ConfigureRegionAdapterMappings()
    {
      RegionAdapterMappings mappings = base.ConfigureRegionAdapterMappings();
      mappings.RegisterMapping(typeof(WindowContainer), Container.Resolve<WindowContainerRegionAdapter>());
      //mappings.RegisterMapping(typeof(DockPanel), Container.Resolve<DockPanelRegionAdapter>());
      return mappings;
    }

    protected override IRegionBehaviorFactory ConfigureDefaultRegionBehaviors()
    {
      IRegionBehaviorFactory factory = base.ConfigureDefaultRegionBehaviors();
      factory.AddIfMissing(RegionUnloadedBehavior.BehaviorKey, typeof(RegionUnloadedBehavior));
      return factory;
    }

    protected virtual void GetResourceInfoCollection(Collection<IResourceInfo> infoCollection)
    {
    }

    #region void ComposeResources(...)

    void ComposeResources(ResourceClass resourceClass)
    {
      Window owner = Window.GetWindow(Shell);
      owner.BeginInit();

      Collection<IResourceInfo> hardInfos = new Collection<IResourceInfo>();
      GetResourceInfoCollection(hardInfos);

      IEnumerable<IResourceInfo> resourceInfos = CompositionHelper.GetExportedValues<IResourceInfo>().Union(hardInfos);

      Stack<Assembly> stack = new Stack<Assembly>();
      Type t = this.GetType();
      {
        while (t != typeof(UnityBootstrapper))
        {
          stack.Push(t.Assembly);
          t = t.BaseType;
        }
      }

      while (stack.Count > 0)
      {
        ComposeResources(resourceInfos, stack.Pop(), resourceClass);
      }

      ApplicationController ac = Container.Resolve<ApplicationController>();

      foreach (ModuleController mc in ac.ChildControllers)
      {
        ComposeResources(resourceInfos, mc.GetType().Assembly, resourceClass);
      }

      owner.EndInit();
    }

    static void ComposeResources(IEnumerable<IResourceInfo> resourceInfos, Assembly assembly, ResourceClass resourceClass)
    {
      foreach (IResourceInfo ri in resourceInfos.Where(ri1 => ri1.GetType().Assembly.Equals(assembly) && ri1.ResourceClass == resourceClass).OrderBy(ri1 => ri1.Priority))
      {
        foreach (Uri uri in ri.GetUriCollection())
        {
          ResourceDictionary dict = new ResourceDictionary();
          dict.Source = uri;
          Application.Current.Resources.MergedDictionaries.Add(dict);
        }
      }
    }

    #endregion
  }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.Utilities
{
  public static class TypeHelper
  {
    public static string GetTypeName(Type type)
    {
      if (type == null) throw new ArgumentNullException("type");

      return string.Format("{0}, {1}", type.FullName, type.Assembly.GetName().Name);
    }
  }
}

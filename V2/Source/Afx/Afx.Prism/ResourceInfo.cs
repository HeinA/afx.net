﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism
{
  public class ResourceInfo : IResourceInfo
  {
    public ResourceInfo(Collection<Uri> uriCollection, ResourceClass resourceClass, decimal priority)
    {
      mUriCollection = uriCollection;
      mClass = resourceClass;
      mPriority = priority;
    }

    Collection<Uri> mUriCollection;
    public Uri[] GetUriCollection()
    {
      return mUriCollection.ToArray();
    }

    ResourceClass mClass;
    public ResourceClass ResourceClass
    {
      get { return mClass; }
    }

    decimal mPriority;
    public decimal Priority
    {
      get { return mPriority; }
    }
  }
}

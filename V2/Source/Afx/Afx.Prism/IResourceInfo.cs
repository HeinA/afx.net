﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism
{
  public interface IResourceInfo
  {
    Uri[] GetUriCollection();
    ResourceClass ResourceClass { get; }
    decimal Priority { get; }
  }
}

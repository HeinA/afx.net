﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism
{
  public enum ResourceClass
  {
    GlobalResource = 0
    , Dialog = 1
    , Extension = 2
    , View = 3
  }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.ComponentModel
{
  public class PropertyChangedEventArgs<T> : PropertyChangedEventArgs
  {
    public T PreviousValue { get; private set; }
    public T CurrentValue { get; private set; }

    public PropertyChangedEventArgs(string propertyName, T previousValue, T currentValue)
      : base(propertyName)
    {
      this.PreviousValue = previousValue;
      this.CurrentValue = currentValue;
    }
  }
}

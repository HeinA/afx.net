﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition.Hosting;
using System.ComponentModel.Composition.Primitives;
using System.ComponentModel.Composition.ReflectionModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Afx.Desktop.Utilities
{
  public static class CompositionHelper
  {
    static CompositionHelper()
    {
      string s = null;
      if (!string.IsNullOrWhiteSpace(HttpRuntime.AppDomainAppVirtualPath))
      {
        s = HttpRuntime.BinDirectory;
      }
      else
      {
        s = AppDomain.CurrentDomain.BaseDirectory;
      }
      DirectoryCatalog dc1 = new DirectoryCatalog(s);
      AggregateCatalog ag = new AggregateCatalog(dc1);

      string folder = string.Format("{0}{1}", Environment.GetFolderPath(System.Environment.SpecialFolder.ApplicationData), @"\Afx.net");
      DirectoryInfo di = new DirectoryInfo(folder);
      if (di.Exists)
      {
        DirectoryCatalog dc2 = new DirectoryCatalog(folder);
        ag.Catalogs.Add(dc2);
      }

      mCompositionContainer = new CompositionContainer(ag, true);
    }

    static CompositionContainer mCompositionContainer;

    public static IEnumerable<T> GetExportedValues<T>()
    {
      return mCompositionContainer.GetExportedValues<T>();
    }

    public static T GetExportedValue<T>(string contractName)
    {
      return mCompositionContainer.GetExportedValue<T>(contractName);
    }

    public static IEnumerable<Type> GetExportedTypes<T>()
    {
      return mCompositionContainer.Catalog.Parts
          .Select(part => ComposablePartExportType<T>(part))
          .Where(t => t != null);
    }

    public static IEnumerable<Type> GetExportedTypes(Type t)
    {
      return mCompositionContainer.Catalog.Parts
          .Select(part => ComposablePartExportType(t, part))
          .Where(t1 => t1 != null);
    }

    public static Type GetExportedType(string contractName)
    {
      ComposablePartDefinition part = mCompositionContainer.Catalog.Parts.FirstOrDefault(p => p.ExportDefinitions.Any(ed => ed.ContractName == contractName));
      if (part == null) return null;
      return ReflectionModelServices.GetPartType(part).Value;
    }

    private static Type ComposablePartExportType(Type t, ComposablePartDefinition part)
    {
      string identity = t.FullName;
      if (t.IsGenericType)
      {
        identity = t.GetGenericTypeDefinition().FullName.Replace("`1", string.Empty);
        identity += "(";
        bool bIsFirst = true;
        foreach (Type tt in t.GetGenericArguments())
        {
          identity += string.Format("{0}{1}", tt.FullName, bIsFirst ? string.Empty : ",");
        }
        identity += ")";
      }

      if (part.ExportDefinitions.Any(
          def => def.Metadata.ContainsKey("ExportTypeIdentity") &&
              def.Metadata["ExportTypeIdentity"].Equals(identity)))
      {
        return ReflectionModelServices.GetPartType(part).Value;
      }
      return null;
    }

    private static Type ComposablePartExportType<T>(ComposablePartDefinition part)
    {

      if (part.ExportDefinitions.Any(
          def => def.Metadata.ContainsKey("ExportTypeIdentity") &&
              def.Metadata["ExportTypeIdentity"].Equals(typeof(T).FullName)))
      {
        return ReflectionModelServices.GetPartType(part).Value;
      }
      return null;
    }

    private static Type ComposablePartExportType<T>(ComposablePartDefinition part, string name)
    {

      if (part.ExportDefinitions.Any(
          def => def.Metadata.ContainsKey("ExportTypeIdentity")
            && def.Metadata["ExportTypeIdentity"].Equals(typeof(T).FullName)))
      {
        return ReflectionModelServices.GetPartType(part).Value;
      }
      return null;
    }
  }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Desktop.Utilities
{
  public static class TypeHelper
  {
    public static string GetTypeName(Type type)
    {
      return string.Format("{0}, {1}", type.FullName, type.Assembly.GetName().Name);
    }
  }
}

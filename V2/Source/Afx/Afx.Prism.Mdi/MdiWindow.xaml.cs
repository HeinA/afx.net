﻿using Afx.Prism.Mdi.Controllers;
using Afx.Prism.Mdi.ViewModels;
using Microsoft.Practices.ServiceLocation;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Afx.Prism.Mdi
{
  /// <summary>
  /// Interaction logic for MainWindow.xaml
  /// </summary>
  partial class MdiWindow : Fluent.RibbonWindow
  {
    public MdiWindow()
    {
      InitializeComponent();
    }

    MdiWindowViewModel mViewModel;
    [Dependency]
    public MdiWindowViewModel ViewModel
    {
      get { return mViewModel; }
      set
      {
        mViewModel = value;
        this.DataContext = value;
      }
    }
  }
}

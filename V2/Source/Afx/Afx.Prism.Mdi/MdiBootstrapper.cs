﻿using Microsoft.Practices.Prism.UnityExtensions;
using System;
using System.Collections.Generic;
using System.Linq;  
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Microsoft.Practices.Unity;
using Afx.Prism.Controllers;
using Afx.Prism.Mdi.ViewModels;
using Afx.Prism.Mdi.Controllers;
using System.ComponentModel.Composition;
using System.Collections.ObjectModel;
using Fluent;
using System.Security.Principal;

namespace Afx.Prism.Mdi
{
  public abstract class MdiBootstrapper : AfxBootstrapper
  {
    MdiWindow mMainWindow;
    MdiApplicationController mApplicationController;

    protected override DependencyObject CreateShell()
    {
      mApplicationController = this.Container.Resolve<MdiApplicationController>();

      MdiWindowViewModel vm = Container.Resolve<MdiWindowViewModel>();
      Container.RegisterInstance<MdiWindowViewModel>(vm);

      mMainWindow = Container.Resolve<MdiWindow>();
      Container.RegisterInstance<MdiWindow>(mMainWindow);
      return mMainWindow;
    }

    protected override void GetResourceInfoCollection(Collection<IResourceInfo> infoCollection)
    {
      if (infoCollection == null) throw new ArgumentNullException("infoCollection");

      base.GetResourceInfoCollection(infoCollection);

      //infoCollection.Add(new Afx.Prism.ResourceInfo(RibbonThemeUriCollection, ResourceClass.GlobalResource, 0));
    }

    protected virtual Collection<Uri> RibbonThemeUriCollection
    {
      get
      {
        Collection<Uri> uris = new Collection<Uri>();
        //uris.Add(new Uri("pack://application:,,,/Fluent;component/Themes/Generic.xaml"));
        //uris.Add(new Uri("pack://application:,,,/Fluent;component/Themes/Office2010/Silver.xaml"));
        uris.Add(new Uri("pack://application:,,,/Fluent;Component/Themes/Windows8/Generic.xaml"));
        return uris;
      }
    }

    protected override void InitializeModules()
    {
      base.InitializeModules();

      mApplicationController.ComposeRibbonTabs();
      mMainWindow.Show();

      var id = WindowsIdentity.GetCurrent();
    }
  }
}

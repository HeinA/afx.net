﻿using Afx.Prism.Mdi.Ribbon;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.Mdi.ViewModels
{
  public abstract class RibbonItemViewModel : IRibbonItem
  {
    public abstract string TabName { get; }
    public abstract string GroupName { get; }
    public abstract string ItemIdentifier { get; }

    public virtual int Index
    {
      get { return 0; }
    }
  }
}

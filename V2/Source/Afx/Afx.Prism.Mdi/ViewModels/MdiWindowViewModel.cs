﻿using Afx.Prism.Mdi.Controllers;
using Afx.Prism.Mdi.Ribbon;
using Afx.Prism.ViewModels;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.Mdi.ViewModels
{
  public class MdiWindowViewModel : ViewModel
  {
    [Dependency]
    public MdiApplicationController ApplicationController { get; set; }

    #region IEnumerable<IRibbonTab> RibbonTabs

    public IEnumerable<IRibbonTab> RibbonTabs
    {
      get { return ApplicationController.RibbonTabs; }
    }

    #endregion

    #region bool IsRibbonEnabled

    public const string IsRibbonEnabledProperty = "IsRibbonEnabled";
    bool mIsRibbonEnabled = true;
    public bool IsRibbonEnabled
    {
      get { return mIsRibbonEnabled; }
      set { SetProperty<bool>(ref mIsRibbonEnabled, value); }
    }

    #endregion

    #region IEnumerable<UseCaseViewModel> UseCaseViewModels

    public IEnumerable<UseCaseViewModel> UseCaseViewModels
    {
      get { return ApplicationController.UseCaseViewModels; }
    }

    #endregion

    #region void OnLoaded()

    protected internal override void OnLoaded()
    {
      base.OnLoaded();

      ApplicationController.RaiseOnLoaded();
    }

    #endregion
  }
}

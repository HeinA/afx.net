﻿using Afx.Prism.Mdi.Ribbon;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.Mdi.ViewModels
{
  public interface IMdiViewModel
  {
    IEnumerable<IRibbonTab> RibbonTabs { get; }
  }
}

﻿using Afx.Prism.Mdi.Controllers;
using Afx.Prism.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Regions;
using System.Windows;
using System.Globalization;

namespace Afx.Prism.Mdi.ViewModels
{
  public abstract class UseCaseViewModel : ViewModel
  {
    #region IRegionManager RegionManager

    IRegionManager mRegionManager;
    public IRegionManager RegionManager
    {
      get { return mRegionManager ?? (mRegionManager = Container.Resolve<IRegionManager>()); }
    }

    #endregion

    #region bool IsActive

    public const string IsActiveProperty = "IsActive";
    bool mIsActive;
    public bool IsActive
    {
      get { return mIsActive; }
      set
      {
        if (SetProperty<bool>(ref mIsActive, value))
        {
          if (IsActive)
          {
            ApplicationController.ActiveUseCase = UseCaseController;
          }
        }
      }
    }

    #endregion

    #region string Title

    public const string TitleProperty = "Title";
    string mTitle = "Use Case";
    public string Title
    {
      get { return mTitle; }
      set
      {
        if (SetProperty<string>(ref mTitle, value))
        {
          OnPropertyChanged(FormattedTitleProperty);
        }
      }
    }

    #endregion

    #region string FormattedTitle

    public const string FormattedTitleProperty = "FormattedTitle";
    public string FormattedTitle
    {
      get { return UseCaseController.IsDirty ? string.Format(CultureInfo.InvariantCulture, "*{0}", Title) : Title; }
    }

    #endregion

    #region string Error

    public const string ErrorProperty = "Error";
    public string Error
    {
      get { return UseCaseController.Error; }
    }

    #endregion

    #region Visibility ErrorVisibility

    public const string ErrorVisibilityProperty = "ErrorVisibility";
    public Visibility ErrorVisibility
    {
      get { return string.IsNullOrWhiteSpace(Error) ? Visibility.Collapsed : Visibility.Visible; }
    }

    #endregion

    #region DelegateCommand CloseCommand

    DelegateCommand mCloseCommand;
    public DelegateCommand CloseCommand
    {
      get { return mCloseCommand ?? (mCloseCommand = new DelegateCommand(ExecuteClose)); }
    }

    void ExecuteClose()
    {
      UseCaseController.Dispose();
    }

    #endregion


    #region MdiApplicationController ApplicationController

    MdiApplicationController mApplicationController;
    protected MdiApplicationController ApplicationController
    {
      get { return mApplicationController ?? (mApplicationController = Container.Resolve<MdiApplicationController>()); }
    }

    #endregion

    #region void OnLoaded()

    protected internal override void OnLoaded()
    {
      base.OnLoaded();

      UseCaseController.RaiseOnLoaded();
    }

    #endregion


    #region UseCaseController UseCaseController

    UseCaseController mUseCaseController;
    UseCaseController UseCaseController
    {
      get { return mUseCaseController ?? (mUseCaseController = Container.Resolve<UseCaseController>()); }
    }

    #endregion
  }
}

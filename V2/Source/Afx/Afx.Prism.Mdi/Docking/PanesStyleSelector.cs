﻿using Afx.Prism.Mdi.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace Afx.Prism.Mdi.Docking
{
  [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1812:AvoidUninstantiatedInternalClasses")]
  class PanesStyleSelector : StyleSelector
  {
    public override Style SelectStyle(object item, DependencyObject container)
    {
      if (item == null) throw new ArgumentNullException("item");
      if (container == null) throw new ArgumentNullException("container");

      if (typeof(UseCaseViewModel).IsAssignableFrom(item.GetType())) return (Style)Application.Current.FindResource("Afx.Prism.Mdi.Docking.UseCase");
      //if (typeof(IMdiDockableToolViewModel).IsAssignableFrom(item.GetType())) return (Style)Application.Current.FindResource("Tool");
      return base.SelectStyle(item, container);
    }
  }
}

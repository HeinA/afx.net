﻿using Afx.Prism.Controllers;
using Afx.Prism.Mdi.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Prism.PubSubEvents;

namespace Afx.Prism.Mdi.Controllers
{
  public abstract class UseCaseController : Controller
  {
    public IRegionManager RegionManager { get; private set; }

    UseCaseViewModel mUseCaseViewModel;

    #region string Error

    public const string ErrorProperty = "Error";
    string mError;
    public string Error
    {
      get { return mError; }
      set
      {
        if (SetProperty<string>(ref mError, value))
        {
          NotifyViewModelOfError();
        }
      }
    }

    void NotifyViewModelOfError()
    {
      if (mUseCaseViewModel == null) return;
      mUseCaseViewModel.OnPropertyChanged(UseCaseViewModel.ErrorProperty);
      mUseCaseViewModel.OnPropertyChanged(UseCaseViewModel.ErrorVisibilityProperty);
    }

    #endregion

    #region bool Validate()

    public virtual bool Validate()
    {
      return true;
    }

    #endregion

    #region bool IsDirty

    public const string IsDirtyProperty = "IsDirty";
    bool mIsDirty;
    public bool IsDirty
    {
      get { return mIsDirty; }
      set
      {
        if (SetProperty<bool>(ref mIsDirty, value))
        {
          mUseCaseViewModel.OnPropertyChanged(UseCaseViewModel.FormattedTitleProperty);
        }
      }
    }

    #endregion


    protected abstract UseCaseViewModel ResolveUseCaseViewModel();

    #region MdiApplicationController ApplicationController

    MdiApplicationController mApplicationController;
    protected MdiApplicationController ApplicationController
    {
      get { return mApplicationController ?? (mApplicationController = Container.Resolve<MdiApplicationController>()); }
    }

    #endregion

    #region void ConfigureContainer(...)

    protected internal override void ConfigureContainer()
    {
      base.ConfigureContainer();

      IRegionManager rm = Container.Resolve<IRegionManager>();
      RegionManager = rm.CreateRegionManager();
      Container.RegisterInstance<IRegionManager>(RegionManager);

      EventAggregator = new EventAggregator();
      Container.RegisterInstance<IEventAggregator>(EventAggregator);

      Container.RegisterInstance<UseCaseController>(this);
      Container.RegisterInstance(this.GetType(), this);
    }

    #endregion

    #region void OnInitialized(...)

    protected override void OnInitialized()
    {
      base.OnInitialized();

      mUseCaseViewModel = ResolveUseCaseViewModel();
      if (mUseCaseViewModel == null) throw new InvalidOperationException(Properties.Resources.NoUseCaseViewModel);
      Container.RegisterInstance<UseCaseViewModel>(mUseCaseViewModel);

      ApplicationController.AddUseCase(mUseCaseViewModel);
      Activate();
    }

    #endregion

    #region void OnActivate(...)

    protected override void OnActivate()
    {
      base.OnActivate();

      mUseCaseViewModel.IsActive = true;
    }

    #endregion

    #region void Dispose(...)

    protected override void Dispose(bool disposing)
    {
      if (disposing)
      {
        ApplicationController.RemoveUseCase(Container.Resolve<UseCaseViewModel>());
        mUseCaseViewModel.Dispose();
      }

      base.Dispose(disposing);
    }

    #endregion
  }
}

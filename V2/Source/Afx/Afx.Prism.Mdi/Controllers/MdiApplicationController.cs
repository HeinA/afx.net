﻿using Afx.Prism.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Practices.Unity;
using Afx.Prism.Mdi.Ribbon;
using System.Collections.ObjectModel;
using System.Reflection;
using Afx.Prism.Mdi.ViewModels;
using System.Globalization;
using System.Windows.Threading;
using System.Windows;
using Afx.Prism.Utilities;

namespace Afx.Prism.Mdi.Controllers
{
  public sealed class MdiApplicationController : ApplicationController
  {
    public const string DialogRegion = "Afx.Prism.Mdi.Controllers.MdiApplicationController.DialogRegion";

    MdiWindowViewModel mWindowViewModel;
    public MdiWindowViewModel WindowViewModel
    {
      get { return mWindowViewModel ?? (mWindowViewModel = Container.Resolve<MdiWindowViewModel>()); }
    }

    #region UseCaseController ActiveUseCase

    public const string ActiveUseCaseProperty = "ActiveUseCase";
    UseCaseController mActiveUseCase;
    public UseCaseController ActiveUseCase
    {
      get { return mActiveUseCase; }
      set
      {
        if (SetProperty<UseCaseController>(ref mActiveUseCase, value))
        {
          ActiveUseCase.Activate();
        }
      }
    }

    #endregion

    #region IEnumerable<IRibbonTab> RibbonTabs

    Collection<IRibbonTab> mRibbonTabs = new Collection<IRibbonTab>();
    public IEnumerable<IRibbonTab> RibbonTabs
    {
      get { return mRibbonTabs; }
    }

    #endregion

    #region IRibbonTab GetRibbonTab(...)

    public IRibbonTab GetRibbonTab(string name)
    {
      return RibbonTabs.FirstOrDefault(rt => rt.Name == name);
    }

    #endregion

    #region IEnumerable<UseCaseViewModel> UseCaseViewModels

    ObservableCollection<UseCaseViewModel> mUseCaseViewModels = new ObservableCollection<UseCaseViewModel>();
    public IEnumerable<UseCaseViewModel> UseCaseViewModels
    {
      get { return mUseCaseViewModels; }
    }

    #endregion

    #region void ConfigureContainer(...)

    internal protected override void ConfigureContainer()
    {
      base.ConfigureContainer();

      Container.RegisterInstance<MdiApplicationController>(this);
    }

    #endregion

    #region Dialogs

    protected override void OnShowDialog(Prism.ViewModels.DialogViewModel vm)
    {
      WindowViewModel.IsRibbonEnabled = DialogCount == 0;
      RegionManager.Regions[DialogRegion].Add(vm);
    }

    protected override void OnCloseDialog(Prism.ViewModels.DialogViewModel vm)
    {
      RegionManager.Regions[DialogRegion].Remove(vm);
      WindowViewModel.IsRibbonEnabled = DialogCount == 0;
    }

    #endregion

    #region UseCases

    internal void AddUseCase(UseCaseViewModel viewModel)
    {
      mUseCaseViewModels.Add(viewModel);
    }


    internal void RemoveUseCase(UseCaseViewModel viewModel)
    {
      if (Application.Current == null) return;
      mUseCaseViewModels.Remove(viewModel);
    }

    #endregion

    #region void ComposeRibbonTabs(...)

    internal void ComposeRibbonTabs()
    {
      IRibbonTab rtHome = Container.Resolve<HomeTab>();
      Container.RegisterInstance<IRibbonTab>(rtHome.Name, rtHome);

      IRibbonTab rtAdministration = Container.Resolve<AdministrationTab>();
      Container.RegisterInstance<IRibbonTab>(rtAdministration.Name, rtAdministration);

      //Add the Home tab first
      mRibbonTabs.Add(rtHome);

      //Get RibbonTabTypes for each module in module load order
      IEnumerable<Type> moduleTabTypes = CompositionHelper.GetExportedTypes(typeof(IRibbonTab));
      foreach (ModuleController mc in this.ChildControllers)
      {
        Collection<IRibbonTab> moduleTabs = new Collection<IRibbonTab>();
        foreach (Type t in moduleTabTypes.Where(t1 => t1.Assembly.Equals(mc.GetType().Assembly)))
        {
          IRibbonTab rt = (IRibbonTab)Container.Resolve(t);

          if (Container.IsRegistered<IRibbonTab>(rt.Name))
          {
            throw new InvalidOperationException(string.Format(CultureInfo.InvariantCulture, Properties.Resources.RibbonNameAlreadyRegistered, rt.Name));
          }
          moduleTabs.Add(rt);
          Container.RegisterInstance<IRibbonTab>(rt.Name, rt);
        }

        //Add each module's tabs in their indexed order
        foreach (IRibbonTab rt1 in moduleTabs.OrderBy(rt1 => rt1.Index))
        {
          mRibbonTabs.Add(rt1);
        }
      }

      //Add the Administration tab last
      mRibbonTabs.Add(rtAdministration);

      //Add the ribbon tab groups
      IEnumerable<Type> groupTypes = CompositionHelper.GetExportedTypes(typeof(IRibbonGroup));
      AddRibbonGroups(groupTypes.Where(t1 => t1.Assembly.Equals(typeof(MdiApplicationController).Assembly)));
      foreach (ModuleController mc in this.ChildControllers)
      {
        AddRibbonGroups(groupTypes.Where(t1 => t1.Assembly.Equals(mc.GetType().Assembly)));
      }

      //Add the ribbon items
      IEnumerable<Type> itemTypes = CompositionHelper.GetExportedTypes(typeof(IRibbonItem));
      AddRibbonItems(itemTypes.Where(t1 => t1.Assembly.Equals(typeof(MdiApplicationController).Assembly)));
      foreach (ModuleController mc in this.ChildControllers)
      {
        AddRibbonItems(itemTypes.Where(t1 => t1.Assembly.Equals(mc.GetType().Assembly)));
      }
    }

    void AddRibbonGroups(IEnumerable<Type> moduleGroupTypes)
    {
      Collection<IRibbonGroup> tabGroups = new Collection<IRibbonGroup>();
      foreach (Type t in moduleGroupTypes)
      {
        IRibbonGroup rg = (IRibbonGroup)Container.Resolve(t);
        IRibbonTab ribbonTab = GetRibbonTab(rg.TabName);

        string key = string.Format(CultureInfo.InvariantCulture, "{0}.{1}", rg.TabName, rg.Name);
        if (Container.IsRegistered<IRibbonGroup>(key))
        {
          throw new InvalidOperationException(string.Format(CultureInfo.InvariantCulture, Properties.Resources.RibbonGroupNameAlreadyRegistered, rg.Name, rg.TabName));
        }
        tabGroups.Add(rg);
        Container.RegisterInstance<IRibbonGroup>(key, rg);
      }

      //Add each module's groups to their tabs in their indexed order
      foreach (IRibbonGroup rg in tabGroups.OrderBy(rg1 => rg1.Index))
      {
        IRibbonTab ribbonTab = GetRibbonTab(rg.TabName);
        ribbonTab.AddGroup(rg);
      }
    }

    void AddRibbonItems(IEnumerable<Type> itemTypes)
    {
      Collection<IRibbonItem> groupsItems = new Collection<IRibbonItem>();
      foreach (Type t in itemTypes)
      {
        ModuleController mc = (ModuleController)GetModuleController(t.Assembly);

        IRibbonItem ri = (IRibbonItem)mc.Container.Resolve(t);
        IRibbonTab ribbonTab = GetRibbonTab(ri.TabName);
        IRibbonGroup ribbonGroup = ribbonTab.GetGroup(ri.GroupName);

        if (mc.Container.IsRegistered<IRibbonItem>(ri.ItemIdentifier))
        {
          throw new InvalidOperationException(string.Format(CultureInfo.InvariantCulture, Properties.Resources.RibbonItemAlreadyRegistered, ri.ItemIdentifier, ribbonGroup.Name, ribbonGroup.TabName));
        }
        groupsItems.Add(ri);
        mc.Container.RegisterInstance<IRibbonItem>(ri.ItemIdentifier, ri);
      }

      //Add each module's items to their groups in their indexed order
      foreach (IRibbonItem ri in groupsItems.OrderBy(ri1 => ri1.Index))
      {
        IRibbonTab ribbonTab = GetRibbonTab(ri.TabName);
        IRibbonGroup ribbonGroup = ribbonTab.GetGroup(ri.GroupName);
        ribbonGroup.AddItem(ri);
      }
    }

    #endregion
  }
}

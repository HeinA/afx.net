﻿using Afx.Prism.Controllers;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.Mdi.Controllers
{
  internal class ControllerFactory
  {
    public static MdiApplicationController CreateApplicationController(IUnityContainer container)
    {
      MdiApplicationController c = new MdiApplicationController();
      container.BuildUp<MdiApplicationController>(c);
      return c;
    }
  }
}

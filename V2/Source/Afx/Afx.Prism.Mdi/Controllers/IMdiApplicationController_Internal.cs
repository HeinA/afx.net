﻿using Afx.Prism.Controllers;
using Afx.Prism.Mdi.Ribbon;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.Mdi.Controllers
{
  internal interface IMdiApplicationController_Internal : IApplicationController_Internal
  {
    void ComposeRibbonTabs();
  }
}

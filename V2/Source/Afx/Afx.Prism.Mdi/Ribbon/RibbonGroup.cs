﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.Mdi.Ribbon
{
  public abstract class RibbonGroup : IRibbonGroup
  {
    public abstract string TabName { get; }
    public abstract string Name { get; }

    public virtual int Index
    {
      get { return 0; }
    }

    #region Items

    ObservableCollection<IRibbonItem> mItems = new ObservableCollection<IRibbonItem>();
    public IEnumerable<IRibbonItem> Items
    {
      get { return mItems; }
    }

    public void AddItem(IRibbonItem item)
    {
      mItems.Add(item);
    }

    #endregion
  }
}

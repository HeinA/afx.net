﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.Mdi.Ribbon
{
  public interface IRibbonGroup
  {
    string TabName { get; }
    string Name { get; }
    int Index { get; }
    IEnumerable<IRibbonItem> Items { get; }

    void AddItem(IRibbonItem item);
  }
}

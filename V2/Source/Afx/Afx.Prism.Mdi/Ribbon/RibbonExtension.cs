﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace Afx.Prism.Mdi.Ribbon
{
  static class RibbonExtension
  {
    public static IEnumerable<IRibbonTab> GetItems(DependencyObject obj)
    {
      return (IEnumerable<IRibbonTab>)obj.GetValue(ItemsProperty);
    }

    public static void SetItems(DependencyObject obj, IEnumerable<IRibbonTab> value)
    {
      obj.SetValue(ItemsProperty, value);
    }

    public static readonly DependencyProperty ItemsProperty = DependencyProperty.RegisterAttached("Items", typeof(IEnumerable<IRibbonTab>), typeof(RibbonExtension), new FrameworkPropertyMetadata(null, OnItemsPropertyChanged));

    private static void OnItemsPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
      Fluent.Ribbon ribbon = (Fluent.Ribbon)d;
      IEnumerable<IRibbonTab> tabs = (IEnumerable<IRibbonTab>)e.NewValue;
      ribbon.Tabs.Clear();
      foreach (IRibbonTab tab in tabs)
      {
        Fluent.RibbonTabItem ti = new Fluent.RibbonTabItem();
        ti.Header = tab.Name;

        foreach (IRibbonGroup group in tab.Groups)
        {
          Fluent.RibbonGroupBox gb = new Fluent.RibbonGroupBox();
          gb.Header = group.Name;

          Binding binding = new Binding();
          binding.Source = group.Items;
          gb.SetBinding(Fluent.RibbonGroupBox.ItemsSourceProperty, binding); 

          ti.Groups.Add(gb);
        }

        ribbon.Tabs.Add(ti);
      }
    }
  }
}

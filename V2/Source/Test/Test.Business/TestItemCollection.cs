﻿using Afx.ComponentModel;
using Afx.ComponentModel.Collections;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test.Business
{
  public class TestItemCollection : BusinessCollection<TestItem>
  {
    public TestItemCollection()
    {
    }

    public TestItemCollection(BusinessObject owner)
      : base(owner)
    {
    }
  }
}

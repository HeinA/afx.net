﻿using Afx.ComponentModel;
using Afx.ServiceModel;
using Afx.ServiceModel.Description;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Test.Business.Service
{
  [ServiceContract(Namespace = "http://openafx.net/Test/Business/")]
  [DefaultServiceBehavior]
  public interface ITestService
  {
    [OperationContract]
    //[SerializeCachedTypes(typeof(TestItem))]
    BusinessObject SaveTestObject(int number, BusinessObject obj);
  }
}

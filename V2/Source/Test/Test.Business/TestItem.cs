﻿using Afx.ComponentModel;
using Afx.ComponentModel.Markup;
using Afx.ComponentModel.Markup.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test.Business
{
  public class TestItem : BusinessObject
  {
    public TestItem()
    {
    }

    public TestItem(BusinessObject owner)
      : base(owner)
    {
    }

    #region string Text

    public const string TextProperty = "Text";
    [DataMember(Name = TextProperty)]
    string mText;
    [Mandatory("Text is mandatory.")]
    public string Text
    {
      get { return mText; }
      set { SetProperty<string>(ref mText, value); }
    }

    #endregion
  }
}

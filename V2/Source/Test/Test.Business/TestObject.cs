﻿using Afx.ComponentModel;
using Afx.ComponentModel.Collections;
using Afx.ComponentModel.Markup;
using Afx.ComponentModel.Markup.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test.Business
{
  public class TestObject : BusinessObject
  {
    #region string Name

    public const string NameProperty = "Name";
    [DataMember(Name = "Name")]
    string mName;
    [Mandatory("Name is mandatory.")]
    public string Name
    {
      get { return mName; }
      set { SetProperty<string>(ref mName, value); }
    }

    #endregion

    [DataMember(Name = "Items")]
    BusinessCollection<TestObject> mItems;
    public BusinessCollection<TestObject> Items
    {
      get { return mItems ?? (mItems = new BusinessCollection<TestObject>(this)); }
    }

    //#region DateTime? Date

    //public const string DateProperty = "Date";
    //[DataMember(Name = "Date")]
    //DateTime? mDate;
    //public DateTime? Date
    //{
    //  get { return mDate; }
    //  set { SetProperty<DateTime?>(ref mDate, value); }
    //}

    //#endregion

    //#region TestItem MainItem

    //public const string MainItemProperty = "MainItem";
    //[DataMember(Name = "MainItem")]
    //TestItem mMainItem;
    //[Mandatory("Main Item is mandatory.")]
    //public TestItem MainItem
    //{
    //  get { return mMainItem; }
    //  set { mMainItem = value; }
    //}

    //#endregion

    //[DataMember(Name = "Items")]
    //TestItemCollection mItems;
    //public TestItemCollection Items
    //{
    //  get { return mItems ?? (mItems = new TestItemCollection(this)); }
    //}
  }
}

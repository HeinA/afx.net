﻿using Afx.Prism.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using Microsoft.Practices.Unity;
using System.Windows;
using Afx.Prism.ViewModels;
using Test.Business.Service;
using Test.Business;
using Afx.ServiceModel;
using Afx.ComponentModel;
using System.Diagnostics;
using System.ComponentModel;

namespace Test.Module.TestDialog
{
  public class TestDialogController : DialogController
  {
    #region TestObject TestObject

    public const string TestObjectProperty = "TestObject";
    TestObject mTestObject;
    public TestObject TestObject
    {
      get { return mTestObject; }
      set { mTestObject = value; }
    }

    #endregion

    protected override DialogViewModel ResolveDialogViewModel()
    {
      return Container.Resolve<TestDialogViewModel>();
    }

    //protected override void OnLoaded()
    //{
    //  base.OnLoaded();

    //  //Dictionary<Guid, Guid> dict = new Dictionary<Guid, Guid>();
    //  //try
    //  //{
    //  //  for (int i = 0; i < 10000; i++)
    //  //  {
    //  //    Guid g = IdGenerator.GetSqlSequentialId();
    //  //    //Debug.WriteLine(g.ToString());
    //  //    dict.Add(g, g);
    //  //  }
    //  //}
    //  //catch
    //  //{
    //  //  throw;
    //  //}

    //  using (var svc = ServiceFactory.GetService<ITestService>())
    //  {
    //    TestObject to = new TestObject() ;
    //    using (new EventStateSuppressor(to))
    //    {
    //      to.Name = "Piet";
    //      to.Date = DateTime.Now;
    //      TestItem ti = new TestItem();
    //      to.MainItem = ti;
    //      to.Items.Add(ti);
    //      TestItem ti1 = new TestItem();
    //      to.Items.Add(ti1);
    //      TestItem ti2 = new TestItem();
    //      to.Items.Add(ti2);

    //      ti.Text = "aaa";
    //      ti1.Text = "bbb";
    //      ti2.Text = "ccc";

    //      to.Items.Remove(ti1);
    //    }

    //    //to.Items[0].Text = "ddd";
    //    //to.Items.Add(ti1);

    //    //to.Items.Clear();
    //    //foreach (var i in to.Items)
    //    //{
    //    //}

    //    to.ObjectEdited += (s,e) =>
    //      {
    //        Debug.WriteLine("Object Edited!");
    //      };

    //    to.Items.ObjectEdited += (s, e) =>
    //    {
    //      Debug.WriteLine("List Item Edited!");
    //    };

    //    //using (new EventStateSuppressor(to.Items))
    //    {
    //      to.Items[1].Text = "111";
    //    }

    //    //((INotifyDataErrorInfo)to).ErrorsChanged += TestDialogController_ErrorsChanged;
    //    //string error = to.Errors;

    //    //svc.Instance.SaveTestObject(3, to);
    //    TestObject = to;
    //  } 

    //  //RegionManager.Regions["TestRegion"].Add(new TextBlock() { Text = "Test Dialog", Margin = new Thickness(6) });
    //}

    void TestDialogController_ErrorsChanged(object sender, DataErrorsChangedEventArgs e)
    {
    }

    public override bool Validate()
    {
      Error = TestObject.Errors;
      return string.IsNullOrWhiteSpace(Error); 
    }

    protected override void OnApply()
    {
      using (var svc = ServiceFactory.GetService<ITestService>())
      {
        object ret = svc.Instance.SaveTestObject(3, TestObject);
      }
      base.OnApply();
    }
  }
}

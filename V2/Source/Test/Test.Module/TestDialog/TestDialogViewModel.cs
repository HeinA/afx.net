﻿using Afx.ComponentModel;
using Afx.Prism.ViewModels;
using Microsoft.Practices.ObjectBuilder2;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using Test.Business;

namespace Test.Module.TestDialog
{
  public class TestDialogViewModel : DialogViewModel
  {
    [Dependency]
    public TestDialogController TestDialogController { get; set; }

    protected override void OnLoaded()
    {
      base.OnLoaded();

      this.Model = TestDialogController.TestObject;
    }

    protected override void Dispose(bool disposing)
    {
      base.Dispose(disposing);
    }
  }
}

﻿using Afx.Prism.Controllers;
using Afx.Prism.Mdi.Controllers;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test.Module
{
  public class ModuleController : Afx.Prism.Controllers.ModuleController
  {
    [Dependency]
    public MdiApplicationController ApplicationController { get; set; }

    public override string ModuleNamespace
    {
      get { return ModuleInfo.Namespace; }
    }

    protected override void ConfigureContainer()
    {
      Uri uri = new Uri("ns://Test.Module");
      Uri uri1 = new Uri(uri, "ModuleController");

      base.ConfigureContainer();
    }
  }
}

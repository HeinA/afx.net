﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.Mdi.ApplicationConfigurationModule.Test
{
  [Export(typeof(IResourceInfo))]
  class ResourceInfo : IResourceInfo
  {
    public Uri[] GetUriCollection()
    {
      return new Uri[] { new Uri("pack://application:,,,/Test.Module;component/TestUseCase/Resources.xaml") };
    }

    public ResourceClass ResourceClass
    {
      get { return ResourceClass.View; }
    }

    public decimal Priority
    {
      get { return 0; }
    }
  }
}

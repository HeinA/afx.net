﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test.Module.TestUseCase
{
  public class TestItem
  {
    #region string Text

    public const string TextProperty = "Text";
    string mText;
    public string Text
    {
      get { return mText; }
      set { mText = value; }
    }

    #endregion
  }
}

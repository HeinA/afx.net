﻿using Afx.Prism.Collections;
using Afx.Prism.Mdi.ViewModels;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace Test.Module.TestUseCase
{
  public class TestUseCaseViewModel : UseCaseViewModel
  {
    Timer mTimer;
    ObservableCollection<TestItem> mItems = new ObservableCollection<TestItem>();

    public TestUseCaseViewModel()
    {
      Title = "Test Use Case";

      mTimer = new Timer(OnTimer, null, 2000, 1);
    }

    ViewModelCollection<TestItemViewModel> mItemViewModels;
    public ViewModelCollection<TestItemViewModel> ItemViewModels
    {
      get { return mItemViewModels ?? (mItemViewModels = new ViewModelCollection<TestItemViewModel>(this)); }
    }

    protected override void OnLoaded()
    {
      //Test ViewModelCollection<T>

      for (int i = 0; i < 100000; i++)
      {
        mItems.Add(new TestItem() { Text = string.Format("Item {0}", i) });
      }

      ItemViewModels.ItemsSource = mItems;

      BackgroundWorker bw = new BackgroundWorker();
      bw.DoWork += (s, e) =>
        {
          Thread.Sleep(500);
        };
      bw.RunWorkerCompleted += (s, e) =>
        {
          bw.Dispose();

          mItems.Move(99, 1);
          mItems[0] = new TestItem() { Text = "Replaced" };
          mItems.Insert(2, new TestItem() { Text = "Inserted" });
          mItems.Add(new TestItem() { Text = "Added" });

          //mItems.Clear();
        };
      bw.RunWorkerAsync();

      base.OnLoaded();
    }

    private void OnTimer(object state)
    {
      try
      {
        Application.Current.Dispatcher.BeginInvoke(new Action(() =>
        {
          if (mItems.Count > 0) mItems.RemoveAt(0);
          else mTimer.Dispose();
        }), System.Windows.Threading.DispatcherPriority.Normal);
      }
      catch
      {
        mTimer.Dispose();
      }
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing)
      {
        ItemViewModels.Dispose();
        mTimer.Dispose();
      }

      base.Dispose(disposing);
    }
  }
}

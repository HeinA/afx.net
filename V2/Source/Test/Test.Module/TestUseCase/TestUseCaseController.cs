﻿using Afx.Prism.Mdi.Controllers;
using Afx.Prism.Mdi.ViewModels;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace Test.Module.TestUseCase
{
  public class TestUseCaseController : UseCaseController
  {
    protected override UseCaseViewModel ResolveUseCaseViewModel()
    {
      return Container.Resolve<TestUseCaseViewModel>();
    }

    //protected override void OnLoaded()
    //{
    //  base.OnLoaded();

    //  RegionManager.Regions["TestRegion"].Add(new TextBlock() { Text = "Test Use Case", Margin = new Thickness(6) });
    //}
  }
}

﻿using Afx.ComponentModel;
using Afx.Prism.Mdi.Ribbon;
using Afx.Prism.Mdi.ViewModels;
using Afx.ServiceModel;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Test.Business;
using Test.Business.Service;
using Test.Module.TestDialog;

namespace Test.Module.Ribbon
{
  [Export(typeof(IRibbonItem))]
  public class ShowDialogButton : RibbonButtonViewModel
  {
    public override string TabName
    {
      get { return TestTab.TabName; }
    }

    public override string GroupName
    {
      get { return TestGroup.GroupName; }
    }

    public override string ItemIdentifier
    {
      get { return "Test Dialog"; }
    }

    public override int Index
    {
      get { return 1; }
    }

    [Dependency]
    public ModuleController ModuleController { get; set; }

    protected override void Execute()
    {
      TestDialogController tc = ModuleController.GetCreateChildController<TestDialogController>("Test Dialog");
      Stopwatch sw = new Stopwatch();
      sw.Start();
      TestObject top = new TestObject();
      using (new EventStateSuppressor(top))
      {
        top.Name = "aaa";
        for (int i = 0; i < 10; i++)
        {
          TestObject to = new TestObject();
          top.Items.Add(to);
          to.Name = "bbb";
          for (int ii = 0; ii < 10; ii++)
          {
            TestObject to1 = new TestObject();
            to.Items.Add(to1);
            to1.Name = "ccc";
          }
        }

        //top.Items[9].Name = string.Empty;
      }
      sw.Stop();
      Debug.WriteLine(sw.ElapsedMilliseconds);

      tc.TestObject = top;
      tc.TestObject.PropertyChanged += TestObject_PropertyChanged;
      //

      //using (var svc = ServiceFactory.GetService<ITestService>())
      //{
      //  object ret = svc.Instance.SaveTestObject(3, top);
      //}

      tc.Initialize();
    }

    void TestObject_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
    {
      if (e.PropertyName == BusinessObject.HasErrorsProperty)
      {
        bool b = ((IErrorProvider)sender).HasErrors;
      }

      if (e.PropertyName == BusinessObject.ErrorsProperty)
      {
        string s = ((IErrorProvider)sender).Errors;
      }
    }
  }
}

﻿using Afx.Prism.Mdi.Ribbon;
using Microsoft.Practices.ObjectBuilder2;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test.Module.Ribbon
{
  [Export(typeof(IRibbonTab))]
  public class TestTab : RibbonTab
  {
    public const string TabName = "Test";

    public override string Name
    {
      get { return TabName; }
    }
  }
}

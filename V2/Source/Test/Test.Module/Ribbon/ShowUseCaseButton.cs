﻿using Afx.Prism.Mdi.ApplicationConfigurationModule.Test;
using Afx.Prism.Mdi.Ribbon;
using Afx.Prism.Mdi.ViewModels;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Test.Module.TestUseCase;

namespace Test.Module.Ribbon
{
  [Export(typeof(IRibbonItem))]
  public class ShowUseCaseButton : RibbonButtonViewModel
  {
    public override string TabName
    {
      get { return TestTab.TabName; }
    }

    public override string GroupName
    {
      get { return TestGroup.GroupName; }
    }

    public override string ItemIdentifier
    {
      get { return "Test Use Case"; }
    }

    [Dependency]
    public ModuleController ModuleController { get; set; }

    protected override void Execute()
    {
      TestUseCaseController tc = ModuleController.GetCreateChildController<TestUseCaseController>("Test Use Case");
      tc.Initialize();
    }
  }
}

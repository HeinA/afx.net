﻿using Afx.Prism.Mdi.Ribbon;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test.Module.Ribbon
{
  [Export(typeof(IRibbonGroup))]
  public class TestGroup : RibbonGroup
  {
    public const string GroupName = "Testing Group";

    public override string TabName
    {
      get { return TestTab.TabName; }
    }

    public override string Name
    {
      get { return GroupName; }
    }
  }
}

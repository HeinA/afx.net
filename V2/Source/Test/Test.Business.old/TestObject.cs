﻿using Afx.ComponentModel;
using Afx.ComponentModel.Markup;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test.Business
{
  [DataContract("http://openafx.net/Test/Business/")]
  public class TestObject : BusinessObject
  {
    [DataMember(Name = "Name")]
    string mName = "Hein";

    [DataMember(Name = "Date")]
    DateTime mDate = DateTime.Now;
  }
}

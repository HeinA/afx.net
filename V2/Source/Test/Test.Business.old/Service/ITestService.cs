﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Test.Business.Service
{
  [ServiceContract(Namespace = "http://test.net/test")]
  //[XmlSerializerFormat]
  public interface ITestService
  {
    [OperationContract]
    void SaveTestObject(TestObject obj);
  }
}

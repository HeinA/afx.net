﻿using Afx.Business.Security;
using Afx.Prism.RibbonMdi;
using CrystalDecisions.CrystalReports.Engine;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.ProFormaInvoice
{
  [Export(typeof(ICrystalPrintDocumentInfo))]
  [Authorization(Afx.Business.Security.Roles.Administrator)]
  public class PrintDocumentInfo : CrystalPrintDocumentInfo<FreightManagement.Business.Waybill>
  {
    public override byte[] GetReport()
    {
      return ResourceHelper.GetResource(System.Reflection.Assembly.GetExecutingAssembly(), "FreightManagement.ProFormaInvoice.FreightManagement.ProFormaInvoice.rpt");
    }

    public override string Key
    {
      get { return FreightManagement.Prism.Documents.Waybill.Operations.PrintProFormaInvoice; }
    }

    public override void SetParameters(ReportDocument rpt, Business.Waybill doc, params object[] args)
    {
      rpt.SetParameterValue("@DocumentId", doc.Id);
      rpt.SetParameterValue("@VatRate", args[0]);

      //base.SetParameters(rpt, doc, args);
    }
  }
}

﻿using Afx.Business.Security;
using Afx.Prism.RibbonMdi;
using CrystalDecisions.CrystalReports.Engine;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.ProrataInvoice
{
  [Export(typeof(ICrystalPrintDocumentInfo))]
  [Authorization(Afx.Business.Security.Roles.Administrator)]
  public class PrintDocumentInfo : CrystalPrintDocumentInfo<Afx.Business.Documents.Document> // TODO: Replace with the correct Document Class
  {
    public override byte[] GetReport()
    {
      return ResourceHelper.GetResource(System.Reflection.Assembly.GetExecutingAssembly(), "FreightManagement.ProrataInvoice.FreightManagement.ProrataInvoice.rpt");
    }
  }
}

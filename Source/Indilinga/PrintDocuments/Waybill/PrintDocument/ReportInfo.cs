﻿using Afx.Prism.RibbonMdi;
using CrystalDecisions.CrystalReports.Engine;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrintDocument
{
  [Export(typeof(ICrystalPrintDocumentInfo))]
  public class PrintDocumentInfo : CrystalPrintDocumentInfo<Afx.Business.Documents.Document> // TODO: Replace with the correct Document Class
  {
    public override byte[] GetReport()
    {
      return ResourceHelper.GetResource(System.Reflection.Assembly.GetExecutingAssembly(), "PrintDocument.PrintDocument.rpt");
    }
  }
}

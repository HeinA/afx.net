﻿using Afx.Business.Security;
using Afx.Business.Service;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using CrystalDecisions.CrystalReports.Engine;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace ExcelExport.GetParametersDialog
{
  public class GetParametersDialogController : MdiExcelReportParameterController<GetParametersDialogContext, GetParametersDialogViewModel>
  {
    public const string GetParametersDialogControllerKey = "ExcelExport.GetParametersDialog.GetParametersDialogController";

    public GetParametersDialogController(IController controller)
      : base(GetParametersDialogControllerKey, controller)
    {
    }

    protected override void ApplyChanges()
    {
      //using (var svc = ServiceFactory.GetService<IService>(SecurityContext.ServerName))
      //{
      //  ExcelReport = svc.GetReport();
      //}
    }
  }
}

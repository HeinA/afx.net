﻿using Afx.Business.Security;
using Afx.Prism.RibbonMdi;
using CrystalDecisions.CrystalReports.Engine;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelExport
{
  [Export(typeof(IExcelReportInfo))]
  [Authorization(Afx.Business.Security.Roles.Administrator)]
  public class ReportInfo : ExcelReportInfo<GetParametersDialog.GetParametersDialogController>
  {
    public override string GroupName { get { return "Excel Exports"; } }
    public override string ReportName { get { return "Export1"; } }
    public override string ReportDescription { get { return "Report Description"; } }

  }
}

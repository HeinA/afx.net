﻿using Afx.Business;
using Afx.Business.Validation;
using Afx.Prism.RibbonMdi;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.WaybillManifest.GetParametersDialog
{
  public class GetParametersDialogContext : MdiReportParameterContext
  {
    #region DateTime? StartDate

    public const string StartDateProperty = "StartDate";
    DateTime? mStartDate;
    [Mandatory("Start Date is Mandatory")]
    public DateTime? StartDate
    {
      get { return mStartDate; }
      set { SetProperty<DateTime?>(ref mStartDate, value); }
    }

    #endregion

    #region DateTime? EndDate

    public const string EndDateProperty = "EndDate";
    DateTime? mEndDate;
    [Mandatory("End Date is Mandatory")]
    public DateTime? EndDate
    {
      get { return mEndDate; }
      set { SetProperty<DateTime?>(ref mEndDate, value); }
    }

    #endregion
  }
}
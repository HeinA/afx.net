﻿using Afx.Business.Security;
using Afx.Prism.RibbonMdi;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.WaybillManifest
{
  [Export(typeof(IExcelReportInfo))]
  [Authorization(Afx.Business.Security.Roles.Administrator)]
  [Authorization(FreightManagement.Business.Roles.Accounts)]
  [Authorization(FreightManagement.Business.Roles.AccountsSupervisor)]
  public class ReportInfo : ExcelReportInfo<GetParametersDialog.GetParametersDialogController>
  {
    public override string GroupName { get { return "Excel Exports"; } }
    public override string ReportName { get { return "Waybills with Manifest"; } }
    public override string ReportDescription { get { return "Waybills with Manifest Details.  Waybills may be duplicated if on multiple Manifests."; } }
  }
}

﻿using Afx.Business.Security;
using Afx.Prism.RibbonMdi;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.OutstandingPODExcel
{
  [Export(typeof(IExcelReportInfo))]
  [Authorization(Afx.Business.Security.Roles.Administrator)]
  [Authorization(FreightManagement.Business.Roles.Accounts)]
  [Authorization(FreightManagement.Business.Roles.AccountsSupervisor)]
  [Authorization(FreightManagement.Business.Roles.Dispatch)]
  [Authorization(FreightManagement.Business.Roles.DispatchSupervisor)]
  [Authorization(FreightManagement.Business.Roles.Operations)]
  [Authorization(FreightManagement.Business.Roles.OperationsSupervisor)]
  public class ReportInfo : ExcelReportInfo<GetParametersDialog.GetParametersDialogController>
  {
    public override string GroupName { get { return "Excel Exports"; } }
    public override string ReportName { get { return "Outstanding Pod"; } }
    public override string ReportDescription { get { return "Outstanding Pod Waybills"; } }
  }
}

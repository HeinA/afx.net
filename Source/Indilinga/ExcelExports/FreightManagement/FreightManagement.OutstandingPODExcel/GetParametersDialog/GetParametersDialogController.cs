﻿using Afx.Business.Security;
using Afx.Business.Service;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using FreightManagement.Business.Service;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace FreightManagement.OutstandingPODExcel.GetParametersDialog
{
  public class GetParametersDialogController : MdiExcelReportParameterController<GetParametersDialogContext, GetParametersDialogViewModel>
  {
    public const string GetParametersDialogControllerKey = "FreightManagement.OutstandingPODExcel.GetParametersDialog.GetParametersDialogController";

    public GetParametersDialogController(IController controller)
      : base(GetParametersDialogControllerKey, controller)
    {
    }

    protected override void ApplyChanges()
    {
      using (var svc = ServiceFactory.GetService<IFreightManagementService>(SecurityContext.ServerName))
      {
        ExcelReport = svc.Instance.GetOutstandingPodExport(DataContext.StartDate, DataContext.EndDate, DataContext.Account.Name, DataContext.Consignee);
      }
    }
  }
}

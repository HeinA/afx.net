﻿using Afx.Business;
using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using ContactManagement.Business;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace AccountManagement.Business
{
  [PersistantObject(Schema = "Accounts")]
  public partial class AccountAddress : AssociativeObject<Account, Address>
  {
    protected override bool ReferenceIsOwned
    {
      get { return true; }
    }
  }
}

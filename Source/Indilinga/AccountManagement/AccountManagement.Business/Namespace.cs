﻿using Afx.Business;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccountManagement.Business
{
  [Export(typeof(INamespace))]
  public class Namespace : INamespace
  {
    public const string AccountManagement = "http://indilinga.com/AccountManagement/Business";

    public string GetUri()
    {
      return AccountManagement;
    }
  }
}

﻿using Afx.Business.Collections;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccountManagement.Business
{
  public interface IAccountCollection
  {
    BusinessObjectCollection<Account> Accounts
    {
      get;
    }
  }
}

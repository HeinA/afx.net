﻿using FileHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccountManagement.Business
{
  [DelimitedRecord(",")]
  [IgnoreFirst]
  class AccountCsvRecord
  {
    public string AccountNumber;
    public string AccountName;
  }
}

﻿using Afx.Business;
using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Security;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using System.Linq;
using ContactManagement.Business;

namespace AccountManagement.Business
{
  [PersistantObject(Schema = "Accounts", TableName = "vAccountReference", IsReadOnly = true)]
  [Cache(Replicate = false)]
  public partial class AccountReference : BusinessObject
  {
    #region Constructors

    public AccountReference()
    {
    }

    public AccountReference(bool isNull)
      : base(isNull)
    {
    }

    #endregion

    #region string AccountNumber

    public const string AccountNumberProperty = "AccountNumber";
    [DataMember(Name = AccountNumberProperty, EmitDefaultValue = false)]
    string mAccountNumber;
    [PersistantProperty]
    public string AccountNumber
    {
      get { return mAccountNumber; }
      private set { SetProperty<string>(ref mAccountNumber, value); }
    }

    #endregion

    #region string Name

    public const string NameProperty = "Name";
    [DataMember(Name = NameProperty, EmitDefaultValue = false)]
    string mName = string.Empty;
    [PersistantProperty]
    public string Name
    {
      get { return mName; }
      private set { SetProperty<string>(ref mName, value); }
    }

    #endregion

    #region string Alert

    public const string AlertProperty = "Alert";
    [DataMember(Name = AlertProperty, EmitDefaultValue = false)]
    string mAlert;
    [PersistantProperty]
    public string Alert
    {
      get { return mAlert; }
      set { SetProperty<string>(ref mAlert, value); }
    }

    #endregion

    #region bool IsDebtor

    public const string IsDebtorProperty = "IsDebtor";
    [DataMember(Name = IsDebtorProperty, EmitDefaultValue = false)]
    bool mIsDebtor;
    [PersistantProperty]
    public bool IsDebtor
    {
      get { return mIsDebtor; }
      set { SetProperty<bool>(ref mIsDebtor, value); }
    }

    #endregion

    #region bool IsCreditor

    public const string IsCreditorProperty = "IsCreditor";
    [DataMember(Name = IsCreditorProperty, EmitDefaultValue = false)]
    bool mIsCreditor;
    [PersistantProperty]
    public bool IsCreditor
    {
      get { return mIsCreditor; }
      set { SetProperty<bool>(ref mIsCreditor, value); }
    }

    #endregion

    #region OrganizationalUnit RepresentativeOrganizationalUnit

    public const string RepresentativeOrganizationalUnitProperty = "RepresentativeOrganizationalUnit";
    [PersistantProperty]
    public OrganizationalUnit RepresentativeOrganizationalUnit
    {
      get { return GetCachedObject<OrganizationalUnit>(); }
      set { SetCachedObject<OrganizationalUnit>(value); }
    }

    #endregion

    #region Address PhysicalAddress

    public const string PhysicalAddressProperty = "PhysicalAddress";
    [DataMember(Name = PhysicalAddressProperty, EmitDefaultValue = false)]
    Address mPhysicalAddress;
    [PersistantProperty(ForceLoad = true)]
    public Address PhysicalAddress
    {
      get { return mPhysicalAddress; }
      set { SetProperty<Address>(ref mPhysicalAddress, value); }
    }

    #endregion

    #region Address PostalAddress

    public const string PostalAddressProperty = "PostalAddress";
    [DataMember(Name = PostalAddressProperty, EmitDefaultValue = false)]
    Address mPostalAddress;
    [PersistantProperty(ForceLoad = true)]
    public Address PostalAddress
    {
      get { return mPostalAddress; }
      set { SetProperty<Address>(ref mPostalAddress, value); }
    }

    #endregion
  }
}

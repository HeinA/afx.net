﻿using Afx.Business;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Documents;
using Afx.Business.Security;
using Afx.Business.Validation;
using ContactManagement.Business;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace AccountManagement.Business
{
  [PersistantObject(Schema = "Accounts")]
  public partial class Account : Document, IAddressCollection, ITelephoneCollection, ISimpleContactCollection, IContactOwner
  {
    #region Constructors

    public Account()
      : base(Cache.Instance.GetObject<DocumentType>(DocumentTypeIdentifier))
    {

    }

    #endregion

    #region string Name

    public const string NameProperty = "Name";
    [DataMember(Name = NameProperty, EmitDefaultValue = false)]
    string mName;
    [PersistantProperty]
    [Mandatory("Account Name is mandatory.")]
    public string Name
    {
      get { return mName; }
      set 
      {
        if (SetProperty<string>(ref mName, value))
        {
          if (DocumentNumber == null)
          {
            Regex rgx = new Regex("[^a-zA-Z0-9 -]");
            if (!String.IsNullOrEmpty(value) && value.Length >= 3) DocumentNumber = string.Format("{0}{1}", rgx.Replace(value, "").ToUpper().Substring(0, 3), "001");
          }
        }
      }
    }

    #endregion

    #region string Alert

    public const string AlertProperty = "Alert";
    [DataMember(Name = AlertProperty, EmitDefaultValue = false)]
    string mAlert;
    [PersistantProperty]
    public string Alert
    {
      get { return mAlert; }
      set { SetProperty<string>(ref mAlert, value); }
    }

    #endregion

    #region bool IsDebtor

    public const string IsDebtorProperty = "IsDebtor";
    [DataMember(Name = IsDebtorProperty, EmitDefaultValue = false)]
    bool mIsDebtor;
    [PersistantProperty]
    public bool IsDebtor
    {
      get { return mIsDebtor; }
      set { SetProperty<bool>(ref mIsDebtor, value); }
    }

    #endregion

    #region bool IsCreditor

    public const string IsCreditorProperty = "IsCreditor";
    [DataMember(Name = IsCreditorProperty, EmitDefaultValue = false)]
    bool mIsCreditor;
    [PersistantProperty]
    public bool IsCreditor
    {
      get { return mIsCreditor; }
      set { SetProperty<bool>(ref mIsCreditor, value); }
    }

    #endregion

    #region OrganizationalUnit RepresentativeOrganizationalUnit

    public const string RepresentativeOrganizationalUnitProperty = "RepresentativeOrganizationalUnit";
    [PersistantProperty]
    public OrganizationalUnit RepresentativeOrganizationalUnit
    {
      get { return GetCachedObject<OrganizationalUnit>(); }
      set { SetCachedObject<OrganizationalUnit>(value); }
    }

    #endregion

    #region Associative BusinessObjectCollection<Address> Addresses

    public const string AddressesProperty = "Addresses";
    AssociativeObjectCollection<AccountAddress, Address> mAddresses;
    [PersistantCollection(AssociativeType = typeof(AccountAddress))]
    //[DataMember]  //**** Uncomment if the collection type is not cached ****//
    public BusinessObjectCollection<Address> Addresses
    {
      get
      {
        if (mAddresses == null) using (new EventStateSuppressor(this)) { mAddresses = new AssociativeObjectCollection<AccountAddress, Address>(this); }
        return mAddresses;
      }
    }

    #endregion

    #region Associative BusinessObjectCollection<Telephone> TelephoneNumbers

    public const string TelephoneNumbersProperty = "TelephoneNumbers";
    AssociativeObjectCollection<AccountTelephone, Telephone> mTelephoneNumbers;
    [PersistantCollection(AssociativeType = typeof(AccountTelephone))]
    //[DataMember]  //**** Uncomment if the collection type is not cached ****//
    public BusinessObjectCollection<Telephone> TelephoneNumbers
    {
      get
      {
        if (mTelephoneNumbers == null) using (new EventStateSuppressor(this)) { mTelephoneNumbers = new AssociativeObjectCollection<AccountTelephone, Telephone>(this); }
        return mTelephoneNumbers;
      }
    }

    #endregion

    #region Associative BusinessObjectCollection<SimpleContact> Contacts

    public const string ContactsProperty = "Contacts";
    AssociativeObjectCollection<AccountContact, SimpleContact> mContacts;
    [PersistantCollection(AssociativeType = typeof(AccountContact))]
    public BusinessObjectCollection<SimpleContact> Contacts
    {
      get
      {
        if (mContacts == null) using (new EventStateSuppressor(this)) { mContacts = new AssociativeObjectCollection<AccountContact, SimpleContact>(this); }
        return mContacts;
      }
    }

    #endregion
    
    #region Associative BusinessObjectCollection<Contact> Controllers

    public const string ControllersProperty = "Controllers";
    AssociativeObjectCollection<AccountController, Contact> mControllers;
    [PersistantCollection(AssociativeType = typeof(AccountController))]
    //[DataMember]  //**** Uncomment if the collection type is not cached ****//
    public BusinessObjectCollection<Contact> Controllers
    {
      get
      {
        if (mControllers == null) using (new EventStateSuppressor(this)) { mControllers = new AssociativeObjectCollection<AccountController, Contact>(this); }
        return mControllers;
      }
    }

    #endregion

    #region void GetErrors(...)

    protected override void GetErrors(Collection<string> errors, string propertyName)
    {
      if (IsPropertyApplicable(propertyName, Document.DocumentNumberProperty))
      {
        if (string.IsNullOrWhiteSpace(DocumentNumber)) errors.Add("Account Number is mandatory.");
      }

      base.GetErrors(errors, propertyName);
    }

    #endregion

    #region IContactOwner

    public string ContactOwnerText
    {
      get { return Name; }
    }

    #endregion


    #region Address DefaultPhysicalAddress

    public const string DefaultPhysicalAddressProperty = "DefaultPhysicalAddress";
    [DataMember(Name = DefaultPhysicalAddressProperty, EmitDefaultValue = false)]
    [PersistantProperty]
    public Address DefaultPhysicalAddress
    {
      get
      {
        Address tempAddress = GetChildObject<Address>(Addresses);
        if (BusinessObject.IsNull(tempAddress))
        {
          tempAddress = Addresses.FirstOrDefault(x => x.AddressType.IsFlagged(AddressTypeFlags.PhysicalAddress));
          if (tempAddress != null) SetChildObject<Address>(tempAddress);
        }

        return tempAddress;
      }
      set { SetChildObject<Address>(value); }
    }

    #endregion

    #region Address DefaultPostalAddress

    public const string DefaultPostalAddressProperty = "DefaultPostalAddress";
    [DataMember(Name = DefaultPostalAddressProperty, EmitDefaultValue = false)]
    [PersistantProperty]
    public Address DefaultPostalAddress
    {
      get
      {
        Address tempAddress = GetChildObject<Address>(Addresses);
        if (BusinessObject.IsNull(tempAddress))
        {
          tempAddress = Addresses.FirstOrDefault(x => x.AddressType != null && x.AddressType.IsFlagged(AddressTypeFlags.PostalAddress));
          if (tempAddress != null) SetChildObject<Address>(tempAddress);
        }

        return tempAddress;
      }
      set { SetChildObject<Address>(value); }
    }

    #endregion
  }
}

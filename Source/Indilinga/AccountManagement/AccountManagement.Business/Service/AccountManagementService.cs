﻿using Afx.Business;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Documents;
using Afx.Business.Security;
using Afx.Business.Service;
using ContactManagement.Business;
using FileHelpers;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.WCF;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.Data;
using System.Data.Odbc;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace AccountManagement.Business.Service
{
  [ServiceBehavior(Namespace = Namespace.AccountManagement, ConcurrencyMode = ConcurrencyMode.Multiple)]
  [ExceptionShielding]
  [Export(typeof(IService))]
  public partial class AccountManagementService : ServiceBase, IAccountManagementService
  {
    public BasicCollection<Address> LoadAccountAddresses(Guid account)
    {
      Account acc = LoadAccount(account);
      BasicCollection<Address> addresses = new BasicCollection<Address>(acc.Addresses.OrderBy(ac => ac.Name));
      return addresses;
    }

    public void EditAccountAddresses(Guid account, Address address)
    {
      Account acc = LoadAccount(account);
      //COU
      //if (!acc.ControllingOrganizationalUnit.Equals(SecurityContext.OrganizationalUnit)) //Transfer Document
      //{
      //  using (new ConnectionScope())
      //  {
      //    acc = (Account)DocumentHelper.TransferAllDocuments(acc, SecurityContext.OrganizationalUnit.GlobalIdentifier, PersistanceManager);
      //  }
      //}
      Address a = acc.Addresses.FirstOrDefault(a1 => a1.GlobalIdentifier.Equals(address.GlobalIdentifier));
      if (a != null)
      {
        a.Name = address.Name;
        a.AddressText = address.AddressText;
        a.ImportReference = address.ImportReference;
        a.Contact = address.Contact;
        a.ContactNumber = address.ContactNumber;
        a.ExportReference = address.ExportReference;
      }
      else
      {
        acc.Addresses.Add(address);
      }
      acc = SaveAccount(acc);
    }

    public bool VerifyDocumentNumberDoesNotExist(string documentNumber)
    {

      try
      {
        Collection<IDataFilter> filters = new Collection<IDataFilter>();
        filters.Add(new DataFilter<Account>(Account.DocumentNumberProperty, FilterType.Equals, documentNumber));

        BasicCollection<Account> col = GetInstances<Account>(filters);
        if (col.Count() > 0) { return true; } else { return false; }
      }
      catch
      {
        throw;
      }
    }

    public void ImportAccountsCsv(byte[] csvfile)
    {
      try
      {
        var repo = PersistanceManager.GetRepository<Account>();
        var engine = new FileHelperEngine<AccountCsvRecord>();
        using (var ms = new MemoryStream(csvfile))
        {
          using (var sr = new StreamReader(ms))
          {
            var records = engine.ReadStream(sr);

            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            using (new ConnectionScope())
            {
              foreach (var r in records)
              {
                var a = new Account() { DocumentNumber=r.AccountNumber, Name=r.AccountName };

                repo.Persist(a);
              }
              scope.Complete();
            }
          }
        }
      }
      catch
      {
        throw;
      }
    }

    //public void Import()
    //{
    //  ContactManagement.Business.AddressType postalAT = Cache.Instance.GetObjects<ContactManagement.Business.AddressType>().First(tt => tt.GlobalIdentifier.Equals(Guid.Parse("735c375e-2903-4500-85d3-ff52b6b35561")));
    //  ContactManagement.Business.AddressType deliveryAT = Cache.Instance.GetObjects<ContactManagement.Business.AddressType>().First(tt => tt.GlobalIdentifier.Equals(Guid.Parse("43255d1a-2f75-4358-ae5e-3f43e3bc64ae")));
    //  ContactManagement.Business.TelephoneType telephoneType = Cache.Instance.GetObjects<ContactManagement.Business.TelephoneType>().First(tt => tt.GlobalIdentifier.Equals(Guid.Parse("28e6e854-ac6e-4d42-a3c3-90fd8008d689")));
    //  ContactManagement.Business.ContactType contactType = Cache.Instance.GetObjects<ContactManagement.Business.ContactType>().First(tt => tt.GlobalIdentifier.Equals(Guid.Parse("30f942f3-d554-4335-8308-3f0e4c11880a")));
      
    //  using (SqlConnection con = new SqlConnection("data source=.;Initial Catalog=IFMS_Panther_Windhoek;Integrated Security=SSPI"))
    //  {
    //    con.Open();

    //    using (SqlCommand cmd = new SqlCommand("select * from dbo.sheet1$", con))
    //    {
    //      DataSet ds = DataHelper.ExecuteDataSet(cmd);
    //      foreach (DataRow dr in ds.Tables[0].Rows)
    //      {
    //        string accountNo = (string)dr["AccNr"];
    //        string accountName = (string)dr["AccName"];

    //        string telephone = null;
    //        if (dr["Tel Nr"] != DBNull.Value) telephone = (string)dr["Tel Nr"];

    //        string contact = null;
    //        if (dr["Contact"] != DBNull.Value) contact = (string)dr["Contact"];

    //        string email = null;
    //        if (dr["email"] != DBNull.Value) email = (string)dr["email"];

    //        string postal = null;
    //        if (dr["Postal 1"] != DBNull.Value) postal += (string)dr["Postal 1"];
    //        if (dr["Postal 2"] != DBNull.Value) postal += "\r\n" + (string)dr["Postal 2"];
    //        if (dr["Postal 3"] != DBNull.Value) postal += "\r\n" + (string)dr["Postal 3"];
    //        if (dr["Postal 4"] != DBNull.Value) postal += "\r\n" + (string)dr["Postal 4"];
    //        if (dr["Postal 5"] != DBNull.Value) postal += "\r\n" + (string)dr["Postal 5"];

    //        string delivery = null;
    //        if (dr["Del 1"] != DBNull.Value) delivery += (string)dr["Del 1"];
    //        if (dr["Del 2"] != DBNull.Value) delivery += "\r\n" + (string)dr["Del 2"];
    //        if (dr["Del 3"] != DBNull.Value) delivery += "\r\n" + (string)dr["Del 3"];
    //        if (dr["Del 4"] != DBNull.Value) delivery += "\r\n" + (string)dr["Del 4"];
    //        if (dr["Del 5"] != DBNull.Value) delivery += "\r\n" + (string)dr["Del 5"];

    //        AccountManagement.Business.Account a = new AccountManagement.Business.Account();
    //        a.DocumentNumber = accountNo;
    //        a.Name = accountName;
    //        if (telephone != null) a.TelephoneNumbers.Add(new ContactManagement.Business.Telephone() { TelephoneType = telephoneType, TelephoneNumber = telephone });
    //        if (postal != null) a.Addresses.Add(new ContactManagement.Business.Address() { AddressType = postalAT, AddressText = postal });
    //        if (delivery != null) a.Addresses.Add(new ContactManagement.Business.Address() { AddressType = deliveryAT, AddressText = delivery });

    //        if (!string.IsNullOrWhiteSpace(contact))
    //        {
    //          ContactManagement.Business.SimpleContact c = new ContactManagement.Business.SimpleContact();
    //          c.Firstname = contact;
    //          c.Lastname = contact;
    //          c.ContactType = contactType;
    //          if (email != null) c.EMail = email;
    //          a.Contacts.Add(c);
    //        }

    //        Debug.WriteLine(a.DocumentNumber);
    //        Persist<Account>(a, PersistanceFlags.None, false);
    //      }
    //    }

    //    con.Close();
    //  }
    //}
  }
}

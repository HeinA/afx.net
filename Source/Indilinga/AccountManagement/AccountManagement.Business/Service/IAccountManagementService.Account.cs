﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace AccountManagement.Business.Service
{
  public partial interface IAccountManagementService
  {
    [OperationContract]
    AccountManagement.Business.Account LoadAccount(Guid globalIdentifier);

    [OperationContract]
    AccountManagement.Business.Account SaveAccount(AccountManagement.Business.Account obj);
  }
}

﻿using Afx.Business.Service;
using Afx.Business.ServiceModel;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.WCF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace AccountManagement.Business.Service
{
  public partial class AccountManagementService
  {
    public AccountManagement.Business.Account LoadAccount(Guid globalIdentifier)
    {
      try
      {
        AccountManagement.Business.Account a = GetInstance<AccountManagement.Business.Account>(globalIdentifier);
        return a;
      }
      catch
      {
        throw;
      }
    }

    public AccountManagement.Business.Account SaveAccount(AccountManagement.Business.Account obj)
    {
      try
      {
        //CacheExtension.RefreshCache = true;
        return Persist<AccountManagement.Business.Account>(obj);
      }
      catch
      {
        throw;
      }
    }
  }
}
﻿using Afx.Business;
using Afx.Business.Collections;
using ContactManagement.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace AccountManagement.Business.Service
{
  [ServiceContract(Namespace = Namespace.AccountManagement)]
  public partial interface IAccountManagementService : IDisposable
  {
    [OperationContract]
    BasicCollection<Address> LoadAccountAddresses(Guid account);

    [OperationContract]
    void EditAccountAddresses(Guid account, Address address);

    [OperationContract]
    bool VerifyDocumentNumberDoesNotExist(string documentNumber);

    [OperationContract]
    void ImportAccountsCsv(byte[] csvfile);

    //[OperationContract]
    //void Import();
  }
}

﻿using Afx.Business.Data;
using Afx.Business.Documents;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace AccountManagement.Business
{
  [DataContract(IsReference = true, Namespace = Namespace.AccountManagement)]
  public partial class Account
  {
    public const string DocumentTypeIdentifier = "{53fe507b-1733-4d0e-838a-a2c42443fb80}";
    public class States
    {
      public const string Active = "{71f5819b-5679-4732-864b-bcd79a530b21}";
    }

    protected Account(DocumentType documentType)
      : base(documentType)
    {
    }
  }
}

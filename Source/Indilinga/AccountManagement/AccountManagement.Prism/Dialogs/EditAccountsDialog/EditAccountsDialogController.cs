﻿using AccountManagement.Business;
using Afx.Business.Collections;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace AccountManagement.Prism.Dialogs.EditAccountsDialog
{
  public class EditAccountsDialogController : MdiDialogController<EditAccountsDialogContext, EditAccountsDialogViewModel>
  {
    public const string EditAccountsDialogControllerKey = "AccountManagement.Prism.Dialogs.EditAccountsDialog.EditAccountsDialogController";

    public EditAccountsDialogController(IController controller)
      : base(EditAccountsDialogControllerKey, controller)
    {
    }

    protected override bool OnRun()
    {
      if (DataContext == null) DataContext = new EditAccountsDialogContext();
      ViewModel.Caption = "Edit Accounts";
      return base.OnRun();
    }

    protected override void ApplyChanges()
    {
      Stack<Account> removed = new Stack<Account>();
      foreach (Account a in AccountCollection.Accounts)
      {
        if (!Accounts.Contains(a)) removed.Push(a);
      }
      while (removed.Count > 0)
      {
        AccountCollection.Accounts.Remove(removed.Pop());
      }

      foreach (Account a in Accounts)
      {
        if (!AccountCollection.Accounts.Contains(a)) AccountCollection.Accounts.Add(a);
      }

      base.ApplyChanges();
    }

    BasicCollection<Account> mAccounts = new BasicCollection<Account>();
    public BasicCollection<Account> Accounts
    {
      get { return mAccounts; }
    }

    #region IAccountCollection AccountCollection

    public const string AccountCollectionProperty = "AccountCollection";
    IAccountCollection mAccountCollection;
    public IAccountCollection AccountCollection
    {
      get { return mAccountCollection; }
      set
      {
        mAccountCollection = value;
        foreach (var v in mAccountCollection.Accounts)
        {
          Accounts.Add(v);
        }
      }
    }

    #endregion
  }
}

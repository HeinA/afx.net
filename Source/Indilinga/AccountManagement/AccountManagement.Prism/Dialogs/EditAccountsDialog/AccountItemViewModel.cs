﻿using AccountManagement.Business;
using Afx.Prism;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccountManagement.Prism.Dialogs.EditAccountsDialog
{
  public class AccountItemViewModel : SelectableViewModel<Account>
  {
    [InjectionConstructor]
    public AccountItemViewModel(IController controller)
      : base(controller)
    {
    }

    [Dependency]
    public EditAccountsDialogController EditAccountsDialogController { get; set; }

    new EditAccountsDialogViewModel Parent
    {
      get { return (EditAccountsDialogViewModel)base.Parent; }
    }


    public override bool IsSelected
    {
      get
      {
        if (base.IsSelected) Parent.SelectedItemViewModel = this;
        return base.IsSelected;
      }
      set
      {
        base.IsSelected = value;
        if (base.IsSelected) Parent.SelectedItemViewModel = this;
      }
    }

    #region bool IsChecked

    public const string IsCheckedProperty = "IsChecked";
    public bool IsChecked
    {
      get { return EditAccountsDialogController.Accounts.Contains(Model); }
      set
      {
        if (value)
        {
          if (!EditAccountsDialogController.Accounts.Contains(Model))
          {
            EditAccountsDialogController.Accounts.Add(Model);
            OnPropertyChanged(IsCheckedProperty);
          }
        }
        else
        {
          if (EditAccountsDialogController.Accounts.Contains(Model))
          {
            EditAccountsDialogController.Accounts.Remove(Model);
            OnPropertyChanged(IsCheckedProperty);
          }
        }
      }
    }

    #endregion
  }
}

﻿using AccountManagement.Business;
using Afx.Business.Data;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace AccountManagement.Prism.Dialogs.EditAccountsDialog
{
  public class EditAccountsDialogViewModel : MdiDialogViewModel<EditAccountsDialogContext>
  {
    [InjectionConstructor]
    public EditAccountsDialogViewModel(IController controller)
      : base(controller)
    {
    }

    [Dependency]
    public EditAccountsDialogController EditAccountsDialogController { get; set; }

    public IEnumerable<AccountItemViewModel> Accounts
    {
      get { return ResolveViewModels<AccountItemViewModel, Account>(Cache.Instance.GetObjects<Account>(a => a.DocumentNumber, true)); }
    }

    #region AccountItemViewModel SelectedItemViewModel

    public const string SelectedItemViewModelProperty = "SelectedItemViewModel";
    AccountItemViewModel mSelectedItemViewModel;
    public AccountItemViewModel SelectedItemViewModel
    {
      get { return mSelectedItemViewModel; }
      set { SetProperty<AccountItemViewModel>(ref mSelectedItemViewModel, value); }
    }

    #endregion

    #region DelegateCommand KeyDownCommand

    DelegateCommand<KeyEventArgs> mKeyDownCommand;
    public DelegateCommand<KeyEventArgs> KeyDownCommand
    {
      get { return mKeyDownCommand ?? (mKeyDownCommand = new DelegateCommand<KeyEventArgs>(ExecuteKeyDown)); }
    }

    void ExecuteKeyDown(KeyEventArgs e)
    {
      if (e.Key != Key.Space) return;
      if (SelectedItemViewModel != null) SelectedItemViewModel.IsChecked = !SelectedItemViewModel.IsChecked;
      e.Handled = true;
    }

    #endregion
  }
}

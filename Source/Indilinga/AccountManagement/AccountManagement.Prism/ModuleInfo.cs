﻿using Afx.Prism.RibbonMdi;
using AccountManagement.Business;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Afx.Prism;

namespace AccountManagement.Prism
{
  [Export(typeof(IModuleInfo))]
  public class ModuleInfo : IModuleInfo
  {
    public string ModuleNamespace
    {
      get { return Namespace.AccountManagement; }
    }

    public string[] ModuleDependencies
    {
      get { return new string[] { Afx.Business.Namespace.Afx, ContactManagement.Business.Namespace.ContactManagement }; }
    }

    public Type ModuleController
    {
      get { return typeof(AccountManagementModuleController); }
    }
  }
}
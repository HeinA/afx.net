﻿using AccountManagement.Business.Service;
using Afx.Business;
using Afx.Business.Security;
using Afx.Business.Service;
using Afx.Prism.RibbonMdi;
using Afx.Prism.RibbonMdi.Dialogs.Import;
using Afx.Prism.RibbonMdi.Ribbon;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccountManagement.Prism.Ribbon
{
  [Export(typeof(IRibbonItem))]
  public class ImportAccountsButton : RibbonButtonViewModel
  {
    public override string TabName
    {
      get { return AdministrationTab.TabName; }
    }

    public override string GroupName
    {
      get { return ToolsGroup.GroupName; }
    }

    public override string ItemIdentifier
    {
      get { return "ImportAccountsButtonAdministration"; }
    }

    public override int Index
    {
      get { return 2; }
    }

    protected override void OnExecute()
    {
      OpenFileDialog ofd = new OpenFileDialog();
      ofd.Filter = "CSV Files|*.csv";
      if (ofd.ShowDialog() != true) return;
      using (var svc = ProxyFactory.GetService<IAccountManagementService>(SecurityContext.MasterServer.Name))
      {
        svc.ImportAccountsCsv(File.ReadAllBytes(ofd.FileName));
      }
    }
  }
}

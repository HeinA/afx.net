﻿using AccountManagement.Business;
using Afx.Business.Data;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfControls.Editors;

namespace AccountManagement.Prism.Extensions.Account
{
  public class AccountProvider : ISuggestionProvider
  {
    public IEnumerable GetSuggestions(string filter)
    {
      return Cache.Instance.GetObjects<AccountReference>(ar => ar.Name.ToUpperInvariant().Contains(filter.ToUpperInvariant()) || ar.AccountNumber.ToUpperInvariant().Contains(filter.ToUpperInvariant()));
    }
  }
}

﻿using AccountManagement.Business;
using AccountManagement.Business.Service;
using Afx.Business;
using Afx.Business.Data;
using Afx.Business.Documents;
using Afx.Business.Security;
using Afx.Business.Service;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Documents;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccountManagement.Prism.Extensions.Account
{
  public class DocumentAccountSelectionViewModel : ExtensionViewModel<Document>
  {
    #region Constructors

    [InjectionConstructor]
    public DocumentAccountSelectionViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    #region IMdiDocumentController DocumentController

    IMdiDocumentController mDocumentController;
    [Dependency]
    public IMdiDocumentController DocumentController
    {
      get { return mDocumentController; }
      set { mDocumentController = value; }
    }

    #endregion

    #region AccountReference AccountReference

    bool mInternalUpdate = false;
    AccountReference mAccountReference;
    public AccountReference AccountReference
    {
      get { return mAccountReference; }
      set
      {
        if (SetProperty<AccountReference>(ref mAccountReference, value))
        {
          if (!mInternalUpdate)
          {
            try
            {
              mInternalUpdate = true;
              if (AccountReference == null)
              {
                Account = null;
              }
              else
              {
                using (new WaitCursor())
                using (var svc = ProxyFactory.GetService<IAccountManagementService>(SecurityContext.ServerName))
                {
                  Account = svc.LoadAccount(AccountReference.GlobalIdentifier);
                }
              }
            }
            catch
            {
              throw;
            }
            finally
            {
              mInternalUpdate = false;
            }
          }
        }
        else
        {
          OnPropertyChanged("AccountReference");
        }
      }
    }

    #endregion

    #region Account Account

    public const string AccountProperty = "Account";
    AccountManagement.Business.Account mAccount;
    public AccountManagement.Business.Account Account
    {
      get { return mAccount; }
      set
      {
        if (SetProperty<AccountManagement.Business.Account>(ref mAccount, value))
        {
          if (!mInternalUpdate)
          {
            try
            {
              mInternalUpdate = true;
              if (Account == null) AccountReference = null;
              else AccountReference = Cache.Instance.GetObject<AccountReference>(Account.GlobalIdentifier);
            }
            catch
            {
              throw;
            }
            finally
            {
              mInternalUpdate = false;
            }
          }
        }
      }
    }

    #endregion

    #region IDataErrorInfo Source

    IDataErrorInfo mSource;
    protected IDataErrorInfo Source
    {
      get { return mSource; }
      private set { mSource = value; }
    }

    #endregion

    #region string PropertyName

    string mPropertyName;
    protected string PropertyName
    {
      get { return mPropertyName; }
      private set { mPropertyName = value; }
    }

    #endregion

    public virtual void Bind(INotifyPropertyChanged source, string propertyName)
    {
      Source = source as IDataErrorInfo;
      PropertyName = propertyName;
      BindingHelper.Bind(source, propertyName, this, AccountProperty);
    }

    #region string GetError(...)

    protected override string GetError(string propertyName)
    {
      if (propertyName == null || propertyName == "AccountReference")
      {
        if (Source != null)
        {
          return Source[PropertyName];
        }
      }

      return base.GetError(propertyName);
    }

    #endregion
  }
}

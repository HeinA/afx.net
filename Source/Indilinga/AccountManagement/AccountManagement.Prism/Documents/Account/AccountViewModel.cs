﻿using Afx.Business;
using Afx.Business.Activities;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Documents;
using AccountManagement.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows;
using ContactManagement.Business;

namespace AccountManagement.Prism.Documents.Account
{
  public partial class AccountViewModel : MdiDocumentViewModel<AccountContext>
  {
    #region Constructors

    [InjectionConstructor]
    public AccountViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    #region string Name

    public string Name
    {
      get { return Model == null ? GetDefaultValue<string>() : Model.Document == null ? GetDefaultValue<string>() : Model.Document.Name; }
      set { Model.Document.Name = value; }
    }

    #endregion

    #region string Alert

    public string Alert
    {
      get { return Model == null ? GetDefaultValue<string>() : Model.Document == null ? GetDefaultValue<string>() : Model.Document.Alert; }
      set { Model.Document.Alert = value; }
    }

    #endregion


    //#region View Setup

    //protected override bool IsDocumentNumberReadOnlyCore
    //{
    //  get { return false; }
    //}

    //public override Visibility OrganizationalUnitVisibility
    //{
    //  get { return Visibility.Collapsed; }
    //}

    //public override string DocumentNumberText
    //{
    //  get { return "Account Number"; }
    //}

    //public override string DocumentDateText
    //{
    //  get { return "Account Creation Date"; }
    //}

    //public override Visibility ReferencesVisibilityCore
    //{
    //  get { return Visibility.Collapsed; }
    //}

    //#endregion
  }
}


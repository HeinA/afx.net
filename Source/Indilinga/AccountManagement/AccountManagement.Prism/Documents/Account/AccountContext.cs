﻿using Afx.Business.Security;
using Afx.Business.Service;
using Afx.Prism.RibbonMdi.Documents;
using AccountManagement.Business;
using AccountManagement.Business.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccountManagement.Prism.Documents.Account
{
  public partial class AccountContext : DocumentContext<AccountManagement.Business.Account>
  {
    public AccountContext()
      : base(AccountManagement.Business.Account.DocumentTypeIdentifier)
    {
    }

    public AccountContext(Guid documentIdentifier)
      : base(AccountManagement.Business.Account.DocumentTypeIdentifier, documentIdentifier)
    {
    }

    public override void SaveData()
    {
      using (var svc = ProxyFactory.GetService<IAccountManagementService>(SecurityContext.ServerName))
      {
        Document = svc.SaveAccount(Document);
      }
      base.SaveData();
    }

    public override void LoadData()
    {
      using (var svc = ProxyFactory.GetService<IAccountManagementService>(SecurityContext.ServerName))
      {
        Document = svc.LoadAccount(DocumentIdentifier);
      }
      base.LoadData();
    }
  }
}


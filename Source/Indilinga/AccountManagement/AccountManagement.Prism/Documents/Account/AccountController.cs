﻿using Afx.Business;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Documents;
using AccountManagement.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ContactManagement.Prism.Extensions.Contacts;
using Afx.Business.Data;
using ContactManagement.Business;

namespace AccountManagement.Prism.Documents.Account
{
  public partial class AccountController : MdiDocumentController<AccountContext, AccountViewModel>
  {
    [InjectionConstructor]
    public AccountController(IController controller)
      : base(controller)
    {
    }

    private AccountHeaderViewModel hvm;

    protected override void ExtendDocument()
    {
      hvm = ExtendDocument<AccountHeaderViewModel>(HeaderRegion);
      ExtendDocument<AddressViewModel>(TabRegion);
      ExtendDocument<TelephoneViewModel>(TabRegion);

      SimpleContactViewModel vm = ExtendDocument<SimpleContactViewModel>(TabRegion);
      vm.ContactTypes = Cache.Instance.GetObjects<ContactType>(true, t => t.IsFlagged(ContactTypeFlags.AccountContact), t => t.Name, true);
      
      base.ExtendDocument();
    }

    protected override void OnLoaded()
    {
      base.OnLoaded();
    }
    //protected override void ExecuteOperation(Operation op, BusinessObject argument)
    //{
    //  switch (op.Identifier)
    //  {
    //    case Operations.Save:
    //      if (!hvm.IsDocumentNumberValid)
    //      {
    //        this.CancelSave = true;
    //        ExceptionHelper.HandleException(new Exception(String.Format("A document with number {0} already exists in database!", DataContext.Document.DocumentNumber)));
    //      }
    //      else
    //      {
    //        this.CancelSave = false;
    //      }
    //      break;
    //  }
    //  base.ExecuteOperation(op, argument);
    //}
  }
}


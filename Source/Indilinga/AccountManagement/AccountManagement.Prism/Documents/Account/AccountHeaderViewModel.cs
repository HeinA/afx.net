﻿using Afx.Business;
using Afx.Business.Activities;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Documents;
using AccountManagement.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using Afx.Business.Data;
using Afx.Business.Security;
using ContactManagement.Business;
using Afx.Business.Service;
using System.Threading.Tasks;
using AccountManagement.Business.Service;
using Afx.Prism.RibbonMdi;

namespace AccountManagement.Prism.Documents.Account
{
  public class AccountHeaderViewModel : DocumentHeaderViewModel<AccountManagement.Business.Account>
  {
    #region Constructors

    [InjectionConstructor]
    public AccountHeaderViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    //#region IEnumerable<OrganizationalUnit> OrganizationalUnits

    //public IEnumerable<OrganizationalUnit> OrganizationalUnits
    //{
    //  get { return Cache.Instance.GetObjects<OrganizationalUnit>(ou => ou.Name, true); }
    //}

    //#endregion



    public IEnumerable<Address> PhysicalAddresses
    {
      get { return Model.Addresses.Where(a => a.AddressType.IsFlagged(ContactManagement.Business.AddressTypeFlags.PhysicalAddress)); }
    }

    public IEnumerable<Address> PostalAddresses
    {
      get { return Model.Addresses.Where(a => a.AddressType.IsFlagged(ContactManagement.Business.AddressTypeFlags.PostalAddress)); }
    }

    #region bool IsInternalAccount

    public const string IsInternalAccountProperty = "IsInternalAccount";
    public bool IsInternalAccount
    {
      get { return !BusinessObject.IsNull(Model.RepresentativeOrganizationalUnit); }
      set
      {
        if (value)
        {
          if (BusinessObject.IsNull(Model.RepresentativeOrganizationalUnit))
          {
            RepresentativeOrganizationalUnit = OrganizationalUnits.FirstOrDefault();
          }
        }
        else
        {
          RepresentativeOrganizationalUnit = new OrganizationalUnit(true);
        }
      }
    }

    #endregion

    #region OrganizationalUnit RepresentativeOrganizationalUnit

    public OrganizationalUnit RepresentativeOrganizationalUnit
    {
      get { return Model == null ? GetDefaultValue<OrganizationalUnit>() : Model.RepresentativeOrganizationalUnit; }
      set
      {
        Model.RepresentativeOrganizationalUnit = value;
        OnPropertyChanged(IsInternalAccountProperty);
      }
    }

    #endregion
  }
}


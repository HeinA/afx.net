﻿using Afx.Business.Security;
using Afx.Business.Service;
using Afx.Prism.RibbonMdi.Documents;
using AccountManagement.Business;
using AccountManagement.Business.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.Composition;

namespace AccountManagement.Prism.Documents.Account
{
  [Export("Context:" + AccountManagement.Business.Account.DocumentTypeIdentifier)]
  public partial class AccountContext
  {
  }
}


﻿using Afx.Business;
using Afx.Prism.RibbonMdi;
using DocumentManagement.Business;
using DocumentManagement.Prism.Dialogs.FileDocumentDialog;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocumentManagement.Prism
{
  public static class AttachFileHelper
  {
    public static void AttachFile(IDocumentRepositoryAware dra)
    {
      OpenFileDialog ofd = new OpenFileDialog();
      if (ofd.ShowDialog() == false) return;

      try
      {
        MdiApplicationController.Current.Dispatcher.BeginInvoke(new Action(() =>
        {
          try
          {
            var fdc = MdiApplicationController.Current.CreateChildController<FileDocumentDialogController>();
            fdc.Filename = ofd.FileName;
            fdc.Document = dra;
            fdc.DeleteFile = false;
            fdc.Run();
          }
          catch (Exception ex)
          {
            if (ExceptionHelper.HandleException(ex)) throw;
          }
        }), System.Windows.Threading.DispatcherPriority.Normal);
      }
      catch (Exception ex)
      {
        if (ExceptionHelper.HandleException(ex)) throw;
      }
    }
  }
}

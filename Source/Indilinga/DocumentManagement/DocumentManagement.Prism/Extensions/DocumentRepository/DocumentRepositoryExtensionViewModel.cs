﻿using Afx.Business;
using Afx.Business.Data;
using Afx.Business.Documents;
using Afx.Business.Security;
using Afx.Business.Service;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Documents;
using DocumentManagement.Business;
using DocumentManagement.Business.Service;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocumentManagement.Prism.Extensions.DocumentRepository
{
  public class DocumentRepositoryExtensionViewModel : ExtensionViewModel<DocumentRepositoryExtension>
  {
    #region Constructors

    [InjectionConstructor]
    public DocumentRepositoryExtensionViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    #region string Title

    public string Title
    {
      get { return "Scanned Documents"; }
    }

    #endregion

    #region bool IsAdministrator

    public const string IsAdministratorProperty = "IsAdministrator";
    public bool IsAdministrator
    {
      get { return SecurityContext.User.HasRole(Roles.Administrator); }
    }

    #endregion

    #region DelegateCommand<object> ViewDocumentCommand

    DelegateCommand<object> mViewDocumentCommand;
    public DelegateCommand<object> ViewDocumentCommand
    {
      get { return mViewDocumentCommand ?? (mViewDocumentCommand = new DelegateCommand<object>(ExecuteViewDocument)); }
    }

    void ExecuteViewDocument(object args)
    {
      ScannedDocument sd = args as ScannedDocument;
      if (sd == null) return;

      using (new WaitCursor())
      {
        try
        {
          string filename = string.Format("{0}{1}.{2}", Path.GetTempPath(), Guid.NewGuid(), sd.FileExtension);
          byte[] bytes = null;

          try
          {
            using (var svc = ProxyFactory.GetService<IDocumentManagementService>(SecurityContext.ServerName))
            {
              bytes = svc.LoadDocument(sd.GlobalIdentifier);
            }
          }
          catch { }

          if (bytes == null)
          {
            try
            {
              using (var svc = ProxyFactory.GetService<IDocumentManagementService>(SecurityContext.MasterServer.Name))
              {
                bytes = svc.LoadDocument(sd.GlobalIdentifier);
              }
            }
            catch { }
          }

          if (bytes == null)
          {
            foreach (var server in Cache.Instance.GetObjects<ServerInstance>().Where(x => x.Name != SecurityContext.ServerName && x.Name != SecurityContext.MasterServer.Name))
            {
              try
              {
                using (var svc = ProxyFactory.GetService<IDocumentManagementService>(server.Name))
                {
                  bytes = svc.LoadDocument(sd.GlobalIdentifier);
                }
              }
              catch { }
            }
          }

          if (bytes != null)
          {
            File.WriteAllBytes(filename, bytes);
            Process.Start(filename);
          }
        }
        catch (Exception ex)
        {
          if (ExceptionHelper.HandleException(ex)) throw;
        }
      }
    }

    #endregion
  }
}

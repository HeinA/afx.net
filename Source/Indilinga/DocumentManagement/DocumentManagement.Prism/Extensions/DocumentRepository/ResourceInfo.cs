﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocumentManagement.Prism.Extensions
{
  [Export(typeof(Afx.Prism.ResourceInfo))]
  public class ResourceInfo : Afx.Prism.ResourceInfo
  {
    public override Uri GetUri()
    {
      return new Uri("pack://application:,,,/DocumentManagement.Prism;component/Extensions/DocumentRepository/Resources.xaml", UriKind.Absolute);
    }

    public override int Priority
    {
      get { return 50; }
    }
  }
}

﻿using Afx.Business.Data;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using DocumentManagement.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocumentManagement.Prism.Settings
{
  [Export(typeof(MdiSettingViewModel<ScanSetting>))]
  public class ScanSettingViewModel : MdiSettingViewModel<ScanSetting>
  {
    #region Constructors

    [InjectionConstructor]
    public ScanSettingViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    public IEnumerable<ScannerType> ScannerTypes
    {
      get { return Cache.Instance.GetObjects<ScannerType>(true, st => st.Name, true); }
    }
  }
}

﻿using Afx.Prism;
using Afx.Prism.RibbonMdi;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace DocumentManagement.Prism.Dialogs.FilingDocumentDialog
{
  public class FilingDocumentDialogController : MdiDialogController<FilingDocumentDialogContext, FilingDocumentDialogViewModel>
  {
    public const string FilingDocumentDialogControllerKey = "DocumentManagement.Prism.Dialogs.FilingDocumentDialog.FilingDocumentDialogController";

    public FilingDocumentDialogController(IController controller)
      : base(FilingDocumentDialogControllerKey, controller)
    {
    }

    protected override bool OnRun()
    {
      if (DataContext == null) DataContext = new FilingDocumentDialogContext();
      ViewModel.Caption = "Filing Document";
      return base.OnRun();
    }

    protected override void ApplyChanges()
    {
      base.ApplyChanges();
    }
  }
}

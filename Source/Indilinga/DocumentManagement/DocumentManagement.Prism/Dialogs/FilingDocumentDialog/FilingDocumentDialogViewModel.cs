﻿using Afx.Prism;
using Afx.Prism.RibbonMdi;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocumentManagement.Prism.Dialogs.FilingDocumentDialog
{
  public class FilingDocumentDialogViewModel : MdiDialogViewModel<FilingDocumentDialogContext>
  {
    [InjectionConstructor]
    public FilingDocumentDialogViewModel(IController controller)
      : base(controller)
    {
    }
  }
}

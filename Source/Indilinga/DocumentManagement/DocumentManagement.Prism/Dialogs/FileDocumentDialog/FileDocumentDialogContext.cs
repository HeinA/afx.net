﻿using Afx.Business;
using Afx.Business.Documents;
using Afx.Business.Validation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocumentManagement.Prism.Dialogs.FileDocumentDialog
{
  public class FileDocumentDialogContext : BusinessObject
  {
    #region string Description

    public const string DescriptionProperty = "Description";
    string mDescription;
    public string Description
    {
      get { return mDescription; }
      set { SetProperty<string>(ref mDescription, value); }
    }

    #endregion

    #region DocumentTypeScanType ScanType

    public const string ScanTypeProperty = "ScanType";
    DocumentTypeScanType mScanType;
    [Mandatory("Scan Type is Mandatory")]
    public DocumentTypeScanType ScanType
    {
      get { return mScanType; }
      set { SetProperty<DocumentTypeScanType>(ref mScanType, value); }
    }

    #endregion
  }
}
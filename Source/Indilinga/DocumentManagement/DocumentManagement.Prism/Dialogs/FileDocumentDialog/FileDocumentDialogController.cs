﻿using Afx.Business;
using Afx.Business.Data;
using Afx.Business.Documents;
using Afx.Business.Security;
using Afx.Business.Service;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using DocumentManagement.Business;
using DocumentManagement.Business.Service;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace DocumentManagement.Prism.Dialogs.FileDocumentDialog
{
  public class FileDocumentDialogController : MdiDialogController<FileDocumentDialogContext, FileDocumentDialogViewModel>
  {
    public const string FileDocumentDialogControllerKey = "DocumentManagement.Prism.Dialogs.FileDocumentDialog.FileDocumentDialogController";

    public FileDocumentDialogController(IController controller)
      : base(FileDocumentDialogControllerKey, controller)
    {
      DeleteFile = true;
    }

    protected override bool OnRun()
    {
      if (DataContext == null) DataContext = new FileDocumentDialogContext();
      ViewModel.Caption = "File Document";
      return base.OnRun();
    }

    #region string Filename

    public const string FilenameProperty = "Filename";
    string mFilename;
    public string Filename
    {
      get { return mFilename; }
      set { mFilename = value; }
    }

    #endregion

    #region IDocumentRepositoryAware Document

    public const string DocumentProperty = "Document";
    IDocumentRepositoryAware mDocument;
    public IDocumentRepositoryAware Document
    {
      get { return mDocument; }
      set { mDocument = value; }
    }

    #endregion

    public bool DeleteFile { get; set; }

    protected override void ApplyChanges()
    {
      ScannedDocument sd = new ScannedDocument();
      sd.Description = DataContext.Description;
      sd.User = Cache.Instance.GetObject<UserReference>(SecurityContext.User.GlobalIdentifier);
      sd.FileExtension = Filename.Substring(Filename.LastIndexOf('.'));
      sd.ScanType = DataContext.ScanType;

      bool repeat = true;
      while (repeat)
      {
        try
        {
          using (var svc = ProxyFactory.GetService<IDocumentManagementService>(SecurityContext.ServerName))
          {
            byte[] bytes = File.ReadAllBytes(Filename);
            svc.FileDocument(sd.GlobalIdentifier, bytes);
          }
        }
        catch (IOException)
        {
          Thread.Sleep(500);
          continue;
        }
        repeat = false;
      }

      if (!File.Exists(Filename)) throw new MessageException("File no longer esists!");
      Document.AddScannedDocument(sd);
      if (DeleteFile) File.Delete(Filename);

      base.ApplyChanges();
    }
  }
}

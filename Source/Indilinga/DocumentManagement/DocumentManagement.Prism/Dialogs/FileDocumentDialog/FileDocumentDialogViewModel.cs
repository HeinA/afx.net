﻿using Afx.Business.Documents;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocumentManagement.Prism.Dialogs.FileDocumentDialog
{
  public class FileDocumentDialogViewModel : MdiDialogViewModel<FileDocumentDialogContext>
  {
    [InjectionConstructor]
    public FileDocumentDialogViewModel(IController controller)
      : base(controller)
    {
    }

    [Dependency]
    public FileDocumentDialogController FileDocumentDialogController { get; set; }

    public IEnumerable<DocumentTypeScanType> ScanTypes
    {
      get { return FileDocumentDialogController.Document.Document.DocumentType.ScanTypes.OrderBy(st => st.Text); }
    }
  }
}

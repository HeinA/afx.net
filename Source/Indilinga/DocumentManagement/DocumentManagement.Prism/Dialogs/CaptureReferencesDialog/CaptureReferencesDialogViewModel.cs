﻿using Afx.Business;
using Afx.Business.Documents;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocumentManagement.Prism.Dialogs.CaptureReferencesDialog
{
  public class CaptureReferencesDialogViewModel : MdiDialogViewModel<CaptureReferencesDialogContext>
  {
    [InjectionConstructor]
    public CaptureReferencesDialogViewModel(IController controller)
      : base(controller)
    {
    }

    [Dependency]
    public CaptureReferencesDialogController CaptureReferencesDialogController { get; set; }

    public IEnumerable<DocumentTypeReference> ReferenceTypes
    {
      get { return Document.DocumentType.References.OrderBy(dtr => dtr.Name); }
    }

    #region DocumentTypeReference SelectedDocumentTypeReference

    public const string SelectedDocumentTypeReferenceProperty = "SelectedDocumentTypeReference";
    DocumentTypeReference mSelectedDocumentTypeReference;
    public DocumentTypeReference SelectedDocumentTypeReference
    {
      get { return mSelectedDocumentTypeReference; }
      set { SetProperty<DocumentTypeReference>(ref mSelectedDocumentTypeReference, value); }
    }

    #endregion

    #region string Reference

    public const string ReferenceProperty = "Reference";
    string mReference;
    public string Reference
    {
      get { return mReference; }
      set { SetProperty<string>(ref mReference, value); }
    }

    #endregion

    #region string Note

    public const string NoteProperty = "Note";
    string mNote;
    public string Note
    {
      get { return mNote; }
      set { SetProperty<string>(ref mNote, value); }
    }

    #endregion

    public override void OnEnterPressed()
    {
      base.OnEnterPressed();

      if (!BusinessObject.IsNull(SelectedDocumentTypeReference))
      {
        Document.References.Add(new DocumentReference() { DocumentTypeReference = SelectedDocumentTypeReference, Reference = string.Format(string.IsNullOrWhiteSpace(this.Note) ? "{0}" : "{0} ({1})", this.Reference, this.Note) });
        Reference = string.Empty;
      }
    }

    public Document Document
    {
      get { return CaptureReferencesDialogController.Document; }
    }
  }
}

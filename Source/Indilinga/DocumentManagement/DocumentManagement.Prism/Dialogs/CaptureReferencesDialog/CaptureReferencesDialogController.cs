﻿using Afx.Business.Documents;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace DocumentManagement.Prism.Dialogs.CaptureReferencesDialog
{
  public class CaptureReferencesDialogController : MdiDialogController<CaptureReferencesDialogContext, CaptureReferencesDialogViewModel>
  {
    public const string CaptureReferencesDialogControllerKey = "DocumentManagement.Prism.Dialogs.CaptureReferencesDialog.CaptureReferencesDialogController";

    public CaptureReferencesDialogController(IController controller)
      : base(CaptureReferencesDialogControllerKey, controller)
    {
    }

    #region Document Document

    public const string DocumentProperty = "Document";
    Document mDocument;
    public Document Document
    {
      get { return mDocument; }
      set { mDocument = value; }
    }

    #endregion

    protected override bool OnRun()
    {
      if (DataContext == null) DataContext = new CaptureReferencesDialogContext();
      ViewModel.Caption = "Capture References";
      return base.OnRun();
    }

    protected override void ApplyChanges()
    {
      base.ApplyChanges();
    }
  }
}

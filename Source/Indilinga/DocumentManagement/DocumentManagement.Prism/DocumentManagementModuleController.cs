﻿using Afx.Business;
using Afx.Business.Activities;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Documents;
using Afx.Business.Security;
using Afx.Business.Service;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using Afx.Prism.RibbonMdi.Documents;
using Afx.Prism.RibbonMdi.Events;
using DocumentManagement.Business;
using DocumentManagement.Business.Service;
using DocumentManagement.Prism.Dialogs.FileDocumentDialog;
using DocumentManagement.Prism.Dialogs.FilingDocumentDialog;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DocumentManagement.Prism
{
  public class DocumentManagmentModuleController : MdiModuleController
  {
    [InjectionConstructor]
    public DocumentManagmentModuleController(IController controller)
      : base("DocumentManagement Module Controller", controller)
    {
    }

    FileSystemWatcher mFileSystemWatcher;

    protected override void OnUserAuthenticated(EventArgs obj)
    {
      base.OnUserAuthenticated(obj);
      WatchFolder();

      //using (var svc = ProxyFactory.GetService<IDocumentManagementService>(SecurityContext.ServerName))
      //{
      //  svc.LoadDocument(Guid.NewGuid());
      //}
    }

    protected override void OnRunning()
    {
      base.OnRunning();
      MdiApplicationController.Current.EventAggregator.GetEvent<SettingsChangedEvent>().Subscribe(SettingsChanged);
    }

    private void SettingsChanged(EventArgs obj)
    {
      WatchFolder();
    }

    private void WatchFolder()
    {
      if (mFileSystemWatcher != null)
      {
        mFileSystemWatcher.Created -= PdfCreated;
        mFileSystemWatcher.Renamed -= PdfRenamed;
        mFileSystemWatcher.Dispose();
        mFileSystemWatcher = null;
      }

      if (!BusinessObject.IsNull(SecurityContext.User))
      {
        ScanSetting ss = Setting.GetSetting<ScanSetting>();
        if (!string.IsNullOrWhiteSpace(ss.Folder))
        {
          DirectoryInfo di = new DirectoryInfo(ss.Folder);
          if (!di.Exists) di.Create();

          mFileSystemWatcher = new FileSystemWatcher(ss.Folder, "*.pdf");
          if (ss.ScannerType.FileOnCreate) mFileSystemWatcher.Created += PdfCreated;
          else mFileSystemWatcher.Renamed += PdfRenamed;
          mFileSystemWatcher.EnableRaisingEvents = true;
        }
      }
    }

    void PdfRenamed(object sender, RenamedEventArgs e)
    {
      ProcessPdf(e.FullPath);
    }

    void PdfCreated(object sender, FileSystemEventArgs e)
    {
      ProcessPdf(e.FullPath);
    }

    void ProcessPdf(string fullPath)
    {
      IDocumentRepositoryAware dra = MdiApplicationController.Current.ActiveDocument as IDocumentRepositoryAware;
      if (dra != null)
      {
        try
        {
          MdiApplicationController.Current.Dispatcher.BeginInvoke(new Action(() =>
          {
            try
            {
              var fdc = CreateChildController<FileDocumentDialogController>();
              fdc.Filename = fullPath;
              fdc.Document = dra;
              fdc.Run();
            }
            catch (Exception ex)
            {
              if (ExceptionHelper.HandleException(ex)) throw;
            }
          }), System.Windows.Threading.DispatcherPriority.Normal);
        }
        catch (Exception ex)
        {
          if (ExceptionHelper.HandleException(ex)) throw;
        }
      }
    }
  }
}

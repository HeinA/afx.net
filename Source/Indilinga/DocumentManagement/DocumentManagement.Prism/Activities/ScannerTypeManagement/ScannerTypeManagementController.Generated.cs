﻿using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Prism.RibbonMdi.Activities;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.Composition;

namespace DocumentManagement.Prism.Activities.ScannerTypeManagement
{
  //**************************************************************************************************************
  //**************************************************************************************************************
  //*                                                                                                            *
  //*     This code is auto-generated.  Do not place any cutom code here, it will be lost on re-generation.      *
  //*                                                                                                            *
  //**************************************************************************************************************
  //**************************************************************************************************************
  [Export("Controller:" + global::DocumentManagement.Prism.Activities.ScannerTypeManagement.ScannerTypeManagement.Key)]
  public partial class ScannerTypeManagementController
  {
  }
}

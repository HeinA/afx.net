﻿using Afx.Business.Security;
using Afx.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Activities;
using DocumentManagement.Business;

namespace DocumentManagement.Prism.Activities.ScannerTypeManagement
{
  public partial class ScannerTypeManagementViewModel : MdiSimpleActivityViewModel<ScannerTypeManagement>
  {
    #region Constructors

    [InjectionConstructor]
    public ScannerTypeManagementViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion
  }
}

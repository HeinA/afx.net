﻿using DocumentManagement.Business;
using DocumentManagement.Business.Service;
using Afx.Business.Service;
using Afx.Prism.RibbonMdi.Activities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.Composition;
using Afx.Business.Security;

namespace DocumentManagement.Prism.Activities.ScannerTypeManagement
{
  //**************************************************************************************************************
  //**************************************************************************************************************
  //*                                                                                                            *
  //*     This code is auto-generated.  Do not place any cutom code here, it will be lost on re-generation.      *
  //*                                                                                                            *
  //**************************************************************************************************************
  //**************************************************************************************************************
  [Export("Context:" + ScannerTypeManagement.Key)]
  public partial class ScannerTypeManagement
  {
    public const string Key = "{94269687-7111-4277-b2ea-9b8cb208e0c0}";

    public override void LoadData()
    {
      using (var svc = ProxyFactory.GetService<IDocumentManagementService>(SecurityContext.MasterServer.Name))
      {
        Data = svc.LoadScannerTypeCollection();
      }
      base.LoadData();
    }

    public override void SaveData()
    {
      using (var svc = ProxyFactory.GetService<IDocumentManagementService>(SecurityContext.MasterServer.Name))
      {
        Data = svc.SaveScannerTypeCollection(Data);
      }
      base.SaveData();
    }
  }
}

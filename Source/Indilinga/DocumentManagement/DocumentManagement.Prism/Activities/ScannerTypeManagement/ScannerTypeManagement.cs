﻿using Afx.Business.Activities;
using DocumentManagement.Business;
using DocumentManagement.Business.Service;
using Afx.Prism.RibbonMdi.Activities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocumentManagement.Prism.Activities.ScannerTypeManagement
{
  public partial class ScannerTypeManagement : SimpleActivityContext<ScannerType>
  {
    public ScannerTypeManagement()
    {
    }

    public ScannerTypeManagement(Activity activity)
      : base(activity)
    {
    }
  }
}

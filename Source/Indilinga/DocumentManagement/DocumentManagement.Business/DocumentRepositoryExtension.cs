﻿using Afx.Business;
using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Documents;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace DocumentManagement.Business
{
  [PersistantObject(Schema = "DocumentManagement")]
  [DataContract(IsReference = true, Namespace = DocumentManagement.Business.Namespace.DocumentManagement)]
  public partial class DocumentRepositoryExtension : ExtensionObject<Document>
  {
    #region Constructors

    public DocumentRepositoryExtension()
    {
    }

    #endregion

    #region BusinessObjectCollection<ScannedDocument> ScannedDocuments

    public const string ScannedDocumentsProperty = "ScannedDocuments";
    BusinessObjectCollection<ScannedDocument> mScannedDocuments;
    [PersistantCollection]
    [DataMember]
    public BusinessObjectCollection<ScannedDocument> ScannedDocuments
    {
      get { return mScannedDocuments ?? (mScannedDocuments = new BusinessObjectCollection<ScannedDocument>(this)); }
    }

    #endregion
  }
}

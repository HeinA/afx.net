﻿using Afx.Business;
using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Validation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.Runtime.Serialization;

namespace DocumentManagement.Business
{
  [PersistantObject(Schema = "DocumentManagement")]
  [Export(typeof(Setting))]
  public partial class ScanSetting : MachineSetting 
  {
    #region Constructors

    public ScanSetting()
    {
    }

    #endregion

    #region string Name

    public override string Name
    {
      get { return "Scanning"; }
    }

    #endregion

    #region string SettingGroupIdentifier

    protected override string SettingGroupIdentifier
    {
      get { return "da82bb79-1b21-4186-832e-08a39476c141"; }
    }

    #endregion

    #region ScannerType ScannerType

    public const string ScannerTypeProperty = "ScannerType";
    [PersistantProperty(IsCached = true)]
    public ScannerType ScannerType
    {
      get { return GetCachedObject<ScannerType>(); }
      set { SetCachedObject<ScannerType>(value); }
    }

    #endregion

    #region string Folder

    public const string FolderProperty = "Folder";
    [DataMember(Name = FolderProperty, EmitDefaultValue = false)]
    string mFolder;
    [PersistantProperty]
    public string Folder
    {
      get { return mFolder; }
      set { SetProperty<string>(ref mFolder, value); }
    }

    #endregion
  }
}

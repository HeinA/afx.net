﻿using Afx.Business.Documents;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocumentManagement.Business
{
  public interface IDocumentRepositoryAware
  {
    void AddScannedDocument(ScannedDocument document);
    Document Document { get; }
  }
}

﻿using Afx.Business;
using Afx.Business.Collections;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace DocumentManagement.Business.Service
{
  [ServiceContract(Namespace = Namespace.DocumentManagement)]
  public partial interface IDocumentManagementService : IDisposable
  {
    [OperationContract]
    void FileDocument(Guid id, byte[] data);

    [OperationContract]
    byte[] LoadDocument(Guid id);
  }
}

﻿using Afx.Business.Collections;
using Afx.Business.Service;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.WCF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace DocumentManagement.Business.Service
{
  public partial class DocumentManagementService
  {
    public BasicCollection<DocumentManagement.Business.ScannerType> LoadScannerTypeCollection()
    {
      try
      {
        return GetAllInstances<DocumentManagement.Business.ScannerType>();
      }
      catch
      {
        throw;
      }
    }

    public BasicCollection<DocumentManagement.Business.ScannerType> SaveScannerTypeCollection(BasicCollection<DocumentManagement.Business.ScannerType> col)
    {
      try
      {
        return Persist<DocumentManagement.Business.ScannerType>(col);
      }
      catch
      {
        throw;
      }
    }
  }
}
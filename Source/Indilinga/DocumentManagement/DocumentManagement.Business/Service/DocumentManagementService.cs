﻿using Afx.Business;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Business.Service;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.WCF;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Configuration;
using System.Data;
using System.Data.Odbc;
using System.Data.SqlClient;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace DocumentManagement.Business.Service
{
  [ServiceBehavior(Namespace = Namespace.DocumentManagement, ConcurrencyMode = ConcurrencyMode.Multiple)]
  [ExceptionShielding]
  [Export(typeof(IService))]
  public partial class DocumentManagementService : ServiceBase, IDocumentManagementService
  {
    public void FileDocument(Guid id, byte[] data)
    {
      try
      {
        string cs = ConfigurationManager.ConnectionStrings["ScannedDocuments"].ConnectionString;
        using (SqlConnection con = new SqlConnection(cs))
        {
          con.Open();

          using (SqlCommand cmd = new SqlCommand("INSERT INTO dbo.ScannedDocument (Id, Document) VALUES (@id, @doc)", con))
          {
            cmd.Parameters.AddWithValue("@id", id);
            cmd.Parameters.AddWithValue("@doc", data);
            cmd.ExecuteNonQuery();
          }

          con.Close();
        }
      }
      catch
      {
        throw;
      }
    }

    public byte[] LoadDocument(Guid id)
    {
      byte[] doc = null;

      string cs = ConfigurationManager.ConnectionStrings["ScannedDocuments"].ConnectionString;
      using (SqlConnection con = new SqlConnection(cs))
      {
        con.Open();

        using (SqlCommand cmd = new SqlCommand("SELECT [Document] FROM dbo.ScannedDocument WHERE Id=@id", con))
        {
          cmd.Parameters.AddWithValue("@id", id);
          doc = (byte[])cmd.ExecuteScalar();
        }

        con.Close();
      }

      try
      {
        if (doc == null && !BusinessObject.IsNull(SecurityContext.MasterServer))
        {
          using (var svc = ServiceFactory.GetService<IDocumentManagementService>())
          {
            if (svc == null) return null;
            return svc.Instance.LoadDocument(id);
          }
        }
      }
      catch { }

      return doc;
    }
  }
}

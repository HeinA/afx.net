﻿using Afx.Business.Collections;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace DocumentManagement.Business.Service
{
  public partial interface IDocumentManagementService
  {
    [OperationContract]
    BasicCollection<DocumentManagement.Business.ScannerType> LoadScannerTypeCollection();

    [OperationContract]
    BasicCollection<DocumentManagement.Business.ScannerType> SaveScannerTypeCollection(BasicCollection<DocumentManagement.Business.ScannerType> col);
  }
}


﻿using Afx.Business;
using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Documents;
using Afx.Business.Security;
using Afx.Business.Validation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace DocumentManagement.Business
{
  [PersistantObject(Schema = "DocumentManagement")]
  public partial class ScannedDocument : BusinessObject<DocumentRepositoryExtension> 
  {
    #region Constructors

    public ScannedDocument()
    {
    }

    #endregion

    #region DateTime Timestamp

    public const string TimestampProperty = "Timestamp";
    [DataMember(Name = TimestampProperty, EmitDefaultValue = false)]
    DateTime mTimestamp = DateTime.Now;
    [PersistantProperty]
    public DateTime Timestamp
    {
      get { return mTimestamp; }
      set { SetProperty<DateTime>(ref mTimestamp, value); }
    }

    #endregion

    #region UserReference User

    public const string UserProperty = "User";
    [PersistantProperty(IsCached = true)]
    public UserReference User
    {
      get { return GetCachedObject<UserReference>(); }
      set { SetCachedObject<UserReference>(value); }
    }

    #endregion

    #region DocumentTypeScanType ScanType

    public const string ScanTypeProperty = "ScanType";
    [PersistantProperty(IsCached = true)]
    public DocumentTypeScanType ScanType
    {
      get { return GetCachedObject<DocumentTypeScanType>(); }
      set { SetCachedObject<DocumentTypeScanType>(value); }
    }

    #endregion

    #region string Description

    public const string DescriptionProperty = "Description";
    [DataMember(Name = DescriptionProperty, EmitDefaultValue = false)]
    string mDescription;
    [PersistantProperty]
    public string Description
    {
      get { return mDescription; }
      set { SetProperty<string>(ref mDescription, value); }
    }

    #endregion

    #region string FileExtension

    public const string FileExtensionProperty = "FileExtension";
    [DataMember(Name = FileExtensionProperty, EmitDefaultValue = false)]
    string mFileExtension;
    [PersistantProperty]
    public string FileExtension
    {
      get { return mFileExtension; }
      set { SetProperty<string>(ref mFileExtension, value); }
    }

    #endregion
  }
}

﻿using Afx.Business;
using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Documents;
using Afx.Business.Validation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace DocumentManagement.Business
{
  [PersistantObject(Schema = "Afx", TableName = "Document")]
  public partial class DocumentReference : BusinessObject 
  {
    #region Constructors

    public DocumentReference(Document doc)
    {
      GlobalIdentifier = doc.GlobalIdentifier;
      Id = doc.Id;
      IsNew = false;
      IsDirty = false;
    }

    #endregion
  }
}

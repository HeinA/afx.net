﻿using Afx.Business;
using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Validation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace DocumentManagement.Business
{
  [PersistantObject(Schema = "DocumentManagement")]
  [Cache]
  public partial class ScannerType : BusinessObject, IRevisable 
  {
    #region Constructors

    public ScannerType()
    {
    }

    public ScannerType(bool isNull)
      : base(isNull)
    {
    }

    #endregion

    #region int Revision

    public const string RevisionProperty = "Revision";
    [DataMember(Name = RevisionProperty, EmitDefaultValue = false)]
    int mRevision;
    [PersistantProperty]
    public int Revision
    {
      get { return mRevision; }
      set { SetProperty<int>(ref mRevision, value); }
    }

    #endregion

    #region string Name

    public const string NameProperty = "Name";
    [DataMember(Name = NameProperty, EmitDefaultValue = false)]
    string mName;
    [PersistantProperty]
    public string Name
    {
      get { return mName; }
      set { SetProperty<string>(ref mName, value); }
    }

    #endregion

    #region bool FileOnCreate

    public const string FileOnCreateProperty = "FileOnCreate";
    [DataMember(Name = FileOnCreateProperty, EmitDefaultValue = false)]
    bool mFileOnCreate;
    [PersistantProperty]
    public bool FileOnCreate
    {
      get { return mFileOnCreate; }
      set
      {
        if (SetProperty<bool>(ref mFileOnCreate, value))
        {
          FileOnRename = !FileOnCreate;
        }
      }
    }

    #endregion

    #region bool FileOnRename

    public const string FileOnRenameProperty = "FileOnRename";
    [DataMember(Name = FileOnRenameProperty, EmitDefaultValue = false)]
    bool mFileOnRename;
    [PersistantProperty]
    public bool FileOnRename
    {
      get { return mFileOnRename; }
      set
      {
        if (SetProperty<bool>(ref mFileOnRename, value))
        {
          FileOnCreate = !FileOnRename;
        }
      }
    }

    #endregion
  }
}

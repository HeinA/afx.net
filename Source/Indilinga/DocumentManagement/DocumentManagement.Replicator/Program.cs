﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocumentManagement.Replicator
{
  class Program
  {
    static void Main(string[] args)
    {
      string local = ConfigurationManager.ConnectionStrings["Local"].ConnectionString;
      string master = ConfigurationManager.ConnectionStrings["Master"].ConnectionString;

      using (var conLocal = new SqlConnection(local))
      using (var conMaster = new SqlConnection(master))
      {
        conLocal.Open();
        conMaster.Open();


        using (var cmdNonReplicated = new SqlCommand("SELECT Id FROM ScannedDocument WHERE Replicated=0", conLocal))
        {
          DataSet dsNonReplicated = ExecuteDataSet(cmdNonReplicated);
          foreach (DataRow drNonReplicated in dsNonReplicated.Tables[0].Rows)
          {
            try
            {
              using (var cmdIsReplicated = new SqlCommand("SELECT COUNT(1) FROM ScannedDocument WHERE Id=@id", conMaster))
              {
                cmdIsReplicated.Parameters.AddWithValue("@id", drNonReplicated["Id"]);
                int count = (int)cmdIsReplicated.ExecuteScalar();

                if (count == 0)
                {
                  Console.Write("Replicating Document {0}...", drNonReplicated["Id"]);

                  byte[] blob = null;

                  using (var cmdBlob = new SqlCommand("SELECT [Document] FROM ScannedDocument WHERE Id=@id", conLocal))
                  {
                    cmdBlob.Parameters.AddWithValue("@id", drNonReplicated["Id"]);
                    blob = (byte[])cmdBlob.ExecuteScalar();
                  }

                  using (var cmdUpdateBlob = new SqlCommand("INSERT INTO ScannedDocument (Id, [Document]) VALUES (@id, @blob)", conMaster))
                  {
                    cmdUpdateBlob.Parameters.AddWithValue("@blob", blob);
                    cmdUpdateBlob.Parameters.AddWithValue("@id", drNonReplicated["Id"]);
                    cmdUpdateBlob.ExecuteNonQuery();
                  }

                  Console.WriteLine("done!");
                }

                using (var cmdMarkReplicated = new SqlCommand("UPDATE ScannedDocument SET Replicated=1 WHERE Id=@id", conLocal))
                {
                  cmdMarkReplicated.Parameters.AddWithValue("@id", drNonReplicated["Id"]);
                  cmdMarkReplicated.ExecuteNonQuery();
                }
              }
            }
            catch(Exception ex)
            {
              Console.WriteLine(ex);
            }
          }
        }

        conMaster.Close();
        conLocal.Close();
      }
    }

    #region ExecuteDataSet()

    public static DataSet ExecuteDataSet(IDbCommand cmd)
    {
      System.Data.DataSet ds = new System.Data.DataSet();
      ds.EnforceConstraints = false;
      ds.Locale = CultureInfo.InvariantCulture;
      using (IDataReader r = cmd.ExecuteReader())
      {
        ds.Load(r, LoadOption.OverwriteChanges, string.Empty);
        r.Close();
      }

      return ds;
    }

    #endregion
  }
}

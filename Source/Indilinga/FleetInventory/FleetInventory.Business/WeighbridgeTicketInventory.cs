﻿using Afx.Business;
using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Validation;
using FleetManagement.Business;
using InventoryControl.Business;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace FleetInventory.Business
{
  [PersistantObject(Schema = "FleetIC")]
  public partial class WeighbridgeTicketInventory : ExtensionObject<WeighbridgeTicket>
  {
    #region Constructors

    public WeighbridgeTicketInventory()
    {
    }

    #endregion

    #region BusinessObjectCollection<WeighbridgeTicketInventoryItem> Items

    public const string ItemsProperty = "Items";
    BusinessObjectCollection<WeighbridgeTicketInventoryItem> mItems;
    [PersistantCollection]
    [DataMember]
    [Mandatory("There must be atleast one inventory item.")]
    public BusinessObjectCollection<WeighbridgeTicketInventoryItem> Items
    {
      get { return mItems ?? (mItems = new BusinessObjectCollection<WeighbridgeTicketInventoryItem>(this)); }
    }

    #endregion

    #region InventoryPostingType InventoryPostingType

    public const string InventoryPostingTypeProperty = "InventoryPostingType";
    [DataMember(Name = InventoryPostingTypeProperty, EmitDefaultValue = false)]
    InventoryPostingType mInventoryPostingType;
    [PersistantProperty]
    [Mandatory("Inventory Direction is mandatory.")]
    public InventoryPostingType InventoryPostingType
    {
      get { return mInventoryPostingType; }
      set { SetProperty<InventoryPostingType>(ref mInventoryPostingType, value); }
    }

    #endregion
  }
}

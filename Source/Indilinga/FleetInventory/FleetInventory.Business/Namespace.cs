﻿using Afx.Business;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FleetInventory.Business
{
  [Export(typeof(INamespace))]
  public class Namespace : INamespace
  {
    public const string FleetInventory = "http://indilinga.com/FleetIC/Business";

    public string GetUri()
    {
      return FleetInventory;
    }
  }
}

﻿using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Documents;
using FleetManagement.Business;
using InventoryControl.Business;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FleetInventory.Business.EventManagers.DocumentState
{
  [Export(typeof(IDocumentStateEventManager))]
  public class WeighbridgeFinalizedEventManager : DocumentStateEventManager<WeighbridgeTicket>
  {
    public override string TargetStateIdentifier
    {
      get { return WeighbridgeTicket.States.Finalized; }
    }

    public override void Process(WeighbridgeTicket doc, DocumentTypeState originalState)
    {
      try
      {
        BasicCollection<InventoryPosting> col = new BasicCollection<InventoryPosting>();
        WeighbridgeTicketInventory wii = doc.GetExtensionObject<WeighbridgeTicketInventory>();
        foreach (WeighbridgeTicketInventoryItem ii in wii.Items)
        {
          InventoryPosting p = new InventoryPosting() { Timestamp = DateTime.Now, OrganizationalUnit = doc.OrganizationalUnit, InventoryItem = ii.Item, Document = doc, InventoryPostingType = wii.InventoryPostingType, Quantity = ii.Quantity, QUOM = ii.QuantityUOM };
          col.Add(p);
        }

        ObjectRepository<InventoryPosting> or = PersistanceManager.GetRepository<InventoryPosting>();
        or.Persist(col);
      }
      catch
      {
        throw;
      }
    }
  }
}

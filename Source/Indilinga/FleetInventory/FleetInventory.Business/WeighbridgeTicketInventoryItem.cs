﻿using Afx.Business;
using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using FleetManagement.Business;
using InventoryControl.Business;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace FleetInventory.Business
{
  [PersistantObject(Schema = "FleetIC")]
  public partial class WeighbridgeTicketInventoryItem : BusinessObject<WeighbridgeTicketInventory> 
  {
    #region Constructors

    public WeighbridgeTicketInventoryItem()
    {
    }

    #endregion

    #region InventoryItem Item

    public const string ItemProperty = "Item";
    [PersistantProperty]
    public InventoryItem Item
    {
      get { return GetCachedObject<InventoryItem>(); }
      set
      {
        if (SetCachedObject<InventoryItem>(value) && IsNull(QuantityUOM))
        {
          QuantityUOM = Item.SaleQUOM;
        }
      }
    }

    #endregion

    #region decimal Quantity

    public const string QuantityProperty = "Quantity";
    [DataMember(Name = QuantityProperty, EmitDefaultValue = false)]
    decimal mQuantity;
    [PersistantProperty]
    public decimal Quantity
    {
      get { return mQuantity; }
      set { SetProperty<decimal>(ref mQuantity, value); }
    }

    #endregion

    #region InventoryItemQUOM QuantityUOM

    public const string QuantityUOMProperty = "QuantityUOM";
    [PersistantProperty]
    public InventoryItemQUOM QuantityUOM
    {
      get { return GetCachedObject<InventoryItemQUOM>(); }
      set { SetCachedObject<InventoryItemQUOM>(value); }
    }

    #endregion
  }
}

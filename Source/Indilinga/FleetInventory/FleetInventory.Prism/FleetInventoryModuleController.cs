﻿using Afx.Business;
using Afx.Business.Activities;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Documents;
using Afx.Business.Security;
using Afx.Business.Service;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using Afx.Prism.RibbonMdi.Documents;
using Afx.Prism.RibbonMdi.Events;
using FleetInventory.Business;
using FleetInventory.Prism.Extensions.WeighbridgeTicket;
using FleetInventory.Prism.Extensions.WeighbridgeTicket.EditInventoryDialog;
using FleetManagement.Business;
using FleetManagement.Prism.Documents.WeighbridgeTicket;
using InventoryControl.Business;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace FleetInventory.Prism
{
  public class FleetInventoryModuleController : MdiModuleController
  {
    [InjectionConstructor]
    public FleetInventoryModuleController(IController controller)
      : base("FleetInventory Module Controller", controller)
    {
    }

    protected override void AfterContainerConfigured()
    {
      this.EventAggregator.GetEvent<ExtendDocumentEvent>().Subscribe(OnExtendDocument);
      ApplicationController.EventAggregator.GetEvent<ExecuteOperationEvent>().Subscribe(OnExecuteOperation);

      base.AfterContainerConfigured();
    }

    private void OnExecuteOperation(ExecuteOperationEventArgs obj)
    {
      switch (obj.Operation.Identifier)
      {
        case Extensions.WeighbridgeTicket.Operations.EditInventory:
          EditInventoryDialogController c = obj.Controller.CreateChildController<EditInventoryDialogController>();
          c.WeighbridgeTicketController = (WeighbridgeTicketController)obj.Controller;
          c.Run();
          break;
      }
    }

    private void OnExtendDocument(ExtendDocumentEventArgs obj)
    {
      if (obj.Document is WeighbridgeTicket)
      {
        obj.Controller.ExtendDocument<WeighbridgeTicketInventory, WeighbridgeTicketInventoryViewModel>(Regions.DockRegion);
        obj.Controller.ExtendDocument<WeighbridgeTicketInventory, WeighbridgeTicketDetailViewModel>(Regions.TopDetailsRegion);
      }
    }
  }
}

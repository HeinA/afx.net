﻿using Afx.Prism.RibbonMdi;
using FleetInventory.Business;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FleetInventory.Prism
{
  [Export(typeof(IModuleInfo))]
  public class ModuleInfo : IModuleInfo
  {
    public string ModuleNamespace
    {
      get { return Namespace.FleetInventory; }
    }

    public string[] ModuleDependencies
    {
      get { return new string[] { Afx.Business.Namespace.Afx, AccountManagement.Business.Namespace.AccountManagement, InventoryControl.Business.Namespace.InventoryControl, FleetManagement.Business.Namespace.FleetManagement }; }
    }

    public Type ModuleController
    {
      get { return typeof(FleetInventoryModuleController); }
    }
  }
}
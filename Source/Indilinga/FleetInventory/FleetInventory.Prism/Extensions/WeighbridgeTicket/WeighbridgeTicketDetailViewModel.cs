﻿using Afx.Prism;
using Afx.Prism.RibbonMdi.Documents;
using FleetInventory.Business;
using InventoryControl.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FleetInventory.Prism.Extensions.WeighbridgeTicket
{
  public class WeighbridgeTicketDetailViewModel : ViewModel<WeighbridgeTicketInventory>, IExtensionViewModel
  {
    #region Constructors

    [InjectionConstructor]
    public WeighbridgeTicketDetailViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    #region InventoryPostingType InventoryPostingType

    public InventoryPostingType InventoryPostingType
    {
      get { return Model == null ? GetDefaultValue<InventoryPostingType>() : Model.InventoryPostingType; }
      set { Model.InventoryPostingType = value; }
    }

    #endregion

    #region bool IsReadOnly

    bool mIsReadOnly = false;
    public override bool IsReadOnly
    {
      get { return mIsReadOnly; }
    }

    bool IExtensionViewModel.IsReadOnly
    {
      set { mIsReadOnly = value; }
    }

    #endregion
  }
}

﻿using Afx.Prism;
using Afx.Prism.RibbonMdi;
using FleetInventory.Business;
using FleetManagement.Prism.Documents.WeighbridgeTicket;
using InventoryControl.Business;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace FleetInventory.Prism.Extensions.WeighbridgeTicket.EditInventoryDialog
{
  public class EditInventoryDialogController : MdiDialogController<EditInventoryDialogContext, EditInventoryDialogViewModel>
  {
    public const string EditInventoryDialogControllerKey = "FleetInventory.Prism.Extensions.WeighbridgeTicket.EditInventoryDialog.EditInventoryDialogController";

    public EditInventoryDialogController(IController controller)
      : base(EditInventoryDialogControllerKey, controller)
    {
    }

    Collection<InventoryItem> mItems;
    public Collection<InventoryItem> Items
    {
      get { return mItems; }
      set { mItems = value; }
    }

    #region WeighbridgeTicketController WeighbridgeTicketController

    WeighbridgeTicketController mWeighbridgeTicketController;
    public WeighbridgeTicketController WeighbridgeTicketController
    {
      get { return mWeighbridgeTicketController; }
      set
      {
        mWeighbridgeTicketController = value;
        Items = new Collection<InventoryItem>();
        foreach (var i in mWeighbridgeTicketController.DataContext.Document.GetExtensionObject<WeighbridgeTicketInventory>().Items)
        {
          if (!i.IsDeleted) Items.Add(i.Item);
        }
      }
    }

    #endregion

    protected override bool OnRun()
    {
      if (DataContext == null) DataContext = new EditInventoryDialogContext();
      ViewModel.Caption = "Edit Inventory";
      return base.OnRun();
    }

    protected override void ApplyChanges()
    {
      WeighbridgeTicketInventory inventory = mWeighbridgeTicketController.DataContext.Document.GetExtensionObject<WeighbridgeTicketInventory>();
      foreach (var i in Items)
      {
        WeighbridgeTicketInventoryItem ii = inventory.Items.FirstOrDefault(ii1 => ii1.Item.Equals(i));
        if (ii == null)
        {
          ii = new WeighbridgeTicketInventoryItem() { Item = i };
          inventory.Items.Add(ii);
        }
        else
        {
          ii.IsDeleted = false;
        }
      }

      foreach (var ii in inventory.Items)
      {
        if (!Items.Contains(ii.Item))
        {
          ii.IsDeleted = true;
        }
      }

      IRegion tabRegion = WeighbridgeTicketController.RegionManager.Regions[Afx.Prism.RibbonMdi.Documents.Regions.TabRegion];
      foreach (object o in tabRegion.Views)
      {
        if (o is WeighbridgeTicketInventoryViewModel)
        {
          tabRegion.Activate(o);
        }
      }
      base.ApplyChanges();
    }
  }
}

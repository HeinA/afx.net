﻿using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using InventoryControl.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FleetInventory.Prism.Extensions.WeighbridgeTicket.EditInventoryDialog
{
  public class EditInventoryDialogViewModel : MdiDialogViewModel<EditInventoryDialogContext>
  {
    #region Constructor

    [InjectionConstructor]
    public EditInventoryDialogViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    #region EditInventoryDialogController EditInventoryDialogController

    EditInventoryDialogController mEditInventoryDialogController;
    [Dependency]
    public EditInventoryDialogController EditInventoryDialogController
    {
      get { return mEditInventoryDialogController; }
      set { mEditInventoryDialogController = value; }
    }

    #endregion

    IList mItems;
    IList Items
    {
      get { return mItems ?? (mItems = Cache.Instance.GetObjects<InventoryItem>(ii => ii.ApplicableOrganizationalUnits.Contains(EditInventoryDialogController.WeighbridgeTicketController.DataContext.Document.OrganizationalUnit))); }
    }

    public IEnumerable<ItemViewModel> ItemViewModels
    {
      get { return ResolveViewModels<ItemViewModel, InventoryItem>(Items); }
    }
  }
}

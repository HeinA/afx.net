﻿using Afx.Prism;
using Afx.Prism.RibbonMdi.Documents;
using FleetInventory.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FleetInventory.Prism.Extensions.WeighbridgeTicket
{
  public class WeighbridgeTicketInventoryViewModel : ViewModel<WeighbridgeTicketInventory>, IExtensionViewModel
  {
    #region Constructors

    [InjectionConstructor]
    public WeighbridgeTicketInventoryViewModel(IController controller)
      : base(controller)
    {
      //Title = "_Inventory";
    }

    #endregion

    #region Items

    public IEnumerable<WeighbridgeTicketInventoryItem> Items
    {
      get { return Model.Items; }
    }

    #endregion

    bool mIsReadOnly = false;
    public override bool IsReadOnly
    {
      get { return mIsReadOnly; }
    }

    bool IExtensionViewModel.IsReadOnly
    {
      set { mIsReadOnly = value; }
    }
  }
}

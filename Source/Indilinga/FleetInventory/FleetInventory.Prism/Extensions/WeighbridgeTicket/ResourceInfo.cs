﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FleetInventory.Prism.Extensions.WeighbridgeTicket
{
  [Export(typeof(Afx.Prism.RibbonMdi.ResourceInfo))]
  public class ResourceInfo : Afx.Prism.RibbonMdi.ResourceInfo
  {
    public override Uri GetUri()
    {
      return new Uri("pack://application:,,,/FleetInventory.Prism;component/Extensions/WeighbridgeTicket/Resources.xaml", UriKind.Absolute);
    }
  }
}

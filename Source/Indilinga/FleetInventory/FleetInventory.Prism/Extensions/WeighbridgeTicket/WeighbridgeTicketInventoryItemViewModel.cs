﻿using Afx.Prism;
using FleetInventory.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FleetInventory.Prism.Extensions.WeighbridgeTicket
{
  public class WeighbridgeTicketInventoryItemViewModel : SelectableViewModel<WeighbridgeTicketInventoryItem>
  {
    #region Constructors

    [InjectionConstructor]
    public WeighbridgeTicketInventoryItemViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    #region string ItemDescription

    public string ItemDescription
    {
      get { return Model == null ? GetDefaultValue<string>() : Model.Item.Description; }
    }

    #endregion

    #region decimal Quantity

    public decimal Quantity
    {
      get { return Model == null ? GetDefaultValue<decimal>() : Model.Quantity; }
      set { Model.Quantity = value; }
    }

    #endregion

  }
}

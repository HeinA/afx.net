﻿using Afx.Business.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PastelPartnerIntegration.Business
{
  public class TaxTypeAttribute : ValidationAttribute
  {
    public TaxTypeAttribute()
      : base("Tax Type must be between 0 and 30")
    {
    }

    public override bool Validate(object value)
    {
      if (value == null) return false;
      if (!(value is int)) return false;

      int i = (int)value;
      if (i < 0 || i > 30) return false;
      return true;
    }
  }
}

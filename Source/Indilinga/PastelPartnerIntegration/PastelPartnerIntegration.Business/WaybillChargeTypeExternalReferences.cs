﻿using Afx.Business;
using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using FreightManagement.Business;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace PastelPartnerIntegration.Business
{
  [PersistantObject(Schema = "PastelPartnerIntegration")]
  public partial class WaybillChargeTypeExternalReferences : ExtensionObject<WaybillChargeType>
  {
    #region Constructors

    public WaybillChargeTypeExternalReferences()
    {
    }

    #endregion

    #region bool AllowAccountTaxTypeOverride

    public const string AllowAccountTaxTypeOverrideProperty = "AllowAccountTaxTypeOverride";
    [DataMember(Name = AllowAccountTaxTypeOverrideProperty, EmitDefaultValue = false)]
    bool mAllowAccountTaxTypeOverride;
    [PersistantProperty]
    public bool AllowAccountTaxTypeOverride
    {
      get { return mAllowAccountTaxTypeOverride; }
      set { SetProperty<bool>(ref mAllowAccountTaxTypeOverride, value); }
    }

    #endregion

    #region BusinessObjectCollection<WaybillChargeTypeExternalReference> References

    public const string ReferencesProperty = "References";
    BusinessObjectCollection<WaybillChargeTypeExternalReference> mReferences;
    [PersistantCollection]
    [DataMember]
    public BusinessObjectCollection<WaybillChargeTypeExternalReference> References
    {
      get { return mReferences ?? (mReferences = new BusinessObjectCollection<WaybillChargeTypeExternalReference>(this)); }
    }

    #endregion

    public void SyncSystems()
    {
      foreach (var es in Cache.Instance.GetObjects<ExternalSystem>(es1 => es1.IsFlagged(ExternalSystemFlags.PastelPartner)))
      {
        if (!References.Any(r => r.ExternalSystem.Equals(es)))
        {
          References.Add(new WaybillChargeTypeExternalReference() { ExternalSystem = es });
        }
      }
    }
  }
}

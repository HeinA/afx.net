﻿using Afx.Business;
using Afx.Business.Collections;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace PastelPartnerIntegration.Business.Service
{
  [ServiceContract(Namespace = Namespace.PastelPartnerIntegration)]
  public partial interface IPastelPartnerIntegrationService : IDisposable
  {
    [OperationContract]
    BasicCollection<PendingInvoice> GetInvoices();

    [OperationContract]
    void Export(BasicCollection<PendingInvoice> invoices);

    [OperationContract]
    bool ValidateAccount(string connectionName, string accountNumber);
  }
}

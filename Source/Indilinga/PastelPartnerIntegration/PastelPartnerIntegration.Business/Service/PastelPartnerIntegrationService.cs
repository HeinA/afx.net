﻿using Afx.Business;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Business.Service;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.WCF;
using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Configuration;
using System.Data;
using System.Data.Odbc;
using System.Data.SqlClient;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using Pastel.Interface;
using System.Collections.ObjectModel;
using FreightManagement.Business;
using Afx.Business.Documents;

namespace PastelPartnerIntegration.Business.Service
{
  [ServiceBehavior(Namespace = Namespace.PastelPartnerIntegration, ConcurrencyMode = ConcurrencyMode.Multiple)]
  [ExceptionShielding]
  [Export(typeof(IService))]
  public partial class PastelPartnerIntegrationService : ServiceBase, IPastelPartnerIntegrationService
  {
    public BasicCollection<PendingInvoice> GetInvoices()
    {
      try
      {
        return GetAllInstances<PendingInvoice>();
      }
      catch
      {
        throw;
      }
    }

    public bool ValidateAccount(string connectionName, string accountNumber)
    {
      using (var svc = Pastel.Interface.ServiceFactory.GetService<Pastel.Interface.IPastelService>())
      {
        return svc.Instance.ValidateAccount(connectionName, accountNumber);
      }
    }

    public void Export(BasicCollection<PendingInvoice> invoices)
    {
      try
      {
        Dictionary<string, Collection<InventoryItem>> inventoryItemDictionary = new Dictionary<string, Collection<InventoryItem>>();

        using (var svc = Pastel.Interface.ServiceFactory.GetService<Pastel.Interface.IPastelService>())
        {
          foreach (var invoice in invoices)
          {
            if (!inventoryItemDictionary.ContainsKey(invoice.ExternalSystem.Name))
            {
              inventoryItemDictionary.Add(invoice.ExternalSystem.Name, svc.Instance.GetInventoryItems(invoice.ExternalSystem.Name));
            }
            var inventoryItems = inventoryItemDictionary[invoice.ExternalSystem.Name];

            //var ae = invoice.Account.GetExtensionObject<AccountReferenceExtension>();
            bool exportAsOrder = invoice.ExportAsOrder;

            var doc = new Pastel.Interface.Document();
            if (invoice.Account.PostalAddress != null) doc.Address = invoice.Account.PostalAddress.AddressText;
            doc.CustomerCode = invoice.Account.AccountNumber;
            doc.ClosingDate = DateTime.Now;
            doc.Description = "Imported from IFMS";
            doc.DocumentDate = DateTime.Now;
            //if (ConfigurationManager.AppSettings["Pastel.OverrideDocumentNumber"].ToUpperInvariant() == "TRUE") doc.DocumentNumber = invoice.WaybillNumber;
            doc.ReferenceNumber = invoice.WaybillNumber;

            foreach (var item in invoice.Items)
            {
              var ers = item.ChargeType.GetExtensionObject<WaybillChargeTypeExternalReferences>();
              var er = ers.References.First(e => e.ExternalSystem.Equals(invoice.ExternalSystem));
              var ii = inventoryItems.First(ii1 => ii1.Code == er.Reference);

              var i = new Pastel.Interface.DocumentLine();
              i.LineType = LineType.Inventory;
              i.Code = er.Reference;
              i.Description = item.ChargeType.Text;
              i.Quantity = 1;
              i.ExclusiveUnitSellingPrice = item.Charge;
              if (ii.AllowTax) 
              {
                if (ers.AllowAccountTaxTypeOverride && invoice.TaxType != null)
                {
                  i.TaxType = invoice.TaxType.Value;
                }
                else
                {
                  i.TaxType = ii.SalesTaxType;
                }
              }

              doc.Lines.Add(i);

              string docno = svc.Instance.ImportDocument(invoice.ExternalSystem.Name, exportAsOrder ? Pastel.Interface.DocumentType.SalesOrder : Pastel.Interface.DocumentType.CustomerInvoice, true, doc);

              using (new ConnectionScope())
              using(var ts = new TransactionScope(TransactionScopeOption.Required))
              {
                var waybill = GetInstance<Waybill>(invoice.GlobalIdentifier);
                waybill.InvoiceNumber = docno;
                waybill.InvoiceDate = DateTime.Now;
                DocumentHelper.SetState(waybill, Waybill.States.Invoiced);

                ts.Complete();
              }
            }
          }
        }
      }
      catch
      {
        throw;
      }
    }
  }
}

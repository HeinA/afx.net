﻿using AccountManagement.Business;
using Afx.Business;
using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace PastelPartnerIntegration.Business
{
  [PersistantObject(Schema = "PastelPartnerIntegration", TableName = "AccountExtension", OwnerColumn = "Account")]
  public partial class AccountReferenceExtension : ExtensionObject<AccountReference>
  {
    #region Constructors

    public AccountReferenceExtension()
    {
    }

    #endregion

    #region ExternalSystem ExternalSystem

    public const string ExternalSystemProperty = "ExternalSystem";
    [PersistantProperty(IsCached = true)]
    public ExternalSystem ExternalSystem
    {
      get { return GetCachedObject<ExternalSystem>(); }
      set { SetCachedObject<ExternalSystem>(value); }
    }

    #endregion

    #region int? TaxType

    public const string TaxTypeProperty = "TaxType";
    [DataMember(Name = TaxTypeProperty, EmitDefaultValue = false)]
    short? mTaxType;
    [PersistantProperty]
    public short? TaxType
    {
      get { return mTaxType; }
      set { SetProperty<short?>(ref mTaxType, value); }
    }

    #endregion

    #region bool ExportAsOrder

    public const string ExportAsOrderProperty = "ExportAsOrder";
    [DataMember(Name = ExportAsOrderProperty, EmitDefaultValue = false)]
    bool mExportAsOrder;
    [PersistantProperty]
    public bool ExportAsOrder
    {
      get { return mExportAsOrder; }
      set { SetProperty<bool>(ref mExportAsOrder, value); }
    }

    #endregion
  }
}

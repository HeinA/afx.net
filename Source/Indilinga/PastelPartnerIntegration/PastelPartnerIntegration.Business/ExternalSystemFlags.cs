﻿using Afx.Business;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PastelPartnerIntegration.Business
{
  [Export(typeof(IObjectFlags))]
  public class ExternalSystemFlags : IObjectFlags
  {
    public const string PastelPartner = "Pastel Partner";

    public Type ObjectType
    {
      get { return typeof(ExternalSystem); }
    }

    public IEnumerable<string> Flags
    {
      get { return new string[] { PastelPartner }; }
    }
  }
}

﻿using Afx.Business;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PastelPartnerIntegration.Business
{
  [Export(typeof(INamespace))]
  public class Namespace : INamespace
  {
    public const string PastelPartnerIntegration = "http://indilinga.com/PastelPartnerIntegration/Business";

    public string GetUri()
    {
      return PastelPartnerIntegration; 
    }
  }
}

﻿using Afx.Business;
using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Validation;
using FreightManagement.Business;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace PastelPartnerIntegration.Business
{
  [PersistantObject(Schema = "PastelPartnerIntegration", TableName = "vPendingInvoiceItem")]
  public partial class PendingInvoiceItem : BusinessObject<PendingInvoice> 
  {
    #region Constructors

    public PendingInvoiceItem()
    {
    }

    #endregion

    #region decimal Charge

    public const string ChargeProperty = "Charge";
    [DataMember(Name = ChargeProperty, EmitDefaultValue = false)]
    decimal mCharge;
    [PersistantProperty]
    public decimal Charge
    {
      get { return mCharge; }
      set { SetProperty<decimal>(ref mCharge, value); }
    }

    #endregion

    #region WaybillChargeType ChargeType

    public const string ChargeTypeProperty = "ChargeType";
    [PersistantProperty(IsCached = true)]
    public WaybillChargeType ChargeType
    {
      get { return GetCachedObject<WaybillChargeType>(); }
      set { SetCachedObject<WaybillChargeType>(value); }
    }

    #endregion
  }
}

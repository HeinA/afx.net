﻿using Afx.Business;
using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Validation;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using AccountManagement.Business;

namespace PastelPartnerIntegration.Business
{
  [PersistantObject(Schema = "PastelPartnerIntegration", TableName = "vPendingInvoice")]
  public partial class PendingInvoice : BusinessObject 
  {
    #region Constructors

    public PendingInvoice()
    {
    }

    #endregion

    #region string WaybillNumber

    public const string WaybillNumberProperty = "WaybillNumber";
    [DataMember(Name = WaybillNumberProperty, EmitDefaultValue = false)]
    string mWaybillNumber;
    [PersistantProperty]
    public string WaybillNumber
    {
      get { return mWaybillNumber; }
      set { SetProperty<string>(ref mWaybillNumber, value); }
    }

    #endregion

    #region AccountReference Account

    public const string AccountProperty = "Account";
    [PersistantProperty(IsCached = true)]
    public AccountReference Account
    {
      get { return GetCachedObject<AccountReference>(); }
      set { SetCachedObject<AccountReference>(value); }
    }

    #endregion

    #region ExternalSystem ExternalSystem

    public const string ExternalSystemProperty = "ExternalSystem";
    [PersistantProperty(IsCached = true)]
    public ExternalSystem ExternalSystem
    {
      get { return GetCachedObject<ExternalSystem>(); }
      set { SetCachedObject<ExternalSystem>(value); }
    }

    #endregion

    #region short? TaxType

    public const string TaxTypeProperty = "TaxType";
    [DataMember(Name = TaxTypeProperty, EmitDefaultValue = false)]
    short? mTaxType;
    [PersistantProperty]
    public short? TaxType
    {
      get { return mTaxType; }
      set { SetProperty<short?>(ref mTaxType, value); }
    }

    #endregion

    #region bool ExportAsOrder

    public const string ExportAsOrderProperty = "ExportAsOrder";
    [DataMember(Name = ExportAsOrderProperty, EmitDefaultValue = false)]
    bool mExportAsOrder;
    [PersistantProperty]
    public bool ExportAsOrder
    {
      get { return mExportAsOrder; }
      set { SetProperty<bool>(ref mExportAsOrder, value); }
    }

    #endregion

    #region decimal Total

    public const string TotalProperty = "Total";
    public decimal Total
    {
      get { return Items.Sum(i => i.Charge); }
    }

    #endregion

    #region BusinessObjectCollection<PendingInvoiceItem> Items

    public const string ItemsProperty = "Items";
    BusinessObjectCollection<PendingInvoiceItem> mItems;
    [PersistantCollection]
    [DataMember]
    public BusinessObjectCollection<PendingInvoiceItem> Items
    {
      get { return mItems ?? (mItems = new BusinessObjectCollection<PendingInvoiceItem>(this)); }
    }

    #endregion
  }
}

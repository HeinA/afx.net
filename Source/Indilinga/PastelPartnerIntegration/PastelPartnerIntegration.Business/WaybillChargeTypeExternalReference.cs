﻿using Afx.Business;
using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Validation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace PastelPartnerIntegration.Business
{
  [PersistantObject(Schema = "PastelPartnerIntegration")]
  public partial class WaybillChargeTypeExternalReference : BusinessObject<WaybillChargeTypeExternalReferences> 
  {
    #region Constructors

    public WaybillChargeTypeExternalReference()
    {
    }

    #endregion

    #region ExternalSystem ExternalSystem

    public const string ExternalSystemProperty = "ExternalSystem";
    [PersistantProperty(IsCached = true)]
    public ExternalSystem ExternalSystem
    {
      get { return GetCachedObject<ExternalSystem>(); }
      set { SetCachedObject<ExternalSystem>(value); }
    }

    #endregion

    #region string Reference

    public const string ReferenceProperty = "Reference";
    [DataMember(Name = ReferenceProperty, EmitDefaultValue = false)]
    string mReference;
    [PersistantProperty]
    public string Reference
    {
      get { return mReference; }
      set { SetProperty<string>(ref mReference, value); }
    }

    #endregion
  }
}

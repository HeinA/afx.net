﻿using AccountManagement.Prism.Documents.Account;
using Afx.Business;
using Afx.Business.Activities;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Documents;
using Afx.Business.Security;
using Afx.Business.Service;
using Afx.Prism;
using Afx.Prism.Events;
using Afx.Prism.RibbonMdi;
using Afx.Prism.RibbonMdi.Documents;
using Afx.Prism.RibbonMdi.Events;
using FreightManagement.Prism.Activities.WaybillCostComponentTypeManagement;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Unity;
using PastelPartnerIntegration.Business;
using PastelPartnerIntegration.Prism.Extensions.EditPastelItemDialog;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace PastelPartnerIntegration.Prism
{
  public class PastelPartnerIntegrationModuleController : MdiModuleController
  {
    const string SetPastelReferenceOnLevy = "{f1553dc2-b3ba-4d2c-a184-fa4d3ab2e978}";

    [InjectionConstructor]
    public PastelPartnerIntegrationModuleController(IController controller)
      : base("PastelPartnerIntegration Module Controller", controller)
    {
    }

    protected override void AfterContainerConfigured()
    {
      EventAggregator.GetEvent<ExtendDocumentEvent>().Subscribe(OnExtendDocumentEvent);
      EventAggregator.GetEvent<ContextChangedEvent>().Subscribe(OnExtendActivityContext);
      EventAggregator.GetEvent<GetGridButtonsEvent>().Subscribe(OnGetGridButtons);
      EventAggregator.GetEvent<ExecuteOperationEvent>().Subscribe(OnExecuteOperation);

      base.AfterContainerConfigured();
    }

    private void OnExecuteOperation(ExecuteOperationEventArgs obj)
    {
      switch (obj.Operation.Identifier)
      {
        case SetPastelReferenceOnLevy:
          OnSetPastelItemClick(obj.Argument);
          break;
      }
    }

    private void OnGetGridButtons(GetGridButtonsEventArgs obj)
    {
      if (obj.ViewModel is WaybillCostComponentTypeManagementViewModel)
      {
        obj.Buttons.Add(new GridButtonViewModel("Set Pastel Item", OnSetPastelItemClick));
      }
    }

    private void OnSetPastelItemClick(object context)
    {
      var ct = context as FreightManagement.Business.WaybillChargeType;
      var c = CreateChildController<EditPastelItemDialogController>();
      c.WaybillChargeType = ct;
      c.Run();
    }

    private void OnExtendActivityContext(ContextChangedEventArgs obj)
    {
      if (obj.Controller is WaybillCostComponentTypeManagementController)
      {
        using (var svc = ServiceFactory.GetService<Pastel.Interface.IPastelService>())
        {
          svc.Instance.GetInventoryItems("Pastel (Namibia)");
        }
      }
    }

    private void OnExtendDocumentEvent(ExtendDocumentEventArgs obj)
    {
      if (obj.Controller is AccountController)
      {
        obj.Controller.ExtendDocument<AccountExtension, Prism.Extensions.Account.AccountHeaderViewModel>(Regions.HeaderRegion);
      }
    }
  }
}

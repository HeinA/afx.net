﻿using Afx.Business;
using Afx.Prism;
using Microsoft.Practices.Unity;
using Pastel.Interface;
using PastelPartnerIntegration.Business;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PastelPartnerIntegration.Prism.Extensions.EditPastelItemDialog
{
  public class ExternalSystemViewModel : ViewModel<WaybillChargeTypeExternalReference>
  {
    [InjectionConstructor]
    public ExternalSystemViewModel(IController controller)
      : base(controller)
    {
    }

    Collection<InventoryItem> mInventoryItems = null;
    public Collection<InventoryItem> InventoryItems
    {
      get
      {
        if (mInventoryItems == null)
        {
          try
          {
            using (var svc = ServiceFactory.GetService<IPastelService>())
            {
              mInventoryItems = svc.Instance.GetInventoryItems(Model.ExternalSystem.Name);
            }
          }
          catch (Exception ex)
          {
            if (ExceptionHelper.HandleException(ex)) throw;
          }
        }
        return mInventoryItems;
      }
    }
  }
}

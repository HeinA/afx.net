﻿using Afx.Prism;
using Afx.Prism.RibbonMdi;
using FreightManagement.Business;
using Microsoft.Practices.Unity;
using PastelPartnerIntegration.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace PastelPartnerIntegration.Prism.Extensions.EditPastelItemDialog
{
  public class EditPastelItemDialogController : MdiDialogController<EditPastelItemDialogContext, EditPastelItemDialogViewModel>
  {
    public const string EditPastelItemDialogControllerKey = "PastelPartnerIntegration.Prism.Extensions.EditPastelItemDialog.EditPastelItemDialogController";

    public EditPastelItemDialogController(IController controller)
      : base(EditPastelItemDialogControllerKey, controller)
    {
    }

    protected override bool OnRun()
    {
      if (DataContext == null) DataContext = new EditPastelItemDialogContext();
      ViewModel.Caption = "Edit Pastel Item";

      WaybillChargeTypeClone.GetExtensionObject<WaybillChargeTypeExternalReferences>().SyncSystems();

      return base.OnRun();
    }

    protected override void ApplyChanges()
    {
      base.ApplyChanges();

      var erTarget = WaybillChargeType.GetExtensionObject<WaybillChargeTypeExternalReferences>();
      foreach (var er in WaybillChargeTypeClone.GetExtensionObject<WaybillChargeTypeExternalReferences>().References)
      {
        var er1 = erTarget.References.FirstOrDefault(er2 => er2.Equals(er));
        if (er1 == null)
        {
          erTarget.References.Add(new WaybillChargeTypeExternalReference() { ExternalSystem = er.ExternalSystem, Reference = er.Reference });
        }
        else
        {
          er1.Reference = er.Reference;
        }
      }
      erTarget.AllowAccountTaxTypeOverride = WaybillChargeTypeClone.GetExtensionObject<WaybillChargeTypeExternalReferences>().AllowAccountTaxTypeOverride;
    }

    public WaybillChargeType WaybillChargeTypeClone { get; set; }

    WaybillChargeType mWaybillChargeType;
    public WaybillChargeType WaybillChargeType
    {
      get { return mWaybillChargeType; }
      set
      {
        mWaybillChargeType = value;
        WaybillChargeTypeClone = (WaybillChargeType)value.Clone();
      }
    }
  }
}

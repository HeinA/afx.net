﻿using Afx.Prism;
using Afx.Prism.RibbonMdi;
using Microsoft.Practices.Unity;
using Pastel.Interface;
using PastelPartnerIntegration.Business;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PastelPartnerIntegration.Prism.Extensions.EditPastelItemDialog
{
  public class EditPastelItemDialogViewModel : MdiDialogViewModel<EditPastelItemDialogContext>
  {
    [InjectionConstructor]
    public EditPastelItemDialogViewModel(IController controller)
      : base(controller)
    {
    }

    [Dependency]
    public EditPastelItemDialogController EditPastelItemDialogController { get; set; }

    public IEnumerable<ExternalSystemViewModel> Systems
    {
      get
      {
        if (Model == null) return null;
        return this.ResolveViewModels<ExternalSystemViewModel, WaybillChargeTypeExternalReference>(EditPastelItemDialogController.WaybillChargeTypeClone.GetExtensionObject<WaybillChargeTypeExternalReferences>().References);
      }
    }

    public bool AllowAccountTaxTypeOverride
    {
      get { return EditPastelItemDialogController.WaybillChargeTypeClone.GetExtensionObject<WaybillChargeTypeExternalReferences>().AllowAccountTaxTypeOverride; }
      set { EditPastelItemDialogController.WaybillChargeTypeClone.GetExtensionObject<WaybillChargeTypeExternalReferences>().AllowAccountTaxTypeOverride = value; }
    }
  }
}

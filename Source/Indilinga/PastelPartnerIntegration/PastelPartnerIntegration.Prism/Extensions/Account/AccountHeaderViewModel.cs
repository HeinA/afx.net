﻿using Afx.Business;
using Afx.Business.Data;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Documents;
using Microsoft.Practices.Unity;
using Pastel.Interface;
using PastelPartnerIntegration.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PastelPartnerIntegration.Prism.Extensions.Account
{
  public class AccountHeaderViewModel : ExtensionViewModel<Business.AccountExtension>
  {
    #region Constructors

    [InjectionConstructor]
    public AccountHeaderViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    public IEnumerable<ExternalSystem> ExternalSystems
    {
      get { return Cache.Instance.GetObjects<ExternalSystem>(true, es => es.IsFlagged(ExternalSystemFlags.PastelPartner), es => es.Name, true); }
    }

    public IEnumerable<TaxType> TaxTypes
    {
      get
      {
        if (BusinessObject.IsNull(Model.ExternalSystem)) return null;

        try
        {
          using (var svc = ServiceFactory.GetService<IPastelService>())
          {
            var list = svc.Instance.GetTaxTypes(Model.ExternalSystem.Name);
            return list;
          }
        }
        catch
        {
          throw;
        }
      }
    }

    protected override void OnModelPropertyChanged(string propertyName)
    {
      switch (propertyName)
      {
        case Business.AccountExtension.ExternalSystemProperty:
          Model.TaxType = null;
          OnPropertyChanged("TaxTypes");
          break;
      }

      base.OnModelPropertyChanged(propertyName);
    }
  }
}

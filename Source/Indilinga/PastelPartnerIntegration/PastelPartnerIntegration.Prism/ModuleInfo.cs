﻿using Afx.Prism;
using Afx.Prism.RibbonMdi;
using PastelPartnerIntegration.Business;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PastelPartnerIntegration.Prism
{
  [Export(typeof(IModuleInfo))]
  public class ModuleInfo : IModuleInfo
  {
    public string ModuleNamespace
    {
      get { return Namespace.PastelPartnerIntegration; }
    }

    public string[] ModuleDependencies
    {
      get { return new string[] { Afx.Business.Namespace.Afx, FreightManagement.Business.Namespace.FreightManagement }; }
    }

    public Type ModuleController
    {
      get { return typeof(PastelPartnerIntegrationModuleController); }
    }
  }
}
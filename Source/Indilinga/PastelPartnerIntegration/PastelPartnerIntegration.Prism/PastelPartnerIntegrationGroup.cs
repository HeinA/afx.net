﻿using Afx.Prism.RibbonMdi.Ribbon;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PastelPartnerIntegration.Prism
{
  [Export(typeof(IRibbonGroup))]
  public class PastelPartnerIntegrationGroup : RibbonGroup
  {
    public const string GroupName = "Pastel";

    public override string TabName
    {
      get { return HomeTab.TabName; }
    }

    public override string Name
    {
      get { return GroupName; }
    }
  }
}

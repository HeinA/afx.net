﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PastelPartnerIntegration.Prism.Activities.PastelExportManagement
{
  //**************************************************************************************************************
  //**************************************************************************************************************
  //*                                                                                                            *
  //*     This code is auto-generated.  Do not place any cutom code here, it will be lost on re-generation.      *
  //*                                                                                                            *
  //**************************************************************************************************************
  //**************************************************************************************************************
  public class Operations : Afx.Prism.RibbonMdi.StandardOperations
  {
    public const string Export = "{b68ca15d-3966-4b90-899e-ef2fe3297107}";
  }
}

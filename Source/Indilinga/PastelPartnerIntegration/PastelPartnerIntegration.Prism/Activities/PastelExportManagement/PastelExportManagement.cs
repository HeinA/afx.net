﻿using Afx.Business.Activities;
using PastelPartnerIntegration.Business;
using Afx.Prism.RibbonMdi.Activities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Afx.Business.Service;
using PastelPartnerIntegration.Business.Service;
using Afx.Business.Security;
using Afx.Business.Collections;

namespace PastelPartnerIntegration.Prism.Activities.PastelExportManagement
{
  public partial class PastelExportManagement : CustomActivityContext
  {
    public PastelExportManagement()
    {
    }

    public PastelExportManagement(Activity activity)
      : base(activity)
    {
    }

    public override void LoadData()
    {
      LoadWaybills();
      base.LoadData();
    }

    #region BasicCollection<PendingInvoice> Invoices

    public const string InvoicesProperty = "Invoices";
    BasicCollection<PendingInvoice> mInvoices;
    public BasicCollection<PendingInvoice> Invoices
    {
      get { return mInvoices; }
      set { SetProperty<BasicCollection<PendingInvoice>>(ref mInvoices, value); }
    }

    #endregion

    void LoadWaybills()
    {
      using (var svc = ProxyFactory.GetService<IPastelPartnerIntegrationService>(SecurityContext.ServerName))
      {
        Invoices = svc.GetInvoices();
      }
    }
  }
}

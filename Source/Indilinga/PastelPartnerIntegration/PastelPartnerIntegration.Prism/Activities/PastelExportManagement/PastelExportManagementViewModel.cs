﻿using Afx.Business.Security;
using Afx.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Activities;
using PastelPartnerIntegration.Business;
using Afx.Business.Collections;
using Afx.Business.Data;
using Microsoft.Practices.Prism.Commands;
using Afx.Prism.RibbonMdi;

namespace PastelPartnerIntegration.Prism.Activities.PastelExportManagement
{
  public partial class PastelExportManagementViewModel : MdiCustomActivityViewModel<PastelExportManagement>
  {
    #region Constructors

    [InjectionConstructor]
    public PastelExportManagementViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    BasicCollection<ExternalSystem> mExternalSystems;
    public IEnumerable<ExternalSystem> ExternalSystems
    {
      get { return mExternalSystems ?? (mExternalSystems = Cache.Instance.GetObjects<ExternalSystem>(true, es => es.IsFlagged(ExternalSystemFlags.PastelPartner), es => es.Name, true)); }
    }

    public IEnumerable<InvoiceViewModel> Items
    {
      get
      {
        if (Model == null) return null;
        return this.ResolveViewModels<InvoiceViewModel, PendingInvoice>(Model.Invoices);
      }
    }

    #region DelegateCommand<InvoiceViewModel> OpenWaybillCommand

    DelegateCommand<InvoiceViewModel> mOpenWaybillCommand;
    public DelegateCommand<InvoiceViewModel> OpenWaybillCommand
    {
      get { return mOpenWaybillCommand ?? (mOpenWaybillCommand = new DelegateCommand<InvoiceViewModel>(ExecuteOpenWaybill, CanExecuteOpenWaybill)); }
    }

    bool CanExecuteOpenWaybill(InvoiceViewModel args)
    {
      return true;
    }

    void ExecuteOpenWaybill(InvoiceViewModel args)
    {
      MdiApplicationController.Current.EditDocument(FreightManagement.Business.Waybill.DocumentTypeIdentifier, args.Model.GlobalIdentifier);
    }

    #endregion
  }
}

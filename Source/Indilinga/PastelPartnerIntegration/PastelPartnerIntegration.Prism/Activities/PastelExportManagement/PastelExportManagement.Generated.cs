﻿using PastelPartnerIntegration.Business;
using Afx.Business.Service;
using Afx.Prism.RibbonMdi.Activities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.Composition;
using Afx.Business.Security;

namespace PastelPartnerIntegration.Prism.Activities.PastelExportManagement
{
  //**************************************************************************************************************
  //**************************************************************************************************************
  //*                                                                                                            *
  //*     This code is auto-generated.  Do not place any cutom code here, it will be lost on re-generation.      *
  //*                                                                                                            *
  //**************************************************************************************************************
  //**************************************************************************************************************
  [Export("Context:" + global::PastelPartnerIntegration.Prism.Activities.PastelExportManagement.PastelExportManagement.Key)]
  public partial class PastelExportManagement
  {
    public const string Key = "{8e306413-1c8d-43e7-b437-c64cac303652}";
  }
}

﻿using Afx.Business;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Business.Service;
using Afx.Prism;
using Microsoft.Practices.Unity;
using PastelPartnerIntegration.Business;
using PastelPartnerIntegration.Business.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PastelPartnerIntegration.Prism.Activities.PastelExportManagement
{
  public class InvoiceViewModel : ViewModel<PendingInvoice>
  {
    #region Constructors

    [InjectionConstructor]
    public InvoiceViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    #region bool Export

    public const string ExportProperty = "Export";
    bool mExport;
    public bool Export
    {
      get { return mExport; }
      set { SetProperty<bool>(ref mExport, value); }
    }

    #endregion

    #region string ValidationMessage

    public const string ValidationMessageProperty = "ValidationMessage";
    string mValidationMessage;
    public string ValidationMessage
    {
      get { return mValidationMessage; }
      set { SetProperty<string>(ref mValidationMessage, value); }
    }

    #endregion

    BasicCollection<ExternalSystem> mExternalSystems;
    public IEnumerable<ExternalSystem> ExternalSystems
    {
      get { return mExternalSystems ?? (mExternalSystems = Cache.Instance.GetObjects<ExternalSystem>(true, es => es.IsFlagged(ExternalSystemFlags.PastelPartner), es => es.Name, true)); }
    }

    protected override void OnModelPropertyChanged(string propertyName)
    {
      ValidateInvoice();
      base.OnModelPropertyChanged(propertyName);
    }

    protected override void OnPropertyChanged(string propertyName)
    {
      if (propertyName == ExportProperty) ValidateInvoice();
      base.OnPropertyChanged(propertyName);
    }

    void ValidateInvoice()
    {
      ValidationMessage = string.Empty;
      if (!Export) return;
      List<string> errors = new List<string>();

      try
      {
        if (BusinessObject.IsNull(Model.ExternalSystem))
        {
          errors.Add("Please select a Pastel Company");
          return;
        }

        using (var svc = ProxyFactory.GetService<IPastelPartnerIntegrationService>(SecurityContext.ServerName))
        {
          if (!svc.ValidateAccount(Model.ExternalSystem.Name, Model.Account.AccountNumber))
          {
            errors.Add(string.Format("Account '{0}' does not exist in the currently selected Pastel company", Model.Account.AccountNumber));
          }
        }

        foreach (var item in Model.Items)
        {
          var ii = item.ChargeType.GetExtensionObject<WaybillChargeTypeExternalReferences>().References.FirstOrDefault(r => r.ExternalSystem.Equals(Model.ExternalSystem));
          if (ii == null || string.IsNullOrWhiteSpace(ii.Reference))
          {
            errors.Add(string.Format("Charge Type '{0}' does not have an external reference set for the currently selected Pastel company", item.ChargeType.Text));
          }
        }
      }
      finally
      {
        ValidationMessage = string.Join("; ", errors);
      }
    }
  }
}

﻿using Afx.Business;
using Afx.Business.Activities;
using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Activities;
using PastelPartnerIntegration.Business;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Afx.Business.Collections;
using Afx.Business.Service;
using PastelPartnerIntegration.Business.Service;

namespace PastelPartnerIntegration.Prism.Activities.PastelExportManagement
{
  public partial class PastelExportManagementController : MdiCustomActivityController<PastelExportManagement, PastelExportManagementViewModel>
  {
    #region Constructors

    [InjectionConstructor]
    public PastelExportManagementController(IController controller)
      : base(controller)
    {
    }

    #endregion

    #region OnLoaded
    
    protected override void OnLoaded()
    {
      ViewModel.IsFocused = true;
      base.OnLoaded();
    }

    #endregion
    
    #region ExecuteOperation
    
    protected override void ExecuteOperation(Operation op, BusinessObject argument)
    {
      switch (op.Identifier)
      {
        case Operations.Export:
          BasicCollection<PendingInvoice> col = new BasicCollection<PendingInvoice>(ViewModel.Items.Where(es1 => es1.Export).Select(es1 => es1.Model));
          using (var svc = ProxyFactory.GetService<IPastelPartnerIntegrationService>(SecurityContext.ServerName))
          {
            svc.Export(col);
          }
          Refresh(true);
          break;
      }
      base.ExecuteOperation(op, argument);
    }
    
    #endregion
  }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportingModule.Prism.Reports.OperationsManifestReportManagement
{
  public class OperationsManifestReportManagementPrintViewModel
  {
    public OperationsManifestReportManagementPrintViewModel(FreightManagement.Business.ManifestReference details)
    {
      Details = details;
    }

    #region ManifestReference Details

    FreightManagement.Business.ManifestReference mDetails;
    FreightManagement.Business.ManifestReference Details
    {
      get { return mDetails; }
      set { mDetails = value; }
    }

    #endregion



    public int ManifestWaybillItems
    {
      get { return Details.Items; }
    }

    public int ManifestWaybills
    {
      get { return Details.WaybillCount; }
    }

    //public decimal OnFloor
    //{
    //  get { return Details.OnFloor; }
    //}

    //public decimal Pending
    //{
    //  get { return Details.Pending; }
    //}
  }
}

﻿using Afx.Business;
using FreightManagement.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportingModule.Prism.Reports.OperationsManifestReportManagement
{
  public class ItemPrintViewModel
  {
    public ItemPrintViewModel(FreightManagement.Business.ManifestReference item)
    {
      Item = item;
    }

    #region ManifestReference Item

    FreightManagement.Business.ManifestReference mItem;
    FreightManagement.Business.ManifestReference Item
    {
      get { return mItem; }
      set { mItem = value; }
    }

    #endregion

    public string OriginDC
    {
      get { return Item.OriginDC.Name; }
    }

    public string DestinationDC
    {
      get { return Item.DestinationDC.Name; }
    }

    public int WaybillCount
    {
      get { return Item.WaybillCount; }
    }

    public string ManifestNumber
    {
      get { return Item.ManifestNumber; }
    }

    public DateTime ManifestDate
    {
      get { return Item.ManifestDate; }
    }


    public string WaybillRoute
    {
      get { return Item.Route == null ? "" : Item.Route.Name; }
    }

    public string Vehicle
    {
      get { return Item.Horse == null ? "" : Item.Horse.Text; }
    }


    #region "Dimensions, Weights & Charges"

    //public decimal ManifestDeclaredValue
    //{
    //  get { return Item.DelaredValue; }
    //}

    public decimal ManifestWaybillPhysicalWeight
    {
      get { return Item.Weight; }
    }

    public int ManifestWaybillItems
    {
      get { return Item.Items; }
    }

    public decimal ChargeDocumentation
    {
      get { return Item.ChargeDocumentation; }
    }

    public decimal ChargeCollectionDelivery
    {
      get { return Item.ChargeCollectionDelivery; }
    }

    public decimal ChargeTransport
    {
      get { return Item.ChargeTransport; }
    }

    public decimal ChargeOther
    {
      get { return Item.ChargeOther + Item.ChargeDisbursementFee; }
    }

    public decimal ChargeFuelLevy
    {
      get { return Item.ChargeFuelLevy; }
    }

    public decimal ChargeTotal
    {
      get { return Item.ChargeTotal; }
    }

    #endregion
  }
}

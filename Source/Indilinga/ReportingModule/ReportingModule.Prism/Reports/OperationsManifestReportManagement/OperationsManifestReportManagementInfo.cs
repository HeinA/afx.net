﻿using Afx.Business;
using Afx.Business.Activities;
using Afx.Business.Collections;
using Afx.Business.Documents;
using Afx.Prism;
using Afx.Prism.Activities;
using Afx.Prism.Documents;
using Afx.Prism.RibbonMdi.Documents;
using FreightManagement.Business;
using ReportingModule.Prism.Activities.OperationsManifestReportManagement;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportingModule.Prism.Reports.OperationsManifestReportManagement
{
  //[Export("{1c9f46d9-95ee-41d9-8d47-bfe75b5f34ce}", typeof(IActivityPrintInfo))]
  [Export(Activities.OperationsManifestReportManagement.OperationsManifestReportManagement.Key, typeof(IActivityPrintInfo))]
  class OperationsManifestReportManagementInfo : IActivityPrintInfo
  {
    public int Priority
    {
      get { return 90; }
    }

    public Stream ReportStream(object argument)
    {
      return this.GetType().Assembly.GetManifestResourceStream("ReportingModule.Prism.Reports.OperationsManifestReportManagement.OperationsManifestReportManagement.rdlc"); 
    }

    public object GetScope(IController controller)
    {
      return null;
    }

    //public void RefreshReportData(LocalReport report, Activity activity, object scope)
    public void RefreshReportData(LocalReport report, BusinessObject activityContext, object scope, object argument)
    {
      ReportingModule.Prism.Activities.OperationsManifestReportManagement.OperationsManifestReportManagement fs = (ReportingModule.Prism.Activities.OperationsManifestReportManagement.OperationsManifestReportManagement)activityContext;

      ReportDataSource reportDataSource = new ReportDataSource();
      reportDataSource.Name = "Activity";
      reportDataSource.Value = new object[] { new ActivityPrintViewModel(fs.Activity) };
      report.DataSources.Add(reportDataSource);

      reportDataSource = new ReportDataSource();
      reportDataSource.Name = "ManifestsWaybillItems";
      Collection<ItemPrintViewModel> items = new Collection<ItemPrintViewModel>();
      foreach (var wb in fs.Manifests)
      {
        if (wb.IsChecked || wb.Horse != null)
        {
          items.Add(new ItemPrintViewModel(wb));
        }
      }

      reportDataSource.Value = items;
      report.DataSources.Add(reportDataSource);

      //report.SetParameters(new ReportParameter("TotalWaybillsOnFloor", fs.Waybills.Count(wb => wb.OnFloor > 0).ToString()));
      //report.SetParameters(new ReportParameter("TotalWaybillsSelected", fs.Waybills.Count(wb => wb.IsChecked == true).ToString()));

      //report.ShowDetailedSubreportMessages = true;
      //Stream SubReportStream = this.GetType().Assembly.GetManifestResourceStream("FreightManagement.Prism.Reports.FloorStatus.FloorStatusSummarySubReport.rdlc");
      //report.SubreportProcessing += new SubreportProcessingEventHandler(SubReportProcessing);
      //report.LoadSubreportDefinition("FloorStatusSummarySubReport", SubReportStream);
    }

    private void SubReportProcessing(object sender, SubreportProcessingEventArgs e)
    {
      //e.Parameters.Add(new ReportParameter("", 0));
    }

    public short Copies
    {
      get { return 1; }
    }
  }
}

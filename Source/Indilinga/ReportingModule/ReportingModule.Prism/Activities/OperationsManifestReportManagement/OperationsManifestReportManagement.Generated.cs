﻿using ReportingModule.Business;
using ReportingModule.Business.Service;
using Afx.Business.Service;
using Afx.Prism.RibbonMdi.Activities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.Composition;
using Afx.Business.Security;

namespace ReportingModule.Prism.Activities.OperationsManifestReportManagement
{
  //**************************************************************************************************************
  //**************************************************************************************************************
  //*                                                                                                            *
  //*     This code is auto-generated.  Do not place any cutom code here, it will be lost on re-generation.      *
  //*                                                                                                            *
  //**************************************************************************************************************
  //**************************************************************************************************************
  [Export("Context:" + global::ReportingModule.Prism.Activities.OperationsManifestReportManagement.OperationsManifestReportManagement.Key)]
  public partial class OperationsManifestReportManagement
  {
    public const string Key = "{4777b535-87f0-4cd0-8cc2-ec585633dbb3}";
  }
}

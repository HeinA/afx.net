﻿using Afx.Business;
using Afx.Business.Activities;
using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Activities;
using ReportingModule.Business;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Afx.Business.Collections;
using FreightManagement.Business;

namespace ReportingModule.Prism.Activities.OperationsManifestReportManagement
{
  public partial class OperationsManifestReportManagementController : MdiCustomActivityController<OperationsManifestReportManagement, OperationsManifestReportManagementViewModel>
  {
    #region Constructors

    [InjectionConstructor]
    public OperationsManifestReportManagementController(IController controller)
      : base(controller)
    {
    }

    #endregion

    #region OnLoaded
    
    protected override void OnLoaded()
    {
      ViewModel.IsFocused = true;
      HookManifests();
      base.OnLoaded();
    }

    #endregion
    
    #region ExecuteOperation
    
    protected override void ExecuteOperation(Operation op, BusinessObject argument)
    {
      //switch (op.Identifier)
      //{
      //  case Operations.Print:
      //    break;
      //}
      base.ExecuteOperation(op, argument);
    }

    #endregion

    #region "Hooks"

    protected override void LoadDataContext()
    {
      UnhookManifests();
      base.LoadDataContext();
      HookManifests();
    }

    private void HookManifests()
    {
      if (DataContext.Manifests == null) return;

      foreach (var w in DataContext.Manifests)
      {
        w.PropertyChanged += Manifest_PropertyChanged;
      }
    }

    private void UnhookManifests()
    {
      if (DataContext.Manifests == null) return;

      foreach (var w in DataContext.Manifests)
      {
        w.PropertyChanged -= Manifest_PropertyChanged;
      }
    }

    #endregion

    void Manifest_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
    {
      //Collection<VehicleSummary> vsCol = new Collection<VehicleSummary>();

      //switch (e.PropertyName)
      //{
      //  case "IsChecked":
      //    foreach (var w in DataContext.Manifests)
      //    {
      //      //if (BusinessObject.IsNull(w.Vehicle)) continue;

      //      if (w.IsChecked)
      //      {
      //        VehicleSummary vs = vsCol.Where(vs1 => vs1.Vehicle.Equals(new OwnedVehicle(true))).FirstOrDefault();
      //        if (vs == null)
      //        {
      //          vs = new VehicleSummary() { Vehicle = new OwnedVehicle(true) };
      //          vsCol.Add(vs);
      //        }

      //        vs.WaybillCount++;
      //        vs.PhysicalWeight += w.OnFloor;
      //        vs.Volume += w.Volume;
      //        if (vs.Waybills == null) vs.Waybills = new BusinessObjectCollection<ManifestReference>();
      //        vs.Waybills.Add(w);
      //      }
      //    }
      //    break;
      //  default:
      //    foreach (var w in DataContext.Manifests)
      //    {
      //      if (BusinessObject.IsNull(w.Vehicle)) continue;

      //      VehicleSummary vs = vsCol.Where(vs1 => vs1.Vehicle.Equals(w.Vehicle)).FirstOrDefault();
      //      if (vs == null)
      //      {
      //        vs = new VehicleSummary() { Vehicle = w.Vehicle };
      //        vsCol.Add(vs);
      //      }
      //      vs.WaybillCount++;
      //      vs.PhysicalWeight += w.OnFloor;
      //      vs.Volume += w.Volume;
      //      if (vs.Waybills == null) vs.Waybills = new BusinessObjectCollection<ManifestReference>();
      //      vs.Waybills.Add(w);
      //    }
      //    break;
      //}

      //DataContext.VehicleSummary = vsCol;


    }
  }
}
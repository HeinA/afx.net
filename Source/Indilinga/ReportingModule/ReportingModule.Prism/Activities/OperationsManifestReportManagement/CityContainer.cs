﻿using Afx.Business;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Documents;
using Afx.Prism.RibbonMdi;
using Afx.Prism.RibbonMdi.Events;
using FreightManagement.Business;
using Geographics.Business;
using Microsoft.Practices.Prism.Commands;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportingModule.Prism.Activities.OperationsManifestReportManagement
{
  public class CityContainer : BusinessObject
  {
    public CityContainer(Location city, IEnumerable<ManifestReference> collections)
    {
      City = city;
      Manifests = collections;
    }

    #region Location City

    public const string CityProperty = "City";
    Location mCity;
    public Location City
    {
      get { return mCity; }
      set { mCity = value; }
    }

    #endregion

    #region IEnumerable<ManifestReference> Manifests

    public const string ManifestsProperty = "Manifests";
    IEnumerable<ManifestReference> mManifests;
    public IEnumerable<ManifestReference> Manifests
    {
      get { return mManifests; }
      private set
      {
        mManifests = new BasicCollection<ManifestReference>(value);
      }
    }

    #endregion

    #region DelegateCommand<ManifestReference> OpenManifestCommand

    DelegateCommand<ManifestReference> mOpenManifestCommand;
    public DelegateCommand<ManifestReference> OpenManifestCommand
    {
      get
      {
        return mOpenManifestCommand ?? (mOpenManifestCommand = new DelegateCommand<ManifestReference>(ExecuteOpenDeliveryManifest, CanExecuteOpenDeliveryManifest));
      }
    }

    bool CanExecuteOpenDeliveryManifest(ManifestReference args)
    {
      return true;
    }

    void ExecuteOpenDeliveryManifest(ManifestReference args)
    {
      MdiApplicationController.Current.EventAggregator.GetEvent<EditDocumentEvent>().Publish(new EditDocumentEventArgs(Cache.Instance.GetObject<DocumentType>(args.ManifestType.Identifier), args.GlobalIdentifier));
    }

    #endregion

  }
}

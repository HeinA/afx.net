﻿using Afx.Business.Security;
using Afx.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Activities;
using ReportingModule.Business;
using FreightManagement.Business;
using Afx.Business.Data;
using Afx.Business.Documents;
using ContactManagement.Business;
using FleetManagement.Business;
using AccountManagement.Business;

namespace ReportingModule.Prism.Activities.OperationsManifestReportManagement
{
  public partial class OperationsManifestReportManagementViewModel : MdiCustomActivityViewModel<OperationsManifestReportManagement>
  {
    #region Constructors

    [InjectionConstructor]
    public OperationsManifestReportManagementViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    #region IEnumerable<DistributionCenter> DistributionCenters

    public IEnumerable<DistributionCenter> DistributionCenters
    {
      get { return Cache.Instance.GetObjects<DistributionCenter>(true, dc => dc.Name, true); }
    }

    #endregion

    #region IEnumerable<DocumentTypeState> DocumentStates

    public IEnumerable<DocumentTypeState> DocumentStates
    {
      get { return Cache.Instance.GetObjects<DocumentTypeState>(true, dts => dts.Owner.Identifier.Equals(CollectionManifest.DocumentTypeIdentifier) || dts.Owner.Identifier.Equals(TransferManifest.DocumentTypeIdentifier) || dts.Owner.Identifier.Equals(DeliveryManifest.DocumentTypeIdentifier)).Distinct(); }
    }

    #endregion

    #region IEnumerable<Contractor> Couriers

    public IEnumerable<Contractor> Couriers
    {
      get { return Cache.Instance.GetObjects<Contractor>(true, c => (!BusinessObject.IsNull(c.ContractorType) && c.ContractorType.IsFlagged(ContractorTypeFlags.Is3rdPartyCourier)), c => c.CompanyName, true); }
    }

    #endregion

    #region IEnumerable<Contact> Drivers

    public IEnumerable<Contact> Drivers
    {
      get
      {
        IEnumerable<Contact> drivers = null;
        if (BusinessObject.IsNull(Model.Courier))
        {
          drivers = Cache.Instance.GetObjects<Employee>(true, e => e.ContactType.IsFlagged(FleetManagement.Business.ContactTypeFlags.Driver), e => e.Fullname, true);
        }
        else
        {
          drivers = new Contact[] { new SimpleContact(true) }.Union(Model.Courier.Contacts.Where(e => e.ContactType.IsFlagged(FleetManagement.Business.ContactTypeFlags.Driver)).OrderBy(e => e.Fullname));
        }
        return drivers;
      }
    }

    #endregion

    #region IEnumerable<Route> Routes

    public IEnumerable<Route> Routes
    {
      get { return Cache.Instance.GetObjects<Route>(true, r => r.Owner.Equals(this.Model.DistributionCenter), r => r.Name, true); }
    }

    #endregion

    #region IEnumerable<Account> Accounts

    public IEnumerable<Account> Accounts
    {
      get { return Cache.Instance.GetObjects<Account>(true, a => a.OrganizationalUnit.Equals(SecurityContext.OrganizationalUnit), a => a.Name, true); }
    }

    #endregion

    public void RefreshRoutes()
    {
      OnPropertyChanged("Routes");
    }

    protected override void OnModelPropertyChanged(string propertyName)
    {
      switch (propertyName)
      {
        case "DistributionCenter":
          OnPropertyChanged("Routes");
          break;
        case "Courier":
          OnPropertyChanged("Drivers");
          break;
      }

      base.OnModelPropertyChanged(propertyName);
    }
  }
}

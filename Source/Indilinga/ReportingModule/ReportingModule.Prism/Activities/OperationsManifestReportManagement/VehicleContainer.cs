﻿using Afx.Business;
using Afx.Business.Collections;
using FleetManagement.Business;
using FreightManagement.Business;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace ReportingModule.Prism.Activities.OperationsManifestReportManagement
{
  public class VehicleContainer : BusinessObject
  {
    #region Constructors

    public VehicleContainer(Vehicle vehicle, IEnumerable<ManifestReference> collections)
    {
      Vehicle = vehicle;
      if (collections.DefaultIfEmpty() == null)
      {
        Manifests = new BasicCollection<ManifestReference>();
      }
      else
      {
        Manifests = collections;
      }
    }

    #endregion

    #region Vehicle Vehicle

    public const string VehicleProperty = "Vehicle";
    Vehicle mVehicle;
    public Vehicle Vehicle
    {
      get { return mVehicle; }
      set { mVehicle = value; }
    }

    #endregion


    public string FullName
    {
      get { return (Vehicle == null ? "Blank" : String.Format("{0}, {1}", String.IsNullOrEmpty(Vehicle.FleetNumber) ? "NO FLEET NUMBER" : Vehicle.FleetNumber, String.IsNullOrEmpty(Vehicle.RegistrationNumber) ? "NO REGISTRATION NUMBER" : Vehicle.RegistrationNumber)); }
    }

    #region IEnumerable<ManifestReference> Manifests

    public const string ManifestsProperty = "Manifests";
    IEnumerable<ManifestReference> mManifests;
    public IEnumerable<ManifestReference> Manifests
    {
      get { return mManifests; }
      private set
      {
        mManifests = value;

        try
        {
          Collection<RouteContainer> routes = new Collection<RouteContainer>();
          //routes.Add(new RouteContainer(new Route(true), mManifests.Where(mw => BusinessObject.IsNull(mw.Route) && mw.Driver.Equals(Driver))));
          var varResults = (BusinessObject.IsNull(Vehicle) ?
            mManifests.Where(mw => mw.Horse == null).Select(c => c.Route).Distinct().OrderBy(r => r.Name) :
            mManifests.Where(mw => mw.Horse != null && mw.Horse.Equals(Vehicle)).Select(c => c.Route).Distinct().OrderBy(r => r.Name));

          if (varResults != null && varResults.DefaultIfEmpty() != null)
          {
            foreach (var route in varResults)
            //foreach (var route in mManifests.Where(mw => !BusinessObject.IsNull(mw.Driver) && mw.Driver.Equals(Driver)).Select(ms => ms.Route).Distinct().OrderBy(mob => mob.Name))
            {
              if (!BusinessObject.IsNull(route))
              {
                if (!BusinessObject.IsNull(route) && route != null)
                {
                  routes.Add(new RouteContainer(route, mManifests.Where(mw => mw.Route.Equals(route))));
                }
                else
                {
                  routes.Add(new RouteContainer(route, mManifests.Where(mw => BusinessObject.IsNull(mw.Route))));
                }
              }
              else
              {
                routes.Add(new RouteContainer(route, mManifests));
              }
            }
          }
          Routes = routes;
        }
        catch
        {
          throw;
        }
      }
    }

    #endregion

    #region IEnumerable<RouteContainer> Routes

    public const string RoutesProperty = "Routes";
    IEnumerable<RouteContainer> mRoutes;
    public IEnumerable<RouteContainer> Routes
    {
      get { return mRoutes; }
      set { mRoutes = value; }
    }

    #endregion

    #region IEnumerable<CityContainer> Cities

    public const string CitiesProperty = "Cities";
    IEnumerable<CityContainer> mCities;
    public IEnumerable<CityContainer> Cities
    {
      get { return mCities; }
      set { mCities = value; }
    }

    #endregion



    #region "Totals"

    #region int TotalManifests

    public int TotalManifests
    {
      get { return mManifests.Count(); }
    }

    #endregion

    #region int TotalWaybills

    public int TotalWaybills
    {
      get { return mManifests.Sum(m => m.WaybillCount); }
    }

    #endregion

    #region int TotalWaybillItems

    public int TotalWaybillItems
    {
      get { return mManifests.Sum(m => m.Items); }
    }

    #endregion

    #region decimal TotalWaybillWeights

    public decimal TotalWaybillWeights
    {
      get { return mManifests.Sum(m => m.Weight); }
    }

    #endregion

    #region decimal TotalWaybillChargesDocumentation

    public decimal TotalWaybillChargesDocumentation
    {
      get { return mManifests.Sum(m => m.ChargeDocumentation); }
    }

    #endregion

    #region decimal TotalWaybillChargesTransport

    public decimal TotalWaybillChargesTransport
    {
      get { return mManifests.Sum(m => m.ChargeTransport); }
    }

    #endregion

    #region decimal TotalWaybillChargesCollectionDelivery

    public decimal TotalWaybillChargesCollectionDelivery
    {
      get { return mManifests.Sum(m => m.ChargeCollectionDelivery); }
    }

    #endregion

    #region decimal TotalWaybillChargesFuelLevy

    public decimal TotalWaybillChargesFuelLevy
    {
      get { return mManifests.Sum(m => m.ChargeFuelLevy); }
    }

    #endregion

    #region decimal TotalWaybillChargesOther

    public decimal TotalWaybillChargesOther
    {
      get { return mManifests.Sum(m => m.ChargeOther); }
    }

    #endregion

    #region decimal TotalWaybillChargesDisbursementFee

    public decimal TotalWaybillChargesDisbursementFee
    {
      get { return mManifests.Sum(m => m.ChargeDisbursementFee); }
    }

    #endregion

    #endregion
  }
}

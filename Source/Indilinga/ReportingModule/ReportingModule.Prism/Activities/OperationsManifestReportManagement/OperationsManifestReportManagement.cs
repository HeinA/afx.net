﻿using Afx.Business.Activities;
using ReportingModule.Business;
using ReportingModule.Business.Service;
using Afx.Prism.RibbonMdi.Activities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Afx.Business.Collections;
using FreightManagement.Business;
using System.Collections.ObjectModel;
using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Business.Service;
using FreightManagement.Business.Service;
using Afx.Business.Documents;
using ContactManagement.Business;
using FleetManagement.Business;
using Afx.Business;

namespace ReportingModule.Prism.Activities.OperationsManifestReportManagement
{
  public partial class OperationsManifestReportManagement : CustomActivityContext
  {
    public OperationsManifestReportManagement()
    {
    }

    public OperationsManifestReportManagement(Activity activity)
      : base(activity)
    {
    }

    public override bool IsDirty
    {
      get { return false; }
      set { }
    }

    #region "Containers"

    #region Collection<ManifestReference> Manifests

    public const string ManifestsProperty = "Manifests";
    BasicCollection<ManifestReference> mManifests;
    public BasicCollection<ManifestReference> Manifests
    {
      get { return mManifests; }
      set
      {
        if (SetProperty<BasicCollection<ManifestReference>>(ref mManifests, value))
        {
          switch (FilterBy.Replace("System.Windows.Controls.ComboBoxItem: ", ""))
          {
            //******************************************************************************************************************************
            //  VEHICLES
            //******************************************************************************************************************************
            case "By Horse":
              Collection<VehicleContainer> vehicles = new Collection<VehicleContainer>();
              var varVehicleResults = mManifests.Select(c => c.Horse).Distinct().OrderBy(r => r == null ? string.Empty : r.FleetNumber);
              if (varVehicleResults != null && varVehicleResults.DefaultIfEmpty() != null)
              {
                foreach (var vehicle in varVehicleResults)
                {
                  if (!BusinessObject.IsNull(vehicle))
                  {
                    if (mManifests.Where(c => c.Horse != null && c.Horse.Equals(vehicle)).Count() != 0)
                    {
                      vehicles.Add(new VehicleContainer(vehicle, mManifests.Where(c => c.Horse != null && c.Horse.Equals(vehicle))));
                    }
                  }
                  else
                  {
                    if (mManifests.Where(mw => BusinessObject.IsNull(mw.Horse)).Count() != 0)
                    {
                      vehicles.Add(new VehicleContainer(vehicle, mManifests.Where(mw => BusinessObject.IsNull(mw.Horse))));
                    }
                  }
                }
              }
              Vehicles = vehicles;
              break;
            //******************************************************************************************************************************
            //  ROUTES
            //******************************************************************************************************************************
            case "By Routes":
              
              Collection<RouteContainer> routes = new Collection<RouteContainer>();
              var varRouteResults = mManifests.Select(c => c.Route).Distinct().OrderBy(r => r.Name);
              if (varRouteResults != null && varRouteResults.DefaultIfEmpty() != null)
              {
                foreach (var route in varRouteResults)
                {
                  if (!BusinessObject.IsNull(route))
                  {
                    routes.Add(new RouteContainer(route, mManifests.Where(c => c.Route.Equals(route))));
                  }
                  else
                  {
                    routes.Add(new RouteContainer(route, mManifests.Where(mw => BusinessObject.IsNull(mw.Route))));
                  }
                }
              }

              //Collection<RouteContainer> routes = new Collection<RouteContainer>();
              //var varRouteResults = mManifests.Where(c => (!BusinessObject.IsNull(c.Route) && c.Route != null)).Select(c => c.Route).Distinct().OrderBy(r => r.Name);
              //if (varRouteResults != null && varRouteResults.DefaultIfEmpty() != null)
              //{
              //  foreach (var route in varRouteResults)
              //  {
              //    if (!BusinessObject.IsNull(route))
              //    {
              //      routes.Add(new RouteContainer(route, mManifests.Where(c => c.Route.Equals(route))));
              //    }
              //    //else
              //    //{
              //    //  routes.Add(new RouteContainer(route, mManifests.Where(mw => BusinessObject.IsNull(mw.Route))));
              //    //}
              //  }
              //}


              //// Handle Transfer Manifests
              //var varDCResults = mManifests.Where(m => (BusinessObject.IsNull(m.Route) || m.Route == null) && m.OriginDC != null).Select(c => new { c.OriginDC, c.DestinationDC }).Distinct().OrderBy(r => r.OriginDC.Name);
              //if (varDCResults != null && varDCResults.DefaultIfEmpty() != null)
              //{
              //  foreach (var DCCombo in varDCResults)
              //  {
              //    if (DCCombo != null && (!BusinessObject.IsNull(DCCombo.DestinationDC) && DCCombo.DestinationDC != null))
              //    {
              //      routes.Add(new RouteContainer(DCCombo.OriginDC, DCCombo.DestinationDC, mManifests.Where(c => c.OriginDC.Equals(DCCombo.OriginDC) && c.DestinationDC.Equals(DCCombo.DestinationDC))));
              //    }
              //    else
              //    {
              //      routes.Add(new RouteContainer(null, null, mManifests.Where(c => c.Route ==null && c.OriginDC == null && c.DestinationDC == null)));
              //    }
              //  }
              //}

              Routes = routes;
              break;
            //******************************************************************************************************************************
            //  DRIVERS
            //******************************************************************************************************************************
            case "By Drivers":
              Collection<DriverContainer> drivers = new Collection<DriverContainer>();
              if (mManifests.Where(mw => mw.Driver == null).DefaultIfEmpty() != null)
              {
                drivers.Add(new DriverContainer(new SimpleContact(true), mManifests.Where(mw => mw.Driver == null)));
              }

              var varDriverResults = mManifests.Where(mw => mw.Driver != null).Select(c => c.Driver).Distinct().OrderBy(r => r.Fullname);
              if (varDriverResults != null && varDriverResults.DefaultIfEmpty() != null)
              {
                foreach (var driver in varDriverResults)
                {
                  if (!BusinessObject.IsNull(driver))
                  {
                    drivers.Add(new DriverContainer(driver, mManifests.Where(c => !BusinessObject.IsNull(c.Driver) && c.Driver.Equals(driver))));
                  }
                  else
                  {
                    drivers.Add(new DriverContainer(driver, mManifests.Where(mw => BusinessObject.IsNull(mw.Driver))));
                  }
                }
              }
              Drivers = drivers;
              break;
            //******************************************************************************************************************************
            //  COURIERS
            //******************************************************************************************************************************
            case "By Couriers":
              Collection<CourierContainer> couriers = new Collection<CourierContainer>();
              couriers.Add(new CourierContainer(new Contractor(true), mManifests.Where(mw => mw.Courier == null)));


              var varCourierResults = mManifests.Where(mw => mw.Courier != null).Select(c => c.Courier).Distinct().OrderBy(r => r.CompanyName);
              if (varCourierResults != null && varCourierResults.DefaultIfEmpty() != null)
              {
                foreach (var courier in varCourierResults)
                {
                  if (!BusinessObject.IsNull(courier))
                  {
                    couriers.Add(new CourierContainer(courier, mManifests.Where(c => !BusinessObject.IsNull(c.Courier) && c.Courier.Equals(courier))));
                  }
                  else
                  {
                    couriers.Add(new CourierContainer(courier, mManifests.Where(mw => BusinessObject.IsNull(mw.Courier))));
                  }
                }
              }
              Couriers = couriers;
              break;
          }
        }
      }
    }

    #endregion

    #region Collection<VehicleContainer> Vehicles

    Collection<VehicleContainer> mVehicles;
    public Collection<VehicleContainer> Vehicles
    {
      get { return mVehicles; }
      set { SetProperty<Collection<VehicleContainer>>(ref mVehicles, value); }
    }

    #endregion

    #region Collection<VehicleContainer> Routes

    Collection<RouteContainer> mRoutes;
    public Collection<RouteContainer> Routes
    {
      get { return mRoutes; }
      set { SetProperty<Collection<RouteContainer>>(ref mRoutes, value); }
    }

    #endregion

    #region Collection<CityContainer> Cities

    Collection<CityContainer> mCities;
    public Collection<CityContainer> Cities
    {
      get { return mCities; }
      set { SetProperty<Collection<CityContainer>>(ref mCities, value); }
    }

    #endregion

    #region Collection<DriverContainer> Drivers

    Collection<DriverContainer> mDrivers;
    public Collection<DriverContainer> Drivers
    {
      get { return mDrivers; }
      set { SetProperty<Collection<DriverContainer>>(ref mDrivers, value); }
    }

    #endregion

    #region Collection<CourierContainer> Couriers

    Collection<CourierContainer> mCouriers;
    public Collection<CourierContainer> Couriers
    {
      get { return mCouriers; }
      set { SetProperty<Collection<CourierContainer>>(ref mCouriers, value); }
    }

    #endregion

    #endregion


    #region "Filters"

    #region string FilterBy

    public const string FilterByProperty = "FilterBy";
    string mFilterBy = "";
    public string FilterBy
    {
      get { return mFilterBy; }
      set
      {
        if (SetProperty(ref mFilterBy, value))
        {
          LoadManifests();
        }
      }
    }

    #endregion

    #region DistributionCenter DistributionCenter

    public const string DistributionCenterProperty = "DistributionCenter";
    DistributionCenter mDistributionCenter = Cache.Instance.GetObjects<DistributionCenter>(dc => dc.OrganizationalUnit.Equals(SecurityContext.OrganizationalUnit)).FirstOrDefault();
    public DistributionCenter DistributionCenter
    {
      get { return mDistributionCenter; }
      set
      {
        if (SetProperty<DistributionCenter>(ref mDistributionCenter, value))
        {
          LoadManifests();
          //OnPropertyChanged("Routes");
        }
      }
    }

    #endregion

    #region DocumentState DocumentState

    public const string DocumentStateProperty = "DocumentState";
    DocumentTypeState mDocumentState;// = Cache.Instance.GetObjects<DocumentTypeState>(true, dts => dts.Owner.Identifier.Equals(CollectionManifest.DocumentTypeIdentifier) || dts.Owner.Identifier.Equals(TransferManifest.DocumentTypeIdentifier) || dts.Owner.Identifier.Equals(DeliveryManifest.DocumentTypeIdentifier)).Distinct();
    public DocumentTypeState DocumentState
    {
      get { return mDocumentState; }
      set
      {
        if (SetProperty<DocumentTypeState>(ref mDocumentState, value))
        {
          LoadManifests();
        }
      }
    }

    #endregion

    #region Contractor Courier

    public const string CourierProperty = "Courier";
    Contractor mCourier;// = Cache.Instance.GetObjects<Contractor>(true, c => (!BusinessObject.IsNull(c.ContractorType) && c.ContractorType.IsFlagged(ContractorTypeFlags.Is3rdPartyCourier)), c => c.CompanyName, true);
    public Contractor Courier
    {
      get { return mCourier; }
      set
      {
        if (SetProperty<Contractor>(ref mCourier, value))
        {
          LoadManifests();
        }
      }
    }

    #endregion

    #region Employee Driver

    public const string DriverProperty = "Driver";
    Contact mDriver;// = Cache.Instance.GetObjects<Employee>(true, e => e.ContactType.IsFlagged(ContactTypeFlags.Driver), e => e.Fullname, true);
    public Contact Driver
    {
      get { return mDriver; }
      set
      {
        if (SetProperty<Contact>(ref mDriver, value))
        {
          LoadManifests();
        }
      }
    }

    #endregion

    #region Route Route

    public const string RouteProperty = "Route";
    Route mRoute;
    public Route Route
    {
      get { return mRoute; }
      set
      {
        if (SetProperty<Route>(ref mRoute, value))
        {
          LoadManifests();
        }
      }
    }

    #endregion


    #region DateTime fromDateFilter

    public const string fromDateFilterProperty = "fromDateFilter";
    DateTime mfromDateFilter = DateTime.Today.AddMonths(-1);
    public DateTime fromDateFilter
    {
      get { return mfromDateFilter; }
      set { mfromDateFilter = value; }
    }

    #endregion

    #region DateTime toDateFilter

    public const string toDateFilterProperty = "toDateFilter";
    DateTime mtoDateFilter = DateTime.Today.AddDays(1);
    public DateTime toDateFilter
    {
      get { return mtoDateFilter; }
      set { mtoDateFilter = value; }
    }

    #endregion

    #region string textFilter

    public const string textFilterProperty = "textFilter";
    string mtextFilter;
    public string textFilter
    {
      get { return mtextFilter; }
      set { mtextFilter = value; }
    }

    #endregion

    #endregion

    public override void LoadData()
    {
      LoadManifests();
      base.LoadData();
    }

    void LoadManifests()
    {
      try
      {
        using (var svc = ProxyFactory.GetService<IReportingModuleService>(SecurityContext.ServerName))
        {
          Manifests = svc.LoadManifestsAny(0, (Route == null ? 0 : Route.Id), (Courier == null ? 0 : Courier.Id), (Driver == null ? 0 : Driver.Id), (DocumentState == null ? 0 : DocumentState.Id), textFilter, fromDateFilter, toDateFilter);
        }
      } catch (Exception ex)
      {
        ExceptionHelper.HandleException(ex);
      }
    }

    protected override void OnPropertyChanged(string propertyName)
    {
      switch (propertyName)
      {
        case ManifestReference.IsCheckedProperty:
          OnPropertyChanged("IsChecked");
          break;
        case "FilterBy":
          //OnPropertyChanged("FilterBy");
          break;
      }
      base.OnPropertyChanged(propertyName);
    }
  }
}

﻿using Afx.Business;
using Afx.Business.Data;
using Afx.Business.Documents;
using Afx.Prism.RibbonMdi;
using Afx.Prism.RibbonMdi.Events;
using FreightManagement.Business;
using Geographics.Business;
using Microsoft.Practices.Prism.Commands;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportingModule.Prism.Activities.OperationsManifestReportManagement
{
  public class RouteContainer : BusinessObject
  {
    public RouteContainer(Route route, IEnumerable<ManifestReference> collections)
    {
      Route = route;
      Manifests = collections;
    }
    public RouteContainer(DistributionCenter originDC, DistributionCenter destinationDC, IEnumerable<ManifestReference> collections)
    {
      DCtoDC = new CustomRoute(originDC, destinationDC);
      Manifests = collections;
    }

    #region Route Route

    public const string RouteProperty = "Route";
    Route mRoute;
    public Route Route
    {
      get { return mRoute; }
      private set { mRoute = value; }
    }

    #endregion

    #region CustomRoute DCtoDC

    public const string DCtoDCProperty = "DCtoDC";
    CustomRoute mDCtoDC;
    public CustomRoute DCtoDC
    {
      get { return mDCtoDC; }
      set { mDCtoDC = value; }
    }

    #endregion

    public string RouteName
    {
      get
      {
        if (!BusinessObject.IsNull(Route))
        {
          return Route.Name;
        }
        else
        {
          return (DCtoDC != null ? DCtoDC.Name : "Blank");
        }
      }
    }

    /// <summary>
    /// Custom sub class to handle Transfers
    /// </summary>
    public class CustomRoute
    {
      public CustomRoute(DistributionCenter originDC, DistributionCenter destinationDC) { Origin = originDC.Location; Destination = destinationDC.Location; }

      public string Name { get { return string.Format("{0} -> {1}", Origin.Name, Destination.Name); } }
      public Location Origin { get; set; }
      public Location Destination { get; set; }
    }

    #region IEnumerable<WaybillFloorStatus> Manifests

    public const string ManifestsProperty = "Manifests";
    IEnumerable<ManifestReference> mManifests;
    public IEnumerable<ManifestReference> Manifests
    {
      get { return mManifests; }
      private set
      {
        mManifests = value;

        try
        {
          Collection<CityContainer> cities = new Collection<CityContainer>();
          if (!BusinessObject.IsNull(Route))
          {
            foreach (var city in mManifests.Where(c => !BusinessObject.IsNull(c.Route) && !BusinessObject.IsNull(c.Route.Destination)).Select(c => c.Route.Destination.City).Distinct().OrderBy(l => l.Name))
            {
              cities.Add(new CityContainer(city, mManifests.Where(c => c.Route.Destination.City.Equals(city)).OrderBy(c => c.ManifestDate)));
            }
          }
          else
          {
            foreach (var city in mManifests.Where(mw => !BusinessObject.IsNull(mw.DestinationDC)).Select(c => c.DestinationDC).Distinct().OrderBy(l => l.Name))
            {
              cities.Add(new CityContainer(city.Location, mManifests.Where(c => c.DestinationDC.Equals(city)).OrderBy(m => m.ManifestDate)));
            }
          }
          Cities = cities;
        } 
        catch
        {
          throw;
        }
      }
    }

    #endregion

    #region IEnumerable<CityContainer> Cities

    public const string CitiesProperty = "Cities";
    IEnumerable<CityContainer> mCities;
    public IEnumerable<CityContainer> Cities
    {
      get { return mCities; }
      private set { mCities = value; }
    }

    #endregion

    #region DelegateCommand<ManifestReference> OpenManifestCommand

    DelegateCommand<ManifestReference> mOpenManifestCommand;
    public DelegateCommand<ManifestReference> OpenManifestCommand
    {
      get
      {
        return mOpenManifestCommand ?? (mOpenManifestCommand = new DelegateCommand<ManifestReference>(ExecuteOpenDeliveryManifest, CanExecuteOpenDeliveryManifest));
      }
    }

    bool CanExecuteOpenDeliveryManifest(ManifestReference args)
    {
      return true;
    }

    void ExecuteOpenDeliveryManifest(ManifestReference args)
    {
      MdiApplicationController.Current.EventAggregator.GetEvent<EditDocumentEvent>().Publish(new EditDocumentEventArgs(Cache.Instance.GetObject<DocumentType>(args.ManifestType.Identifier), args.GlobalIdentifier));
    }

    #endregion

  }
}

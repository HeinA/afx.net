﻿using Afx.Business;
using Afx.Business.Activities;
using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Activities;
using ReportingModule.Business;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportingModule.Prism.Activities.OperationsRouteReportManagement
{
  public partial class OperationsRouteReportManagementController : MdiCustomActivityController<OperationsRouteReportManagement, OperationsRouteReportManagementViewModel>
  {
    #region Constructors

    [InjectionConstructor]
    public OperationsRouteReportManagementController(IController controller)
      : base(controller)
    {
    }

    #endregion

    #region OnLoaded
    
    protected override void OnLoaded()
    {
      ViewModel.IsFocused = true;
      base.OnLoaded();
    }

    #endregion
    
    #region ExecuteOperation
    
    protected override void ExecuteOperation(Operation op, BusinessObject argument)
    {
      switch (op.Identifier)
      {
        case Operations.ExcelExport:
          //if (DataContext.Waybills.Where(w => w.IsChecked).Count() != 0)
          //{
          //  ExcelHelper.Export("Outstanding POD", DataContext.Waybills.Where(w => w.IsChecked).OrderBy(w => w.WaybillDate).ThenBy(w => w.WaybillNumber), WaybillReference.ExportKeys.OutstandingPods);
          //}
          //else
          //{
          //  ExcelHelper.Export("Outstanding POD", DataContext.Waybills.OrderBy(w => w.WaybillDate).ThenBy(w => w.WaybillNumber), WaybillReference.ExportKeys.OutstandingPods);
          //}
          break;
      }

      base.ExecuteOperation(op, argument);
    }
    
    #endregion
  }
}
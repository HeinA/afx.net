﻿using Afx.Business.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace ReportingModule.Prism.Activities.Converters
{
  public class ReportingSecurityAdminVisibilityConverter : IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    {
      
      if (SecurityContext.User.Roles.Where(r => r.Name.Contains("Administrator") || r.Name.Contains("Supervisor")).Count() > 0)
      {
        return Visibility.Visible;
      }
      else
      {
        return Visibility.Collapsed;
      }

      //string st = ((string)value).Replace("System.Windows.Controls.ComboBoxItem: ", "");
      //switch (st)
      //{
      //  case "By Routes":
      //    return Visibility.Visible;
      //  default:
      //    return Visibility.Collapsed;
      //}
    }

    public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    {
      throw new NotImplementedException();
    }
  }
}

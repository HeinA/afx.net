﻿using Afx.Business.Activities;
using ReportingModule.Business;
using ReportingModule.Business.Service;
using Afx.Prism.RibbonMdi.Activities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportingModule.Prism.Activities.AccountingAgingReportManagement
{
  public partial class AccountingAgingReportManagement : CustomActivityContext
  {
    public AccountingAgingReportManagement()
    {
    }

    public AccountingAgingReportManagement(Activity activity)
      : base(activity)
    {
    }
  }
}

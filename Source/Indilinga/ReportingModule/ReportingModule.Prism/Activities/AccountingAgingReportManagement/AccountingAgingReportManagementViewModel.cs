﻿using Afx.Business.Security;
using Afx.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Activities;
using ReportingModule.Business;

namespace ReportingModule.Prism.Activities.AccountingAgingReportManagement
{
  public partial class AccountingAgingReportManagementViewModel : MdiCustomActivityViewModel<AccountingAgingReportManagement>
  {
    #region Constructors

    [InjectionConstructor]
    public AccountingAgingReportManagementViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion
  }
}

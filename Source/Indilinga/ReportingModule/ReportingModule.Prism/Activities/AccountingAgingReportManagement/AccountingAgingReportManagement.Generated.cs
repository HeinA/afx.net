﻿using ReportingModule.Business;
using ReportingModule.Business.Service;
using Afx.Business.Service;
using Afx.Prism.RibbonMdi.Activities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.Composition;
using Afx.Business.Security;

namespace ReportingModule.Prism.Activities.AccountingAgingReportManagement
{
  //**************************************************************************************************************
  //**************************************************************************************************************
  //*                                                                                                            *
  //*     This code is auto-generated.  Do not place any cutom code here, it will be lost on re-generation.      *
  //*                                                                                                            *
  //**************************************************************************************************************
  //**************************************************************************************************************
  [Export("Context:" + global::ReportingModule.Prism.Activities.AccountingAgingReportManagement.AccountingAgingReportManagement.Key)]
  public partial class AccountingAgingReportManagement
  {
    public const string Key = "{a1d07afa-47b4-4676-b19f-9fb9a2e611f1}";
  }
}

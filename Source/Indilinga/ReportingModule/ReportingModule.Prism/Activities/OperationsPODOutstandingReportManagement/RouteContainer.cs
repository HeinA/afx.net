﻿using Afx.Business;
using FreightManagement.Business;
using Geographics.Business;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportingModule.Prism.Activities.OperationsPODOutstandingReportManagement
{
  public class RouteContainer : BusinessObject
  {
    public RouteContainer(Route route, IEnumerable<WaybillReference> collections)
    {
      Route = route;
      Waybills = new ObservableCollection<WaybillReference>(collections);
    }
    public RouteContainer(DistributionCenter originDC, DistributionCenter destinationDC, IEnumerable<WaybillReference> collections)
    {
      DCtoDC = new CustomRoute(originDC, destinationDC);
      Waybills = collections;
    }

    #region Route Route

    public const string RouteProperty = "Route";
    Route mRoute;
    public Route Route
    {
      get { return mRoute; }
      private set { mRoute = value; }
    }

    #endregion

    #region CustomRoute DCtoDC

    public const string DCtoDCProperty = "DCtoDC";
    CustomRoute mDCtoDC;
    public CustomRoute DCtoDC
    {
      get { return mDCtoDC; }
      set { mDCtoDC = value; }
    }

    #endregion

    public string RouteName
    {
      get
      {
        if (!BusinessObject.IsNull(Route))
        {
          return Route.Name;
        }
        else
        {
          return (DCtoDC != null ? DCtoDC.Name : "Blank");
        }
      }
    }

    /// <summary>
    /// Custom sub class to handle Transfers
    /// </summary>
    public class CustomRoute
    {
      public CustomRoute(DistributionCenter originDC, DistributionCenter destinationDC) { Origin = originDC.Location; Destination = destinationDC.Location; }

      public string Name { get { return string.Format("{0} -> {1}", Origin.Name, Destination.Name); } }
      public Location Origin { get; set; }
      public Location Destination { get; set; }
    }

    #region IEnumerable<WaybillFloorStatus> Waybills

    public const string WaybillsProperty = "Waybills";
    IEnumerable<WaybillReference> mWaybills;
    public IEnumerable<WaybillReference> Waybills
    {
      get { return mWaybills; }
      private set
      {
        mWaybills = value;

        try
        {
          Collection<CityContainer> cities = new Collection<CityContainer>();
          if (!BusinessObject.IsNull(Route))
          {
            foreach (var city in mWaybills.Where(c => !BusinessObject.IsNull(c.Route) && !BusinessObject.IsNull(c.Route.Destination)).Select(c => c.Route.Destination.City).Distinct().OrderBy(l => l.Name))
            {
              cities.Add(new CityContainer(city, mWaybills.Where(ww => ww.Route.Destination.City.Equals(city)).OrderBy(c => c.WaybillDate)));
            }
          }
          else
          {
            foreach (var city in mWaybills.Where(ww => !BusinessObject.IsNull(ww.Route.Destination.City)).Select(c => c.Route.Destination.City).Distinct().OrderBy(l => l.Name))
            {
              cities.Add(new CityContainer(city, mWaybills.Where(c => c.Route.Destination.City.Equals(city)).OrderBy(m => m.WaybillDate)));
            }
          }
          Cities = cities;
        } 
        catch
        {
          throw;
        }
      }
    }

    #endregion

    #region IEnumerable<CityContainer> Cities

    public const string CitiesProperty = "Cities";
    IEnumerable<CityContainer> mCities;
    public IEnumerable<CityContainer> Cities
    {
      get { return mCities; }
      private set { mCities = value; }
    }

    #endregion

  }
}

﻿using AccountManagement.Business;
using AccountManagement.Prism.Documents.Account;
using Afx.Business;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Documents;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using Afx.Prism.RibbonMdi.Events;
using FreightManagement.Business;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportingModule.Prism.Activities.OperationsPODOutstandingReportManagement
{
  public class WaybillStateContainer : BusinessObject
  {
    public WaybillStateContainer(DocumentTypeState waybillState, IEnumerable<WaybillReference> collections)
    {
      WaybillState = waybillState;
      Waybills = collections;
    }

    public string StateName
    {
      get
      {
        string s = WaybillState.Name;
        if (string.IsNullOrWhiteSpace(s)) return "Blank";
        return s;
      }
    }




    #region DocumentTypeState WaybillState

    public const string WaybillStateProperty = "WaybillState";
    DocumentTypeState mWaybillState;
    public DocumentTypeState WaybillState
    {
      get { return mWaybillState; }
      private set
      {
        mWaybillState = value;
      }
    }

    #endregion

    #region IEnumerable<WaybillReference> Waybills

    public const string WaybillsProperty = "Waybills";
    IEnumerable<WaybillReference> mWaybills;
    public IEnumerable<WaybillReference> Waybills
    {
      get { return mWaybills; }
      //private set
      //{
      //  mManifests = value;
      //}
      private set
      {
        mWaybills = value;

        try
        {
          Collection<AccountContainer> accounts = new Collection<AccountContainer>();
          //routes.Add(new RouteContainer(new Route(true), mManifests.Where(mw => BusinessObject.IsNull(mw.Route) && mw.Driver.Equals(Driver))));

          var varResults = (
                              BusinessObject.IsNull(WaybillState)
                              ?
                              mWaybills.Where(ww => ww.WaybillState == null).Select(ws => ws.Account).Distinct().OrderBy(wob => wob.Name)
                              :
                              mWaybills.Where(ww => ww.WaybillState.Equals(WaybillState)).Select(ws => ws.Account).Distinct().OrderBy(wob => wob.Name)
                           );
          if (varResults != null && varResults.DefaultIfEmpty() != null)
          {
            foreach (var account in varResults)
            //foreach (var route in mManifests.Where(mw => !BusinessObject.IsNull(mw.Driver) && mw.Driver.Equals(Driver)).Select(ms => ms.Route).Distinct().OrderBy(mob => mob.Name))
            {
              if (!BusinessObject.IsNull(account))
              {
                accounts.Add(new AccountContainer(account, mWaybills.Where(ww => ww.Account.Equals(account))));
              }
              else
              {
                accounts.Add(new AccountContainer(account, mWaybills));
              }
            }
          }
          Accounts = accounts;
        }
        catch
        {
          throw;
        }
      }
    }

    #endregion

    #region IEnumerable<AccountContainer> Accounts

    public const string AccountsProperty = "Accounts";
    IEnumerable<AccountContainer> mAccounts;
    public IEnumerable<AccountContainer> Accounts
    {
      get { return mAccounts; }
      set { mAccounts = value; }
    }

    #endregion

    #region IEnumerable<CityContainer> Cities

    public const string CitiesProperty = "Cities";
    IEnumerable<CityContainer> mCities;
    public IEnumerable<CityContainer> Cities
    {
      get { return mCities; }
      set { mCities = value; }
    }

    #endregion


  }
}

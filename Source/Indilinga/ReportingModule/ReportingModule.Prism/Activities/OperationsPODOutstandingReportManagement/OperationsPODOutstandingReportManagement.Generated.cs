﻿using ReportingModule.Business;
using ReportingModule.Business.Service;
using Afx.Business.Service;
using Afx.Prism.RibbonMdi.Activities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.Composition;
using Afx.Business.Security;

namespace ReportingModule.Prism.Activities.OperationsPODOutstandingReportManagement
{
  //**************************************************************************************************************
  //**************************************************************************************************************
  //*                                                                                                            *
  //*     This code is auto-generated.  Do not place any cutom code here, it will be lost on re-generation.      *
  //*                                                                                                            *
  //**************************************************************************************************************
  //**************************************************************************************************************
  [Export("Context:" + global::ReportingModule.Prism.Activities.OperationsPODOutstandingReportManagement.OperationsPODOutstandingReportManagement.Key)]
  public partial class OperationsPODOutstandingReportManagement
  {
    public const string Key = "{c665ca9d-cbc7-4bb8-b574-a10e772f8f2f}";
  }
}

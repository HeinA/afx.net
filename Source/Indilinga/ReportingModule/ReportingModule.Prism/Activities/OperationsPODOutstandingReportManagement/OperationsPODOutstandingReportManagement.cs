﻿using Afx.Business.Activities;
using ReportingModule.Business;
using ReportingModule.Business.Service;
using Afx.Prism.RibbonMdi.Activities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using Afx.Business.Collections;
using Afx.Business.Service;
using Afx.Business.Security;
using FreightManagement.Business;
using AccountManagement.Business;
using Afx.Business.Documents;
using Afx.Business.Data;
using Afx.Business;

namespace ReportingModule.Prism.Activities.OperationsPODOutstandingReportManagement
{
  public partial class OperationsPODOutstandingReportManagement : CustomActivityContext
  {
    public OperationsPODOutstandingReportManagement()
    {
    }

    public OperationsPODOutstandingReportManagement(Activity activity)
      : base(activity)
    {
    }

    public override bool IsDirty
    {
      get { return false; }
      set { }
    }

    #region "Containers"

    #region Collection<WaybillReference> Waybills

    public const string WaybillsProperty = "Waybills";
    BasicCollection<WaybillReference> mWaybills;
    public BasicCollection<WaybillReference> Waybills
    {
      get { return mWaybills; }
      set
      {
        if (SetProperty<BasicCollection<WaybillReference>>(ref mWaybills, value))
        {
          switch (FilterBy.Replace("System.Windows.Controls.ComboBoxItem: ", ""))
          {

            case "By Waybill States":
              Collection<WaybillStateContainer> waybillstates = new Collection<WaybillStateContainer>();
              //if (mWaybills.Where(mw => mw.WaybillState == null).DefaultIfEmpty() != null)
              //{
              //  waybillstates.Add(new WaybillStateContainer(new DocumentTypeState(true), mWaybills.Where(mw => mw.WaybillState == null)));
              //}

              var varWaybillStateResults = mWaybills.Where(mw => mw.WaybillState != null).Select(c => c.WaybillState).Distinct().OrderBy(r => r.Name);
              if (varWaybillStateResults != null && varWaybillStateResults.DefaultIfEmpty() != null)
              {
                foreach (var waybillstate in varWaybillStateResults)
                {
                  if (!BusinessObject.IsNull(waybillstate))
                  {
                    waybillstates.Add(new WaybillStateContainer(waybillstate, mWaybills.Where(c => !BusinessObject.IsNull(c.WaybillState) && c.WaybillState.Equals(waybillstate))));
                  }
                  else
                  {
                    waybillstates.Add(new WaybillStateContainer(waybillstate, mWaybills.Where(mw => BusinessObject.IsNull(mw.WaybillState))));
                  }
                }
              }
              WaybillStates = waybillstates;
              break;
            case "By Accounts":
              Collection<AccountContainer> accounts = new Collection<AccountContainer>();
              //accounts.Add(new AccountContainer(new AccountReference(true), mWaybills.Where(mw => mw.Account == null)));


              var varCourierResults = mWaybills.Where(mw => mw.Account != null).Select(c => c.Account).Distinct().OrderBy(r => r.Name);
              if (varCourierResults != null && varCourierResults.DefaultIfEmpty() != null)
              {
                foreach (var account in varCourierResults)
                {
                  if (!BusinessObject.IsNull(account))
                  {
                    accounts.Add(new AccountContainer(account, mWaybills.Where(c => c.Account != null && c.Account.Equals(account))));
                  }
                  else
                  {
                    accounts.Add(new AccountContainer(account, mWaybills.Where(mw => mw.Account == null)));
                  }
                }
              }
              Accounts = accounts;
              break;
          }
        }
      }
    }

    #endregion

    #region Collection<VehicleContainer> Routes

    Collection<RouteContainer> mRoutes;
    public Collection<RouteContainer> Routes
    {
      get { return mRoutes; }
      set { SetProperty<Collection<RouteContainer>>(ref mRoutes, value); }
    }

    #endregion

    #region Collection<CityContainer> Cities

    Collection<CityContainer> mCities;
    public Collection<CityContainer> Cities
    {
      get { return mCities; }
      set { SetProperty<Collection<CityContainer>>(ref mCities, value); }
    }

    #endregion

    #region Collection<AccountContainer> Accounts

    Collection<AccountContainer> mAccounts;
    public Collection<AccountContainer> Accounts
    {
      get { return mAccounts; }
      set { SetProperty<Collection<AccountContainer>>(ref mAccounts, value); }
    }

    #endregion

    #region Collection<WaybillStateContainer> WaybillStates

    Collection<WaybillStateContainer> mWaybillStates;
    public Collection<WaybillStateContainer> WaybillStates
    {
      get { return mWaybillStates; }
      set { SetProperty<Collection<WaybillStateContainer>>(ref mWaybillStates, value); }
    }

    #endregion

    #endregion


    #region "Filters"

    #region string FilterBy

    public const string FilterByProperty = "FilterBy";
    string mFilterBy = "";
    public string FilterBy
    {
      get { return mFilterBy; }
      set
      {
        if (SetProperty(ref mFilterBy, value))
        {
          LoadWaybills();
        }
      }
    }

    #endregion

    #region DistributionCenter DistributionCenter

    public const string DistributionCenterProperty = "DistributionCenter";
    DistributionCenter mDistributionCenter = Cache.Instance.GetObjects<DistributionCenter>(dc => dc.OrganizationalUnit.Equals(SecurityContext.OrganizationalUnit)).FirstOrDefault();
    public DistributionCenter DistributionCenter
    {
      get { return mDistributionCenter; }
      set
      {
        if (SetProperty<DistributionCenter>(ref mDistributionCenter, value))
        {
          LoadWaybills();
          //OnPropertyChanged("Routes");
        }
      }
    }

    #endregion

    #region AccountReference Account

    public const string AccountProperty = "Account";
    AccountReference mAccount;// = Cache.Instance.GetObjects<Contractor>(true, c => (!BusinessObject.IsNull(c.ContractorType) && c.ContractorType.IsFlagged(ContractorTypeFlags.Is3rdPartyCourier)), c => c.CompanyName, true);
    public AccountReference Account
    {
      get { return mAccount; }
      set
      {
        if (SetProperty<AccountReference>(ref mAccount, value))
        {
          LoadWaybills();
        }
      }
    }

    #endregion

    #region Route Route

    public const string RouteProperty = "Route";
    Route mRoute;
    public Route Route
    {
      get { return mRoute; }
      set
      {
        if (SetProperty<Route>(ref mRoute, value))
        {
          LoadWaybills();
        }
      }
    }

    #endregion


    #region DateTime fromDateFilter

    public const string fromDateFilterProperty = "fromDateFilter";
    DateTime mfromDateFilter = DateTime.Today.AddMonths(-1);
    public DateTime fromDateFilter
    {
      get { return mfromDateFilter; }
      set { mfromDateFilter = value; }
    }

    #endregion

    #region DateTime toDateFilter

    public const string toDateFilterProperty = "toDateFilter";
    DateTime mtoDateFilter = DateTime.Today.AddDays(1);
    public DateTime toDateFilter
    {
      get { return mtoDateFilter; }
      set { mtoDateFilter = value; }
    }

    #endregion

    #region string textFilter

    public const string textFilterProperty = "textFilter";
    string mtextFilter;
    public string textFilter
    {
      get { return mtextFilter; }
      set { mtextFilter = value; }
    }

    #endregion

    #endregion

    public override void LoadData()
    {
      LoadWaybills();
      base.LoadData();
    }

    void LoadWaybills()
    {
      using (var svc = ProxyFactory.GetService<IReportingModuleService>(SecurityContext.ServerName))
      {
        Waybills = svc.LoadWaybillsOutstandingPODs(0, (Route == null ? 0 : Route.Id), (Account == null ? 0 : Account.Id), textFilter, fromDateFilter, toDateFilter);
      }
    }

    protected override void OnPropertyChanged(string propertyName)
    {
      switch (propertyName)
      {
        case WaybillReference.IsCheckedProperty:
          OnPropertyChanged("IsChecked");
          break;
        case "FilterBy":
          //OnPropertyChanged("FilterBy");
          break;
      }
      base.OnPropertyChanged(propertyName);
    }
  }
}

﻿using Afx.Business;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Documents;
using Afx.Prism.RibbonMdi;
using Afx.Prism.RibbonMdi.Events;
using FreightManagement.Business;
using Geographics.Business;
using Microsoft.Practices.Prism.Commands;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportingModule.Prism.Activities.OperationsPODOutstandingReportManagement
{
  public class CityContainer : BusinessObject
  {
    public CityContainer(Location city, IEnumerable<WaybillReference> collections)
    {
      City = city;
      Waybills = new ObservableCollection<WaybillReference>(collections);
    }

    #region Location City

    public const string CityProperty = "City";
    Location mCity;
    public Location City
    {
      get { return mCity; }
      set { mCity = value; }
    }

    #endregion

    #region IEnumerable<WaybillReference> Waybills

    public const string WaybillsProperty = "Waybills";
    IEnumerable<WaybillReference> mWaybills;
    public IEnumerable<WaybillReference> Waybills
    {
      get { return mWaybills; }
      private set
      {
        mWaybills = new BasicCollection<WaybillReference>(value);
      }
    }

    #endregion

    #region DelegateCommand<WaybillReference> OpenWaybillCommand

    DelegateCommand<WaybillReference> mOpenWaybillCommand;
    public DelegateCommand<WaybillReference> OpenWaybillCommand
    {
      get
      {
        return mOpenWaybillCommand ?? (mOpenWaybillCommand = new DelegateCommand<WaybillReference>(ExecuteOpenDeliveryManifest, CanExecuteOpenDeliveryManifest));
      }
    }

    bool CanExecuteOpenDeliveryManifest(WaybillReference args)
    {
      return true;
    }

    void ExecuteOpenDeliveryManifest(WaybillReference args)
    {
      MdiApplicationController.Current.EventAggregator.GetEvent<EditDocumentEvent>().Publish(new EditDocumentEventArgs(Cache.Instance.GetObject<DocumentType>(Waybill.DocumentTypeIdentifier), args.GlobalIdentifier));
    }

    #endregion

  }
}

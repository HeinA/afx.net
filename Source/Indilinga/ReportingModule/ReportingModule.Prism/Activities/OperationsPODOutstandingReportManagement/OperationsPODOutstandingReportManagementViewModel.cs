﻿using Afx.Business.Security;
using Afx.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Activities;
using ReportingModule.Business;
using AccountManagement.Business;
using Afx.Business.Data;
using Afx.Business.Documents;
using FreightManagement.Business;

namespace ReportingModule.Prism.Activities.OperationsPODOutstandingReportManagement
{
  public partial class OperationsPODOutstandingReportManagementViewModel : MdiCustomActivityViewModel<OperationsPODOutstandingReportManagement>
  {
    #region Constructors

    [InjectionConstructor]
    public OperationsPODOutstandingReportManagementViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    #region IEnumerable<DistributionCenter> DistributionCenters

    public IEnumerable<DistributionCenter> DistributionCenters
    {
      get { return Cache.Instance.GetObjects<DistributionCenter>(true, dc => dc.Name, true); }
    }

    #endregion

    #region IEnumerable<Account> Accounts

    public IEnumerable<AccountReference> Accounts
    {
      get { return Cache.Instance.GetObjects<AccountReference>(true, c => c.Name, true); }
    }

    #endregion

    #region IEnumerable<DocumentTypeState> WaybillStates

    public IEnumerable<DocumentTypeState> WaybillStates
    {
      get { return Cache.Instance.GetObjects<DocumentTypeState>(true, dts => dts.Owner.Identifier.Equals(Waybill.DocumentTypeIdentifier), dts => dts.Name, true); }
    }

    #endregion

    #region IEnumerable<Route> Routes

    public IEnumerable<Route> Routes
    {
      get { return Cache.Instance.GetObjects<Route>(true, r => r.Owner.Equals(this.Model.DistributionCenter), r => r.Name, true); }
    }

    #endregion

    public void RefreshRoutes()
    {
      OnPropertyChanged("Routes");
    }

    public void RefreshCities()
    {
      OnPropertyChanged("Accounts");
    }

    protected override void OnModelPropertyChanged(string propertyName)
    {
      switch (propertyName)
      {
        case "DistributionCenter":
          OnPropertyChanged("Routes");
          break;
        case "Account":
          OnPropertyChanged("Accounts");
          break;
      }

      base.OnModelPropertyChanged(propertyName);
    }
  }
}

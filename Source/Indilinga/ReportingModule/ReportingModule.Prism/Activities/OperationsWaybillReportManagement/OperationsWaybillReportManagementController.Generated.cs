﻿using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Prism.RibbonMdi.Activities;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.Composition;

namespace ReportingModule.Prism.Activities.OperationsWaybillReportManagement
{
  //**************************************************************************************************************
  //**************************************************************************************************************
  //*                                                                                                            *
  //*     This code is auto-generated.  Do not place any cutom code here, it will be lost on re-generation.      *
  //*                                                                                                            *
  //**************************************************************************************************************
  //**************************************************************************************************************
  [Export("Controller:" + global::ReportingModule.Prism.Activities.OperationsWaybillReportManagement.OperationsWaybillReportManagement.Key)]
  public partial class OperationsWaybillReportManagementController
  {
  }
}

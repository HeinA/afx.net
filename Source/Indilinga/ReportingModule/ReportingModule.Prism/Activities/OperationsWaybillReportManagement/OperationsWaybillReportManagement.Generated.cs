﻿using ReportingModule.Business;
using ReportingModule.Business.Service;
using Afx.Business.Service;
using Afx.Prism.RibbonMdi.Activities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.Composition;
using Afx.Business.Security;

namespace ReportingModule.Prism.Activities.OperationsWaybillReportManagement
{
  //**************************************************************************************************************
  //**************************************************************************************************************
  //*                                                                                                            *
  //*     This code is auto-generated.  Do not place any cutom code here, it will be lost on re-generation.      *
  //*                                                                                                            *
  //**************************************************************************************************************
  //**************************************************************************************************************
  [Export("Context:" + global::ReportingModule.Prism.Activities.OperationsWaybillReportManagement.OperationsWaybillReportManagement.Key)]
  public partial class OperationsWaybillReportManagement
  {
    public const string Key = "{2c873004-f900-42dd-b92c-298c3cdafa9d}";
  }
}

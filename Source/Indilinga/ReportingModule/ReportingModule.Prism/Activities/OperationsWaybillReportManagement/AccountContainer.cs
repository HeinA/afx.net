﻿using AccountManagement.Business;
using AccountManagement.Prism.Documents.Account;
using Afx.Business;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Documents;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using Afx.Prism.RibbonMdi.Events;
using FreightManagement.Business;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportingModule.Prism.Activities.OperationsWaybillReportManagement
{
  public class AccountContainer : BusinessObject
  {
    public AccountContainer(AccountReference account, IEnumerable<WaybillReference> collections)
    {
      Account = account;
      Waybills = collections;
    }

    public string AccountName
    {
      get
      {
        string s = Account.Name;
        if (string.IsNullOrWhiteSpace(s)) return "Blank";
        return s;
      }
    }

    public string AccountNumber
    {
      get
      {
        string s = Account.AccountNumber;
        if (string.IsNullOrWhiteSpace(s)) return "Blank";
        return s;
      }
    }




    #region AccountReference Account

    public const string AccountProperty = "Account";
    AccountReference mAccount;
    public AccountReference Account
    {
      get { return mAccount; }
      private set { mAccount = value; }
    }

    #endregion

    #region IEnumerable<WaybillReference> Waybills

    public const string WaybillsProperty = "Waybills";
    IEnumerable<WaybillReference> mWaybills;
    public IEnumerable<WaybillReference> Waybills
    {
      get { return mWaybills; }
      //private set
      //{
      //  mManifests = value;
      //}
      private set
      {
        mWaybills = value;

        try
        {
          Collection<CityContainer> cities = new Collection<CityContainer>();
          //routes.Add(new RouteContainer(new Route(true), mManifests.Where(mw => BusinessObject.IsNull(mw.Route) && mw.Driver.Equals(Driver))));

          var varResults = (
                              BusinessObject.IsNull(Account) 
                              ? 
                              mWaybills.Where(ww => ww.Account == null).Select(ws => ws.DeliveryCity).Distinct().OrderBy(wob => wob.Name) 
                              : 
                              mWaybills.Where(ww => ww.Account.Equals(Account)).Select(ws => ws.DeliveryCity).Distinct().OrderBy(wob => wob.Name)
                           );
          if (varResults != null && varResults.DefaultIfEmpty() != null)
          {
            foreach (var city in varResults)
            //foreach (var route in mManifests.Where(mw => !BusinessObject.IsNull(mw.Driver) && mw.Driver.Equals(Driver)).Select(ms => ms.Route).Distinct().OrderBy(mob => mob.Name))
            {
              if (!BusinessObject.IsNull(city))
              {
                cities.Add(new CityContainer(city, mWaybills.Where(ww => ww.DeliveryCity.Equals(city))));
              }
              else
              {
                cities.Add(new CityContainer(city, mWaybills));
              }
            }
          }
          Cities = cities;
        }
        catch
        {
          throw;
        }
      }
    }

    #endregion

    #region IEnumerable<CityContainer> Cities

    public const string CitiesProperty = "Cities";
    IEnumerable<CityContainer> mCities;
    public IEnumerable<CityContainer> Cities
    {
      get { return mCities; }
      set { mCities = value; }
    }

    #endregion


    #region "Totals"

    #region int TotalWaybills

    public int TotalWaybills
    {
      get { return mWaybills.Count(); }
    }

    #endregion

    #region int TotalWaybillItems

    public int TotalWaybillItems
    {
      get { return mWaybills.Sum(w => w.Items); }
    }

    #endregion

    #region decimal TotalWaybillWeights

    public decimal TotalWaybillWeights
    {
      get { return mWaybills.Sum(w => w.PhysicalWeight); }
    }

    #endregion

    #region decimal TotalWaybillDeclaredValue
    
    public decimal TotalWaybillDeclaredValue
    {
      get { return mWaybills.Sum(w => w.DeclaredValue); }
    }
    
    #endregion

    #region decimal TotalWaybillCharges
    
    public decimal TotalWaybillCharges
    {
      get { return mWaybills.Sum(w => w.Charge); }
    }
    
    #endregion

    #endregion

    #region DelegateCommand<WaybillReference> OpenWaybillCommand

    DelegateCommand<WaybillReference> mOpenWaybillCommand;
    public DelegateCommand<WaybillReference> OpenWaybillCommand
    {
      get
      {
        return mOpenWaybillCommand ?? (mOpenWaybillCommand = new DelegateCommand<WaybillReference>(ExecuteOpenDeliveryManifest, CanExecuteOpenDeliveryManifest));
      }
    }

    bool CanExecuteOpenDeliveryManifest(WaybillReference args)
    {
      return true;
    }

    void ExecuteOpenDeliveryManifest(WaybillReference args)
    {
      MdiApplicationController.Current.EventAggregator.GetEvent<EditDocumentEvent>().Publish(new EditDocumentEventArgs(Cache.Instance.GetObject<DocumentType>(Waybill.DocumentTypeIdentifier), args.GlobalIdentifier));
    }

    #endregion
  }
}

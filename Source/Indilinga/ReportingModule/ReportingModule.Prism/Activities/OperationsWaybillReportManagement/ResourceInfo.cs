﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportingModule.Prism.Activities.OperationsWaybillReportManagement
{
  [Export(typeof(Afx.Prism.ResourceInfo))]
  public class ResourceInfo : Afx.Prism.ResourceInfo
  {
    public override Uri GetUri()
    {
      return new Uri("pack://application:,,,/ReportingModule.Prism;component/Activities/OperationsWaybillReportManagement/Resources.xaml", UriKind.Absolute);
    }
  
    public override int Priority
    {
      get { return 100; }
    }
}
}
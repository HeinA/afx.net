﻿using Afx.Business;
using Afx.Business.Activities;
using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Activities;
using ReportingModule.Business;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Afx.Prism.Utilities;
using FreightManagement.Business;

namespace ReportingModule.Prism.Activities.OperationsDriverTripReportManagement
{
  public partial class OperationsDriverTripReportManagementController : MdiCustomActivityController<OperationsDriverTripReportManagement, OperationsDriverTripReportManagementViewModel>
  {
    #region Constructors

    [InjectionConstructor]
    public OperationsDriverTripReportManagementController(IController controller)
      : base(controller)
    {
    }

    #endregion

    #region OnLoaded
    
    protected override void OnLoaded()
    {
      ViewModel.IsFocused = true;
      base.OnLoaded();
    }

    #endregion
    
    #region ExecuteOperation
    
    protected override void ExecuteOperation(Operation op, BusinessObject argument)
    {
      switch (op.Identifier)
      {
        case Operations.ExcelExport:
          if (DataContext.Manifests.Where(w => w.IsChecked).Count() != 0)
          {
            ExcelHelper.Export("Driver trip", DataContext.Manifests.Where(w => w.IsChecked).OrderBy(w => w.ManifestDate).ThenBy(w => w.ManifestNumber), ManifestReference.ExportKeys.DriverTrip);
          }
          else
          {
            ExcelHelper.Export("Driver trip", DataContext.Manifests.OrderBy(w => w.ManifestDate).ThenBy(w => w.ManifestNumber), ManifestReference.ExportKeys.DriverTrip);
          }
          break;
      }

      base.ExecuteOperation(op, argument);
    }
    
    #endregion
  }
}
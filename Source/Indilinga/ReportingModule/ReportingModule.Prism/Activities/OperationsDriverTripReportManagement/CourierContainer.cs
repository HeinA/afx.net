﻿using AccountManagement.Business;
using AccountManagement.Prism.Documents.Account;
using Afx.Business;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Documents;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using Afx.Prism.RibbonMdi.Events;
using ContactManagement.Business;
using FreightManagement.Business;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportingModule.Prism.Activities.OperationsDriverTripReportManagement
{
  public class CourierContainer : BusinessObject
  {
    public CourierContainer(Contractor courier, IEnumerable<ManifestReference> collections)
    {
      Courier = courier;
      Manifests = collections;
    }

    public string ContractorName
    {
      get
      {
        string s = Courier.CompanyName;
        if (string.IsNullOrWhiteSpace(s)) return "None";
        return s;
      }
    }




    #region Contractor Courier

    public const string CourierProperty = "Courier";
    Contractor mCourier;
    public Contractor Courier
    {
      get { return mCourier; }
      private set { mCourier = value; }
    }

    #endregion



    #region IEnumerable<ManifestReference> Manifests

    public const string ManifestsProperty = "Manifests";
    IEnumerable<ManifestReference> mManifests;
    public IEnumerable<ManifestReference> Manifests
    {
      get { return mManifests; }
      //private set
      //{
      //  mManifests = new BasicCollection<ManifestReference>(value);
      //}
      private set
      {
        mManifests = value;

        try
        {
          Collection<RouteContainer> routes = new Collection<RouteContainer>();
          var varResults = (BusinessObject.IsNull(Courier) ? mManifests.Where(mw => mw.Courier == null).Select(c => c.Route).Distinct().OrderBy(r => r.Name) : mManifests.Where(mw => mw.Courier.Equals(Courier)).Select(c => c.Route).Distinct().OrderBy(r => r.Name));
          if (varResults != null && varResults.DefaultIfEmpty() != null)
          {
            foreach (var route in varResults)
            {
              if (!BusinessObject.IsNull(route))
              {
                routes.Add(new RouteContainer(route, mManifests.Where(mw => mw.Route.Equals(route))));
              }
              else
              {
                routes.Add(new RouteContainer(route, mManifests));
              }
            }
          }
          Routes = routes;
        } 
        catch
        {
          throw;
        }
      }
    }

    #endregion

    #region IEnumerable<RouteContainer> Routes

    public const string RoutesProperty = "Routes";
    IEnumerable<RouteContainer> mRoutes;
    public IEnumerable<RouteContainer> Routes
    {
      get { return mRoutes; }
      set { mRoutes = value; }
    }

    #endregion

    #region IEnumerable<CityContainer> Cities

    public const string CitiesProperty = "Cities";
    IEnumerable<CityContainer> mCities;
    public IEnumerable<CityContainer> Cities
    {
      get { return mCities; }
      set { mCities = value; }
    }

    #endregion



    #region "Totals"

    #region int TotalWaybills

    public int TotalWaybills
    {
      get { return mManifests.Sum(m => m.WaybillCount); }
    }

    #endregion

    #region int TotalWaybillItems

    public int TotalWaybillItems
    {
      get { return mManifests.Sum(m => m.Items); }
    }

    #endregion

    #region decimal TotalWaybillWeights

    public decimal TotalWaybillWeights
    {
      get { return mManifests.Sum(m => m.Weight); }
    }

    #endregion

    #endregion

  }
}

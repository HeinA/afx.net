﻿using Afx.Business;
using FleetManagement.Business;
using FreightManagement.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportingModule.Prism.Activities.OperationsDriverTripReportManagement
{
  public class VehicleContainer : BusinessObject
  {
    public VehicleContainer(VehicleReference vehicle, IEnumerable<ManifestReference> collections)
    {
      Vehicle = vehicle;
      Manifests = collections;
    }

    #region VehicleReference Vehicle

    public const string VehicleProperty = "Vehicle";
    VehicleReference mVehicle;
    public VehicleReference Vehicle
    {
      get { return mVehicle; }
      set { mVehicle = value; }
    }

    #endregion

    #region IEnumerable<DriverContainer> Drivers

    public const string DriversProperty = "Drivers";
    IEnumerable<DriverContainer> mDrivers;
    public IEnumerable<DriverContainer> Drivers
    {
      get { return mDrivers; }
      private set { mDrivers = value; }
    }

    #endregion

    #region IEnumerable<ManifestReference> Manifests

    public const string ManifestsProperty = "Manifests";
    IEnumerable<ManifestReference> mManifests;
    public IEnumerable<ManifestReference> Manifests
    {
      get { return mManifests; }
      set { mManifests = value; }
    }

    #endregion
  }
}

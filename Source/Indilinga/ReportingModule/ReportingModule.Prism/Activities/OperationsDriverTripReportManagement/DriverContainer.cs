﻿using Afx.Business;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Documents;
using Afx.Prism.RibbonMdi;
using Afx.Prism.RibbonMdi.Events;
using ContactManagement.Business;
using FleetManagement.Business;
using FreightManagement.Business;
using Microsoft.Practices.Prism.Commands;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportingModule.Prism.Activities.OperationsDriverTripReportManagement
{
  public class DriverContainer : BusinessObject
  {
    public DriverContainer(Contact driver, IEnumerable<ManifestReference> collections)
    {
      Driver = driver;
      if (collections.DefaultIfEmpty() == null)
      {
        Manifests = new BasicCollection<ManifestReference>();
      }
      else
      {
        Manifests = collections;
      }
    }

    #region Employee Driver

    public const string DriverProperty = "Driver";
    Contact mDriver;
    public Contact Driver
    {
      get { return mDriver; }
      set { mDriver = value; }
    }

    #endregion

    public string Fullname
    {
      get { return (Driver == null ? "Blank" : Driver.Fullname); }
    }

    //#region IEnumerable<VehicleReference> Vehicles

    //public const string VehiclesProperty = "Vehicles";
    //IEnumerable<VehicleReference> mVehicles;
    //public IEnumerable<VehicleReference> Vehicles
    //{
    //  get { return mVehicles; }
    //  set { mVehicles = value; }
    //}

    //#endregion

    #region IEnumerable<ManifestReference> Manifests

    public const string ManifestsProperty = "Manifests";
    IEnumerable<ManifestReference> mManifests;
    public IEnumerable<ManifestReference> Manifests
    {
      get { return mManifests; }
      //private set
      //{
      //  mManifests = value;
      //}
      private set
      {
        mManifests = value;

        try
        {
          Collection<RouteContainer> routes = new Collection<RouteContainer>();
          //routes.Add(new RouteContainer(new Route(true), mManifests.Where(mw => BusinessObject.IsNull(mw.Route) && mw.Driver.Equals(Driver))));

          var varResults = (BusinessObject.IsNull(Driver) ? mManifests.Where(mw => mw.Driver == null).Select(c => c.Route).Distinct().OrderBy(r => r.Name) : mManifests.Where(mw => mw.Driver.Equals(Driver)).Select(c => c.Route).Distinct().OrderBy(r => r.Name));
          if (varResults != null && varResults.DefaultIfEmpty() != null)
          {
            foreach (var route in varResults)
            //foreach (var route in mManifests.Where(mw => !BusinessObject.IsNull(mw.Driver) && mw.Driver.Equals(Driver)).Select(ms => ms.Route).Distinct().OrderBy(mob => mob.Name))
            {
              if (!BusinessObject.IsNull(route))
              {
                routes.Add(new RouteContainer(route, mManifests.Where(mw => mw.Route.Equals(route))));
              }
              else
              {
                routes.Add(new RouteContainer(route, mManifests));
              }
            }
          }
          Routes = routes;
        } 
        catch
        {
          throw;
        }
      }
    }

    #endregion

    #region IEnumerable<RouteContainer> Routes

    public const string RoutesProperty = "Routes";
    IEnumerable<RouteContainer> mRoutes;
    public IEnumerable<RouteContainer> Routes
    {
      get { return mRoutes; }
      set { mRoutes = value; }
    }

    #endregion

    #region IEnumerable<CityContainer> Cities

    public const string CitiesProperty = "Cities";
    IEnumerable<CityContainer> mCities;
    public IEnumerable<CityContainer> Cities
    {
      get { return mCities; }
      set { mCities = value; }
    }

    #endregion



    #region "Totals"

    #region int TotalWaybills

    public int TotalWaybills
    {
      get { return mManifests.Sum(m => m.WaybillCount); }
    }

    #endregion

    #region int TotalWaybillItems

    public int TotalWaybillItems
    {
      get { return mManifests.Sum(m => m.Items); }
    }

    #endregion

    #region decimal TotalWaybillWeights

    public decimal TotalWaybillWeights
    {
      get { return mManifests.Sum(m => m.Weight); }
    }

    #endregion

    #endregion
  }
}

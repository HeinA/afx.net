﻿//using Afx.Business.Activities;
//using Afx.Business.Data;
//using Afx.Prism.RibbonMdi;
//using Afx.Prism.RibbonMdi.Ribbon;
//using System;
//using System.Collections.Generic;
//using System.ComponentModel.Composition;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;

//namespace ReportingModule.Prism.Activities.OperationsDriverTripReportManagement
//{
//  [Export(typeof(IRibbonItem))]
//  public class OperationsDriverTripReportManagementButton : RibbonButtonViewModel
//  {
//    public override string TabName
//    {
//      get { return ReportingModuleTab.TabName; }
//    }

//    public override string GroupName
//    {
//      get { return ReportingModule.Prism.OperationsGroup.GroupName; }
//    }

//    public override string ItemIdentifier
//    {
//      get { return "OperationsDriverTripReportManagement"; }
//    }

//    public override int Index
//    {
//      get { return 0; }
//    }

//    #region Activity Activity

//    public Activity Activity
//    {
//      get { return Cache.Instance.GetObject<Activity>(OperationsDriverTripReportManagement.Key); }
//    }

//    #endregion

//    public bool IsEnabled
//    {
//      get
//      {
//        try
//        {
//          if (Activity == null) return false;
//          return Activity.CanView;
//        }
//        catch
//        {
//          return false;
//        }
//      }
//    }

//    protected override void OnExecute()
//    {
//      MdiApplicationController.Current.ExecuteActivity(Activity);
//    }
//  }
//}

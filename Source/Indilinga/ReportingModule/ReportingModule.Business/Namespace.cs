﻿using Afx.Business;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportingModule.Business
{
  [Export(typeof(INamespace))]
  public class Namespace : INamespace
  {
    public const string ReportingModule = "http://indilinga.com/ReportingModule/Business";

    public string GetUri()
    {
      return ReportingModule;
    }
  }
}

﻿using Afx.Business;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Documents;
using Afx.Business.Service;
using FreightManagement.Business;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.WCF;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace ReportingModule.Business.Service
{
  [ServiceBehavior(Namespace = Namespace.ReportingModule, ConcurrencyMode = ConcurrencyMode.Multiple)]
  [ExceptionShielding]
  [Export(typeof(IService))]
  public partial class ReportingModuleService : ServiceBase, IReportingModuleService
  {


    //*****************************************************************************************************************************
    //  MANIFESTS
    //*****************************************************************************************************************************


    public BasicCollection<ManifestReference> LoadManifestsAll()
    {
      try
      {
        Collection<IDataFilter> filters = new Collection<IDataFilter>();
        return GetInstances<ManifestReference>(filters);
      }
      catch
      {
        throw;
      }
    }

    public BasicCollection<ManifestReference> LoadManifestsAny(int ou, int route, int courier, int driver, int documentState, string manifestNumber, DateTime fromDate, DateTime toDate)
    {
      try
      {
        Collection<IDataFilter> filters = new Collection<IDataFilter>();
        if (ou != 0) filters.Add(new DataFilter<ManifestReference>(ManifestReference.OriginDCProperty, FilterType.Equals, ou));
        if (route != 0) filters.Add(new DataFilter<ManifestReference>(ManifestReference.RouteProperty, FilterType.Equals, route));
        if (courier != 0) filters.Add(new DataFilter<ManifestReference>(ManifestReference.CourierProperty, FilterType.Equals, courier));
        if (driver != 0) filters.Add(new DataFilter<ManifestReference>(ManifestReference.DriverProperty, FilterType.Equals, driver));
        if (documentState != 0) filters.Add(new DataFilter<ManifestReference>(ManifestReference.ManifestStateProperty, FilterType.Equals, documentState));
        filters.Add(new DataFilter<ManifestReference>(ManifestReference.ManifestDateProperty, FilterType.GreaterThanEqual, fromDate));
        filters.Add(new DataFilter<ManifestReference>(ManifestReference.ManifestDateProperty, FilterType.LessThanEqual, toDate));
        return GetInstances<ManifestReference>(filters);
      }
      catch
      {
        throw;
      }
    }







    //*****************************************************************************************************************************
    //  WAYBILLS
    //*****************************************************************************************************************************


    public BasicCollection<WaybillReference> LoadWaybillsAny(int ou, int route, int account, int documentState, bool podCaptured, string waybillNumber, DateTime fromDate, DateTime toDate)
    {
      try
      {
        Collection<IDataFilter> filters = new Collection<IDataFilter>();
        if (ou != 0) filters.Add(new DataFilter<WaybillReference>(WaybillReference.DistributionCenterProperty, FilterType.Equals, ou));
        if (route != 0) filters.Add(new DataFilter<WaybillReference>(WaybillReference.RouteProperty, FilterType.Equals, route));
        if (account != 0) filters.Add(new DataFilter<WaybillReference>(WaybillReference.AccountProperty, FilterType.Equals, account));
        if (documentState != 0) filters.Add(new DataFilter<WaybillReference>(WaybillReference.WaybillStateProperty, FilterType.Equals, documentState));
        filters.Add(new DataFilter<WaybillReference>(WaybillReference.WaybillDateProperty, FilterType.GreaterThanEqual, fromDate));
        filters.Add(new DataFilter<WaybillReference>(WaybillReference.WaybillDateProperty, FilterType.LessThanEqual, toDate));
        return GetInstances<WaybillReference>(filters);
      }
      catch
      {
        throw;
      }
    }




    //*****************************************************************************************************************************
    //  WAYBILLS - OUTSTANDING PODS
    //*****************************************************************************************************************************


    public BasicCollection<WaybillReference> LoadWaybillsOutstandingPODs(int ou, int route, int account, string waybillNumber, DateTime fromDate, DateTime toDate)
    {
      try
      {
        Collection<IDataFilter> filters = new Collection<IDataFilter>();
        if (ou != 0) filters.Add(new DataFilter<WaybillReference>(WaybillReference.DistributionCenterProperty, FilterType.Equals, ou));
        if (route != 0) filters.Add(new DataFilter<WaybillReference>(WaybillReference.RouteProperty, FilterType.Equals, route));
        if (account != 0) filters.Add(new DataFilter<WaybillReference>(WaybillReference.AccountProperty, FilterType.Equals, account));
        filters.Add(new DataFilter<WaybillReference>(WaybillReference.WaybillStateProperty, FilterType.Equals, Cache.Instance.GetObject<DocumentTypeState>(Guid.Parse(Waybill.States.InTransit)).Id));
        filters.Add(new DataFilter<WaybillReference>(WaybillReference.WaybillDateProperty, FilterType.GreaterThanEqual, fromDate));
        filters.Add(new DataFilter<WaybillReference>(WaybillReference.WaybillDateProperty, FilterType.LessThanEqual, toDate));
        BasicCollection<WaybillReference> results1 = GetInstances<WaybillReference>(filters);


        filters = new Collection<IDataFilter>();


        if (ou != 0) filters.Add(new DataFilter<WaybillReference>(WaybillReference.DistributionCenterProperty, FilterType.Equals, ou));
        if (route != 0) filters.Add(new DataFilter<WaybillReference>(WaybillReference.RouteProperty, FilterType.Equals, route));
        if (account != 0) filters.Add(new DataFilter<WaybillReference>(WaybillReference.AccountProperty, FilterType.Equals, account));
        filters.Add(new DataFilter<WaybillReference>(WaybillReference.WaybillStateProperty, FilterType.Equals, Cache.Instance.GetObject<DocumentTypeState>(Guid.Parse(Waybill.States.Delivered)).Id));
        filters.Add(new DataFilter<WaybillReference>(WaybillReference.WaybillDateProperty, FilterType.GreaterThanEqual, fromDate));
        filters.Add(new DataFilter<WaybillReference>(WaybillReference.WaybillDateProperty, FilterType.LessThanEqual, toDate));
        BasicCollection<WaybillReference> results2 = GetInstances<WaybillReference>(filters);
        
        filters = new Collection<IDataFilter>();


        if (ou != 0) filters.Add(new DataFilter<WaybillReference>(WaybillReference.DistributionCenterProperty, FilterType.Equals, ou));
        if (route != 0) filters.Add(new DataFilter<WaybillReference>(WaybillReference.RouteProperty, FilterType.Equals, route));
        if (account != 0) filters.Add(new DataFilter<WaybillReference>(WaybillReference.AccountProperty, FilterType.Equals, account));
        filters.Add(new DataFilter<WaybillReference>(WaybillReference.WaybillStateProperty, FilterType.Equals, Cache.Instance.GetObject<DocumentTypeState>(Guid.Parse(Waybill.States.OnDelivery)).Id));
        filters.Add(new DataFilter<WaybillReference>(WaybillReference.WaybillDateProperty, FilterType.GreaterThanEqual, fromDate));
        filters.Add(new DataFilter<WaybillReference>(WaybillReference.WaybillDateProperty, FilterType.LessThanEqual, toDate));
        BasicCollection<WaybillReference> results3 = GetInstances<WaybillReference>(filters);
        
        return new BasicCollection<WaybillReference>(results1.Union(results2.Union(results3)));
      }
      catch
      {
        throw;
      }
    }
  }
}

﻿using Afx.Business.Collections;
using FreightManagement.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace ReportingModule.Business.Service
{
  [ServiceContract(Namespace = Namespace.ReportingModule)]
  public partial interface IReportingModuleService : IDisposable
  {
    [OperationContract]
    BasicCollection<ManifestReference> LoadManifestsAll();

    [OperationContract]
    BasicCollection<ManifestReference> LoadManifestsAny(int ou, int route, int courier, int driver, int documentState, string manifestNumber, DateTime fromDate, DateTime toDate);

    [OperationContract]
    BasicCollection<WaybillReference> LoadWaybillsAny(int ou, int route, int account, int documentState, bool podCaptured, string waybillNumber, DateTime fromDate, DateTime toDate);
    
    [OperationContract]
    BasicCollection<WaybillReference> LoadWaybillsOutstandingPODs(int ou, int route, int account, string waybillNumber, DateTime fromDate, DateTime toDate);
  }
}

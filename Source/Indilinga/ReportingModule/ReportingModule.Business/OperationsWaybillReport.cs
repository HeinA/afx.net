﻿using Afx.Business;
using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Validation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace ReportingModule.Business
{
  [PersistantObject(Schema = "Reporting", TableName = "vOperations_Waybills")]
  public partial class OperationsWaybillReport : BusinessObject 
  {
    #region Constructors

    public OperationsWaybillReport()
    {
    }

    #endregion

    #region string DocumentNumber

    public const string DocumentNumberProperty = "DocumentNumber";
    [DataMember(Name = DocumentNumberProperty, EmitDefaultValue = false)]
    string mDocumentNumber;
    [PersistantProperty]
    public string DocumentNumber
    {
      get { return mDocumentNumber; }
      set { SetProperty<string>(ref mDocumentNumber, value); }
    }

    #endregion

  }
}

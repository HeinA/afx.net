﻿using Afx.Business;
using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Validation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace ReportingModule.Business
{
  [PersistantObject(Schema = "Reporting", TableName = "vOperations_Manifest")]
  public partial class OperationsManifestReport : BusinessObject 
  {
    #region Constructors

    public OperationsManifestReport()
    {
    }

    #endregion

    
    #region string Text

    public const string TextProperty = "Text";
    [DataMember(Name = TextProperty, EmitDefaultValue = false)]
    string mText;
    [PersistantProperty]
    [Mandatory("Text is mandatory.")]
    public string Text
    {
      get { return mText; }
      set { SetProperty<string>(ref mText, value); }
    }

    #endregion

  }
}

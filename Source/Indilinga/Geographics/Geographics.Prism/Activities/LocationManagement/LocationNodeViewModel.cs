﻿using Afx.Business;
using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using Geographics.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace Geographics.Prism.Activities.LocationManagement
{
  public partial class LocationNodeViewModel : MdiNavigationTreeNodeViewModel<Location>
  {
    #region Constructors

    [InjectionConstructor]
    public LocationNodeViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    #region string Name

    public string Name
    {
      get { return Model == null ? GetDefaultValue<string>() : Model.Name; }
      set { Model.Name = value; }
    }

    #endregion

    public IEnumerable<LocationNodeViewModel> SubLocations
    {
      get { return this.ResolveViewModels<LocationNodeViewModel, Location>(Model.SubLocations); }
    }

    public ICollectionView SubLocationsSorted
    {
      get
      {
        ICollectionView cvs = CollectionViewSource.GetDefaultView(SubLocations); // new CollectionViewSource();
        //cvs.Source = SubLocations;
        cvs.SortDescriptions.Add(new System.ComponentModel.SortDescription("Model.Name", System.ComponentModel.ListSortDirection.Ascending));
        return cvs;
      }
    }
  }
}

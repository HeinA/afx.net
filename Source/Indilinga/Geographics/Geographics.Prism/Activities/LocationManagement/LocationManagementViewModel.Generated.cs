﻿using Afx.Business.Security;
using Afx.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Afx.Prism.RibbonMdi.Activities;
using Geographics.Business;
using Microsoft.Practices.Prism;

namespace Geographics.Prism.Activities.LocationManagement
{
  //**************************************************************************************************************
  //**************************************************************************************************************
  //*                                                                                                            *
  //*     This code is auto-generated.  Do not place any cutom code here, it will be lost on re-generation.      *
  //*                                                                                                            *
  //**************************************************************************************************************
  //**************************************************************************************************************
  public partial class LocationManagementViewModel
  {
    public IEnumerable<LocationNodeViewModel> Items
    {
      get
      {
        if (Model == null) return null;
        return this.ResolveViewModels<LocationNodeViewModel, Location>(Model.Data);
      }
    }
  }
}

﻿using Afx.Business;
using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Activities;
using Geographics.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Geographics.Prism.Activities.LocationManagement
{
  public partial class LocationDetailViewModel : ActivityDetailViewModel<Location>
  {
    #region Constructors

    [InjectionConstructor]
    public LocationDetailViewModel(IController controller)
      : base(controller)
    {
    }
    
    #endregion

    #region string Name

    public string Name
    {
      get { return Model == null ? GetDefaultValue<string>() : Model.Name; }
      set { Model.Name = value; }
    }

    #endregion
  }
}

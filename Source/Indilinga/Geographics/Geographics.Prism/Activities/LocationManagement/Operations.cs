﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Geographics.Prism.Activities.LocationManagement
{
  //**************************************************************************************************************
  //**************************************************************************************************************
  //*                                                                                                            *
  //*     This code is auto-generated.  Do not place any cutom code here, it will be lost on re-generation.      *
  //*                                                                                                            *
  //**************************************************************************************************************
  //**************************************************************************************************************
  public class Operations : Afx.Prism.RibbonMdi.StandardOperations
  {
      public const string AddRootLocation = "{a3c2389c-4a20-4e63-8403-8e10a0903187}";
      public const string AddSubLocation = "{71514fa6-6201-49a3-bac1-795e59760384}";
  }
}

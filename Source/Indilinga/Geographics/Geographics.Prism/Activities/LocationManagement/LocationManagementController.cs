﻿using Afx.Business;
using Afx.Business.Activities;
using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Activities;
using Geographics.Business;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Geographics.Prism.Activities.LocationManagement
{
  public partial class LocationManagementController : MdiContextualActivityController<LocationManagement, LocationManagementViewModel>
  {
    #region Constructors

    [InjectionConstructor]
    public LocationManagementController(IController controller)
      : base(controller)
    {
    }

    #endregion

    #region OnLoaded
    
    protected override void OnLoaded()
    {
      if (DataContext.Data.Count > 0)
      {
        //Focus the first element 
        LocationNodeViewModel ivm = GetCreateViewModel<LocationNodeViewModel>(DataContext.Data[0], ViewModel);
        ivm.IsFocused = true;
      }

      RegisterContextRegion(RegionManager.Regions[LocationManagement.DetailsRegion]);
      RegisterContextRegion(RegionManager.Regions[LocationManagement.TabRegion]);

      ContextualActivity ca = this.DataContext.Activity;

      base.OnLoaded();
    }

    #endregion
    
    #region OnContextViewModelChanged
    
    protected override void OnContextViewModelChanged(ISelectableViewModel context)
    {
      if (context.Model is Location)
      {
        try
        {
          LocationDetailViewModel vm = GetCreateViewModel<LocationDetailViewModel>(context.Model, ViewModel);
          AddDetailsViewModel(vm);
        }
        catch
        {
          throw;
        }
      }

      base.OnContextViewModelChanged(context);
    }

    #endregion

    public override bool IsOperationVisible(Operation op)
    {
      return base.IsOperationVisible(op);
    }

    public override string GetOperationText(Operation op)
    {
      LocationType lt = op.Tag as LocationType;
      if (lt != null)
      {
        return string.Format("Add {0}", lt.Text);
      }
      return base.GetOperationText(op);
    }

    public override Operation[] ReplaceOperation(Operation op)
    {
      if (op.Identifier == Operations.AddSubLocation && ContextViewModel != null)
      {
        Collection<Operation> ops = new Collection<Operation>();
        Location l = ContextViewModel.Model as Location;
        foreach (var st in l.LocationType.SubTypes.OrderBy((st) => st.Text))
        {
          Operation rop = op.CreateReplicate();
          rop.Tag = st;
          ops.Add(rop);
        }
        return ops.ToArray();
      }
      return base.ReplaceOperation(op);
    }

    #region ExecuteOperation
    
    protected override void ExecuteOperation(Operation op, BusinessObject argument)
    {
      switch (op.Identifier)
      {
        case Operations.AddRootLocation:
          {
            Location o = new Location();
            o.LocationType = Cache.Instance.GetObjects<LocationType>((lt) => BusinessObject.IsNull(lt.Owner)).FirstOrDefault();
            DataContext.Data.Add(o);
            SelectContextViewModel(o);
            FocusViewModel<LocationDetailViewModel>(o);
          }
          break;

        case Operations.AddSubLocation:
          {
            Location l = (Location)argument;
            Location newL = new Location();
            newL.LocationType = (LocationType)op.Tag;
            l.SubLocations.Add(newL);
            SelectContextViewModel(newL);
            FocusViewModel<LocationDetailViewModel>(newL);
          }
          break;
      }
      base.ExecuteOperation(op, argument);
    }
    
    #endregion
  }
}

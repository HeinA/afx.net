﻿using Afx.Business.Security;
using Afx.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Activities;
using Geographics.Business;

namespace Geographics.Prism.Activities.LocationManagement
{
  public partial class LocationManagementViewModel : MdiContextualActivityViewModel<LocationManagement>
  {
    #region Constructors

    [InjectionConstructor]
    public LocationManagementViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion
  }
}

﻿using Afx.Business.Activities;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Business.Service;
using Geographics.Business;
using Geographics.Business.Service;
using Afx.Prism.RibbonMdi.Activities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.Composition;
  
namespace Geographics.Prism.Activities.LocationManagement
{
  //**************************************************************************************************************
  //**************************************************************************************************************
  //*                                                                                                            *
  //*     This code is auto-generated.  Do not place any cutom code here, it will be lost on re-generation.      *
  //*                                                                                                            *
  //**************************************************************************************************************
  //**************************************************************************************************************
  [Export("Context:" + LocationManagement.Key)]
  public partial class LocationManagement
  {
    public const string Key = "{a477d12c-d6e6-4638-9a93-b85cc15e553b}";

    public override void LoadData()
    {
      using (var svc = ProxyFactory.GetService<IGeographicsService>(SecurityContext.MasterServer.Name))
      {
        Data = svc.LoadLocationCollection();
      }
      base.LoadData();
    }

    public override void SaveData()
    {
      using (var svc = ProxyFactory.GetService<IGeographicsService>(SecurityContext.MasterServer.Name))
      {
        Data = svc.SaveLocationCollection(Data);
      }
      base.SaveData();
    }
  }
}

﻿using Afx.Business.Activities;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Business.Service;
using Geographics.Business;
using Geographics.Business.Service;
using Afx.Prism.RibbonMdi.Activities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Geographics.Prism.Activities.LocationTypeManagement
{
  public partial class LocationTypeManagement : ContextualActivityContext<LocationType>
  {
    public const string DetailsRegion = "DetailsRegion";
    public const string TabRegion = "TabRegion";

    public LocationTypeManagement()
    {
    }

    public LocationTypeManagement(ContextualActivity activity)
      : base(activity)
    {
    }
  }
}

﻿using Afx.Business;
using Afx.Business.Activities;
using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Activities;
using Geographics.Business;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Geographics.Prism.Activities.LocationTypeManagement
{
  public partial class LocationTypeManagementController : MdiContextualActivityController<LocationTypeManagement, LocationTypeManagementViewModel>
  {
    #region Constructors

    [InjectionConstructor]
    public LocationTypeManagementController(IController controller)
      : base(controller)
    {
    }

    #endregion

    #region OnLoaded
    
    protected override void OnLoaded()
    {
      if (DataContext.Data.Count > 0)
      {
        //Focus the first element 
        LocationTypeNodeViewModel ivm = GetCreateViewModel<LocationTypeNodeViewModel>(DataContext.Data[0], ViewModel);
        ivm.IsFocused = true;
      }

      RegisterContextRegion(RegionManager.Regions[LocationTypeManagement.DetailsRegion]);
      RegisterContextRegion(RegionManager.Regions[LocationTypeManagement.TabRegion]);

      base.OnLoaded();
    }

    #endregion
    
    #region OnContextViewModelChanged
    
    protected override void OnContextViewModelChanged(ISelectableViewModel context)
    {
      if (context.Model is LocationType)
      {
        LocationTypeDetailViewModel vm = GetCreateViewModel<LocationTypeDetailViewModel>(context.Model, ViewModel);
        AddDetailsViewModel(vm);
      }

      base.OnContextViewModelChanged(context);
    }

    #endregion

    public override bool IsOperationVisible(Operation op)
    {
      if (op.Identifier == Operations.AddRootLocationType && DataContext.Data.Count > 0) return false;
      return base.IsOperationVisible(op);
    }

    #region ExecuteOperation
    
    protected override void ExecuteOperation(Operation op, BusinessObject argument)
    {
      switch (op.Identifier)
      {
        case Operations.AddRootLocationType:
          CreateRootItem();
          break;


        case Operations.AddSubLocationType:
          {
            LocationType lt = (LocationType)argument;
            LocationType newLt = new LocationType();
            lt.SubTypes.Add(newLt);
            SelectContextViewModel(newLt);
            FocusViewModel<LocationTypeDetailViewModel>(newLt);
          }
          break;
      }

      base.ExecuteOperation(op, argument);
    }
    
    #endregion
  }
}

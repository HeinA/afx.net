﻿using Afx.Business;
using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Activities;
using Geographics.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Geographics.Prism.Activities.LocationTypeManagement
{
  public partial class LocationTypeDetailViewModel : ActivityDetailViewModel<LocationType>
  {
    #region Constructors

    [InjectionConstructor]
    public LocationTypeDetailViewModel(IController controller)
      : base(controller)
    {
    }
    
    #endregion

    #region string Text

    public string Text
    {
      get { return Model == null ? GetDefaultValue<string>() : Model.Text; }
      set { Model.Text = value; }
    }

    #endregion

    #region SystemLocationType SystemType

    public SystemLocationType SystemType
    {
      get { return Model == null ? GetDefaultValue<SystemLocationType>() : Model.SystemType; }
      set { Model.SystemType = value; }
    }

    #endregion

  }
}

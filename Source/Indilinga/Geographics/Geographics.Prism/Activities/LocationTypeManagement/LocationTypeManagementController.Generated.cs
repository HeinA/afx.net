﻿using Geographics.Business;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.Composition;
using Microsoft.Practices.Prism;

namespace Geographics.Prism.Activities.LocationTypeManagement
{
  //**************************************************************************************************************
  //**************************************************************************************************************
  //*                                                                                                            *
  //*     This code is auto-generated.  Do not place any cutom code here, it will be lost on re-generation.      *
  //*                                                                                                            *
  //**************************************************************************************************************
  //**************************************************************************************************************
  [Export("Controller:" + global::Geographics.Prism.Activities.LocationTypeManagement.LocationTypeManagement.Key)]
  public partial class LocationTypeManagementController
  {
    void CreateRootItem()
    {
      LocationType o = new LocationType();
      DataContext.Data.Add(o);
      SelectContextViewModel(o);
      FocusViewModel<LocationTypeDetailViewModel>(o);
    }

    void AddDetailsViewModel(object viewModel)
    {
      RegionManager.Regions[LocationTypeManagement.DetailsRegion].Add(viewModel);
    }

    void AddTabViewModel(object viewModel)
    {
      IRegion tabRegion = RegionManager.Regions[LocationTypeManagement.TabRegion];
      tabRegion.Add(viewModel);
      IActiveAware aw = viewModel as IActiveAware;
      if (aw != null && aw.IsActive) tabRegion.Activate(viewModel);
    }
  }
}

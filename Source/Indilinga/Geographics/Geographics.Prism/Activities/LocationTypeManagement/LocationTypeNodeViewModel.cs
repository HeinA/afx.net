﻿using Afx.Business;
using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using Geographics.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Geographics.Prism.Activities.LocationTypeManagement
{
  public partial class LocationTypeNodeViewModel : MdiNavigationTreeNodeViewModel<LocationType>
  {
    #region Constructors

    [InjectionConstructor]
    public LocationTypeNodeViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    #region string Text

    public string Text
    {
      get { return Model == null ? GetDefaultValue<string>() : Model.Text; }
      set { Model.Text = value; }
    }

    #endregion

    public IEnumerable<LocationTypeNodeViewModel> SubTypes
    {
      get { return this.ResolveViewModels<LocationTypeNodeViewModel, LocationType>(Model.SubTypes); }
    }
  }
}

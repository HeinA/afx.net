﻿using Afx.Business.Activities;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Business.Service;
using Geographics.Business;
using Geographics.Business.Service;
using Afx.Prism.RibbonMdi.Activities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.Composition;
  
namespace Geographics.Prism.Activities.LocationTypeManagement
{
  //**************************************************************************************************************
  //**************************************************************************************************************
  //*                                                                                                            *
  //*     This code is auto-generated.  Do not place any cutom code here, it will be lost on re-generation.      *
  //*                                                                                                            *
  //**************************************************************************************************************
  //**************************************************************************************************************
  [Export("Context:" + LocationTypeManagement.Key)]
  public partial class LocationTypeManagement
  {
    public const string Key = "{4733c4da-f900-4db2-87ac-89109164000f}";

    public override void LoadData()
    {
      using (var svc = ProxyFactory.GetService<IGeographicsService>(SecurityContext.MasterServer.Name))
      {
        Data = svc.LoadLocationTypeCollection();
      }
      base.LoadData();
    }

    public override void SaveData()
    {
      using (var svc = ProxyFactory.GetService<IGeographicsService>(SecurityContext.MasterServer.Name))
      {
        Data = svc.SaveLocationTypeCollection(Data);
      }
      base.SaveData();
    }
  }
}

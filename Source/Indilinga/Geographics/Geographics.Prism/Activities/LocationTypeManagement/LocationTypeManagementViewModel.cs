﻿using Afx.Business.Security;
using Afx.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Activities;
using Geographics.Business;

namespace Geographics.Prism.Activities.LocationTypeManagement
{
  public partial class LocationTypeManagementViewModel : MdiContextualActivityViewModel<LocationTypeManagement>
  {
    #region Constructors

    [InjectionConstructor]
    public LocationTypeManagementViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion
  }
}

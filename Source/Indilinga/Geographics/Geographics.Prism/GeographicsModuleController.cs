﻿using Afx.Business;
using Afx.Business.Activities;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Documents;
using Afx.Business.Security;
using Afx.Business.Service;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using Afx.Prism.RibbonMdi.Documents;
using Afx.Prism.RibbonMdi.Events;
using Geographics.Business;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Geographics.Prism
{
  public class GeographicsModuleController : MdiModuleController
  {
    [InjectionConstructor]
    public GeographicsModuleController(IController controller)
      : base("Geographics Module Controller", controller)
    {
    }

    protected override void OnUserAuthenticated(EventArgs obj)
    {
      base.OnUserAuthenticated(obj);
    }
  }
}

﻿using Afx.Business.Data;
using Geographics.Business;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfControls.Editors;

namespace Geographics.Prism
{
  public class LocationProvider : ISuggestionProvider
  {
    IEnumerable<Location> mLocations = null;
    public IEnumerable GetSuggestions(string filter)
    {
      if (mLocations == null) mLocations = Cache.Instance.GetObjects<Location>();
      return mLocations.Where(c => c.Name.ToUpperInvariant().Contains(filter.ToUpperInvariant()));
    }
  }
}

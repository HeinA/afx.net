﻿using Afx.Business;
using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Validation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace Geographics.Business
{
  [PersistantObject(Schema = "Geographics", OwnerColumn = "Parent")]
  [Cache(Priority = 0)]
  public partial class LocationType : BusinessObject<LocationType>, IRevisable 
  {
    #region Constructors

    public LocationType()
    {
    }

    public LocationType(bool isNull)
      : base(isNull)
    {
    }

    #endregion

    #region string Text

    public const string TextProperty = "Text";
    [DataMember(Name = TextProperty, EmitDefaultValue = false)]
    string mText;
    [PersistantProperty]
    public string Text
    {
      get { return mText; }
      set { SetProperty<string>(ref mText, value); }
    }

    #endregion

    #region SystemLocationType SystemType

    public const string SystemTypeProperty = "SystemType";
    [DataMember(Name = SystemTypeProperty, EmitDefaultValue = false)]
    SystemLocationType mSystemType;
    [PersistantProperty]
    [Mandatory("System Location Type is mandatory.")]
    public SystemLocationType SystemType
    {
      get { return mSystemType; }
      set { SetProperty<SystemLocationType>(ref mSystemType, value); }
    }

    #endregion

    #region BusinessObjectCollection<LocationType> SubTypes

    public const string SubTypesProperty = "SubTypes";
    BusinessObjectCollection<LocationType> mSubTypes;
    [PersistantCollection]
    [DataMember]
    public BusinessObjectCollection<LocationType> SubTypes
    {
      get { return mSubTypes ?? (mSubTypes = new BusinessObjectCollection<LocationType>(this)); }
    }

    #endregion

    #region int Revision

    public const string RevisionProperty = "Revision";
    [DataMember(Name = RevisionProperty, EmitDefaultValue = false)]
    int mRevision;
    [PersistantProperty]
    public int Revision
    {
      get { return mRevision; }
      set { SetProperty<int>(ref mRevision, value); }
    }

    #endregion
  }
}

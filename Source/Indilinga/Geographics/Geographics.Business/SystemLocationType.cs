﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Geographics.Business
{
  public enum SystemLocationType
  {
    Invalid
    ,Country
    ,Region
    ,City
    ,Zone
    ,Suburb
    ,Other
  }
}

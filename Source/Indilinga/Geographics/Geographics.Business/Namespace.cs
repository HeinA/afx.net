﻿using Afx.Business;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Geographics.Business
{
  [Export(typeof(INamespace))]
  public class Namespace : INamespace
  {
    public const string Geographics = "http://indilinga.com/Geographics/Business";

    public string GetUri()
    {
      return Geographics;
    }
  }
}

﻿using Afx.Business;
using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace Geographics.Business
{
  [PersistantObject(Schema = "Geographics", OwnerColumn = "Parent")]
  [Cache(Priority = 0)]
  public partial class Location : BusinessObject<Location>, IRevisable 
  {
    #region Constructors

    public Location()
    {
    }

    public Location(bool isNull)
      : base(isNull)
    {
    }

    #endregion

    #region int Revision

    public const string RevisionProperty = "Revision";
    [DataMember(Name = RevisionProperty, EmitDefaultValue = false)]
    int mRevision;
    [PersistantProperty]
    public int Revision
    {
      get { return mRevision; }
      set { SetProperty<int>(ref mRevision, value); }
    }

    #endregion

    #region LocationType LocationType

    public const string LocationTypeProperty = "LocationType";
    [PersistantProperty]
    public LocationType LocationType
    {
      get { return GetCachedObject<LocationType>(); }
      set { SetCachedObject<LocationType>(value); }
    }

    #endregion

    #region string Name

    public const string NameProperty = "Name";
    [DataMember(Name = NameProperty, EmitDefaultValue = false)]
    string mName;
    [PersistantProperty]
    public string Name
    {
      get { return mName; }
      set { SetProperty<string>(ref mName, value); }
    }

    #endregion

    #region BusinessObjectCollection<Location> SubLocations

    public const string SubLocationsProperty = "SubLocations";
    BusinessObjectCollection<Location> mSubLocations;
    [PersistantCollection]
    [DataMember]
    public BusinessObjectCollection<Location> SubLocations
    {
      get { return mSubLocations ?? (mSubLocations = new BusinessObjectCollection<Location>(this)); }
    }

    #endregion

    //#region string ICAOCode

    //public const string ICAOCodeProperty = "ICAOCode";
    //[DataMember(Name = ICAOCodeProperty, EmitDefaultValue = false)]
    //string mICAOCode;
    //[PersistantProperty]
    //public string ICAOCode
    //{
    //  get { return mICAOCode; }
    //  set { SetProperty<string>(ref mICAOCode, value); }
    //}

    //#endregion

    //#region string IATACode

    //public const string IATACodeProperty = "IATACode";
    //[DataMember(Name = IATACodeProperty, EmitDefaultValue = false)]
    //string mIATACode;
    //[PersistantProperty]
    //public string IATACode
    //{
    //  get { return mIATACode; }
    //  set { SetProperty<string>(ref mIATACode, value); }
    //}

    //#endregion

    //#region double Longitude

    //public const string LongitudeProperty = "Longitude";
    //[DataMember(Name = LongitudeProperty, EmitDefaultValue = false)]
    //double mLongitude;
    //[PersistantProperty]
    //public double Longitude
    //{
    //  get { return mLongitude; }
    //  set { SetProperty<double>(ref mLongitude, value); }
    //}

    //#endregion

    //#region double Latitude

    //public const string LatitudeProperty = "Latitude";
    //[DataMember(Name = LatitudeProperty, EmitDefaultValue = false)]
    //double mLatitude;
    //[PersistantProperty]
    //public double Latitude
    //{
    //  get { return mLatitude; }
    //  set { SetProperty<double>(ref mLatitude, value); }
    //}

    //#endregion

    //#region int Altitude

    //public const string AltitudeProperty = "Altitude";
    //[DataMember(Name = AltitudeProperty, EmitDefaultValue = false)]
    //int mAltitude;
    //[PersistantProperty]
    //public int Altitude
    //{
    //  get { return mAltitude; }
    //  set { SetProperty<int>(ref mAltitude, value); }
    //}

    //#endregion

    public Location Country
    {
      get
      {
        if (IsDeserializing) return null;
        if (BusinessObject.IsNull(this.LocationType)) return null;
        if (this.LocationType.SystemType == SystemLocationType.Country) return this;
        if (BusinessObject.IsNull(Owner)) return null;
        return Owner.Country;
      }
    }

    public Location City
    {
      get
      {
        if (IsDeserializing) return null;
        if (BusinessObject.IsNull(this.LocationType)) return null;
        if (this.LocationType.SystemType == SystemLocationType.City) return this;
        if (BusinessObject.IsNull(Owner)) return null;
        return Owner.City;
      }
    }

    public Location Suburb
    {
      get
      {
        if (IsDeserializing) return null;
        if (BusinessObject.IsNull(this.LocationType)) return null;
        if (this.LocationType.SystemType == SystemLocationType.Suburb) return this;
        if (BusinessObject.IsNull(Owner)) return null;
        return Owner.Suburb;
      }
    }

    public string FullName
    {
      get
      {
        if (!BusinessObject.IsNull(Owner)) return string.Concat(Name, ", ", Owner.FullName);
        return Name;
      }
    }

    public string FullNameWithoutZone
    {
      get
      {
        if (!BusinessObject.IsNull(Owner))
        {
          if (this.LocationType.SystemType == SystemLocationType.Zone) return Owner.FullNameWithoutZone;
          return string.Concat(Name, ", ", Owner.FullNameWithoutZone);
        }

        if (this.LocationType.SystemType == SystemLocationType.Zone) return string.Empty;
        return Name;
      }
    }

    private bool IsEqual(string s)
    {
      if (s.ToUpperInvariant() == this.Name.ToUpperInvariant()) return true;
      //Check aliases.
      return false;
    }

    public bool IsSubLocationOf(Location parent)
    {
      if (BusinessObject.IsNull(Owner)) return false;
      if (Owner.Equals(parent)) return true;
      return Owner.IsSubLocationOf(parent);
    }

    public static Location GetLocation(string address)
    {
      if (address == null) return null;

      address = address.Replace('\r', ',');
      address = address.Replace('\n', ',');
      List<string> addressLines = address.ToUpperInvariant().Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(p => p.Trim()).ToList();

      return GetLocation(addressLines, null);
    }

    static Location GetLocation(List<string> addressLines, Location parent)
    {
      IEnumerable<Location> locations = null;
      if (parent == null) locations = Cache.Instance.GetObjects<Location>();
      else locations = Cache.Instance.GetObjects<Location>(l1 => l1.IsSubLocationOf(parent));

      foreach (var l in locations.Where(l1 => l1.LocationType.SystemType == SystemLocationType.Country))
      {
        foreach(var line in addressLines)
        {
          if (l.IsEqual(line))
          {
            Location ret = GetLocation(addressLines, l);
            if (ret == null) ret = l;
            return ret;
          }
        }
      }

      foreach (var l in locations.Where(l1 => l1.LocationType.SystemType == SystemLocationType.Region))
      {
        foreach (var line in addressLines)
        {
          if (l.IsEqual(line))
          {
            Location ret = GetLocation(addressLines, l);
            if (ret == null) ret = l;
            return ret;
          }
        }
      }

      foreach (var l in locations.Where(l1 => l1.LocationType.SystemType == SystemLocationType.City))
      {
        foreach (var line in addressLines)
        {
          if (l.IsEqual(line))
          {
            Location ret = GetLocation(addressLines, l);
            if (ret == null) ret = l;
            return ret;
          }
        }
      }

      foreach (var l in locations.Where(l1 => l1.LocationType.SystemType == SystemLocationType.Suburb))
      {
        foreach (var line in addressLines)
        {
          if (l.IsEqual(line))
          {
            Location ret = GetLocation(addressLines, l);
            if (ret == null) ret = l;
            return ret;
          }
        }
      }

      foreach (var l in locations.Where(l1 => l1.LocationType.SystemType == SystemLocationType.Other))
      {
        foreach (var line in addressLines)
        {
          if (l.IsEqual(line))
          {
            Location ret = GetLocation(addressLines, l);
            if (ret == null) ret = l;
            return ret;
          }
        }
      }

      return null;
    }

    public bool Contains(Location location)
    {
      if (this.Equals(location)) return true;
      foreach (var l in this.SubLocations)
      {
        bool b = l.Contains(location);
        if (b) return true;
      }
      return false;
    }

    public override string ToString()
    {
      return FullName;
    }
  }

}

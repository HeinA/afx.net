﻿using Afx.Business.Collections;
using Afx.Business.Service;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.WCF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Geographics.Business.Service
{
  public partial class GeographicsService
  {
    public BasicCollection<Geographics.Business.Location> LoadLocationCollection()
    {
      try
      {
        return GetAllInstances<Geographics.Business.Location>();
      }
      catch
      {
        throw;
      }
    }

    public BasicCollection<Geographics.Business.Location> SaveLocationCollection(BasicCollection<Geographics.Business.Location> col)
    {
      try
      {
        return Persist<Geographics.Business.Location>(col);
      }
      catch
      {
        throw;
      }
    }
  }
}
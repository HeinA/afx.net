﻿using Afx.Business.Collections;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Geographics.Business.Service
{
  public partial interface IGeographicsService
  {
    [OperationContract]
    BasicCollection<Geographics.Business.Location> LoadLocationCollection();

    [OperationContract]
    BasicCollection<Geographics.Business.Location> SaveLocationCollection(BasicCollection<Geographics.Business.Location> col);
  }
}


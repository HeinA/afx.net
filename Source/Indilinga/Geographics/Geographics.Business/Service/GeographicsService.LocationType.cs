﻿using Afx.Business.Collections;
using Afx.Business.Service;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.WCF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Geographics.Business.Service
{
  public partial class GeographicsService
  {
    public BasicCollection<Geographics.Business.LocationType> LoadLocationTypeCollection()
    {
      try
      {
        return GetAllInstances<Geographics.Business.LocationType>();
      }
      catch
      {
        throw;
      }
    }

    public BasicCollection<Geographics.Business.LocationType> SaveLocationTypeCollection(BasicCollection<Geographics.Business.LocationType> col)
    {
      try
      {
        return Persist<Geographics.Business.LocationType>(col);
      }
      catch
      {
        throw;
      }
    }
  }
}
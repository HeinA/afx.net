﻿using Afx.Business;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeighbridgeIc.Business
{
  [Export(typeof(INamespace))]
  public class Namespace : INamespace
  {
    public const string WeighbridgeIc = "http://indilinga.com/WeighbridgeIc/Business";

    public string GetUri()
    {
      return WeighbridgeIc;
    }
  }
}

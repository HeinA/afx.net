﻿using Afx.Business.Service;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.WCF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace WeighbridgeIc.Business.Service
{
  public partial class WeighbridgeIcService
  {
    public WeighbridgeIc.Business.WeighbridgeTicket LoadWeighbridgeTicket(Guid globalIdentifier)
    {
      try
      {
        return GetInstance<WeighbridgeIc.Business.WeighbridgeTicket>(globalIdentifier);
      }
      catch
      {
        throw;
      }
    }

    public WeighbridgeIc.Business.WeighbridgeTicket SaveWeighbridgeTicket(WeighbridgeIc.Business.WeighbridgeTicket obj)
    {
      try
      {
        return Persist<WeighbridgeIc.Business.WeighbridgeTicket>(obj);
      }
      catch
      {
        throw;
      }
    }
  }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace WeighbridgeIc.Business.Service
{
  public partial interface IWeighbridgeIcService
  {
    [OperationContract]
    WeighbridgeIc.Business.WeighbridgeTicket LoadWeighbridgeTicket(Guid globalIdentifier);

    [OperationContract]
    WeighbridgeIc.Business.WeighbridgeTicket SaveWeighbridgeTicket(WeighbridgeIc.Business.WeighbridgeTicket obj);
  }
}

﻿using Afx.Business;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Documents;
using Afx.Business.Validation;
using InventoryControl.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WeighbridgeIc.Business
{
  [PersistantObject(Schema = "WeighbridgeIc")]
  public partial class WeighbridgeTicket : Document, IInventoryItemCollectionHolder
  {
    public WeighbridgeTicket()
      : base(Cache.Instance.GetObject<DocumentType>(DocumentTypeIdentifier))
    {
    }

    #region Associative BusinessObjectCollection<InventoryItem> Items

    public const string ItemsProperty = "Items";
    AssociativeObjectCollection<WeighbridgeTicketItem, InventoryItem> mItems;
    [PersistantCollection(AssociativeType = typeof(WeighbridgeTicketItem))]
    [Mandatory("Items are mandatory.")]
    public BusinessObjectCollection<InventoryItem> Items
    {
      get
      {
        if (mItems == null) using (new EventStateSuppressor(this)) { mItems = new AssociativeObjectCollection<WeighbridgeTicketItem, InventoryItem>(this); }
        return mItems;
      }
    }

    #endregion

    #region IInventoryItemCollectionHolder

    void IInventoryItemCollectionHolder.CalculateItems()
    {
    }

    IEnumerable<InventoryItem> IInventoryItemCollectionHolder.Items
    {
      get { return Items; }
    }

    void IInventoryItemCollectionHolder.AddItem(InventoryItem item)
    {
      Items.Add(item);
    }

    void IInventoryItemCollectionHolder.RemoveItem(InventoryItem item)
    {
      Items.Remove(item);
    }

    #endregion
  }
}

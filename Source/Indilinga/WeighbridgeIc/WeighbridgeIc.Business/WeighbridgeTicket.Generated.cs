﻿using Afx.Business.Data;
using Afx.Business.Documents;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WeighbridgeIc.Business
{
  [DataContract(IsReference = true, Namespace = Namespace.WeighbridgeIc)]
  public partial class WeighbridgeTicket
  {
    public const string DocumentTypeIdentifier = "{4d494f0a-b3a5-4ace-8dec-3d602aac4260}";
    public class States
    {
      public const string WeighedIn = "{b1ba8d04-081d-44a6-a119-00b3e45e4b3c}";
      public const string WeighedOut = "{6e4e703b-35fd-4133-a34f-40fc86956c4a}";
    }

    protected WeighbridgeTicket(DocumentType documentType)
      : base(documentType)
    {
    }
  }
}

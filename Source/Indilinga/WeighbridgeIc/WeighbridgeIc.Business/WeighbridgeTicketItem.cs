﻿using Afx.Business;
using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using InventoryControl.Business;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace WeighbridgeIc.Business
{
  [PersistantObject(Schema = "WeighbridgeIc")]
  public partial class WeighbridgeTicketItem : AssociativeObject<WeighbridgeTicket, InventoryItem>
  {
  }
}

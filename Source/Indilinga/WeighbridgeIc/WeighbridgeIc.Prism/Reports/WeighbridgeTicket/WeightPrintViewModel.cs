﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Weighbridge.Business;

namespace WeighbridgeIc.Prism.Reports.WeighbridgeTicket
{
  public class WeightPrintViewModel
  {
    public WeightPrintViewModel(WeighbridgeDetailsWeightItem weightItem)
    {
      WeighbridgeDetailsWeightItem = weightItem;
    }

    #region WeighbridgeDetailsWeightItem WeighbridgeDetailsWeightItem
    
    WeighbridgeDetailsWeightItem mWeighbridgeDetailsWeightItem;
    WeighbridgeDetailsWeightItem WeighbridgeDetailsWeightItem
    {
      get { return mWeighbridgeDetailsWeightItem; }
      set { mWeighbridgeDetailsWeightItem = value; }
    }
    
    #endregion

    public Guid WeightIdentifier
    {
      get { return WeighbridgeDetailsWeightItem.Owner.GlobalIdentifier; }
    }

    public string WeightCategory
    {
      get { return WeighbridgeDetailsWeightItem.Owner.WeightCategory.Description; }
    }

    public string WeightItemText
    {
      get { return WeighbridgeDetailsWeightItem.Description; }
    }

    public decimal WeightItemWeight
    {
      get { return WeighbridgeDetailsWeightItem.Weight; }
    }
  }
}

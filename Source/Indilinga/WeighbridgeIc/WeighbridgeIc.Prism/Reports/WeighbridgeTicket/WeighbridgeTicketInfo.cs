﻿using Afx.Business.Documents;
using Afx.Prism.Documents;
using Afx.Prism.RibbonMdi.Documents;
using InventoryControl.Business;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Weighbridge.Business;

namespace WeighbridgeIc.Prism.Reports.WeighbridgeTicket
{
  [Export(WeighbridgeIc.Business.WeighbridgeTicket.DocumentTypeIdentifier, typeof(IDocumentPrintInfo))]
  class WeighbridgeTicketInfo : IDocumentPrintInfo
  {
    public int Priority
    {
      get { return 90; }
    }

    public Stream ReportStream
    {
      get { return this.GetType().Assembly.GetManifestResourceStream("WeighbridgeIc.Prism.Reports.WeighbridgeTicket.WeighbridgeTicket.rdlc"); }
    }

    public void RefreshReportData(LocalReport report, Document document)
    {
      WeighbridgeIc.Business.WeighbridgeTicket wt = (WeighbridgeIc.Business.WeighbridgeTicket)document;

      ReportDataSource reportDataSource = new ReportDataSource();
      reportDataSource.Name = "Document";
      reportDataSource.Value = new object[] { new DocumentPrintViewModel(document) };
      report.DataSources.Add(reportDataSource);

      WeighbridgeDetails wd = document.GetExtensionObject<WeighbridgeDetails>();
      reportDataSource = new ReportDataSource();
      reportDataSource.Name = "WeighbridgeDetails";
      reportDataSource.Value = new object[] { new WeighbridgeDetailsPrintViewModel(wd) };
      report.DataSources.Add(reportDataSource);

      //reportDataSource = new ReportDataSource();
      //reportDataSource.Name = "Vehicles";
      //Collection<VehiclePrintViewModel> vehicles = new Collection<VehiclePrintViewModel>();
      //foreach (WeighbridgeDetailsVehicle v in wd.Vehicles)
      //{
      //  if (!v.IsDeleted) vehicles.Add(new VehiclePrintViewModel(v));
      //}
      //reportDataSource.Value = vehicles;
      //report.DataSources.Add(reportDataSource);

      reportDataSource = new ReportDataSource();
      reportDataSource.Name = "Weights";
      Collection<WeightPrintViewModel> weights = new Collection<WeightPrintViewModel>();
      foreach (WeighbridgeDetailsWeight w in wd.Weights)
      {
        foreach (WeighbridgeDetailsWeightItem wi in w.Items)
        {
          if (!wi.IsDeleted) weights.Add(new WeightPrintViewModel(wi));
        }
      }
      reportDataSource.Value = weights;
      report.DataSources.Add(reportDataSource);

      reportDataSource = new ReportDataSource();
      reportDataSource.Name = "Items";
      Collection<ItemPrintViewModel> items = new Collection<ItemPrintViewModel>();
      foreach (InventoryItem i in wt.Items)
      {
        if (!i.IsDeleted) items.Add(new ItemPrintViewModel(i));
      }
      reportDataSource.Value = items;
      report.DataSources.Add(reportDataSource);
    }

    public short Copies
    {
      get { return 2; }
    }
  }
}

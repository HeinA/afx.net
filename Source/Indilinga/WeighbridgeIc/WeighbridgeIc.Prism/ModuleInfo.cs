﻿using Afx.Prism.RibbonMdi;
using WeighbridgeIc.Business;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Afx.Prism;

namespace WeighbridgeIc.Prism
{
  [Export(typeof(IModuleInfo))]
  public class ModuleInfo : IModuleInfo
  {
    public string ModuleNamespace
    {
      get { return Namespace.WeighbridgeIc; }
    }

    public string[] ModuleDependencies
    {
      get { return new string[] { Afx.Business.Namespace.Afx, Weighbridge.Business.Namespace.Weighbridge, InventoryControl.Business.Namespace.InventoryControl }; }
    }

    public Type ModuleController
    {
      get { return typeof(WeighbridgeIcModuleController); }
    }
  }
}
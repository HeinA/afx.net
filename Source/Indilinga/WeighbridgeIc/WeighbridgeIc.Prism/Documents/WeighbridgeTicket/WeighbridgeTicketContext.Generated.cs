﻿using Afx.Business.Security;
using Afx.Business.Service;
using Afx.Prism.RibbonMdi.Documents;
using WeighbridgeIc.Business;
using WeighbridgeIc.Business.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.Composition;

namespace WeighbridgeIc.Prism.Documents.WeighbridgeTicket
{
  [Export("Context:" + WeighbridgeIc.Business.WeighbridgeTicket.DocumentTypeIdentifier)]
  public partial class WeighbridgeTicketContext
  {
  }
}


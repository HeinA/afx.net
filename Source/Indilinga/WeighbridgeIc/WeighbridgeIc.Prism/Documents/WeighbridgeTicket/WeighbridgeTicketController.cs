﻿using Afx.Business;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Documents;
using WeighbridgeIc.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InventoryControl.Prism.Dialogs.EditInventoryDialog;
using Weighbridge.Prism.Extensions.WeighbridgeDetails;
using Weighbridge.Business;

namespace WeighbridgeIc.Prism.Documents.WeighbridgeTicket
{
  public partial class WeighbridgeTicketController : MdiDocumentController<WeighbridgeTicketContext, WeighbridgeTicketViewModel>
  {
    [InjectionConstructor]
    public WeighbridgeTicketController(IController controller)
      : base(controller)
    {
    }

    protected override void ExtendDocument()
    {
      WeighbridgeDetailsViewModel vm = ExtendDocument<WeighbridgeDetails, WeighbridgeDetailsViewModel>(Afx.Prism.RibbonMdi.Documents.Regions.TopDetailsRegion);
      WeighbridgeFooterViewModel vm1 = ExtendDocument<WeighbridgeDetails, WeighbridgeFooterViewModel>(Afx.Prism.RibbonMdi.Documents.Regions.BottomDetailsRegion);

      base.ExtendDocument();
    }

    protected override void ExecuteOperation(Operation op, BusinessObject argument)
    {
      switch (op.Identifier)
      {
        case Operations.EditInventory:
          {
            EditInventoryDialogController c = CreateChildController<EditInventoryDialogController>();
            c.ItemCollectionHolder = DataContext.Document;
            c.Run();
          }
          break;

        case Operations.WeighOut:
          {
            SetDocumentState(WeighbridgeIc.Business.WeighbridgeTicket.States.WeighedOut);
            Print(doNotSave:true);
            Terminate();
          }
          break;
      }
      base.ExecuteOperation(op, argument);
    }

    protected override void OnSaved(bool userClickedSaved)
    {
      if (userClickedSaved) Terminate();
      base.OnSaved(userClickedSaved);
    }
  }
}


﻿using Afx.Business;
using Afx.Business.Activities;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Documents;
using WeighbridgeIc.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using InventoryControl.Business;

namespace WeighbridgeIc.Prism.Documents.WeighbridgeTicket
{
  public partial class WeighbridgeTicketViewModel : MdiDocumentViewModel<WeighbridgeTicketContext>
  {
    [InjectionConstructor]
    public WeighbridgeTicketViewModel(IController controller)
      : base(controller)
    {
    }

    #region Items

    public IEnumerable<InventoryItem> Items
    {
      get { return Model.Document.Items; }
    }

    #endregion
  }
}


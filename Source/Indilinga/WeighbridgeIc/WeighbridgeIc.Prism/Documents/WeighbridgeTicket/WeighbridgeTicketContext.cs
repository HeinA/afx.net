﻿using Afx.Business.Security;
using Afx.Business.Service;
using Afx.Prism.RibbonMdi.Documents;
using WeighbridgeIc.Business;
using WeighbridgeIc.Business.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeighbridgeIc.Prism.Documents.WeighbridgeTicket
{
  public partial class WeighbridgeTicketContext : DocumentContext<WeighbridgeIc.Business.WeighbridgeTicket>
  {
    public WeighbridgeTicketContext()
      : base(WeighbridgeIc.Business.WeighbridgeTicket.DocumentTypeIdentifier)
    {
    }

    public WeighbridgeTicketContext(Guid documentIdentifier)
      : base(WeighbridgeIc.Business.WeighbridgeTicket.DocumentTypeIdentifier, documentIdentifier)
    {
    }

    public override void SaveData()
    {
      using (var svc = ProxyFactory.GetService<IWeighbridgeIcService>(SecurityContext.Server))
      {
        Document = svc.SaveWeighbridgeTicket(Document);
      }
      base.SaveData();
    }

    public override void LoadData()
    {
      using (var svc = ProxyFactory.GetService<IWeighbridgeIcService>(SecurityContext.Server))
      {
        Document = svc.LoadWeighbridgeTicket(DocumentIdentifier);
      }
      base.LoadData();
    }
  }
}


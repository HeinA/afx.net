﻿using Afx.Business;
using Afx.Business.Activities;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Documents;
using Afx.Business.Security;
using Afx.Business.Service;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using Afx.Prism.RibbonMdi.Documents;
using Afx.Prism.RibbonMdi.Events;
using InventoryControl.Business;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Weighbridge.Business;
using Weighbridge.Prism.Extensions.WeighbridgeDetails;
using Weighbridge.Prism.Extensions.WeighbridgeDetails.AddWeightDialog;
using Weighbridge.Prism.Extensions.WeighbridgeDetails.SetVehicleDialog;

namespace WeighbridgeIc.Prism
{
  public class WeighbridgeIcModuleController : MdiModuleController
  {
    [InjectionConstructor]
    public WeighbridgeIcModuleController(IController controller)
      : base("Weighbridge Inventory Control Module Controller", controller)
    {
    }

    protected override void AfterContainerConfigured()
    {
      this.EventAggregator.GetEvent<ExtendDocumentEvent>().Subscribe(OnExtendDocument);
      ApplicationController.EventAggregator.GetEvent<ExecuteOperationEvent>().Subscribe(OnExecuteOperation);

      base.AfterContainerConfigured();
    }

    private void OnExecuteOperation(ExecuteOperationEventArgs obj)
    {
      switch (obj.Operation.Identifier)
      {
        case Operations.SetVehicle:
          {
            SetVehicleDialogController c = obj.Controller.GetCreateChildController<SetVehicleDialogController>(SetVehicleDialogController.SetVehicleDialogControllerKey);
            c.Run();
          }
          break;

        case Operations.SetDefaultWeight:
          {
            WeighbridgeDetails wd = obj.Controller.DataContext.Document.GetExtensionObject<WeighbridgeDetails>();
            wd.SetVehicleWeight();
            //WeighbridgeDetailsWeight w = wd.Weights.FirstOrDefault(w1 => w1.WeightCategory.IsTare && !w1.IsDeleted);
            //if (w == null)
            //{
            //  w = new WeighbridgeDetailsWeight();
            //  w.WeightCategory = Cache.Instance.GetObjects<WeightCategory>().FirstOrDefault(w1 => w1.IsTare);
            //  wd.Weights.Add(w);
            //}

            //foreach (var w1 in w.Items)
            //{
            //  w1.IsDeleted = true;
            //}
            //foreach (WeighbridgeDetailsVehicle v in wd.Vehicles.Where(v => !v.IsDeleted))
            //{
            //  WeighbridgeDetailsWeightItem wi = new WeighbridgeDetailsWeightItem() { Description = v.Vehicle.Text, Weight = v.VehicleWeight };
            //  w.Items.Add(wi);
            //}
          }
          break;

        case Operations.AddWeight:
          {
            AddWeightDialogController c = obj.Controller.GetCreateChildController<AddWeightDialogController>(AddWeightDialogController.AddWeightDialogControllerKey);
            c.Run();
          }
          break;
      }
    }

    private void OnExtendDocument(ExtendDocumentEventArgs obj)
    {
      if (obj.Document is CustomerDeliveryNote)
      {
        obj.Controller.ExtendDocument<WeighbridgeDetails, WeighbridgeDetailsViewModel>(Regions.TopDetailsRegion);
      }
    }
  }
}

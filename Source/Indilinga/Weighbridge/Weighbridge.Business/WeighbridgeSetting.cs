﻿using Afx.Business;
using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.IO.Ports;
using System.Runtime.Serialization;

namespace Weighbridge.Business
{
  [PersistantObject(Schema = "Weighbridge")]
  [Export(typeof(Setting))]
  public partial class WeighbridgeSetting : MachineSetting 
  {
    #region Constructors

    public WeighbridgeSetting()
    {
    }

    #endregion

    #region string Name

    public override string Name
    {
      get { return "Weighbridge"; }
    }

    #endregion

    #region string SettingGroupIdentifier

    protected override string SettingGroupIdentifier
    {
      get { return "da82bb79-1b21-4186-832e-08a39476c141"; }
    }

    #endregion


    #region bool IsEnabled

    public const string IsEnabledProperty = "IsEnabled";
    [DataMember(Name = IsEnabledProperty, EmitDefaultValue = false)]
    bool mIsEnabled;
    [PersistantProperty]
    public bool IsEnabled
    {
      get { return mIsEnabled; }
      set { SetProperty<bool>(ref mIsEnabled, value); }
    }

    #endregion

    #region string ComPort

    public const string ComPortProperty = "ComPort";
    [DataMember(Name = ComPortProperty, EmitDefaultValue = false)]
    string mComPort;
    [PersistantProperty]
    public string ComPort
    {
      get { return mComPort; }
      set { SetProperty<string>(ref mComPort, value); }
    }

    #endregion

    #region int Baud

    public const string BaudProperty = "Baud";
    [DataMember(Name = BaudProperty, EmitDefaultValue = false)]
    int mBaud;
    [PersistantProperty]
    public int Baud
    {
      get { return mBaud; }
      set { SetProperty<int>(ref mBaud, value); }
    }

    #endregion

    #region Parity Parity

    public const string ParityProperty = "Parity";
    [DataMember(Name = ParityProperty, EmitDefaultValue = false)]
    Parity mParity;
    [PersistantProperty]
    public Parity Parity
    {
      get { return mParity; }
      set { SetProperty<Parity>(ref mParity, value); }
    }

    #endregion

    #region int DataBits

    public const string DataBitsProperty = "DataBits";
    [DataMember(Name = DataBitsProperty, EmitDefaultValue = false)]
    int mDataBits;
    [PersistantProperty]
    public int DataBits
    {
      get { return mDataBits; }
      set { SetProperty<int>(ref mDataBits, value); }
    }

    #endregion

    #region StopBits StopBits

    public const string StopBitsProperty = "StopBits";
    [DataMember(Name = StopBitsProperty, EmitDefaultValue = false)]
    StopBits mStopBits;
    [PersistantProperty]
    public StopBits StopBits
    {
      get { return mStopBits; }
      set { SetProperty<StopBits>(ref mStopBits, value); }
    }

    #endregion
  }
}

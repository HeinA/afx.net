﻿using Afx.Business;
using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Validation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using System.Linq;

namespace Weighbridge.Business
{
  [PersistantObject(Schema = "Weighbridge")]
  public partial class WeighbridgeTicketWeight : BusinessObject<WeighbridgeTicket> 
  {
    #region Constructors

    public WeighbridgeTicketWeight()
    {
    }

    #endregion

    #region WeightCategory WeightCategory

    public const string WeightCategoryProperty = "WeightCategory";
    [PersistantProperty]
    [Mandatory("Weight category is a mandatory field.")]
    public WeightCategory WeightCategory
    {
      get { return GetCachedObject<WeightCategory>(); }
      set { SetCachedObject<WeightCategory>(value); }
    }

    #endregion

    #region decimal Weight

    public const string WeightProperty = "Weight";
    public decimal Weight
    {
      get
      {
        decimal weight = 0;
        foreach (var wi in Items.Where(i => !i.IsDeleted))
        {
          weight += wi.Weight;
        }
        return weight;
      }
    }

    public void RaiseWeightChanged()
    {
      OnPropertyChanged(WeightProperty);
    }

    #endregion

    #region BusinessObjectCollection<WeighbridgeTicketWeightItem> Items

    public const string ItemProperty = "Items";
    BusinessObjectCollection<WeighbridgeTicketWeightItem> mItems;
    [PersistantCollection]
    [DataMember]
    public BusinessObjectCollection<WeighbridgeTicketWeightItem> Items
    {
      get { return mItems ?? (mItems = new BusinessObjectCollection<WeighbridgeTicketWeightItem>(this)); }
    }

    #endregion

    #region void OnCompositionChanged(...)

    protected override void OnCompositionChanged(CompositionChangedType compositionChangedType, string propertyName, object originalSource, BusinessObject item)
    {
      if (propertyName == ItemProperty)
      {
        OnPropertyChanged(WeightProperty);
      }

      base.OnCompositionChanged(compositionChangedType, propertyName, originalSource, item);
    }

    #endregion
  }
}

﻿using Afx.Business;
using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Documents;
using FleetManagement.Business;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using System.Linq;
using Afx.Business.Validation;

namespace Weighbridge.Business
{
  [PersistantObject(Schema = "Weighbridge")]
  public partial class WeighbridgeDetails : ExtensionObject<Document>
  {
    #region Constructors

    public WeighbridgeDetails()
    {
    }

    #endregion

    #region VehicleUnit VehicleUnit

    public const string VehicleUnitProperty = "VehicleUnit";
    [DataMember(Name = VehicleUnitProperty, EmitDefaultValue = false)]
    VehicleUnit mVehicleUnit;
    [PersistantProperty]
    public VehicleUnit VehicleUnit
    {
      get { return mVehicleUnit; }
      set
      {
        if (SetProperty<VehicleUnit>(ref mVehicleUnit, value))
        {
          if (!this.SuppressEventState)
          {
            VehicleReference = VehicleUnit.FleetUnitNumber;

            foreach (var v in Vehicles)
            {
              v.IsDeleted = true;
            }
            foreach (var v in VehicleUnit.Vehicles)
            {
              Vehicles.Add(new WeighbridgeDetailsVehicle(v));
            }
            SetVehicleWeight();
          }
        }
      }
    }

    #endregion

    #region string VehicleReference

    public const string VehicleReferenceProperty = "VehicleReference";
    [DataMember(Name = VehicleReferenceProperty, EmitDefaultValue = false)]
    string mVehicleReference;
    [PersistantProperty]
    [Mandatory("Vehicle Reference is mandatory.")]
    public string VehicleReference
    {
      get { return mVehicleReference; }
      set { SetProperty<string>(ref mVehicleReference, value); }
    }

    #endregion

    #region WeighbridgeDirection Direction

    public const string DirectionProperty = "Direction";
    [DataMember(Name = DirectionProperty, EmitDefaultValue = false)]
    WeighbridgeDirection mDirection;
    [PersistantProperty]
    [Mandatory("Direction is mandatory.")]
    public WeighbridgeDirection Direction
    {
      get { return mDirection; }
      set { SetProperty<WeighbridgeDirection>(ref mDirection, value); }
    }

    #endregion

    #region WeighbridgeIncomingSource IncomingSource

    public const string IncomingSourceProperty = "IncomingSource";
    [PersistantProperty]
    public WeighbridgeIncomingSource IncomingSource
    {
      get { return GetCachedObject<WeighbridgeIncomingSource>(); }
      set { SetCachedObject<WeighbridgeIncomingSource>(value); }
    }

    #endregion

    #region BusinessObjectCollection<WeighbridgeDetailsVehicle> Vehicles

    public const string VehiclesProperty = "Vehicles";
    BusinessObjectCollection<WeighbridgeDetailsVehicle> mVehicles;
    [PersistantCollection]
    [DataMember]
    public BusinessObjectCollection<WeighbridgeDetailsVehicle> Vehicles
    {
      get { return mVehicles ?? (mVehicles = new BusinessObjectCollection<WeighbridgeDetailsVehicle>(this)); }
    }

    #endregion

    #region BusinessObjectCollection<WeighbridgeDetailsWeight> Weights

    public const string WeightsProperty = "Weights";
    BusinessObjectCollection<WeighbridgeDetailsWeight> mWeights;
    [PersistantCollection]
    [DataMember]
    public BusinessObjectCollection<WeighbridgeDetailsWeight> Weights
    {
      get { return mWeights ?? (mWeights = new BusinessObjectCollection<WeighbridgeDetailsWeight>(this)); }
    }

    #endregion

    #region decimal TareWeight

    public decimal TareWeight
    {
      get
      {
        WeighbridgeDetailsWeight w = Weights.FirstOrDefault(w1 => w1.WeightCategory.IsTare && !w1.IsDeleted);
        if (w != null) return w.Weight;
        return 0;
      }
    }

    #endregion

    #region decimal GrossWeight

    public decimal GrossWeight
    {
      get
      {
        WeighbridgeDetailsWeight w = Weights.FirstOrDefault(w1 => w1.WeightCategory.IsFinal && !w1.IsDeleted);
        if (w != null) return w.Weight;
        return 0;
      }
    }

    #endregion

    #region decimal NetWeight

    public decimal NetWeight
    {
      get { return GrossWeight == 0 ? 0 : GrossWeight - TareWeight; }
    }

    #endregion

    #region void SetVehicleWeight()

    public void SetVehicleWeight()
    {
      WeighbridgeDetailsWeight w = Weights.FirstOrDefault(w1 => w1.WeightCategory.IsTare && !w1.IsDeleted);
      if (w == null)
      {
        w = new WeighbridgeDetailsWeight();
        w.WeightCategory = Cache.Instance.GetObjects<WeightCategory>().FirstOrDefault(w1 => w1.IsTare);
        Weights.Add(w);
      }

      foreach (var w1 in w.Items)
      {
        w1.IsDeleted = true;
      }

      decimal total = 0;
      foreach (WeighbridgeDetailsVehicle v in Vehicles.Where(v => !v.IsDeleted))
      {
        total += v.VehicleWeight;
      }
      WeighbridgeDetailsWeightItem wi = new WeighbridgeDetailsWeightItem() { Description = "Vehicle Default Weight", Weight = total };
      w.Items.Add(wi);
    }

    #endregion

    #region void GetErrors(...)

    protected override void GetErrors(Collection<string> errors, string propertyName)
    {
      if (IsPropertyApplicable(propertyName, IncomingSourceProperty))
      {
        if (Direction == WeighbridgeDirection.Incoming && IsNull(IncomingSource)) 
          errors.Add("Incoming source is mandatory.");
      }

      base.GetErrors(errors, propertyName);
    }

    #endregion
  }
}

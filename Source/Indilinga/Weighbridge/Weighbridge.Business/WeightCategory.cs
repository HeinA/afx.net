﻿using Afx.Business;
using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Validation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace Weighbridge.Business
{
  [PersistantObject(Schema = "Weighbridge")]
  [Cache]
  public partial class WeightCategory : BusinessObject, IRevisable 
  {
    #region Constructors

    public WeightCategory()
    {
    }

    public WeightCategory(bool isNull)
      : base(isNull)
    {
    }

    #endregion

    #region int Revision

    public const string RevisionProperty = "Revision";
    [DataMember(Name = RevisionProperty, EmitDefaultValue = false)]
    int mRevision = 1;
    [PersistantProperty]
    public int Revision
    {
      get { return mRevision; }
      set { SetProperty<int>(ref mRevision, value); }
    }

    #endregion

    #region string Description

    public const string DescriptionProperty = "Description";
    [DataMember(Name = DescriptionProperty, EmitDefaultValue = false)]
    string mDescription;
    [PersistantProperty]
    [Mandatory("Description is mandatory.")]
    public string Description
    {
      get { return mDescription; }
      set { SetProperty<string>(ref mDescription, value); }
    }

    #endregion

    #region bool IsTare

    public const string IsTareProperty = "IsTare";
    [DataMember(Name = IsTareProperty, EmitDefaultValue = false)]
    bool mIsTare;
    [PersistantProperty]
    public bool IsTare
    {
      get { return mIsTare; }
      set { SetProperty<bool>(ref mIsTare, value); }
    }

    #endregion

    #region bool IsFinal

    public const string IsFinalProperty = "IsFinal";
    [DataMember(Name = IsFinalProperty, EmitDefaultValue = false)]
    bool mIsFinal;
    [PersistantProperty]
    public bool IsFinal
    {
      get { return mIsFinal; }
      set { SetProperty<bool>(ref mIsFinal, value); }
    }

    #endregion

    #region bool AllowMultiple

    public const string AllowMultipleProperty = "AllowMultiple";
    [DataMember(Name = AllowMultipleProperty, EmitDefaultValue = false)]
    bool mAllowMultiple;
    [PersistantProperty]
    public bool AllowMultiple
    {
      get { return mAllowMultiple; }
      set { SetProperty<bool>(ref mAllowMultiple, value); }
    }

    #endregion
  }
}

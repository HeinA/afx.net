﻿using Afx.Business.Service;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.WCF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Weighbridge.Business.Service
{
  public partial class WeighbridgeService
  {
    public Weighbridge.Business.WeighbridgeTicket LoadWeighbridgeTicket(Guid globalIdentifier)
    {
      try
      {
        return GetInstance<Weighbridge.Business.WeighbridgeTicket>(globalIdentifier);
      }
      catch
      {
        throw;
      }
    }

    public Weighbridge.Business.WeighbridgeTicket SaveWeighbridgeTicket(Weighbridge.Business.WeighbridgeTicket obj)
    {
      try
      {
        return Persist<Weighbridge.Business.WeighbridgeTicket>(obj);
      }
      catch
      {
        throw;
      }
    }
  }
}
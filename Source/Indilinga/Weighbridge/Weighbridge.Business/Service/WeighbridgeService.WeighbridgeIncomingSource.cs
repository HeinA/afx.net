﻿using Afx.Business.Collections;
using Afx.Business.Service;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.WCF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Weighbridge.Business.Service
{
  public partial class WeighbridgeService
  {
    public BasicCollection<Weighbridge.Business.WeighbridgeIncomingSource> LoadWeighbridgeIncomingSourceCollection()
    {
      try
      {
        return GetAllInstances<Weighbridge.Business.WeighbridgeIncomingSource>();
      }
      catch
      {
        throw;
      }
    }

    public BasicCollection<Weighbridge.Business.WeighbridgeIncomingSource> SaveWeighbridgeIncomingSourceCollection(BasicCollection<Weighbridge.Business.WeighbridgeIncomingSource> col)
    {
      try
      {
        return Persist<Weighbridge.Business.WeighbridgeIncomingSource>(col);
      }
      catch
      {
        throw;
      }
    }
  }
}
﻿using Afx.Business.Collections;
using Afx.Business.Service;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.WCF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Weighbridge.Business.Service
{
  public partial class WeighbridgeService
  {
    public BasicCollection<Weighbridge.Business.WeightCategory> LoadWeightCategoryCollection()
    {
      try
      {
        return GetAllInstances<Weighbridge.Business.WeightCategory>();
      }
      catch
      {
        throw;
      }
    }

    public BasicCollection<Weighbridge.Business.WeightCategory> SaveWeightCategoryCollection(BasicCollection<Weighbridge.Business.WeightCategory> col)
    {
      try
      {
        return Persist<Weighbridge.Business.WeightCategory>(col);
      }
      catch
      {
        throw;
      }
    }
  }
}
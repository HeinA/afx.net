﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Weighbridge.Business.Service
{
  public partial interface IWeighbridgeService
  {
    [OperationContract]
    Weighbridge.Business.WeighbridgeTicket LoadWeighbridgeTicket(Guid globalIdentifier);

    [OperationContract]
    Weighbridge.Business.WeighbridgeTicket SaveWeighbridgeTicket(Weighbridge.Business.WeighbridgeTicket obj);
  }
}

﻿using Afx.Business.Collections;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Weighbridge.Business.Service
{
  public partial interface IWeighbridgeService
  {
    [OperationContract]
    BasicCollection<Weighbridge.Business.WeightCategory> LoadWeightCategoryCollection();

    [OperationContract]
    BasicCollection<Weighbridge.Business.WeightCategory> SaveWeightCategoryCollection(BasicCollection<Weighbridge.Business.WeightCategory> col);
  }
}


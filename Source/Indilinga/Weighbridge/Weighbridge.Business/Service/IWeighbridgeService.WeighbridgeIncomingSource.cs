﻿using Afx.Business.Collections;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Weighbridge.Business.Service
{
  public partial interface IWeighbridgeService
  {
    [OperationContract]
    BasicCollection<Weighbridge.Business.WeighbridgeIncomingSource> LoadWeighbridgeIncomingSourceCollection();

    [OperationContract]
    BasicCollection<Weighbridge.Business.WeighbridgeIncomingSource> SaveWeighbridgeIncomingSourceCollection(BasicCollection<Weighbridge.Business.WeighbridgeIncomingSource> col);
  }
}


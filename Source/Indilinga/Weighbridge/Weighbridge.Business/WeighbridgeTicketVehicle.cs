﻿using Afx.Business;
using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using FleetManagement.Business;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace Weighbridge.Business
{
  [PersistantObject(Schema = "Weighbridge")]
  public partial class WeighbridgeTicketVehicle : BusinessObject<WeighbridgeTicket> 
  {
    #region Constructors

    public WeighbridgeTicketVehicle()
    {
    }

    public WeighbridgeTicketVehicle(Vehicle v)
    {
      Vehicle = v;
      FleetNumber = v.FleetNumber;
      RegistrationNumber = v.RegistrationNumber;
      Make = v.Make;
      Model = v.Model;
      VehicleWeight = v.VehicleWeight;
    }

    #endregion

    #region Vehicle Vehicle

    public const string VehicleProperty = "Vehicle";
    [PersistantProperty]
    public Vehicle Vehicle
    {
      get { return GetCachedObject<Vehicle>(); }
      set { SetCachedObject<Vehicle>(value); }
    }

    #endregion

    #region string FleetNumber

    public const string FleetNumberProperty = "FleetNumber";
    [DataMember(Name = FleetNumberProperty, EmitDefaultValue = false)]
    string mFleetNumber;
    [PersistantProperty]
    public string FleetNumber
    {
      get { return mFleetNumber; }
      set { SetProperty<string>(ref mFleetNumber, value); }
    }

    #endregion

    #region string RegistrationNumber

    public const string RegistrationNumberProperty = "RegistrationNumber";
    [DataMember(Name = RegistrationNumberProperty, EmitDefaultValue = false)]
    string mRegistrationNumber;
    [PersistantProperty]
    public string RegistrationNumber
    {
      get { return mRegistrationNumber; }
      set { SetProperty<string>(ref mRegistrationNumber, value); }
    }

    #endregion

    #region string Make

    public const string MakeProperty = "Make";
    [DataMember(Name = MakeProperty, EmitDefaultValue = false)]
    string mMake;
    [PersistantProperty]
    public string Make
    {
      get { return mMake; }
      set { SetProperty<string>(ref mMake, value); }
    }

    #endregion

    #region string Model

    public const string ModelProperty = "Model";
    [DataMember(Name = ModelProperty, EmitDefaultValue = false)]
    string mModel;
    [PersistantProperty]
    public string Model
    {
      get { return mModel; }
      set { SetProperty<string>(ref mModel, value); }
    }

    #endregion

    #region decimal VehicleWeight

    public const string VehicleWeightProperty = "VehicleWeight";
    [DataMember(Name = VehicleWeightProperty, EmitDefaultValue = false)]
    decimal mVehicleWeight;
    [PersistantProperty]
    public decimal VehicleWeight
    {
      get { return mVehicleWeight; }
      set { SetProperty<decimal>(ref mVehicleWeight, value); }
    }

    #endregion
  }
}

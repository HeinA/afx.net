﻿using Afx.Business;
using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace Weighbridge.Business
{
  [PersistantObject(Schema = "Weighbridge")]
  public partial class WeighbridgeTicketWeightItem : BusinessObject<WeighbridgeTicketWeight> 
  {
    #region Constructors

    public WeighbridgeTicketWeightItem()
    {
    }

    public WeighbridgeTicketWeightItem(string description, decimal weight)
    {
      Description = description;
      Weight = weight;
    }

    #endregion

    #region string Description

    public const string DescriptionProperty = "Description";
    [DataMember(Name = DescriptionProperty, EmitDefaultValue = false)]
    string mDescription;
    [PersistantProperty]
    public string Description
    {
      get { return mDescription; }
      set { SetProperty<string>(ref mDescription, value); }
    }

    #endregion

    #region decimal Weight

    public const string WeightProperty = "Weight";
    [DataMember(Name = WeightProperty, EmitDefaultValue = false)]
    decimal mWeight;
    [PersistantProperty]
    public decimal Weight
    {
      get { return mWeight; }
      set
      {
        if (SetProperty<decimal>(ref mWeight, value))
        {
          if (Owner != null) Owner.RaiseWeightChanged();
        }
      }
    }

    #endregion
  }
}

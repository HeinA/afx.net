﻿using Afx.Business;
using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using InventoryControl.Business;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace Weighbridge.Business
{
  [PersistantObject(Schema = "Weighbridge")]
  public partial class WeighbridgeTicketItem : AssociativeObject<WeighbridgeTicket, InventoryItem>
  {
  }
}

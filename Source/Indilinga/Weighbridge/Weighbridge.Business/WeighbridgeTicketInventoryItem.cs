﻿using Afx.Business;
using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Validation;
using InventoryControl.Business;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace Weighbridge.Business
{
  [PersistantObject(Schema = "Weighbridge")]
  public partial class WeighbridgeTicketInventoryItem : ExtensionObject<WeighbridgeTicket>
  {
    #region Constructors

    public WeighbridgeTicketInventoryItem()
    {
    }

    #endregion

    #region InventoryItem InventoryItem

    public const string InventoryItemProperty = "Item";
    [PersistantProperty]
    public InventoryItem InventoryItem
    {
      get { return GetCachedObject<InventoryItem>(); }
      set { SetCachedObject<InventoryItem>(value); }
    }

    #endregion
  }
}

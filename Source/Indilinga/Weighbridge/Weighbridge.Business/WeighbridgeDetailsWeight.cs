﻿using Afx.Business;
using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Validation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using System.Linq;

namespace Weighbridge.Business
{
  [PersistantObject(Schema = "Weighbridge")]
  public partial class WeighbridgeDetailsWeight : BusinessObject<WeighbridgeDetails> 
  {
    #region Constructors

    public WeighbridgeDetailsWeight()
    {
    }

    #endregion

    #region WeightCategory WeightCategory

    public const string WeightCategoryProperty = "WeightCategory";
    [PersistantProperty]
    [Mandatory("Weight category is a mandatory field.")]
    public WeightCategory WeightCategory
    {
      get { return GetCachedObject<WeightCategory>(); }
      set { SetCachedObject<WeightCategory>(value); }
    }

    #endregion

    #region decimal Weight

    public const string WeightProperty = "Weight";
    public decimal Weight
    {
      get
      {
        decimal weight = 0;
        foreach (var wi in Items.Where(i => !i.IsDeleted))
        {
          weight += wi.Weight;
        }
        return weight;
      }
    }

    #endregion

    #region BusinessObjectCollection<WeighbridgeDetailsWeightItem> Items

    public const string ItemProperty = "Items";
    BusinessObjectCollection<WeighbridgeDetailsWeightItem> mItems;
    [PersistantCollection]
    [DataMember]
    public BusinessObjectCollection<WeighbridgeDetailsWeightItem> Items
    {
      get { return mItems ?? (mItems = new BusinessObjectCollection<WeighbridgeDetailsWeightItem>(this)); }
    }

    #endregion

    #region void OnCompositionChanged(...)

    protected override void OnCompositionChanged(CompositionChangedType compositionChangedType, string propertyName, object originalSource, BusinessObject item)
    {
      if (propertyName == ItemProperty)
      {
        OnPropertyChanged(WeightProperty);
      }

      base.OnCompositionChanged(compositionChangedType, propertyName, originalSource, item);
    }

    #endregion
  }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Weighbridge.Business
{
  public enum WeighbridgeDirection
  {
    None = 0
    , Incoming = 1
    , Outgoing = -1
  }
}

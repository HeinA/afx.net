﻿using AccountManagement.Business;
using Afx.Business;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Documents;
using Afx.Business.Validation;
using ContactManagement.Business;
using FleetManagement.Business;
using InventoryControl.Business;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Weighbridge.Business
{
  [PersistantObject(Schema = "Weighbridge")]
  public partial class WeighbridgeTicket : Document //, IInventoryItemCollectionHolder
  {
    public WeighbridgeTicket()
      : base(Cache.Instance.GetObject<DocumentType>(DocumentTypeIdentifier))
    {
    }

    #region VehicleUnit VehicleUnit

    public const string VehicleUnitProperty = "VehicleUnit";
    [DataMember(Name = VehicleUnitProperty, EmitDefaultValue = false)]
    VehicleUnit mVehicleUnit;
    [PersistantProperty]
    public VehicleUnit VehicleUnit
    {
      get { return mVehicleUnit; }
      set
      {
        if (SetProperty<VehicleUnit>(ref mVehicleUnit, value))
        {
          if (!this.SuppressEventState)
          {
            VehicleReference = VehicleUnit.FleetUnitNumber;

            foreach (var v in Vehicles)
            {
              v.IsDeleted = true;
            }
            foreach (var v in VehicleUnit.Vehicles)
            {
              Vehicles.Add(new WeighbridgeTicketVehicle(v));
            }
            SetVehicleWeight();
          }
        }
      }
    }

    #endregion

    #region string VehicleReference

    public const string VehicleReferenceProperty = "VehicleReference";
    [DataMember(Name = VehicleReferenceProperty, EmitDefaultValue = false)]
    string mVehicleReference;
    [PersistantProperty]
    [Mandatory("Vehicle Reference is mandatory.")]
    public string VehicleReference
    {
      get { return mVehicleReference; }
      set { SetProperty<string>(ref mVehicleReference, value); }
    }

    #endregion

    #region WeighbridgeDirection Direction

    public const string DirectionProperty = "Direction";
    [DataMember(Name = DirectionProperty, EmitDefaultValue = false)]
    WeighbridgeDirection mDirection;
    [PersistantProperty]
    [Mandatory("Direction is mandatory.")]
    public WeighbridgeDirection Direction
    {
      get { return mDirection; }
      set { SetProperty<WeighbridgeDirection>(ref mDirection, value); }
    }

    #endregion

    #region WeighbridgeIncomingSource IncomingSource

    public const string IncomingSourceProperty = "IncomingSource";
    [PersistantProperty]
    public WeighbridgeIncomingSource IncomingSource
    {
      get { return GetCachedObject<WeighbridgeIncomingSource>(); }
      set { SetCachedObject<WeighbridgeIncomingSource>(value); }
    }

    #endregion

    #region AccountReference Account

    public const string AccountProperty = "Account";
    [PersistantProperty]
    public AccountReference Account
    {
      get { return GetCachedObject<AccountReference>(); }
      set { SetCachedObject<AccountReference>(value); }
    }

    #endregion

    #region Contact Driver

    public const string DriverProperty = "Driver";
    [DataMember(Name = DriverProperty, EmitDefaultValue = false)]
    Contact mDriver;
    [PersistantProperty]
    public Contact Driver
    {
      get { return mDriver; }
      set { SetProperty<Contact>(ref mDriver, value); }
    }

    #endregion

    #region BusinessObjectCollection<WeighbridgeTicketVehicle> Vehicles

    public const string VehiclesProperty = "Vehicles";
    BusinessObjectCollection<WeighbridgeTicketVehicle> mVehicles;
    [PersistantCollection]
    [DataMember]
    public BusinessObjectCollection<WeighbridgeTicketVehicle> Vehicles
    {
      get { return mVehicles ?? (mVehicles = new BusinessObjectCollection<WeighbridgeTicketVehicle>(this)); }
    }

    #endregion

    #region BusinessObjectCollection<WeighbridgeTicketWeight> Weights

    public const string WeightsProperty = "Weights";
    BusinessObjectCollection<WeighbridgeTicketWeight> mWeights;
    [PersistantCollection]
    [DataMember]
    public BusinessObjectCollection<WeighbridgeTicketWeight> Weights
    {
      get { return mWeights ?? (mWeights = new BusinessObjectCollection<WeighbridgeTicketWeight>(this)); }
    }

    #endregion

    #region decimal TareWeight

    [Mandatory("Tare is mandatory.")]
    [PersistantProperty(Write = true)]
    public decimal TareWeight
    {
      get
      {
        WeighbridgeTicketWeight w = Weights.FirstOrDefault(w1 => w1.WeightCategory.IsTare && !w1.IsDeleted);
        if (w != null) return w.Weight;
        return 0;
      }
    }

    #endregion

    #region decimal GrossWeight

    [PersistantProperty(Write = true)]
    public decimal GrossWeight
    {
      get
      {
        WeighbridgeTicketWeight w = Weights.FirstOrDefault(w1 => w1.WeightCategory.IsFinal && !w1.IsDeleted);
        if (w != null) return w.Weight;
        return 0;
      }
    }

    #endregion

    #region decimal NetWeight

    [PersistantProperty(Write = true)]
    public decimal NetWeight
    {
      get { return GrossWeight == 0 ? 0 : GrossWeight - TareWeight; }
    }

    #endregion

    #region void SetVehicleWeight()

    public void SetVehicleWeight()
    {
      WeighbridgeTicketWeight w = Weights.FirstOrDefault(w1 => w1.WeightCategory.IsTare && !w1.IsDeleted);
      if (w == null)
      {
        w = new WeighbridgeTicketWeight();
        w.WeightCategory = Cache.Instance.GetObjects<WeightCategory>().FirstOrDefault(w1 => w1.IsTare);
        Weights.Add(w);
      }

      foreach (var w1 in w.Items)
      {
        w1.IsDeleted = true;
      }

      decimal total = 0;
      foreach (WeighbridgeTicketVehicle v in Vehicles.Where(v => !v.IsDeleted))
      {
        total += v.VehicleWeight;
      }
      WeighbridgeTicketWeightItem wi = new WeighbridgeTicketWeightItem() { Description = "Vehicle Default Weight", Weight = total };
      w.Items.Add(wi);
    }

    #endregion

    #region void GetErrors(...)

    protected override void GetErrors(Collection<string> errors, string propertyName)
    {
      if (IsPropertyApplicable(propertyName, AccountProperty))
      {
        if (Direction == WeighbridgeDirection.Incoming && IsNull(Account))
          errors.Add("Account is mandatory.");
      }

      base.GetErrors(errors, propertyName);
    }

    #endregion
  }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Weighbridge.Touch.Events
{
  public class ScaleWeightUpdatedEventArgs : EventArgs
  {
    public ScaleWeightUpdatedEventArgs(decimal weight)
    {
      Weight = weight;
    }

    #region decimal Weight

    decimal mWeight;
    public decimal Weight
    {
      get { return mWeight; }
      private set { mWeight = value; }
    }

    #endregion
  }
}

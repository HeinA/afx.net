﻿using AccountManagement.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Weighbridge.Business;

namespace Weighbridge.Touch.Events
{
  public class SelectAccountEventArgs : EventArgs
  {
    public SelectAccountEventArgs(AccountReference account)
    {
      Account = account;
    }

    #region AccountReference Account

    AccountReference mAccount;
    public AccountReference Account
    {
      get { return mAccount; }
      private set { mAccount = value; }
    }

    #endregion
  }
}

﻿using InventoryControl.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Weighbridge.Touch.Events
{
  public class SelectInventoryItemEventArgs : EventArgs
  {
    public SelectInventoryItemEventArgs(InventoryItem item)
    {
      InventoryItem = item;
    }

    #region InventoryItem InventoryItem

    InventoryItem mInventoryItem;
    public InventoryItem InventoryItem
    {
      get { return mInventoryItem; }
      private set { mInventoryItem = value; }
    }

    #endregion
  }
}

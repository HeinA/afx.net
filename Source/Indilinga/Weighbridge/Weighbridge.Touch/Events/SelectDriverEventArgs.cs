﻿using ContactManagement.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Weighbridge.Touch.Events
{
  public class SelectDriverEventArgs : EventArgs
  {
    public SelectDriverEventArgs(Contact driver)
    {
      Driver = driver;
    }

    #region Contact Driver

    Contact mDriver;
    public Contact Driver
    {
      get { return mDriver; }
      private set { mDriver = value; }
    }

    #endregion
  }
}

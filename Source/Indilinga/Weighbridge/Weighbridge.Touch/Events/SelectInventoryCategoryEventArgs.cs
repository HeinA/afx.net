﻿using InventoryControl.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Weighbridge.Touch.Events
{
  public class SelectInventoryCategoryEventArgs : EventArgs
  {
    public SelectInventoryCategoryEventArgs(InventoryCategory inventoryCategory)
    {
      InventoryCategory = inventoryCategory;
    }

    #region InventoryCategory InventoryCategory

    public const string InventoryCategoryProperty = "InventoryCategory";
    InventoryCategory mInventoryCategory;
    public InventoryCategory InventoryCategory
    {
      get { return mInventoryCategory; }
      set { mInventoryCategory = value; }
    }

    #endregion
  }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Weighbridge.Business;

namespace Weighbridge.Touch.Events
{
  public class SetWeightEventArgs : EventArgs
  {
    public SetWeightEventArgs(WeighbridgeTicketWeight weight)
    {
      Weight = weight;
    }

    #region WeighbridgeTicketWeight Weight

    WeighbridgeTicketWeight mWeight;
    public WeighbridgeTicketWeight Weight
    {
      get { return mWeight; }
      private set { mWeight = value; }
    }

    #endregion
  }
}

﻿using Afx.Business;
using Afx.Business.Activities;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Documents;
using Afx.Business.Security;
using Afx.Business.Service;
using Afx.Prism;
using Afx.Prism.Touch;
using Afx.Prism.Touch.Events;
using InventoryControl.Business;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Weighbridge.Business;
using Weighbridge.Touch.Events;
using Weighbridge.Touch.Views.Error;
using Weighbridge.Touch.Views.Home;

namespace Weighbridge.Touch
{
  public class WeighbridgeModuleController : TouchModuleControllerBase
  {
    [InjectionConstructor]
    public WeighbridgeModuleController(IController controller)
      : base("Weighbridge Touch Module Controller", controller)
    {
    }

#if DEBUG
    Timer mTimer;

    private void OnTimer(object state)
    {
      try
      {
        mData = "16000";
        UpdateWeight();
      }
      catch (Exception ex)
      {
        if (ExceptionHelper.HandleException(ex)) throw ex;
      }
    }
#endif

    private void OnError(ExceptionEventArgs obj)
    {
      ErrorViewModel vm = Navigate<ErrorViewModel>();
      vm.Exception = obj.Exception;
    }

    protected override void OnUserAuthenticated(EventArgs obj)
    {
      try
      {
        OpenPort();
        Navigate<HomeViewModel>();
      }
      catch (Exception ex)
      {
        if (ExceptionHelper.HandleException(ex)) throw ex;
      }

      base.OnUserAuthenticated(obj);
    }

    void OpenPort()
    {
      WeighbridgeSetting ws = Setting.GetSetting<WeighbridgeSetting>();
      if (ws.IsEnabled)
      {
        mSerialPort = new SerialPort(ws.ComPort, ws.Baud, ws.Parity, ws.DataBits, ws.StopBits);

        mSerialPort.DataReceived += SerialPort_DataReceived;
        mSerialPort.ErrorReceived += SerialPort_ErrorReceived;
        if (!mSerialPort.IsOpen)
        {
          mSerialPort.Open();
          //using (StreamWriter sw = new StreamWriter(@"c:\logs\scale.txt", true))
          //{
          //  sw.WriteLine("Port Open: {0}", ws.ComPort);
          //}
        }
      }
    }

    void SerialPort_ErrorReceived(object sender, SerialErrorReceivedEventArgs e)
    {
      try
      {
        using (StreamWriter sw = new StreamWriter(@"c:\logs\scale.txt", true))
        {
          sw.WriteLine("Serial Error Received - rebinding");
        }
        if (mSerialPort.IsOpen)
        {
          mSerialPort.Close();
        }
        OpenPort();
      }
      catch (Exception ex)
      {
        if (ExceptionHelper.HandleException(ex)) throw ex;
      }
    }

    protected override void OnNavigated(NavigationResult result)
    {
      base.OnNavigated(result);
    }

    protected override void OnRunning()
    {
      EventAggregator.GetEvent<ExceptionEvent>().Subscribe(OnError);
      TouchApplicationController.Current.EventAggregator.GetEvent<HomeEvent>().Subscribe(OnHome);
#if DEBUG
      mTimer = new Timer(OnTimer, null, 1000, 1000);
#endif

      base.OnRunning();
    }

    private void OnHome(EventArgs obj)
    {
      Navigate<HomeViewModel>();
    }

    protected override void OnTerminated()
    {
      if (mSerialPort != null) mSerialPort.Close();
      //using (StreamWriter sw = new StreamWriter(@"c:\logs\scale.txt", true))
      //{
      //  sw.WriteLine("\nPort Closed");
      //}
#if DEBUG
      if (mTimer != null) mTimer.Dispose();
#endif
      base.OnTerminated();
    }

    #region Scale

    SerialPort mSerialPort;
    string mData = string.Empty;
    bool bReadingWeight = false;

    private void SerialPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
    {
      ProcessInput(mSerialPort.ReadExisting());
    }

    void ProcessInput(string data)
    {
      try
      {
        //using (StreamWriter sw = new StreamWriter(@"c:\logs\scale.txt", true))
        //{
        //  sw.Write(data);
        //}
        foreach (char c in data)
        {
          if (c == 0x53 || c == 0x2B) // S or +
          {
            UpdateWeight();
            bReadingWeight = true;
          }
          else
          {
            if (bReadingWeight && c >= 48 && c <= 57) // 0-9
            {
              mData += c;
            }
            else
            {
              if (!string.IsNullOrWhiteSpace(mData)) UpdateWeight();
              bReadingWeight = false;
            }
          }
        }
      }
      catch (Exception ex)
      {
        if (ExceptionHelper.HandleException(ex)) throw ex;
      }
    }

    void UpdateWeight()
    {
      if (!string.IsNullOrEmpty(mData))
      {
        try
        {
          decimal weight = decimal.Parse(mData);
          mData = string.Empty;
          ApplicationController.EventAggregator.GetEvent<ScaleWeightUpdatedEvent>().Publish(new ScaleWeightUpdatedEventArgs(weight));
        }
        catch (Exception ex)
        {
          if (ExceptionHelper.HandleException(ex)) throw ex;
        }
      }
    }

    #endregion
  }
}

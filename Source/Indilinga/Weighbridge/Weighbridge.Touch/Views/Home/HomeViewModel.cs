﻿using Afx.Business;
using Afx.Business.Data;
using Afx.Business.Documents;
using Afx.Business.Security;
using Afx.Business.Service;
using Afx.Prism;
using Afx.Prism.Touch;
using FleetManagement.Business;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Weighbridge.Business.Service;
using Weighbridge.Touch.Views.WeighbridgeTicket;

namespace Weighbridge.Touch.Views.Home
{
  [ViewModelRegistration]
  public class HomeViewModel : ViewModel
  {
    [InjectionConstructor]
    public HomeViewModel(IController controller)
      : base(controller)
    {
    }

    [Dependency]
    public ITouchModuleController ModuleController { get; set; }

    Collection<VehicleUnit> mVehicleUnits;
    public Collection<VehicleUnit> VehicleUnits
    {
      get { return mVehicleUnits ?? (mVehicleUnits = Cache.Instance.GetObjects<VehicleUnit>(true, vu => vu.ApplicableOrganizationalUnits.Contains(SecurityContext.OrganizationalUnit), vu => vu.FleetUnitNumber, true)); } 
    }

    #region VehicleUnit SelectedVehicleUnit

    VehicleUnit mSelectedVehicleUnit;
    public VehicleUnit SelectedVehicleUnit
    {
      get { return mSelectedVehicleUnit; }
      set
      {
        try
        {
          if (SetProperty<VehicleUnit>(ref mSelectedVehicleUnit, value))
          {
            Weighbridge.Business.WeighbridgeTicket t = new Weighbridge.Business.WeighbridgeTicket();
            t.VehicleUnit = SelectedVehicleUnit;
            ModuleController.Navigate<WeighbridgeTicketViewModel>(t);
          }
        }
        catch (Exception ex)
        {
          if (ExceptionHelper.HandleException(ex)) throw ex;
        }
      }
    }

    #endregion

    #region bool IsNewView

    bool mIsNewView = true;
    public bool IsNewView
    {
      get { return mIsNewView; }
      set
      {
        if (SetProperty<bool>(ref mIsNewView, value))
        {
          if (value)
          {
            IsExistingListVisible = Visibility.Collapsed;
            IsNewListVisible = Visibility.Visible;
          }
        }
      }
    }

    #endregion

    #region Visibility IsNewListVisible

    Visibility mIsNewListVisible = Visibility.Visible;
    public Visibility IsNewListVisible
    {
      get { return mIsNewListVisible; }
      set { SetProperty<Visibility>(ref mIsNewListVisible, value); }
    }

    #endregion

    #region bool IsExistingView

    bool mIsExistingView;
    public bool IsExistingView
    {
      get { return mIsExistingView; }
      set
      {
        if (SetProperty<bool>(ref mIsExistingView, value))
        {
          if (value)
          {
            IsExistingListVisible = Visibility.Visible;
            IsNewListVisible = Visibility.Collapsed;
          }
        }
      }
    }

    #endregion

    #region Visibility IsExistingListVisible

    Visibility mIsExistingListVisible = Visibility.Collapsed;
    public Visibility IsExistingListVisible
    {
      get { return mIsExistingListVisible; }
      set { SetProperty<Visibility>(ref mIsExistingListVisible, value); }
    }

    #endregion

    #region DelegateCommand ExitCommand

    DelegateCommand mExitCommand;
    public DelegateCommand ExitCommand
    {
      get { return mExitCommand ?? (mExitCommand = new DelegateCommand(ExecuteExit, CanExecuteExit)); }
    }

    bool CanExecuteExit()
    {
      return true;
    }

    void ExecuteExit()
    {
      Application.Current.MainWindow.Close();
    }

    #endregion

    #region SearchResults 

    SearchResults GetOpen()
    {
      Afx.Business.Documents.DocumentType dt = Cache.Instance.GetObject<Afx.Business.Documents.DocumentType>(Guid.Parse(Weighbridge.Business.WeighbridgeTicket.DocumentTypeIdentifier));
      Collection<string> filter = new Collection<string>();
      foreach (var dts in dt.States.Where(dts1 => dts1.IsInRecentList))
      {
        filter.Add(dts.Name);
      }

      using (var svc = ServiceFactory.GetService<IAfxService>(SecurityContext.ServerName))
      {
        SearchDatagraph sg = new SearchDatagraph(typeof(Weighbridge.Business.WeighbridgeTicket))
          .Select(Document.DocumentDateProperty, "Date", "dd MMM yyyy")
          .Select(Document.DocumentNumberProperty, "Document")
          .Join(Document.OrganizationalUnitProperty)
            .Join(OrganizationalUnit.ServerProperty)
              .Select(ServerInstance.NameProperty)
              .Where(ServerInstance.NameProperty, FilterType.Equals, SecurityContext.ServerName)
            .EndJoin()
          .EndJoin()
          .Join(Document.StateProperty)
            .Join(DocumentState.DocumentTypeStateProperty)
              .Select(DocumentTypeState.NameProperty, "State")
              .Select(DocumentTypeState.IsInRecentListProperty, "IsInRecentList")
              .Where(DocumentTypeState.NameProperty, FilterType.In, filter)
            .EndJoin()
          .EndJoin()
          .Select(Weighbridge.Business.WeighbridgeTicket.VehicleReferenceProperty, "Vehicle Reference");

        return svc.Instance.Search(sg);
      }
    }

    SearchResults GetRecent()
    {
      Afx.Business.Documents.DocumentType dt = Cache.Instance.GetObject<Afx.Business.Documents.DocumentType>(Guid.Parse(Weighbridge.Business.WeighbridgeTicket.DocumentTypeIdentifier));
      Collection<string> filter = new Collection<string>();
      foreach (var dts in dt.States.Where(dts1 => dts1.IsInRecentList))
      {
        filter.Add(dts.Name);
      }

      using (var svc = ServiceFactory.GetService<IAfxService>(SecurityContext.ServerName))
      {
        SearchDatagraph sg = new SearchDatagraph(typeof(Weighbridge.Business.WeighbridgeTicket))
          .Select(Document.DocumentDateProperty, "Date", "dd MMM yyyy")
            .Where(Document.DocumentDateProperty, FilterType.GreaterThanEqual, DateTime.Now.Date)
          .Select(Document.DocumentNumberProperty, "Document")
          .Join(Document.OrganizationalUnitProperty)
            .Join(OrganizationalUnit.ServerProperty)
              .Select(ServerInstance.NameProperty)
              .Where(ServerInstance.NameProperty, FilterType.Equals, SecurityContext.ServerName)
            .EndJoin()
          .EndJoin()
          .Join(Document.StateProperty)
            .Join(DocumentState.DocumentTypeStateProperty)
              .Select(DocumentTypeState.NameProperty, "State")
              .Select(DocumentTypeState.IsInRecentListProperty, "IsInRecentList")
              .Where(DocumentTypeState.NameProperty, FilterType.NotIn, filter)
            .EndJoin()
          .EndJoin()
          .Select(Weighbridge.Business.WeighbridgeTicket.VehicleReferenceProperty, "Vehicle Reference");

        return svc.Instance.Search(sg);
      }
    }

    #endregion

    #region RecentWeighbridgeTicketViewModel SelectedTicket

    RecentWeighbridgeTicketViewModel mSelectedTicket;
    public RecentWeighbridgeTicketViewModel SelectedTicket
    {
      get { return mSelectedTicket; }
      set
      {
        try
        {
          if (SetProperty<RecentWeighbridgeTicketViewModel>(ref mSelectedTicket, value))
          {
            using (var svc = ProxyFactory.GetService<IWeighbridgeService>(SecurityContext.ServerName))
            {
              Weighbridge.Business.WeighbridgeTicket t = svc.LoadWeighbridgeTicket(SelectedTicket.GlobalIdentifier);
              ModuleController.Navigate<WeighbridgeTicketViewModel>(t);
            }
          }
        }
        catch (Exception ex)
        {
          if (ExceptionHelper.HandleException(ex)) throw ex;
        }
      }
    }

    #endregion

    #region IEnumerable<RecentWeighbridgeTicketViewModel> RecentTickets

    public IEnumerable<RecentWeighbridgeTicketViewModel> RecentTickets
    {
      get
      {
        Collection<RecentWeighbridgeTicketViewModel> col = new Collection<RecentWeighbridgeTicketViewModel>();
        try
        {
          foreach (SearchResult sr in GetOpen().Results)
          {
            col.Add(new RecentWeighbridgeTicketViewModel(this.Controller) { GlobalIdentifier = sr.GlobalIdentifier, DocumentDate = (DateTime)sr["Date"], DocumentNumber = (string)sr["Document"], DocumentState = (string)sr["State"], VehicleReference = (string)sr["Vehicle Reference"], IsInRecentList = (bool)sr["IsInRecentList"] });
          }
          foreach (SearchResult sr in GetRecent().Results)
          {
            col.Add(new RecentWeighbridgeTicketViewModel(this.Controller) { GlobalIdentifier = sr.GlobalIdentifier, DocumentDate = (DateTime)sr["Date"], DocumentNumber = (string)sr["Document"], DocumentState = (string)sr["State"], VehicleReference = (string)sr["Vehicle Reference"], IsInRecentList = (bool)sr["IsInRecentList"] });
          }
        }
        catch(Exception ex)
        {
          if (ExceptionHelper.HandleException(ex)) throw ex;
        }
        return col.Where(t => t.IsInRecentList).OrderByDescending(t => t.DocumentDate).Union(col.Where(t => !t.IsInRecentList).OrderByDescending(t => t.DocumentDate));
      }
    }

    #endregion
  }
}

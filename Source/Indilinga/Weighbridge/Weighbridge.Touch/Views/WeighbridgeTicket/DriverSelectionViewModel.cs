﻿using AccountManagement.Business;
using AccountManagement.Business.Service;
using Afx.Business;
using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Business.Service;
using Afx.Prism;
using ContactManagement.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Weighbridge.Touch.Events;

namespace Weighbridge.Touch.Views.WeighbridgeTicket
{
  public class DriverSelectionViewModel : ViewModel
  {
    [InjectionConstructor]
    public DriverSelectionViewModel(IController controller)
      : base(controller)
    {
    }

    #region AccountReference Account

    public const string AccountProperty = "Account";
    AccountReference mAccount;
    public AccountReference Account
    {
      get { return mAccount; }
      set
      {
        if (SetProperty<AccountReference>(ref mAccount, value))
        {
          LoadDrivers();
        }
      }
    }

    #endregion

    bool mIsLoading = false;
    private void LoadDrivers()
    {
      if (mIsLoading) return;
      if (!BusinessObject.IsNull(Account))
      {
        if (Drivers != null) return;
        mIsLoading = true;
        Task.Run(() =>
        {
          try
          {
            if (BusinessObject.IsNull(Account.RepresentativeOrganizationalUnit))
            {
              using (var svc = ProxyFactory.GetService<IAccountManagementService>(SecurityContext.ServerName))
              {
                Account a = svc.LoadAccount(Account.GlobalIdentifier);
                mDrivers = Cache.Instance.GetObjects<Employee>(c => c.ContactType.IsFlagged(FleetManagement.Business.ContactTypeFlags.Driver), c => c.Fullname, true).Cast<Contact>().Union(a.Contacts.Where(c => c.ContactType.IsFlagged(FleetManagement.Business.ContactTypeFlags.Driver)).OrderBy(c => c.Fullname));
              }
            }
            else
            {
              mDrivers = Cache.Instance.GetObjects<Employee>(c => c.ContactType.IsFlagged(FleetManagement.Business.ContactTypeFlags.Driver), c => c.Fullname, true);
            }
            mIsLoading = false;
            OnPropertyChanged("Drivers");
          }
          catch
          {
            throw;
          }
        });
      }
      else
      {
        mDrivers = null;
        OnPropertyChanged("Drivers");
      }
    }

    IEnumerable<Contact> mDrivers = null;
    public IEnumerable<Contact> Drivers
    {
      get { return mDrivers; }
    }

    #region Contact SelectedContact

    public const string SelectedContactProperty = "SelectedContact";
    Contact mSelectedContact;
    public Contact SelectedContact
    {
      get { return mSelectedContact; }
      set
      {
        if (SetProperty<Contact>(ref mSelectedContact, value))
        {
          ApplicationController.Current.EventAggregator.GetEvent<SelectDriverEvent>().Publish(new SelectDriverEventArgs(value));
        }
      }
    }

    #endregion
  }
}

﻿using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Prism;
using InventoryControl.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Weighbridge.Touch.Events;

namespace Weighbridge.Touch.Views.WeighbridgeTicket
{
  public class InventoryCategorySelectionViewModel : ViewModel
  {
    [InjectionConstructor]
    public InventoryCategorySelectionViewModel(IController controller)
      : base(controller)
    {
    }

    #region InventoryCategory SelectedCategory

    public const string SelectedCategoryProperty = "SelectedCategory";
    InventoryCategory mSelectedCategory;
    public InventoryCategory SelectedCategory
    {
      get { return mSelectedCategory; }
      set
      {
        if (SetProperty<InventoryCategory>(ref mSelectedCategory, value))
        {
          ApplicationController.Current.EventAggregator.GetEvent<SelectInventoryCategoryEvent>().Publish(new SelectInventoryCategoryEventArgs(value));
        }
      }
    }

    #endregion

    IEnumerable<InventoryCategory> mCategories;
    public IEnumerable<InventoryCategory> Categories
    {
      get 
      {
        if (mCategories != null) return mCategories;
        IEnumerable<InventoryItem> items = Cache.Instance.GetObjects<InventoryItem>(ii => ii.ApplicableOrganizationalUnits.Contains(SecurityContext.OrganizationalUnit));
        mCategories = items.Select<InventoryItem, InventoryCategory>(ii => ii.Category).Distinct().OrderBy(ic => ic.Description);
        return mCategories;
      }
    }
  }
}

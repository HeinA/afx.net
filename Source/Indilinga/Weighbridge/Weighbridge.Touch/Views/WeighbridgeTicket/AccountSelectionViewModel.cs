﻿using AccountManagement.Business;
using Afx.Business;
using Afx.Business.Data;
using Afx.Prism;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Weighbridge.Business;
using Weighbridge.Touch.Events;

namespace Weighbridge.Touch.Views.WeighbridgeTicket
{
  public class AccountSelectionViewModel : ViewModel
  {
    [InjectionConstructor]
    public AccountSelectionViewModel(IController controller)
      : base(controller)
    {
    }

    #region WeighbridgeDirection WeighbridgeDirection

    public const string WeighbridgeDirectionProperty = "WeighbridgeDirection";
    WeighbridgeDirection mWeighbridgeDirection;
    public WeighbridgeDirection WeighbridgeDirection
    {
      get { return mWeighbridgeDirection; }
      set { mWeighbridgeDirection = value; }
    }

    #endregion

    public IEnumerable<AccountReference> Accounts
    {
      get
      {
        if(WeighbridgeDirection == Business.WeighbridgeDirection.Outgoing) return Cache.Instance.GetObjects<AccountReference>(ar => !BusinessObject.IsNull(ar.RepresentativeOrganizationalUnit) || ar.IsDebtor, ar => ar.Name, true);
        if (WeighbridgeDirection == Business.WeighbridgeDirection.Incoming) return Cache.Instance.GetObjects<AccountReference>(ar => !BusinessObject.IsNull(ar.RepresentativeOrganizationalUnit) || ar.IsCreditor, ar => ar.Name, true);
        return null;
      }
    }

    #region AccountReference Account

    AccountReference mAccount;
    public AccountReference SelectedAccount
    {
      get { return mAccount; }
      set
      {
        if (SetProperty<AccountReference>(ref mAccount, value))
        {
          ApplicationController.Current.EventAggregator.GetEvent<SelectAccountEvent>().Publish(new SelectAccountEventArgs(value));
        }
      }
    }

    #endregion
  }
}

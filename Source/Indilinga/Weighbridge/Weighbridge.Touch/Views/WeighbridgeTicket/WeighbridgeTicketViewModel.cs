﻿using Afx.Business;
using Afx.Business.Data;
using Afx.Business.Documents;
using Afx.Business.Security;
using Afx.Business.Service;
using Afx.Prism;
using Afx.Prism.Touch;
using InventoryControl.Business;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Weighbridge.Business;
using Weighbridge.Business.Service;
using Weighbridge.Touch.Events;
using Weighbridge.Touch.Views.WeighbridgeTicket.SetWeightDialog;

namespace Weighbridge.Touch.Views.WeighbridgeTicket
{
  [ViewModelRegistration]
  public class WeighbridgeTicketViewModel : ViewModel<Weighbridge.Business.WeighbridgeTicket>
  {
    public const string DialogRegion = "Weighbridge.Touch.Views.WeighbridgeTicket.WeighbridgeTicketViewModel.DialogRegion";

    [InjectionConstructor]
    public WeighbridgeTicketViewModel(IController controller)
      : base(controller)
    {
      ApplicationController.Current.EventAggregator.GetEvent<SelectInventoryItemEvent>().Subscribe(OnSelectInventoryItem);
      ApplicationController.Current.EventAggregator.GetEvent<SelectInventoryCategoryEvent>().Subscribe(OnSelectInventoryCategory);
      ApplicationController.Current.EventAggregator.GetEvent<SelectAccountEvent>().Subscribe(OnSelectAccount);
      ApplicationController.Current.EventAggregator.GetEvent<SelectDriverEvent>().Subscribe(OnDriverAccount);
      ApplicationController.Current.EventAggregator.GetEvent<SetWeightEvent>().Subscribe(OnSetWeightEvent);
    }

    ~WeighbridgeTicketViewModel()
    {
    }

    private void OnSetWeightEvent(SetWeightEventArgs obj)
    {
      if (!ModuleController.RegionManager.Regions[WeighbridgeModuleController.PageRegion].Views.Contains(this))
        return;

      foreach (var wi in Model.Weights)
      {
        if (wi.WeightCategory.Equals(obj.Weight.WeightCategory))
        {
          wi.IsDeleted = true;
        }
      }
      Model.Weights.Add((WeighbridgeTicketWeight)obj.Weight.Clone());
      OnPropertyChanged("TareWeight");
      OnPropertyChanged("GrossWeight");
      OnPropertyChanged("NetWeight");

      IRegion region = RegionManager.Regions[DialogRegion];
      ModuleController.Navigate<EmptyViewModel>(region);
    }

    private void OnSelectAccount(SelectAccountEventArgs obj)
    {
      Model.Account = obj.Account;
      OnPropertyChanged("IsDriverVisible");
    }

    private void OnDriverAccount(SelectDriverEventArgs obj)
    {
      Model.Driver = obj.Driver;
    }

    private void OnSelectInventoryItem(SelectInventoryItemEventArgs obj)
    {
      if (WeighbridgeTicketInventoryItem == null) return;
      WeighbridgeTicketInventoryItem.InventoryItem = obj.InventoryItem;
      IRegion region = RegionManager.Regions[DialogRegion];
      ModuleController.Navigate<EmptyViewModel>(region);
      OnPropertyChanged("Item");
    }

    private void OnSelectInventoryCategory(SelectInventoryCategoryEventArgs obj)
    {
      IRegion region = RegionManager.Regions[DialogRegion];
      ModuleController.Navigate<InventoryItemSelectionViewModel, InventoryCategory>(region, obj.InventoryCategory);
    }

    [Dependency]
    public ITouchModuleController ModuleController { get; set; }

    IRegionManager mRegionManager;
    public IRegionManager RegionManager
    {
      get { return mRegionManager ?? (mRegionManager = ApplicationController.Current.RegionManager.CreateRegionManager()); }
    }

    public IEnumerable<DocumentReference> References
    {
      get
      {
        if (Model.References.Count == 0)
        {
          foreach (var dtr in Model.DocumentType.References)
          {
            Model.References.Add(new DocumentReference() { DocumentTypeReference = dtr });
          }
        }
        return Model.References;
      }
    }

    #region string Title

    string mTitle;
    public string Title
    {
      get { return mTitle; }
      set { SetProperty<string>(ref mTitle, value); }
    }

    #endregion

    #region string VehicleReference

    public string VehicleReference
    {
      get { return Model == null ? GetDefaultValue<string>() : !BusinessObject.IsNull(Model.VehicleUnit) ? Model.VehicleUnit.FleetUnitNumber : Model.VehicleReference; }
      set { Model.VehicleReference = value; }
    }

    public bool IsVehicleReferenceReadOnly
    {
      get { return Model == null ? true : !BusinessObject.IsNull(Model.VehicleUnit) ? true : false; }
    }

    #endregion

    #region Visibility PrintVisibility

    Visibility mPrintVisibility;
    public Visibility PrintVisibility
    {
      get { return mPrintVisibility; }
      set { SetProperty<Visibility>(ref mPrintVisibility, value); }
    }

    #endregion

    #region Visibility SaveVisibility

    Visibility mSaveVisibility = Visibility.Collapsed;
    public Visibility SaveVisibility
    {
      get { return mSaveVisibility; }
      set { SetProperty<Visibility>(ref mSaveVisibility, value); }
    }

    #endregion

    #region bool IsIncoming

    public bool IsIncoming
    {
      get { return Model.Direction == Business.WeighbridgeDirection.Incoming; }
      set
      {
        if (IsIncoming) return;
        if (value)
        {
          Model.Direction = Business.WeighbridgeDirection.Incoming;
          Model.Account = null;
        }
        OnPropertyChanged("IsIncoming");
        OnPropertyChanged("IsAccountVisible");
      }
    }

    #endregion

    #region bool IsOutgoing

    public bool IsOutgoing
    {
      get { return Model.Direction == Business.WeighbridgeDirection.Outgoing; }
      set
      {
        if (IsOutgoing) return;
        if (value)
        {
          Model.Direction = Business.WeighbridgeDirection.Outgoing;
          Model.Account = null;
          //Model.IncomingSource = GetDefaultValue<WeighbridgeIncomingSource>();
        }
        OnPropertyChanged("IsOutgoing");
        OnPropertyChanged("IsAccountVisible");
      }
    }

    #endregion

    #region string Account

    public Visibility IsAccountVisible
    {
      get { return (IsIncoming || IsOutgoing) ? Visibility.Visible : Visibility.Collapsed; }
    }

    public string Account
    {
      get { return Model == null ? GetDefaultValue<string>() : Model.Account.Name; }
    }

    #endregion

    #region DelegateCommand<RoutedEventArgs> AccountGotFocusCommand

    DelegateCommand<RoutedEventArgs> mAccountGotFocusCommand;
    public DelegateCommand<RoutedEventArgs> AccountGotFocusCommand
    {
      get { return mAccountGotFocusCommand ?? (mAccountGotFocusCommand = new DelegateCommand<RoutedEventArgs>(ExecuteAccountGotFocus, CanExecuteAccountGotFocus)); }
    }

    bool CanExecuteAccountGotFocus(RoutedEventArgs args)
    {
      return true;
    }

    void ExecuteAccountGotFocus(RoutedEventArgs args)
    {
      try
      {
        IRegion region = RegionManager.Regions[DialogRegion];
        AccountSelectionViewModel vm = ModuleController.Navigate<AccountSelectionViewModel>(region);
        vm.WeighbridgeDirection = Model.Direction;
      }
      catch (Exception ex)
      {
        if (ExceptionHelper.HandleException(ex)) throw ex;
      }
    }

    #endregion

    #region DelegateCommand<RoutedEventArgs> AccountLostFocusCommand

    DelegateCommand<RoutedEventArgs> mAccountLostFocusCommand;
    public DelegateCommand<RoutedEventArgs> AccountLostFocusCommand
    {
      get { return mAccountLostFocusCommand ?? (mAccountLostFocusCommand = new DelegateCommand<RoutedEventArgs>(ExecuteAccountLostFocus, CanExecuteAccountLostFocus)); }
    }

    bool CanExecuteAccountLostFocus(RoutedEventArgs args)
    {
      return true;
    }

    void ExecuteAccountLostFocus(RoutedEventArgs args)
    {
      try
      {
        IRegion region = RegionManager.Regions[DialogRegion];
        ModuleController.Navigate<EmptyViewModel>(region);
      }
      catch (Exception ex)
      {
        if (ExceptionHelper.HandleException(ex)) throw ex;
      }
    }

    #endregion


    #region string Driver

    public Visibility IsDriverVisible
    {
      get
      {
        if (BusinessObject.IsNull(Model.Account)) return Visibility.Collapsed;
        if (BusinessObject.IsNull(Model.Account.RepresentativeOrganizationalUnit)) return Visibility.Collapsed;
        return Visibility.Visible;
      }
    }

    public string Driver
    {
      get { return Model == null ? GetDefaultValue<string>() : Model.Driver.Fullname; }
    }

    #endregion

    #region DelegateCommand<RoutedEventArgs> DriverGotFocusCommand

    DelegateCommand<RoutedEventArgs> mDriverGotFocusCommand;
    public DelegateCommand<RoutedEventArgs> DriverGotFocusCommand
    {
      get { return mDriverGotFocusCommand ?? (mDriverGotFocusCommand = new DelegateCommand<RoutedEventArgs>(ExecuteDriverGotFocus, CanExecuteDriverGotFocus)); }
    }

    bool CanExecuteDriverGotFocus(RoutedEventArgs args)
    {
      return true;
    }

    void ExecuteDriverGotFocus(RoutedEventArgs args)
    {
      try
      {
        IRegion region = RegionManager.Regions[DialogRegion];
        DriverSelectionViewModel vm = ModuleController.Navigate<DriverSelectionViewModel>(region);
        vm.Account = Model.Account;
      }
      catch (Exception ex)
      {
        if (ExceptionHelper.HandleException(ex)) throw ex;
      }
    }

    #endregion

    #region DelegateCommand<RoutedEventArgs> DriverLostFocusCommand

    DelegateCommand<RoutedEventArgs> mDriverLostFocusCommand;
    public DelegateCommand<RoutedEventArgs> DriverLostFocusCommand
    {
      get { return mDriverLostFocusCommand ?? (mDriverLostFocusCommand = new DelegateCommand<RoutedEventArgs>(ExecuteDriverLostFocus, CanExecuteDriverLostFocus)); }
    }

    bool CanExecuteDriverLostFocus(RoutedEventArgs args)
    {
      return true;
    }

    void ExecuteDriverLostFocus(RoutedEventArgs args)
    {
      try
      {
        IRegion region = RegionManager.Regions[DialogRegion];
        ModuleController.Navigate<EmptyViewModel>(region);
      }
      catch (Exception ex)
      {
        if (ExceptionHelper.HandleException(ex)) throw ex;
      }
    }

    #endregion


    #region DelegateCommand SaveCommand

    DelegateCommand mSaveCommand;
    public DelegateCommand SaveCommand
    {
      get { return mSaveCommand ?? (mSaveCommand = new DelegateCommand(ExecuteSave, CanExecuteSave)); }
    }

    bool CanExecuteSave()
    {
      return !mSaving;
    }

    bool mSaving = false;
    void ExecuteSave()
    {
      mSaving = true;
      try
      {
        using (new WaitCursor())
        using (var svc = ProxyFactory.GetService<IWeighbridgeService>(SecurityContext.ServerName))
        {
          Model = svc.SaveWeighbridgeTicket(Model);
        }
        ModuleController.Navigate<Weighbridge.Touch.Views.Home.HomeViewModel>();
      }
      catch (Exception ex)
      {
        if (ExceptionHelper.HandleException(ex)) throw ex;
      }
      finally
      {
        mSaving = false;
      }
    }

    #endregion

    #region DelegateCommand PrintCommand

    DelegateCommand mPrintCommand;
    public DelegateCommand PrintCommand
    {
      get { return mPrintCommand ?? (mPrintCommand = new DelegateCommand(ExecutePrint, CanExecutePrint)); }
    }

    bool CanExecutePrint()
    {
      return !mPrinting;
    }

    bool mPrinting = false;
    void ExecutePrint()
    {
      mPrinting = true;
      try
      {
        using (new WaitCursor())
        {
          SetDocumentState(Weighbridge.Business.WeighbridgeTicket.States.WeighedOut);
          TouchApplicationController.Current.PrintReport(Model, false);
          ModuleController.Navigate<Weighbridge.Touch.Views.Home.HomeViewModel>();
        }
      }
      catch (Exception ex)
      {
        if (ExceptionHelper.HandleException(ex)) throw ex;
      }
      finally
      {
        mPrinting = false;
      }
    }

    protected void SetDocumentState(string stateIdentifier)
    {
      if (!Model.Validate())
      {
        System.Media.SystemSounds.Hand.Play();
        return;
      }

      using (var svc = ProxyFactory.GetService<IAfxService>(SecurityContext.ServerName))
      {
        Model = (Weighbridge.Business.WeighbridgeTicket)svc.SetDocumentState(Model, stateIdentifier);
      }
    }

    #endregion

    #region DelegateCommand CancelCommand

    DelegateCommand mCancelCommand;
    public DelegateCommand CancelCommand
    {
      get { return mCancelCommand ?? (mCancelCommand = new DelegateCommand(ExecuteCancel, CanExecuteCancel)); }
    }

    bool CanExecuteCancel()
    {
      return true;
    }

    void ExecuteCancel()
    {
      try
      {
        ModuleController.Navigate<Weighbridge.Touch.Views.Home.HomeViewModel>();
      }
      catch (Exception ex)
      {
        if (ExceptionHelper.HandleException(ex)) throw ex;
      }
    }

    #endregion



    #region decimal TareWeight

    public decimal TareWeight
    {
      get { return Model == null ? GetDefaultValue<decimal>() : Model.TareWeight; }
    }

    #endregion

    #region DelegateCommand<RoutedEventArgs> TareWeightGotFocusCommand

    DelegateCommand<RoutedEventArgs> mTareWeightGotFocusCommand;
    public DelegateCommand<RoutedEventArgs> TareWeightGotFocusCommand
    {
      get { return mTareWeightGotFocusCommand ?? (mTareWeightGotFocusCommand = new DelegateCommand<RoutedEventArgs>(ExecuteTareWeightGotFocus, CanExecuteTareWeightGotFocus)); }
    }

    bool CanExecuteTareWeightGotFocus(RoutedEventArgs args)
    {
      return true;
    }

    void ExecuteTareWeightGotFocus(RoutedEventArgs args)
    {
      try
      {
        WeightCategory wc = Cache.Instance.GetObjects<WeightCategory>(wc1 => wc1.IsTare).FirstOrDefault();
        IRegion region = RegionManager.Regions[DialogRegion];
        SetWeightViewModel vm = ModuleController.Navigate<SetWeightViewModel>(region);
        vm.WeightCategory = wc;
      }
      catch (Exception ex)
      {
        if (ExceptionHelper.HandleException(ex)) throw ex;
      }
    }

    #endregion

    #region DelegateCommand<RoutedEventArgs> TareWeightLostFocusCommand

    DelegateCommand<RoutedEventArgs> mTareWeightLostFocusCommand;
    public DelegateCommand<RoutedEventArgs> TareWeightLostFocusCommand
    {
      get { return mTareWeightLostFocusCommand ?? (mTareWeightLostFocusCommand = new DelegateCommand<RoutedEventArgs>(ExecuteTareWeightLostFocus, CanExecuteTareWeightLostFocus)); }
    }

    bool CanExecuteTareWeightLostFocus(RoutedEventArgs args)
    {
      return true;
    }

    void ExecuteTareWeightLostFocus(RoutedEventArgs args)
    {
    }

    #endregion



    #region decimal GrossWeight

    public decimal GrossWeight
    {
      get { return Model == null ? GetDefaultValue<decimal>() : Model.GrossWeight; }
    }

    #endregion

    #region DelegateCommand<RoutedEventArgs> GrossWeightGotFocusCommand

    DelegateCommand<RoutedEventArgs> mGrossWeightGotFocusCommand;
    public DelegateCommand<RoutedEventArgs> GrossWeightGotFocusCommand
    {
      get { return mGrossWeightGotFocusCommand ?? (mGrossWeightGotFocusCommand = new DelegateCommand<RoutedEventArgs>(ExecuteGrossWeightGotFocus, CanExecuteGrossWeightGotFocus)); }
    }

    bool CanExecuteGrossWeightGotFocus(RoutedEventArgs args)
    {
      return true;
    }

    void ExecuteGrossWeightGotFocus(RoutedEventArgs args)
    {
      try
      {
        WeightCategory wc = Cache.Instance.GetObjects<WeightCategory>(wc1 => wc1.IsFinal).FirstOrDefault();
        IRegion region = RegionManager.Regions[DialogRegion];
        SetWeightViewModel vm = ModuleController.Navigate<SetWeightViewModel>(region);
        vm.WeightCategory = wc;
      }
      catch (Exception ex)
      {
        if (ExceptionHelper.HandleException(ex)) throw ex;
      }
    }

    #endregion

    #region DelegateCommand<RoutedEventArgs> GrossWeightLostFocusCommand

    DelegateCommand<RoutedEventArgs> mGrossWeightLostFocusCommand;
    public DelegateCommand<RoutedEventArgs> GrossWeightLostFocusCommand
    {
      get { return mGrossWeightLostFocusCommand ?? (mGrossWeightLostFocusCommand = new DelegateCommand<RoutedEventArgs>(ExecuteGrossWeightLostFocus, CanExecuteGrossWeightLostFocus)); }
    }

    bool CanExecuteGrossWeightLostFocus(RoutedEventArgs args)
    {
      return true;
    }

    void ExecuteGrossWeightLostFocus(RoutedEventArgs args)
    {
    }

    #endregion



    #region decimal NetWeight

    public decimal NetWeight
    {
      get { return Model == null ? GetDefaultValue<decimal>() : Model.NetWeight; }
    }

    #endregion


    #region WeighbridgeTicketInventoryItem WeighbridgeTicketInventoryItem

    WeighbridgeTicketInventoryItem mWeighbridgeTicketInventoryItem;
    WeighbridgeTicketInventoryItem WeighbridgeTicketInventoryItem
    {
      get
      {
        if (Model == null) return null;
        if (mWeighbridgeTicketInventoryItem == null)
        {
          mWeighbridgeTicketInventoryItem = Model.GetExtensionObject<WeighbridgeTicketInventoryItem>();
          if (mWeighbridgeTicketInventoryItem == null) return null;
          Validate();
        }
        return mWeighbridgeTicketInventoryItem;
      }
    }

    #endregion
    
    #region InventoryItem Item

    public Visibility IsItemVisible
    {
      get { return Item == null ? Visibility.Collapsed : Visibility.Visible; }
    }

    public InventoryItem Item
    {
      get
      {
        try
        {
          if (Model == null) return GetDefaultValue<InventoryItem>();
          if (WeighbridgeTicketInventoryItem == null) return GetDefaultValue<InventoryItem>();
          return WeighbridgeTicketInventoryItem.InventoryItem;
        }
        catch (Exception ex)
        {
          if (ExceptionHelper.HandleException(ex)) throw ex;
        }
        return null;
      }
    }

    #endregion

    #region DelegateCommand<RoutedEventArgs> ItemGotFocusCommand

    DelegateCommand<RoutedEventArgs> mItemGotFocusCommand;
    public DelegateCommand<RoutedEventArgs> ItemGotFocusCommand
    {
      get { return mItemGotFocusCommand ?? (mItemGotFocusCommand = new DelegateCommand<RoutedEventArgs>(ExecuteItemGotFocus, CanExecuteItemGotFocus)); }
    }

    bool CanExecuteItemGotFocus(RoutedEventArgs args)
    {
      return true;
    }

    void ExecuteItemGotFocus(RoutedEventArgs args)
    {
      try
      {
        IRegion region = RegionManager.Regions[DialogRegion];
        ModuleController.Navigate<InventoryCategorySelectionViewModel>(region);
      }
      catch (Exception ex)
      {
        if (ExceptionHelper.HandleException(ex)) throw ex;
      }
    }

    #endregion

    #region DelegateCommand<RoutedEventArgs> ItemLostFocusCommand

    DelegateCommand<RoutedEventArgs> mItemLostFocusCommand;
    public DelegateCommand<RoutedEventArgs> ItemLostFocusCommand
    {
      get { return mItemLostFocusCommand ?? (mItemLostFocusCommand = new DelegateCommand<RoutedEventArgs>(ExecuteItemLostFocus, CanExecuteItemLostFocus)); }
    }

    bool CanExecuteItemLostFocus(RoutedEventArgs args)
    {
      return true;
    }

    void ExecuteItemLostFocus(RoutedEventArgs args)
    {
      try
      {
        IRegion region = RegionManager.Regions[DialogRegion];
        ModuleController.Navigate<EmptyViewModel>(region);
      }
      catch (Exception ex)
      {
        if (ExceptionHelper.HandleException(ex)) throw ex;
      }
    }

    #endregion



    #region void OnModelChanged(...)

    protected override void OnModelChanged()
    {
      if (Model.IsNew) Title = string.Format("New {0}", Model.DocumentType.Name);
      else Title = Model.DocumentNumber;

      DoValidation();
      OnPropertyChanged(null);
      base.OnModelChanged();
    }

    #endregion

    #region void OnModelCompositionChanged(...)

    protected override void OnModelCompositionChanged(CompositionChangedEventArgs e)
    {
      DoValidation();
      base.OnModelCompositionChanged(e);
    }

    void DoValidation()
    {
      if (Validate())
      {
        SaveVisibility = Visibility.Visible;

        if ((NetWeight > 0 || GrossWeight > 0) && !BusinessObject.IsNull(Item))
        {
          PrintVisibility = Visibility.Visible;
        }
        else
        {
          PrintVisibility = Visibility.Collapsed;
        }
      }
      else
      {
        PrintVisibility = Visibility.Collapsed;
        SaveVisibility = Visibility.Collapsed;
      }
    }

    #endregion
  }
}

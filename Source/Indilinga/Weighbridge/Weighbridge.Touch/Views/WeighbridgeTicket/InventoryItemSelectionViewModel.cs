﻿using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Prism;
using InventoryControl.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Weighbridge.Touch.Events;

namespace Weighbridge.Touch.Views.WeighbridgeTicket
{
  public class InventoryItemSelectionViewModel : ViewModel<InventoryCategory>
  {
    [InjectionConstructor]
    public InventoryItemSelectionViewModel(IController controller)
      : base(controller)
    {
    }

    #region InventoryItem SelectedItem

    InventoryItem mSelectedItem;
    public InventoryItem SelectedItem
    {
      get { return mSelectedItem; }
      set
      {
        if (SetProperty<InventoryItem>(ref mSelectedItem, value))
        {
          ApplicationController.Current.EventAggregator.GetEvent<SelectInventoryItemEvent>().Publish(new SelectInventoryItemEventArgs(value));
        }
      }
    }

    #endregion

    IEnumerable<InventoryItem> mItems;
    public IEnumerable<InventoryItem> Items
    {
      get { return mItems ?? (mItems = Cache.Instance.GetObjects<InventoryItem>(ii => ii.Category.Equals(Model) && ii.ApplicableOrganizationalUnits.Contains(SecurityContext.OrganizationalUnit), ii => ii.Description, true)); }
    }
  }
}

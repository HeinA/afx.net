﻿using Afx.Business;
using Afx.Business.Collections;
using Afx.Prism;
using Afx.Prism.Touch;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Weighbridge.Business;
using Weighbridge.Touch.Events;

namespace Weighbridge.Touch.Views.WeighbridgeTicket.SetWeightDialog
{
  public class SetWeightViewModel : DialogViewModel<AddWeightDialogContext>
  {
    [InjectionConstructor]
    public SetWeightViewModel(IController controller)
      : base(controller)
    {
      Model = new AddWeightDialogContext();
      ApplicationController.Current.EventAggregator.GetEvent<ScaleWeightUpdatedEvent>().Subscribe(OnScaleWeightUpdated);
    }

    private void OnScaleWeightUpdated(ScaleWeightUpdatedEventArgs obj)
    {
      CurrentWeight = obj.Weight;
    }


    #region decimal CurrentWeight

    public decimal CurrentWeight
    {
      get { return Model == null ? GetDefaultValue<decimal>() : Model.CurrentWeight; }
      set { Model.CurrentWeight = value; }
    }

    #endregion

    #region IEnumerable<WeighbridgeTicketWeightItem> Items

    public IEnumerable<WeighbridgeTicketWeightItem> Items
    {
      get { return Model == null ? GetDefaultValue<BusinessObjectCollection<WeighbridgeTicketWeightItem>>() : Model.Items; }
    }

    #endregion

    #region DelegateCommand AddWeightCommand

    DelegateCommand mAddWeightCommand;
    public DelegateCommand AddWeightCommand
    {
      get { return mAddWeightCommand ?? (mAddWeightCommand = new DelegateCommand(ExecuteAddWeight, CanExecuteAddWeight)); }
    }

    bool CanExecuteAddWeight()
    {
      return true;
    }

    void ExecuteAddWeight()
    {
      Model.Items.Add(new WeighbridgeTicketWeightItem(string.Format("Axle {0}", Model.Items.Count + 1), CurrentWeight));
    }

    #endregion

    #region DelegateCommand SubtractWeightCommand

    DelegateCommand mSubtractWeightCommand;
    public DelegateCommand SubtractWeightCommand
    {
      get { return mSubtractWeightCommand ?? (mSubtractWeightCommand = new DelegateCommand(ExecuteSubtractWeight, CanExecuteSubtractWeight)); }
    }

    bool CanExecuteSubtractWeight()
    {
      return true;
    }

    void ExecuteSubtractWeight()
    {
      Model.Items.Add(new WeighbridgeTicketWeightItem(string.Format("Axle {0}", Model.Items.Count + 1), CurrentWeight * -1));
    }

    #endregion

    #region decimal TotalWeight

    public decimal TotalWeight
    {
      get { return Model == null ? GetDefaultValue<decimal>() : Model.TotalWeight; }
      set { Model.TotalWeight = value; }
    }

    #endregion

    #region WeightCategory WeightCategory

    public WeightCategory WeightCategory
    {
      get { return Model == null ? GetDefaultValue<WeightCategory>() : Model.WeightCategory; }
      set { Model.WeightCategory = value; }
    }

    #endregion

    #region DelegateCommand SetWeightCommand

    DelegateCommand mSetWeightCommand;
    public DelegateCommand SetWeightCommand
    {
      get { return mSetWeightCommand ?? (mSetWeightCommand = new DelegateCommand(ExecuteSetWeight, CanExecuteSetWeight)); }
    }

    bool CanExecuteSetWeight()
    {
      return true;
    }

    void ExecuteSetWeight()
    {
      try
      {
        if (Model.Validate())
        {
          WeighbridgeTicketWeight w = new WeighbridgeTicketWeight();
          w.WeightCategory = WeightCategory;
          foreach (var wi in Items)
          {
            w.Items.Add(wi);
          }
          ApplicationController.Current.EventAggregator.GetEvent<SetWeightEvent>().Publish(new SetWeightEventArgs(w));
        }
      }
      catch (Exception ex)
      {
        if (ExceptionHelper.HandleException(ex)) throw ex;
      }
    }

    #endregion
  }
}

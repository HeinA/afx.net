﻿using Afx.Prism;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Weighbridge.Touch.Views.WeighbridgeTicket
{
  public class EmptyViewModel : ViewModel
  {
    [InjectionConstructor]
    public EmptyViewModel(IController controller)
      : base(controller)
    {
    }
  }
}

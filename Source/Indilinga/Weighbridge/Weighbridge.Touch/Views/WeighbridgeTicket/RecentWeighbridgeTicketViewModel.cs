﻿using Afx.Prism;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace Weighbridge.Touch.Views.WeighbridgeTicket
{
  public class RecentWeighbridgeTicketViewModel : ViewModel
  {
    [InjectionConstructor]
    public RecentWeighbridgeTicketViewModel(IController controller)
      : base(controller)
    {
    }

    #region Guid GlobalIdentifier

    Guid mGlobalIdentifier;
    public Guid GlobalIdentifier
    {
      get { return mGlobalIdentifier; }
      set { SetProperty<Guid>(ref mGlobalIdentifier, value); }
    }

    #endregion

    #region string DocumentNumber

    string mDocumentNumber;
    public string DocumentNumber
    {
      get { return mDocumentNumber; }
      set { SetProperty<string>(ref mDocumentNumber, value); }
    }

    #endregion

    #region DateTime DocumentDate

    DateTime mDocumentDate;
    public DateTime DocumentDate
    {
      get { return mDocumentDate; }
      set { SetProperty<DateTime>(ref mDocumentDate, value); }
    }

    #endregion

    #region string DocumentState

    string mDocumentState;
    public string DocumentState
    {
      get { return mDocumentState; }
      set { SetProperty<string>(ref mDocumentState, value); }
    }

    #endregion

    #region bool IsInRecentList

    public const string IsInRecentListProperty = "IsInRecentList";
    bool mIsInRecentList;
    public bool IsInRecentList
    {
      get { return mIsInRecentList; }
      set { mIsInRecentList = value; }
    }

    #endregion

    #region Brush BackgroundBrush

    public Brush BackgroundBrush
    {
      get { return IsInRecentList ? Brushes.LightYellow : Brushes.LightSlateGray; }
    }

    #endregion

    #region Brush BorderBrush

    public Brush BorderBrush
    {
      get { return IsInRecentList ? Brushes.Yellow : Brushes.SlateGray; }
    }

    #endregion

    #region string VehicleReference

    string mVehicleReference;
    public string VehicleReference
    {
      get { return mVehicleReference; }
      set { SetProperty<string>(ref mVehicleReference, value); }
    }

    #endregion
  }
}

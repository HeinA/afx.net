﻿using Afx.Prism;
using Afx.Prism.Touch;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Weighbridge.Touch.Events;
using Weighbridge.Touch.Views.Home;

namespace Weighbridge.Touch.Views.Error
{
  public class ErrorViewModel : ViewModel
  {
    [InjectionConstructor]
    public ErrorViewModel(IController controller)
      : base(controller)
    {
    }

    #region Exception Exception

    Exception mException;
    public Exception Exception
    {
      get { return mException; }
      set
      {
        if (SetProperty<Exception>(ref mException, value))
        {
          OnPropertyChanged("ExceptionText");
        }
      }
    }

    #endregion

    #region string ExceptionText

    public string ExceptionText
    {
      get { return Exception.ToString(); }
    }

    #endregion

    #region DelegateCommand CloseCommand

    DelegateCommand mCloseCommand;
    public DelegateCommand CloseCommand
    {
      get { return mCloseCommand ?? (mCloseCommand = new DelegateCommand(ExecuteClose, CanExecuteClose)); }
    }

    bool CanExecuteClose()
    {
      return true;
    }

    void ExecuteClose()
    {
      TouchApplicationController.Current.EventAggregator.GetEvent<HomeEvent>().Publish(EventArgs.Empty);
    }

    #endregion

  }
}

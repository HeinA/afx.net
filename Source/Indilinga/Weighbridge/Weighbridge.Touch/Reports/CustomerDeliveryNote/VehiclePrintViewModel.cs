﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Weighbridge.Business;

namespace WeighbridgeIc.Prism.Reports.CustomerDeliveryNote
{
  public class VehiclePrintViewModel
  {
    public VehiclePrintViewModel(WeighbridgeDetailsVehicle vehicle)
    {
      Vehicle = vehicle;
    }

    #region WeighbridgeDetailsVehicle Vehicle

    WeighbridgeDetailsVehicle mVehicle;
    WeighbridgeDetailsVehicle Vehicle
    {
      get { return mVehicle; }
      set { mVehicle = value; }
    }

    #endregion

    public string FleetNumber
    {
      get { return Vehicle.FleetNumber; }
    }

    public string RegistrationNumber
    {
      get { return Vehicle.RegistrationNumber; }
    }

    public string Make
    {
      get { return Vehicle.Make; }
    }

    public string Model
    {
      get { return Vehicle.Model; }
    }

    public decimal VehicleWeight
    {
      get { return Vehicle.VehicleWeight; }
    }
  }
}

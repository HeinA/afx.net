﻿using Afx.Business.Documents;
using Afx.Prism.Documents;
using InventoryControl.Business;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Weighbridge.Business;

namespace WeighbridgeIc.Prism.Reports.CustomerDeliveryNote
{
  [Export(InventoryControl.Business.CustomerDeliveryNote.DocumentTypeIdentifier, typeof(IDocumentPrintInfo))]
  class CustomerDeliveryNoteInfo : IDocumentPrintInfo
  {
    public int Priority
    {
      get { return 90; }
    }

    public Stream ReportStream
    {
      get { return this.GetType().Assembly.GetManifestResourceStream("WeighbridgeIc.Prism.Reports.CustomerDeliveryNote.CustomerDeliveryNote.rdlc"); }
    }

    public void RefreshReportData(LocalReport report, Document document)
    {
      InventoryDocument id = (InventoryDocument)document;

      ReportDataSource reportDataSource = new ReportDataSource();
      reportDataSource.Name = "Document";
      reportDataSource.Value = new object[] { new DocumentPrintViewModel(document) };
      report.DataSources.Add(reportDataSource);

      WeighbridgeDetails wd = document.GetExtensionObject<WeighbridgeDetails>();

      reportDataSource = new ReportDataSource();
      reportDataSource.Name = "Vehicles";
      Collection<VehiclePrintViewModel> vehicles = new Collection<VehiclePrintViewModel>();
      foreach (WeighbridgeDetailsVehicle v in wd.Vehicles)
      {
        if (!v.IsDeleted) vehicles.Add(new VehiclePrintViewModel(v));
      }
      reportDataSource.Value = vehicles;
      report.DataSources.Add(reportDataSource);

      reportDataSource = new ReportDataSource();
      reportDataSource.Name = "Weights";
      Collection<WeightPrintViewModel> weights = new Collection<WeightPrintViewModel>();
      foreach (WeighbridgeDetailsWeight w in wd.Weights)
      {
        foreach (WeighbridgeDetailsWeightItem wi in w.Items)
        {
          if (!wi.IsDeleted) weights.Add(new WeightPrintViewModel(wi));
        }
      }
      reportDataSource.Value = weights;
      report.DataSources.Add(reportDataSource);

      reportDataSource = new ReportDataSource();
      reportDataSource.Name = "Items";
      Collection<ItemPrintViewModel> items = new Collection<ItemPrintViewModel>();
      foreach (InventoryDocumentItem i in id.Items)
      {
        if (!i.IsDeleted) items.Add(new ItemPrintViewModel(i));
      }
      reportDataSource.Value = items;
      report.DataSources.Add(reportDataSource);
    }

    public short Copies
    {
      get { return 1; }
    }
  }
}

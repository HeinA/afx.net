﻿using Afx.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Weighbridge.Business;

namespace Weighbridge.Prism.Reports.WeighbridgeTicket
{
  public class WeighbridgeTicketPrintViewModel
  {
    public WeighbridgeTicketPrintViewModel(Weighbridge.Business.WeighbridgeTicket details)
    {
      Details = details;
    }

    #region WeighbridgeTicket Details

    Weighbridge.Business.WeighbridgeTicket mDetails;
    Weighbridge.Business.WeighbridgeTicket Details
    {
      get { return mDetails; }
      set { mDetails = value; }
    }

    #endregion

    public string Vehicle
    {
      get { return Details.VehicleReference; }
    }

    public decimal NetWeight
    {
      get { return Details.NetWeight; }
    }

    public string Direction
    {
      get { return Details.Direction.ToString(); }
    }

    public string Account
    {
      get { return Details.Account.Name; }
    }

    public string Driver
    {
      get
      {
        if (BusinessObject.IsNull(Details.Driver)) return string.Empty;
        return Details.Driver.Fullname;
      }
    }
  }
}

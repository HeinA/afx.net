﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Weighbridge.Business;

namespace WeighbridgeIc.Prism.Reports.WeighbridgeTicket
{
  public class WeighbridgeDetailsPrintViewModel
  {
    public WeighbridgeDetailsPrintViewModel(WeighbridgeDetails details)
    {
      Details = details;
    }

    #region WeighbridgeDetails Details

    WeighbridgeDetails mDetails;
    WeighbridgeDetails Details
    {
      get { return mDetails; }
      set { mDetails = value; }
    }

    #endregion

    public string Vehicle
    {
      get { return Details.VehicleReference; }
    }

    public decimal NetWeight
    {
      get { return Details.NetWeight; }
    }

    public string Direction
    {
      get { return Details.Direction.ToString(); }
    }

    public string Source
    {
      get { return Details.IncomingSource.Text; }
    }
  }
}

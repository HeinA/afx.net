﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Weighbridge.Business;

namespace Weighbridge.Prism.Reports.WeighbridgeTicket
{
  public class WeightPrintViewModel
  {
    public WeightPrintViewModel(WeighbridgeTicketWeightItem weightItem)
    {
      WeighbridgeTicketWeightItem = weightItem;
    }

    #region WeighbridgeTicketWeightItem WeighbridgeDetailsWeightItem

    WeighbridgeTicketWeightItem mWeighbridgeTicketWeightItem;
    WeighbridgeTicketWeightItem WeighbridgeTicketWeightItem
    {
      get { return mWeighbridgeTicketWeightItem; }
      set { mWeighbridgeTicketWeightItem = value; }
    }
    
    #endregion

    public Guid WeightIdentifier
    {
      get { return WeighbridgeTicketWeightItem.Owner.GlobalIdentifier; }
    }

    public string WeightCategory
    {
      get { return WeighbridgeTicketWeightItem.Owner.WeightCategory.Description; }
    }

    public string WeightItemText
    {
      get { return WeighbridgeTicketWeightItem.Description; }
    }

    public decimal WeightItemWeight
    {
      get { return WeighbridgeTicketWeightItem.Weight; }
    }
  }
}

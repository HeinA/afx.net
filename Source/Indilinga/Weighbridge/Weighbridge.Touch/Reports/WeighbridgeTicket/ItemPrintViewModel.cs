﻿using InventoryControl.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Weighbridge.Prism.Reports.WeighbridgeTicket
{
  public class ItemPrintViewModel
  {
    public ItemPrintViewModel(InventoryItem item)
    {
      Item = item;
    }

    #region InventoryItem Item

    InventoryItem mItem;
    InventoryItem Item
    {
      get { return mItem; }
      set { mItem = value; }
    }

    #endregion

    public string ItemDescription
    {
      get { return Item.Description; }
    }
  }
}

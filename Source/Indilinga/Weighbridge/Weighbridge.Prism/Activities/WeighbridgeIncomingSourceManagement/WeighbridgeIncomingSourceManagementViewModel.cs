﻿using Afx.Business.Security;
using Afx.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Activities;
using Weighbridge.Business;

namespace Weighbridge.Prism.Activities.WeighbridgeIncomingSourceManagement
{
  public partial class WeighbridgeIncomingSourceManagementViewModel : MdiSimpleActivityViewModel<WeighbridgeIncomingSourceManagement>
  {
    #region Constructors

    [InjectionConstructor]
    public WeighbridgeIncomingSourceManagementViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion
  }
}

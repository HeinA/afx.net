﻿using Weighbridge.Business;
using Weighbridge.Business.Service;
using Afx.Business.Service;
using Afx.Prism.RibbonMdi.Activities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.Composition;
using Afx.Business.Security;

namespace Weighbridge.Prism.Activities.WeighbridgeIncomingSourceManagement
{
  //**************************************************************************************************************
  //**************************************************************************************************************
  //*                                                                                                            *
  //*     This code is auto-generated.  Do not place any cutom code here, it will be lost on re-generation.      *
  //*                                                                                                            *
  //**************************************************************************************************************
  //**************************************************************************************************************
  [Export("Context:" + WeighbridgeIncomingSourceManagement.Key)]
  public partial class WeighbridgeIncomingSourceManagement
  {
    public const string Key = "{d540e8fb-7fa9-4b88-9a12-9143765135a7}";

    public override void LoadData()
    {
      using (var svc = ProxyFactory.GetService<IWeighbridgeService>(SecurityContext.MasterServer.Name))
      {
        Data = svc.LoadWeighbridgeIncomingSourceCollection();
      }
      base.LoadData();
    }

    public override void SaveData()
    {
      using (var svc = ProxyFactory.GetService<IWeighbridgeService>(SecurityContext.MasterServer.Name))
      {
        Data = svc.SaveWeighbridgeIncomingSourceCollection(Data);
      }
      base.SaveData();
    }
  }
}

﻿using Afx.Business.Activities;
using Weighbridge.Business;
using Weighbridge.Business.Service;
using Afx.Prism.RibbonMdi.Activities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Weighbridge.Prism.Activities.WeighbridgeIncomingSourceManagement
{
  public partial class WeighbridgeIncomingSourceManagement : SimpleActivityContext<WeighbridgeIncomingSource>
  {
    public WeighbridgeIncomingSourceManagement()
    {
    }

    public WeighbridgeIncomingSourceManagement(ContextualActivity activity)
      : base(activity)
    {
    }
  }
}

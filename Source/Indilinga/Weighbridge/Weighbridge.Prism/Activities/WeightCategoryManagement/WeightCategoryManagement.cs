﻿using Afx.Business.Activities;
using Weighbridge.Business;
using Weighbridge.Business.Service;
using Afx.Prism.RibbonMdi.Activities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Weighbridge.Prism.Activities.WeightCategoryManagement
{
  public partial class WeightCategoryManagement : SimpleActivityContext<WeightCategory>
  {
    public WeightCategoryManagement()
    {
    }

    public WeightCategoryManagement(ContextualActivity activity)
      : base(activity)
    {
    }
  }
}

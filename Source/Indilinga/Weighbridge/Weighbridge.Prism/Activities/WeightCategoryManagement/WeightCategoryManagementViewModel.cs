﻿using Afx.Business.Security;
using Afx.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Activities;
using Weighbridge.Business;

namespace Weighbridge.Prism.Activities.WeightCategoryManagement
{
  public partial class WeightCategoryManagementViewModel : MdiSimpleActivityViewModel<WeightCategoryManagement>
  {
    #region Constructors

    [InjectionConstructor]
    public WeightCategoryManagementViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion
  }
}

﻿using Weighbridge.Business;
using Weighbridge.Business.Service;
using Afx.Business.Service;
using Afx.Prism.RibbonMdi.Activities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.Composition;
using Afx.Business.Security;

namespace Weighbridge.Prism.Activities.WeightCategoryManagement
{
  //**************************************************************************************************************
  //**************************************************************************************************************
  //*                                                                                                            *
  //*     This code is auto-generated.  Do not place any cutom code here, it will be lost on re-generation.      *
  //*                                                                                                            *
  //**************************************************************************************************************
  //**************************************************************************************************************
  [Export("Context:" + WeightCategoryManagement.Key)]
  public partial class WeightCategoryManagement
  {
    public const string Key = "{ffe46cdc-182b-4bb5-8343-fc3530c592ca}";

    public override void LoadData()
    {
      using (var svc = ProxyFactory.GetService<IWeighbridgeService>(SecurityContext.MasterServer.Name))
      {
        Data = svc.LoadWeightCategoryCollection();
      }
      base.LoadData();
    }

    public override void SaveData()
    {
      using (var svc = ProxyFactory.GetService<IWeighbridgeService>(SecurityContext.MasterServer.Name))
      {
        Data = svc.SaveWeightCategoryCollection(Data);
      }
      base.SaveData();
    }
  }
}

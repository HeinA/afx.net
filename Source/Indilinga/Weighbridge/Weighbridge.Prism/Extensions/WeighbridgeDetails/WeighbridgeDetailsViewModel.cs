﻿using Afx.Business;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Documents;
using FleetManagement.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Weighbridge.Business;

namespace Weighbridge.Prism.Extensions.WeighbridgeDetails
{
  public class WeighbridgeDetailsViewModel : ExtensionViewModel<Weighbridge.Business.WeighbridgeDetails>
  {
    #region Constructors

    [InjectionConstructor]
    public WeighbridgeDetailsViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    #region string VehicleReference

    public string VehicleReference
    {
      get { return Model == null ? GetDefaultValue<string>() : Model.VehicleReference; }
      set { Model.VehicleReference = value; }
    }

    #endregion

    #region bool IsVehicleReferenceReadOnly

    public bool IsVehicleReferenceReadOnly
    {
      get { return !BusinessObject.IsNull(Model.VehicleUnit); }
    }

    #endregion

    #region WeighbridgeDirection Direction

    public WeighbridgeDirection Direction
    {
      get { return Model == null ? GetDefaultValue<WeighbridgeDirection>() : Model.Direction; }
      set { Model.Direction = value; }
    }

    #endregion

    #region WeighbridgeIncomingSource IncomingSource

    public Visibility IncomingSourceVisibility
    {
      get
      {
        if (Direction == WeighbridgeDirection.Incoming) return Visibility.Visible;
        return Visibility.Collapsed;
      }
    }

    IEnumerable<WeighbridgeIncomingSource> mIncomingSources;
    public IEnumerable<WeighbridgeIncomingSource> IncomingSources
    {
      get { return mIncomingSources ?? (mIncomingSources = Cache.Instance.GetObjects<WeighbridgeIncomingSource>(true, ics => ics.Text, true)); }
    }

    public WeighbridgeIncomingSource IncomingSource
    {
      get { return Model == null ? GetDefaultValue<WeighbridgeIncomingSource>() : Model.IncomingSource; }
      set { Model.IncomingSource = value; }
    }

    #endregion


    #region void OnModelPropertyChanged(...)

    protected override void OnModelPropertyChanged(string propertyName)
    {
      switch (propertyName)
      {
        case Weighbridge.Business.WeighbridgeDetails.VehicleUnitProperty:
          {
            OnPropertyChanged("IsVehicleReferenceReadOnly");
          }
          break;

        case Weighbridge.Business.WeighbridgeDetails.DirectionProperty:
          {
            OnPropertyChanged("IncomingSourceVisibility");
          }
          break;
      }
      base.OnModelPropertyChanged(propertyName);
    }

    #endregion

    #region IEnumerable<WeighbridgeTicketWeight> Weights

    BasicCollection<Weighbridge.Business.WeightCategory> mWeightCategoryCollection;
    public IEnumerable<Weighbridge.Business.WeightCategory> WeightCategoryCollection
    {
      get { return mWeightCategoryCollection ?? (mWeightCategoryCollection = Cache.Instance.GetObjects<Weighbridge.Business.WeightCategory>(wc => wc.Description, true)); }
    }

    public IEnumerable<WeighbridgeDetailsWeight> Weights
    {
      get { return Model == null ? GetDefaultValue<IEnumerable<WeighbridgeDetailsWeight>>() : Model.Weights; }
    }

    #endregion

    #region IEnumerable<WeighbridgeTicketVehicle> Vehicles

    BasicCollection<Vehicle> mVehicleCollection;
    public IEnumerable<Vehicle> VehicleCollection
    {
      get { return mVehicleCollection ?? (mVehicleCollection = Cache.Instance.GetObjects<Vehicle>(true, v => v.Text, true)); }
    }

    public IEnumerable<WeighbridgeDetailsVehicle> Vehicles
    {
      get { return Model == null ? null : Model.Vehicles; }
    }

    #endregion

  }
}

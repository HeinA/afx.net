﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Weighbridge.Prism.Extensions.WeighbridgeDetails.EditVehiclesDialog
{
  [Export(typeof(Afx.Prism.RibbonMdi.ResourceInfo))]
  public class ResourceInfo : Afx.Prism.RibbonMdi.ResourceInfo
  {
    public override Uri GetUri()
    {
      return new Uri("pack://application:,,,/Weighbridge.Prism;component/Extensions/WeighbridgeDetails/EditVehiclesDialog/Resources.xaml", UriKind.Absolute);
    }

    public override int Priority
    {
      get { return 40; }
    }
  }
}
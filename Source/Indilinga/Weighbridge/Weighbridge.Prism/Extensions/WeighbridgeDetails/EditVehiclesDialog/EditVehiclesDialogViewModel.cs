﻿using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using FleetManagement.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Weighbridge.Prism.Extensions.WeighbridgeDetails.EditVehiclesDialog
{
  public class EditVehiclesDialogViewModel : MdiDialogViewModel<EditVehiclesDialogContext>
  {
    [InjectionConstructor]
    public EditVehiclesDialogViewModel(IController controller)
      : base(controller)
    {
    }

    BasicCollection<VehicleUnit> mVehicleUnits;
    public IEnumerable<VehicleUnit> Units
    {
      get
      {
        return mVehicleUnits ?? (mVehicleUnits = Cache.Instance.GetObjects<VehicleUnit>(true, v => v.FleetUnitNumber, true));
      }
    }

    #region VehicleUnit VehicleUnit

    public VehicleUnit VehicleUnit
    {
      get { return Model == null ? GetDefaultValue<VehicleUnit>() : Model.VehicleUnit; }
      set { Model.VehicleUnit = value; }
    }

    #endregion

    BasicCollection<Vehicle> mVehicles;
    public IEnumerable<VehicleItemViewModel> Vehicles
    {
      get
      {
        if (mVehicles == null) mVehicles = Cache.Instance.GetObjects<Vehicle>(v => v.FleetNumber, true);
        return ResolveViewModels<VehicleItemViewModel, Vehicle>(mVehicles);
      }
    }
  }
}

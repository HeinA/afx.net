﻿using Afx.Business;
using Afx.Business.Collections;
using FleetManagement.Business;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Weighbridge.Prism.Extensions.WeighbridgeDetails.EditVehiclesDialog
{
  public class EditVehiclesDialogContext : BusinessObject
  {
    #region VehicleUnit VehicleUnit

    public const string VehicleUnitProperty = "VehicleUnit";
    VehicleUnit mVehicleUnit;
    public VehicleUnit VehicleUnit
    {
      get { return mVehicleUnit; }
      set { SetProperty<VehicleUnit>(ref mVehicleUnit, value); }
    }

    #endregion

    BasicCollection<Vehicle> mVehicles;
    public BasicCollection<Vehicle> Vehicles
    {
      get { return mVehicles ?? (mVehicles = new BasicCollection<Vehicle>()); }
    }
  }
}
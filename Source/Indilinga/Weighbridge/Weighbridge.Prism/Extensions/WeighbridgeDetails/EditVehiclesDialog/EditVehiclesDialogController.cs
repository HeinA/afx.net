﻿using Afx.Business;
using Afx.Business.Collections;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using Afx.Prism.RibbonMdi.Documents;
using FleetManagement.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;
using Weighbridge.Business;

namespace Weighbridge.Prism.Extensions.WeighbridgeDetails.EditVehiclesDialog
{
  public class EditVehiclesDialogController : MdiDialogController<EditVehiclesDialogContext, EditVehiclesDialogViewModel>
  {
    public const string EditVehiclesDialogControllerKey = "Weighbridge.Prism.Extensions.WeighbridgeDetails.EditVehiclesDialog.AddVehicleDialogController";

    public EditVehiclesDialogController(IController controller)
      : base(EditVehiclesDialogControllerKey, controller)
    {
    }

    IMdiDocumentController mDocumentController;
    [Dependency]
    public IMdiDocumentController DocumentController
    {
      get { return mDocumentController; }
      set { mDocumentController = value; }
    }

    global::Weighbridge.Business.WeighbridgeDetails mWeighbridgeDetails;

    protected override bool OnRun()
    {
      if (DataContext == null) DataContext = new EditVehiclesDialogContext();
      ViewModel.Caption = "Edit Vehicles";
      mWeighbridgeDetails = DocumentController.DataContext.Document.GetExtensionObject<global::Weighbridge.Business.WeighbridgeDetails>();

      foreach (WeighbridgeDetailsVehicle wtv in mWeighbridgeDetails.Vehicles.Where(v => !BusinessObject.IsNull(v.Vehicle)))
      {
        if (!wtv.IsDeleted) DataContext.Vehicles.Add(wtv.Vehicle);
      }
      return base.OnRun();
    }

    protected override void ApplyChanges()
    {
      IList<Vehicle> vehicles = null;
      if (!BusinessObject.IsNull(DataContext.VehicleUnit)) vehicles = DataContext.VehicleUnit.Vehicles;
      else vehicles = DataContext.Vehicles;

      foreach (WeighbridgeDetailsVehicle wtv in mWeighbridgeDetails.Vehicles.Where(v => !BusinessObject.IsNull(v.Vehicle)))
      {
        if (!vehicles.Contains(wtv.Vehicle)) wtv.IsDeleted = true;
      }

      foreach (Vehicle v in vehicles)
      {
        WeighbridgeDetailsVehicle wtv = mWeighbridgeDetails.Vehicles.FirstOrDefault(wtv1 => wtv1.Vehicle.Equals(v));
        if (wtv == null) mWeighbridgeDetails.Vehicles.Add(new WeighbridgeDetailsVehicle(v));
      }

      base.ApplyChanges();
    }
  }
}

﻿using Afx.Prism;
using Afx.Prism.RibbonMdi;
using Afx.Prism.RibbonMdi.Documents;
using FleetManagement.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace Weighbridge.Prism.Extensions.WeighbridgeDetails.SetVehicleDialog
{
  public class SetVehicleDialogController : MdiDialogController<SetVehicleDialogContext, SetVehicleDialogViewModel>
  {
    public const string SetVehicleDialogControllerKey = "Weighbridge.Prism.Extensions.WeighbridgeDetails.SetVehicleDialog.SetVehicleDialogController";

    public SetVehicleDialogController(IController controller)
      : base(SetVehicleDialogControllerKey, controller)
    {
    }

    IMdiDocumentController mDocumentController;
    [Dependency]
    public IMdiDocumentController DocumentController
    {
      get { return mDocumentController; }
      set { mDocumentController = value; }
    }

    global::Weighbridge.Business.WeighbridgeDetails mWeighbridgeDetails;

    #region VehicleUnit SelectedVehicleUnit

    public VehicleUnit SelectedVehicleUnit
    {
      get { return ViewModel.SelectedVehicleUnit; }
      set { ViewModel.SelectedVehicleUnit = value; }
    }

    #endregion

    protected override bool OnRun()
    {
      if (DataContext == null) DataContext = new SetVehicleDialogContext();
      ViewModel.Caption = "Set Vehicle";
      mWeighbridgeDetails = DocumentController.DataContext.Document.GetExtensionObject<global::Weighbridge.Business.WeighbridgeDetails>();
      SelectedVehicleUnit = mWeighbridgeDetails.VehicleUnit ?? new VehicleUnit(true);

      return base.OnRun();
    }

    protected override void ApplyChanges()
    {
      mWeighbridgeDetails.VehicleUnit = SelectedVehicleUnit;
      base.ApplyChanges();
    }
  }
}

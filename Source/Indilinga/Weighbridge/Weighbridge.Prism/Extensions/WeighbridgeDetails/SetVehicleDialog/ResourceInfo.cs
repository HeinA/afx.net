﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Weighbridge.Prism.Extensions.WeighbridgeDetails.SetVehicleDialog
{
  [Export(typeof(Afx.Prism.ResourceInfo))]
  public class ResourceInfo : Afx.Prism.ResourceInfo
  {
    public override Uri GetUri()
    {
      return new Uri("pack://application:,,,/Weighbridge.Prism;component/Extensions/WeighbridgeDetails/SetVehicleDialog/Resources.xaml", UriKind.Absolute);
    }
  }
}
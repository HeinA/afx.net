﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Weighbridge.Prism.Extensions.WeighbridgeDetails
{
  public class Operations
  {
    public const string SetVehicle = "{d03a113b-607e-4b03-b616-9ce1b951a891}";
    public const string SetDefaultWeight = "{9e958a90-97f0-4c7f-8bd6-65d9b8aa7574}";
    public const string AddWeight = "{f94a82e0-0907-4c82-a575-87bb12679eba}";
  }
}

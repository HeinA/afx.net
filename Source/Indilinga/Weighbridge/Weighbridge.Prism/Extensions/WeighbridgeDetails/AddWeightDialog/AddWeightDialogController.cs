﻿using Afx.Business;
using Afx.Business.Data;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using Afx.Prism.RibbonMdi.Documents;
using FleetManagement.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;
using Weighbridge.Business;

namespace Weighbridge.Prism.Extensions.WeighbridgeDetails.AddWeightDialog
{
  public class AddWeightDialogController : MdiDialogController<AddWeightDialogContext, AddWeightDialogViewModel>
  {
    public const string AddWeightDialogControllerKey = "Weighbridge.Prism.Extensions.WeighbridgeDetails.AddWeightDialog.AddWeightDialogController";

    public AddWeightDialogController(IController controller)
      : base(AddWeightDialogControllerKey, controller)
    {
    }

    IMdiDocumentController mDocumentController;
    [Dependency]
    public IMdiDocumentController DocumentController
    {
      get { return mDocumentController; }
      set { mDocumentController = value; }
    }

    global::Weighbridge.Business.WeighbridgeDetails mWeighbridgeDetails;

    protected override bool OnRun()
    {
      if (DataContext == null) DataContext = new AddWeightDialogContext();
      ViewModel.Caption = "Add Weight";
      mWeighbridgeDetails = DocumentController.DataContext.Document.GetExtensionObject<global::Weighbridge.Business.WeighbridgeDetails>();

      WeightCategory emptyWeight = Cache.Instance.GetObjects<WeightCategory>(wc => wc.IsTare).FirstOrDefault();
      WeightCategory finalWeight = Cache.Instance.GetObjects<WeightCategory>(wc => wc.IsFinal).FirstOrDefault();

      Weighbridge.Business.WeighbridgeDetailsWeight w = mWeighbridgeDetails.Weights.FirstOrDefault(w1 => w1.WeightCategory.Equals(emptyWeight));
      if (w == null || w.Weight == 0)
      {
        DataContext.WeightCategory = emptyWeight;
      }
      else
      {
        DataContext.WeightCategory = finalWeight;
      }

      return base.OnRun();
    }

    protected override void OnRunning()
    {
      try
      {
        WeighbridgeSetting ws = Setting.GetSetting<WeighbridgeSetting>();
        if (ws.IsEnabled)
        {
          mSerialPort = new SerialPort(ws.ComPort, ws.Baud, ws.Parity, ws.DataBits, ws.StopBits);

          mSerialPort.DataReceived += SerialPort_DataReceived;
          if (!mSerialPort.IsOpen)
          {
            mSerialPort.Open();
          }
        }
      }
      catch (Exception ex)
      {
        if (ExceptionHelper.HandleException(ex)) throw ex;
      }

      base.OnRunning();
    }

    #region Scale

    SerialPort mSerialPort;
    string mData = string.Empty;
    bool bReadingWeight = false;

    private void SerialPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
    {
      ProcessInput(mSerialPort.ReadExisting());
    }

    void ProcessInput(string data)
    {
      try
      {
        foreach (char c in data)
        {
          if (c == 0x53 || c == 0x2B) // S or +
          {
            UpdateWeight();
            bReadingWeight = true;
          }
          else
          {
            if (bReadingWeight && c >= 48 && c <= 57) // 0-9
            {
              mData += c;
            }
            else
            {
              bReadingWeight = false;
            }
          }
        }
      }
      catch (Exception ex)
      {
        if (ExceptionHelper.HandleException(ex)) throw ex;
      }
    }

    void UpdateWeight()
    {
      if (!string.IsNullOrEmpty(mData))
      {
        DataContext.CurrentWeight = decimal.Parse(mData);
        mData = string.Empty;
      }
    }

    #endregion

    protected override void ApplyChanges()
    {
      WeighbridgeDetailsWeight w = null;
      WeighbridgeDetailsWeight existingWeight = mWeighbridgeDetails.Weights.FirstOrDefault(w1 => w1.WeightCategory.Equals(DataContext.WeightCategory));
      if (DataContext.WeightCategory.AllowMultiple || existingWeight == null)
      {
        w = new WeighbridgeDetailsWeight();
        mWeighbridgeDetails.Weights.Add(w);
        w.WeightCategory = DataContext.WeightCategory;
      }
      else
      {
        w = existingWeight;
        foreach (var wi in existingWeight.Items)
        {
          wi.IsDeleted = true;
        }
      }
      foreach (var wi in DataContext.Items)
      {
        w.Items.Add(wi);
      }

      base.ApplyChanges();
    }

    protected override void OnTerminated()
    {
      if (mSerialPort != null) mSerialPort.Close();
      base.OnTerminated();
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing)
      {
        if (mSerialPort != null) mSerialPort.Dispose();
      }

      base.Dispose(disposing);
    }
  }
}

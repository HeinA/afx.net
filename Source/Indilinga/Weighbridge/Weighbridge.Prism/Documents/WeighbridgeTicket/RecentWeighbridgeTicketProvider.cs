﻿using Afx.Business;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Documents;
using Afx.Business.Security;
using Afx.Business.Service;
using Afx.Prism.RibbonMdi.Documents;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Weighbridge.Business;

namespace Weighbridge.Prism.Documents.WeighbridgeTicket
{
  [Export(typeof(IRecentDocumentsProvider))]
  public class RecentWeighbridgeTicketProvider : IRecentDocumentsProvider
  {
    public Guid DocumentType
    {
      get { return new Guid(Weighbridge.Business.WeighbridgeTicket.DocumentTypeIdentifier); }
    }

    SearchResults GetResults()
    {
      Afx.Business.Documents.DocumentType dt = Cache.Instance.GetObject<Afx.Business.Documents.DocumentType>(DocumentType);
      Collection<string> filter = new Collection<string>();
      foreach (var dts in dt.States.Where(dts1 => dts1.IsInRecentList))
      {
        filter.Add(dts.Name);
      }

      using (var svc = ServiceFactory.GetService<IAfxService>(SecurityContext.ServerName))
      {
        SearchDatagraph sg = new SearchDatagraph(typeof(Weighbridge.Business.WeighbridgeTicket))
          .Select(Document.DocumentDateProperty, "Date", "dd MMM yyyy")
          .Select(Document.DocumentNumberProperty, "Document")
          .Join(Document.StateProperty)
            .Join(DocumentState.DocumentTypeStateProperty)
              .Select(DocumentTypeState.NameProperty, "State")
              .Where(DocumentTypeState.NameProperty, FilterType.In, filter)
              .EndJoin()
            .EndJoin()
          .Join(Document.OrganizationalUnitProperty)
            .Select(OrganizationalUnit.NameProperty, "Organizational Unit")
            .EndJoin()
          .Select(Weighbridge.Business.WeighbridgeTicket.VehicleReferenceProperty, "Vehicle Reference");

        return svc.Instance.Search(sg);
      }
    }

    public Collection<RecentDocumentBase> GetRecentDocuments()
    {
      Collection<RecentDocumentBase> col = new Collection<RecentDocumentBase>();
      try
      {
        foreach (SearchResult sr in GetResults().Results)
        {
          col.Add(new RecentWeighbridgeTicket(this) { GlobalIdentifier = sr.GlobalIdentifier, DocumentDate = (DateTime)sr["Date"], DocumentNumber = (string)sr["Document"], DocumentState = (string)sr["State"], OrganizationalUnit = (string)sr["Organizational Unit"], VehicleReference = (string)sr["Vehicle Reference"] });
        }
      }
      catch
      {
        throw;
      }
      return col;
    }

    public Afx.Prism.IViewModel GetDetailViewModel(IBusinessObject model, Afx.Prism.IViewModelBase parent)
    {
      return parent.Controller.GetCreateViewModel<RecentWeighbridgeTicketDetailViewModel>(model, parent);
    }
  }
}

﻿using AccountManagement.Business;
using Afx.Business;
using Afx.Business.Data;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfControls.Editors;

namespace Weighbridge.Prism.Documents.WeighbridgeTicket
{
  public class OutgoingProvider : ISuggestionProvider
  {
    IEnumerable<AccountReference> mAccounts;

    public IEnumerable GetSuggestions(string filter)
    {
      if (mAccounts == null) mAccounts = Cache.Instance.GetObjects<AccountReference>(ar => ar.IsDebtor || !BusinessObject.IsNull(ar.RepresentativeOrganizationalUnit), ar => ar.Name, true);
      return mAccounts.Where(a => a.Name.ToUpperInvariant().Contains(filter.ToUpperInvariant()) || a.AccountNumber.ToUpperInvariant().Contains(filter.ToUpperInvariant()));
    }
  }
}

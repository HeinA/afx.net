﻿using Afx.Business;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Documents;
using Weighbridge.Business;
using Afx.Business.Security;
using Afx.Business.Service;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.Composition;
using Afx.Prism.RibbonMdi;
using Afx.Prism.RibbonMdi.Events;

namespace Weighbridge.Prism.Documents.WeighbridgeTicket
{
  //**************************************************************************************************************
  //**************************************************************************************************************
  //*                                                                                                            *
  //*     This code is auto-generated.  Do not place any cutom code here, it will be lost on re-generation.      *
  //*                                                                                                            *
  //**************************************************************************************************************
  //**************************************************************************************************************
  [Export("Controller:" + Weighbridge.Business.WeighbridgeTicket.DocumentTypeIdentifier)]
  public partial class WeighbridgeTicketController
  {
    protected void SetDocumentState(string stateIdentifier)
    {
      if (!DataContext.Document.Validate()) throw new MessageException("The document has validation errors.");

      using (var svc = ProxyFactory.GetService<IAfxService>(SecurityContext.ServerName))
      {
        DataContext.Document = (Weighbridge.Business.WeighbridgeTicket)svc.SetDocumentState(DataContext.Document, stateIdentifier);
        MdiApplicationController.Current.EventAggregator.GetEvent<DocumentSavedEvent>().Publish(new DocumentSavedEventArgs(this.DataContext.Document));
      }
    }
  }
}


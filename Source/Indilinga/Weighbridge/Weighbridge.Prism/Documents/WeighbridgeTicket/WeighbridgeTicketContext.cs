﻿using Afx.Business.Security;
using Afx.Business.Service;
using Afx.Prism.RibbonMdi.Documents;
using Weighbridge.Business;
using Weighbridge.Business.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Weighbridge.Prism.Documents.WeighbridgeTicket
{
  public partial class WeighbridgeTicketContext : DocumentContext<Weighbridge.Business.WeighbridgeTicket>
  {
    public WeighbridgeTicketContext()
      : base(Weighbridge.Business.WeighbridgeTicket.DocumentTypeIdentifier)
    {
    }

    public WeighbridgeTicketContext(Guid documentIdentifier)
      : base(Weighbridge.Business.WeighbridgeTicket.DocumentTypeIdentifier, documentIdentifier)
    {
    }

    public override void SaveData()
    {
      using (var svc = ProxyFactory.GetService<IWeighbridgeService>(SecurityContext.ServerName))
      {
        Document = svc.SaveWeighbridgeTicket(Document);
      }
      base.SaveData();
    }

    public override void LoadData()
    {
      using (var svc = ProxyFactory.GetService<IWeighbridgeService>(SecurityContext.ServerName))
      {
        Document = svc.LoadWeighbridgeTicket(DocumentIdentifier);
      }
      base.LoadData();
    }
  }
}


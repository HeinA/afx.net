﻿using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Documents;
using InventoryControl.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Weighbridge.Business;

namespace Weighbridge.Prism.Documents.WeighbridgeTicket
{
  public class WeighbridgeTicketInventoryItemViewModel : ExtensionViewModel<WeighbridgeTicketInventoryItem>
  {
    #region Constructors

    [InjectionConstructor]
    public WeighbridgeTicketInventoryItemViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    #region InventoryItem InventoryItem

    public InventoryItem InventoryItem
    {
      get { return Model == null ? GetDefaultValue<InventoryItem>() : Model.InventoryItem; }
      set { Model.InventoryItem = value; }
    }

    #endregion

    IEnumerable<InventoryItem> mItems;
    public IEnumerable<InventoryItem> Items
    {
      get { return mItems ?? (mItems = Cache.Instance.GetObjects<InventoryItem>(true, ii => ii.ApplicableOrganizationalUnits.Contains(SecurityContext.OrganizationalUnit), ii => ii.Description, true)); }
    }
  }
}

﻿using Afx.Business;
using Afx.Business.Collections;
using Afx.Business.Validation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Weighbridge.Business;

namespace Weighbridge.Prism.Documents.WeighbridgeTicket.AddWeightDialog
{
  public class AddWeightDialogContext : BusinessObject
  {
    #region decimal CurrentWeight

    decimal mCurrentWeight;
    public decimal CurrentWeight
    {
      get { return mCurrentWeight; }
      set { SetProperty<decimal>(ref mCurrentWeight, value); }
    }

    #endregion

    #region BusinessObjectCollection<WeighbridgeTicketWeightItem> Items

    public const string ItemsProperty = "Items";
    BusinessObjectCollection<WeighbridgeTicketWeightItem> mItems;
    public BusinessObjectCollection<WeighbridgeTicketWeightItem> Items
    {
      get { return mItems ?? (mItems = new BusinessObjectCollection<WeighbridgeTicketWeightItem>(this)); }
    }

    #endregion

    #region decimal TotalWeight

    decimal mTotalWeight;
    [Mandatory("Weight must be greather than zero.")]
    public decimal TotalWeight
    {
      get { return mTotalWeight; }
      set { SetProperty<decimal>(ref mTotalWeight, value); }
    }

    #endregion

    #region WeightCategory WeightCategory

    WeightCategory mWeightCategory;
    [Mandatory("Weight category is a mandatory field.")]
    public WeightCategory WeightCategory
    {
      get { return mWeightCategory; }
      set { SetProperty<WeightCategory>(ref mWeightCategory, value); }
    }

    #endregion

    #region void OnCompositionChanged(...)

    protected override void OnCompositionChanged(CompositionChangedType compositionChangedType, string propertyName, object originalSource, BusinessObject item)
    {
      if (propertyName == ItemsProperty)
      {
        decimal tw = 0;
        foreach (WeighbridgeTicketWeightItem i in Items)
        {
          tw += i.Weight;
        }
        TotalWeight = tw;
      }
      base.OnCompositionChanged(compositionChangedType, propertyName, originalSource, item);
    }

    #endregion
  }
}
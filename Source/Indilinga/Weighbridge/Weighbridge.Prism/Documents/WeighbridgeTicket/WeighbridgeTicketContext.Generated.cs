﻿using Afx.Business.Security;
using Afx.Business.Service;
using Afx.Prism.RibbonMdi.Documents;
using Weighbridge.Business;
using Weighbridge.Business.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.Composition;

namespace Weighbridge.Prism.Documents.WeighbridgeTicket
{
  [Export("Context:" + Weighbridge.Business.WeighbridgeTicket.DocumentTypeIdentifier)]
  public partial class WeighbridgeTicketContext
  {
  }
}


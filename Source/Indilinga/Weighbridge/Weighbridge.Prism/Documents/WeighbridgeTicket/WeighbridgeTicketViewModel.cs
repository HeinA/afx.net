﻿using Afx.Business;
using Afx.Business.Activities;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Documents;
using Weighbridge.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows;
using Afx.Business.Data;
using Afx.Business.Collections;
using FleetManagement.Business;
using InventoryControl.Business;

namespace Weighbridge.Prism.Documents.WeighbridgeTicket
{
  public partial class WeighbridgeTicketViewModel : MdiDocumentViewModel<WeighbridgeTicketContext>
  {
    [InjectionConstructor]
    public WeighbridgeTicketViewModel(IController controller)
      : base(controller)
    {
    }

    //#region string VehicleReference

    //public string VehicleReference
    //{
    //  get { return Model == null ? GetDefaultValue<string>() : Model.Document == null ? GetDefaultValue<string>() : Model.Document.VehicleReference; }
    //  set { Model.Document.VehicleReference = value; }
    //}

    //#endregion

    //#region bool IsVehicleReferenceReadOnly

    //public bool IsVehicleReferenceReadOnly
    //{
    //  get { return !BusinessObject.IsNull(Model.Document.VehicleUnit); }
    //}

    //#endregion

    //#region WeighbridgeDirection Direction

    //public WeighbridgeDirection Direction
    //{
    //  get { return Model == null ? GetDefaultValue<WeighbridgeDirection>() : Model.Document == null ? GetDefaultValue<WeighbridgeDirection>() : Model.Document.Direction; }
    //  set { Model.Document.Direction = value; }
    //}

    //#endregion

    //#region WeighbridgeIncomingSource IncomingSource

    //public Visibility IncomingSourceVisibility
    //{
    //  get
    //  {
    //    if (Direction == WeighbridgeDirection.Incoming) return Visibility.Visible;
    //    return Visibility.Collapsed;
    //  }
    //}

    //IEnumerable<WeighbridgeIncomingSource> mIncomingSources;
    //public IEnumerable<WeighbridgeIncomingSource> IncomingSources
    //{
    //  get { return mIncomingSources ?? (mIncomingSources = Cache.Instance.GetObjects<WeighbridgeIncomingSource>(true, ics => ics.Text, true)); }
    //}

    //public WeighbridgeIncomingSource IncomingSource
    //{
    //  get { return Model == null ? GetDefaultValue<WeighbridgeIncomingSource>() : Model.Document == null ? GetDefaultValue<WeighbridgeIncomingSource>() : Model.Document.IncomingSource; }
    //  set { Model.Document.IncomingSource = value; }
    //}

    //#endregion


    //#region void OnModelPropertyChanged(...)

    //protected override void OnModelPropertyChanged(string propertyName)
    //{
    //  switch (propertyName)
    //  {
    //    case Weighbridge.Business.WeighbridgeTicket.VehicleUnitProperty:
    //      {
    //        OnPropertyChanged("IsVehicleReferenceReadOnly");
    //      }
    //      break;

    //    case Weighbridge.Business.WeighbridgeTicket.DirectionProperty:
    //      {
    //        OnPropertyChanged("IncomingSourceVisibility");
    //      }
    //      break;
    //  }
    //  base.OnModelPropertyChanged(propertyName);
    //}

    //#endregion

    //#region IEnumerable<WeighbridgeTicketWeight> Weights

    //BasicCollection<Weighbridge.Business.WeightCategory> mWeightCategoryCollection;
    //public IEnumerable<Weighbridge.Business.WeightCategory> WeightCategoryCollection
    //{
    //  get { return mWeightCategoryCollection ?? (mWeightCategoryCollection = Cache.Instance.GetObjects<Weighbridge.Business.WeightCategory>(wc => wc.Description, true)); }
    //}

    //public IEnumerable<WeighbridgeTicketWeight> Weights
    //{
    //  get { return Model == null ? GetDefaultValue<IEnumerable<WeighbridgeTicketWeight>>() : Model.Document == null ? GetDefaultValue<IEnumerable<WeighbridgeTicketWeight>>() : Model.Document.Weights; }
    //}

    //#endregion

    //#region IEnumerable<WeighbridgeTicketVehicle> Vehicles

    //BasicCollection<Vehicle> mVehicleCollection;
    //public IEnumerable<Vehicle> VehicleCollection
    //{
    //  get { return mVehicleCollection ?? (mVehicleCollection = Cache.Instance.GetObjects<Vehicle>(true, v => v.Text, true)); }
    //}

    //public IEnumerable<WeighbridgeTicketVehicle> Vehicles
    //{
    //  get { return Model == null ? null : Model.Document == null ? null : Model.Document.Vehicles; }
    //}

    //#endregion

    //#region IEnumerable<InventoryItem> Items

    //public IEnumerable<InventoryItem> Items
    //{
    //  get { return Model.Document.Items; }
    //}

    //#endregion

  }
}


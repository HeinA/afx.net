﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Weighbridge.Prism.Documents.WeighbridgeTicket.SetVehicleDialog
{
  [Export(typeof(Afx.Prism.ResourceInfo))]
  public class ResourceInfo : Afx.Prism.ResourceInfo
  {
    public override Uri GetUri()
    {
      return new Uri("pack://application:,,,/Weighbridge.Prism;component/Documents/WeighbridgeTicket/SetVehicleDialog/Resources.xaml", UriKind.Absolute);
    }
  }
}
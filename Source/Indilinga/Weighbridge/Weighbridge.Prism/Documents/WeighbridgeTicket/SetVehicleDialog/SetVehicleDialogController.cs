﻿using Afx.Prism;
using Afx.Prism.RibbonMdi;
using FleetManagement.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace Weighbridge.Prism.Documents.WeighbridgeTicket.SetVehicleDialog
{
  public class SetVehicleDialogController : MdiDialogController<SetVehicleDialogContext, SetVehicleDialogViewModel>
  {
    public const string SetVehicleDialogControllerKey = "Weighbridge.Prism.Documents.WeighbridgeTicket.SetVehicleDialog.SetVehicleDialogController";

    public SetVehicleDialogController(IController controller)
      : base(SetVehicleDialogControllerKey, controller)
    {
    }

    WeighbridgeTicketController mDocumentController;
    [Dependency]
    public WeighbridgeTicketController DocumentController
    {
      get { return mDocumentController; }
      set { mDocumentController = value; }
    }

    Weighbridge.Business.WeighbridgeTicket Document
    {
      get { return DocumentController.DataContext.Document; }
    }

    #region VehicleUnit SelectedVehicleUnit

    public VehicleUnit SelectedVehicleUnit
    {
      get { return ViewModel.SelectedVehicleUnit; }
      set { ViewModel.SelectedVehicleUnit = value; }
    }

    #endregion

    protected override bool OnRun()
    {
      if (DataContext == null) DataContext = new SetVehicleDialogContext();
      ViewModel.Caption = "Set Vehicle";
      SelectedVehicleUnit = Document.VehicleUnit ?? new VehicleUnit(true);

      return base.OnRun();
    }

    protected override void ApplyChanges()
    {
      Document.VehicleUnit = SelectedVehicleUnit;
      base.ApplyChanges();
    }
  }
}

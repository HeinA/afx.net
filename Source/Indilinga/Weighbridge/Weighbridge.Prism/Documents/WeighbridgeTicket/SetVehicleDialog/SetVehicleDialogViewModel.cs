﻿using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using FleetManagement.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Weighbridge.Prism.Documents.WeighbridgeTicket.SetVehicleDialog
{
  public class SetVehicleDialogViewModel : MdiDialogViewModel<SetVehicleDialogContext>
  {
    [InjectionConstructor]
    public SetVehicleDialogViewModel(IController controller)
      : base(controller)
    {
    }

    #region IEnumerable<VehicleUnit> VehicleUnits

    BasicCollection<VehicleUnit> mVehicleUnits;
    public IEnumerable<VehicleUnit> VehicleUnits
    {
      get
      {
        return mVehicleUnits ?? (mVehicleUnits = Cache.Instance.GetObjects<VehicleUnit>(true, v => v.ApplicableOrganizationalUnits.Contains(SecurityContext.OrganizationalUnit), v => v.FleetUnitNumber, true));
      }
    }

    #endregion

    #region VehicleUnit SelectedVehicleUnit

    VehicleUnit mSelectedVehicleUnit;
    public VehicleUnit SelectedVehicleUnit
    {
      get { return mSelectedVehicleUnit; }
      set { SetProperty<VehicleUnit>(ref mSelectedVehicleUnit, value); }
    }

    #endregion
  }
}

﻿using Afx.Prism;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Weighbridge.Prism.Documents.WeighbridgeTicket
{
  public class RecentWeighbridgeTicketDetailViewModel : ViewModel<RecentWeighbridgeTicket>
  {
    [InjectionConstructor]
    public RecentWeighbridgeTicketDetailViewModel(IController controller)
      : base(controller)
    {
    }

    #region string DocumentNumber

    public string DocumentNumber
    {
      get { return Model == null ? GetDefaultValue<string>() : Model.DocumentNumber; }
      set { Model.DocumentNumber = value; }
    }

    #endregion

    #region DateTime DocumentDate

    public DateTime DocumentDate
    {
      get { return Model == null ? GetDefaultValue<DateTime>() : Model.DocumentDate; }
      set { Model.DocumentDate = value; }
    }

    #endregion

    #region string OrganizationalUnit

    public string OrganizationalUnit
    {
      get { return Model == null ? GetDefaultValue<string>() : Model.OrganizationalUnit; }
      set { Model.OrganizationalUnit = value; }
    }

    #endregion

    #region string DocumentState

    public string DocumentState
    {
      get { return Model == null ? GetDefaultValue<string>() : Model.DocumentState; }
      set { Model.DocumentState = value; }
    }

    #endregion

    #region string VehicleReference

    public string VehicleReference
    {
      get { return Model == null ? GetDefaultValue<string>() : Model.VehicleReference; }
      set { Model.VehicleReference = value; }
    }

    #endregion

  }
}

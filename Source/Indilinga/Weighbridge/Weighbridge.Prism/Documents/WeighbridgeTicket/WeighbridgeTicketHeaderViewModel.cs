﻿using AccountManagement.Business;
using AccountManagement.Business.Service;
using Afx.Business;
using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Business.Service;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Documents;
using ContactManagement.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Weighbridge.Business;

namespace Weighbridge.Prism.Documents.WeighbridgeTicket
{
  public class WeighbridgeTicketHeaderViewModel : DocumentHeaderViewModel<Weighbridge.Business.WeighbridgeTicket>
  {
    #region Constructors

    [InjectionConstructor]
    public WeighbridgeTicketHeaderViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    #region string VehicleReference

    public string VehicleReference
    {
      get { return Model == null ? GetDefaultValue<string>() : Model.VehicleReference; }
      set { Model.VehicleReference = value; }
    }

    #endregion

    #region bool IsVehicleReferenceReadOnly

    public bool IsVehicleReferenceReadOnly
    {
      get { return !BusinessObject.IsNull(Model.VehicleUnit); }
    }

    #endregion

    #region WeighbridgeDirection Direction

    public WeighbridgeDirection Direction
    {
      get { return Model == null ? GetDefaultValue<WeighbridgeDirection>() : Model.Direction; }
      set { Model.Direction = value; }
    }

    #endregion

    #region WeighbridgeIncomingSource IncomingSource

    public Visibility IncomingSourceVisibility
    {
      get
      {
        if (Direction == WeighbridgeDirection.Incoming) return Visibility.Visible;
        return Visibility.Collapsed;
      }
    }

    IEnumerable<WeighbridgeIncomingSource> mIncomingSources;
    public IEnumerable<WeighbridgeIncomingSource> IncomingSources
    {
      get { return mIncomingSources ?? (mIncomingSources = Cache.Instance.GetObjects<WeighbridgeIncomingSource>(true, ics => ics.Text, true)); }
    }

    public WeighbridgeIncomingSource IncomingSource
    {
      get { return Model == null ? GetDefaultValue<WeighbridgeIncomingSource>() :Model.IncomingSource; }
      set { Model.IncomingSource = value; }
    }

    #endregion

    #region Visibility OutgoingDestinationVisibility

    public Visibility OutgoingDestinationVisibility
    {
      get
      {
        if (Direction == WeighbridgeDirection.Outgoing) return Visibility.Visible;
        return Visibility.Collapsed;
      }
    }

    #endregion

    #region Visibility SelectDirectionVisibility

    public Visibility SelectDirectionVisibility
    {
      get
      {
        if (Direction == WeighbridgeDirection.None) return Visibility.Visible;
        return Visibility.Collapsed;
      }
    }

    #endregion

    #region AccountReference Account

    public AccountReference Account
    {
      get
      {
        if (Model == null) return null;
        if (BusinessObject.IsNull(Model.Account)) return null;
        if (mDrivers == null) LoadDrivers();
        return Model == null ? GetDefaultValue<AccountReference>() : Model.Account;
      }
      set
      {
        Model.Account = value;
        mDrivers = null;
        LoadDrivers();
      }
    }

    bool mIsLoading = false;
    private void LoadDrivers()
    {
      if (mIsLoading) return;
      if (!BusinessObject.IsNull(this.Model.Account))
      {
        if (Drivers != null) return;
        mIsLoading = true;
        Task.Run(() =>
        {
          if (BusinessObject.IsNull(this.Model.Account.RepresentativeOrganizationalUnit))
          {
            using (var svc = ProxyFactory.GetService<IAccountManagementService>(SecurityContext.ServerName))
            {
              Account a = svc.LoadAccount(Model.Account.GlobalIdentifier);
              mDrivers = Cache.Instance.GetObjects<Employee>(true, c => c.ContactType.IsFlagged(FleetManagement.Business.ContactTypeFlags.Driver), c => c.Fullname, true).Cast<Contact>().Union(a.Contacts.Where(c => c.ContactType.IsFlagged(FleetManagement.Business.ContactTypeFlags.Driver)).OrderBy(c => c.Fullname));
            }
          }
          else
          {
            mDrivers = Cache.Instance.GetObjects<Employee>(true, c => c.ContactType.IsFlagged(FleetManagement.Business.ContactTypeFlags.Driver), c => c.Fullname, true);
          }
          mIsLoading = false;
          OnPropertyChanged("Drivers");
        });
      }
      else
      {
        mDrivers = null;
        OnPropertyChanged("Drivers");
      }
    }

    #endregion

    #region IEnumerable<Contact> Drivers

    IEnumerable<Contact> mDrivers = null;
    public IEnumerable<Contact> Drivers
    {
      get { return mDrivers; }
    }

    #endregion

    #region void OnModelPropertyChanged(...)

    protected override void OnModelPropertyChanged(string propertyName)
    {
      switch (propertyName)
      {
        case Weighbridge.Business.WeighbridgeTicket.VehicleUnitProperty:
          {
            OnPropertyChanged("IsVehicleReferenceReadOnly");
          }
          break;

        case Weighbridge.Business.WeighbridgeTicket.DirectionProperty:
          {
            OnPropertyChanged("IncomingSourceVisibility");
            OnPropertyChanged("OutgoingDestinationVisibility");
            OnPropertyChanged("SelectDirectionVisibility");
          }
          break;
      }
      base.OnModelPropertyChanged(propertyName);
    }

    #endregion
  }
}

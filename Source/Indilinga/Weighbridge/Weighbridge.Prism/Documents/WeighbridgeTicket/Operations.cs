﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Weighbridge.Prism.Documents.WeighbridgeTicket
{
  public class Operations : Afx.Prism.RibbonMdi.StandardOperations
  {
      public const string SetVehicle = "{d03a113b-607e-4b03-b616-9ce1b951a891}";
      public const string AddWeight = "{f94a82e0-0907-4c82-a575-87bb12679eba}";
      public const string WeighOut = "{67221725-b1e3-403f-bf14-4867c65fde66}";
      public const string Cancel = "{3add1585-756b-4fb5-b3e5-94cb6001d828}";
  }
}
﻿using Afx.Prism;
using Afx.Prism.RibbonMdi.Documents;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Weighbridge.Business;

namespace Weighbridge.Prism.Documents.WeighbridgeTicket
{
  public class WeighbridgeTicketFooterViewModel : ExtensionViewModel<Weighbridge.Business.WeighbridgeTicket>
  {
    #region Constructors

    [InjectionConstructor]
    public WeighbridgeTicketFooterViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    protected override void OnModelCompositionChanged(Afx.Business.CompositionChangedEventArgs e)
    {
      OnPropertyChanged("TareWeight");
      OnPropertyChanged("GrossWeight");
      OnPropertyChanged("NetWeight");

      base.OnModelCompositionChanged(e);
    }

    public decimal TareWeight
    {
      get { return Model.TareWeight; }
    }

    public decimal GrossWeight
    {
      get { return Model.GrossWeight; }
    }

    public decimal NetWeight
    {
      get { return Model.NetWeight; }
    }
  }
}

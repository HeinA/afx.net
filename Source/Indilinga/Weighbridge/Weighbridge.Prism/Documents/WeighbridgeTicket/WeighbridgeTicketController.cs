﻿using Afx.Business;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Documents;
using Weighbridge.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Weighbridge.Prism.Documents.WeighbridgeTicket.SetVehicleDialog;
using Weighbridge.Prism.Documents.WeighbridgeTicket.AddWeightDialog;
using InventoryControl.Prism.Dialogs.EditInventoryDialog;
using System.Windows;

namespace Weighbridge.Prism.Documents.WeighbridgeTicket
{
  public partial class WeighbridgeTicketController : MdiDocumentController<WeighbridgeTicketContext, WeighbridgeTicketViewModel>
  {
    [InjectionConstructor]
    public WeighbridgeTicketController(IController controller)
      : base(controller)
    {
    }

    protected override void ExtendDocument()
    {
      ExtendDocument<WeighbridgeTicketHeaderViewModel>(HeaderRegion);
      ExtendDocument<DocumentReferencesViewModel>(TabRegion);

      ExtendDocument<WeighbridgeTicketContentViewModel>(DockRegion);
      ExtendDocument<WeighbridgeTicketFooterViewModel>(FooterRegion);

      ExtendDocument<WeighbridgeTicketInventoryItem, WeighbridgeTicketInventoryItemViewModel>(HeaderRegion);

      base.ExtendDocument();
    }

    protected override void ExecuteOperation(Operation op, BusinessObject argument)
    {
      switch (op.Identifier)
      {
        case Operations.SetVehicle:
          {
            SetVehicleDialogController c = GetCreateChildController<SetVehicleDialogController>(SetVehicleDialogController.SetVehicleDialogControllerKey);
            c.Run();
          }
          break;

        case Operations.AddWeight:
          {
            AddWeightDialogController c = GetCreateChildController<AddWeightDialogController>(AddWeightDialogController.AddWeightDialogControllerKey);
            c.Run();
          }
          break;

        case Operations.WeighOut:
          {
            SetDocumentState(Weighbridge.Business.WeighbridgeTicket.States.WeighedOut);
          }
          break;

        case Operations.Cancel:
          {
            if (MessageBox.Show("Are you sure you want to cancel the ticket", "Cancel", MessageBoxButton.YesNo, MessageBoxImage.Exclamation) == MessageBoxResult.Yes)
            {
              SetDocumentState(Weighbridge.Business.WeighbridgeTicket.States.Cancelled);
            }
          }
          break;
      }
      
      base.ExecuteOperation(op, argument);
    }
  }
}


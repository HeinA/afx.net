﻿using Afx.Business.Documents;
using Afx.Prism.RibbonMdi.Documents;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Weighbridge.Prism.Documents.WeighbridgeTicket
{
  public class RecentWeighbridgeTicket : RecentDocumentBase
  {
    public RecentWeighbridgeTicket(IRecentDocumentsProvider provider)
      : base(provider)
    {
    }

    #region string VehicleReference

    string mVehicleReference;
    public string VehicleReference
    {
      get { return mVehicleReference; }
      set { mVehicleReference = value; }
    }

    #endregion
  }
}

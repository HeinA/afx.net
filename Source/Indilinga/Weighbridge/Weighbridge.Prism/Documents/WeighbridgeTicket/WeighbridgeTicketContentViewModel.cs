﻿using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Documents;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Weighbridge.Business;

namespace Weighbridge.Prism.Documents.WeighbridgeTicket
{
  public class WeighbridgeTicketContentViewModel : ExtensionViewModel<Weighbridge.Business.WeighbridgeTicket>
  {
    #region Constructors

    [InjectionConstructor]
    public WeighbridgeTicketContentViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    #region IEnumerable<WeighbridgeTicketWeight> Weights

    BasicCollection<Weighbridge.Business.WeightCategory> mWeightCategoryCollection;
    public IEnumerable<Weighbridge.Business.WeightCategory> WeightCategoryCollection
    {
      get { return mWeightCategoryCollection ?? (mWeightCategoryCollection = Cache.Instance.GetObjects<Weighbridge.Business.WeightCategory>(wc => wc.Description, true)); }
    }

    public IEnumerable<WeighbridgeTicketWeight> Weights
    {
      get { return Model == null ? GetDefaultValue<IEnumerable<WeighbridgeTicketWeight>>() : Model.Weights; }
    }

    #endregion
  }
}

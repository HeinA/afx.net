﻿using Afx.Business.Documents;
using Afx.Prism;
using Afx.Prism.Documents;
using Afx.Prism.RibbonMdi.Documents;
using InventoryControl.Business;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Weighbridge.Business;

namespace Weighbridge.Prism.Reports.WeighbridgeTicket
{
  [Export(Weighbridge.Business.WeighbridgeTicket.DocumentTypeIdentifier, typeof(IDocumentPrintInfo))]
  class WeighbridgeTicketInfo : IDocumentPrintInfo
  {
    public int Priority
    {
      get { return 90; }
    }

    public Stream ReportStream(object argument)
    {
      return this.GetType().Assembly.GetManifestResourceStream("Weighbridge.Prism.Reports.WeighbridgeTicket.WeighbridgeTicket.rdlc"); 
    }

    public object GetScope(IController controller)
    {
      return null;
    }

    public void RefreshReportData(LocalReport report, Document document, object scope, object argument)
    {
      Weighbridge.Business.WeighbridgeTicket wt = (Weighbridge.Business.WeighbridgeTicket)document;

      ReportDataSource reportDataSource = new ReportDataSource();
      reportDataSource.Name = "Document";
      reportDataSource.Value = new object[] { new DocumentPrintViewModel(document) };
      report.DataSources.Add(reportDataSource);

      reportDataSource = new ReportDataSource();
      reportDataSource.Name = "WeighbridgeDetails";
      reportDataSource.Value = new object[] { new WeighbridgeTicketPrintViewModel(wt) };
      report.DataSources.Add(reportDataSource);

      reportDataSource = new ReportDataSource();
      reportDataSource.Name = "DocumentReference";
      Collection<DocumentReferencePrintViewModel> references = new Collection<DocumentReferencePrintViewModel>();
      foreach (DocumentReference r in document.References)
      {
        references.Add(new DocumentReferencePrintViewModel(r));
      }
      reportDataSource.Value = references;
      report.DataSources.Add(reportDataSource);

      reportDataSource = new ReportDataSource();
      reportDataSource.Name = "Weights";
      Collection<WeightPrintViewModel> weights = new Collection<WeightPrintViewModel>();
      foreach (WeighbridgeTicketWeight w in wt.Weights)
      {
        foreach (WeighbridgeTicketWeightItem wi in w.Items)
        {
          if (!wi.IsDeleted) weights.Add(new WeightPrintViewModel(wi));
        }
      }
      reportDataSource.Value = weights;
      report.DataSources.Add(reportDataSource);

      reportDataSource = new ReportDataSource();
      reportDataSource.Name = "Items";
      Collection<ItemPrintViewModel> items = new Collection<ItemPrintViewModel>();

      WeighbridgeTicketInventoryItem ii = document.GetExtensionObject<WeighbridgeTicketInventoryItem>();
      if (ii != null)
      {
        items.Add(new ItemPrintViewModel(ii.InventoryItem));
      }
      //foreach (InventoryItem i in wt.Items)
      //{
      //  if (!i.IsDeleted) items.Add(new ItemPrintViewModel(i));
      //}
      reportDataSource.Value = items;
      report.DataSources.Add(reportDataSource);
    }

    public short Copies
    {
      get { return 1; }
    }
  }
}

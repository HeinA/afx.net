﻿using InventoryControl.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeighbridgeIc.Prism.Reports.CustomerDeliveryNote
{
  public class ItemPrintViewModel
  {
    public ItemPrintViewModel(InventoryDocumentItem item)
    {
      Item = item;
    }

    #region InventoryDocumentItem Item

    InventoryDocumentItem mItem;
    InventoryDocumentItem Item
    {
      get { return mItem; }
      set { mItem = value; }
    }

    #endregion

    public string ItemDescription
    {
      get { return Item.Item.Description; }
    }

    public decimal Quantity
    {
      get { return Item.Quantity; }
    }

    public string UOM
    {
      get { return Item.QuantityUOM.Text; }
    }
  }
}

﻿using Afx.Prism.RibbonMdi;
using Weighbridge.Business;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Afx.Prism;

namespace Weighbridge.Prism
{
  [Export(typeof(IModuleInfo))]
  public class ModuleInfo : IModuleInfo
  {
    public string ModuleNamespace
    {
      get { return Namespace.Weighbridge; }
    }

    public string[] ModuleDependencies
    {
      get { return new string[] { Afx.Business.Namespace.Afx, FleetManagement.Business.Namespace.FleetManagement, InventoryControl.Business.Namespace.InventoryControl, ContactManagement.Business.Namespace.ContactManagement }; }
    }

    public Type ModuleController
    {
      get { return typeof(WeighbridgeModuleController); }
    }
  }
}
﻿using Afx.Business.Data;
using FleetManagement.Business;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using WpfControls.Editors;

namespace FleetManagement.Prism
{
  public class TruckProvider : ISuggestionProvider
  {
    IEnumerable<Vehicle> mVehicles = null;
    public IEnumerable GetSuggestions(string filter)
    {
      if (mVehicles == null) mVehicles = Cache.Instance.GetObjects<Vehicle>(v=> v.IsTruck, v => v.RegistrationNumber, true);
      return mVehicles.Where(v => v.FleetNumber.ToUpperInvariant().Contains(filter.ToUpperInvariant()) || v.RegistrationNumber.ToUpperInvariant().Contains(filter.ToUpperInvariant()));
    }
  }

  public class TrailerProvider : ISuggestionProvider
  {
    IEnumerable<Vehicle> mVehicles = null;
    public IEnumerable GetSuggestions(string filter)
    {
      if (mVehicles == null) mVehicles = Cache.Instance.GetObjects<Vehicle>(v => v.IsTrailer, v => v.RegistrationNumber, true);
      return mVehicles.Where(v => v.FleetNumber.ToUpperInvariant().Contains(filter.ToUpperInvariant()) || v.RegistrationNumber.ToUpperInvariant().Contains(filter.ToUpperInvariant()));
    }
  }
}

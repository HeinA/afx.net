﻿using Afx.Business.Security;
using Afx.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Afx.Prism.RibbonMdi.Activities;
using FleetManagement.Business;
using Microsoft.Practices.Prism;

namespace FleetManagement.Prism.Activities.OwnedVehicleManagement
{
  //**************************************************************************************************************
  //**************************************************************************************************************
  //*                                                                                                            *
  //*     This code is auto-generated.  Do not place any cutom code here, it will be lost on re-generation.      *
  //*                                                                                                            *
  //**************************************************************************************************************
  //**************************************************************************************************************
  public partial class OwnedVehicleManagementViewModel
  {
    public IEnumerable<OwnedVehicleItemViewModel> Items
    {
      get
      {
        if (Model == null) return null;
        return this.ResolveViewModels<OwnedVehicleItemViewModel, OwnedVehicle>(Model.Data);
      }
    }
  }
}

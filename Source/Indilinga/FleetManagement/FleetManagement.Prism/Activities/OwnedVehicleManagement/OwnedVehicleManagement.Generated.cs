﻿using Afx.Business.Activities;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Business.Service;
using FleetManagement.Business;
using FleetManagement.Business.Service;
using Afx.Prism.RibbonMdi.Activities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.Composition;
  
namespace FleetManagement.Prism.Activities.OwnedVehicleManagement
{
  //**************************************************************************************************************
  //**************************************************************************************************************
  //*                                                                                                            *
  //*     This code is auto-generated.  Do not place any cutom code here, it will be lost on re-generation.      *
  //*                                                                                                            *
  //**************************************************************************************************************
  //**************************************************************************************************************
  [Export("Context:" + OwnedVehicleManagement.Key)]
  public partial class OwnedVehicleManagement
  {
    public const string Key = "{ed9b02aa-1adf-4b98-8f78-d21cbd9ad8ec}";

    public override void LoadData()
    {
      using (var svc = ProxyFactory.GetService<IFleetManagementService>(SecurityContext.MasterServer.Name))
      {
        Data = svc.LoadOwnedVehicleCollection();
      }
      base.LoadData();
    }

    public override void SaveData()
    {
      using (var svc = ProxyFactory.GetService<IFleetManagementService>(SecurityContext.MasterServer.Name))
      {
        Data = svc.SaveOwnedVehicleCollection(Data);
      }
      base.SaveData();
    }
  }
}

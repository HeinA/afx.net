﻿using FleetManagement.Business;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.Composition;
using Microsoft.Practices.Prism;

namespace FleetManagement.Prism.Activities.OwnedVehicleManagement
{
  //**************************************************************************************************************
  //**************************************************************************************************************
  //*                                                                                                            *
  //*     This code is auto-generated.  Do not place any cutom code here, it will be lost on re-generation.      *
  //*                                                                                                            *
  //**************************************************************************************************************
  //**************************************************************************************************************
  [Export("Controller:" + global::FleetManagement.Prism.Activities.OwnedVehicleManagement.OwnedVehicleManagement.Key)]
  public partial class OwnedVehicleManagementController
  {
    void CreateRootItem()
    {
      OwnedVehicle o = new OwnedVehicle();
      DataContext.Data.Add(o);
      SelectContextViewModel(o);
      FocusViewModel<OwnedVehicleDetailViewModel>(o);
    }

    public void AddDetailsViewModel(object viewModel)
    {
      RegionManager.Regions[OwnedVehicleManagement.DetailsRegion].Add(viewModel);
    }

    public void AddTabViewModel(object viewModel)
    {
      IRegion tabRegion = RegionManager.Regions[OwnedVehicleManagement.TabRegion];
      tabRegion.Add(viewModel);
      IActiveAware aw = viewModel as IActiveAware;
      if (aw != null && aw.IsActive) tabRegion.Activate(viewModel);
    }
  }
}

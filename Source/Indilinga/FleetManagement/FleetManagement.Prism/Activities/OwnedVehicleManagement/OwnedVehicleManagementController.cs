﻿using Afx.Business;
using Afx.Business.Activities;
using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Business.Service;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using Afx.Prism.RibbonMdi.Activities;
using Afx.Prism.RibbonMdi.Dialogs.Import;
using FleetManagement.Business;
using FleetManagement.Business.Service;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Unity;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FleetManagement.Prism.Activities.OwnedVehicleManagement
{
  public partial class OwnedVehicleManagementController : MdiContextualActivityController<OwnedVehicleManagement, OwnedVehicleManagementViewModel>, IImportController
  {
    #region Constructors

    [InjectionConstructor]
    public OwnedVehicleManagementController(IController controller)
      : base(controller)
    {
    }

    #endregion

    #region OnLoaded
    
    protected override void OnLoaded()
    {
      if (DataContext.Data.Count > 0)
      {
        //Focus the first element 
        OwnedVehicleItemViewModel ivm = GetCreateViewModel<OwnedVehicleItemViewModel>(DataContext.Data[0], ViewModel);
        ivm.IsFocused = true;
      }

      RegisterContextRegion(RegionManager.Regions[OwnedVehicleManagement.DetailsRegion]);
      RegisterContextRegion(RegionManager.Regions[OwnedVehicleManagement.TabRegion]);

      base.OnLoaded();
    }

    #endregion
    
    #region OnContextViewModelChanged
    
    protected override void OnContextViewModelChanged(ISelectableViewModel context)
    {
      IRegion tabRegion = RegionManager.Regions[OwnedVehicleManagement.TabRegion];

      if (context.Model is OwnedVehicle)
      {
        OwnedVehicleDetailViewModel vm = GetCreateViewModel<OwnedVehicleDetailViewModel>(context.Model, ViewModel);
        AddDetailsViewModel(vm);

        OwnedVehicleReferencesViewModel rvm = GetCreateViewModel<OwnedVehicleReferencesViewModel>(context.Model, ViewModel);
        AddTabViewModel(rvm);
      }

      base.OnContextViewModelChanged(context);
    }

    #endregion
    
    #region ExecuteOperation
    
    protected override void ExecuteOperation(Operation op, BusinessObject argument)
    {
      switch (op.Identifier)
      {
        case Operations.AddVehicle:
          CreateRootItem();
          break;

        case Operations.Import:
          OpenFileDialog ofd = new OpenFileDialog();
          ofd.Filter = "CSV Files|*.csv";
          if (ofd.ShowDialog() != true) return;
          using (var svc = ProxyFactory.GetService<IFleetManagementService>(SecurityContext.ServerName))
          {
            DataContext.Data = svc.ImportVehiclesCsv(File.ReadAllBytes(ofd.FileName));
            DataContext.IsDirty = false;
            DataContextUpdated();
          }
          //ImportDialogController ic = CreateChildController<ImportDialogController>();
          //ic.ImportController = this;
          //ic.Run();
          break;
      }

      base.ExecuteOperation(op, argument);
    }
    
    #endregion

    public void DoImport(ExternalSystem system, string identifier)
    {
      try
      {
        using (var svc = ProxyFactory.GetService<IFleetManagementService>(SecurityContext.MasterServer.Name))
        {
          DataContext.Data = svc.ImportVehicles(system);
          DataContextUpdated();
          DataContext.IsDirty = false;
        }
      }
      catch (Exception ex)
      {
        if (ExceptionHelper.HandleException(ex)) throw;
      }
    }
  }
}

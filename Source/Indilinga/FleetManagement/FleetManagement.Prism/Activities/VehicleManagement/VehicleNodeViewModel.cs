﻿using Afx.Business;
using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Prism;
using FleetManagement.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FleetManagement.Prism.Activities.VehicleManagement
{
  public partial class VehicleNodeViewModel
  {
    #region Constructors

    [InjectionConstructor]
    public VehicleNodeViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion
  }
}

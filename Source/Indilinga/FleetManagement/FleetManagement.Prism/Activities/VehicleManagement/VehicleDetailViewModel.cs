﻿using Afx.Business;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Activities;
using FleetManagement.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FleetManagement.Prism.Activities.VehicleManagement
{
  public partial class VehicleDetailViewModel : ActivityDetailViewModel<Vehicle>
  {
    [InjectionConstructor]
    public VehicleDetailViewModel(IController controller)
      : base(controller)
    {
    }

    #region string FleetNumber

    public string FleetNumber
    {
      get { return Model == null ? GetDefaultValue<string>() : Model.FleetNumber; }
      set { Model.FleetNumber = value; }
    }

    #endregion

    #region string RegistrationNumber

    public string RegistrationNumber
    {
      get
      {
        if (Model == null) return default(string);
        return Model.RegistrationNumber;
      }
      set { Model.RegistrationNumber = value; }
    }

    #endregion
      
    #region string Make

    public string Make
    {
      get
      {
        if (Model == null) return GetDefaultValue<string>();
        return Model.Make;
      }
      set { Model.Make = value; }
    }

    #endregion

    #region string VehicleModel

    public string VehicleModel
    {
      get
      {
        if (Model == null) return GetDefaultValue<string>();
        return Model.Model;
      }
      set { Model.Model = value; }
    }

    #endregion

    #region string Vin

    public string Vin
    {
      get
      {
        if (Model == null) return GetDefaultValue<string>();
        return Model.Vin;
      }
      set { Model.Vin = value; }
    }

    #endregion

    #region bool IsTruck

    public bool IsTruck
    {
      get { return Model == null ? GetDefaultValue<bool>() : Model.IsTruck; }
      set { Model.IsTruck = value; }
    }

    #endregion

    #region bool IsTrailer

    public bool IsTrailer
    {
      get { return Model == null ? GetDefaultValue<bool>() : Model.IsTrailer; }
      set { Model.IsTrailer = value; }
    }

    #endregion


    #region bool IsRefrigerated

    public bool IsRefrigerated
    {
      get { return Model == null ? GetDefaultValue<bool>() : Model.IsRefrigerated; }
      set { Model.IsRefrigerated = value; Model.HasGenerator = value; }
    }

    #endregion

    #region bool HasGenerator

    public bool HasGenerator
    {
      get { return Model == null ? GetDefaultValue<bool>() : Model.HasGenerator; }
      set { Model.HasGenerator = value; }
    }

    #endregion


    #region decimal VehicleWeight

    public decimal VehicleWeight
    {
      get { return Model == null ? GetDefaultValue<decimal>() : Model.VehicleWeight; }
      set { Model.VehicleWeight = value; }
    }

    #endregion

    #region decimal MaxLoadWeight

    public decimal MaxLoadWeight
    {
      get { return Model == null ? GetDefaultValue<decimal>() : Model.MaxLoadWeight; }
      set { Model.MaxLoadWeight = value; }
    }

    #endregion

    #region string Notes

    public string Notes
    {
      get { return Model == null ? GetDefaultValue<string>() : Model.Notes; }
      set { Model.Notes = value; }
    }

    #endregion

    #region void OnModelPropertyChanged(...)

    protected override void OnModelPropertyChanged(string propertyName)
    {
      switch (propertyName)
      {
        case Vehicle.ModelProperty:
          OnPropertyChanged("VehicleModel");
          break;
      }

      base.OnModelPropertyChanged(propertyName);
    }

    #endregion
  }
}

﻿using Afx.Business;
using Afx.Business.Activities;
using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Business.Service;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using Afx.Prism.RibbonMdi.Activities;
using Afx.Prism.RibbonMdi.Dialogs.Import;
using FleetManagement.Business;
using FleetManagement.Business.Service;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FleetManagement.Prism.Activities.VehicleManagement
{
  public partial class VehicleManagementController : MdiContextualActivityController<VehicleManagement, VehicleManagementViewModel>
  {
    #region Constructors

    [InjectionConstructor]
    public VehicleManagementController(IController controller)
      : base(controller)
    {
    }

    #endregion

    #region OnLoaded
    
    protected override void OnLoaded()
    {
      if (DataContext.Data.Count > 0)
      {
        //Focus the first element 
        VehicleItemViewModel ivm = GetCreateViewModel<VehicleItemViewModel>(DataContext.Data[0], ViewModel);
        ivm.IsFocused = true;
      }

      RegisterContextRegion(RegionManager.Regions[VehicleManagement.TabRegion]);
      RegisterContextRegion(RegionManager.Regions[VehicleManagement.DetailsRegion]);

      base.OnLoaded();
    }

    #endregion
    
    #region OnContextViewModelChanged
    
    protected override void OnContextViewModelChanged(ISelectableViewModel context)
    {
      IRegion tabRegion = RegionManager.Regions[VehicleManagement.TabRegion];

      if (context.Model is Vehicle)
      {
        VehicleDetailViewModel vm = GetCreateViewModel<VehicleDetailViewModel>(context.Model, ViewModel);
        AddDetailsViewModel(vm);

        //VehicleReferencesViewModel rvm = GetCreateViewModel<VehicleReferencesViewModel>(context.Model, ViewModel);
        //AddTabViewModel(rvm);
      }

      base.OnContextViewModelChanged(context);
    }

    #endregion
    
    #region ExecuteOperation
    
    protected override void ExecuteOperation(Operation op, BusinessObject argument)
    {
      switch (op.Identifier)
      {
        case Operations.AddVehicle:
          CreateRootItem();
          break;

        case Operations.Import:
          //ImportDialogController ic = CreateChildController<ImportDialogController>();
          //ic.ImportController = this;
          //ic.Run();
          break;
      }

      base.ExecuteOperation(op, argument);
    }
    
    #endregion

    //public void DoImport(ExternalSystem system)
    //{
    //  try
    //  {
    //    using (var svc = ProxyFactory.GetService<IFleetManagementService>(SecurityContext.MasterServer.Name))
    //    {
    //      //DataContext.Data = svc.ImportVehicles(system);
    //      //DataContextUpdated();
    //      //DataContext.IsDirty = false;
    //    }
    //  }
    //  catch (Exception ex)
    //  {
    //    if (ExceptionHelper.HandleException(ex)) throw;
    //  }
    //}
  }
}
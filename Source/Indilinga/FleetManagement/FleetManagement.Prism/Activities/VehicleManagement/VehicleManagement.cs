﻿using Afx.Business.Activities;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Business.Service;
using Afx.Prism.RibbonMdi.Activities;
using FleetManagement.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FleetManagement.Prism.Activities.VehicleManagement
{
  public partial class VehicleManagement : ContextualActivityContext<Vehicle>
  {
    public const string TabRegion = "TabRegion";
    public const string DetailsRegion = "DetailsRegion";

    public VehicleManagement()
    {
    }

    public VehicleManagement(ContextualActivity activity)
      : base(activity)
    {
    }
  }
}

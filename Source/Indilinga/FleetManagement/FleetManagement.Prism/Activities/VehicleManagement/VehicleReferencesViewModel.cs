﻿using Afx.Business;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Activities;
using FleetManagement.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FleetManagement.Prism.Activities.VehicleManagement
{
  public class VehicleReferencesViewModel : ActivityTabDetailViewModel<Vehicle>
  {
    #region Constructors

    [InjectionConstructor]
    public VehicleReferencesViewModel(IController controller)
      : base(controller)
    {
      Title = "_References";
    }

    #endregion

    BasicCollection<ExternalSystem> mExternalSystems;
    public IEnumerable<ExternalSystem> ExternalSystems
    {
      get { return mExternalSystems ?? (mExternalSystems = Cache.Instance.GetObjects<ExternalSystem>(true, es => es.Name, true)); }
    }

    #region IEnumerable<VehicleReference> ExternalReferences

    public IEnumerable<VehicleReference> ExternalReferences
    {
      get
      {
        if (Model == null) return null;
        return Model.ExternalReferences;
      }
    }

    #endregion
  }
}

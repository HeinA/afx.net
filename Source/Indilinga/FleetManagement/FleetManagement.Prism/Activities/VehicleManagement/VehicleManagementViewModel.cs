﻿using Afx.Business.Security;
using Afx.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Activities;

namespace FleetManagement.Prism.Activities.VehicleManagement
{
  public partial class VehicleManagementViewModel : MdiContextualActivityViewModel<VehicleManagement>
  {
    #region Constructors

    [InjectionConstructor]
    public VehicleManagementViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion
  }
}

﻿using Afx.Business;
using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using FleetManagement.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FleetManagement.Prism.Activities.VehicleManagement
{
  public partial class VehicleItemViewModel : MdiNavigationListItemViewModel<Vehicle>
  {
    #region Constructors

    [InjectionConstructor]
    public VehicleItemViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    #region string FleetNumber

    public string FleetNumber
    {
      get { return Model == null ? GetDefaultValue<string>() : Model.FleetNumber; }
      set { Model.FleetNumber = value; }
    }

    #endregion

    #region string RegistrationNumber

    public string RegistrationNumber
    {
      get
      {
        if (Model == null) return default(string);
        return Model.RegistrationNumber;
      }
    }

    #endregion

    #region string VehicleType

    public string VehicleType
    {
      get
      {
        if (Model == null) return string.Empty;
        if (Model.IsTruck && Model.IsTrailer) return "Truck/Trailer";
        if (Model.IsTruck) return "Truck";
        if (Model.IsTrailer) return "Trailer";
        return string.Empty;
      }
    }

    #endregion

    #region string Make

    public string Make
    {
      get
      {
        if (Model == null) return GetDefaultValue<string>();
        return Model.Make;
      }
      set { Model.Make = value; }
    }

    #endregion

    #region string VehicleModel

    public string VehicleModel
    {
      get
      {
        if (Model == null) return GetDefaultValue<string>();
        return Model.Model;
      }
      set { Model.Model = value; }
    }

    #endregion

    #region void OnModelPropertyChanged(...)

    protected override void OnModelPropertyChanged(string propertyName)
    {
      switch (propertyName)
      {
        case Vehicle.ModelProperty:
          OnPropertyChanged("VehicleModel");
          break;

        case Vehicle.IsTruckProperty:
        case Vehicle.IsTrailerProperty:
          OnPropertyChanged("VehicleType");
          break;
      }

      base.OnModelPropertyChanged(propertyName);
    }

    #endregion
  }
}

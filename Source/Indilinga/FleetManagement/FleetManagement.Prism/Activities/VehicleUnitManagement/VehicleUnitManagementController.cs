﻿using Afx.Business;
using Afx.Business.Activities;
using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Activities;
using FleetManagement.Business;
using FleetManagement.Prism.Activities.VehicleUnitManagement.AddVehicleDialog;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FleetManagement.Prism.Activities.VehicleUnitManagement
{
  public partial class VehicleUnitManagementController : MdiContextualActivityController<VehicleUnitManagement, VehicleUnitManagementViewModel>
  {
    #region Constructors

    [InjectionConstructor]
    public VehicleUnitManagementController(IController controller)
      : base(controller)
    {
    }

    #endregion

    #region OnLoaded
    
    protected override void OnLoaded()
    {
      if (DataContext.Data.Count > 0)
      {
        //Focus the first element 
        VehicleUnitItemViewModel ivm = GetCreateViewModel<VehicleUnitItemViewModel>(DataContext.Data[0], ViewModel);
        ivm.IsFocused = true;
      }

      RegisterContextRegion(RegionManager.Regions[VehicleUnitManagement.DetailsRegion]);
      RegisterContextRegion(RegionManager.Regions[VehicleUnitManagement.TabRegion]);

      base.OnLoaded();
    }

    #endregion
    
    #region OnContextViewModelChanged
    
    protected override void OnContextViewModelChanged(ISelectableViewModel context)
    {
      IRegion tabRegion = RegionManager.Regions[VehicleUnitManagement.TabRegion];

      if (context.Model is VehicleUnit)
      {
        VehicleUnitDetailViewModel vm = GetCreateViewModel<VehicleUnitDetailViewModel>(context.Model, ViewModel);
        RegionManager.Regions[VehicleUnitManagement.DetailsRegion].Add(vm);

        OrganizationalUnitsViewModel ouvm = GetCreateViewModel<OrganizationalUnitsViewModel>(context.Model, ViewModel);
        tabRegion.Add(ouvm);
        if (ouvm.IsActive) tabRegion.Activate(ouvm);
      }

      base.OnContextViewModelChanged(context);
    }

    #endregion
    
    #region ExecuteOperation
    
    protected override void ExecuteOperation(Operation op, BusinessObject argument)
    {
      switch (op.Identifier)
      {
        case Operations.AddUnit:
          CreateRootItem();
          break;

        case Operations.LinkVehicles:
          AddVehicleDialogController c = GetCreateChildController<AddVehicleDialogController>(AddVehicleDialogController.AddVehicleDialogControllerKey);
          c.VehicleUnit = (VehicleUnit)argument;
          c.Filter = c.VehicleUnit.FleetUnitNumber;
          c.Run();
          break;
      }

      base.ExecuteOperation(op, argument);
    }
    
    #endregion
  }
}

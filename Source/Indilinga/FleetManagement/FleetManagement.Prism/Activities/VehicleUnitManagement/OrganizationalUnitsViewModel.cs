﻿using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Activities;
using FleetManagement.Business;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace FleetManagement.Prism.Activities.VehicleUnitManagement
{
  public class OrganizationalUnitsViewModel : ActivityTabDetailViewModel<VehicleUnit>
  {
    #region Constructors

    [InjectionConstructor]
    public OrganizationalUnitsViewModel(IController controller)
      : base(controller)
    {
      Title = "_Organizational Units";
    }

    #endregion

    #region DelegateCommand KeyDownCommand

    DelegateCommand<KeyEventArgs> mKeyDownCommand;
    public DelegateCommand<KeyEventArgs> KeyDownCommand
    {
      get { return mKeyDownCommand ?? (mKeyDownCommand = new DelegateCommand<KeyEventArgs>(ExecuteKeyDown)); }
    }

    void ExecuteKeyDown(KeyEventArgs e)
    {
      if (e.Key != Key.Space) return;
      if (SelectedOrganizationalUnitItemViewModel != null) SelectedOrganizationalUnitItemViewModel.HasOrganizationalUnit = !SelectedOrganizationalUnitItemViewModel.HasOrganizationalUnit;
      e.Handled = true;
    }

    #endregion

    #region ListView

    #region BasicCollection<OrganizationalUnit> OrganizationalUnits

    BasicCollection<OrganizationalUnit> mOrganizationalUnits;
    public BasicCollection<OrganizationalUnit> OrganizationalUnits
    {
      get { return mOrganizationalUnits ?? (mOrganizationalUnits = Cache.Instance.GetObjects<OrganizationalUnit>((ou) => ou.UserAssignable)); }
    }

    #endregion

    #region IEnumerable<OrganizationalUnitItemViewModel> OrganizationalUnitViewModels

    public IEnumerable<OrganizationalUnitItemViewModel> OrganizationalUnitViewModels
    {
      get
      {
        if (Model == null) return null;
        return this.ResolveViewModels<OrganizationalUnitItemViewModel, OrganizationalUnit>(OrganizationalUnits);
      }
    }

    #endregion

    #region OrganizationalUnitItemViewModel SelectedOrganizationalUnitItemViewModel

    OrganizationalUnitItemViewModel mSelectedOrganizationalUnitItemViewModel;
    public OrganizationalUnitItemViewModel SelectedOrganizationalUnitItemViewModel
    {
      get { return mSelectedOrganizationalUnitItemViewModel; }
      set { mSelectedOrganizationalUnitItemViewModel = value; }
    }

    #endregion

    #endregion
  }
}

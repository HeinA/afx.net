﻿using FleetManagement.Business;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.Composition;
using Microsoft.Practices.Prism;

namespace FleetManagement.Prism.Activities.VehicleUnitManagement
{
  //**************************************************************************************************************
  //**************************************************************************************************************
  //*                                                                                                            *
  //*     This code is auto-generated.  Do not place any cutom code here, it will be lost on re-generation.      *
  //*                                                                                                            *
  //**************************************************************************************************************
  //**************************************************************************************************************
  [Export("Controller:" + global::FleetManagement.Prism.Activities.VehicleUnitManagement.VehicleUnitManagement.Key)]
  public partial class VehicleUnitManagementController
  {
    void CreateRootItem()
    {
      VehicleUnit o = new VehicleUnit();
      DataContext.Data.Add(o);
      SelectContextViewModel(o);
      FocusViewModel<VehicleUnitDetailViewModel>(o);
    }

    public void AddDetailsViewModel(object viewModel)
    {
      RegionManager.Regions[VehicleUnitManagement.DetailsRegion].Add(viewModel);
    }

    public void AddTabViewModel(object viewModel)
    {
      IRegion tabRegion = RegionManager.Regions[VehicleUnitManagement.TabRegion];
      tabRegion.Add(viewModel);
      IActiveAware aw = viewModel as IActiveAware;
      if (aw != null && aw.IsActive) tabRegion.Activate(viewModel);
    }
  }
}

﻿using Afx.Business.Security;
using Afx.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Activities;
using FleetManagement.Business;

namespace FleetManagement.Prism.Activities.VehicleUnitManagement
{
  public partial class VehicleUnitManagementViewModel : MdiContextualActivityViewModel<VehicleUnitManagement>
  {
    #region Constructors

    [InjectionConstructor]
    public VehicleUnitManagementViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion
  }
}

﻿using Afx.Business.Activities;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Business.Service;
using FleetManagement.Business;
using FleetManagement.Business.Service;
using Afx.Prism.RibbonMdi.Activities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FleetManagement.Prism.Activities.VehicleUnitManagement
{
  public partial class VehicleUnitManagement : ContextualActivityContext<VehicleUnit>
  {
    public const string TabRegion = "TabRegion";
    public const string DetailsRegion = "DetailsRegion";

    public VehicleUnitManagement()
    {
    }

    public VehicleUnitManagement(ContextualActivity activity)
      : base(activity)
    {
    }
  }
}

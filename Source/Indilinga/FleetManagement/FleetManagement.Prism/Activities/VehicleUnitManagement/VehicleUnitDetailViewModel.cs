﻿using Afx.Business;
using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Activities;
using FleetManagement.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FleetManagement.Prism.Activities.VehicleUnitManagement
{
  public partial class VehicleUnitDetailViewModel : ActivityDetailViewModel<VehicleUnit>
  {
    #region Constructors

    [InjectionConstructor]
    public VehicleUnitDetailViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    #region string FleetUnitNumber

    public string FleetUnitNumber
    {
      get { return Model == null ? GetDefaultValue<string>() : Model.FleetUnitNumber; }
      set { Model.FleetUnitNumber = value; }
    }

    #endregion

    public IEnumerable<Vehicle> Vehicles
    {
      get { return Model == null ? null : Model.Vehicles; }
    }

  }
}

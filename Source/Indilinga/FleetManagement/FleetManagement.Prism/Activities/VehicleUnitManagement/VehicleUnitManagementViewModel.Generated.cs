﻿using Afx.Business.Security;
using Afx.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Afx.Prism.RibbonMdi.Activities;
using FleetManagement.Business;
using Microsoft.Practices.Prism;

namespace FleetManagement.Prism.Activities.VehicleUnitManagement
{
  //**************************************************************************************************************
  //**************************************************************************************************************
  //*                                                                                                            *
  //*     This code is auto-generated.  Do not place any cutom code here, it will be lost on re-generation.      *
  //*                                                                                                            *
  //**************************************************************************************************************
  //**************************************************************************************************************
  public partial class VehicleUnitManagementViewModel
  {
    public IEnumerable<VehicleUnitItemViewModel> Items
    {
      get
      {
        if (Model == null) return null;
        return this.ResolveViewModels<VehicleUnitItemViewModel, VehicleUnit>(Model.Data);
      }
    }
  }
}

﻿using Afx.Business.Activities;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Business.Service;
using FleetManagement.Business;
using FleetManagement.Business.Service;
using Afx.Prism.RibbonMdi.Activities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.Composition;
  
namespace FleetManagement.Prism.Activities.VehicleUnitManagement
{
  //**************************************************************************************************************
  //**************************************************************************************************************
  //*                                                                                                            *
  //*     This code is auto-generated.  Do not place any cutom code here, it will be lost on re-generation.      *
  //*                                                                                                            *
  //**************************************************************************************************************
  //**************************************************************************************************************
  [Export("Context:" + VehicleUnitManagement.Key)]
  public partial class VehicleUnitManagement
  {
    public const string Key = "{da16a70f-1e45-4103-b283-e821441c4c5a}";

    public override void LoadData()
    {
      using (var svc = ProxyFactory.GetService<IFleetManagementService>(SecurityContext.MasterServer.Name))
      {
        Data = svc.LoadVehicleUnitCollection();
      }
      base.LoadData();
    }

    public override void SaveData()
    {
      using (var svc = ProxyFactory.GetService<IFleetManagementService>(SecurityContext.MasterServer.Name))
      {
        Data = svc.SaveVehicleUnitCollection(Data);
      }
      base.SaveData();
    }
  }
}

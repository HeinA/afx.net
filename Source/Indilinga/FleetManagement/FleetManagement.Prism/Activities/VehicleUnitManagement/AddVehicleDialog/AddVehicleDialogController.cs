﻿using Afx.Business.Collections;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using FleetManagement.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace FleetManagement.Prism.Activities.VehicleUnitManagement.AddVehicleDialog
{
  public class AddVehicleDialogController : MdiDialogController<AddVehicleDialogContext, AddVehicleDialogViewModel>
  {
    public const string AddVehicleDialogControllerKey = "FleetManagement.Prism.Activities.VehicleUnitManagement.AddVehicleDialog.AddVehicleDialogController";

    public AddVehicleDialogController(IController controller)
      : base(AddVehicleDialogControllerKey, controller)
    {
    }

    #region VehicleUnit VehicleUnit

    VehicleUnit mVehicleUnit;
    public VehicleUnit VehicleUnit
    {
      get { return mVehicleUnit; }
      set { mVehicleUnit = value; }
    }

    #endregion

    #region string Filter

    string mFilter;
    public string Filter
    {
      get { return mFilter; }
      set { mFilter = value; }
    }

    #endregion

    BasicCollection<Vehicle> mVehicles;
    public BasicCollection<Vehicle> Vehicles
    {
      get { return mVehicles ?? (mVehicles = new BasicCollection<Vehicle>()); }
    }

    protected override bool OnRun()
    {
      if (DataContext == null) DataContext = new AddVehicleDialogContext();
      ViewModel.Caption = "Add Vehicle(s)";
      ViewModel.Filter = Filter;
      foreach (Vehicle v in VehicleUnit.Vehicles)
      {
        Vehicles.Add(v);
      }
      return base.OnRun();
    }

    protected override void ApplyChanges()
    {
      Queue<Vehicle> removed = new Queue<Vehicle>();
      foreach (Vehicle v in VehicleUnit.Vehicles)
      {
        if (!Vehicles.Contains(v)) removed.Enqueue(v);
      }

      while (removed.Count > 0)
      {
        VehicleUnit.Vehicles.Remove(removed.Dequeue());
      }

      foreach (Vehicle v in Vehicles)
      {
        Vehicle v2 = VehicleUnit.Vehicles.FirstOrDefault(v1 => v1.Equals(v));
        if (v2 == null) VehicleUnit.Vehicles.Add(v);
      }

      base.ApplyChanges();
    }
  }
}

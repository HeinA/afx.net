﻿using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using FleetManagement.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FleetManagement.Prism.Activities.VehicleUnitManagement.AddVehicleDialog
{
  public class AddVehicleDialogViewModel : MdiDialogViewModel<AddVehicleDialogContext>
  {
    [InjectionConstructor]
    public AddVehicleDialogViewModel(IController controller)
      : base(controller)
    {
    }

    #region AddVehicleDialogController AddVehicleDialogController

    AddVehicleDialogController mAddVehicleDialogController;
    [Dependency]
    public AddVehicleDialogController AddVehicleDialogController
    {
      get { return mAddVehicleDialogController; }
      set { mAddVehicleDialogController = value; }
    }

    #endregion

    #region string Filter

    string mFilter;
    public string Filter
    {
      get { return mFilter; }
      set
      {
        if (SetProperty<string>(ref mFilter, value))
        {
          OnPropertyChanged("Vehicles");
        }
      }
    }

    #endregion

    BasicCollection<OwnedVehicle> mVehicles;
    public IEnumerable<VehicleItemViewModel> Vehicles
    {
      get
      {
        if (!string.IsNullOrWhiteSpace(Filter))
        {
          mVehicles = Cache.Instance.GetObjects<OwnedVehicle>(v => v.FleetNumber.Contains(Filter) || v.RegistrationNumber.Contains(Filter), v => v.FleetNumber, true);
        }
        else
        {
          mVehicles = Cache.Instance.GetObjects<OwnedVehicle>(v => v.FleetNumber, true);
        }
        return ResolveViewModels<VehicleItemViewModel, Vehicle>(mVehicles);
      }
    }
  }
}

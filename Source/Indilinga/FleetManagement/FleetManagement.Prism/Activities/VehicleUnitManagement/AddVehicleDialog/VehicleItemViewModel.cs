﻿using Afx.Prism;
using FleetManagement.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FleetManagement.Prism.Activities.VehicleUnitManagement.AddVehicleDialog
{
  public class VehicleItemViewModel : SelectableViewModel<Vehicle>
  {
    #region Constructors

    [InjectionConstructor]
    public VehicleItemViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    #region AddVehicleDialogController AddVehicleDialogController

    AddVehicleDialogController mAddVehicleDialogController;
    [Dependency]
    public AddVehicleDialogController AddVehicleDialogController
    {
      get { return mAddVehicleDialogController; }
      set { mAddVehicleDialogController = value; }
    }

    #endregion

    #region bool IsChecked

    public bool IsChecked
    {
      get { return AddVehicleDialogController.Vehicles.Contains(Model); }
      set
      {
        if (value && !AddVehicleDialogController.Vehicles.Contains(Model)) AddVehicleDialogController.Vehicles.Add(Model);
        if (!value && AddVehicleDialogController.Vehicles.Contains(Model)) AddVehicleDialogController.Vehicles.Remove(Model);
      }
    }

    #endregion

    #region string FleetNumber

    public string FleetNumber
    {
      get { return Model == null ? GetDefaultValue<string>() : Model.FleetNumber; }
    }

    #endregion

    #region string RegistrationNumber

    public string RegistrationNumber
    {
      get { return Model == null ? GetDefaultValue<string>() : Model.RegistrationNumber; }
    }

    #endregion

    #region string Make

    public string Make
    {
      get { return Model == null ? GetDefaultValue<string>() : Model.Make; }
    }

    #endregion

    #region string VehicleModel

    public string VehicleModel
    {
      get { return Model == null ? GetDefaultValue<string>() : Model.Model; }
    }

    #endregion
  }
}

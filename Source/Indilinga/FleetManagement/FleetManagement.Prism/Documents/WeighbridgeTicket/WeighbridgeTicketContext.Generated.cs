﻿using Afx.Business.Security;
using Afx.Business.Service;
using Afx.Prism.RibbonMdi.Documents;
using FleetManagement.Business;
using FleetManagement.Business.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.Composition;

namespace FleetManagement.Prism.Documents.WeighbridgeTicket
{
  [Export("Context:" + FleetManagement.Business.WeighbridgeTicket.DocumentTypeIdentifier)]
  public partial class WeighbridgeTicketContext
  {
  }
}


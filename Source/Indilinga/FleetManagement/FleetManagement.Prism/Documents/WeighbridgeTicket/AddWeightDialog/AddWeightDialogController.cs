﻿using Afx.Business;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using FleetManagement.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace FleetManagement.Prism.Documents.WeighbridgeTicket.AddWeightDialog
{
  public class AddWeightDialogController : MdiDialogController<AddWeightDialogContext, AddWeightDialogViewModel>
  {
    public const string AddWeightDialogControllerKey = "FleetManagement.Prism.Documents.WeighbridgeTicket.AddWeightDialog.AddWeightDialogController";

    public AddWeightDialogController(IController controller)
      : base(AddWeightDialogControllerKey, controller)
    {
    }

    WeighbridgeTicketController mWeighbridgeTicketController;
    [Dependency]
    public WeighbridgeTicketController WeighbridgeTicketController
    {
      get { return mWeighbridgeTicketController; }
      set { mWeighbridgeTicketController = value; }
    }

    protected override bool OnRun()
    {
      if (DataContext == null) DataContext = new AddWeightDialogContext();
      ViewModel.Caption = "Add Weight";
      return base.OnRun();
    }

    SerialPort mSerialPort;

    protected override void OnRunning()
    {
      try
      {
        ScaleSetting ss = Setting.GetSetting<ScaleSetting>();
        if (ss.IsEnabled)
        {
          mSerialPort = new SerialPort(ss.ComPort, ss.Baud, ss.Parity, ss.DataBits, ss.StopBits);

          mSerialPort.DataReceived += SerialPort_DataReceived;
          if (!mSerialPort.IsOpen)
          {
            mSerialPort.Open();
          }
        }
      }
      catch (Exception ex)
      {
        if (ExceptionHelper.HandleException(ex)) throw ex;
      }

      base.OnRunning();
    }

    string mData = string.Empty;
    bool bReadingWeight = false;

    private void SerialPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
    {
      ProcessInput(mSerialPort.ReadExisting());
    }

    void ProcessInput(string data)
    {
      try
      {
        foreach (char c in data)
        {
          if (c == 0x53 || c == 0x2B) // S or +
          {
            UpdateWeight();
            bReadingWeight = true;
          }
          else
          {
            if (bReadingWeight && c >= 48 && c <= 57) // 0-9
            {
              mData += c;
            }
            else
            {
              bReadingWeight = false;
            }
          }
        }
      }
      catch (Exception ex)
      {
        if (ExceptionHelper.HandleException(ex)) throw ex;
      }
    }

    void UpdateWeight()
    {
      if (!string.IsNullOrEmpty(mData))
      {
        DataContext.CurrentWeight = decimal.Parse(mData);
        mData = string.Empty;
      }
    }

    protected override void ApplyChanges()
    {
      WeighbridgeTicketWeight w = new WeighbridgeTicketWeight();
      w.WeightCategory = DataContext.WeightCategory;
      foreach (var wi in DataContext.Items)
      {
        w.Items.Add(wi);
      }
      WeighbridgeTicketController.DataContext.Document.Weights.Add(w);

      base.ApplyChanges();
    }
  }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FleetManagement.Prism.Documents.WeighbridgeTicket.AddWeightDialog
{
  [Export(typeof(Afx.Prism.RibbonMdi.ResourceInfo))]
  public class ResourceInfo : Afx.Prism.RibbonMdi.ResourceInfo
  {
    public override Uri GetUri()
    {
      return new Uri("pack://application:,,,/FleetManagement.Prism;component/Documents/WeighbridgeTicket/AddWeightDialog/Resources.xaml", UriKind.Absolute);
    }
  }
}
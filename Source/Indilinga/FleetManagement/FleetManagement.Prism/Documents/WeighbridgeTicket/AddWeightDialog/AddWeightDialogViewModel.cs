﻿using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using FleetManagement.Business;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FleetManagement.Prism.Documents.WeighbridgeTicket.AddWeightDialog
{
  public class AddWeightDialogViewModel : MdiDialogViewModel<AddWeightDialogContext>
  {
    [InjectionConstructor]
    public AddWeightDialogViewModel(IController controller)
      : base(controller)
    {
    }

    #region decimal CurrentWeight

    public decimal CurrentWeight
    {
      get { return Model == null ? GetDefaultValue<decimal>() : Model.CurrentWeight; }
      set { Model.CurrentWeight = value; }
    }

    #endregion

    #region IEnumerable<WeighbridgeTicketWeightItem> Items

    public IEnumerable<WeighbridgeTicketWeightItem> Items
    {
      get { return Model == null ? GetDefaultValue<BusinessObjectCollection<WeighbridgeTicketWeightItem>>() : Model.Items; }
    }

    #endregion

    #region DelegateCommand AddWeightCommand

    DelegateCommand mAddWeightCommand;
    public DelegateCommand AddWeightCommand
    {
      get { return mAddWeightCommand ?? (mAddWeightCommand = new DelegateCommand(ExecuteAddWeight, CanExecuteAddWeight)); }
    }

    bool CanExecuteAddWeight()
    {
      return true;
    }

    void ExecuteAddWeight()
    {
      Model.Items.Add(new WeighbridgeTicketWeightItem(string.Format("Weight {0}", Model.Items.Count + 1), CurrentWeight));
    }

    #endregion

    #region DelegateCommand SubtractWeightCommand

    DelegateCommand mSubtractWeightCommand;
    public DelegateCommand SubtractWeightCommand
    {
      get { return mSubtractWeightCommand ?? (mSubtractWeightCommand = new DelegateCommand(ExecuteSubtractWeight, CanExecuteSubtractWeight)); }
    }

    bool CanExecuteSubtractWeight()
    {
      return true;
    }

    void ExecuteSubtractWeight()
    {
      Model.Items.Add(new WeighbridgeTicketWeightItem(string.Format("Weight {0}", Model.Items.Count + 1), CurrentWeight *-1));
    }

    #endregion

    #region decimal TotalWeight

    public decimal TotalWeight
    {
      get { return Model == null ? GetDefaultValue<decimal>() : Model.TotalWeight; }
      set { Model.TotalWeight = value; }
    }

    #endregion

    #region WeightCategory WeightCategory

    public WeightCategory WeightCategory
    {
      get { return Model == null ? GetDefaultValue<WeightCategory>() : Model.WeightCategory; }
      set { Model.WeightCategory = value; }
    }

    BasicCollection<WeightCategory> mWeightCategories;
    public IEnumerable<WeightCategory> WeightCategories
    {
      get { return mWeightCategories ?? (mWeightCategories = Cache.Instance.GetObjects<WeightCategory>(true, wc => wc.Description, true)); }
    }

    #endregion

  }
}

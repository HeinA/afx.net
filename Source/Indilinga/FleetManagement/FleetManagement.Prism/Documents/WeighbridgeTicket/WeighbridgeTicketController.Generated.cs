﻿using Afx.Business;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Documents;
using FleetManagement.Business;
using Afx.Business.Security;
using Afx.Business.Service;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.Composition;

namespace FleetManagement.Prism.Documents.WeighbridgeTicket
{
  //**************************************************************************************************************
  //**************************************************************************************************************
  //*                                                                                                            *
  //*     This code is auto-generated.  Do not place any cutom code here, it will be lost on re-generation.      *
  //*                                                                                                            *
  //**************************************************************************************************************
  //**************************************************************************************************************
  [Export("Controller:" + FleetManagement.Business.WeighbridgeTicket.DocumentTypeIdentifier)]
  public partial class WeighbridgeTicketController
  {
    protected void SetDocumentState(string stateIdentifier)
    {
      if (!DataContext.Document.Validate()) throw new MessageException("The document has validation errors.");

      using (var svc = ProxyFactory.GetService<IAfxService>(SecurityContext.Server))
      {
        DataContext.Document = (FleetManagement.Business.WeighbridgeTicket)svc.SetDocumentState(DataContext.Document, stateIdentifier);
      }
    }
  }
}


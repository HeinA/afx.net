﻿using Afx.Business.Security;
using Afx.Business.Service;
using Afx.Prism.RibbonMdi.Documents;
using FleetManagement.Business;
using FleetManagement.Business.Service;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace FleetManagement.Prism.Documents.WeighbridgeTicket
{
  public partial class WeighbridgeTicketContext : DocumentContext<FleetManagement.Business.WeighbridgeTicket>
  {
    public WeighbridgeTicketContext()
      : base(FleetManagement.Business.WeighbridgeTicket.DocumentTypeIdentifier)
    {
    }

    public WeighbridgeTicketContext(Guid documentIdentifier)
      : base(FleetManagement.Business.WeighbridgeTicket.DocumentTypeIdentifier, documentIdentifier)
    {
    }

    public override void SaveData()
    {
      using (var svc = ProxyFactory.GetService<IFleetManagementService>(SecurityContext.Server))
      {
        Document = svc.SaveWeighbridgeTicket(Document);
      }
      base.SaveData();
    }

    public override void LoadData()
    {
      using (var svc = ProxyFactory.GetService<IFleetManagementService>(SecurityContext.Server))
      {
        Document = svc.LoadWeighbridgeTicket(DocumentIdentifier);
      }
      base.LoadData();
    }

    //protected override void SetupReport()
    //{
    //  ReportViewer.LocalReport.ReportEmbeddedResource = "FleetManagement.Prism.Reports.WeighbridgeTicket.rdlc";

    //  base.SetupReport();
    //}

    //protected override void RefreshReportData()
    //{

    //  ReportDataSource reportDataSource = new ReportDataSource();
    //  reportDataSource.Name = "Ticket";
    //  reportDataSource.Value = new object[] { Document };
    //  ReportViewer.LocalReport.DataSources.Add(reportDataSource);

    //  reportDataSource = new ReportDataSource();
    //  reportDataSource.Name = "Vehicles";
    //  reportDataSource.Value = Document.Vehicles;
    //  ReportViewer.LocalReport.DataSources.Add(reportDataSource);

    //  reportDataSource = new ReportDataSource();
    //  reportDataSource.Name = "Weights";
    //  reportDataSource.Value = Document.Weights;
    //  ReportViewer.LocalReport.DataSources.Add(reportDataSource);
  
    //  base.RefreshReportData();
    //}
  }
}


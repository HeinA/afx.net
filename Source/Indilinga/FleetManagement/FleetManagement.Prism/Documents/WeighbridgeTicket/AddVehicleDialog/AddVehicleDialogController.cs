﻿using Afx.Business;
using Afx.Business.Collections;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using FleetManagement.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace FleetManagement.Prism.Documents.WeighbridgeTicket.AddVehicleDialog
{
  public class AddVehicleDialogController : MdiDialogController<AddVehicleDialogContext, AddVehicleDialogViewModel>
  {
    public const string AddVehicleDialogControllerKey = "FleetManagement.Prism.Documents.WeighbridgeTicket.AddVehicleDialog.AddVehicleDialogController";

    public AddVehicleDialogController(IController controller)
      : base(AddVehicleDialogControllerKey, controller)
    {
    }

    WeighbridgeTicketController mWeighbridgeTicketController;
    [Dependency]
    public WeighbridgeTicketController WeighbridgeTicketController
    {
      get { return mWeighbridgeTicketController; }
      set { mWeighbridgeTicketController = value; }
    }

    protected override bool OnRun()
    {
      if (DataContext == null) DataContext = new AddVehicleDialogContext();
      ViewModel.Caption = "Edit Vehicles";
      foreach (WeighbridgeTicketVehicle wtv in WeighbridgeTicketController.DataContext.Document.Vehicles.Where(v => !BusinessObject.IsNull(v.Vehicle)))
      {
        if (!wtv.IsDeleted) DataContext.Vehicles.Add(wtv.Vehicle);
      }
      return base.OnRun();
    }

    protected override void ApplyChanges()
    {
      IList<Vehicle> vehicles = null;
      if (!BusinessObject.IsNull(DataContext.VehicleUnit)) vehicles = DataContext.VehicleUnit.Vehicles;
      else vehicles = DataContext.Vehicles;

      foreach (WeighbridgeTicketVehicle wtv in WeighbridgeTicketController.DataContext.Document.Vehicles.Where(v => !BusinessObject.IsNull(v.Vehicle)))
      {
        if (!vehicles.Contains(wtv.Vehicle)) wtv.IsDeleted = true;
      }

      foreach (Vehicle v in vehicles)
      {
        WeighbridgeTicketVehicle wtv = WeighbridgeTicketController.DataContext.Document.Vehicles.FirstOrDefault(wtv1 => wtv1.Vehicle.Equals(v));
        if (wtv == null) WeighbridgeTicketController.DataContext.Document.Vehicles.Add(new WeighbridgeTicketVehicle(v));
      }

      base.ApplyChanges();
    }
  }
}

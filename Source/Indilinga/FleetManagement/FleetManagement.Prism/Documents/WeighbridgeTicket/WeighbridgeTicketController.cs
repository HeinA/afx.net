﻿using AccountManagement.Prism.Extensions.Account;
using Afx.Business;
using Afx.Business.Data;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Documents;
using FleetManagement.Business;
using FleetManagement.Prism.Documents.WeighbridgeTicket.AddVehicleDialog;
using FleetManagement.Prism.Documents.WeighbridgeTicket.AddWeightDialog;
using Microsoft.Practices.Unity;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FleetManagement.Prism.Documents.WeighbridgeTicket
{
  public partial class WeighbridgeTicketController : MdiDocumentController<WeighbridgeTicketContext, WeighbridgeTicketViewModel>
  {
    [InjectionConstructor]
    public WeighbridgeTicketController(IController controller)
      : base(controller)
    {
    }

    protected override void AfterContainerConfigured()
    {
      base.AfterContainerConfigured();
    }

    protected override void ExtendDocument()
    {
      DocumentAccountSelectionViewModel vm = ExtendDocument<DocumentAccountSelectionViewModel>(Afx.Prism.RibbonMdi.Documents.Regions.TopDetailsRegion);
      vm.Bind(DataContext.Document, FleetManagement.Business.WeighbridgeTicket.AccountProperty);

      base.ExtendDocument();
    }

    protected override void ExecuteOperation(Operation op, BusinessObject argument)
    {
      switch (op.Identifier)
      {
        case Operations.AddVehicle:
          {
            AddVehicleDialogController c = GetCreateChildController<AddVehicleDialogController>(AddVehicleDialogController.AddVehicleDialogControllerKey);
            c.Run();
          }
          break;

        case Operations.AddDefaultWeight:
          {
            WeighbridgeTicketWeight w = DataContext.Document.Weights.FirstOrDefault(w1 => w1.WeightCategory.IsTare);
            if (w == null)
            {
              w = new WeighbridgeTicketWeight();
              w.WeightCategory = Cache.Instance.GetObjects<WeightCategory>().FirstOrDefault(w1 => w1.IsTare);
              DataContext.Document.Weights.Add(w);
            }

            foreach (var w1 in w.Items)
            {
              w1.IsDeleted = true;
            }
            foreach (WeighbridgeTicketVehicle v in DataContext.Document.Vehicles.Where(v => !v.IsDeleted))
            {
              WeighbridgeTicketWeightItem wi = new WeighbridgeTicketWeightItem() { Description = v.Vehicle.Text, Weight = v.VehicleWeight };
              w.Items.Add(wi);
            }
          }
          break;

        case Operations.AddWeight:
          {
            AddWeightDialogController c = GetCreateChildController<AddWeightDialogController>(AddWeightDialogController.AddWeightDialogControllerKey);
            c.Run();
          }
          break;

        case Operations.Finalize:
          {
            SetDocumentState(FleetManagement.Business.WeighbridgeTicket.States.Finalized);
          }
          break;
      }

      base.ExecuteOperation(op, argument);
    }

    protected override void SetupReport(ReportViewer viewer)
    {
      viewer.LocalReport.ReportEmbeddedResource = "FleetManagement.Prism.Reports.WeighbridgeTicket.rdlc";

      base.SetupReport(viewer);
    }

    protected override void RefreshReportData(ReportViewer viewer)
    {

      ReportDataSource reportDataSource = new ReportDataSource();
      reportDataSource.Name = "Ticket";
      reportDataSource.Value = new object[] { DataContext.Document };
      viewer.LocalReport.DataSources.Add(reportDataSource);

      reportDataSource = new ReportDataSource();
      reportDataSource.Name = "Vehicles";
      reportDataSource.Value = DataContext.Document.Vehicles;
      viewer.LocalReport.DataSources.Add(reportDataSource);

      reportDataSource = new ReportDataSource();
      reportDataSource.Name = "Weights";
      reportDataSource.Value = DataContext.Document.Weights;
      viewer.LocalReport.DataSources.Add(reportDataSource);

      base.RefreshReportData(viewer);
    }

  }
}


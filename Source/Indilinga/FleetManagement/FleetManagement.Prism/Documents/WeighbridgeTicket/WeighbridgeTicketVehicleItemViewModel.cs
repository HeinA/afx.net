﻿using Afx.Business;
using Afx.Prism;
using FleetManagement.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FleetManagement.Prism.Documents.WeighbridgeTicket
{
  public class WeighbridgeTicketVehicleItemViewModel : SelectableViewModel<WeighbridgeTicketVehicle>, IReadOnlyAware
  {
    #region Constructors

    [InjectionConstructor]
    public WeighbridgeTicketVehicleItemViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    //#region string FleetNumber

    //public string FleetNumber
    //{
    //  get { return Model == null ? GetDefaultValue<string>() : Model.FleetNumber; }
    //}

    //#endregion

    #region string RegistrationNumber

    public string RegistrationNumber
    {
      get { return Model == null ? GetDefaultValue<string>() : Model.RegistrationNumber; }
    }

    #endregion

    #region string Make

    public string Make
    {
      get { return Model == null ? GetDefaultValue<string>() : Model.Make; }
    }

    #endregion

    #region string VehicleModel

    public string VehicleModel
    {
      get { return Model == null ? GetDefaultValue<string>() : Model.Model; }
    }

    #endregion

    public override bool IsReadOnly
    {
      get { return !BusinessObject.IsNull(Model.Vehicle); }
    }
  }
}

﻿using Afx.Business;
using Afx.Business.Activities;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Documents;
using Afx.Prism.RibbonMdi.Events;
using FleetManagement.Business;
using Microsoft.Practices.Unity;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace FleetManagement.Prism.Documents.WeighbridgeTicket
{
  public partial class WeighbridgeTicketViewModel : MdiDocumentViewModel<WeighbridgeTicketContext>
  {
    [InjectionConstructor]
    public WeighbridgeTicketViewModel(IController controller)
      : base(controller)
    {
      //ApplicationController.Current.EventAggregator.GetEvent<DialogDisplayedEvent>().Subscribe(DialogDisplayed);
    }

    //private void DialogDisplayed(EventArgs obj)
    //{
    //  Model.IsPrintPreview = false;
    //}

    #region IEnumerable<WeighbridgeTicketWeight> Weights

    BasicCollection<WeightCategory> mWeightCategoryCollection;
    public IEnumerable<WeightCategory> WeightCategoryCollection
    {
      get { return mWeightCategoryCollection ?? (mWeightCategoryCollection = Cache.Instance.GetObjects<WeightCategory>(wc => wc.Description, true)); }
    }

    public IEnumerable<WeighbridgeTicketWeight> Weights
    {
      get { return Model == null ? GetDefaultValue<IEnumerable<WeighbridgeTicketWeight>>() : Model.Document == null ? GetDefaultValue<IEnumerable<WeighbridgeTicketWeight>>() : Model.Document.Weights; }
    }

    #endregion

    #region IEnumerable<WeighbridgeTicketVehicle> Vehicles

    BasicCollection<Vehicle> mVehicleCollection;
    public IEnumerable<Vehicle> VehicleCollection
    {
      get { return mVehicleCollection ?? (mVehicleCollection = Cache.Instance.GetObjects<Vehicle>(true, v => v.Text, true)); }
    }

    public IEnumerable<WeighbridgeTicketVehicle> Vehicles
    {
      get { return Model == null ? null : Model.Document == null ? null : Model.Document.Vehicles; }
    }

    #endregion
  }
}


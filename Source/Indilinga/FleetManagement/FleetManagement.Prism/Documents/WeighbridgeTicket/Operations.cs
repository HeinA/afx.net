﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FleetManagement.Prism.Documents.WeighbridgeTicket
{
  public class Operations : Afx.Prism.RibbonMdi.StandardOperations
  {
      public const string AddVehicle = "{9d752a07-47b8-4615-b14f-e006e42bb10d}";
      public const string AddDefaultWeight = "{8898db92-50d5-4d69-9544-6cc4eebe72d0}";
      public const string AddWeight = "{3d185cd4-097c-4e30-8cb4-7f7c0552cf68}";
      public const string EditInventory = "{aecb42f3-99d9-4539-a2f7-e28f2ea1fe79}";
      public const string Finalize = "{70e82896-5619-480b-913f-7ce598fc89d9}";
  }
}
﻿using Afx.Business;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Documents;
using FleetManagement.Business;
using Afx.Business.Security;
using Afx.Business.Service;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FleetManagement.Prism.Documents.TruckTicket
{
  //**************************************************************************************************************
  //**************************************************************************************************************
  //*                                                                                                            *
  //*     This code is auto-generated.  Do not place any cutom code here, it will be lost on re-generation.      *
  //*                                                                                                            *
  //**************************************************************************************************************
  //**************************************************************************************************************
  public partial class TruckTicketController
  {
    protected void SetDocumentState(string stateIdentifier)
    {
      if (!DataContext.Document.Validate()) throw new MessageException("The document has validation errors.");

      using (var svc = ServiceFactory.GetService<IAfxService>(SecurityContext.Server))
      {
        DataContext.Document = (FleetManagement.Business.TruckTicket)svc.Instance.SetDocumentState(DataContext.Document, stateIdentifier);
      }
    }
  }
}


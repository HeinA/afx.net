﻿using Afx.Business;
using Afx.Business.Activities;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Documents;
using FleetManagement.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace FleetManagement.Prism.Documents.TruckTicket
{
  public class TruckTicketViewModel : MdiDocumentViewModel<TruckTicketContext>
  {
    [InjectionConstructor]
    public TruckTicketViewModel(IController controller)
      : base(controller)
    {
    }

  }
}


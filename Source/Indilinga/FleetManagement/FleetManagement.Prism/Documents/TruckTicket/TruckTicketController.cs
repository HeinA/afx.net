﻿using Afx.Business;
using Afx.Business.Security;
using Afx.Business.Service;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Documents;
using FleetManagement.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FleetManagement.Prism.Documents.TruckTicket
{
  public partial class TruckTicketController : MdiDocumentController<TruckTicketContext, TruckTicketViewModel>
  {
    [InjectionConstructor]
    public TruckTicketController(IController controller)
      : base(controller)
    {
    }

    protected override void ExecuteOperation(Operation op, BusinessObject argument)
    {
      switch (op.Identifier)
      {
        case Operations.FinalizeTicket:
          SetDocumentState(FleetManagement.Business.TruckTicket.States.Finalized);
          break;
      }

      base.ExecuteOperation(op, argument);
    }
  }
}


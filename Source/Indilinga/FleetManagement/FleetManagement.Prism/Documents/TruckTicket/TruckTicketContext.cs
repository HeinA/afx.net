﻿using Afx.Business.Security;
using Afx.Business.Service;
using Afx.Prism.RibbonMdi.Documents;
using FleetManagement.Business;
using FleetManagement.Business.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FleetManagement.Prism.Documents.TruckTicket
{
  public partial class TruckTicketContext : DocumentContext<FleetManagement.Business.TruckTicket>
  {
    public TruckTicketContext()
      : base(FleetManagement.Business.TruckTicket.DocumentTypeIdentifier)
    {
    }

    public TruckTicketContext(Guid documentIdentifier)
      : base(FleetManagement.Business.TruckTicket.DocumentTypeIdentifier, documentIdentifier)
    {
    }

    public override void SaveData()
    {
      using (var svc = ServiceFactory.GetService<IFleetManagementService>(SecurityContext.Server))
      {
        Document = svc.Instance.SaveTruckTicket(Document);
      }
      base.SaveData();
    }

    public override void LoadData()
    {
      using (var svc = ServiceFactory.GetService<IFleetManagementService>(SecurityContext.Server))
      {
        Document = svc.Instance.LoadTruckTicket(DocumentIdentifier);
      }
      base.LoadData();
    }
  }
}


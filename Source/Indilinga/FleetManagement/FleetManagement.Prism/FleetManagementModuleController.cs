﻿using Afx.Business;
using Afx.Business.Activities;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Documents;
using Afx.Business.Security;
using Afx.Business.Service;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using Afx.Prism.RibbonMdi.Documents;
using Afx.Prism.RibbonMdi.Events;
using ContactManagement.Business;
using ContactManagement.Prism.Activities.ContractorManagement;
using ContactManagement.Prism.Activities.EmployeeManagement;
using FleetManagement.Business;
using FleetManagement.Business.Service;
using FleetManagement.Prism.Extensions.ContractorVehicles;
using FleetManagement.Prism.Extensions.DriverDetail;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace FleetManagement.Prism
{
  public class FleetManagementModuleController : MdiModuleController
  {
    [InjectionConstructor]
    public FleetManagementModuleController(IController controller)
      : base("Fleet Management Module Controller", controller)
    {
    }

    protected override void OnUserAuthenticated(EventArgs obj)
    {
      //Debug.WriteLine(BusinessObject.GenerateGuid());
      //Debug.WriteLine(BusinessObject.GenerateGuid());
      //Debug.WriteLine(BusinessObject.GenerateGuid());
      //Debug.WriteLine(BusinessObject.GenerateGuid());
      //Debug.WriteLine(BusinessObject.GenerateGuid());

      //ThreadPool.SetMinThreads(20, 5);

      //mSW.Start();

      //for (int i = 0; i < 1000; i++)  
      //{
      //  ThreadStart ts = new ThreadStart(DoWork);
      //  Thread t = new Thread(ts);
      //  t.Start();
      //}
    }

    protected override void AfterContainerConfigured()
    {
      EventAggregator.GetEvent<ContextChangedEvent>().Subscribe(OnExtendActivityContext);
      base.AfterContainerConfigured();
    }

    private void OnExtendActivityContext(ContextChangedEventArgs obj)
    {
      EmployeeManagementController emc = obj.Controller as EmployeeManagementController;
      if (emc != null)
      {
        Employee e = obj.Context as Employee;
        if (e != null)
        {
          if (e.ContactType.Flags.Contains(ContactTypeFlags.Driver))
          {
            DriverDetail dd = e.GetExtensionObject<DriverDetail>();
            emc.AddDetailsViewModel(emc.GetCreateViewModel<DriverDetailViewModel>(dd, emc.ViewModel));
          }
        }
      }

      ContractorManagementController cmc = obj.Controller as ContractorManagementController;
      if (cmc != null)
      {
        Contractor c = obj.Context as Contractor;
        if (c != null)
        {
          ContractorFleetDetail cfd = c.GetExtensionObject<ContractorFleetDetail>();
          cmc.AddTabViewModel(cmc.GetCreateViewModel<ContractorVehiclesViewModel>(cfd, cmc.ViewModel));
        }
      }
    }
  }
}

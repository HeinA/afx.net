﻿using Afx.Prism.RibbonMdi.Ribbon;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FleetManagement.Prism
{
  [Export(typeof(IRibbonGroup))]
  public class FleetManagementGroup : RibbonGroup
  {
    public const string GroupName = "Fleet Management";

    public override string TabName
    {
      get { return AdministrationTab.TabName; }
    }

    public override string Name
    {
      get { return GroupName; }
    }
  }
}

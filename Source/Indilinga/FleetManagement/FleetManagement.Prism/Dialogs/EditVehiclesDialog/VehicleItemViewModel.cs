﻿using Afx.Prism;
using FleetManagement.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FleetManagement.Prism.Dialogs.EditVehiclesDialog
{
  public class VehicleItemViewModel : SelectableViewModel<Vehicle>
  {
    [InjectionConstructor]
    public VehicleItemViewModel(IController controller)
      : base(controller)
    {
    }

    [Dependency]
    public EditVehiclesDialogController EditVehiclesDialogController { get; set; }

    new EditVehiclesDialogViewModel Parent
    {
      get { return (EditVehiclesDialogViewModel)base.Parent; }
    }


    public override bool IsSelected
    {
      get
      {
        if (base.IsSelected) Parent.SelectedItemViewModel = this;
        return base.IsSelected;
      }
      set
      {
        base.IsSelected = value;
        if (base.IsSelected) Parent.SelectedItemViewModel = this;
      }
    }

    #region bool IsChecked

    public const string IsCheckedProperty = "IsChecked";
    public bool IsChecked
    {
      get { return EditVehiclesDialogController.Vehicles.Contains(Model); }
      set
      {
        if (value)
        {
          if (!EditVehiclesDialogController.Vehicles.Contains(Model))
          {
            EditVehiclesDialogController.Vehicles.Add(Model);
            OnPropertyChanged(IsCheckedProperty);
          }
        }
        else
        {
          if (EditVehiclesDialogController.Vehicles.Contains(Model))
          {
            EditVehiclesDialogController.Vehicles.Remove(Model);
            OnPropertyChanged(IsCheckedProperty);
          }
        }
      }
    }

    #endregion
  }
}

﻿using Afx.Business.Collections;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using ContactManagement.Business;
using FleetManagement.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace FleetManagement.Prism.Dialogs.EditVehiclesDialog
{
  public class EditVehiclesDialogController : MdiDialogController<EditVehiclesDialogContext, EditVehiclesDialogViewModel>
  {
    public const string EditVehiclesDialogControllerKey = "FleetManagement.Prism.Dialogs.EditVehiclesDialog.EditVehiclesDialogController";

    public EditVehiclesDialogController(IController controller)
      : base(EditVehiclesDialogControllerKey, controller)
    {
    }

    protected override bool OnRun()
    {
      if (DataContext == null) DataContext = new EditVehiclesDialogContext();
      ViewModel.Caption = "Edit Vehicles";
      ViewModel.Contractor = Contractor;
      return base.OnRun();
    }

    protected override void ApplyChanges()
    {
      Stack<Vehicle> removed = new Stack<Vehicle>();
      foreach(Vehicle v in VehicleCollection.Vehicles)
      {
        if (!Vehicles.Contains(v)) removed.Push(v);
      }
      while (removed.Count > 0)
      {
        VehicleCollection.Vehicles.Remove(removed.Pop());
      }

      foreach (Vehicle v in Vehicles)
      {
        if (!VehicleCollection.Vehicles.Contains(v)) VehicleCollection.Vehicles.Add(v);
      }

      base.ApplyChanges();
    }

    BasicCollection<Vehicle> mVehicles = new BasicCollection<Vehicle>();
    public BasicCollection<Vehicle> Vehicles
    {
      get { return mVehicles; }
    }

    #region IVehicleCollection VehicleCollection

    public const string VehicleCollectionProperty = "VehicleCollection";
    IVehicleCollection mVehicleCollection;
    public IVehicleCollection VehicleCollection
    {
      get { return mVehicleCollection; }
      set
      {
        mVehicleCollection = value;
        foreach (var v in mVehicleCollection.Vehicles)
        {
          Vehicles.Add(v);
        }
      }
    }

    #endregion

    #region Contractor Contractor

    public const string ContractorProperty = "Contractor";
    Contractor mContractor;
    public Contractor Contractor
    {
      get { return mContractor; }
      set { mContractor = value; }
    }

    #endregion
  }
}

﻿using Afx.Business;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Business.Service;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using ContactManagement.Business;
using FleetManagement.Business;
using FleetManagement.Business.Service;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace FleetManagement.Prism.Dialogs.EditVehiclesDialog
{
  public class EditVehiclesDialogViewModel : MdiDialogViewModel<EditVehiclesDialogContext>
  {
    [InjectionConstructor]
    public EditVehiclesDialogViewModel(IController controller)
      : base(controller)
    {
    }

    [Dependency]
    public EditVehiclesDialogController EditVehiclesDialogController { get; set; }

    public IEnumerable<VehicleItemViewModel> Vehicles
    {
      get 
      {
        switch (BusinessObject.IsNull(Contractor))
        { 
          case false:
            if (!String.IsNullOrWhiteSpace(FilterText))
            {
              return ResolveViewModels<VehicleItemViewModel, Vehicle>(Contractor.GetExtensionObject<ContractorFleetDetail>().Vehicles.Where(
                v => v.MakeModel.ToUpperInvariant().Contains(FilterText.ToUpperInvariant()) 
                  || 
                  v.RegistrationNumber.ToUpperInvariant().Contains(FilterText.ToUpperInvariant()) 
                  || 
                  v.FleetNumber.ToUpperInvariant().Contains(FilterText.ToUpperInvariant())).ToList()
                  );
            }
            else
            {
              return ResolveViewModels<VehicleItemViewModel, Vehicle>(Contractor.GetExtensionObject<ContractorFleetDetail>().Vehicles);
            }
          default:
            if (!String.IsNullOrWhiteSpace(FilterText))
            {
              return ResolveViewModels<VehicleItemViewModel, OwnedVehicle>(Cache.Instance.GetObjects<OwnedVehicle>(
                v => v.MakeModel.ToUpperInvariant().Contains(FilterText.ToUpperInvariant()) 
                  || 
                  v.RegistrationNumber.ToUpperInvariant().Contains(FilterText.ToUpperInvariant()) 
                  || 
                  v.FleetNumber.ToUpperInvariant().Contains(FilterText.ToUpperInvariant())
                , v => v.FleetNumber
                , true));
            }
            else
            {
              return ResolveViewModels<VehicleItemViewModel, OwnedVehicle>(Cache.Instance.GetObjects<OwnedVehicle>(v => v.FleetNumber, true));
            }
        }
      }
    }

    public IEnumerable<VehicleUnit> VehicleUnits
    {
      get { return Cache.Instance.GetObjects<VehicleUnit>(v=> v.FleetUnitNumber.ToUpperInvariant().Contains(FilterText.ToUpperInvariant()), v => v.FleetUnitNumber, true); }
    }

    #region string FilterText

    public const string FilterTextProperty = "FilterText";
    string mFilterText;
    public string FilterText
    {
      get { return mFilterText; }
      set 
      { 
        if (SetProperty<string>(ref mFilterText, value)) 
        {
          OnPropertyChanged("Vehicles"); 
        } 
      }
    }

    #endregion

    #region Contractor Contractor

    public const string ContractorProperty = "Contractor";
    Contractor mContractor;
    public Contractor Contractor
    {
      get { return mContractor; }
      set 
      {
        if (SetProperty<Contractor>(ref mContractor, value))
        {
          OnPropertyChanged("Vehicles");
        }
      }
    }

    #endregion

    #region VehicleItemViewModel SelectedItemViewModel

    public const string SelectedItemViewModelProperty = "SelectedItemViewModel";
    VehicleItemViewModel mSelectedItemViewModel;
    public VehicleItemViewModel SelectedItemViewModel
    {
      get { return mSelectedItemViewModel; }
      set { SetProperty<VehicleItemViewModel>(ref mSelectedItemViewModel, value); }
    }

    #endregion

    #region DelegateCommand KeyDownCommand

    DelegateCommand<KeyEventArgs> mKeyDownCommand;
    public DelegateCommand<KeyEventArgs> KeyDownCommand
    {
      get { return mKeyDownCommand ?? (mKeyDownCommand = new DelegateCommand<KeyEventArgs>(ExecuteKeyDown)); }
    }

    void ExecuteKeyDown(KeyEventArgs e)
    {
      if (e.Key != Key.Space) return;
      if (SelectedItemViewModel != null) SelectedItemViewModel.IsChecked = !SelectedItemViewModel.IsChecked;
      e.Handled = true;
    }

    #endregion
  }
}

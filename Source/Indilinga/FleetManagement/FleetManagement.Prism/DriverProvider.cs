﻿using Afx.Business.Data;
using ContactManagement.Business;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfControls.Editors;

namespace FleetManagement.Prism
{
  public class DriverProvider : ISuggestionProvider
  {
    IEnumerable<Contact> mContacts = null;
    public IEnumerable GetSuggestions(string filter)
    {
      if (mContacts == null) mContacts = Cache.Instance.GetObjects<Contact>(ar => ar.ContactType.IsFlagged(FleetManagement.Business.ContactTypeFlags.Driver));
      return mContacts.Where(c => c.Fullname.ToUpperInvariant().Contains(filter.ToUpperInvariant()));
    }
  }
}

﻿using Afx.Prism;
using Afx.Prism.RibbonMdi.Documents;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FleetManagement.Prism.Extensions.DriverDetail
{
  public class DriverDetailViewModel : ExtensionViewModel<FleetManagement.Business.DriverDetail>
  {
    #region Constructors

    [InjectionConstructor]
    public DriverDetailViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion
  }
}

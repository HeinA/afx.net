﻿using Afx.Prism;
using Afx.Prism.RibbonMdi.Documents;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FleetManagement.Prism.Extensions.ContractorVehicles
{
  class ContractorVehiclesViewModel : ExtensionViewModel<FleetManagement.Business.ContractorFleetDetail>
  {
    #region Constructors

    [InjectionConstructor]
    public ContractorVehiclesViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    #region string Title

    public string Title
    {
      get { return "Vehicles"; }
    }

    #endregion
  }
}

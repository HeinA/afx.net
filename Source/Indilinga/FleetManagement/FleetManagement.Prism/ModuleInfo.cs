﻿using Afx.Prism;
using Afx.Prism.RibbonMdi;
using FleetManagement.Business;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FleetManagement.Prism
{
  [Export(typeof(IModuleInfo))]
  public class ModuleInfo : IModuleInfo
  {
    public string ModuleNamespace
    {
      get { return Namespace.FleetManagement; }
    }

    public string[] ModuleDependencies
    {
      get { return new string[] { Afx.Business.Namespace.Afx, AccountManagement.Business.Namespace.AccountManagement }; }
    }

    public Type ModuleController
    {
      get { return typeof(FleetManagementModuleController); }
    }
  }
}

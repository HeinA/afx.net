﻿using Afx.Prism;
using Afx.Prism.RibbonMdi;
using FleetManagement.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FleetManagement.Prism.Settings
{
  [Export(typeof(MdiSettingViewModel<ScaleSetting>))]
  public class ScaleSettingViewModel : MdiSettingViewModel<ScaleSetting>
  {
    #region Constructors

    [InjectionConstructor]
    public ScaleSettingViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion


    #region bool IsEnabled

    public bool IsEnabled
    {
      get { return Model == null ? GetDefaultValue<bool>() : Model.IsEnabled; }
      set { Model.IsEnabled = value; }
    }

    #endregion

    #region string ComPort

    public string ComPort
    {
      get { return Model == null ? GetDefaultValue<string>() : Model.ComPort; }
      set { Model.ComPort = value; }
    }

    #endregion

    #region int Baud

    public int Baud
    {
      get { return Model == null ? GetDefaultValue<int>() : Model.Baud; }
      set { Model.Baud = value; }
    }

    #endregion

    #region Parity Parity

    public Parity Parity
    {
      get { return Model == null ? GetDefaultValue<Parity>() : Model.Parity; }
      set { Model.Parity = value; }
    }

    #endregion

    #region int DataBits

    public int DataBits
    {
      get { return Model == null ? GetDefaultValue<int>() : Model.DataBits; }
      set { Model.DataBits = value; }
    }

    #endregion

    #region StopBits StopBits

    public StopBits StopBits
    {
      get { return Model == null ? GetDefaultValue<StopBits>() : Model.StopBits; }
      set { Model.StopBits = value; }
    }

    #endregion

  }
}

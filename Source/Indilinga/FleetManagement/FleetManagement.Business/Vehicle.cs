﻿using Afx.Business;
using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Validation;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace FleetManagement.Business
{
  [PersistantObject(Schema = "Fleet")]
  [Cache(Priority = 1)]
  public partial class Vehicle : BusinessObject, IRevisable
  {
    #region Constructors

    public Vehicle()
    {
    }

    public Vehicle(bool isNull)
      : base(isNull)
    {
    }

    #endregion

    #region int Revision

    public const string RevisionProperty = "Revision";
    [DataMember(Name = RevisionProperty, EmitDefaultValue = false)]
    int mRevision = 1;
    [PersistantProperty]
    public int Revision
    {
      get { return mRevision; }
      set { SetProperty<int>(ref mRevision, value); }
    }

    #endregion

    #region string FleetNumber

    public const string FleetNumberProperty = "FleetNumber";
    [DataMember(Name = FleetNumberProperty, EmitDefaultValue = false)]
    string mFleetNumber;
    [PersistantProperty]
    public string FleetNumber
    {
      get { return mFleetNumber; }
      set { SetProperty<string>(ref mFleetNumber, value); }
    }

    #endregion

    /// <summary>
    /// Field for the vehicle registration number or license plate number.
    /// </summary>
    #region string RegistrationNumber

    public const string RegistrationNumberProperty = "RegistrationNumber";
    [DataMember(Name = RegistrationNumberProperty, EmitDefaultValue = false)]
    string mRegistrationNumber;
    [PersistantProperty]
    [Mandatory("Registration Number is a mandatory field.")]
    public string RegistrationNumber
    {
      get { return mRegistrationNumber; }
      set { SetProperty<string>(ref mRegistrationNumber, value); }
    }

    #endregion

    /// <summary>
    /// Vehicle description field.
    /// </summary>
    #region string Text

    public string Text
    {
      get
      {
        if (GlobalIdentifier == Guid.Empty) return string.Empty;
        if (string.IsNullOrWhiteSpace(FleetNumber)) return RegistrationNumber;
        return string.Format("{0} ({1})", FleetNumber, RegistrationNumber);
      }
    }

    #endregion




    /// <summary>
    /// Field for the vehicle make.
    /// </summary>
    #region string Make

    public const string MakeProperty = "Make";
    [DataMember(Name = MakeProperty, EmitDefaultValue = false)]
    string mMake;
    [PersistantProperty]
    public string Make
    {
      get { return mMake; }
      set { SetProperty<string>(ref mMake, value); }
    }

    #endregion

    /// <summary>
    /// Field for the vehicle model.
    /// </summary>
    #region string Model

    public const string ModelProperty = "Model";
    [DataMember(Name = ModelProperty, EmitDefaultValue = false)]
    string mModel;
    [PersistantProperty]
    public string Model
    {
      get { return mModel; }
      set { SetProperty<string>(ref mModel, value); }
    }

    #endregion

    /// <summary>
    /// Readonly field which concatenates Make & Model
    /// </summary>
    #region string MakeModel

    public const string MakeModelProperty = "MakeModel";
    public string MakeModel
    {
      get { return Make + " " + Model; }
    }

    #endregion

    #region int Year

    public const string YearProperty = "Year";
    [DataMember(Name = YearProperty, EmitDefaultValue = false)]
    int mYear;
    [PersistantProperty]
    public int Year
    {
      get { return mYear; }
      set { SetProperty<int>(ref mYear, value); }
    }

    #endregion

    #region string Color

    public const string ColorProperty = "Color";
    [DataMember(Name = ColorProperty, EmitDefaultValue = false)]
    string mColor;
    [PersistantProperty]
    public string Color
    {
      get { return mColor; }
      set { SetProperty<string>(ref mColor, value); }
    }

    #endregion



    /// <summary>
    /// Field for the Vehicle Identification Number (VIN).
    /// </summary>
    #region string Vin

    public const string VinProperty = "Vin";
    [DataMember(Name = VinProperty, EmitDefaultValue = false)]
    string mVin;
    [PersistantProperty]
    public string Vin
    {
      get { return mVin; }
      set { SetProperty<string>(ref mVin, value); }
    }

    #endregion

    #region string VehicleBodyType

    public string VehicleBodyType
    {
      get
      {
        if (IsTruck && IsTrailer) return "Unibody";
        if (IsTruck) return "Truck";
        if (IsTrailer) return "Trailer";
        return string.Empty;
      }
    }

    #endregion

    #region string EngineNumber

    public const string EngineNumberProperty = "EngineNumber";
    [DataMember(Name = EngineNumberProperty, EmitDefaultValue = false)]
    string mEngineNumber;
    [PersistantProperty]
    public string EngineNumber
    {
      get { return mEngineNumber; }
      set { SetProperty<string>(ref mEngineNumber, value); }
    }

    #endregion





    /// <summary>
    /// Indicate if vehicle is a horse.
    /// </summary>
    #region bool IsTruck

    public const string IsTruckProperty = "IsTruck";
    [DataMember(Name = IsTruckProperty, EmitDefaultValue = false)]
    bool mIsTruck;
    [PersistantProperty]
    public bool IsTruck
    {
      get { return mIsTruck; }
      set { SetProperty<bool>(ref mIsTruck, value); }
    }

    #endregion

    /// <summary>
    /// Indicate if vehicle is a trailer.
    /// </summary>
    #region bool IsTrailer

    public const string IsTrailerProperty = "IsTrailer";
    [DataMember(Name = IsTrailerProperty, EmitDefaultValue = false)]
    bool mIsTrailer;
    [PersistantProperty]
    public bool IsTrailer
    {
      get { return mIsTrailer; }
      set { SetProperty<bool>(ref mIsTrailer, value); }
    }

    #endregion

    /// <summary>
    /// Is there a refrigeration unit installed?
    /// </summary>
    #region bool IsRefrigerated

    public const string IsRefrigeratedProperty = "IsRefrigerated";
    [DataMember(Name = IsRefrigeratedProperty, EmitDefaultValue = false)]
    bool mIsRefrigerated;
    [PersistantProperty]
    public bool IsRefrigerated
    {
      get { return mIsRefrigerated; }
      set { SetProperty<bool>(ref mIsRefrigerated, value); }
    }

    #endregion

    /// <summary>
    /// Is there a generator for refrigeration?
    /// </summary>
    #region bool HasGenerator

    public const string HasGeneratorProperty = "HasGenerator";
    [DataMember(Name = HasGeneratorProperty, EmitDefaultValue = false)]
    bool mHasGenerator;
    [PersistantProperty]
    public bool HasGenerator
    {
      get { return mHasGenerator; }
      set { SetProperty<bool>(ref mHasGenerator, value); }
    }

    #endregion



    /// <summary>
    /// Vehicle weight in kilograms.
    /// </summary>
    #region decimal VehicleWeight

    public const string VehicleWeightProperty = "VehicleWeight";
    [DataMember(Name = VehicleWeightProperty, EmitDefaultValue = false)]
    decimal mVehicleWeight;
    [PersistantProperty]
    public decimal VehicleWeight
    {
      get { return mVehicleWeight; }
      set { SetProperty<decimal>(ref mVehicleWeight, value); }
    }

    #endregion

    /// <summary>
    /// Maximum load weight in kilograms.
    /// </summary>
    #region decimal MaxLoadWeight

    public const string MaxLoadWeightProperty = "MaxLoadWeight";
    [DataMember(Name = MaxLoadWeightProperty, EmitDefaultValue = false)]
    decimal mMaxLoadWeight;
    [PersistantProperty]
    public decimal MaxLoadWeight
    {
      get { return mMaxLoadWeight; }
      set { SetProperty<decimal>(ref mMaxLoadWeight, value); }
    }

    #endregion

    /// <summary>
    /// What is the maximum loading volume.
    /// </summary>
    #region decimal MaxLoadVolume

    public const string MaxLoadVolumeProperty = "MaxLoadVolume";
    [DataMember(Name = MaxLoadVolumeProperty, EmitDefaultValue = false)]
    decimal mMaxLoadVolume;
    [PersistantProperty]
    public decimal MaxLoadVolume
    {
      get { return mMaxLoadVolume; }
      set { SetProperty<decimal>(ref mMaxLoadVolume, value); }
    }

    #endregion

    /// <summary>
    /// What is the maximum loading area.
    /// </summary>
    #region decimal MaxLoadArea

    public const string MaxLoadAreaProperty = "MaxLoadArea";
    [DataMember(Name = MaxLoadAreaProperty, EmitDefaultValue = false)]
    decimal mMaxLoadArea;
    [PersistantProperty]
    public decimal MaxLoadArea
    {
      get { return mMaxLoadArea; }
      set { SetProperty<decimal>(ref mMaxLoadArea, value); }
    }

    #endregion



    #region string Notes

    public const string NotesProperty = "Notes";
    [DataMember(Name = NotesProperty, EmitDefaultValue = false)]
    string mNotes;
    [PersistantProperty]
    public string Notes
    {
      get { return mNotes; }
      set { SetProperty<string>(ref mNotes, value); }
    }

    #endregion



  }
}
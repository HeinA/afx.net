﻿using Afx.Business;
using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using ContactManagement.Business;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace FleetManagement.Business
{
  [PersistantObject(Schema = "Fleet")]
  public partial class DriverDetail : ExtensionObject<Employee>
  {
    #region Constructors

    public DriverDetail()
    {
    }

    #endregion

    #region decimal OverNightRate

    public const string OverNightRateProperty = "OverNightRate";
    [DataMember(Name = OverNightRateProperty, EmitDefaultValue = false)]
    decimal mOverNightRate;
    [PersistantProperty]
    public decimal OverNightRate
    {
      get { return mOverNightRate; }
      set { SetProperty<decimal>(ref mOverNightRate, value); }
    }

    #endregion
  }
}

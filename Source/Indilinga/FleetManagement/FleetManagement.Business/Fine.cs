﻿using Afx.Business;
using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using ContactManagement.Business;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace FleetManagement.Business
{
  [PersistantObject(Schema = "Fleet")]
  public partial class Fine : BusinessObject<OwnedVehicle> 
  {
    #region Constructors

    public Fine()
    {
    }

    #endregion

    /// <summary>
    /// The date of the fina.
    /// </summary>
    #region DateTime Date

    public const string DateProperty = "Date";
    [DataMember(Name = DateProperty, EmitDefaultValue = false)]
    DateTime mDate;
    [PersistantProperty]
    public DateTime Date
    {
      get { return mDate; }
      set { SetProperty<DateTime>(ref mDate, value); }
    }

    #endregion

    /// <summary>
    /// Date the fine is due by.
    /// </summary>
    #region DateTime DueDate

    public const string DueDateProperty = "DueDate";
    [DataMember(Name = DueDateProperty, EmitDefaultValue = false)]
    DateTime mDueDate;
    [PersistantProperty]
    public DateTime DueDate
    {
      get { return mDueDate; }
      set { SetProperty<DateTime>(ref mDueDate, value); }
    }

    #endregion

    /// <summary>
    /// The amount of the fine.
    /// </summary>
    #region decimal Amount

    public const string AmountProperty = "Amount";
    [DataMember(Name = AmountProperty, EmitDefaultValue = false)]
    decimal mAmount;
    [PersistantProperty]
    public decimal Amount
    {
      get { return mAmount; }
      set { SetProperty<decimal>(ref mAmount, value); }
    }

    #endregion

    /// <summary>
    /// The driver who committed the offense.
    /// </summary>
    #region Contact Driver

    public const string DriverProperty = "Driver";
    [PersistantProperty]
    public Contact Driver
    {
      get { return GetCachedObject<Contact>(); }
      set { SetCachedObject<Contact>(value); }
    }

    #endregion

  }
}

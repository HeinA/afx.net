﻿using Afx.Business;
using ContactManagement.Business;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FleetManagement.Business
{
  [Export(typeof(IObjectFlags))]
  public class ContactTypeFlags : IObjectFlags
  {
    public const string Driver = "FleetManagement.Driver";

    public Type ObjectType
    {
      get { return typeof(ContactType); }
    }

    public IEnumerable<string> Flags
    {
      get { return new string[] { Driver }; }
    }
  }
}

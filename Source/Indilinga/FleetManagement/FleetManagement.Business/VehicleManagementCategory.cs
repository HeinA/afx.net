﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FleetManagement.Business
{
  public enum VehicleManagementCategory
  {
    None = 0,
    Primary = 1,
    Secondary = 2
  }
}

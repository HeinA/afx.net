﻿using Afx.Business.Collections;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FleetManagement.Business
{
  public interface IVehicleCollection
  {
    BusinessObjectCollection<Vehicle> Vehicles
    {
      get;
    }
  }
}

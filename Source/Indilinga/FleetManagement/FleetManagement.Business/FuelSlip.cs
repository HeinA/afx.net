﻿using Afx.Business;
using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace FleetManagement.Business
{
  [PersistantObject(Schema = "Fleet")]
  public partial class FuelSlip : BusinessObject<OwnedVehicle> 
  {
    #region Constructors

    public FuelSlip()
    {
    }

    #endregion

    /// <summary>
    /// The date of fuelling.
    /// </summary>
    #region DateTime Date

    public const string DateProperty = "Date";
    [DataMember(Name = DateProperty, EmitDefaultValue = false)]
    DateTime mDate;
    [PersistantProperty]
    public DateTime Date
    {
      get { return mDate; }
      set { SetProperty<DateTime>(ref mDate, value); }
    }

    #endregion

    /// <summary>
    /// The Odo meter reading at time of fuelling.
    /// </summary>
    #region decimal OdoMeter

    public const string OdoMeterProperty = "OdoMeter";
    [DataMember(Name = OdoMeterProperty, EmitDefaultValue = false)]
    decimal mOdoMeter;
    [PersistantProperty]
    public decimal OdoMeter
    {
      get { return mOdoMeter; }
      set { SetProperty<decimal>(ref mOdoMeter, value); }
    }

    #endregion

    /// <summary>
    /// The amount of liters of fuel uplifted.
    /// </summary>
    #region decimal AmountLiters

    public const string AmountLitersProperty = "AmountLiters";
    [DataMember(Name = AmountLitersProperty, EmitDefaultValue = false)]
    decimal mAmountLiters;
    [PersistantProperty]
    public decimal AmountLiters
    {
      get { return mAmountLiters; }
      set { SetProperty<decimal>(ref mAmountLiters, value); }
    }

    #endregion

    /// <summary>
    /// The monetary value uplifted.
    /// </summary>
    #region decimal AmountMoney

    public const string AmountMoneyProperty = "AmountMoney";
    [DataMember(Name = AmountMoneyProperty, EmitDefaultValue = false)]
    decimal mAmountMoney;
    [PersistantProperty]
    public decimal AmountMoney
    {
      get { return mAmountMoney; }
      set { SetProperty<decimal>(ref mAmountMoney, value); }
    }

    #endregion
  }
}

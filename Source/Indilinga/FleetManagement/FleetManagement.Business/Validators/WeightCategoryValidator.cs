﻿using Afx.Business;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FleetManagement.Business.Validators
{
  [Export(typeof(ICollectionValidator))]
  public class WeightCategoryValidator : CollectionValidator<WeightCategory>
  {
    public override void GetErrors(System.Collections.ObjectModel.Collection<WeightCategory> col, System.Collections.ObjectModel.Collection<string> errors)
    {
      int tareCount = 0;
      int finalCount = 0;
      foreach (WeightCategory c in col)
      {
        if (c.IsTare) tareCount++;
        if (c.IsFinal) finalCount++;
      }
      if (tareCount != 1) errors.Add("There must be exactly 1 weight category with 'Is Tare' specified.");
      if (finalCount != 1) errors.Add("There must be exactly 1 weight category with 'Is Final' specified.");
    }
  }
}

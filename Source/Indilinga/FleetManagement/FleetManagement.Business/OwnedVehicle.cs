﻿using Afx.Business;
using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Validation;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace FleetManagement.Business
{
  [PersistantObject(Schema = "Fleet")]
  //[Cache]
  public partial class OwnedVehicle : Vehicle, IExternalReferenceCollection 
  {
    #region Constructors

    public OwnedVehicle()
    {
    }

    public OwnedVehicle(bool isNull)
      : base(isNull)
    {
    }

    #endregion

    #region "Servicing Information"

    /// <summary>
    /// When was the last service performed.
    /// </summary>
    #region DateTime? LastService

    public const string LastServiceProperty = "LastService";
    [DataMember(Name = LastServiceProperty, EmitDefaultValue = false)]
    DateTime? mLastService;
    [PersistantProperty]
    public DateTime? LastService
    {
      get { return mLastService; }
      set { SetProperty<DateTime?>(ref mLastService, value); }
    }

    #endregion

    /// <summary>
    /// The ODO meter reading at last service.
    /// </summary>
    #region decimal LastServiceKilometers

    public const string LastServiceKilometersProperty = "LastServiceKilometers";
    [DataMember(Name = LastServiceKilometersProperty, EmitDefaultValue = false)]
    decimal mLastServiceKilometers;
    [PersistantProperty]
    public decimal LastServiceKilometers
    {
      get { return mLastServiceKilometers; }
      set { SetProperty<decimal>(ref mLastServiceKilometers, value); }
    }

    #endregion

    /// <summary>
    /// How long in kilometers before next service.
    /// </summary>
    #region decimal ServiceIntervalKilometers

    public const string ServiceIntervalKilometersProperty = "ServiceIntervalKilometers";
    [DataMember(Name = ServiceIntervalKilometersProperty, EmitDefaultValue = false)]
    decimal mServiceIntervalKilometers;
    [PersistantProperty]
    public decimal ServiceIntervalKilometers
    {
      get { return mServiceIntervalKilometers; }
      set { SetProperty<decimal>(ref mServiceIntervalKilometers, value); }
    }

    #endregion

    #endregion

    #region string GPSId

    public const string GPSIdProperty = "GPSId";
    [DataMember(Name = GPSIdProperty, EmitDefaultValue = false)]
    string mGPSId;
    [PersistantProperty]
    public string GPSId
    {
      get { return mGPSId; }
      set { SetProperty<string>(ref mGPSId, value); }
    }

    #endregion

    #region BusinessObjectCollection<Fine> Fines

    public const string FinesProperty = "Fines";
    BusinessObjectCollection<Fine> mFines;
    [PersistantCollection]
    [DataMember]
    public BusinessObjectCollection<Fine> Fines
    {
      get { return mFines ?? (mFines = new BusinessObjectCollection<Fine>(this)); }
    }

    #endregion

    #region BusinessObjectCollection<FuelSlip> FuelSlips

    public const string FuelSlipsProperty = "FuelSlips";
    BusinessObjectCollection<FuelSlip> mFuelSlips;
    [PersistantCollection]
    [DataMember]
    public BusinessObjectCollection<FuelSlip> FuelSlips
    {
      get { return mFuelSlips ?? (mFuelSlips = new BusinessObjectCollection<FuelSlip>(this)); }
    }

    #endregion

    #region BusinessObjectCollection<VehicleReference> ExternalReferences

    public const string ExternalReferencesProperty = "ExternalReferences";
    BusinessObjectCollection<VehicleReference> mExternalReferences;
    [PersistantCollection]
    [DataMember]
    public BusinessObjectCollection<VehicleReference> ExternalReferences
    {
      get { return mExternalReferences ?? (mExternalReferences = new BusinessObjectCollection<VehicleReference>(this)); }
    }

    #endregion

    #region void OnCompositionChanged(...)

    protected override void OnCompositionChanged(CompositionChangedType compositionChangedType, string propertyName, object originalSource, BusinessObject item)
    {
      if ((compositionChangedType == CompositionChangedType.ChildAdded || compositionChangedType == CompositionChangedType.ChildRemoved) && propertyName == ExternalReferencesProperty)
      {
        OnPropertyChanged("ExternalReferenceText");
      }
      base.OnCompositionChanged(compositionChangedType, propertyName, originalSource, item);
    }

    #endregion

    #region IExternalReferenceCollection

    public string ExternalReferenceText
    {
      get { return ExternalReferenceHelper.GetReferenceText(this); }
    }

    IList IExternalReferenceCollection.ExternalReferences
    {
      get { return ExternalReferences; }
    }

    IExternalReference IExternalReferenceCollection.CreateReference(ExternalSystem es, string reference)
    {
      return new VehicleReference() { ExternalSystem = es, Reference = reference };
    }

    #endregion

  }
}

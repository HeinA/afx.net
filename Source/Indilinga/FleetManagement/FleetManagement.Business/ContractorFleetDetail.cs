﻿using Afx.Business;
using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using ContactManagement.Business;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace FleetManagement.Business
{
  [PersistantObject(Schema = "Fleet")]
  public partial class ContractorFleetDetail : ExtensionObject<Contractor>
  {
    #region Constructors

    public ContractorFleetDetail()
    {
    }

    #endregion

    #region Associative BusinessObjectCollection<Vehicle> Vehicles

    public const string VehiclesProperty = "Vehicles";
    AssociativeObjectCollection<ContractorVehicle, Vehicle> mVehicles;
    [PersistantCollection(AssociativeType = typeof(ContractorVehicle))]
    public BusinessObjectCollection<Vehicle> Vehicles
    {
      get
      {
        if (mVehicles == null) using (new EventStateSuppressor(this)) { mVehicles = new AssociativeObjectCollection<ContractorVehicle, Vehicle>(this); }
        return mVehicles;
      }
    }

    #endregion
  }
}

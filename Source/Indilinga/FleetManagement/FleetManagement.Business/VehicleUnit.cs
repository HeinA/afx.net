﻿using Afx.Business;
using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Security;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace FleetManagement.Business
{
  [PersistantObject(Schema = "Fleet")]
  [Cache]
  public partial class VehicleUnit : BusinessObject, IRevisable
  {
    #region int Revision

    public const string RevisionProperty = "Revision";
    [DataMember(Name = RevisionProperty, EmitDefaultValue = false)]
    int mRevision = 1;
    [PersistantProperty]
    public int Revision
    {
      get { return mRevision; }
      set { SetProperty<int>(ref mRevision, value); }
    }

    #endregion

    #region Constructors

    public VehicleUnit()
    {
    }

    public VehicleUnit(bool isNull)
      : base(isNull)
    {
    }

    #endregion

    #region string FleetUnitNumber

    public const string FleetUnitNumberProperty = "FleetUnitNumber";
    [DataMember(Name = FleetUnitNumberProperty, EmitDefaultValue = false)]
    string mFleetUnitNumber;
    [PersistantProperty]
    public string FleetUnitNumber
    {
      get { return mFleetUnitNumber; }
      set { SetProperty<string>(ref mFleetUnitNumber, value); }
    }

    #endregion

    #region Associative BusinessObjectCollection<Vehicle> Vehicles

    public const string VehiclesProperty = "Vehicles";
    AssociativeObjectCollection<VehicleUnitVehicle, Vehicle> mVehicles;
    [PersistantCollection(AssociativeType = typeof(VehicleUnitVehicle))]
    public BusinessObjectCollection<Vehicle> Vehicles
    {
      get
      {
        if (mVehicles == null) using (new EventStateSuppressor(this)) { mVehicles = new AssociativeObjectCollection<VehicleUnitVehicle, Vehicle>(this); }
        return mVehicles;
      }
    }

    #endregion

    #region Associative BusinessObjectCollection<OrganizationalUnit> ApplicableOrganizationalUnits

    public const string ApplicableOrganizationalUnitsProperty = "ApplicableOrganizationalUnits";
    AssociativeObjectCollection<VehicleUnitOrganizationalUnit, OrganizationalUnit> mApplicableOrganizationalUnits;
    [PersistantCollection(AssociativeType = typeof(VehicleUnitOrganizationalUnit))]
    public BusinessObjectCollection<OrganizationalUnit> ApplicableOrganizationalUnits
    {
      get
      {
        if (mApplicableOrganizationalUnits == null) using (new EventStateSuppressor(this)) { mApplicableOrganizationalUnits = new AssociativeObjectCollection<VehicleUnitOrganizationalUnit, OrganizationalUnit>(this); }
        return mApplicableOrganizationalUnits;
      }
    }

    #endregion
  }
}

﻿using Afx.Business;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Service;
using ContactManagement.Business;
using FileHelpers;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.WCF;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data;
using System.Data.Odbc;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace FleetManagement.Business.Service
{
  [ServiceBehavior(Namespace = Namespace.FleetManagement, ConcurrencyMode = ConcurrencyMode.Multiple)]
  [ExceptionShielding]
  [Export(typeof(IService))]
  public partial class FleetManagementService : ServiceBase, IFleetManagementService
  {
    #region Import

    public BasicCollection<OwnedVehicle> ImportVehicles(ExternalSystem es)
    {
      try
      {
        using (new SqlScope("ImportVehicles"))
        {
          DataSet ds = null;
          using (OdbcConnection con = new OdbcConnection(es.ConnectionString))
          {
            con.Open();
            using (OdbcCommand cmd = new OdbcCommand("SELECT VT.STVDESCRIPTION, V.* FROM VEHICULES V inner join SF_TYPEVEHICULE VT on VT.STVNOSEQ=V.VENOSF_TYPEVEHICULE WHERE V.VEINACTIF=0", con))
            {
              ds = DataHelper.ExecuteDataSet(cmd);
            }
          }

          BasicCollection<OwnedVehicle> col = GetAllInstances<OwnedVehicle>();

          foreach (DataRow dr in ds.Tables[0].Rows)
          {
            OwnedVehicle v = col.FirstOrDefault(ic1 => ic1.ExternalReferences.Any(er => er.ExternalSystem.Equals(es) && er.Reference == ((int)dr["VENOSEQ"]).ToString()));
            if (v == null)
            {
              string regno = ((string)dr["VEIMMATRICULATION"]).Trim();
              if (string.IsNullOrWhiteSpace(regno)) continue;
              v = new OwnedVehicle();
              v.RegistrationNumber = regno;
              v.Make = ((string)dr["VEFABRICANT"]).Trim();
              v.Model = ((string)dr["VEMODELE"]).Trim();
              v.Vin = ((string)dr["VENOSERIE"]).Trim();
              v.FleetNumber = ((string)dr["VENUMEROUNITE"]).Trim();
              v.Notes = ((string)dr["STVDESCRIPTION"]).Trim();
              v.ExternalReferences.Add(new VehicleReference() { ExternalSystem = es, Reference = ((int)dr["VENOSEQ"]).ToString() });
              col.Add(v);
            }
            else
            {
              string regno = ((string)dr["VEIMMATRICULATION"]).Trim();
              if (string.IsNullOrWhiteSpace(regno)) continue;
              v.RegistrationNumber = regno;
              v.Make = ((string)dr["VEFABRICANT"]).Trim();
              v.Model = ((string)dr["VEMODELE"]).Trim();
              v.Vin = ((string)dr["VENOSERIE"]).Trim();
              v.FleetNumber = ((string)dr["VENUMEROUNITE"]).Trim();
              if (string.IsNullOrWhiteSpace(v.Notes)) v.Notes = ((string)dr["STVDESCRIPTION"]).Trim();
            }
          }

          return col;
        }
      }
      catch (Exception ex)
      {
        throw new ImportException("An error occured during the import.", ex);
      }
    }

    public BasicCollection<OwnedVehicle> ImportVehiclesCsv(byte[] csvfile)
    {
      try
      {
        BasicCollection<OwnedVehicle> vehicles = new BasicCollection<OwnedVehicle>();

        var engine = new FileHelperEngine<VehicleCsvRecord>();
        using (var ms = new MemoryStream(csvfile))
        {
          using (var sr = new StreamReader(ms))
          {
            var records = engine.ReadStream(sr);

            foreach (var r in records)
            {
              var e = new OwnedVehicle() { FleetNumber = r.FleetNumber, RegistrationNumber = r.RegistrationNumber, Make = r.Make, Model = r.Model, EngineNumber = r.EngineNumber };

              vehicles.Add(e);
            }

            return Persist<OwnedVehicle>(vehicles);
          }
        }
      }
      catch
      {
        throw;
      }
    }

    #endregion

  }
}

﻿using Afx.Business;
using Afx.Business.Collections;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace FleetManagement.Business.Service
{
  [ServiceContract(Namespace = Namespace.FleetManagement)]
  public partial interface IFleetManagementService : IDisposable
  {
    #region Import

    [OperationContract]
    BasicCollection<OwnedVehicle> ImportVehicles(ExternalSystem es);

    [OperationContract]
    BasicCollection<OwnedVehicle> ImportVehiclesCsv(byte[] csvfile);

    #endregion

  }
}

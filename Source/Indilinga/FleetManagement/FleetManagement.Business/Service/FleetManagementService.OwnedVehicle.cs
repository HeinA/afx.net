﻿using Afx.Business.Collections;
using Afx.Business.Service;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.WCF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace FleetManagement.Business.Service
{
  public partial class FleetManagementService
  {
    public BasicCollection<FleetManagement.Business.OwnedVehicle> LoadOwnedVehicleCollection()
    {
      try
      {
        return GetAllInstances<FleetManagement.Business.OwnedVehicle>();
      }
      catch
      {
        throw;
      }
    }

    public BasicCollection<FleetManagement.Business.OwnedVehicle> SaveOwnedVehicleCollection(BasicCollection<FleetManagement.Business.OwnedVehicle> col)
    {
      try
      {
        return Persist<FleetManagement.Business.OwnedVehicle>(col);
      }
      catch
      {
        throw;
      }
    }
  }
}
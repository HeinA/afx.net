﻿using Afx.Business.Collections;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace FleetManagement.Business.Service
{
  public partial interface IFleetManagementService
  {
    [OperationContract]
    BasicCollection<FleetManagement.Business.VehicleUnit> LoadVehicleUnitCollection();

    [OperationContract]
    BasicCollection<FleetManagement.Business.VehicleUnit> SaveVehicleUnitCollection(BasicCollection<FleetManagement.Business.VehicleUnit> col);
  }
}


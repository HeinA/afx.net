﻿using Afx.Business;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FleetManagement.Business
{
  [Export(typeof(INamespace))]
  public class Namespace : INamespace
  {
    public const string FleetManagement = "http://indilinga.com/FleetManagement/Business";

    public string GetUri()
    {
      return FleetManagement;
    }
  }
}

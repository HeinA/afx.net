﻿using FileHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FleetManagement.Business
{
  [DelimitedRecord(",")]
  [IgnoreFirst]
  class VehicleCsvRecord
  {
    public string FleetNumber;
    public string RegistrationNumber;
    public string Vin;
    public string EngineNumber;
    public string Model;
    public string Make;
  }
}

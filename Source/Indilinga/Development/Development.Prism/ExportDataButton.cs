﻿using Afx.Business;
using Afx.Business.Activities;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Business.Service;
using Afx.Prism.RibbonMdi;
using Afx.Prism.RibbonMdi.Ribbon;
using Development.Business.Service;
using Development.Prism;
using Development.Prism.Dialogs.SelectExportTypes;
using Microsoft.Practices.Unity;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Development.Prism
{
  [Export(typeof(IRibbonItem))]
  public class ExportDataButton : RibbonButtonViewModel
  {
    public override string TabName
    {
      get { return DevelopmentTab.TabName; }
    }

    public override string GroupName
    {
      get { return ToolsGroup.GroupName; }
    }

    public override string ItemIdentifier
    {
      get { return "ExportApplicationSettingsButton"; }
    }

    public override int Index
    {
      get { return 0; }
    }

    [Dependency]
    public IMdiModuleController ModuleController { get; set; }

    protected override void OnExecute()
    {
      SelectExportTypesController dc = ModuleController.CreateChildController<SelectExportTypesController>();
      dc.Run();
    }
  }
}

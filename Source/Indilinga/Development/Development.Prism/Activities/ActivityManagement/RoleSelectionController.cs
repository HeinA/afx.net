﻿using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace Development.Prism.Activities.ActivityManagement
{
  public class RoleSelectionController : MdiDialogController<RoleSelectionContext, RoleSelectionViewModel>
  {
    public const string RoleSelectionDialogControllerKey = "Role Selection Controller";

    [InjectionConstructor]
    public RoleSelectionController(IController controller)
      : base(RoleSelectionDialogControllerKey, controller)
    {
    }

    OperationFolderItemViewModel mSelectedOperationViewModel;
    public OperationFolderItemViewModel SelectedOperationViewModel
    {
      get { return mSelectedOperationViewModel; }
      set { mSelectedOperationViewModel = value; }
    }

    protected override void OnTerminated()
    {
      SelectedOperationViewModel.IsFocused = true;
      base.OnTerminated();
    }

    protected override void ApplyChanges()
    {
      foreach (Role r in Cache.Instance.GetObjects<Role>())
      {
        if (DataContext.Roles.Contains(r) && !DataContext.OperationRoles.Contains(r)) DataContext.OperationRoles.Add(r);
        if (!DataContext.Roles.Contains(r) && DataContext.OperationRoles.Contains(r)) DataContext.OperationRoles.Remove(r);
      }
      SelectedOperationViewModel.UpdateAllowedRoles();
      base.ApplyChanges();
    }
  }
}

﻿using Afx.Business.Activities;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Development.Prism.Activities.ActivityManagement
{
  public class EditPermissionsController : MdiDialogController<EditPermissionsContext, EditPermissionsViewModel>
  {
    public const string EditPermissionsControllerKey = "Edit Permissions Controller";

    [InjectionConstructor]
    public EditPermissionsController(IController controller)
      : base(EditPermissionsControllerKey, controller)
    {
    }

    bool mHasEdit;
    public bool HasEdit
    {
      get { return mHasEdit; }
      set { mHasEdit = value; }
    }

    bool mHasView;
    public bool HasView
    {
      get { return mHasView; }
      set
      {
        mHasView = value;
        if (!HasView) HasEdit = false;
      }
    }

    void UpdateLocalPermissions()
    {
      if (DataContext == null || SelectedRoleViewModel == null) return;
      HasView = DataContext.Activity.Roles.Contains(SelectedRoleViewModel.Model);
      if (!HasView) HasEdit = false;
      else HasEdit = DataContext.Activity.GetAssociativeObject<ActivityRole>(SelectedRoleViewModel.Model).EditPermission;
    }

    ActivityRoleItemViewModel mSelectedRoleViewModel;
    public ActivityRoleItemViewModel SelectedRoleViewModel
    {
      get { return mSelectedRoleViewModel; }
      set
      {
        mSelectedRoleViewModel = value;
        UpdateLocalPermissions();
      }
    }

    protected override void OnDataContextChanged()
    {
      UpdateLocalPermissions();
      base.OnDataContextChanged();
    }

    protected override void ApplyChanges()
    {
      if (HasView)
      {
        if (!DataContext.Activity.Roles.Contains(SelectedRoleViewModel.Model))
        {
          DataContext.Activity.Roles.Add(SelectedRoleViewModel.Model);
        }
      }
      else
      {
        if (DataContext.Activity.Roles.Contains(SelectedRoleViewModel.Model))
        {
          DataContext.Activity.Roles.Remove(SelectedRoleViewModel.Model);
        }
      }

      if (DataContext.Activity.Roles.Contains(SelectedRoleViewModel.Model))
      {
        DataContext.Activity.GetAssociativeObject<ActivityRole>(SelectedRoleViewModel.Model).EditPermission = HasEdit;
      }

      SelectedRoleViewModel.UpdatePermissions();
      base.ApplyChanges();
    }

    protected override void OnTerminated()
    {
      SelectedRoleViewModel.IsFocused = true;
      base.OnTerminated();
    }
  }
}

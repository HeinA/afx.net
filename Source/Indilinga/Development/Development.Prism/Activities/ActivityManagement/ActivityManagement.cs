﻿using Afx.Business.Activities;
using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Business.Service;
using Afx.Prism.RibbonMdi.Activities;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Development.Prism.Activities.ActivityManagement
{
  [Export("Context:" + ActivityManagement.Key)]
  public class ActivityManagement : ContextualActivityContext<Activity>
  {
    public const string Key = "{8d92749e-d315-4ff3-8c02-720dbf1bb826}";
    public const string ContextRegion = "ContextRegion";
    public const string FillRegion = "FillRegion";

    public ActivityManagement()
    {
    }

    public ActivityManagement(ContextualActivity activity)
      : base(activity)
    {
    }

    public override void LoadData()
    {
      using (var svc = ProxyFactory.GetService<IAfxService>(SecurityContext.MasterServer.Name))
      {
        Data = svc.LoadActivities();
      }
      base.LoadData();
    }

    public override void SaveData()
    {
      using (var svc = ProxyFactory.GetService<IAfxService>(SecurityContext.MasterServer.Name))
      {
        Data = svc.SaveActivities(Data);
      }
      base.SaveData();
    }
  }
}

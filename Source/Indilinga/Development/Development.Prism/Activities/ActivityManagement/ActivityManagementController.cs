﻿using Afx.Business;
using Afx.Business.Activities;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Activities;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Development.Prism.Activities.ActivityManagement
{
  [Export("Controller:" + Development.Prism.Activities.ActivityManagement.ActivityManagement.Key)]
  public class ActivityManagementController : MdiContextualActivityController<ActivityManagement, ActivityManagementViewModel>
  {
    [InjectionConstructor]
    public ActivityManagementController(IController controller)
      : base(controller)
    {
    }

    protected override void OnLoaded()
    {
      if (DataContext.Data.Count > 0)
      {
        //Focus the first element 
        ActivityNodeViewModel nvm = GetCreateViewModel<ActivityNodeViewModel>(DataContext.Data[0], ViewModel);
        nvm.IsFocused = true;
      }

      RegisterContextRegion(RegionManager.Regions[ActivityManagement.ContextRegion]);
      RegisterContextRegion(RegionManager.Regions[ActivityManagement.FillRegion]);

      base.OnLoaded();
    }

    protected override void OnContextViewModelChanged(ISelectableViewModel context)
    {
      if (context.Model is Activity)
      {
        ActivityDetailViewModel vm = GetCreateViewModel<ActivityDetailViewModel>(context.Model, ViewModel);
        RegionManager.Regions[ActivityManagement.ContextRegion].Add(vm);

        ActivityRolesDetailViewModel vm1 = GetCreateViewModel<ActivityRolesDetailViewModel>(context.Model, ViewModel);
        RegionManager.Regions[ActivityManagement.FillRegion].Add(vm1);
      }

      if (context.Model is OperationFolder)
      {
        OperationFolderDetailViewModel vm = GetCreateViewModel<OperationFolderDetailViewModel>(context.Model, ViewModel);
        RegionManager.Regions[ActivityManagement.FillRegion].Add(vm);
      }

      base.OnContextViewModelChanged(context);
    }

    protected override void ExecuteOperation(Operation op, BusinessObject argument)
    {
      switch (op.Identifier)
      {
        case Operations.AddContextualActivity:
          {
            Activity a = new ContextualActivity();
            DataContext.Data.Add(a);
            SelectContextViewModel(a);
            FocusViewModel<ActivityDetailViewModel>(a);
          }
          break;

        case Operations.AddSimpleActivity:
          {
            Activity a = new SimpleActivity();
            DataContext.Data.Add(a);
            SelectContextViewModel(a);
            FocusViewModel<ActivityDetailViewModel>(a);
          }
          break;

        case Operations.AddOperation:
          AddOperationsController aoc = CreateChildController<AddOperationsController>();
          aoc.DataContext = (OperationFolder)argument;
          aoc.SelectedNode = ContextViewModel; 
          aoc.Run();
          break;
      }

      base.ExecuteOperation(op, argument);
    }
  }
}

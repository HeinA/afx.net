﻿using Afx.Business.Activities;
using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Development.Prism.Activities.ActivityManagement
{
  public class RoleSelectionViewModel : MdiDialogViewModel<RoleSelectionContext>
  {
    [InjectionConstructor]
    public RoleSelectionViewModel(IController controller)
      : base(controller)
    {
    }

    public IEnumerable<RoleSelectionItemViewModel> Roles
    {
      get { return ResolveViewModels<RoleSelectionItemViewModel, Role>(Cache.Instance.GetObjects<Role>((r) => r.Name, true)); }
    }

    RoleSelectionItemViewModel mSelectedItemViewModel;
    public RoleSelectionItemViewModel SelectedItemViewModel
    {
      get { return mSelectedItemViewModel; }
      set { mSelectedItemViewModel = value; }
    }

    #region DelegateCommand KeyDownCommand

    DelegateCommand<KeyEventArgs> mKeyDownCommand;
    public DelegateCommand<KeyEventArgs> KeyDownCommand
    {
      get { return mKeyDownCommand ?? (mKeyDownCommand = new DelegateCommand<KeyEventArgs>(ExecuteKeyDown)); }
    }

    void ExecuteKeyDown(KeyEventArgs e)
    {
      if (e.Key != Key.Space) return;
      if (SelectedItemViewModel != null) SelectedItemViewModel.HasRole = !SelectedItemViewModel.HasRole;
      e.Handled = true;
    }

    #endregion
  }
}

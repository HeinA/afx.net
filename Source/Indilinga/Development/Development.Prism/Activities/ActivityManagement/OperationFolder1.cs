﻿using Afx.Business.Activities;
using Afx.Business.Collections;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Activities.ActivityManagement
{
  public class OperationFolder1 : NullContext
  {
    public OperationFolder1(Guid identifier)
      : base(identifier)
    {
    }

    BasicCollection<ContextOperation> mOperations;
    public BasicCollection<ContextOperation> Operations
    {
      get { return mOperations; }
      set { SetValue<BasicCollection<ContextOperation>>(ref mOperations, value); }
    }
  }
}

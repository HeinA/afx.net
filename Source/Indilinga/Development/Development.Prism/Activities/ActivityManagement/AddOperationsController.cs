﻿using Afx.Business;
using Afx.Business.Activities;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Development.Prism.Activities.ActivityManagement
{
  public class AddOperationsController : MdiDialogController<OperationFolder, AddOperationsViewModel>
  {
    public const string AddOperationsControllerKey = "Add Operations Controller";

    [InjectionConstructor]
    public AddOperationsController(IController controller)
      : base(AddOperationsControllerKey, controller)
    {
    }

    BasicCollection<Operation> mAvailableOperations;
    public BasicCollection<Operation> AvailableOperations
    {
      get
      {
        if (mAvailableOperations == null)
        {
          mAvailableOperations = Cache.Instance.GetObjects<Operation>((o) => DataContext.IsContext ? typeof(ContextOperation).IsAssignableFrom(o.GetType()) : !typeof(ContextOperation).IsAssignableFrom(o.GetType()), (o) => o.Text, true);
          foreach (var o in mAvailableOperations.ToList())
          {
            if (DataContext.Activity.GlobalOperations.Contains(o)) mAvailableOperations.Remove(o);
            ContextualActivity ca = DataContext.Activity as ContextualActivity;
            if (ca != null && ca.ContextOperations.Contains(o)) mAvailableOperations.Remove(o);
          }
        }
        return mAvailableOperations;
      }
    }

    BasicCollection<Operation> mSelectedOperations = new BasicCollection<Operation>();
    public BasicCollection<Operation> SelectedOperations
    {
      get { return mSelectedOperations; }
    }

    public ISelectableViewModel SelectedNode { get; set; }

    protected override void ApplyChanges()
    {
      foreach (var o in SelectedOperations)
      {
        DataContext.Operations.Add(o);        
      }

      base.ApplyChanges();
    }

    protected override void OnTerminated()
    {
      if (SelectedNode != null) SelectedNode.IsFocused = true;
      base.OnTerminated();
    }
  }
}

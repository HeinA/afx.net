﻿using Afx.Business.Activities;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Activities;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Development.Prism.Activities.ActivityManagement
{
  public class ActivityManagementViewModel : MdiContextualActivityViewModel<ActivityManagement>
  {
    [InjectionConstructor]
    public ActivityManagementViewModel(IController controller)
      : base(controller)
    {
    }

    public IEnumerable<ActivityNodeViewModel> Activities
    {
      get
      {
        if (Model == null) return null;
        return this.ResolveViewModels<ActivityNodeViewModel, Activity>(Model.Data);
      }
    }
  }
}

﻿using Afx.Business.Activities;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Development.Prism.Activities.ActivityManagement
{
  public class ActivityTabNodeViewModel : MdiNavigationTreeNodeViewModel<ActivityTab>
  {
    [InjectionConstructor]
    public ActivityTabNodeViewModel(IController controller)
      : base(controller)
    {
    }

    public string Name
    {
      get
      {
        if (Model == null) return string.Empty;
        if (string.IsNullOrWhiteSpace(Model.Name)) return "*** Unnamed ***";
        return Model.Name;
      }
    }

    public IEnumerable<ActivityTabGroupNodeViewModel> Groups
    {
      get { return this.ResolveViewModels<ActivityTabGroupNodeViewModel, ActivityGroup>(Model.Groups); }
    }
  }
}

﻿using Afx.Business.Activities;
using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Activities;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Development.Prism.Activities.ActivityManagement
{
  public class ActivityRolesDetailViewModel : ActivityDetailViewModel<Activity>
  {
    [InjectionConstructor]
    public ActivityRolesDetailViewModel(IController controller)
      : base(controller)
    {
    }

    public IEnumerable<ActivityRoleItemViewModel> Roles
    {
      get { return ResolveViewModels<ActivityRoleItemViewModel, Role>(Cache.Instance.GetObjects<Role>((r) => r.Name, true)); }
    }

    ActivityRoleItemViewModel mSelectedRoleViewModel;
    public ActivityRoleItemViewModel SelectedRoleViewModel
    {
      get { return mSelectedRoleViewModel; }
      set
      {
        mSelectedRoleViewModel = value;
        EditPermissionsCommand.RaiseCanExecuteChanged();
      }
    }

    #region DelegateCommand EditPermissionsCommand

    DelegateCommand mEditPermissionsCommand;
    public DelegateCommand EditPermissionsCommand
    {
      get { return mEditPermissionsCommand ?? (mEditPermissionsCommand = new DelegateCommand(ExecuteEditPermissions, CanExecuteEditPermissions)); }
    }

    bool CanExecuteEditPermissions()
    {
      return SelectedRoleViewModel != null;
    }

    void ExecuteEditPermissions()
    {
      if (SelectedRoleViewModel == null) return;
      EditPermissionsController c = Controller.CreateChildController<EditPermissionsController>();
      c.DataContext = new EditPermissionsContext(Model);
      c.SelectedRoleViewModel = SelectedRoleViewModel;
      c.Run();
    }

    #endregion
  }
}

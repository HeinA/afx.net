﻿using Afx.Business.Activities;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Development.Prism.Activities.ActivityManagement
{
  public class ActivityTabGroupNodeViewModel : MdiNavigationTreeNodeViewModel<ActivityGroup>
  {
    [InjectionConstructor]
    public ActivityTabGroupNodeViewModel(IController controller)
      : base(controller)
    {
    }

    public string GroupName
    {
      get
      {
        if (Model == null) return string.Empty;
        if (string.IsNullOrWhiteSpace(Model.GroupName)) return "*** Unnamed ***";
        return Model.GroupName;
      }
    }

    public IEnumerable<ActivityNodeViewModel> Activities
    {
      get { return this.ResolveViewModels<ActivityNodeViewModel, Activity>(Model.Activities); }
    }
  }
}

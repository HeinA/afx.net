﻿using Afx.Business.Activities;
using Afx.Business.Collections;
using Afx.Prism.RibbonMdi;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Development.Prism.Activities.ActivityManagement
{
  public class OperationFolder : NullContext
  {
    public OperationFolder(Guid identifier, Activity activity, bool isContext, IList operations)
      : base(identifier)
    {
      Activity = activity;
      Operations = operations;
      IsContext = isContext;
    }

    bool mIsContext;
    public bool IsContext
    {
      get { return mIsContext; }
      private set { mIsContext = value; }
    }

    Activity mActivity;
    public Activity Activity
    {
      get { return mActivity; }
      private set { mActivity = value; }
    }

    IList mOperations;
    public IList Operations
    {
      get { return mOperations; }
      private set { mOperations = value; }
    }
  }
}

﻿using Afx.Business;
using Afx.Business.Activities;
using Afx.Prism;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Development.Prism.Activities.ActivityManagement
{
  public class OperationFolderItemViewModel : SelectableViewModel<Operation>
  {
    [InjectionConstructor]
    public OperationFolderItemViewModel(IController controller)
      : base(controller)
    {
    }

    public Activity Activity
    {
      get { return Parent.Model.Activity; }
    }

    new OperationFolderDetailViewModel Parent
    {
      get { return (OperationFolderDetailViewModel)base.Parent; }
    }

    OperationFolder OperationFolder
    {
      get { return Parent.Model; }
    }

    public override bool IsSelected
    {
      get
      {
        if (base.IsSelected) Parent.SelectedOperationViewModel = this;
        return base.IsSelected;
      }
      set
      {
        base.IsSelected = value;
        if (value) Parent.SelectedOperationViewModel = this;
        else if (Parent.SelectedOperationViewModel == this) Parent.SelectedOperationViewModel = null;
      }
    }

    public void UpdateAllowedRoles()
    {
      OnPropertyChanged("AllowedRoles");
    }

    #region string Text

    public string Text
    {
      get
      {
        if (Model == null) return default(string);
        return Model.Text;
      }
      set { Model.Text = value; }
    }

    #endregion

    #region ImageSource ImageSource

    ImageSource mImageSource;
    public ImageSource ImageSource
    {
      get
      {
        if (Model == null) return default(ImageSource);
        if (string.IsNullOrWhiteSpace(Model.Base64Image)) return null;
        if (mImageSource == null)
        {
          byte[] imgData = Convert.FromBase64String(Model.Base64Image);
          BitmapImage img = new BitmapImage();
          using (MemoryStream ms = new MemoryStream(imgData))
          {
            img.BeginInit();
            img.CacheOption = BitmapCacheOption.OnLoad;
            img.StreamSource = ms;
            img.EndInit();
          }
          mImageSource = img as ImageSource;
        }
        return mImageSource;
      }
    }

    //public void SetImageFromFile(string filename)
    //{
    //  Model.SetImageFromFile(filename);
    //  mImageSource = null;
    //  OnPropertyChanged("ImageSource");
    //  OnPropertyChanged("ImageVisibility");
    //}

    #endregion

    #region Model Propertry string Description

    public string Description
    {
      get
      {
        if (Model == null) return default(string);
        return Model.Description;
      }
      set { Model.Description = value; }
    }

    #endregion

    public string AllowedRoles
    {
      get
      {
        Collection<string> roles = new Collection<string>();
        if (Model is ContextOperation)
        {
          foreach (var role in Activity.GetAssociativeObject<ContextualActivityContextOperation>(Model).Roles.OrderBy((r) => r.Name))
          {
            roles.Add(role.Name);
          }
        }
        else
        {
          foreach (var role in Activity.GetAssociativeObject<ActivityGlobalOperation>(Model).Roles.OrderBy((r) => r.Name))
          {
            roles.Add(role.Name);
          }
        }
        return string.Join(", ", roles);
      }
    }

  }
}

﻿using Afx.Business.Documents;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using Afx.Prism.RibbonMdi.Activities;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Development.Prism.Activities.DocumentManagement
{
  public class DocumentTypeStateDetailViewModel : ActivityDetailViewModel<DocumentTypeState>
  {
    [InjectionConstructor]
    public DocumentTypeStateDetailViewModel(IController controller)
      : base(controller)
    {
    }

    #region string Name

    public string Name
    {
      get
      {
        if (Model == null) return default(string);
        return Model.Name;
      }
      set { Model.Name = value; }
    }

    #endregion

    //#region bool IsDocumentReadOnly

    //public bool IsDocumentReadOnly
    //{
    //  get { return Model == null ? GetDefaultValue<bool>() : Model.IsReadOnly; }
    //  set { Model.IsReadOnly = value; }
    //}

    //#endregion

    #region bool IsInRecentList

    public bool IsInRecentList
    {
      get { return Model == null ? GetDefaultValue<bool>() : Model.IsInRecentList; }
      set { Model.IsInRecentList = value; }
    }

    #endregion

    #region string Identifier

    public string Identifier
    {
      get
      {
        if (Model == null) return default(string);
        return Model.Identifier;
      }
    }

    #endregion
  }
}

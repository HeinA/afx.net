﻿using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Documents;
using Afx.Business.Security;
using Afx.Prism;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Development.Prism.Activities.DocumentManagement
{
  public class DocumentTypeStateEditRoleViewModel : TabViewModel<DocumentTypeState>
  {
    #region Constructors

    [InjectionConstructor]
    public DocumentTypeStateEditRoleViewModel(IController controller)
      : base(controller)
    {
      Title = "_Edit Permissions";
    }

    #endregion

    #region DelegateCommand KeyDownCommand

    DelegateCommand<KeyEventArgs> mKeyDownCommand;
    public DelegateCommand<KeyEventArgs> KeyDownCommand
    {
      get { return mKeyDownCommand ?? (mKeyDownCommand = new DelegateCommand<KeyEventArgs>(ExecuteKeyDown)); }
    }

    void ExecuteKeyDown(KeyEventArgs e)
    {
      if (e.Key != Key.Space) return;
      if (SelectedRoleItemViewModel != null) SelectedRoleItemViewModel.HasRole = !SelectedRoleItemViewModel.HasRole;
      e.Handled = true;
    }

    #endregion

    #region ListView

    #region IEnumerable<Role> Roles

    public const string RolesProperty = "Roles";
    BasicCollection<Role> mRoles;
    public BasicCollection<Role> Roles
    {
      get { return mRoles ?? (mRoles = Cache.Instance.GetObjects<Role>(r => r.Name, true)); }
    }

    #endregion

    #region IEnumerable<RoleItemViewModel> RoleItemViewModels

    public IEnumerable<RoleItemViewModel> RoleItemViewModels
    {
      get
      {
        if (Model == null) return null;
        return this.ResolveViewModels<RoleItemViewModel, Role>(Roles);
      }
    }

    #endregion

    #region RoleItemViewModel SelectedRoleItemViewModel

    public const string SelectedRoleItemViewModelProperty = "SelectedRoleItemViewModel";
    RoleItemViewModel mSelectedRoleItemViewModel;
    public RoleItemViewModel SelectedRoleItemViewModel
    {
      get { return mSelectedRoleItemViewModel; }
      set { SetProperty<RoleItemViewModel>(ref mSelectedRoleItemViewModel, value); }
    }

    #endregion

    #endregion
  }
}

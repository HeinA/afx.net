﻿using Afx.Business.Activities;
using Afx.Business.Data;
using Afx.Prism.RibbonMdi.Ribbon;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Development.Prism;

namespace Development.Prism.Activities.DocumentManagement
{
  [Export(typeof(IRibbonItem))]
  public class DocumentManagementButton : RibbonButtonViewModel
  {
    public override string TabName
    {
      get { return DevelopmentTab.TabName; }
    }

    public override string GroupName
    {
      get { return SetupGroup.GroupName; }
    }

    public override string ItemIdentifier
    {
      get { return "DocumentManagement"; }
    }

    public override int Index
    {
      get { return 2; }
    }

    #region Activity Activity

    public Activity Activity
    {
      get { return Cache.Instance.GetObject<Activity>(DocumentManagement.Key); }
    }

    #endregion

    public bool IsEnabled
    {
      get
      {
        try
        {
          if (Activity == null) return false;
          return Activity.CanView;
        }
        catch
        {
          return false;
        }
      }
    }

    protected override void OnExecute()
    {
      MdiApplicationController.Current.ExecuteActivity(Activity);
    }
  }
}

﻿using Afx.Business.Documents;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Activities;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Development.Prism.Activities.DocumentManagement
{
  public class DocumentManagementViewModel : MdiContextualActivityViewModel<DocumentManagement>
  {
    [InjectionConstructor]
    public DocumentManagementViewModel(IController controller)
      : base(controller)
    {
    }

    public IEnumerable<DocumentTypeNodeViewModel> DocumentTypes
    {
      get
      {
        if (Model == null) return null;
        return this.ResolveViewModels<DocumentTypeNodeViewModel, DocumentType>(Model.Data);
      }
    }
  }
}

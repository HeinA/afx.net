﻿using Afx.Business.Documents;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Development.Prism.Activities.DocumentManagement
{
  public class DocumentTypeNumberDetailsViewModel : MdiDialogViewModel<DocumentTypeNumberContext>
  {
    [InjectionConstructor]
    public DocumentTypeNumberDetailsViewModel(IController controller)
      : base(controller)
    {
    }

    #region string Acronymn

    public string Acronymn
    {
      get
      {
        if (Model == null) return default(string);
        return Model.Acronymn;
      }
      set { Model.Acronymn = value; }
    }

    #endregion

    #region int NextId

    public int NextId
    {
      get
      {
        if (Model == null) return default(int);
        return Model.NextId;
      }
      set { Model.NextId = value; }
    }

    #endregion

  }
}

﻿using Afx.Business;
using Afx.Business.Documents;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using Afx.Prism.RibbonMdi.Activities;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Development.Prism.Activities.DocumentManagement
{
  public class GlobalOperationsDetailViewModel : ActivityTabDetailViewModel<DocumentType>, IOperationCollection
  {
    [InjectionConstructor]
    public GlobalOperationsDetailViewModel(IController controller)
      : base(controller)
    {
      Title = "Operations";
    }

    public IEnumerable<OperationItemViewModel> GlobalOperations
    {
      get { return ResolveViewModels<OperationItemViewModel, Operation>(Model.Operations); }
    }

    BusinessObject IOperationCollection.Model
    {
      get { return Model; }
    }

    OperationItemViewModel mSelectedOperationViewModel;
    public OperationItemViewModel SelectedOperationViewModel
    {
      get { return mSelectedOperationViewModel; }
      set
      {
        if (SetProperty<OperationItemViewModel>(ref mSelectedOperationViewModel, value))
        {
          MoveUpCommand.RaiseCanExecuteChanged();
          MoveDownCommand.RaiseCanExecuteChanged();
          PropertiesCommand.RaiseCanExecuteChanged();
          DeleteOperationCommand.RaiseCanExecuteChanged();
        }
      }
    }

    #region DelegateCommand MoveUpCommand

    DelegateCommand mMoveUpCommand;
    public DelegateCommand MoveUpCommand
    {
      get { return mMoveUpCommand ?? (mMoveUpCommand = new DelegateCommand(ExecuteMoveUp, CanExecuteMoveUp)); }
    }

    bool CanExecuteMoveUp()
    {
      if (SelectedOperationViewModel == null) return false;
      if (Model.Operations.IndexOf(SelectedOperationViewModel.Model) == 0) return false;
      return true;
    }

    void ExecuteMoveUp()
    {
      int i = Model.Operations.IndexOf(SelectedOperationViewModel.Model);
      Model.Operations.RemoveAt(i);
      Model.Operations.Insert(i - 1, SelectedOperationViewModel.Model);
      Model.GetAssociativeObject<DocumentTypeGlobalOperation>((Model.Operations[i])).SortOrder = i;
      Model.GetAssociativeObject<DocumentTypeGlobalOperation>((Model.Operations[i - 1])).SortOrder = i - 1;
      MoveUpCommand.RaiseCanExecuteChanged();
      MoveDownCommand.RaiseCanExecuteChanged();
      OnPropertyChanged("GlobalOperations");
      IsFocused = true;
      SelectedOperationViewModel.IsFocused = true;
    }

    #endregion

    #region DelegateCommand MoveDownCommand

    DelegateCommand mMoveDownCommand;
    public DelegateCommand MoveDownCommand
    {
      get { return mMoveDownCommand ?? (mMoveDownCommand = new DelegateCommand(ExecuteMoveDown, CanExecuteMoveDown)); }
    }

    bool CanExecuteMoveDown()
    {
      if (SelectedOperationViewModel == null) return false;
      if (Model.Operations.IndexOf(SelectedOperationViewModel.Model) == (Model.Operations.Count - 1)) return false;
      return true;
    }

    void ExecuteMoveDown()
    {
      int i = Model.Operations.IndexOf(SelectedOperationViewModel.Model);
      Model.Operations.RemoveAt(i);
      Model.Operations.Insert(i + 1, SelectedOperationViewModel.Model);
      Model.GetAssociativeObject<DocumentTypeGlobalOperation>((Model.Operations[i])).SortOrder = i;
      Model.GetAssociativeObject<DocumentTypeGlobalOperation>((Model.Operations[i + 1])).SortOrder = i + 1;
      MoveUpCommand.RaiseCanExecuteChanged();
      MoveDownCommand.RaiseCanExecuteChanged();
      OnPropertyChanged("GlobalOperations");
      IsFocused = true;
      SelectedOperationViewModel.IsFocused = true;
    }

    #endregion

    #region DelegateCommand PropertiesCommand

    DelegateCommand mPropertiesCommand;
    public DelegateCommand PropertiesCommand
    {
      get { return mPropertiesCommand ?? (mPropertiesCommand = new DelegateCommand(ExecuteProperties, CanExecuteProperties)); }
    }

    bool CanExecuteProperties()
    {
      return SelectedOperationViewModel != null;
    }

    void ExecuteProperties()
    {
      try
      {
        DocumentTypeGlobalOperation dtop = Model.GetAssociativeObject<DocumentTypeGlobalOperation>(SelectedOperationViewModel.Model);
        OperationPropertiesContext rsc = new OperationPropertiesContext(dtop.Roles, dtop);
        OperationPropertiesController c = Controller.CreateChildController<OperationPropertiesController>();
        c.DataContext = rsc;
        c.SelectedOperationViewModel = SelectedOperationViewModel;
        c.Run();
      }
      catch (Exception ex)
      {
        if (ExceptionHelper.HandleException(ex)) throw;
      }
    }

    #endregion

    #region DelegateCommand DeleteOperationCommand

    DelegateCommand mDeleteOperationCommand;
    public DelegateCommand DeleteOperationCommand
    {
      get { return mDeleteOperationCommand ?? (mDeleteOperationCommand = new DelegateCommand(ExecuteDeleteOperation, CanExecuteDeleteOperation)); }
    }

    bool CanExecuteDeleteOperation()
    {
      return SelectedOperationViewModel != null;
    }

    void ExecuteDeleteOperation()
    {
      Model.Operations.Remove(SelectedOperationViewModel.Model);
      OnPropertyChanged("GlobalOperations");
    }

    #endregion
  }
}

﻿using Afx.Business;
using Afx.Business.Documents;
using Afx.Business.Security;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Development.Prism.Activities.DocumentManagement
{
  public class OrganizationalUnitItemViewModel : SelectableViewModel<OrganizationalUnit>
  {
    #region Constructors

    [InjectionConstructor]
    public OrganizationalUnitItemViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    #region OrganizationalUnitsViewModel Parent

    public new OrganizationalUnitsViewModel Parent
    {
      get { return (OrganizationalUnitsViewModel)base.Parent; }
    }

    #endregion

    #region string Name

    public string Name
    {
      get
      {
        if (Model == null) return default(string);
        return Model.Name;
      }
      set { Model.Name = value; }
    }

    #endregion

    #region bool IsSelected

    public override bool IsSelected
    {
      get
      {
        if (base.IsSelected)
        {
          Parent.SelectedOrganizationalUnitItemViewModel = this;
        }
        return base.IsSelected;
      }
      set
      {
        base.IsSelected = value;
        if (base.IsSelected)
        {
          Parent.SelectedOrganizationalUnitItemViewModel = this;
        }
      }
    }

    #endregion 

    #region bool HasOrganizationalUnit

    public bool HasOrganizationalUnit
    {
      get { return Parent.Model.ApplicableOrganizationalUnits.Contains(Model); }
      set
      {
        Parent.SelectedOrganizationalUnitItemViewModel = this;
        if (value)
        {
          Parent.Model.ApplicableOrganizationalUnits.Add(Model);
          if (Parent.Model.GetAssociativeObject<DocumentTypeNumber>(Model).NextId == 0) 
            Parent.ExecuteEditDocumentNumberDetails();
        }
        else Parent.Model.ApplicableOrganizationalUnits.Remove(Model);
        OnPropertyChanged("HasOrganizationalUnit");
        OnPropertyChanged("Acronymn");
        OnPropertyChanged("NextId");
      }
    }

    #endregion

    #region string Acronymn

    public string Acronymn
    {
      get
      {
        if (Model == null) return default(string);
        if (!HasOrganizationalUnit) return default(string);
        return Parent.Model.GetAssociativeObject<DocumentTypeNumber>(Model).Acronymn;
      }
    }

    #endregion

    #region string NextId

    public string NextId
    {
      get
      {
        if (Model == null) return default(string);
        if (!HasOrganizationalUnit) return default(string);
        return Parent.Model.GetAssociativeObject<DocumentTypeNumber>(Model).NextId.ToString();
      }
    }

    #endregion

    public void RefreshDetails()
    {
      OnPropertyChanged(DocumentTypeNumber.AcronymnProperty);
      OnPropertyChanged(DocumentTypeNumber.NextIdProperty);
    }
  }
}

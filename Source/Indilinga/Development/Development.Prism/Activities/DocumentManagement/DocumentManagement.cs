﻿using Afx.Business.Activities;
using Afx.Business.Data;
using Afx.Business.Documents;
using Afx.Business.Security;
using Afx.Business.Service;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using Afx.Prism.RibbonMdi.Activities;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Development.Prism.Activities.DocumentManagement
{
  [Export("Context:" + DocumentManagement.Key)]
  public class DocumentManagement : ContextualActivityContext<DocumentType>
  {
    public const string Key = "{6811edb6-8db9-4b06-b72b-1eef5c6c5bdf}";
    public const string TabRegion = "TabRegion";
    public const string DetailsRegion = "DetailsRegion";

    public DocumentManagement()
    {
    }

    public DocumentManagement(ContextualActivity activity)
      : base(activity)
    {
    }

    public override void LoadData()
    {
      using (var svc = ProxyFactory.GetService<IAfxService>(SecurityContext.MasterServer.Name))
      {
        Data = svc.LoadDocumentTypes();
      }
      base.LoadData();
    }

    public override void SaveData()
    {
      using (var svc = ProxyFactory.GetService<IAfxService>(SecurityContext.MasterServer.Name))
      {
        Data = svc.SaveDocumentTypes(Data);
      }
      base.SaveData();
    }
  }
}

﻿using Afx.Prism;
using Afx.Prism.RibbonMdi;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Development.Prism.Activities.DocumentManagement
{
  public class DocumentTypeNumberDetailsController : MdiDialogController<DocumentTypeNumberContext, DocumentTypeNumberDetailsViewModel>
  {
    public const string DocumentTypeNumberDetailsControllerKey = "Document Type Number Details Controller";

    [InjectionConstructor]
    public DocumentTypeNumberDetailsController(IController controller)
      : base(DocumentTypeNumberDetailsControllerKey, controller)
    {
    }

    OrganizationalUnitItemViewModel mSelectedItemViewModel;
    public OrganizationalUnitItemViewModel SelectedItemViewModel
    {
      get { return mSelectedItemViewModel; }
      set { mSelectedItemViewModel = value; }
    }

    protected override void OnTerminated()
    {
      SelectedItemViewModel.IsFocused = true;
      if (DataContext.DocumentTypeNumber.NextId == 0)
      {
        SelectedItemViewModel.HasOrganizationalUnit = false;
        //DataContext.DocumentTypeNumber.Owner.ApplicableOrganizationalUnits.Remove(DataContext.DocumentTypeNumber.Reference);
      }
      base.OnTerminated();
    }

    protected override void ApplyChanges()
    {
      DataContext.DocumentTypeNumber.Acronymn = DataContext.Acronymn;
      DataContext.DocumentTypeNumber.NextId = DataContext.NextId;
      SelectedItemViewModel.RefreshDetails();
      base.ApplyChanges();
    }
  }
}

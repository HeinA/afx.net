﻿using Afx.Business;
using Afx.Business.Activities;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Documents;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Development.Prism.Activities.DocumentManagement
{
  public class AddOperationsController : MdiDialogController<IGlobalOperationsOwner, AddOperationsViewModel>
  {
    public const string AddOperationsControllerKey = "Add Operations Controller";

    [InjectionConstructor]
    public AddOperationsController(IController controller)
      : base(AddOperationsControllerKey, controller)
    {
    }

    BasicCollection<GlobalOperation> mAvailableOperations;
    public BasicCollection<GlobalOperation> AvailableOperations
    {
      get
      {
        if (mAvailableOperations == null)
        {
          mAvailableOperations = Cache.Instance.GetObjects<GlobalOperation>((o) => o.Text, true);
          foreach (var o in mAvailableOperations.ToList())
          {
            if (DataContext.Operations.Contains(o)) mAvailableOperations.Remove(o);
          }
        }
        return mAvailableOperations;
      }
    }

    BasicCollection<GlobalOperation> mSelectedOperations = new BasicCollection<GlobalOperation>();
    public BasicCollection<GlobalOperation> SelectedOperations
    {
      get { return mSelectedOperations; }
    }

    public ISelectableViewModel SelectedNode { get; set; }

    protected override void ApplyChanges()
    {
      foreach (var o in SelectedOperations)
      {
        DataContext.AddOperation(o);        
      }

      base.ApplyChanges();
    }

    protected override void OnTerminated()
    {
      if (SelectedNode != null) SelectedNode.IsFocused = true;
      base.OnTerminated();
    }
  }
}

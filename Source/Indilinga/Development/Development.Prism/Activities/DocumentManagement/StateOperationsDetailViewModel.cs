﻿using Afx.Business;
using Afx.Business.Documents;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using Afx.Prism.RibbonMdi.Activities;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Development.Prism.Activities.DocumentManagement
{
  public class StateOperationsDetailViewModel : ActivityTabDetailViewModel<DocumentTypeState>, IOperationCollection
  {
    [InjectionConstructor]
    public StateOperationsDetailViewModel(IController controller)
      : base(controller)
    {
      Title = "Operations";
    }

    public IEnumerable<OperationItemViewModel> Operations
    {
      get { return ResolveViewModels<OperationItemViewModel, Operation>(Model.Operations); }
    }

    BusinessObject IOperationCollection.Model
    {
      get { return Model; }
    }

    OperationItemViewModel mSelectedOperationViewModel;
    public OperationItemViewModel SelectedOperationViewModel
    {
      get { return mSelectedOperationViewModel; }
      set
      {
        if (SetProperty<OperationItemViewModel>(ref mSelectedOperationViewModel, value))
        {
          MoveUpCommand.RaiseCanExecuteChanged();
          MoveDownCommand.RaiseCanExecuteChanged();
          PropertiesCommand.RaiseCanExecuteChanged();
          DeleteOperationCommand.RaiseCanExecuteChanged();
        }
      }
    }

    #region DelegateCommand MoveUpCommand

    DelegateCommand mMoveUpCommand;
    public DelegateCommand MoveUpCommand
    {
      get { return mMoveUpCommand ?? (mMoveUpCommand = new DelegateCommand(ExecuteMoveUp, CanExecuteMoveUp)); }
    }

    bool CanExecuteMoveUp()
    {
      if (SelectedOperationViewModel == null) return false;
      if (Model.Operations.IndexOf(SelectedOperationViewModel.Model) == 0) return false;
      return true;
    }

    void ExecuteMoveUp()
    {
      try
      {
        int i = Model.Operations.IndexOf(SelectedOperationViewModel.Model);
        Model.Operations.RemoveAt(i);
        Model.Operations.Insert(i - 1, SelectedOperationViewModel.Model);
        Model.GetAssociativeObject<DocumentTypeStateOperation>((Model.Operations[i])).SortOrder = i;
        Model.GetAssociativeObject<DocumentTypeStateOperation>((Model.Operations[i - 1])).SortOrder = i - 1;
        MoveUpCommand.RaiseCanExecuteChanged();
        MoveDownCommand.RaiseCanExecuteChanged();
        OnPropertyChanged("Operations");
        IsFocused = true;
        SelectedOperationViewModel.IsFocused = true;
      }
      catch (Exception ex)
      {
        if (ExceptionHelper.HandleException(ex)) throw;
      }
    }

    #endregion

    #region DelegateCommand MoveDownCommand

    DelegateCommand mMoveDownCommand;
    public DelegateCommand MoveDownCommand
    {
      get { return mMoveDownCommand ?? (mMoveDownCommand = new DelegateCommand(ExecuteMoveDown, CanExecuteMoveDown)); }
    }

    bool CanExecuteMoveDown()
    {
      if (SelectedOperationViewModel == null) return false;
      if (Model.Operations.IndexOf(SelectedOperationViewModel.Model) == (Model.Operations.Count - 1)) return false;
      return true;
    }

    void ExecuteMoveDown()
    {
      try
      {
        int i = Model.Operations.IndexOf(SelectedOperationViewModel.Model);
        Model.Operations.RemoveAt(i);
        Model.Operations.Insert(i + 1, SelectedOperationViewModel.Model);
        Model.GetAssociativeObject<DocumentTypeStateOperation>((Model.Operations[i])).SortOrder = i;
        Model.GetAssociativeObject<DocumentTypeStateOperation>((Model.Operations[i + 1])).SortOrder = i + 1;
        MoveUpCommand.RaiseCanExecuteChanged();
        MoveDownCommand.RaiseCanExecuteChanged();
        OnPropertyChanged("Operations");
        IsFocused = true;
        SelectedOperationViewModel.IsFocused = true;
      }
      catch (Exception ex)
      {
        if (ExceptionHelper.HandleException(ex)) throw;
      }
    }

    #endregion

    #region DelegateCommand PropertiesCommand

    DelegateCommand mPropertiesCommand;
    public DelegateCommand PropertiesCommand
    {
      get { return mPropertiesCommand ?? (mPropertiesCommand = new DelegateCommand(ExecuteProperties, CanExecuteProperties)); }
    }

    bool CanExecuteProperties()
    {
      return SelectedOperationViewModel != null;
    }

    void ExecuteProperties()
    {
      try
      {
        OperationPropertiesContext rsc = new OperationPropertiesContext(Model.GetAssociativeObject<DocumentTypeStateOperation>(SelectedOperationViewModel.Model).Roles);
        OperationPropertiesController c = Controller.CreateChildController<OperationPropertiesController>();
        c.DataContext = rsc;
        c.SelectedOperationViewModel = SelectedOperationViewModel;
        c.Run();
      }
      catch (Exception ex)
      {
        if (ExceptionHelper.HandleException(ex)) throw;
      }
    }

    #endregion

    #region DelegateCommand DeleteOperationCommand

    DelegateCommand mDeleteOperationCommand;
    public DelegateCommand DeleteOperationCommand
    {
      get { return mDeleteOperationCommand ?? (mDeleteOperationCommand = new DelegateCommand(ExecuteDeleteOperation, CanExecuteDeleteOperation)); }
    }

    bool CanExecuteDeleteOperation()
    {
      return SelectedOperationViewModel != null;
    }

    void ExecuteDeleteOperation()
    {
      Model.Operations.Remove(SelectedOperationViewModel.Model);
      OnPropertyChanged("Operations");
    }

    #endregion
  }
}

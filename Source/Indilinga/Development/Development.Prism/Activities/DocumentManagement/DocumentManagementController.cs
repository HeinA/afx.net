﻿using Afx.Business;
using Afx.Business.Activities;
using Afx.Business.Documents;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using Afx.Prism.RibbonMdi.Activities;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Development.Prism.Activities.DocumentManagement
{
  [Export("Controller:" + Development.Prism.Activities.DocumentManagement.DocumentManagement.Key)]
  public class DocumentManagementController : MdiContextualActivityController<DocumentManagement, DocumentManagementViewModel>
  {
    [InjectionConstructor]
    public DocumentManagementController(IController controller)
      : base(controller)
    {
    }

    protected override void OnLoaded()
    {
      if (DataContext.Data.Count > 0)
      {
        //Focus the first element 
        DocumentTypeNodeViewModel nvm = GetCreateViewModel<DocumentTypeNodeViewModel>(DataContext.Data[0], ViewModel);
        nvm.IsFocused = true;
      }

      RegisterContextRegion(RegionManager.Regions[DocumentManagement.DetailsRegion]);
      RegisterContextRegion(RegionManager.Regions[DocumentManagement.TabRegion]);

      base.OnLoaded();
    }

    protected override void OnContextViewModelChanged(ISelectableViewModel context)
    {
      if (context.Model is DocumentType)
      {
        DocumentTypeDetailViewModel vm = GetCreateViewModel<DocumentTypeDetailViewModel>(context.Model, ViewModel);
        RegionManager.Regions[DocumentManagement.DetailsRegion].Add(vm);

        IRegion tabRegion = RegionManager.Regions[DocumentManagement.TabRegion];

        GlobalOperationsDetailViewModel godvm = GetCreateViewModel<GlobalOperationsDetailViewModel>(context.Model, ViewModel);
        tabRegion.Add(godvm);
        if (godvm.IsActive) tabRegion.Activate(godvm);

        OrganizationalUnitsViewModel ouvm = GetCreateViewModel<OrganizationalUnitsViewModel>(context.Model, ViewModel);
        tabRegion.Add(ouvm);
        if (ouvm.IsActive) tabRegion.Activate(ouvm);
      }

      if (context.Model is DocumentTypeState)
      {
        DocumentTypeStateDetailViewModel vm = GetCreateViewModel<DocumentTypeStateDetailViewModel>(context.Model, ViewModel);
        RegionManager.Regions[DocumentManagement.DetailsRegion].Add(vm);

        IRegion tabRegion = RegionManager.Regions[DocumentManagement.TabRegion];

        StateOperationsDetailViewModel sodvm = GetCreateViewModel<StateOperationsDetailViewModel>(context.Model, ViewModel);
        tabRegion.Add(sodvm);
        if (sodvm.IsActive) tabRegion.Activate(sodvm);

        DocumentTypeStateEditRoleViewModel ervm = GetCreateViewModel<DocumentTypeStateEditRoleViewModel>(context.Model, ViewModel);
        tabRegion.Add(ervm);
        if (ervm.IsActive) tabRegion.Activate(ervm);
      }

      base.OnContextViewModelChanged(context);
    }

    protected override void ExecuteOperation(Operation op, BusinessObject argument)
    {
      switch (op.Identifier)
      {
        case Operations.AddDocumentType:
          {
            DocumentType dt = new DocumentType();
            DataContext.Data.Add(dt);
            SelectContextViewModel(dt);
            FocusViewModel<DocumentTypeDetailViewModel>(dt);
          }
          break;

        case Operations.AddState:
          {
            DocumentType dt = (DocumentType)argument;
            DocumentTypeState newDts = new DocumentTypeState();
            dt.States.Add(newDts);
            SelectContextViewModel(newDts);
            FocusViewModel<DocumentTypeStateDetailViewModel>(newDts);
          }
          break;

        case Operations.AddGlobalOperation:
          {
            AddOperationsController aoc = CreateChildController<AddOperationsController>();
            aoc.DataContext = (IGlobalOperationsOwner)argument;
            aoc.SelectedNode = ContextViewModel;
            aoc.Run();
          }
          break;

        case Operations.AddStateOperation:
          {
            AddOperationsController aoc = CreateChildController<AddOperationsController>();
            aoc.DataContext = (IGlobalOperationsOwner)argument;
            aoc.SelectedNode = ContextViewModel;
            aoc.Run();
          }
          break;
      }
      base.ExecuteOperation(op, argument);
    }
  }
}

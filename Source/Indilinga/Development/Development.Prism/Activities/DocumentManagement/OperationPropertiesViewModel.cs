﻿using Afx.Business.Activities;
using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace Development.Prism.Activities.DocumentManagement
{
  public class OperationPropertiesViewModel : MdiDialogViewModel<OperationPropertiesContext>
  {
    [InjectionConstructor]
    public OperationPropertiesViewModel(IController controller)
      : base(controller)
    {
    }

    public IEnumerable<OperationPropertiesRoleViewModel> Roles
    {
      get { return ResolveViewModels<OperationPropertiesRoleViewModel, Role>(Cache.Instance.GetObjects<Role>((r) => r.Name, true)); }
    }

    OperationPropertiesRoleViewModel mSelectedItemViewModel;
    public OperationPropertiesRoleViewModel SelectedItemViewModel
    {
      get { return mSelectedItemViewModel; }
      set { mSelectedItemViewModel = value; }
    }

    #region bool ReadOnlyAvailability

    public bool ReadOnlyAvailability
    {
      get { return Model == null ? GetDefaultValue<bool>() : Model.ReadOnlyAvailability; }
      set { Model.ReadOnlyAvailability = value; }
    }

    #endregion

    #region Visibility ReadOnlyAvailabilityVisibility

    public Visibility ReadOnlyAvailabilityVisibility
    {
      get { return Model.DocumentTypeGlobalOperation == null ? Visibility.Collapsed : Visibility.Visible; }
    }

    #endregion

    #region DelegateCommand KeyDownCommand

    DelegateCommand<KeyEventArgs> mKeyDownCommand;
    public DelegateCommand<KeyEventArgs> KeyDownCommand
    {
      get { return mKeyDownCommand ?? (mKeyDownCommand = new DelegateCommand<KeyEventArgs>(ExecuteKeyDown)); }
    }

    void ExecuteKeyDown(KeyEventArgs e)
    {
      if (e.Key != Key.Space) return;
      if (SelectedItemViewModel != null) SelectedItemViewModel.HasRole = !SelectedItemViewModel.HasRole;
      e.Handled = true;
    }

    #endregion
  }
}

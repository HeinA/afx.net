﻿using Afx.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Development.Prism.Activities.DocumentManagement
{
  public interface IOperationCollection
  {
    OperationItemViewModel SelectedOperationViewModel { get; set; }
    BusinessObject Model { get; }
  }
}

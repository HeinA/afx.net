﻿using Afx.Business.Documents;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Development.Prism.Activities.DocumentManagement
{
  public class DocumentTypeStateNodeViewModel : MdiNavigationTreeNodeViewModel<DocumentTypeState>
  {
    [InjectionConstructor]
    public DocumentTypeStateNodeViewModel(IController controller)
      : base(controller)
    {
    }

    public string Name
    {
      get
      {
        if (Model == null) return string.Empty;
        if (string.IsNullOrWhiteSpace(Model.Name)) return "*** Unnamed ***";
        return Model.Name;
      }
    }
  }
}

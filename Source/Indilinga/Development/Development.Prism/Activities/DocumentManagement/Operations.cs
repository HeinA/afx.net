﻿using Afx.Prism.RibbonMdi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Development.Prism.Activities.DocumentManagement
{
  public class Operations : StandardOperations
  {
    public const string AddDocumentType = "{f40d1d79-6089-4b53-9dbc-cab48f743bc7}";
    public const string AddState = "{ae25e85f-6f85-4bab-a72d-6463a2181790}";
    public const string AddGlobalOperation = "{7b151e03-8eee-4e36-af36-e98bf5a2d8c7}";
    public const string AddStateOperation = "{1b1e0310-9a3d-4e91-bc03-52d9ab265c54}";
  }
}

﻿using Afx.Business.Security;
using Afx.Prism;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Development.Prism.Activities.DocumentManagement
{
  public class RoleItemViewModel : SelectableViewModel<Role>
  {
    #region Constructors

    [InjectionConstructor]
    public RoleItemViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    #region DocumentTypeStateEditRoleViewModel Parent

    public new DocumentTypeStateEditRoleViewModel Parent
    {
      get { return (DocumentTypeStateEditRoleViewModel)base.Parent; }
    }

    #endregion

    #region bool IsSelected

    public override bool IsSelected
    {
      get
      {
        if (base.IsSelected)
        {
          Parent.SelectedRoleItemViewModel = this;
        }
        return base.IsSelected;
      }
      set
      {
        base.IsSelected = value;
        if (base.IsSelected)
        {
          Parent.SelectedRoleItemViewModel = this;
        }
      }
    }

    #endregion

    #region bool HasRole

    public bool HasRole
    {
      get { return Parent.Model.EditPermissions.Contains(Model); }
      set
      {
        Parent.SelectedRoleItemViewModel = this;
        if (value) Parent.Model.EditPermissions.Add(Model);
        else Parent.Model.EditPermissions.Remove(Model);
        OnPropertyChanged("HasRole");
      }
    }

    #endregion
  }
}

﻿using Afx.Business;
using Afx.Business.Activities;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Development.Prism.Activities.OperationManagement
{
  public class OperationGroupNodeViewModel : MdiNavigationTreeNodeViewModel<OperationGroup>
  {
    [InjectionConstructor]
    public OperationGroupNodeViewModel(IController controller)
      : base(controller)
    {
    }

    public string GroupName
    {
      get
      {
        if (Model == null) return string.Empty;
        if (string.IsNullOrWhiteSpace(Model.GroupName)) return "*** Unnamed ***";
        return Model.GroupName;
      }
    }

    public IEnumerable<OperationNodeViewModel> Operations
    {
      get { return this.ResolveViewModels<OperationNodeViewModel, Operation>(Model.Operations); }
    }
  }
}

﻿using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Activities.OperationManagement
{
  public class ContextTypeViewModel : ViewModel<TypeWrapper>
  {
    [InjectionConstructor]
    public ContextTypeViewModel(IController controller)
      : base(controller)
    {
    }

    public string ContextTypeName
    {
      get { return Model.ContextTypeName; }
    }
  }
}

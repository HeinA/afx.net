﻿using Afx.Business;
using Afx.Business.Activities;
using Afx.Business.Collections;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Development.Prism.Activities.OperationManagement
{
  public class OperationDetailViewModel : ViewModel<Operation>
  {
    [InjectionConstructor]
    public OperationDetailViewModel(IController controller)
      : base(controller)
    {
    }

    #region string Identifier

    public string Identifier
    {
      get
      {
        if (Model == null) return default(string);
        return Model.Identifier;
      }
    }

    #endregion

    #region string Text

    public string Text
    {
      get
      {
        if (Model == null) return default(string);
        return Model.Text;
      }
      set { Model.Text = value; }
    }

    #endregion

    #region string Description

    public string Description
    {
      get
      {
        if (Model == null) return default(string);
        return Model.Description;
      }
      set { Model.Description = value; }
    }

    #endregion

    #region string Namespace

    public IEnumerable<string> Namespaces
    {
      get { return ExtensibilityManager.Namespaces; }
    }

    public string Namespace
    {
      get
      {
        if (Model == null) return string.Empty;
        return Model.Namespace ?? string.Empty;
      }
      set { Model.Namespace = string.IsNullOrWhiteSpace(value) ? null : value; }
    }

    #endregion
  }
}

﻿using Afx.Business;
using Afx.Business.Activities;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using Afx.Prism.RibbonMdi.Activities;
using Microsoft.Practices.Unity;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Development.Prism.Activities.OperationManagement
{
  [Export("Controller:" + Development.Prism.Activities.OperationManagement.OperationManagement.Key)]
  public class OperationManagementController : MdiContextualActivityController<OperationManagement, OperationManagementViewModel>
  {
    [InjectionConstructor]
    public OperationManagementController(IController controller)
      : base(controller)
    {
    }

    protected override void OnLoaded()
    {
      if (DataContext.Data.Count > 0)
      {
        //Focus the first element 
        OperationGroupNodeViewModel nvm = GetCreateViewModel<OperationGroupNodeViewModel>(DataContext.Data[0], ViewModel);
        nvm.IsFocused = true;
      }

      RegisterContextRegion(RegionManager.Regions[OperationManagement.ContextRegion]);

      base.OnLoaded();
    }

    protected override void OnContextViewModelChanged(ISelectableViewModel context)
    {
      if (context.Model is OperationGroup)
      {
        OperationGroupDetailViewModel vm = GetCreateViewModel<OperationGroupDetailViewModel>(context.Model, ViewModel);
        RegionManager.Regions[OperationManagement.ContextRegion].Add(vm);
      }

      if (context.Model is Operation)
      {
        OperationDetailViewModel vm = GetCreateViewModel<OperationDetailViewModel>(context.Model, ViewModel);
        RegionManager.Regions[OperationManagement.ContextRegion].Add(vm);

        if (context.Model is ContextOperation)
        {
          ContextOperationDetailViewModel vm1 = GetCreateViewModel<ContextOperationDetailViewModel>(context.Model, ViewModel);
          RegionManager.Regions[OperationManagement.ContextRegion].Add(vm1);
        }
      }

      base.OnContextViewModelChanged(context);
    }

    protected override void ExecuteOperation(Operation op, BusinessObject argument)
    {
      switch (op.Identifier)
      {
        case Operations.AddGroup:
          {
            OperationGroup og = new OperationGroup();
            DataContext.Data.Add(og);
            SelectContextViewModel(og);
            FocusViewModel<OperationGroupDetailViewModel>(og);
          }
          break;

        case Operations.AddOperation:
          {
            OperationGroup og = (OperationGroup)argument;
            GlobalOperation newOp = new GlobalOperation();
            og.Operations.Add(newOp);
            SelectContextViewModel(newOp);
            FocusViewModel<OperationDetailViewModel>(newOp);
          }
          break;

        case Operations.AddContextOperation:
          {
            OperationGroup og = (OperationGroup)argument;
            ContextOperation newOp = new ContextOperation();
            og.Operations.Add(newOp);
            SelectContextViewModel(newOp);
            FocusViewModel<OperationDetailViewModel>(newOp);
          }
          break;

        case Operations.SetIcon:
          {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Multiselect = false;
            ofd.Filter = "Icon Files|*.ico;*.png;*.jpg|All Files|*.*";
            if (ofd.ShowDialog().Value)
            {
              Operation opvm = (Operation)argument;
              opvm.SetImageFromFile(ofd.FileName);
            }
          }
          break;
      }
      base.ExecuteOperation(op, argument);
    }
  }
}

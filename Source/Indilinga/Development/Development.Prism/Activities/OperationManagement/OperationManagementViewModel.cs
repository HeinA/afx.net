﻿using Afx.Business;
using Afx.Business.Activities;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using Afx.Prism.RibbonMdi.Activities;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Development.Prism.Activities.OperationManagement
{
  public class OperationManagementViewModel : MdiContextualActivityViewModel<OperationManagement>
  {
    [InjectionConstructor]
    public OperationManagementViewModel(IController controller)
      : base(controller)
    {
    }

    public IEnumerable<OperationGroupNodeViewModel> Groups
    {
      get
      {
        if (Model == null) return null;
        return this.ResolveViewModels<OperationGroupNodeViewModel, OperationGroup>(Model.Data);
      }
    }
  }
}

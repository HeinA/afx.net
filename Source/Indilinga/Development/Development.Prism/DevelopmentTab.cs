﻿using Afx.Prism.RibbonMdi.Ribbon;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Development.Prism
{
  [Export(typeof(IRibbonTab))]
  public class DevelopmentTab : RibbonTab
  {
    public const string TabName = "Development";

    public override string Name
    {
      get { return TabName; }
    }
  }
}

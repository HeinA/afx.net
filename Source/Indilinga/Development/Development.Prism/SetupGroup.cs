﻿using Afx.Prism.RibbonMdi.Ribbon;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Development.Prism
{
  [Export(typeof(IRibbonGroup))]
  public class SetupGroup : RibbonGroup
  {
    public const string GroupName = "Setup";

    public override string TabName
    {
      get { return DevelopmentTab.TabName; }
    }

    public override string Name
    {
      get { return GroupName; }
    }
  }
}

﻿using Afx.Prism;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Development.Prism.Dialogs.SelectExportTypes
{
  public class TypeWrapperViewModel : SelectableViewModel<TypeWrapper>
  {
    [InjectionConstructor]
    public TypeWrapperViewModel(IController controller)
      : base(controller)
    {
    }

    [Dependency]
    public SelectExportTypesController SelectExportTypesController { get; set; }

    #region bool IsChecked

    public const string IsCheckedProperty = "IsChecked";
    public bool IsChecked
    {
      get { return SelectExportTypesController.SelectedTypeWrappers.Contains(this.Model); }
      set
      {
        if (value)
        {
          if (!SelectExportTypesController.SelectedTypeWrappers.Contains(this.Model)) SelectExportTypesController.SelectedTypeWrappers.Add(this.Model);
        }
        else
        {
          if (SelectExportTypesController.SelectedTypeWrappers.Contains(this.Model)) SelectExportTypesController.SelectedTypeWrappers.Remove(this.Model);
        }
      }
    }

    #endregion
  }
}

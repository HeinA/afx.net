﻿using Afx.Business.Data;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;
using System.Reflection;
using Afx.Business;
using Microsoft.Win32;
using Development.Business.Service;
using Afx.Business.Security;
using Afx.Business.Service;
using Afx.Business.Collections;
using System.IO;
using System.Runtime.Serialization;

namespace Development.Prism.Dialogs.SelectExportTypes
{
  public class SelectExportTypesController : MdiDialogController<SelectExportTypesContext, SelectExportTypesViewModel>
  {
    public const string SelectExportTypesControllerKey = "Development.Prism.Dialogs.SelectExportTypes.SelectExportTypesController";

    public SelectExportTypesController(IController controller)
      : base(SelectExportTypesControllerKey, controller)
    {
    }

    #region Collection<TypeWrapper> SelectedTypeWrappers

    Collection<TypeWrapper> mSelectedTypeWrappers = new Collection<TypeWrapper>();
    public Collection<TypeWrapper> SelectedTypeWrappers
    {
      get { return mSelectedTypeWrappers; }
    }

    #endregion

    #region BasicCollection<string> SelectedTypeStrings

    BasicCollection<string> mSelectedTypeStrings = new BasicCollection<string>();
    BasicCollection<string> SelectedTypeStrings
    {
      get { return mSelectedTypeStrings; }
    }

    #endregion

    protected override bool OnRun()
    {
      if (DataContext == null) DataContext = new SelectExportTypesContext();
      ViewModel.Caption = "Select Types to Export";
      return base.OnRun();
    }

    protected override void ApplyChanges()
    {
      Collection<Tuple<int, Type>> unsorted = new Collection<Tuple<int, Type>>();

      foreach (var tw in SelectedTypeWrappers)
      {
        CacheAttribute ca = tw.Type.GetCustomAttribute<CacheAttribute>();
        unsorted.Add(new Tuple<int, Type>(ca.Priority, tw.Type));
      }

      foreach (var t in unsorted.Where(t1 => t1.Item1 >= 0).OrderBy(t1 => t1.Item1))
      {
        SelectedTypeStrings.Add(TypeHelper.GetTypeName(t.Item2));
      }
      foreach (var t in unsorted.Where(t1 => t1.Item1 < 0))
      {
        SelectedTypeStrings.Add(TypeHelper.GetTypeName(t.Item2));
      }

      base.ApplyChanges();
    }

    public override void Terminate()
    {
      base.Terminate();

      if (SelectedTypeStrings.Count > 0)
      {
        SaveFileDialog sfd = new SaveFileDialog();
        sfd.Filter = "Xml File (.xml)|*.xml";
        sfd.DefaultExt = "xml";

        if (sfd.ShowDialog() == false) return;

        using (var svc = ProxyFactory.GetService<IDevelopmentService>(SecurityContext.ServerName))
        {
          BasicCollection<BusinessObject> col = svc.GetExports(SelectedTypeStrings);

          using (FileStream sw = new FileStream(sfd.FileName, FileMode.Create))
          {
            NetDataContractSerializer dcs = new NetDataContractSerializer();
            dcs.WriteObject(sw, col);
          }
        }
      }
    }
  }
}

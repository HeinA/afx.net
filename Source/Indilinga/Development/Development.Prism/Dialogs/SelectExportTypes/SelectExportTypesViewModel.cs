﻿using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Development.Prism.Dialogs.SelectExportTypes
{
  public class SelectExportTypesViewModel : MdiDialogViewModel<SelectExportTypesContext>
  {
    [InjectionConstructor]
    public SelectExportTypesViewModel(IController controller)
      : base(controller)
    {
    }

    public IEnumerable<TypeWrapperViewModel> Types
    {
      get { return this.ResolveViewModels<TypeWrapperViewModel, TypeWrapper>(CachedTypes); }
    }

    #region IEnumerable<TypeWrapper> CachedTypes

    static object mLock = new object();
    static Collection<TypeWrapper> mCachedTypes;
    static Collection<TypeWrapper> CachedTypes
    {
      get
      {
        lock (mLock)
        {
          if (mCachedTypes != null) return mCachedTypes;
          mCachedTypes = new BasicCollection<TypeWrapper>();
          foreach (Type t in ObjectAttributeHelper.EditableCachedTypes)
          {
            mCachedTypes.Add(new TypeWrapper(t));
          }
          return mCachedTypes;
        }
      }
    }

    #endregion
  }
}

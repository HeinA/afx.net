﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Development.Prism.Dialogs.SelectExportTypes
{
  [Export(typeof(Afx.Prism.ResourceInfo))]
  public class ResourceInfo : Afx.Prism.ResourceInfo
  {
    public override Uri GetUri()
    {
      return new Uri("pack://application:,,,/Development.Prism;component/Dialogs/SelectExportTypes/Resources.xaml", UriKind.Absolute);
    }
  }
}
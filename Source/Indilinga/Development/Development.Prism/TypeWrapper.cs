﻿using Afx.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Development.Prism
{
  public class TypeWrapper : BusinessObject
  {
    public TypeWrapper(bool isNull)
      : base(isNull)
    {
    }

    public TypeWrapper(Type type)
    {
      Type = type;
    }

    Type mType;
    public Type Type
    {
      get { return mType; }
      set { SetProperty<Type>(ref mType, value); }
    }

    public string TypeName
    {
      get
      {
        if (Type == null) return string.Empty;
        return string.Format("{0}, {1}", Type.FullName, Type.Assembly.GetName().Name);
      }
    }

    public override int GetHashCode()
    {
      if (GlobalIdentifier == Guid.Empty) return Guid.Empty.GetHashCode();
      return Type.GetHashCode();
    }

    public override bool Equals(object obj)
    {
      TypeWrapper tw = obj as TypeWrapper;
      if (tw == null) return false;
      if (tw.GlobalIdentifier == Guid.Empty)
      {
        if (GlobalIdentifier == Guid.Empty) return true;
        return false;
      }
      if (Type == null) return false;
      return Type.Equals(tw.Type);
    }
  }
}

﻿using Afx.Business;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Development.Business
{
  [Export(typeof(INamespace))]
  public class Namespace : INamespace
  {
    public const string Development = "http://indilinga.com/Development/Business";

    public string GetUri()
    {
      return Development;
    }
  }
}

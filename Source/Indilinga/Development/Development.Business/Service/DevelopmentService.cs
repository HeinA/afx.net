﻿using Afx.Business;
using Afx.Business.Activities;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Documents;
using Afx.Business.Service;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.WCF;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace Development.Business.Service
{
  [ServiceBehavior(Namespace = Namespace.Development, ConcurrencyMode = ConcurrencyMode.Multiple)]
  [ExceptionShielding]
  [Export(typeof(IService))]
  public partial class DevelopmentService : ServiceBase, IDevelopmentService
  {
    public BasicCollection<BusinessObject> GetExports(BasicCollection<string> types)
    {
      BasicCollection<BusinessObject> ret = new BasicCollection<BusinessObject>();

      using (new ConnectionScope())
      using (new TransactionScope(TransactionScopeOption.Suppress))
      using (new SqlScope("GetExports"))
      {
        foreach (string s in types)
        {
          IInternalPersistanceManager pm = PersistanceManager as IInternalPersistanceManager;
          foreach (BusinessObject bo in pm.GetRepository(Type.GetType(s)).GetAllInstances())
          {
            ret.Add(bo);
          }
        }
      }

      return ret;
    }
  }
}

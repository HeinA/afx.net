﻿using Afx.Business;
using Afx.Business.Collections;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Development.Business.Service
{
  [ServiceContract(Namespace = Namespace.Development)]
  public partial interface IDevelopmentService : IDisposable
  {
    [OperationContract]
    BasicCollection<BusinessObject> GetExports(BasicCollection<string> types);
  }
}

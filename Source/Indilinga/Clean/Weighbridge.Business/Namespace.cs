﻿using Afx.Business;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Weighbridge.Business
{
  [Export(typeof(INamespace))]
  public class Namespace : INamespace
  {
    public const string Weighbridge = "http://indilinga.com/Weighbridge/Business";

    public string GetUri()
    {
      return Weighbridge;
    }
  }
}

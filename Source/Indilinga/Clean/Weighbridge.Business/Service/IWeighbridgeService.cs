﻿using Afx.Business;
using Afx.Business.Collections;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Weighbridge.Business.Service
{
  [ServiceContract(Namespace = Namespace.Weighbridge)]
  public partial interface IWeighbridgeService : IDisposable
  {
  }
}

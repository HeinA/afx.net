﻿using Afx.Business.Data;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using FreightManagement.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Settings
{
  [Export(typeof(MdiSettingViewModel<DefaultBaySetting>))]
  public class DefaultBaySettingViewModel : MdiSettingViewModel<DefaultBaySetting>
  {
    #region Constructors

    [InjectionConstructor]
    public DefaultBaySettingViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    public IEnumerable<Bay> Bays
    {
      get { return Cache.Instance.GetObjects<Bay>(true, b => b.Text, true); }
    }
  }
}

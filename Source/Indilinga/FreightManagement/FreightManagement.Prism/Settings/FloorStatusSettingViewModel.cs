﻿using Afx.Business;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using FreightManagement.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Settings
{
  [Export(typeof(MdiSettingViewModel<FloorStatusSetting>))]
  public class FloorStatusSettingViewModel : MdiSettingViewModel<FloorStatusSetting>
  {
    #region Constructors

    [InjectionConstructor]
    public FloorStatusSettingViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion
  }
}

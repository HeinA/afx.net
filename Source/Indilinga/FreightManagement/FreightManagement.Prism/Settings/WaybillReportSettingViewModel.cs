﻿using Afx.Business;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using FreightManagement.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Settings
{
  [Export(typeof(MdiSettingViewModel<WaybillSetting>))]
  public class WaybillReportSettingViewModel : MdiSettingViewModel<WaybillSetting>
  {
    #region Constructors

    [InjectionConstructor]
    public WaybillReportSettingViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion
  }
}

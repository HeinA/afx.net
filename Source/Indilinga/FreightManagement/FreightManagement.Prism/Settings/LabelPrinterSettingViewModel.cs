﻿using Afx.Prism;
using Afx.Prism.RibbonMdi;
using FreightManagement.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Settings
{
  [Export(typeof(MdiSettingViewModel<LabelPrinterSetting>))]
  public class LabelPrinterSettingViewModel : MdiSettingViewModel<LabelPrinterSetting>
  {
    #region Constructors

    [InjectionConstructor]
    public LabelPrinterSettingViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion
  }
}

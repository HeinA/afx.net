﻿using Afx.Business.Data;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using FreightManagement.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Settings
{
  [Export(typeof(MdiSettingViewModel<CostingSetting>))]
  public class CostingSettingViewModel : MdiSettingViewModel<CostingSetting>
  {
    #region Constructors

    [InjectionConstructor]
    public CostingSettingViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion
  }
}

﻿using Afx.Prism;
using Afx.Prism.RibbonMdi;
using FreightManagement.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Settings
{
  [Export(typeof(MdiSettingViewModel<TextPrinterSetting>))]
  public class TextPrinterSettingViewModel : MdiSettingViewModel<TextPrinterSetting>
  {
    #region Constructors

    [InjectionConstructor]
    public TextPrinterSettingViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion
  }
}

﻿using AccountManagement.Business;
using Afx.Business;
using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Business.Service;
using Afx.Prism;
using Afx.Prism.Events;
using Afx.Prism.RibbonMdi;
using FreightManagement.Business;
using FreightManagement.Business.Service;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Tools.PODHandoverWaybills
{
  public class PODHandoverWaybillsViewModel : MdiDockableToolViewModel<PODHandoverWaybillsContext>
  {
    #region "Constructor"

    [InjectionConstructor]
    public PODHandoverWaybillsViewModel(IController controller)
      : base(controller)
    {
      Title = "POD Handover Waybills";
      //MdiApplicationController.Current.EventAggregator.GetEvent<CacheUpdatedEvent>().Subscribe(OnCacheUpdated);
    }

    #endregion

    #region string ContentId

    public override string ContentId
    {
      get { return PODHandoverWaybillsContext.ControllerKey; }
    }

    #endregion

    #region AccountReference SelectedAccount

    public const string SelectedAccountProperty = "SelectedAccount";
    AccountReference mSelectedAccount;
    public AccountReference SelectedAccount
    {
      get { return mSelectedAccount; }
      set { SetProperty<AccountReference>(ref mSelectedAccount, value); }
    }

    #endregion


    #region WaybillItemViewModel SelectedWaybillsItemViewModel

    public const string SelectedWaybillsItemViewModelProperty = "SelectedWaybillsItemViewModel";
    WaybillItemViewModel mSelectedWaybillsItemViewModel;
    public WaybillItemViewModel SelectedWaybillsItemViewModel
    {
      get { return mSelectedWaybillsItemViewModel; }
      set { SetProperty<WaybillItemViewModel>(ref mSelectedWaybillsItemViewModel, value); }
    }

    #endregion

    #region Collection<WaybillReference> Waybills

    public const string WaybillsProperty = "Waybills";
    Collection<WaybillReference> mWaybills;
    public Collection<WaybillReference> Waybills
    {
      get { return mWaybills; }
      set
      {
        if (SetProperty<Collection<WaybillReference>>(ref mWaybills, value))
        {
          OnPropertyChanged("PODHandoverWaybillsViewModels");
        }
      }
    }

    #endregion

    public IEnumerable<WaybillItemViewModel> PODHandoverWaybillsViewModels
    {
      get { return ResolveViewModels<WaybillItemViewModel, WaybillReference>(Waybills); }
    }

    #region DelegateCommand FilterCommand

    DelegateCommand mFilterCommand;
    public DelegateCommand FilterCommand
    {
      get { return mFilterCommand ?? (mFilterCommand = new DelegateCommand(ExecuteFilter, CanExecuteFilter)); }
    }

    bool CanExecuteFilter()
    {
      return true;
    }

    void ExecuteFilter()
    {
      try
      {
        using (new WaitCursor())
        using (var svc = ProxyFactory.GetService<IFreightManagementService>(SecurityContext.ServerName))
        {
          Waybills = svc.LoadWaybillsForAccount(SelectedAccount);
        }
      }
      catch (Exception ex)
      {
        if (ExceptionHelper.HandleException(ex)) throw ex;
      }
    }

    #endregion

    #region DelegateCommand ItemActivatedCommand

    DelegateCommand mItemActivatedCommand;
    public DelegateCommand ItemActivatedCommand
    {
      get { return mItemActivatedCommand ?? (mItemActivatedCommand = new DelegateCommand(ExecuteItemActivated)); }
    }

    void ExecuteItemActivated()
    {
      try
      {
        FreightManagement.Prism.Documents.PODHandover.PODHandoverController PODHandoverController = MdiApplicationController.Current.ActiveDocument as FreightManagement.Prism.Documents.PODHandover.PODHandoverController;

        if (PODHandoverController == null)
        {
          System.Media.SystemSounds.Exclamation.Play();
          return;
        }

        PODHandoverController.AttachWaybill(SelectedWaybillsItemViewModel.Model);
      }
      catch (Exception ex)
      {
        if (ExceptionHelper.HandleException(ex)) throw ex;
      }
    }

    #endregion
  }
}

﻿using Afx.Prism.RibbonMdi.Ribbon;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Tools.PODHandoverWaybills
{
  [Export(typeof(IRibbonItem))]
  public class PODHandoverWaybillsButton : RibbonToggleButtonViewModel
  {
    public override string TabName
    {
      get { return FreightManagementTab.TabName; }
    }

    public override string GroupName
    {
      get { return RoutingGroup.GroupName; }
    }

    public override string ItemIdentifier
    {
      get { return "PODHandoverWaybills"; }
    }

    public override int Index
    {
      get { return 3; }
    }

    PODHandoverWaybillsController mPODHandoverWaybillsController = null;
    [Dependency]
    public PODHandoverWaybillsController PODHandoverWaybillsController
    {
      get { return mPODHandoverWaybillsController; }
      set
      {
        mPODHandoverWaybillsController = value;
        mPODHandoverWaybillsController.ViewModel.PropertyChanged += ViewModel_PropertyChanged;
      }
    }

    void ViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
    {
      if (e.PropertyName == "IsVisible")
      {
        OnPropertyChanged("IsChecked");
      }
    }

    public override bool IsChecked
    {
      get { return PODHandoverWaybillsController.ViewModel.IsVisible; }
      set { PODHandoverWaybillsController.ViewModel.IsVisible = value; }
    }
  }
}

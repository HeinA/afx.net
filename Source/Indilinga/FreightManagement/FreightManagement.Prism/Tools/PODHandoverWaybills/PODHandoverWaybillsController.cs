﻿using Afx.Prism;
using Afx.Prism.RibbonMdi;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Tools.PODHandoverWaybills
{
  public class PODHandoverWaybillsController : MdiDockableToolController<PODHandoverWaybillsContext, PODHandoverWaybillsViewModel>
  {
    [InjectionConstructor]
    public PODHandoverWaybillsController(IController controller)
      : base(PODHandoverWaybillsContext.ControllerKey, controller)
    {
      DataContext = new PODHandoverWaybillsContext();
    }

    protected override void ConfigureContainer()
    {
      MdiApplicationController.Current.Container.RegisterInstance<PODHandoverWaybillsController>(this);
      base.ConfigureContainer();
    }
  }
}

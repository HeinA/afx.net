﻿using Afx.Prism;
using FreightManagement.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Tools.PODHandoverWaybills
{
  public class WaybillItemViewModel : SelectableViewModel<WaybillReference>
  {
    [InjectionConstructor]
    public WaybillItemViewModel(IController controller)
      : base(controller)
    {
    }

    new PODHandoverWaybillsViewModel Parent
    {
      get { return (PODHandoverWaybillsViewModel)base.Parent; }
    }

    public override bool IsSelected
    {
      get
      {
        if (base.IsSelected) Parent.SelectedWaybillsItemViewModel = this;
        return base.IsSelected;
      }
      set
      {
        base.IsSelected = value;
        if (base.IsSelected) Parent.SelectedWaybillsItemViewModel = this;
      }
    }
  }
}

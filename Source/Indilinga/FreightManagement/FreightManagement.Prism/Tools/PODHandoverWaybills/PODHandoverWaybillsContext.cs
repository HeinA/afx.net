﻿using Afx.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Tools.PODHandoverWaybills
{
  public class PODHandoverWaybillsContext : BusinessObject
  {
    public const string ControllerKey = "FreightManagement.Prism.Tools.PODHandoverWaybills.PODHandoverWaybillsController";
  }
}

﻿using Afx.Prism;
using Afx.Prism.RibbonMdi;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Tools.NextWaybillRoute
{
  public class NextWaybillRouteController : MdiDockableToolController<NextWaybillRouteContext, NextWaybillRouteViewModel>
  {
    [InjectionConstructor]
    public NextWaybillRouteController(IController controller)
      : base(NextWaybillRouteContext.ControllerKey, controller)
    {
      DataContext = new NextWaybillRouteContext();
    }

    protected override void ConfigureContainer()
    {
      MdiApplicationController.Current.Container.RegisterInstance<NextWaybillRouteController>(this);
      base.ConfigureContainer();
    }
  }
}

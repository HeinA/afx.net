﻿using Afx.Business;
using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Business.Service;
using Afx.Prism;
using Afx.Prism.Events;
using Afx.Prism.RibbonMdi;
using FreightManagement.Business;
using FreightManagement.Business.Service;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace FreightManagement.Prism.Tools.NextWaybillRoute
{
  public class NextWaybillRouteViewModel : MdiDockableToolViewModel<NextWaybillRouteContext>
  {
    [InjectionConstructor]
    public NextWaybillRouteViewModel(IController controller)
      : base(controller)
    {
      Title = "Waybills Routing";
      MdiApplicationController.Current.EventAggregator.GetEvent<CacheUpdatedEvent>().Subscribe(OnCacheUpdated);
    }

    private void OnCacheUpdated(EventArgs obj)
    {
      if (BusinessObject.IsNull(SelectedDistributionCenter)) SelectedDistributionCenter = DistributionCenters.Where(dc => dc.OrganizationalUnit.Equals(SecurityContext.OrganizationalUnit)).FirstOrDefault();
    }

    #region string ContentId

    public override string ContentId
    {
      get { return NextWaybillRouteContext.ControllerKey; }
    }

    #endregion

    #region WaybillItemViewModel SelectedWaybillViewModel

    public const string SelectedWaybillViewModelProperty = "SelectedWaybillViewModel";
    WaybillItemViewModel mSelectedWaybillViewModel;
    public WaybillItemViewModel SelectedWaybillViewModel
    {
      get { return mSelectedWaybillViewModel; }
      set { SetProperty<WaybillItemViewModel>(ref mSelectedWaybillViewModel, value); }
    }

    #endregion

    #region DistributionCenter SelectedDistributionCenter

    public const string SelectedDistributionCenterProperty = "SelectedDistributionCenter";
    DistributionCenter mSelectedDistributionCenter;
    public DistributionCenter SelectedDistributionCenter
    {
      get { return mSelectedDistributionCenter; }
      set
      {
        if (SetProperty<DistributionCenter>(ref mSelectedDistributionCenter, value))
        {
          OnPropertyChanged("Routes");
        }
      }
    }

    #endregion

    #region IEnumerable<DistributionCenter> DistributionCenters

    public IEnumerable<DistributionCenter> DistributionCenters
    {
      get { return Cache.Instance.GetObjects<DistributionCenter>(r => r.Name, true); }
    }

    #endregion

    #region IEnumerable<Route> Routes

    public IEnumerable<Route> Routes
    {
      get
      {
        if (BusinessObject.IsNull(SelectedDistributionCenter)) return null;
        return Cache.Instance.GetObjects<Route>(true, r => r.Origin.Equals(SelectedDistributionCenter.Location), r => r.Name, true);
      }
    }

    #endregion

    #region Route SelectedRoute

    public const string SelectedRouteProperty = "SelectedRoute";
    Route mSelectedRoute;
    public Route SelectedRoute
    {
      get { return mSelectedRoute; }
      set { SetProperty<Route>(ref mSelectedRoute, value); }
    }

    #endregion

    #region Collection<WaybillNextRoute> Waybills

    public const string WaybillsProperty = "Waybills";
    Collection<WaybillReference> mWaybills;
    Collection<WaybillReference> Waybills
    {
      get { return mWaybills; }
      set
      {
        if (SetProperty<Collection<WaybillReference>>(ref mWaybills, value))
        {
          OnPropertyChanged("WaybillViewModels");
        }
      }
    }

    #endregion

    public IEnumerable<WaybillItemViewModel> WaybillViewModels
    {
      get { return Waybills == null ? null : ResolveViewModels<WaybillItemViewModel, WaybillReference>(Waybills).OrderByDescending(w => w.Model.DeliveryPriority); }
    }

    #region DelegateCommand FilterCommand

    DelegateCommand mFilterCommand;
    public DelegateCommand FilterCommand
    {
      get { return mFilterCommand ?? (mFilterCommand = new DelegateCommand(ExecuteFilter, CanExecuteFilter)); }
    }

    bool CanExecuteFilter()
    {
      return true;
    }

    void ExecuteFilter()
    {
      try
      {
        using (new WaitCursor())
        using (var svc = ProxyFactory.GetService<IFreightManagementService>(SecurityContext.ServerName))
        {
          Waybills = svc.LoadWaybillsForRoute(SelectedDistributionCenter, SelectedRoute);
        }
      }
      catch (Exception ex)
      {
        if (ExceptionHelper.HandleException(ex)) throw ex;
      }
    }

    #endregion

    #region DelegateCommand ItemActivatedCommand

    DelegateCommand mItemActivatedCommand;
    public DelegateCommand ItemActivatedCommand
    {
      get { return mItemActivatedCommand ?? (mItemActivatedCommand = new DelegateCommand(ExecuteItemActivated)); }
    }

    void ExecuteItemActivated()
    {
      FreightManagement.Prism.Documents.DeliveryManifest.DeliveryManifestController deliveryManifestController = MdiApplicationController.Current.ActiveDocument as FreightManagement.Prism.Documents.DeliveryManifest.DeliveryManifestController;
      FreightManagement.Prism.Documents.TransferManifest.TransferManifestController transferManifestController = MdiApplicationController.Current.ActiveDocument as FreightManagement.Prism.Documents.TransferManifest.TransferManifestController;

      if (deliveryManifestController == null && transferManifestController == null)
      {
        System.Media.SystemSounds.Exclamation.Play();
        return;
      }

      if (deliveryManifestController != null) deliveryManifestController.AttachWaybill(SelectedWaybillViewModel.Model);
      if (transferManifestController != null) transferManifestController.AttachWaybill(SelectedWaybillViewModel.Model);
    }

    #endregion

    #region DelegateCommand<WaybillReference> OpenWaybillCommand

    DelegateCommand<WaybillReference> mOpenWaybillCommand;
    public DelegateCommand<WaybillReference> OpenWaybillCommand
    {
      get { return mOpenWaybillCommand ?? (mOpenWaybillCommand = new DelegateCommand<WaybillReference>(ExecuteOpenWaybill, CanExecuteOpenWaybill)); }
    }

    bool CanExecuteOpenWaybill(WaybillReference args)
    {
      return true;
    }

    void ExecuteOpenWaybill(WaybillReference args)
    {
      MdiApplicationController.Current.EditDocument(Business.Waybill.DocumentTypeIdentifier, args.GlobalIdentifier);
    }

    #endregion
  }
}

﻿using Afx.Prism.RibbonMdi.Ribbon;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Tools.NextWaybillRoute
{
  [Export(typeof(IRibbonItem))]
  public class NextWaybillRouteButton : RibbonToggleButtonViewModel
  {
    public override string TabName
    {
      get { return FreightManagementTab.TabName; }
    }

    public override string GroupName
    {
      get { return RoutingGroup.GroupName; }
    }

    public override string ItemIdentifier
    {
      get { return "NextWaybillRoute"; }
    }

    public override int Index
    {
      get { return 1; }
    }

    NextWaybillRouteController mNextWaybillRouteController = null;
    [Dependency]
    public NextWaybillRouteController NextWaybillRouteController
    {
      get { return mNextWaybillRouteController; }
      set
      {
        mNextWaybillRouteController = value;
        mNextWaybillRouteController.ViewModel.PropertyChanged += ViewModel_PropertyChanged;
      }
    }

    void ViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
    {
      if (e.PropertyName == "IsVisible")
      {
        OnPropertyChanged("IsChecked");
      }
    }

    public override bool IsChecked
    {
      get { return NextWaybillRouteController.ViewModel.IsVisible; }
      set { NextWaybillRouteController.ViewModel.IsVisible = value; }
    }
  }
}

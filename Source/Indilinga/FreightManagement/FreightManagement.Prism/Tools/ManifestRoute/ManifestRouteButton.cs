﻿using Afx.Prism.RibbonMdi.Ribbon;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Tools.ManifestRoute
{
  [Export(typeof(IRibbonItem))]
  public class ManifestRouteButton : RibbonToggleButtonViewModel
  {
    public override string TabName
    {
      get { return FreightManagementTab.TabName; }
    }

    public override string GroupName
    {
      get { return RoutingGroup.GroupName; }
    }

    public override string ItemIdentifier
    {
      get { return "ManifestlRoute"; }
    }

    public override int Index
    {
      get { return 2; }
    }

    ManifestRouteController mManifestRouteController = null;
    [Dependency]
    public ManifestRouteController ManifestRouteController
    {
      get { return mManifestRouteController; }
      set
      {
        mManifestRouteController = value;
        mManifestRouteController.ViewModel.PropertyChanged += ViewModel_PropertyChanged;
      }
    }

    void ViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
    {
      if (e.PropertyName == "IsVisible")
      {
        OnPropertyChanged("IsChecked");
      }
    }

    public override bool IsChecked
    {
      get { return ManifestRouteController.ViewModel.IsVisible; }
      set { ManifestRouteController.ViewModel.IsVisible = value; }
    }
  }
}

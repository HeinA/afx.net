﻿using Afx.Prism;
using FreightManagement.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Tools.ManifestRoute
{
  public class ManifestItemViewModel : SelectableViewModel<DeliveryManifestReference>
  {
    [InjectionConstructor]
    public ManifestItemViewModel(IController controller)
      : base(controller)
    {
    }

    new ManifestRouteViewModel Parent
    {
      get { return (ManifestRouteViewModel)base.Parent; }
    }

    public override bool IsSelected
    {
      get
      {
        if (base.IsSelected) Parent.SelectedManifestViewModel = this;
        return base.IsSelected;
      }
      set
      {
        base.IsSelected = value;
        if (base.IsSelected) Parent.SelectedManifestViewModel = this;
      }
    }
  }
}

﻿using Afx.Business;
using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Business.Service;
using Afx.Prism;
using Afx.Prism.Events;
using Afx.Prism.RibbonMdi;
using FreightManagement.Business;
using FreightManagement.Business.Service;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Tools.ManifestRoute
{
  public class ManifestRouteViewModel : MdiDockableToolViewModel<ManifestRouteContext>
  {
    [InjectionConstructor]
    public ManifestRouteViewModel(IController controller)
      : base(controller)
    {
      Title = "DeliveryManifest Routing";
      MdiApplicationController.Current.EventAggregator.GetEvent<CacheUpdatedEvent>().Subscribe(OnCacheUpdated);
    }

    private void OnCacheUpdated(EventArgs obj)
    {
      if (BusinessObject.IsNull(SelectedDistributionCenter)) SelectedDistributionCenter = DistributionCenters.Where(dc => dc.OrganizationalUnit.Equals(SecurityContext.OrganizationalUnit)).FirstOrDefault();
    }

    #region string ContentId

    public override string ContentId
    {
      get { return ManifestRouteContext.ControllerKey; }
    }

    #endregion

    #region DistributionCenter SelectedDistributionCenter

    public const string SelectedDistributionCenterProperty = "SelectedDistributionCenter";
    DistributionCenter mSelectedDistributionCenter;
    public DistributionCenter SelectedDistributionCenter
    {
      get { return mSelectedDistributionCenter; }
      set
      {
        if (SetProperty<DistributionCenter>(ref mSelectedDistributionCenter, value))
        {
          OnPropertyChanged("Routes");
          SelectedRoute = Routes.FirstOrDefault();
        }
      }
    }

    #endregion

    #region ManifestItemViewModel SelectedManifestViewModel

    public const string SelectedManifestViewModelProperty = "SelectedManifestViewModel";
    ManifestItemViewModel mSelectedManifestViewModel;
    public ManifestItemViewModel SelectedManifestViewModel
    {
      get { return mSelectedManifestViewModel; }
      set { SetProperty<ManifestItemViewModel>(ref mSelectedManifestViewModel, value); }
    }

    #endregion

    public IEnumerable<DistributionCenter> DistributionCenters
    {
      get { return Cache.Instance.GetObjects<DistributionCenter>(r => r.Name, true); }
    }

    public IEnumerable<Route> Routes
    {
      get
      {
        if (BusinessObject.IsNull(SelectedDistributionCenter)) return null;
        return Cache.Instance.GetObjects<Route>(r => r.Origin.Equals(SelectedDistributionCenter.Location), r => r.Name, true);
      }
    }

    #region Route SelectedRoute

    public const string SelectedRouteProperty = "SelectedRoute";
    Route mSelectedRoute;
    public Route SelectedRoute
    {
      get { return mSelectedRoute; }
      set { SetProperty<Route>(ref mSelectedRoute, value); }
    }

    #endregion

    #region Collection<DeliveryManifestReference> DManifests

    public const string DManifestsProperty = "Manifests";
    Collection<DeliveryManifestReference> mDManifests;
    public Collection<DeliveryManifestReference> DManifests
    {
      get { return mDManifests; }
      set
      {
        if (SetProperty<Collection<DeliveryManifestReference>>(ref mDManifests, value))
        {
          OnPropertyChanged("ManifestViewModels");
        }
      }
    }

    #endregion

    #region Collection<CollectionManifestReference> CManifests

    public const string CManifestsProperty = "CManifests";
    Collection<CollectionManifestReference> mCManifests;
    public Collection<CollectionManifestReference> CManifests
    {
      get { return mCManifests; }
      set
      {
        if (SetProperty<Collection<CollectionManifestReference>>(ref mCManifests, value))
        {
          OnPropertyChanged("ManifestViewModels");
        }
      }
    }

    #endregion

    #region Collection<TransferManifestReference> TManifests

    public const string TManifestsProperty = "TManifests";
    Collection<TransferManifestReference> mTManifests;
    public Collection<TransferManifestReference> TManifests
    {
      get { return mTManifests; }
      set
      {
        if (SetProperty<Collection<TransferManifestReference>>(ref mTManifests, value))
        {
          OnPropertyChanged("ManifestViewModels");
        }
      }
    }

    #endregion

    public IEnumerable<ManifestItemViewModel> ManifestViewModels
    {
      get { return ResolveViewModels<ManifestItemViewModel, DeliveryManifestReference>(DManifests); }
    }

    #region DelegateCommand FilterCommand

    DelegateCommand mFilterCommand;
    public DelegateCommand FilterCommand
    {
      get { return mFilterCommand ?? (mFilterCommand = new DelegateCommand(ExecuteFilter, CanExecuteFilter)); }
    }

    bool CanExecuteFilter()
    {
      return true;
    }

    void ExecuteFilter()
    {
      try
      {
        using (new WaitCursor())
        using (var svc = ProxyFactory.GetService<IFreightManagementService>(SecurityContext.ServerName))
        {
          DManifests = svc.LoadDeliveryManifestsForRoute(SelectedRoute);
          CManifests = svc.LoadCollectionManifestsForRoute(SelectedRoute);
          TManifests = svc.LoadTransferManifestsForRoute(SelectedRoute);
        }
      }
      catch (Exception ex)
      {
        if (ExceptionHelper.HandleException(ex)) throw ex;
      }
    }

    #endregion

    #region DelegateCommand ItemActivatedCommand

    DelegateCommand mItemActivatedCommand;
    public DelegateCommand ItemActivatedCommand
    {
      get { return mItemActivatedCommand ?? (mItemActivatedCommand = new DelegateCommand(ExecuteItemActivated)); }
    }

    void ExecuteItemActivated()
    {
      //try
      //{
        //TODO: Change for tripsheets in FreightManagement
      //  FreightManagement.Prism.Documents.Tripsheet.TripsheetController tripsheetController = MdiApplicationController.Current.ActiveDocument as FreightManagement.Prism.Documents.Tripsheet.TripsheetController;

      //  if (tripsheetController == null)
      //  {
      //    System.Media.SystemSounds.Exclamation.Play();
      //    return;
      //  }

      //  tripsheetController.AttachManifest(SelectedManifestViewModel.Model);
      //}
      //catch (Exception ex)
      //{
      //  if (ExceptionHelper.HandleException(ex)) throw ex;
      //}
    }

    #endregion
  }
}

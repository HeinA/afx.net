﻿using Afx.Prism;
using Afx.Prism.RibbonMdi;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Tools.ManifestRoute
{
  public class ManifestRouteController : MdiDockableToolController<ManifestRouteContext, ManifestRouteViewModel>
  {
    [InjectionConstructor]
    public ManifestRouteController(IController controller)
      : base(ManifestRouteContext.ControllerKey, controller)
    {
      DataContext = new ManifestRouteContext();
    }

    protected override void ConfigureContainer()
    {
      MdiApplicationController.Current.Container.RegisterInstance<ManifestRouteController>(this);
      base.ConfigureContainer();
    }
  }
}

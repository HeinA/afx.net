﻿using Afx.Prism.RibbonMdi.Ribbon;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism
{
  [Export(typeof(IRibbonGroup))]
  public class ConfigurationGroup : RibbonGroup
  {
    public const string GroupName = "Configuration";

    public override string TabName
    {
      get { return FreightManagementTab.TabName; }
    }

    public override string Name
    {
      get { return GroupName; }
    }

    public override bool PlaceBehind
    {
      get { return true; }
    }
  }
}

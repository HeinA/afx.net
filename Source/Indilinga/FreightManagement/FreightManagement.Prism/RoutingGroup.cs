﻿using Afx.Prism.RibbonMdi.Ribbon;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism
{
  [Export(typeof(IRibbonGroup))]
  public class RoutingGroup : RibbonGroup
  {
    public const string GroupName = "Routing Tools";

    public override string TabName
    {
      get { return FreightManagementTab.TabName; }
    }

    public override string Name
    {
      get { return GroupName; }
    }
  }
}

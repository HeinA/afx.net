﻿using Afx.Business.Security;
using Afx.Business.Service;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using FreightManagement.Business.Service;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace FreightManagement.Prism.Ribbon.GetAccountDialog
{
  public class GetAccountDialogController : MdiDialogController<GetAccountDialogContext, GetAccountDialogViewModel>
  {
    public const string GetAccountDialogControllerKey = "FreightManagement.Prism.Ribbon.GetAccountDialog.GetAccountDialogController";

    public GetAccountDialogController(IController controller)
      : base(GetAccountDialogControllerKey, controller)
    {
    }

    protected override bool OnRun()
    {
      if (DataContext == null) DataContext = new GetAccountDialogContext();
      ViewModel.Caption = "Recost Account";
      return base.OnRun();
    }

    protected override void ApplyChanges()
    {
      base.ApplyChanges();

      using (var svc = ProxyFactory.GetService<IFreightManagementService>(SecurityContext.MasterServer.Name))
      {
        svc.ReCostWaybills(DataContext.Account.GlobalIdentifier);
      }
    }
  }
}

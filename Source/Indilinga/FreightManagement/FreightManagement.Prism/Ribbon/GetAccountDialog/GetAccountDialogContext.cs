﻿using AccountManagement.Business;
using Afx.Business;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Ribbon.GetAccountDialog
{
  public class GetAccountDialogContext : BusinessObject
  {
    #region AccountReference Account

    public const string AccountProperty = "Account";
    AccountReference mAccount;
    public AccountReference Account
    {
      get { return mAccount; }
      set { SetProperty<AccountReference>(ref mAccount, value); }
    }

    #endregion
  }
}
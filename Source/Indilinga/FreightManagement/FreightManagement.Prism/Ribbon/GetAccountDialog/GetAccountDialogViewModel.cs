﻿using Afx.Prism;
using Afx.Prism.RibbonMdi;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Ribbon.GetAccountDialog
{
  public class GetAccountDialogViewModel : MdiDialogViewModel<GetAccountDialogContext>
  {
    [InjectionConstructor]
    public GetAccountDialogViewModel(IController controller)
      : base(controller)
    {
    }
  }
}

﻿//using AccountManagement.Business.Service;
//using Afx.Business;
//using Afx.Business.Security;
//using Afx.Business.Service;
//using Afx.Prism.RibbonMdi;
//using Afx.Prism.RibbonMdi.Dialogs.Import;
//using Afx.Prism.RibbonMdi.Ribbon;
//using FreightManagement.Business.Service;
//using System;
//using System.Collections.Generic;
//using System.ComponentModel.Composition;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;

//namespace FreightManagement.Prism.Ribbon
//{
//  [Export(typeof(IRibbonItem))]
//  public class ImportAccountsButton : RibbonButtonViewModel, IImportController
//  {
//    public override string TabName
//    {
//      get { return AdministrationTab.TabName; }
//    }

//    public override string GroupName
//    {
//      get { return ToolsGroup.GroupName; }
//    }

//    public override string ItemIdentifier
//    {
//      get { return "ImportAccountsButtonAdministration"; }
//    }

//    public override int Index
//    {
//      get { return 2; }
//    }

//    protected override void OnExecute()
//    {
//      ImportDialogController ic = MdiApplicationController.Current.CreateChildController<ImportDialogController>();
//      ic.ImportController = this;
//      ic.Run();
//    }

//    public void DoImport(ExternalSystem system, string Identifier)
//    {
//      using (var svc = ProxyFactory.GetService<IFreightManagementService>(SecurityContext.MasterServer.Name))
//      {
//        svc.ImportAccounts(system);
//      }
//    }
//  }
//}

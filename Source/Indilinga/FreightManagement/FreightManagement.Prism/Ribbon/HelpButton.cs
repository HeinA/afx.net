﻿using AccountManagement.Business.Service;
using Afx.Business;
using Afx.Business.Security;
using Afx.Business.Service;
using Afx.Prism.RibbonMdi;
using Afx.Prism.RibbonMdi.Dialogs.Import;
using Afx.Prism.RibbonMdi.Ribbon;
using FreightManagement.Business.Service;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Ribbon
{
  [Export(typeof(IRibbonItem))]
  public class HelpButton : RibbonButtonViewModel
  {
    public override string TabName
    {
      get { return HomeTab.TabName; }
    }

    public override string GroupName
    {
      get { return ToolsGroup.GroupName; }
    }

    public override string ItemIdentifier
    {
      get { return "HelpButton"; }
    }

    public override int Index
    {
      get { return 4; }
    }

    protected override void OnExecute()
    {
      Process.Start(ConfigurationManager.AppSettings["Help"]);
    }
  }
}

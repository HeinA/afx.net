﻿using Afx.Business.Security;
using Afx.Business.Service;
using Afx.Prism.RibbonMdi;
using Afx.Prism.RibbonMdi.Ribbon;
using FreightManagement.Business.Service;
using FreightManagement.Prism.Ribbon.RecostDialog;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Ribbon
{
  [Export(typeof(IRibbonItem))]
  public class ReCostButton : RibbonButtonViewModel
  {
    public override string TabName
    {
      get { return AdministrationTab.TabName; }
    }

    public override string GroupName
    {
      get { return ToolsGroup.GroupName; }
    }

    public override string ItemIdentifier
    {
      get { return "ReCostButtonAdministration"; }
    }

    public override int Index
    {
      get { return 2; }
    }

    protected override void OnExecute()
    {
      var c = MdiApplicationController.Current.CreateChildController<RecostDialogController>();
      c.Run();
    }
  }
}

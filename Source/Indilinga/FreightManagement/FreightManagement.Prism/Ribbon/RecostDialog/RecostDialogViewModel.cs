﻿using Afx.Prism;
using Afx.Prism.RibbonMdi;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Ribbon.RecostDialog
{
  public class RecostDialogViewModel : MdiDialogViewModel<RecostDialogContext>
  {
    [InjectionConstructor]
    public RecostDialogViewModel(IController controller)
      : base(controller)
    {
    }
  }
}

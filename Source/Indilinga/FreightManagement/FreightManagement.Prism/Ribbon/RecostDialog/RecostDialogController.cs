﻿using Afx.Business.Security;
using Afx.Business.Service;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using FreightManagement.Business.Service;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace FreightManagement.Prism.Ribbon.RecostDialog
{
  public class RecostDialogController : MdiDialogController<RecostDialogContext, RecostDialogViewModel>
  {
    public const string RecostDialogControllerKey = "FreightManagement.Prism.Ribbon.RecostDialog.RecostDialogController";

    public RecostDialogController(IController controller)
      : base(RecostDialogControllerKey, controller)
    {
    }

    protected override bool OnRun()
    {
      if (DataContext == null) DataContext = new RecostDialogContext();
      ViewModel.Caption = "Recost Account";
      return base.OnRun();
    }

    protected override void ApplyChanges()
    {
      base.ApplyChanges();

      Stopwatch sw = new Stopwatch();
      sw.Start();

      using (var svc = ProxyFactory.GetService<IFreightManagementService>(SecurityContext.MasterServer.Name))
      {
        svc.ReCostWaybills(DataContext.Account.GlobalIdentifier, DataContext.StartDate.Value, DataContext.EndDate.Value);
      }

      Debug.WriteLine("Seconds: {0}", sw.ElapsedMilliseconds / 1000);
    }
  }
}

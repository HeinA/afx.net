﻿using AccountManagement.Business;
using Afx.Business;
using Afx.Business.Validation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Ribbon.RecostDialog
{
  public class RecostDialogContext : BusinessObject
  {
    #region AccountReference Account

    public const string AccountProperty = "Account";
    AccountReference mAccount = new AccountReference(true);
    public AccountReference Account
    {
      get { return mAccount; }
      set { SetProperty<AccountReference>(ref mAccount, value); }
    }

    #endregion

    #region DateTime StartDate

    public const string StartDateProperty = "StartDate";
    DateTime? mStartDate;
    [Mandatory("Start Date is mandatory.")]
    public DateTime? StartDate
    {
      get { return mStartDate; }
      set { SetProperty<DateTime?>(ref mStartDate, value); }
    }

    #endregion

    #region DateTime EndDate

    public const string EndDateProperty = "EndDate";
    DateTime? mEndDate;
    [Mandatory("End Date is mandatory.")]
    public DateTime? EndDate
    {
      get { return mEndDate; }
      set { SetProperty<DateTime?>(ref mEndDate, value); }
    }

    #endregion
  }
}
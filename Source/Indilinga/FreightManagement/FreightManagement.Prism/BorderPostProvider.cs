﻿using Afx.Business.Data;
using FreightManagement.Business;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfControls.Editors;

namespace FreightManagement.Prism
{
  public class BorderPostsProvider : ISuggestionProvider
  {
    IEnumerable<BorderPosts> mBorderPosts = null;
    public IEnumerable GetSuggestions(string filter)
    {
      if (mBorderPosts == null) mBorderPosts = Cache.Instance.GetObjects<BorderPosts>();
      return mBorderPosts.Where(c => c.Text.ToUpperInvariant().Contains(filter.ToUpperInvariant()));
    }
  }
}

﻿using Afx.Prism;
using Afx.Prism.RibbonMdi;
using FreightManagement.Business;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism
{
  [Export(typeof(IModuleInfo))]
  public class ModuleInfo : IModuleInfo
  {
    public string ModuleNamespace
    {
      get { return Namespace.FreightManagement; }
    }

    public string[] ModuleDependencies
    {
      get { return new string[] { Afx.Business.Namespace.Afx, AccountManagement.Business.Namespace.AccountManagement, Geographics.Business.Namespace.Geographics }; }
    }

    public Type ModuleController
    {
      get { return typeof(FreightManagementModuleController); }
    }
  }
}
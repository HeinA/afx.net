﻿using Afx.Business;
using FreightManagement.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Reports.CollectionManifest
{
  public class ItemPrintViewModel
  {
    public ItemPrintViewModel(Business.WaybillReference item)
    {
      Item = item;
    }

    public string WaybillNumber
    {
      get { return Item.WaybillNumber; }
    }

    #region WaybillCargoItem Item

    Business.WaybillReference mItem;
    Business.WaybillReference Item
    {
      get { return mItem; }
      set { mItem = value; }
    }

    #endregion

    public string Account
    {
      get { return Item.Account.Name; }
    }


    #region Collection Information

    public string CollectionAddress
    {
      get { return Item.CollectionAddress; }
    }

    public string CollectionCity
    {
      get
      {
        if (BusinessObject.IsNull(Item.CollectionCity)) return string.Empty;
        return Item.CollectionCity.City.Name;
      }
    }

    public string CollectionSuburb
    {
      get
      {
        if (BusinessObject.IsNull(Item.CollectionSuburb)) return string.Empty;
        return Item.CollectionSuburb.Name;
      }
    }

    public string CollectionFull
    {
      get
      {
        if (BusinessObject.IsNull(Item.FullCollectionLocation)) return string.Empty;
        return Item.FullCollectionLocation.Name;
      }
    }

    public string Consignee
    {
      get
      {
        return Item.Consignee;
      }
    }

    #endregion

    #region Delivery Information

    public string DeliveryAddress
    {
      get { return String.Format("{0}\r\n{1}", Item.Consignee, String.Empty); ; }
    }

    public string DeliveryCity
    {
      get
      {
        if (BusinessObject.IsNull(Item.DeliveryCity)) return string.Empty;
        return Item.DeliveryCity.City.Name;
      }
    }

    public DateTime DeliveryDate
    {
      get { return Item.DeliveryDate; }
    }

    public string DeliverySuburb
    {
      get
      {
        if (BusinessObject.IsNull(Item.DeliverySuburb)) return string.Empty;
        return Item.DeliverySuburb.Name;
      }
    }

    public string DeliveryFull
    {
      get
      {
        if (BusinessObject.IsNull(Item.FullDeliveryLocation)) return string.Empty;
        return Item.FullDeliveryLocation.Name;
      }
    }

    public string Consigner
    {
      get
      {
        return Item.Consigner;
      }
    }

    #endregion


    #region Dimensions and Weights

    public decimal PhysicalWeight
    {
      get { return Item.PhysicalWeight; }
    }

    public decimal VolumeWeight
    {
      get { return Item.Volume; }
    }

    public int TotalItems
    {
      get { return Item.Items; }
    }

    #endregion


    #region Income

    /// <summary>
    /// Documentation Income (decimal)
    /// </summary>
    public decimal ChargeDocumentation
    {
      get { return Item.ChargeDocumentation; }
    }

    /// <summary>
    /// Collection / Delivery Income (decimal)
    /// </summary>
    public decimal ChargeCollectionDelivery
    {
      get { return Item.ChargeCollectionDelivery; }
    }

    /// <summary>
    /// Transport Income (decimal)
    /// </summary>
    public decimal ChargeTransport
    {
      get { return Item.ChargeTransport; }
    }

    /// <summary>
    /// Other Income (decimal)
    /// </summary>
    public decimal ChargeOther
    {
      get { return Item.ChargeOther; }
    }

    /// <summary>
    /// Fuel Levy Income (decimal)
    /// </summary>
    public decimal ChargeVat
    {
      get { return Item.ChargeVat; }
    }

    /// <summary>
    /// Total Income (decimal)
    /// </summary>
    public decimal Income
    {
      get { return ChargeDocumentation + ChargeCollectionDelivery + ChargeTransport + ChargeOther + ChargeVat; }
    }

    #endregion


    #region Terms & notes

    public string Terms
    {
      get { return "Important: This shipment is acepted by us, are subject to our standard conditions of trade that are available on request."; }
    }

    public string Note
    {
      get { return "Note: No liability will be accepted unless arrangements have been made for liability cover prior to shipment of freight. To arrange cover, please contact CSD department for a liability reference number."; }
    }

    #endregion

  }
}

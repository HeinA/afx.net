﻿using Afx.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Reports.CollectionManifest
{
  public class CollectionManifestPrintViewModel
  {
    public CollectionManifestPrintViewModel(FreightManagement.Business.CollectionManifest details)
    {
      Details = details;
    }

    #region CollectionManifest Details

    FreightManagement.Business.CollectionManifest mDetails;
    FreightManagement.Business.CollectionManifest Details
    {
      get { return mDetails; }
      set { mDetails = value; }
    }

    #endregion

    public string RouteOrigin
    {
      get
      {
        if (BusinessObject.IsNull(Details.Route)) return string.Empty;
        return Details.Route.Origin.Name;
      }
    }

    public string RouteDestination
    {
      get
      {
        if (BusinessObject.IsNull(Details.Route)) return string.Empty;
        return Details.Route.Destination.Name;
      }
    }

    public int Quantity
    {
      get { return Details.Items; }
    }

    public decimal Weight
    {
      get { return Details.Weight; }
    }

    public int Count
    {
      get { return Details.WaybillCount; }
    }

    public string Driver
    {
      get { return Details.Driver.Fullname;  }
    }

    public string Courier
    {
      get { return Details.Courier.CompanyName; }
    }
  }
}

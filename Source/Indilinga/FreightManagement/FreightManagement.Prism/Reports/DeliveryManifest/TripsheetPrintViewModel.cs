﻿using Afx.Business;
using System;
using System.Linq;

namespace FreightManagement.Prism.Reports.Tripsheet
{
  public class TripsheetPrintViewModel
  {
    public TripsheetPrintViewModel(FreightManagement.Business.DeliveryManifest details)
    {
      Details = details;
    }

    #region Tripsheet Details

    FreightManagement.Business.DeliveryManifest mDetails;
    FreightManagement.Business.DeliveryManifest Details
    {
      get { return mDetails; }
      set { mDetails = value; }
    }

    #endregion


    public string Driver
    {
      get { return BusinessObject.IsNull(Details.Driver) ? null : Details.Driver.Fullname; }
    }

    public string CoDriver
    {
      get { return BusinessObject.IsNull(Details.CoDriver) ? null : Details.CoDriver.Fullname; }
    }

    public string Contractor
    {
      get { return BusinessObject.IsNull(Details.Courier) ? null : Details.Courier.CompanyName; }
    }

    public string DriverPassport
    {
      get { return BusinessObject.IsNull(Details.Driver) ? null : Details.Driver.PassportNumber; }
    }

    public DateTime? DriverPassportExpiry
    {
      get { return BusinessObject.IsNull(Details.Driver) ? null : (DateTime?)Details.Driver.PassportExpiryDate; }
    }

    public string CoDriverPassport
    {
      get { return BusinessObject.IsNull(Details.CoDriver) ? null : Details.CoDriver.PassportNumber; }
    }

    public DateTime? CoDriverPassportExpiry
    {
      get { return BusinessObject.IsNull(Details.CoDriver) ? null : (DateTime?)Details.CoDriver.PassportExpiryDate; }
    }

    public string Vehicles
    {
      get { return string.Join(", ", Details.Vehicles.Select(v => v.RegistrationNumber)); }
    }
  }
}

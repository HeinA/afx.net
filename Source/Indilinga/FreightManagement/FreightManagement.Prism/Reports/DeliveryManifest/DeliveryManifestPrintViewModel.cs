﻿using Afx.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Reports.DeliveryManifest
{
  public class DeliveryManifestPrintViewModel
  {
    public DeliveryManifestPrintViewModel(FreightManagement.Business.DeliveryManifest details)
    {
      Details = details;
    }

    #region DeliveryManifest Details

    FreightManagement.Business.DeliveryManifest mDetails;
    FreightManagement.Business.DeliveryManifest Details
    {
      get { return mDetails; }
      set { mDetails = value; }
    }

    #endregion

    public string RouteOrigin
    {
      get
      {
        if (BusinessObject.IsNull(Details.Route)) return string.Empty;
        return Details.Route.Origin.Name;
      }
    }

    public string RouteDestination
    {
      get
      {
        if (BusinessObject.IsNull(Details.Route)) return string.Empty;
        return Details.Route.Destination.Name;
      }
    }

    public int Quantity
    {
      get { return Details.Items; }
    }

    public decimal Weight
    {
      get { return Details.Weight; }
    }

    public int Count
    {
      get { return Details.WaybillCount; }
    }

    public string Driver
    {
      get { return Details.Driver == null ? "" : Details.Driver.Fullname; }
    }

    public string Courier
    {
      get { return Details.Courier == null ? "" : Details.Courier.CompanyName; }
    }

    public string CoDriver
    {
      get { return BusinessObject.IsNull(Details.CoDriver) ? null : Details.CoDriver.Fullname; }
    }

    public string DriverPassport
    {
      get { return BusinessObject.IsNull(Details.Driver) ? null : Details.Driver.PassportNumber; }
    }

    public DateTime? DriverPassportExpiry
    {
      get { return BusinessObject.IsNull(Details.Driver) ? null : (DateTime?)Details.Driver.PassportExpiryDate; }
    }

    public string CoDriverPassport
    {
      get { return BusinessObject.IsNull(Details.CoDriver) ? null : Details.CoDriver.PassportNumber; }
    }

    public DateTime? CoDriverPassportExpiry
    {
      get { return BusinessObject.IsNull(Details.CoDriver) ? null : (DateTime?)Details.CoDriver.PassportExpiryDate; }
    }

    public string Vehicles
    {
      get { return string.Join(", ", Details.Vehicles.Select(v => string.Format("{0} ({1})", v.FleetNumber, v.RegistrationNumber))); }
    }
  }
}

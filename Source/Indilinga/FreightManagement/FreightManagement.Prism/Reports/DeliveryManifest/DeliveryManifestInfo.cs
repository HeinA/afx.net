﻿using Afx.Business.Documents;
using Afx.Prism;
using Afx.Prism.Documents;
using Afx.Prism.RibbonMdi.Documents;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Reports.DeliveryManifest
{
  [Export(FreightManagement.Business.DeliveryManifest.DocumentTypeIdentifier, typeof(IDocumentPrintInfo))]
  class DeliveryManifestInfo : IDocumentPrintInfo
  {
    public int Priority
    {
      get { return 90; }
    }

    public Stream ReportStream(object argument)
    {
      if (argument == null)
      {
        return this.GetType().Assembly.GetManifestResourceStream("FreightManagement.Prism.Reports.DeliveryManifest.DeliveryManifest.rdlc");
      }
      else if ((bool)argument)
      {
        return this.GetType().Assembly.GetManifestResourceStream("FreightManagement.Prism.Reports.DeliveryManifest.Tripsheet.rdlc");
      }

      return null;
    }

    public object GetScope(IController controller)
    {
      return null;
    }

    public void RefreshReportData(LocalReport report, Document document, object scope, object argument)
    {
      FreightManagement.Business.DeliveryManifest m = (FreightManagement.Business.DeliveryManifest)document;

      if (argument == null)
      {
        ReportDataSource reportDataSource = new ReportDataSource();
        reportDataSource.Name = "Document";
        reportDataSource.Value = new object[] { new DocumentPrintViewModel(document) };
        report.DataSources.Add(reportDataSource);

        reportDataSource = new ReportDataSource();
        reportDataSource.Name = "ManifestDetails";
        reportDataSource.Value = new object[] { new DeliveryManifestPrintViewModel(m) };
        report.DataSources.Add(reportDataSource);

        reportDataSource = new ReportDataSource();
        reportDataSource.Name = "ManifestWaybillItems";
        Collection<ItemPrintViewModel> items = new Collection<ItemPrintViewModel>();

        foreach (var mwb in m.Waybills)
        {
          items.Add(new ItemPrintViewModel(mwb));
        }

        reportDataSource.Value = items;
        report.DataSources.Add(reportDataSource);
      }
      else if ((bool)argument)
      {
        ReportDataSource reportDataSource = new ReportDataSource();
        reportDataSource.Name = "Document";
        reportDataSource.Value = new object[] { new DocumentPrintViewModel(document) };
        report.DataSources.Add(reportDataSource);

        reportDataSource = new ReportDataSource();
        reportDataSource.Name = "TripsheetDetails";
        reportDataSource.Value = new object[] { new DeliveryManifestPrintViewModel(m) };
        report.DataSources.Add(reportDataSource);
      }
    }

    public short Copies
    {
      get { return 1; }
    }
  }
}

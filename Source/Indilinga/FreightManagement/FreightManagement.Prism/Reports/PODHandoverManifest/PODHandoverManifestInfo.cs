﻿using Afx.Business.Documents;
using Afx.Prism;
using Afx.Prism.Documents;
using Afx.Prism.RibbonMdi.Documents;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Reports.PODHandoverManifest
{
  [Export(FreightManagement.Business.PODHandoverManifest.DocumentTypeIdentifier, typeof(IDocumentPrintInfo))]
  class PODHandoverManifestInfo : IDocumentPrintInfo
  {
    public int Priority
    {
      get { return 90; }
    }

    public Stream ReportStream(object argument)
    {
      return this.GetType().Assembly.GetManifestResourceStream("FreightManagement.Prism.Reports.PODHandoverManifest.PODHandoverManifest.rdlc"); 
    }

    public object GetScope(IController controller)
    {
      return null;
    }

    public void RefreshReportData(LocalReport report, Document document, object scope, object argument)
    {
      FreightManagement.Business.PODHandoverManifest m = (FreightManagement.Business.PODHandoverManifest)document;

      ReportDataSource reportDataSource = new ReportDataSource();
      reportDataSource.Name = "Document";
      reportDataSource.Value = new object[] { new DocumentPrintViewModel(document) };
      report.DataSources.Add(reportDataSource);

      reportDataSource = new ReportDataSource();
      reportDataSource.Name = "ManifestDetails";
      reportDataSource.Value = new object[] { new PODHandoverManifestPrintViewModel(m) };
      report.DataSources.Add(reportDataSource);

      reportDataSource = new ReportDataSource();
      reportDataSource.Name = "ManifestPODHandoverItems";
      Collection<ItemPrintViewModel> items = new Collection<ItemPrintViewModel>();

      foreach (var mwb in m.PODHandovers)
      {
        items.Add(new ItemPrintViewModel(mwb));
      }

      reportDataSource.Value = items;
      report.DataSources.Add(reportDataSource);

    }

    public short Copies
    {
      get { return 1; }
    }
  }
}

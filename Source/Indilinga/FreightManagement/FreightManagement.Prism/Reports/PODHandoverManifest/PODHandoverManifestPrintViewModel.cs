﻿using Afx.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Reports.PODHandoverManifest
{
  public class PODHandoverManifestPrintViewModel
  {
    public PODHandoverManifestPrintViewModel(FreightManagement.Business.PODHandoverManifest details)
    {
      Details = details;
    }

    #region PODHandoverManifest Details

    FreightManagement.Business.PODHandoverManifest mDetails;
    FreightManagement.Business.PODHandoverManifest Details
    {
      get { return mDetails; }
      set { mDetails = value; }
    }

    #endregion

    public int Count
    {
      get { return Details.PODHandoverCount; }
    }

    public string Driver
    {
      get { return Details.Driver == null ? string.Empty : Details.Driver.Fullname; }
    }

    public string Courier
    {
      get { return Details.Courier == null ? string.Empty : Details.Courier.CompanyName; }
    }
  }
}

﻿using Afx.Business;
using FreightManagement.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Reports.PODHandoverManifest
{
  public class ItemPrintViewModel
  {
    public ItemPrintViewModel(Business.PODHandoverReference item)
    {
      Item = item;
    }

    public string PODHandoverNumber
    {
      get { return Item.PODHandoverNumber; }
    }

    #region PODHandoverReference Item

    Business.PODHandoverReference mItem;
    Business.PODHandoverReference Item
    {
      get { return mItem; }
      set { mItem = value; }
    }

    #endregion

    public DateTime PODHandoverDate
    {
      get { return Item.PODHandoverDate; }
    }

    public int PODHandoverWaybillCount
    {
      get { return Item.WaybillCount; }
    }

    #region "Terms & notes"

    public string Terms
    {
      get { return "Important: This shipment is acepted by us, are subject to our standard conditions of trade that are available on request."; }
    }

    public string Note
    {
      get { return "Note: No liability will be accepted unless arrangements have been made for liability cover prior to shipment of freight. To arrange cover, please contact CSD department for a liability reference number."; }
    }

    #endregion

    public string Account
    {
      get { return Item.Account == null ? string.Empty : Item.Account.Name; }
    }

  }
}

﻿using Afx.Business;
using Afx.Business.Activities;
using Afx.Business.Collections;
using Afx.Business.Documents;
using Afx.Prism;
using Afx.Prism.Activities;
using Afx.Prism.Documents;
using Afx.Prism.RibbonMdi.Documents;
using FreightManagement.Business;
using FreightManagement.Prism.Activities.FloorStatusManagement;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Reports.FloorStatus
{
  [Export("{1c9f46d9-95ee-41d9-8d47-bfe75b5f34ce}", typeof(IActivityPrintInfo))]
  class FloorStatusInfo : IActivityPrintInfo
  {
    public int Priority
    {
      get { return 90; }
    }

    public Stream ReportStream(object argument)
    {
      return this.GetType().Assembly.GetManifestResourceStream("FreightManagement.Prism.Reports.FloorStatus.FloorStatus.rdlc"); 
    }

    public object GetScope(IController controller)
    {
      return null;
    }

    //public void RefreshReportData(LocalReport report, Activity activity, object scope)
    public void RefreshReportData(LocalReport report, BusinessObject activityContext, object scope, object argument)
    {
      FloorStatusManagement fs = (FloorStatusManagement)activityContext;

      ReportDataSource reportDataSource = new ReportDataSource();
      reportDataSource.Name = "Activity";
      reportDataSource.Value = new object[] { new ActivityPrintViewModel(fs.Activity) };
      report.DataSources.Add(reportDataSource);

      reportDataSource = new ReportDataSource();
      reportDataSource.Name = "FloorStatus";
      reportDataSource.Value = new object[] { new FloorStatusPrintViewModel() { DistributionCenterName = fs.DistributionCenter.Name } };
      report.DataSources.Add(reportDataSource);

      reportDataSource = new ReportDataSource();
      reportDataSource.Name = "WaybillFloorStatusWaybillItems";
      Collection<ItemPrintViewModel> items = new Collection<ItemPrintViewModel>();
      foreach (var wb in fs.Waybills)
      {
        if (fs.SelectBy == FloorStatusManagementViewModel.SelectWithCheck && wb.IsChecked)
        {
          items.Add(new ItemPrintViewModel(wb));
        }

        if (fs.SelectBy == FloorStatusManagementViewModel.SelectWithVehicle && !BusinessObject.IsNull(wb.Vehicle))
        {
          items.Add(new ItemPrintViewModel(wb));
        }
      }

      reportDataSource.Value = items;
      report.DataSources.Add(reportDataSource);
    }

    public short Copies
    {
      get { return 1; }
    }
  }
}

﻿using Afx.Business;
using FreightManagement.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Reports.FloorStatus
{
  public class ItemPrintViewModel
  {
    public ItemPrintViewModel(Business.WaybillFloorStatus item)
    {
      Item = item;
    }

    #region WaybillFloorStatus Item

    Business.WaybillFloorStatus mItem;
    Business.WaybillFloorStatus Item
    {
      get { return mItem; }
      set { mItem = value; }
    }

    #endregion

    public string DistributionCenter
    {
      get { return Item.DistributionCenter.Name; }
    }

    public string WaybillNumber
    {
      get { return Item.WaybillNumber; }
    }

    public DateTime WaybillDate
    {
      get { return Item.WaybillDate; }
    }

    public string Account
    {
      get { return Item.Account == null ? "" : Item.Account.Name; }
    }

    #region "Collection Information"

    public string Consignee
    {
      get { return Item.Consignee; }
    }

    #endregion

    #region "Delivery Information"

    public string DeliveryAddress
    {
      get { return String.Format("{0}\r\n{1}", Item.Consignee, String.Empty); ; }
    }

    public string DeliveryCity
    {
      get
      {
        if (BusinessObject.IsNull(Item.DeliveryCity)) return string.Empty;
        return Item.DeliveryCity.City.Name;
      }
    }

    public DateTime DeliveryDate
    {
      get { return Item.DeliveryDate; }
    }

    public string Consigner
    {
      get
      {
        return Item.Consigner;
      }
    }

    #endregion

    public string WaybillRoute
    {
      get { return Item.Route == null ? "" : Item.Route.Name; }
    }

    public string Vehicle
    {
      get { return Item.Vehicle == null ? "" : Item.Vehicle.Text; }
    }


    #region "Dimensions and Weights"

    public decimal WaybillPhysicalWeight
    {
      get { return Item.OnFloor; }
    }

    public decimal WaybillVolume
    {
      get { return Item.Volume; }
    }

    public int WaybillTotalItems
    {
      get { return Item.Items; }
    }

    #endregion
  }
}

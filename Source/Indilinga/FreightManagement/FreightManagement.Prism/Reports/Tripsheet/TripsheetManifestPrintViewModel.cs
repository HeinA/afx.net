﻿using Afx.Business;
using FreightManagement.Business;
using FreightManagement.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Reports.Tripsheet
{
  public class TripsheetManifestPrintViewModel
  {
    public TripsheetManifestPrintViewModel(ManifestReference details)
    {
      Details = details;
    }

    #region ManifestReference Details

    ManifestReference mDetails;
    ManifestReference Details
    {
      get { return mDetails; }
      set { mDetails = value; }
    }

    #endregion

    public string RouteName
    {
      get { return Details.Route.Name; }
    }

    public string RouteOrigin
    {
      get
      {
        if (BusinessObject.IsNull(Details.Route)) return string.Empty;
        return Details.Route.Origin.FullName;
      }
    }

    public string RouteDestination
    {
      get
      {
        if (BusinessObject.IsNull(Details.Route)) return string.Empty;
        return Details.Route.Destination.FullName;
      }
    }

    public string DocumentNumber
    {
      get { return Details.ManifestNumber; }
    }

    public int Quantity
    {
      get { return Details.Items; }
    }

    public decimal Weight
    {
      get { return Details.Weight; }
    }

    public int WaybillCount
    {
      get { return Details.WaybillCount; }
    }

  }
}

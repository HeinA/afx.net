﻿using Afx.Business;
using System;
using System.Linq;

namespace FreightManagement.Prism.Reports.Tripsheet
{
  public class TripsheetPrintViewModel
  {
    public TripsheetPrintViewModel(FreightManagement.Business.Tripsheet details)
    {
      Details = details;
    }

    #region Tripsheet Details

    FreightManagement.Business.Tripsheet mDetails;
    FreightManagement.Business.Tripsheet Details
    {
      get { return mDetails; }
      set { mDetails = value; }
    }

    #endregion


    public string Driver
    {
      get { return BusinessObject.IsNull(Details.Driver) ? null : Details.Driver.Fullname; }
    }

    public string CoDriver
    {
      get { return BusinessObject.IsNull(Details.CoDriver) ? null : Details.CoDriver.Fullname; }
    }

    public string Contractor
    {
      get { return BusinessObject.IsNull(Details.Contractor) ? null : Details.Contractor.CompanyName; }
    }

    public string DriverPassport
    {
      get { return BusinessObject.IsNull(Details.Driver) ? null : Details.Driver.PassportNumber; }
    }

    public DateTime? DriverPassportExpiry
    {
      get { return BusinessObject.IsNull(Details.Driver) ? null : (DateTime?)Details.Driver.PassportExpiryDate; }
    }

    public string CoDriverPassport
    {
      get { return BusinessObject.IsNull(Details.CoDriver) ? null : Details.CoDriver.PassportNumber; }
    }

    public DateTime? CoDriverPassportExpiry
    {
      get { return BusinessObject.IsNull(Details.CoDriver) ? null : (DateTime?)Details.CoDriver.PassportExpiryDate; }
    }

    public string Vehicles
    {
      get { return String.Join(", ", Details.Vehicles.Select(v => v.RegistrationNumber)); }
    }
  }
}

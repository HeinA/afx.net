﻿using Afx.Business.Documents;
using Afx.Prism;
using Afx.Prism.Documents;
using Afx.Prism.RibbonMdi.Documents;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Reports.PODHandover
{
  [Export(FreightManagement.Business.PODHandover.DocumentTypeIdentifier, typeof(IDocumentPrintInfo))]
  class PODHandoverInfo : IDocumentPrintInfo
  {
    public int Priority
    {
      get { return 90; }
    }

    public Stream ReportStream(object argument)
    {
      return this.GetType().Assembly.GetManifestResourceStream("FreightManagement.Prism.Reports.PODHandover.PODHandover.rdlc"); 
    }

    public object GetScope(IController controller)
    {
      return null;
    }

    public void RefreshReportData(LocalReport report, Document document, object scope, object argument)
    {
      FreightManagement.Business.PODHandover pod = (FreightManagement.Business.PODHandover)document;

      ReportDataSource reportDataSource = new ReportDataSource();
      reportDataSource.Name = "Document";
      reportDataSource.Value = new object[] { new DocumentPrintViewModel(document) };
      report.DataSources.Add(reportDataSource);

      reportDataSource = new ReportDataSource();
      reportDataSource.Name = "PODHandoverDetails";
      reportDataSource.Value = new object[] { new PODHandoverPrintViewModel(pod) };
      report.DataSources.Add(reportDataSource);

      reportDataSource = new ReportDataSource();
      reportDataSource.Name = "PODHandoverWaybillItems";
      Collection<ItemPrintViewModel> items = new Collection<ItemPrintViewModel>();

      foreach (var podwb in pod.Waybills)
      {
        items.Add(new ItemPrintViewModel(podwb));
      }

      reportDataSource.Value = items;
      report.DataSources.Add(reportDataSource);

    }

    public short Copies
    {
      get { return 1; }
    }
  }
}

﻿using Afx.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Reports.PODHandover
{
  public class ItemPrintViewModel
  {
    public ItemPrintViewModel(Business.WaybillReference item)
    {
      Item = item;
    }

    public string WaybillNumber
    {
      get { return Item.WaybillNumber; }
    }

    public DateTime WaybillDate
    {
      get { return Item.WaybillDate; }
    }

    public string WaybillReference
    {
      get { return Item.AccountingOrderNumber; }
    }

    public string Consigner
    {
      get { return Item.Consigner; }
    }

    public string Consignee
    {
      get { return Item.Consignee; }
    }

    public string DeliveryCity
    {
      get { return Item.DeliveryCity.City.Name; }
    }

    public int Quantity
    {
      get { return Item.Items; }
    }

    public decimal BillingWeight
    {
      get { return Item.PhysicalWeight; }
    }

    #region WaybillReference Item

    Business.WaybillReference mItem;
    Business.WaybillReference Item
    {
      get { return mItem; }
      set { mItem = value; }
    }

    #endregion

    public string PODSignature
    {
      get { return Item.PODSignature; }
    }

    public DateTime? PODDate
    {
      get { return Item.PODDate; }
    }

    public string InvoiceNumber
    {
      get { return Item.InvoiceNumber; }
    }

  }
}

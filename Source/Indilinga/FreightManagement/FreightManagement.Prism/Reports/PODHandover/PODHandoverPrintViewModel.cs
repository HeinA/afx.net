﻿using AccountManagement.Business;
using Afx.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Reports.PODHandover
{
  public class PODHandoverPrintViewModel
  {
    public PODHandoverPrintViewModel(FreightManagement.Business.PODHandover details)
    {
      Details = details;
    }

    #region Waybill Details

    FreightManagement.Business.PODHandover mDetails;
    FreightManagement.Business.PODHandover Details
    {
      get { return mDetails; }
      set { mDetails = value; }
    }

    #endregion

    public string AccountName
    {
      get
      {
        if (BusinessObject.IsNull(Details.Account)) return string.Empty;
        return Details.Account.Name;
      }
    }

    public string PODhandoverDeliveryAddress
    {
      get
      {
        if (BusinessObject.IsNull(Details.Account)) return string.Empty;

        return Details.Account.PhysicalAddress == null ? String.Empty : Details.Account.PhysicalAddress.AddressTextSingleLine;
      }
    }

    public int Count
    {
      get { return Details.WaybillCount; }
    }
  }
}

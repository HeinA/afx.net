﻿using Afx.Business;
using FreightManagement.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Reports.WaybillReceipt
{
  public class WaybillPrintViewModel
  {
    public WaybillPrintViewModel(FreightManagement.Business.WaybillReference item)
    {
      Item = item;
    }

    #region WaybillItem Item

    FreightManagement.Business.WaybillReference mItem;
    FreightManagement.Business.WaybillReference Item
    {
      get { return mItem; }
      set { mItem = value; }
    }

    #endregion

    public string WaybillNumber
    {
      get { return Item.WaybillNumber; }
    }

    public DateTime WaybillDate
    {
      get { return Item.WaybillDate; }
    }

    public string WaybillState
    {
      get { return Item.WaybillState.Name; }
    }



    #region string Account

    public string Account
    {
      get { return (Item.Account != null ? Item.Account.Name : ""); }
    }

    #endregion

    #region string InvoiceTo

    public string InvoiceTo
    {
      get { return Item.InvoiceTo; }
    }

    #endregion

    #region string InvoiceNumber

    public string InvoiceNumber
    {
      get { return Item.InvoiceNumber; }
    }

    #endregion




    public string WaybillCurrentDistributionCenter
    {
      get { return Item.LastDistributionCenter.Name; }
    }


    #region "Collection Information"
    
    public string Consigner
    {
      get { return Item.Consigner; }
    }

    public string CollectionCity
    {
      get
      {
        if (BusinessObject.IsNull(Item.CollectionCity)) return string.Empty;
        return Item.CollectionCity.Name;
      }
    }

    public string CollectionLocation
    {
      get
      {
        if (BusinessObject.IsNull(Item.FullCollectionLocation)) return string.Empty;
        return Item.FullCollectionLocation.FullName;
      }
    }

    public string CollectionSuburb
    {
      get
      {
        if (BusinessObject.IsNull(Item.CollectionSuburb)) return string.Empty;
        return Item.CollectionSuburb.FullName;
      }
    }

    public string CollectionFull
    {
      get
      {
        if (BusinessObject.IsNull(Item.FullCollectionLocation)) return string.Empty;
        return Item.FullCollectionLocation.FullName;
      }
    }

    #endregion

    #region "Delivery Information"

    public string Consignee
    {
      get { return Item.Consignee; }
    }

    public string DeliveryCity
    {
      get
      {
        if (BusinessObject.IsNull(Item.DeliveryCity)) return string.Empty;
        return Item.DeliveryCity.Name;
      }
    }

    public DateTime DeliveryDate
    {
      get { return Item.DeliveryDate; }
    }

    public string DeliveryLocation
    {
      get
      {
        if (BusinessObject.IsNull(Item.FullDeliveryLocation)) return string.Empty;
        return Item.FullDeliveryLocation.FullName;
      }
    }

    public string DeliverySuburb
    {
      get
      {
        if (BusinessObject.IsNull(Item.DeliverySuburb)) return string.Empty;
        return Item.DeliverySuburb.FullName;
      }
    }

    public string DeliveryFull
    {
      get
      {
        if (BusinessObject.IsNull(Item.FullDeliveryLocation)) return string.Empty;
        return Item.FullDeliveryLocation.FullName;
      }
    }

    #endregion



    public decimal PhysicalWeight
    {
      get { return Item.PhysicalWeight; }
    }

    public int Items
    {
      get { return Item.Items; }
    }

    public decimal Charges
    {
      get { return Item.Charge; }
    }

    public decimal DeclaredValue
    {
      get { return Item.DeclaredValue; }
    }
  }
}

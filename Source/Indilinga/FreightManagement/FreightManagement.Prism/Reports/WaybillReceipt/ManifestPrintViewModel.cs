﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Reports.WaybillReceipt
{
  public class ManifestPrintViewModel
  {
    public ManifestPrintViewModel(FreightManagement.Business.ManifestReference details)
    {
      Details = details;
    }

    #region ManifestReference Details

    FreightManagement.Business.ManifestReference mDetails;
    FreightManagement.Business.ManifestReference Details
    {
      get { return mDetails; }
      set { mDetails = value; }
    }

    #endregion
  }
}

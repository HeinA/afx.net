﻿using FreightManagement.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Reports.WaybillReceipt
{
  public class ItemPrintViewModel
  {
    public ItemPrintViewModel(ManifestReference item)
    {
      Item = item;
    }

    #region ManifestReference Item

    ManifestReference mItem;
    ManifestReference Item
    {
      get { return mItem; }
      set { mItem = value; }
    }

    #endregion

    public decimal ItemQuantity
    {
      get { return Item.Items; }
    }

    public decimal ItemPhysicalWeight
    {
      get { return Item.Weight; }
    }


  }
}

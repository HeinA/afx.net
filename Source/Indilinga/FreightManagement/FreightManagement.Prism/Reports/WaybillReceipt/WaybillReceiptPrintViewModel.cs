﻿using Afx.Business;
using FreightManagement.Business;
using Geographics.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Reports.WaybillReceipt
{
  public class WaybillReceiptPrintViewModel
  {
    public WaybillReceiptPrintViewModel(FreightManagement.Business.WaybillReceipt details)
    {
      Details = details;
    }

    #region WaybillReceipt Details

    FreightManagement.Business.WaybillReceipt mDetails;
    FreightManagement.Business.WaybillReceipt Details
    {
      get { return mDetails; }
      set { mDetails = value; }
    }

    #endregion



    public string WaybillReceiptCurrentDistributionCenter
    {
      get { return Details.OrganizationalUnit.Name; }
    }

    #region "Dimensions and Weights"

    public decimal PhysicalWeight
    {
      get { return Details.Waybills.Sum(w => w.PhysicalWeight); }
    }

    public int TotalItems
    {
      get { return Details.Waybills.Sum(w => w.Items); }
    }

    #endregion


  }
}

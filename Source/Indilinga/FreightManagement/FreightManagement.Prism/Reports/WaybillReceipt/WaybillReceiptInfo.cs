﻿using Afx.Business.Collections;
using Afx.Business.Documents;
using Afx.Business.Security;
using Afx.Business.Service;
using Afx.Prism;
using Afx.Prism.Documents;
using Afx.Prism.RibbonMdi.Documents;
using FreightManagement.Business;
using FreightManagement.Business.Service;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Reports.WaybillReceipt
{
  [Export(FreightManagement.Business.WaybillReceipt.DocumentTypeIdentifier, typeof(IDocumentPrintInfo))]
  class WaybillReceiptInfo : IDocumentPrintInfo
  {
    public int Priority
    {
      get { return 90; }
    }

    public Stream ReportStream(object argument)
    {
      return this.GetType().Assembly.GetManifestResourceStream("FreightManagement.Prism.Reports.WaybillReceipt.WaybillReceipt.rdlc"); 
    }

    public object GetScope(IController controller)
    {
      return null;
    }

    public void RefreshReportData(LocalReport report, Document document, object scope, object argument)
    {
      FreightManagement.Business.WaybillReceipt wbr = (FreightManagement.Business.WaybillReceipt)document;

      ReportDataSource reportDataSource = new ReportDataSource();
      reportDataSource.Name = "Document";
      reportDataSource.Value = new object[] { new DocumentPrintViewModel(document) };
      report.DataSources.Add(reportDataSource);

      reportDataSource = new ReportDataSource();
      reportDataSource.Name = "WaybillReceiptDetails";
      reportDataSource.Value = new object[] { new WaybillReceiptPrintViewModel(wbr) };
      report.DataSources.Add(reportDataSource);

      reportDataSource = new ReportDataSource();
      reportDataSource.Name = "Waybills";
      Collection<WaybillPrintViewModel> waybills = new Collection<WaybillPrintViewModel>();
      foreach (var wb in wbr.Waybills)
      {
        waybills.Add(new WaybillPrintViewModel(wb));
      }
      reportDataSource.Value = waybills;
      report.DataSources.Add(reportDataSource);


    }

    public short Copies
    {
      get { return 1; }
    }
  }
}

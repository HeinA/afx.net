﻿using Afx.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Reports.TransportManifest
{
  public class TransportManifestPrintViewModel
  {
    public TransportManifestPrintViewModel(FreightManagement.Business.TransportManifest details)
    {
      Details = details;
    }

    #region TransportManifest Details

    FreightManagement.Business.TransportManifest mDetails;
    FreightManagement.Business.TransportManifest Details
    {
      get { return mDetails; }
      set { mDetails = value; }
    }

    #endregion

    /// <summary>
    /// Origin DC location.
    /// </summary>
    public string RouteOrigin
    {
      get
      {
        if (BusinessObject.IsNull(Details.Origin)) return string.Empty;
        return Details.Origin.City.Name;
      }
    }

    /// <summary>
    /// Destination DC location
    /// </summary>
    public string RouteDestination
    {
      get
      {
        if (BusinessObject.IsNull(Details.Destination)) return string.Empty;
        return Details.Destination.City.Name;
      }
    }

    public int Quantity
    {
      get { return Details.Items; }
    }

    public decimal Weight
    {
      get { return Details.Weight; }
    }

    public int Count
    {
      get { return Details.WaybillCount; }
    }
  }
}

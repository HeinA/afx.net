﻿using Afx.Business;
using FreightManagement.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Reports.TransportManifest
{
  public class ItemPrintViewModel
  {
    public ItemPrintViewModel(Business.WaybillReference waybill, WaybillReferenceCargoItem item)
    {
      Waybill = waybill;
      Item = item;
    }

    public string WaybillNumber
    {
      get { return Waybill.WaybillNumber; }
    }

    Business.WaybillReference Waybill { get; set; }
    WaybillReferenceCargoItem Item { get; set; }

    public string Account
    {
      get { return Waybill.Account.Name; }
    }

    public string CollectionAddress
    {
      get { return String.Format("{0}\r\n{1}", Waybill.Consigner, String.Empty); }
    }

    public string CollectionCity
    {
      get
      {
        if (BusinessObject.IsNull(Waybill.CollectionCity)) return string.Empty;
        return Waybill.CollectionCity.City.Name;
      }
    }

    public string CollectionSuburb
    {
      get
      {
        if (BusinessObject.IsNull(Waybill.CollectionSuburb)) return string.Empty;
        return Waybill.CollectionSuburb.Name;
      }
    }

    public string CollectionFull
    {
      get
      {
        if (BusinessObject.IsNull(Waybill.FullCollectionLocation)) return string.Empty;
        return Waybill.FullCollectionLocation.Name;
      }
    }

    public string Consignee
    {
      get
      {
        return Waybill.Consignee;
      }
    }

    public string DeliveryAddress
    {
      get { return String.Format("{0}\r\n{1}", Waybill.Consignee, String.Empty); ; }
    }

    public string DeliveryCity
    {
      get
      {
        if (BusinessObject.IsNull(Waybill.DeliveryCity)) return string.Empty;
        return Waybill.DeliveryCity.City.Name;
      }
    }

    public DateTime DeliveryDate
    {
      get { return Waybill.DeliveryDate; }
    }

    public string DeliverySuburb
    {
      get
      {
        if (BusinessObject.IsNull(Waybill.DeliverySuburb)) return string.Empty;
        return Waybill.DeliverySuburb.Name;
      }
    }

    public string DeliveryFull
    {
      get
      {
        if (BusinessObject.IsNull(Waybill.FullDeliveryLocation)) return string.Empty;
        return Waybill.FullDeliveryLocation.Name;
      }
    }

    public string Consigner
    {
      get
      {
        return Waybill.Consigner;
      }
    }


    public decimal PhysicalWeight
    {
      get { return Item.PhysicalWeight; }
    }

    public decimal Volume
    {
      get { return Item.VolumetricWeight; }
    }

    public int TotalItems
    {
      get { return Item.Items; }
    }

    public string Description
    {
      get { return Item.Description; }
    }

    public string Terms
    {
      get { return "Important: This shipment is acepted by us, are subject to our standard conditions of trade that are available on request."; }
    }

    public string Note
    {
      get { return "Note: No liability will be accepted unless arrangements have been made for liability cover prior to shipment of freight. To arrange cover, please contact CSD department for a liability reference number."; }
    }

  }
}

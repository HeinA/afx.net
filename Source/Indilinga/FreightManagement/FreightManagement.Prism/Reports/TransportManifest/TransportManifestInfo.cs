﻿using Afx.Business.Documents;
using Afx.Prism;
using Afx.Prism.Documents;
using Afx.Prism.RibbonMdi.Documents;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Reports.TransportManifest
{
  [Export(FreightManagement.Business.TransportManifest.DocumentTypeIdentifier, typeof(IDocumentPrintInfo))]
  class TransportManifestInfo : IDocumentPrintInfo
  {
    public int Priority
    {
      get { return 90; }
    }

    public Stream ReportStream(object argument)
    {
      return this.GetType().Assembly.GetManifestResourceStream("FreightManagement.Prism.Reports.TransportManifest.TransportManifest.rdlc"); 
    }

    public object GetScope(IController controller)
    {
      return null;
    }

    public void RefreshReportData(LocalReport report, Document document, object scope, object argument)
    {
      FreightManagement.Business.TransportManifest m = (FreightManagement.Business.TransportManifest)document;

      ReportDataSource reportDataSource = new ReportDataSource();
      reportDataSource.Name = "Document";
      reportDataSource.Value = new object[] { new DocumentPrintViewModel(document) };
      report.DataSources.Add(reportDataSource);

      reportDataSource = new ReportDataSource();
      reportDataSource.Name = "TransportManifestDetails";
      reportDataSource.Value = new object[] { new TransportManifestPrintViewModel(m) };
      report.DataSources.Add(reportDataSource);

      reportDataSource = new ReportDataSource();
      reportDataSource.Name = "TransportManifestWaybillItems";
      Collection<ItemPrintViewModel> items = new Collection<ItemPrintViewModel>();

      foreach (var mwb in m.Waybills)
      {
        foreach (var ci in mwb.CargoItems)
        {
          items.Add(new ItemPrintViewModel(mwb, ci));
        }
      }

      reportDataSource.Value = items;
      report.DataSources.Add(reportDataSource);

    }

    public short Copies
    {
      get { return 1; }
    }
  }
}

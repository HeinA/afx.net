﻿using Afx.Business;
using FreightManagement.Business;
using Geographics.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Reports.Waybill
{
  public class WaybillPrintViewModel
  {
    public WaybillPrintViewModel(FreightManagement.Business.Waybill details)
    {
      Details = details;

      References = string.Join(", ", details.References.Select(r1 => r1.Reference));
    }

    #region Waybill Details

    FreightManagement.Business.Waybill mDetails;
    FreightManagement.Business.Waybill Details
    {
      get { return mDetails; }
      set { mDetails = value; }
    }

    #endregion


    #region string InvoiceTo

    public string InvoiceTo
    {
      get { return Details.InvoiceTo; }
    }

    #endregion

    #region string ClientReferenceNumber

    public string ClientReferenceNumber
    {
      get { return Details.ClientReferenceNumber; }
    }

    #endregion

    #region string InvoiceNumber

    public string InvoiceNumber
    {
      get { return Details.InvoiceNumber; }
    }

    #endregion

    public string References { get; set; }

    #region "Collection Information"

    public string CollectionAddress
    {
      get { return String.Format("{0}\r\n{1}", Details.Consigner, Details.CollectionAddress); }
    }

    public string CollectionCity
    {
      get
      {
        if (BusinessObject.IsNull(Details.CollectionCity)) return string.Empty;
        return Details.CollectionCity.FullName; 
      }
    }

    public DateTime CollectionDate
    {
      get { return Details.CollectionDate; }
    }

    public string CollectionLocation
    {
      get
      {
        if (BusinessObject.IsNull(Details.CollectionLocation)) return string.Empty; 
        return Details.CollectionLocation.FullName;
      }
    }

    public string CollectionSuburb
    {
      get
      {
        if (BusinessObject.IsNull(Details.CollectionSuburb)) return string.Empty; 
        return Details.CollectionSuburb.FullName;
      }
    }

    public string CollectionFull
    {
      get
      {
        if (BusinessObject.IsNull(Details.FullCollectionLocation)) return string.Empty;
        return Details.FullCollectionLocation.FullName;
      }
    }

    #endregion

    #region "Delivery Information"

    public string DeliveryAddress
    {
      get { return String.Format("{0}\r\n{1}", Details.Consignee, Details.DeliveryAddress); ; }
    }

    public string DeliveryCity
    {
      get
      {
        if (BusinessObject.IsNull(Details.DeliveryCity)) return string.Empty;
        return Details.DeliveryCity.FullName;
      }
    }

    public DateTime DeliveryDate
    {
      get { return Details.DeliveryDate; }
    }

    public string DeliveryLocation
    {
      get
      {
        if (BusinessObject.IsNull(Details.DeliveryLocation)) return string.Empty; 
        return Details.DeliveryLocation.FullName;
      }
    }

    public string DeliverySuburb
    {
      get
      {
        if (BusinessObject.IsNull(Details.DeliverySuburb)) return string.Empty;
        return Details.DeliverySuburb.FullName;
      }
    }

    public string DeliveryFull
    {
      get
      {
        if (BusinessObject.IsNull(Details.FullDeliveryLocation)) return string.Empty;
        return Details.FullDeliveryLocation.FullName;
      }
    }

    #endregion


    #region "Dimensions and Weights"

    public decimal PhysicalWeight
    {
      get { return Details.PhysicalWeight; }
    }

    public int TotalItems
    {
      get { return Details.TotalItems; }
    }

    public int VolumetricFactor
    {
      get { return Details.VolumetricFactor; }
    }

    #endregion


    #region Terms & notes

    public string Terms
    {
      get
      {
        return Setting.GetSetting<WaybillSetting>().WaybillTerms;
        //return "Important: This shipment is accepted by us, are subject to our standard conditions of trade that are available on request."; 
      }
    }

    public string Note
    {
      get 
      {
        return Setting.GetSetting<WaybillSetting>().WaybillNotes;
        //return "Note: No liability will be accepted unless arrangements have been made for liability cover prior to shipment of freight. To arrange cover, please contact CSD department for a liability reference number."; 
      }
    }

    #endregion


    public string Account
    {
      get { return Details.Account.Name; }
    }
  }
}

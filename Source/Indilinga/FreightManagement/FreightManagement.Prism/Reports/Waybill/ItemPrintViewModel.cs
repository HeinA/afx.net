﻿using FreightManagement.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Reports.Waybill
{
  public class ItemPrintViewModel
  {
    public ItemPrintViewModel(WaybillCargoItem item)
    {
      Item = item;
    }

    #region WaybillCargoItem Item

    WaybillCargoItem mItem;
    WaybillCargoItem Item
    {
      get { return mItem; }
      set { mItem = value; }
    }

    #endregion

    public decimal ItemQuantity
    {
      get { return Item.Items; }
    }

    public string ItemDescription
    {
      get { return Item.Description; }
    }

    public decimal ItemWeight
    {
      get { return (ItemVolumetricWeight > ItemPhysicalWeight ? ItemVolumetricWeight : ItemPhysicalWeight); }
    }

    public decimal ItemVolumetricWeight
    {
      get { return Item.VolumetricWeight; }
    }

    public decimal ItemPhysicalWeight
    {
      get { return Item.PhysicalWeight; }
    }

    public decimal ItemDeclaredValue
    {
      get { return Item.DeclaredValue; }
    }

    public string ItemCargoType
    {
      get { return Item.CargoType.Text; }
    }


  }
}

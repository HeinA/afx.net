﻿using Afx.Business.Documents;
using Afx.Prism;
using Afx.Prism.Documents;
using Afx.Prism.RibbonMdi.Documents;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Reports.Waybill
{
  [Export(FreightManagement.Business.Waybill.DocumentTypeIdentifier, typeof(IDocumentPrintInfo))]
  class WaybillInfo : IDocumentPrintInfo
  {
    public int Priority
    {
      get { return 90; }
    }

    public Stream ReportStream(object argument)
    {
      return this.GetType().Assembly.GetManifestResourceStream("FreightManagement.Prism.Reports.Waybill.Waybill.rdlc"); 
    }

    public object GetScope(IController controller)
    {
      return null;
    }

    public void RefreshReportData(LocalReport report, Document document, object scope, object argument)
    {
      FreightManagement.Business.Waybill wb = (FreightManagement.Business.Waybill)document;

      ReportDataSource reportDataSource = new ReportDataSource();
      reportDataSource.Name = "Document";
      reportDataSource.Value = new object[] { new DocumentPrintViewModel(document) };
      report.DataSources.Add(reportDataSource);

      reportDataSource = new ReportDataSource();
      reportDataSource.Name = "WaybillDetails";
      reportDataSource.Value = new object[] { new WaybillPrintViewModel(wb) };
      report.DataSources.Add(reportDataSource);

      reportDataSource = new ReportDataSource();
      reportDataSource.Name = "CargoItems";
      Collection<ItemPrintViewModel> items = new Collection<ItemPrintViewModel>();

      foreach (var wbi in wb.Items)
      {
        items.Add(new ItemPrintViewModel(wbi));
      }

      reportDataSource.Value = items;
      report.DataSources.Add(reportDataSource);

      reportDataSource = new ReportDataSource();
      reportDataSource.Name = "WaybillCharges";
      Collection<WaybillChargePrintViewModel> charges = new Collection<WaybillChargePrintViewModel>();
      foreach(var wbc in wb.Charges)
      {
        charges.Add(new WaybillChargePrintViewModel(wbc));
      }

      reportDataSource.Value = charges;
      report.DataSources.Add(reportDataSource);

    }

    public short Copies
    {
      get { return 1; }
    }
  }
}

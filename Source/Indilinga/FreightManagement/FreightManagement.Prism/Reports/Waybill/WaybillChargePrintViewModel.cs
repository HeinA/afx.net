﻿using FreightManagement.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Reports.Waybill
{
  public class WaybillChargePrintViewModel
  {
    public WaybillChargePrintViewModel(WaybillCharge item)
    {
      Item = item;
    }

    #region WaybillCargoItem Item

    WaybillCharge mItem;
    WaybillCharge Item
    {
      get { return mItem; }
      set { mItem = value; }
    }

    #endregion

    public string ChargeName
    {
      get { return Item.ChargeType.Text; }
    }

    public decimal ChargeValue
    {
      get { return Item.Charge; }
    }
  }
}

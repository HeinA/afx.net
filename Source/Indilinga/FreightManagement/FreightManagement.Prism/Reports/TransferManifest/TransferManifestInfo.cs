﻿using Afx.Business.Documents;
using Afx.Prism;
using Afx.Prism.Documents;
using Afx.Prism.RibbonMdi.Documents;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Reports.TransferManifest
{
  [Export(FreightManagement.Business.TransferManifest.DocumentTypeIdentifier, typeof(IDocumentPrintInfo))]
  class TransferManifestInfo : IDocumentPrintInfo
  {
    public const string PrintDetail = "Detail";
    public const string PrintSummary = "Summary";
    public const string PrintRestrictedSummary = "RestrictedSummary";
    public const string PrintTripsheet = "Tripsheet";

    public int Priority
    {
      get { return 90; }
    }

    public Stream ReportStream(object argument)
    {
      if (argument == null || ((string)argument) == PrintDetail)
      {
        return this.GetType().Assembly.GetManifestResourceStream("FreightManagement.Prism.Reports.TransferManifest.TransferManifest.rdlc");
      }
      else if (((string)argument) == PrintSummary)
      {
        return this.GetType().Assembly.GetManifestResourceStream("FreightManagement.Prism.Reports.TransferManifest.TransferManifestSummary.rdlc");
      }
      else if (((string)argument) == PrintRestrictedSummary)
      {
        return this.GetType().Assembly.GetManifestResourceStream("FreightManagement.Prism.Reports.TransferManifest.TransferManifestSummaryRestricted.rdlc");
      }
      else if (((string)argument) == PrintTripsheet)
      {
        return this.GetType().Assembly.GetManifestResourceStream("FreightManagement.Prism.Reports.DeliveryManifest.Tripsheet.rdlc");
      }

      return null;
    }

    public object GetScope(IController controller)
    {
      return null;
    }

    public void RefreshReportData(LocalReport report, Document document, object scope, object argument)
    {
      FreightManagement.Business.TransferManifest m = (FreightManagement.Business.TransferManifest)document;

      if (argument == null || ((string)argument) == PrintDetail)
      {
        ReportDataSource reportDataSource = new ReportDataSource();
        reportDataSource.Name = "Document";
        reportDataSource.Value = new object[] { new DocumentPrintViewModel(document) };
        report.DataSources.Add(reportDataSource);

        reportDataSource = new ReportDataSource();
        reportDataSource.Name = "TransferManifestDetails";
        reportDataSource.Value = new object[] { new TransferManifestPrintViewModel(m) };
        report.DataSources.Add(reportDataSource);

        reportDataSource = new ReportDataSource();
        reportDataSource.Name = "TransferManifestWaybillItems";
        Collection<ItemPrintViewModel> items = new Collection<ItemPrintViewModel>();

        foreach (var mwb in m.Waybills)
        {
          foreach (var ci in mwb.CargoItems)
          {
            items.Add(new ItemPrintViewModel(mwb, ci));
          }
        }

        reportDataSource.Value = items;
        report.DataSources.Add(reportDataSource);
      }
      else if (((string)argument) == PrintSummary || ((string)argument) == PrintRestrictedSummary)
      {
        ReportDataSource reportDataSource = new ReportDataSource();
        reportDataSource.Name = "Document";
        reportDataSource.Value = new object[] { new DocumentPrintViewModel(document) };
        report.DataSources.Add(reportDataSource);

        reportDataSource = new ReportDataSource();
        reportDataSource.Name = "TransferManifestDetails";
        reportDataSource.Value = new object[] { new TransferManifestPrintViewModel(m) };
        report.DataSources.Add(reportDataSource);

        reportDataSource = new ReportDataSource();
        reportDataSource.Name = "TransferManifestWaybillItems";
        Collection<ItemPrintViewModel> items = new Collection<ItemPrintViewModel>();

        foreach (var mwb in m.Waybills)
        {
          items.Add(new ItemPrintViewModel(mwb));
        }

        reportDataSource.Value = items;
        report.DataSources.Add(reportDataSource);
      }
      else if (((string)argument) == PrintTripsheet)
      {
        ReportDataSource reportDataSource = new ReportDataSource();
        reportDataSource.Name = "Document";
        reportDataSource.Value = new object[] { new DocumentPrintViewModel(document) };
        report.DataSources.Add(reportDataSource);

        reportDataSource = new ReportDataSource();
        reportDataSource.Name = "TripsheetDetails";
        reportDataSource.Value = new object[] { new TransferManifestPrintViewModel(m) };
        report.DataSources.Add(reportDataSource);
      }
    }

    public short Copies
    {
      get { return 1; }
    }
  }
}

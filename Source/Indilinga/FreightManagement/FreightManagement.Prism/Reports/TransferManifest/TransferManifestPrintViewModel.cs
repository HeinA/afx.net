﻿using Afx.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Reports.TransferManifest
{
  public class TransferManifestPrintViewModel
  {
    public TransferManifestPrintViewModel(FreightManagement.Business.TransferManifest details)
    {
      Details = details;
    }

    #region TransferManifest Details

    FreightManagement.Business.TransferManifest mDetails;
    FreightManagement.Business.TransferManifest Details
    {
      get { return mDetails; }
      set { mDetails = value; }
    }

    #endregion

    /// <summary>
    /// Origin DC location.
    /// </summary>
    public string RouteOrigin
    {
      get
      {
        if (BusinessObject.IsNull(Details.OriginDC)) return string.Empty;
        return Details.OriginDC.Location.Name;
      }
    }

    /// <summary>
    /// Destination DC location
    /// </summary>
    public string RouteDestination
    {
      get
      {
        if (BusinessObject.IsNull(Details.DestinationDC)) return string.Empty;
        return Details.DestinationDC.Location.Name;
      }
    }

    public int Quantity
    {
      get { return Details.Items; }
    }

    public int Waybills
    {
      get { return Details.Waybills.Count; }
    }

    public decimal Weight
    {
      get { return Details.Weight; }
    }

    public int Count
    {
      get { return Details.WaybillCount; }
    }

    public string Driver
    {
      get { return Details.Driver == null ? "" : Details.Driver.Fullname; }
    }

    public string Courier
    {
      get { return Details.Courier == null ? "" : Details.Courier.CompanyName; }
    }

    public string CoDriver
    {
      get { return BusinessObject.IsNull(Details.CoDriver) ? null : Details.CoDriver.Fullname; }
    }

    public string DriverPassport
    {
      get { return BusinessObject.IsNull(Details.Driver) ? null : Details.Driver.PassportNumber; }
    }

    public DateTime? DriverPassportExpiry
    {
      get { return BusinessObject.IsNull(Details.Driver) ? null : (DateTime?)Details.Driver.PassportExpiryDate; }
    }

    public string CoDriverPassport
    {
      get { return BusinessObject.IsNull(Details.CoDriver) ? null : Details.CoDriver.PassportNumber; }
    }

    public DateTime? CoDriverPassportExpiry
    {
      get { return BusinessObject.IsNull(Details.CoDriver) ? null : (DateTime?)Details.CoDriver.PassportExpiryDate; }
    }

    public string Vehicles
    {
      get { return string.Join(", ", Details.Vehicles.Select(v => string.Format("{0} ({1})", v.FleetNumber, v.RegistrationNumber))); }
    }

    public string SealNumber
    {
      get { return Details.SealNumber; }
    }
  }
}

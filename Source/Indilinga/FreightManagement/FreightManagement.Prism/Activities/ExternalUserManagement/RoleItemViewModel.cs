﻿using Afx.Prism;
using FreightManagement.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Activities.ExternalUserManagement
{
  public class RoleItemViewModel : SelectableViewModel<ExternalRole>
  {
    #region Constructors

    [InjectionConstructor]
    public RoleItemViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    #region RoleTabViewModel Parent

    public new RoleTabViewModel Parent
    {
      get { return (RoleTabViewModel)base.Parent; }
    }

    #endregion

    #region bool HasRole

    public bool HasRole
    {
      get { return Parent.Model.Roles.Contains(Model); }
      set
      {
        if (value) Parent.Model.Roles.Add(Model);
        else Parent.Model.Roles.Remove(Model);
        OnPropertyChanged("HasRole");
      }
    }

    #endregion
  }
}

﻿using Afx.Business;
using Afx.Business.Activities;
using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Activities;
using FreightManagement.Business;
using FreightManagement.Prism.Activities.ExternalUserManagement.SetPasswordDialog;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Activities.ExternalUserManagement
{
  public partial class ExternalUserManagementController : MdiContextualActivityController<ExternalUserManagement, ExternalUserManagementViewModel>
  {
    #region Constructors

    [InjectionConstructor]
    public ExternalUserManagementController(IController controller)
      : base(controller)
    {
    }

    #endregion

    #region OnLoaded
    
    protected override void OnLoaded()
    {
      if (DataContext.Data.Count > 0)
      {
        //Focus the first element 
        ExternalUserItemViewModel ivm = GetCreateViewModel<ExternalUserItemViewModel>(DataContext.Data[0], ViewModel);
        ivm.IsFocused = true;
      }

      RegisterContextRegion(RegionManager.Regions[ExternalUserManagement.DetailsRegion]);
      RegisterContextRegion(RegionManager.Regions[ExternalUserManagement.TabRegion]);

      base.OnLoaded();
    }

    #endregion
    
    #region OnContextViewModelChanged
    
    protected override void OnContextViewModelChanged(ISelectableViewModel context)
    {
      if (context.Model is ExternalUser)
      {
        ExternalUserDetailViewModel vm = GetCreateViewModel<ExternalUserDetailViewModel>(context.Model, ViewModel);
        AddDetailsViewModel(vm);

        AddTabViewModel(GetCreateViewModel<AccountTabViewModel>(context.Model, ViewModel));
        AddTabViewModel(GetCreateViewModel<RoleTabViewModel>(context.Model, ViewModel));
      }

      base.OnContextViewModelChanged(context);
    }

    #endregion
    
    #region ExecuteOperation
    
    protected override void ExecuteOperation(Operation op, BusinessObject argument)
    {
      switch (op.Identifier)
      {
        case Operations.AddUser:
          CreateRootItem();
          break;

        case Operations.SetPassword:
          var spc = CreateChildController<SetPasswordDialogController>();
          spc.ExternalUser = (ExternalUser)argument;
          spc.Run();
          break;
      }
      base.ExecuteOperation(op, argument);
    }
    
    #endregion
  }
}

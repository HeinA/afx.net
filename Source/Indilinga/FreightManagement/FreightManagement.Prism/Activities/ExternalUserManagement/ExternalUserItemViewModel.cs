﻿using Afx.Business;
using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using FreightManagement.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Activities.ExternalUserManagement
{
  public partial class ExternalUserItemViewModel : MdiNavigationListItemViewModel<ExternalUser>
  {
    #region Constructors

    [InjectionConstructor]
    public ExternalUserItemViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    #region string Username

    public string Username
    {
      get
      {
        if (Model == null) return string.Empty;
        if (string.IsNullOrWhiteSpace(Model.Username)) return "*** Unnamed ***";
        return Model.Username;
      }
    }

    #endregion
  }
}

﻿using AccountManagement.Business;
using Afx.Business;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Activities;
using Afx.Prism.RibbonMdi.Documents;
using FreightManagement.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace FreightManagement.Prism.Activities.ExternalUserManagement
{
  public class AccountTabViewModel : ActivityTabDetailViewModel<ExternalUser>
  {
    #region Constructors

    [InjectionConstructor]
    public AccountTabViewModel(IController controller)
      : base(controller)
    {
      Title = "_Accounts";
    }

    #endregion

    #region AccountReference Account

    public const string AccountProperty = "Account";
    AccountReference mAccount;
    public AccountReference Account
    {
      get { return mAccount; }
      set
      {
        if (SetProperty<AccountReference>(ref mAccount, value))
        {
          if (!BusinessObject.IsNull(value))
          {
            if (Model.Accounts.Contains(value))
            {
              Model.Accounts.Remove(value);
            }
            else
            {
              Model.Accounts.Add(value);
            }

            Dispatcher.CurrentDispatcher.BeginInvoke(new Action(() =>
            {
              Account = null;
            }), DispatcherPriority.Normal);
          }
        }
      }
    }

    #endregion
  }
}

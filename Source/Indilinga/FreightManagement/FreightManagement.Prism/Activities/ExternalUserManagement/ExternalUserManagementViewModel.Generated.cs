﻿using Afx.Business.Security;
using Afx.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Afx.Prism.RibbonMdi.Activities;
using FreightManagement.Business;
using Microsoft.Practices.Prism;

namespace FreightManagement.Prism.Activities.ExternalUserManagement
{
  //**************************************************************************************************************
  //**************************************************************************************************************
  //*                                                                                                            *
  //*     This code is auto-generated.  Do not place any cutom code here, it will be lost on re-generation.      *
  //*                                                                                                            *
  //**************************************************************************************************************
  //**************************************************************************************************************
  public partial class ExternalUserManagementViewModel
  {
    public IEnumerable<ExternalUserItemViewModel> Items
    {
      get
      {
        if (Model == null) return null;
        return this.ResolveViewModels<ExternalUserItemViewModel, ExternalUser>(Model.Data);
      }
    }
  }
}

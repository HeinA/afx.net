﻿using Afx.Prism;
using Afx.Prism.RibbonMdi;
using FreightManagement.Business;
using Microsoft.AspNet.Identity;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace FreightManagement.Prism.Activities.ExternalUserManagement.SetPasswordDialog
{
  public class SetPasswordDialogController : MdiDialogController<SetPasswordDialogContext, SetPasswordDialogViewModel>
  {
    public const string SetPasswordDialogControllerKey = "FreightManagement.Prism.Activities.ExternalUserManagement.SetPasswordDialog.SetPasswordDialogController";

    public SetPasswordDialogController(IController controller)
      : base(SetPasswordDialogControllerKey, controller)
    {
    }

    protected override bool OnRun()
    {
      if (DataContext == null) DataContext = new SetPasswordDialogContext();
      ViewModel.Caption = "SetPasswordDialog";
      return base.OnRun();
    }

    public ExternalUser ExternalUser { get; set; }

    protected override void ApplyChanges()
    {
      ExternalUser.PasswordHash = new PasswordHasher().HashPassword(ViewModel.Password);
      base.ApplyChanges();
    }
  }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Activities.ExternalUserManagement
{
  //**************************************************************************************************************
  //**************************************************************************************************************
  //*                                                                                                            *
  //*     This code is auto-generated.  Do not place any cutom code here, it will be lost on re-generation.      *
  //*                                                                                                            *
  //**************************************************************************************************************
  //**************************************************************************************************************
  public class Operations : Afx.Prism.RibbonMdi.StandardOperations
  {
    public const string AddUser = "{c5f20687-d792-4275-af7d-2f42ee9b0459}";
    public const string SetPassword = "{da9d100b-0bf6-420b-b87c-24f0b46f16bd}";
  }
}

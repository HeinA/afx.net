﻿using Afx.Business;
using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Activities;
using FreightManagement.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Activities.ExternalUserManagement
{
  public partial class ExternalUserDetailViewModel : ActivityDetailViewModel<ExternalUser>
  {
    #region Constructors

    [InjectionConstructor]
    public ExternalUserDetailViewModel(IController controller)
      : base(controller)
    {
    }
    
    #endregion
  }
}

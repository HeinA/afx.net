﻿using Afx.Business.Data;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Activities;
using FreightManagement.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Activities.ExternalUserManagement
{
  public class RoleTabViewModel : ActivityTabDetailViewModel<ExternalUser>
  {
    #region Constructors

    [InjectionConstructor]
    public RoleTabViewModel(IController controller)
      : base(controller)
    {
      Title = "_Roles";
    }

    #endregion

    public IEnumerable<RoleItemViewModel> Roles
    {
      get { return ResolveViewModels<RoleItemViewModel, ExternalRole>(Cache.Instance.GetObjects<ExternalRole>(r => r.Text, true)); }
    }
  }
}

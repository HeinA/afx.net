﻿using Afx.Business.Activities;
using FreightManagement.Business;
using FreightManagement.Business.Service;
using Afx.Prism.RibbonMdi.Activities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Activities.ExternalRoleManagement
{
  public partial class ExternalRoleManagement : SimpleActivityContext<ExternalRole>
  {
    public ExternalRoleManagement()
    {
    }

    public ExternalRoleManagement(Activity activity)
      : base(activity)
    {
    }
  }
}

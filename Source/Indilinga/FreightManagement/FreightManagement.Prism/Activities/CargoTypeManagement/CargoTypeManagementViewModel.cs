﻿using Afx.Business.Security;
using Afx.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Activities;
using FreightManagement.Business;

namespace FreightManagement.Prism.Activities.CargoTypeManagement
{
  public partial class CargoTypeManagementViewModel : MdiContextualActivityViewModel<CargoTypeManagement>
  {
    #region Constructors

    [InjectionConstructor]
    public CargoTypeManagementViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion
  }
}

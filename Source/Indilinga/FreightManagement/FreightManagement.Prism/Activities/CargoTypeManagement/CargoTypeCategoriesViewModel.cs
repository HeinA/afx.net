﻿using Afx.Prism;
using FreightManagement.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Activities.CargoTypeManagement
{
  public class CargoTypeCategoriesViewModel : TabViewModel<CargoType>
  {
    #region Constructors

    [InjectionConstructor]
    public CargoTypeCategoriesViewModel(IController controller)
      : base(controller)
    {
      this.Title = "Categories";
    }

    #endregion

  }
}

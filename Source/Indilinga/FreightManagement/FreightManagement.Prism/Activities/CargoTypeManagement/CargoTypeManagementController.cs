﻿using Afx.Business;
using Afx.Business.Activities;
using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Activities;
using FreightManagement.Business;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Activities.CargoTypeManagement
{
  public partial class CargoTypeManagementController : MdiContextualActivityController<CargoTypeManagement, CargoTypeManagementViewModel>
  {
    #region Constructors

    [InjectionConstructor]
    public CargoTypeManagementController(IController controller)
      : base(controller)
    {
    }

    #endregion

    #region OnLoaded
    
    protected override void OnLoaded()
    {
      if (DataContext.Data.Count > 0)
      {
        //Focus the first element 
        CargoTypeItemViewModel ivm = GetCreateViewModel<CargoTypeItemViewModel>(DataContext.Data[0], ViewModel);
        ivm.IsFocused = true;
      }

      RegisterContextRegion(RegionManager.Regions[CargoTypeManagement.DetailsRegion]);
      RegisterContextRegion(RegionManager.Regions[CargoTypeManagement.TabRegion]);

      base.OnLoaded();
    }

    #endregion
    
    #region OnContextViewModelChanged
    
    protected override void OnContextViewModelChanged(ISelectableViewModel context)
    {
      if (context.Model is CargoType)
      {
        CargoTypeDetailViewModel vm = GetCreateViewModel<CargoTypeDetailViewModel>(context.Model, ViewModel);
        AddDetailsViewModel(vm);

        CargoTypeCategoriesViewModel vm1 = GetCreateViewModel<CargoTypeCategoriesViewModel>(context.Model, ViewModel);
        AddTabViewModel(vm1);
      }

      base.OnContextViewModelChanged(context);
    }

    #endregion
    
    #region ExecuteOperation
    
    protected override void ExecuteOperation(Operation op, BusinessObject argument)
    {
      switch (op.Identifier)
      {
        case Operations.AddCargoType:
          CreateRootItem();
          break;
      }
      base.ExecuteOperation(op, argument);
    }
    
    #endregion
  }
}

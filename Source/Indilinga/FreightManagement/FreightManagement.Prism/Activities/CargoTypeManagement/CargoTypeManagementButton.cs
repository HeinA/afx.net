﻿using Afx.Business.Activities;
using Afx.Business.Data;
using Afx.Prism.RibbonMdi;
using Afx.Prism.RibbonMdi.Ribbon;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Activities.CargoTypeManagement
{
  [Export(typeof(IRibbonItem))]
  public class CargoTypeManagementButton : RibbonButtonViewModel
  {
    public override string TabName
    {
      get { return FreightManagementTab.TabName; }
    }

    public override string GroupName
    {
      get { return ConfigurationGroup.GroupName; }
    }

    public override string ItemIdentifier
    {
      get { return "CargoTypeManagement"; }
    }

    public override int Index
    {
      get { return 3; }
    }

    #region Activity Activity

    public Activity Activity
    {
      get { return Cache.Instance.GetObject<Activity>(CargoTypeManagement.Key); }
    }

    #endregion

    public bool IsEnabled
    {
      get
      {
        try
        {
          if (Activity == null) return false;
          return Activity.CanView;
        }
        catch
        {
          return false;
        }
      }
    }

    protected override void OnExecute()
    {
      MdiApplicationController.Current.ExecuteActivity(Activity);
    }
  }
}

﻿using Afx.Business.Activities;
using FreightManagement.Business;
using FreightManagement.Business.Service;
using Afx.Prism.RibbonMdi.Activities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using Afx.Business.Security;
using Afx.Business.Service;
using Afx.Business.Data;

namespace FreightManagement.Prism.Activities.CollectionsViewManagement
{
  public partial class CollectionsViewManagement : CustomActivityContext
  {
    public CollectionsViewManagement()
    {
    }

    public CollectionsViewManagement(Activity activity)
      : base(activity)
    {
    }

    public override bool IsDirty
    {
      get { return false; }
      set { }
    }

    #region Collection<WaybillCollection> Collections

    Collection<WaybillCollection> mCollections;
    Collection<WaybillCollection> Collections
    {
      get { return mCollections; }
      set
      {
        if (SetProperty<Collection<WaybillCollection>>(ref mCollections, value))
        {
          Collection<RouteContainer> routes = new Collection<RouteContainer>();
          foreach (var route in Collections.Select(c => c.CollectionRoute).Distinct().OrderBy(r => r.Name))
          {
            routes.Add(new RouteContainer(route, Collections.Where(c => c.CollectionRoute.Equals(route))));
          }
          Routes = routes;
        }
      }
    }

    #endregion

    #region Collection<RouteContainer> Routes

    Collection<RouteContainer> mRoutes;
    public Collection<RouteContainer> Routes
    {
      get { return mRoutes; }
      set { SetProperty<Collection<RouteContainer>>(ref mRoutes, value); }
    }

    #endregion

    #region DistributionCenter DistributionCenter

    public const string DistributionCenterProperty = "DistributionCenter";
    DistributionCenter mDistributionCenter = Cache.Instance.GetObjects<DistributionCenter>(dc => dc.OrganizationalUnit.Equals(SecurityContext.OrganizationalUnit)).FirstOrDefault();
    public DistributionCenter DistributionCenter
    {
      get { return mDistributionCenter; }
      set
      {
        if (SetProperty<DistributionCenter>(ref mDistributionCenter, value))
        {
          LoadCollections();
        }
      }
    }

    #endregion

    public override void LoadData()
    {
      LoadCollections();
      base.LoadData();
    }

    void LoadCollections()
    {
      using (var svc = ProxyFactory.GetService<IFreightManagementService>(SecurityContext.ServerName))
      {
        Collections = svc.LoadWaybillCollections(DistributionCenter.Id);
      }
    }
  }
}

﻿using Afx.Business;
using FreightManagement.Business;
using Geographics.Business;
using Microsoft.Practices.Prism.Commands;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Activities.CollectionsViewManagement
{
  public class CityContainer : BusinessObject
  {
    public CityContainer(Location city, IEnumerable<WaybillCollection> collections)
    {
      City = city;
      Collections = collections;
    }

    #region Location City

    public const string CityProperty = "City";
    Location mCity;
    public Location City
    {
      get { return mCity; }
      set { mCity = value; }
    }

    #endregion

    #region IEnumerable<WaybillCollection> Collections

    public const string CollectionsProperty = "Collections";
    IEnumerable<WaybillCollection> mCollections;
    public IEnumerable<WaybillCollection> Collections
    {
      get { return mCollections; }
      private set
      {
        mCollections = value;
        foreach (var c in Collections)
        {
          Items += c.Items;
          PhysicalWeight += c.PhysicalWeight;
        }

        Collection<AccountContainer> accounts = new Collection<AccountContainer>();
        foreach (var account in Collections.Select(c => c.Account).Distinct().OrderBy(a => a.Name))
        {
          accounts.Add(new AccountContainer(account, Collections.Where(c => c.Account.Equals(account)).OrderBy(c => c.Account.Name)));
        }
        Accounts = accounts;
      }
    }

    #endregion

    #region IEnumerable<AccountContainer> Accounts

    public const string AccountsProperty = "Accounts";
    IEnumerable<AccountContainer> mAccounts;
    public IEnumerable<AccountContainer> Accounts
    {
      get { return mAccounts; }
      private set { mAccounts = value; }
    }

    #endregion

    #region int Items

    public const string ItemsProperty = "Items";
    int mItems;
    public int Items
    {
      get { return mItems; }
      private set { mItems = value; }
    }

    #endregion

    #region decimal PhysicalWeight

    public const string PhysicalWeightProperty = "PhysicalWeight";
    decimal mPhysicalWeight;
    public decimal PhysicalWeight
    {
      get { return mPhysicalWeight; }
      private set { mPhysicalWeight = value; }
    }

    #endregion
  }
}

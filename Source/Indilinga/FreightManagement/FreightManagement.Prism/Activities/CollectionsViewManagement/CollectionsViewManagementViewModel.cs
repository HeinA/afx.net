﻿using Afx.Business.Security;
using Afx.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Activities;
using FreightManagement.Business;
using Afx.Business.Data;

namespace FreightManagement.Prism.Activities.CollectionsViewManagement
{
  public partial class CollectionsViewManagementViewModel : MdiCustomActivityViewModel<CollectionsViewManagement>
  {
    #region Constructors

    [InjectionConstructor]
    public CollectionsViewManagementViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    public IEnumerable<DistributionCenter> DistributionCenters
    {
      get { return Cache.Instance.GetObjects<DistributionCenter>(true, dc => dc.Name, true); }
    }
  }
}

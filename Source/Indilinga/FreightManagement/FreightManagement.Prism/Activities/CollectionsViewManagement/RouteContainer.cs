﻿using Afx.Business;
using FreightManagement.Business;
using Geographics.Business;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Activities.CollectionsViewManagement
{
  public class RouteContainer : BusinessObject
  {
    public RouteContainer(Route route, IEnumerable<WaybillCollection> collections)
    {
      Route = route;
      Collections = collections;
    }

    #region Route Route

    public const string RouteProperty = "Route";
    Route mRoute;
    public Route Route
    {
      get { return mRoute; }
      private set { mRoute = value; }
    }

    #endregion

    #region IEnumerable<WaybillCollection> Collections

    public const string CollectionsProperty = "Collections";
    IEnumerable<WaybillCollection> mCollections;
    public IEnumerable<WaybillCollection> Collections
    {
      get { return mCollections; }
      private set
      {
        mCollections = value;
        foreach (var c in Collections)
        {
          Items += c.Items;
          PhysicalWeight += c.PhysicalWeight;
        }

        Collection<CityContainer> cities = new Collection<CityContainer>();
        foreach (var city in Collections.Select(c => c.CollectionCity).Distinct().OrderBy(l => l.Name))
        {
          cities.Add(new CityContainer(city, Collections.Where(c => c.CollectionCity.Equals(city)).OrderBy(c => c.CollectionDate)));
        }
        Cities = cities;
      }
    }

    #endregion

    #region IEnumerable<CityContainer> Cities

    public const string CitiesProperty = "Cities";
    IEnumerable<CityContainer> mCities;
    public IEnumerable<CityContainer> Cities
    {
      get { return mCities; }
      private set { mCities = value; }
    }

    #endregion

    #region int Items

    public const string ItemsProperty = "Items";
    int mItems;
    public int Items
    {
      get { return mItems; }
      private set { mItems = value; }
    }

    #endregion

    #region decimal PhysicalWeight

    public const string PhysicalWeightProperty = "PhysicalWeight";
    decimal mPhysicalWeight;
    public decimal PhysicalWeight
    {
      get { return mPhysicalWeight; }
      private set { mPhysicalWeight = value; }
    }

    #endregion

    #region string RouteName

    public string RouteName
    {
      get
      {
        if (BusinessObject.IsNull(Route)) return "Point to Point";
        return Route.Name;
      }
    }

    #endregion
  }
}

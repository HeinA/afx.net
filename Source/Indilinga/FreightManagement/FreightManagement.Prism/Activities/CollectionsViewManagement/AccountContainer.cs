﻿using AccountManagement.Business;
using Afx.Business;
using Afx.Business.Data;
using Afx.Business.Documents;
using Afx.Prism.RibbonMdi;
using Afx.Prism.RibbonMdi.Events;
using FreightManagement.Business;
using Microsoft.Practices.Prism.Commands;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Activities.CollectionsViewManagement
{
  public class AccountContainer : BusinessObject
  {
    public AccountContainer(AccountReference account, IEnumerable<WaybillCollection> collections)
    {
      Account = account;
      Collections = collections;
    }

    public string AccountName
    {
      get
      {
        string s = Account.Name;
        if (string.IsNullOrWhiteSpace(s)) return "None";
        return s;
      }
    }

    #region AccountReference Account

    public const string AccountProperty = "Account";
    AccountReference mAccount;
    public AccountReference Account
    {
      get { return mAccount; }
      private set { mAccount = value; }
    }

    #endregion

    #region IEnumerable<WaybillCollection> Collections

    public const string CollectionsProperty = "Collections";
    IEnumerable<WaybillCollection> mCollections;
    public IEnumerable<WaybillCollection> Collections
    {
      get { return mCollections; }
      private set
      {
        mCollections = value;
        foreach (var c in Collections)
        {
          Items += c.Items;
          PhysicalWeight += c.PhysicalWeight;
        }
      }
    }

    #endregion

    #region IEnumerable<CityContainer> Cities

    public const string CitiesProperty = "Cities";
    IEnumerable<CityContainer> mCities;
    public IEnumerable<CityContainer> Cities
    {
      get { return mCities; }
      set { mCities = value; }
    }

    #endregion

    #region int Items

    public const string ItemsProperty = "Items";
    int mItems;
    public int Items
    {
      get { return mItems; }
      private set { mItems = value; }
    }

    #endregion

    #region decimal PhysicalWeight

    public const string PhysicalWeightProperty = "PhysicalWeight";
    decimal mPhysicalWeight;
    public decimal PhysicalWeight
    {
      get { return mPhysicalWeight; }
      private set { mPhysicalWeight = value; }
    }

    #endregion

    #region DelegateCommand<WaybillCollection> OpenWaybillCommand

    DelegateCommand<WaybillCollection> mOpenWaybillCommand;
    public DelegateCommand<WaybillCollection> OpenWaybillCommand
    {
      get { return mOpenWaybillCommand ?? (mOpenWaybillCommand = new DelegateCommand<WaybillCollection>(ExecuteOpenWaybill, CanExecuteOpenWaybill)); }
    }

    bool CanExecuteOpenWaybill(WaybillCollection args)
    {
      return true;
    }

    void ExecuteOpenWaybill(WaybillCollection args)
    {
      MdiApplicationController.Current.EventAggregator.GetEvent<EditDocumentEvent>().Publish(new EditDocumentEventArgs(Cache.Instance.GetObject<DocumentType>(Waybill.DocumentTypeIdentifier), args.GlobalIdentifier));
    }

    #endregion
  }
}

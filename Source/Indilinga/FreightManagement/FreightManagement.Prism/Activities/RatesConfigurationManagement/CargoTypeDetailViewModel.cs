﻿using Afx.Prism;
using FreightManagement.Business;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Activities.RatesConfigurationManagement
{
  public class CargoTypeDetailViewModel : ViewModel<RatesConfigurationRevisionCargoType>, IRateTableContainer
  {
    #region Constructors

    [InjectionConstructor]
    public CargoTypeDetailViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    public IEnumerable<RateTableViewModel> Tables
    {
      get { return ResolveViewModels<RateTableViewModel, RateTable>(Model.Tables).Where(vm => !vm.Model.IsDeleted); }
    }

    #region DelegateCommand AddTableCommand

    DelegateCommand mAddTableCommand;
    public DelegateCommand AddTableCommand
    {
      get { return mAddTableCommand ?? (mAddTableCommand = new DelegateCommand(ExecuteAddTable, CanExecuteAddTable)); }
    }

    bool CanExecuteAddTable()
    {
      return true;
    }

    void ExecuteAddTable()
    {
      RateTable rt = new RateTable();
      rt.BillingUnit = this.Model.Reference.BillingUnit;
      rt.UnitOfMeasure = this.Model.Reference.UnitOfMeasure;
      Model.Tables.Add(rt);
      RefreshTables();
    }

    #endregion

    public void RefreshTables()
    {
      OnPropertyChanged("Tables");
    }
  }
}

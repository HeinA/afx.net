﻿using Afx.Prism;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Activities.RatesConfigurationManagement
{
  public class NotActivatedViewModel : ViewModel
  {
    #region Constructors

    [InjectionConstructor]
    public NotActivatedViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion
  }
}

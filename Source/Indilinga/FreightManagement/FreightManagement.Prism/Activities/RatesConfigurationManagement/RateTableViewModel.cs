﻿using Afx.Business.Data;
using Afx.Prism;
using FreightManagement.Business;
using FreightManagement.Prism.Activities.RatesConfigurationManagement.EditFeesDialog;
using FreightManagement.Prism.Activities.RatesConfigurationManagement.EditLocationsDialog;
using FreightManagement.Prism.Activities.RatesConfigurationManagement.EditRateCategoriesDialog;
using Geographics.Business;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Activities.RatesConfigurationManagement
{
  public class RateTableViewModel : ViewModel<RateTable>
  {
    #region Constructors

    [InjectionConstructor]
    public RateTableViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    new IRateTableContainer Parent
    {
      get { return (IRateTableContainer)base.Parent; }
    }
    #region DelegateCommand EditFeesCommand

    DelegateCommand mEditFeesCommand;
    public DelegateCommand EditFeesCommand
    {
      get { return mEditFeesCommand ?? (mEditFeesCommand = new DelegateCommand(ExecuteEditFees, CanExecuteEditFees)); }
    }

    bool CanExecuteEditFees()
    {
      return true;
    }

    void ExecuteEditFees()
    {
      EditFeesDialogController c = Controller.CreateChildController<EditFeesDialogController>();
      c.Table = Model;
      c.Run();
    }

    #endregion

    #region DelegateCommand EditRateCategoriesCommand

    DelegateCommand mEditRateCategoriesCommand;
    public DelegateCommand EditRateCategoriesCommand
    {
      get { return mEditRateCategoriesCommand ?? (mEditRateCategoriesCommand = new DelegateCommand(ExecuteEditRateCategories, CanExecuteEditRateCategories)); }
    }

    bool CanExecuteEditRateCategories()
    {
      return true;
    }

    void ExecuteEditRateCategories()
    {
      EditRateCategoriesDialogController c = Controller.CreateChildController<EditRateCategoriesDialogController>();
      c.Table = Model;
      c.Run();
    }

    #endregion

    #region DelegateCommand EditLocationsCommand

    DelegateCommand mEditLocationsCommand;
    public DelegateCommand EditLocationsCommand
    {
      get { return mEditLocationsCommand ?? (mEditLocationsCommand = new DelegateCommand(ExecuteEditLocations, CanExecuteEditLocations)); }
    }

    bool CanExecuteEditLocations()
    {
      return true;
    }

    void ExecuteEditLocations()
    {
      EditLocationsDialogController c = Controller.CreateChildController<EditLocationsDialogController>();
      c.Table = Model;
      c.Run();
    }

    #endregion

    #region DelegateCommand ToggleDeleteCommand

    DelegateCommand mToggleDeleteCommand;
    public DelegateCommand ToggleDeleteCommand
    {
      get { return mToggleDeleteCommand ?? (mToggleDeleteCommand = new DelegateCommand(ExecuteToggleDelete, CanExecuteToggleDelete)); }
    }

    bool CanExecuteToggleDelete()
    {
      return true;
    }

    void ExecuteToggleDelete()
    {
      Model.IsDeleted = !Model.IsDeleted;
      Parent.RefreshTables();
    }

    #endregion

    public IEnumerable<Location> Locations
    {
      get { return Cache.Instance.GetObjects<Location>(true, l => l.GetExtensionObject<DeliveryLocation>() != null && l.GetExtensionObject<DeliveryLocation>().DeliveryPointType != DeliveryPointType.None, l => l.FullName, true); }
    }

    public IEnumerable<DistributionCenter> DistributionCenters
    {
      get { return Cache.Instance.GetObjects<DistributionCenter>(true, dc => dc.Name, true); }
    }

  }
}

﻿using Afx.Business;
using Afx.Business.Activities;
using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Business.Service;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Activities;
using FreightManagement.Business;
using FreightManagement.Business.Service;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace FreightManagement.Prism.Activities.RatesConfigurationManagement
{
  public partial class RatesConfigurationManagementController : MdiCustomActivityController<RatesConfigurationManagement, RatesConfigurationManagementViewModel>
  {
    #region Constructors

    [InjectionConstructor]
    public RatesConfigurationManagementController(IController controller)
      : base(controller)
    {
    }

    #endregion

    #region OnLoaded

    protected override void OnLoaded()
    {
      ViewModel.IsFocused = true;
      base.OnLoaded();
    }

    #endregion
    
    #region ExecuteOperation
    
    protected override void ExecuteOperation(Operation op, BusinessObject argument)
    {

      base.ExecuteOperation(op, argument);
    }
    
    #endregion

    #region void OnRefresh()

    public override void OnRefresh()
    {
      base.OnRefresh();
      ViewModel.Refresh();
    }

    #endregion

    #region void LoadDataContext()

    protected override void LoadDataContext()
    {
      DataContext.Data = null;

      using (var svc = ProxyFactory.GetService<IFreightManagementService>(SecurityContext.ServerName))
      {
        if (DataContext.ContextId == Guid.Empty)
        {
          DataContext.Data = svc.LoadStandardRatesConfiguration();
          WeakEventManager<BusinessObject, CompositionChangedEventArgs>.AddHandler(DataContext.Data, "CompositionChanged", OnDataCompositionChanged);
        }
      }

      if (DataContext.Data.Revisions.Count == 0) DataContext.Data.Revisions.Add(new RatesConfigurationRevision());
      base.LoadDataContext();
    }

    #endregion

    #region void SaveDataContext()

    protected override void SaveDataContext()
    {
      using (var svc = ProxyFactory.GetService<IFreightManagementService>(SecurityContext.ServerName))
      {
        DataContext.Data = svc.SaveRatesConfiguration(DataContext.Data);
      }

      Refresh(true);

      base.SaveDataContext();
    }

    #endregion

    #region void OnDataCompositionChanged(...)

    private void OnDataCompositionChanged(object sender, CompositionChangedEventArgs e)
    {
      ViewModel.RaiseErrorEvents();
    }

    #endregion
  }
}
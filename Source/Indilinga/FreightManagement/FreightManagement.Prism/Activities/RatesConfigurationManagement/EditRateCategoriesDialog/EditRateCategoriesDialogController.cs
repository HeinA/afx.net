﻿using Afx.Prism;
using Afx.Prism.RibbonMdi;
using FreightManagement.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace FreightManagement.Prism.Activities.RatesConfigurationManagement.EditRateCategoriesDialog
{
  public class EditRateCategoriesDialogController : MdiDialogController<EditRateCategoriesDialogContext, EditRateCategoriesDialogViewModel>
  {
    public const string EditRateCategoriesDialogControllerKey = "FreightManagement.Prism.Activities.RatesConfigurationManagement.EditRateCategoriesDialog.EditRateCategoriesDialogController";

    public EditRateCategoriesDialogController(IController controller)
      : base(EditRateCategoriesDialogControllerKey, controller)
    {
    }

    protected override bool OnRun()
    {
      if (DataContext == null) DataContext = new EditRateCategoriesDialogContext();
      ViewModel.Caption = "Edit Rate Categories";

      foreach (var c in Table.Columns)
      {
        DataContext.Categories.Add((RateTableCategory)c.Clone());
      }
      
      return base.OnRun();
    }

    protected override void ApplyChanges()
    {
      foreach (var c in Table.Columns)
      {
        if (!DataContext.Categories.Where(c1 => !c1.IsDeleted).Contains(c))
        {
          c.IsDeleted = true;
        }
      }

      foreach (var c in DataContext.Categories)
      {
        if (Table.Columns.Contains(c))
        {
          var c1 = Table.Columns.OfType<RateTableCategory>().FirstOrDefault(c2 => c2.Equals(c));
          c1.StartUnit = c.StartUnit;
          c1.IsFixedRate = c.IsFixedRate;
          c.IsPercentage = c.IsPercentage;
        }
        else
        {
          Table.AddColumn(c);
        }
      }
      base.ApplyChanges();
    }

    #region RatesConfigurationRevisionCargoTypeTable Table

    public const string TableProperty = "Table";
    RateTable mTable;
    public RateTable Table
    {
      get { return mTable; }
      set { mTable = value; }
    }

    #endregion
  }
}

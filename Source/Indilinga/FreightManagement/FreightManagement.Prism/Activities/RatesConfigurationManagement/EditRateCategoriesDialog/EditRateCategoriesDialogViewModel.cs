﻿using Afx.Prism;
using Afx.Prism.RibbonMdi;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Activities.RatesConfigurationManagement.EditRateCategoriesDialog
{
  public class EditRateCategoriesDialogViewModel : MdiDialogViewModel<EditRateCategoriesDialogContext>
  {
    [InjectionConstructor]
    public EditRateCategoriesDialogViewModel(IController controller)
      : base(controller)
    {
    }
  }
}

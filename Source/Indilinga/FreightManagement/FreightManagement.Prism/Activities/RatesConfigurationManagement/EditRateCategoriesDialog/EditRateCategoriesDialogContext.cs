﻿using Afx.Business;
using Afx.Business.Collections;
using FreightManagement.Business;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Activities.RatesConfigurationManagement.EditRateCategoriesDialog
{
  public class EditRateCategoriesDialogContext : BusinessObject
  {
    BasicCollection<RateTableCategory> mCategories;
    public BasicCollection<RateTableCategory> Categories
    {
      get { return mCategories ?? (mCategories = new BasicCollection<RateTableCategory>()); }
    }
  }
}
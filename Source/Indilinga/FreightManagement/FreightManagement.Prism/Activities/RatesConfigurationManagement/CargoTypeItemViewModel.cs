﻿using Afx.Prism;
using FreightManagement.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Activities.RatesConfigurationManagement
{
  public class CargoTypeItemViewModel : SelectableViewModel<CargoType>
  {
    #region Constructors

    [InjectionConstructor]
    public CargoTypeItemViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    #region RatesConfigurationRevisionViewModel Parent

    public new RatesConfigurationRevisionViewModel Parent
    {
      get { return (RatesConfigurationRevisionViewModel)base.Parent; }
    }

    #endregion

    #region bool HasCargoType

    public const string HasCargoTypeProperty = "HasCargoType";
    public bool HasCargoType
    {
      get { return Parent.Model.CargoTypes.Contains(Model); }
      set
      {
        if (value)
        {
          if (!Parent.Model.CargoTypes.Contains(Model))
          {
            Parent.Model.CargoTypes.Add(Model);
            Parent.DisplayCargoTypeTables();
          }
        }
        else
        {
          if (Parent.Model.CargoTypes.Contains(Model))
          {
            Parent.Model.CargoTypes.Remove(Model);
            Parent.DisplayCargoTypeTables();
          }
        }
      }
    }

    #endregion
  }
}

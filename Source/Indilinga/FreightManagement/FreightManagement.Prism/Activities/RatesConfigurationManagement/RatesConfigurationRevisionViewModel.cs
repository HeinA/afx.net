﻿using Afx.Business.Data;
using Afx.Prism;
using FreightManagement.Business;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Activities.RatesConfigurationManagement
{
  public class RatesConfigurationRevisionViewModel : ViewModel<RatesConfigurationRevision>, IRateTableContainer
  {
    #region Constructors

    [InjectionConstructor]
    public RatesConfigurationRevisionViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    [Dependency]
    public RatesConfigurationManagementController RatesConfigurationManagementController { get; set; }

    #region string TitleText

    public const string TitleTextProperty = "TitleText";
    public string TitleText
    {
      get { return RatesConfigurationManagementController.ViewModel.TitleText; }
    }

    #endregion

    #region IEnumerable<CargoTypeItemViewModel> CargoTypes

    Collection<CargoType> mCargoTypes;
    public IEnumerable<CargoTypeItemViewModel> CargoTypes
    {
      get
      {
        if (mCargoTypes == null)
        {
          mCargoTypes = Cache.Instance.GetObjects<CargoType>(ct => ct.Text, true);
        }
        IEnumerable<CargoTypeItemViewModel> col = ResolveViewModels<CargoTypeItemViewModel, CargoType>(mCargoTypes);
        SelectedCargoType = col.FirstOrDefault();
        return col;
      }
    }

    #endregion

    #region CargoTypeItemViewModel SelectedCargoType

    public const string SelectedCargoTypeProperty = "SelectedCargoType";
    CargoTypeItemViewModel mSelectedCargoType;
    public CargoTypeItemViewModel SelectedCargoType
    {
      get { return mSelectedCargoType; }
      set
      {
        if (SetProperty<CargoTypeItemViewModel>(ref mSelectedCargoType, value))
        {
          DisplayCargoTypeTables();
        }
      }
    }

    #endregion

    #region object CargoTypeDetailsViewModel

    public const string CargoTypeDetailsViewModelProperty = "CargoTypeDetailsViewModel";
    object mCargoTypeDetailsViewModel;
    public object CargoTypeDetailsViewModel
    {
      get { return mCargoTypeDetailsViewModel; }
      set { SetProperty<object>(ref mCargoTypeDetailsViewModel, value); }
    }

    #endregion

    #region DelegateCommand AddTableByWeightCommand

    DelegateCommand mAddTableByWeightCommand;
    public DelegateCommand AddTableByWeightCommand
    {
      get { return mAddTableByWeightCommand ?? (mAddTableByWeightCommand = new DelegateCommand(ExecuteAddTableByWeight, CanExecuteAddTableByWeight)); }
    }

    bool CanExecuteAddTableByWeight()
    {
      return true;
    }

    void ExecuteAddTableByWeight()
    {
      RateTable rt = new RateTable();
      rt.BillingUnit = BillingUnit.Weight;
      rt.UnitOfMeasure = "kg";
      Model.TablesByWeight.Add(rt);
      RefreshTables();
    }

    #endregion

    public IEnumerable<RateTableViewModel> TablesByWeight
    {
      get { return ResolveViewModels<RateTableViewModel, RateTable>(Model.TablesByWeight).Where(vm => !vm.Model.IsDeleted); }
    }

    public void RefreshTables()
    {
      OnPropertyChanged("TablesByWeight");
    }

    #region void DisplayCargoTypeTables()

    public void DisplayCargoTypeTables()
    {
      if (!SelectedCargoType.HasCargoType)
      {
        CargoTypeDetailsViewModel = RatesConfigurationManagementController.GetCreateViewModel<NotActivatedViewModel>(this);
      }
      else
      {
        CargoTypeDetailsViewModel = RatesConfigurationManagementController.GetCreateViewModel<CargoTypeDetailViewModel>(Model.GetAssociativeObject<RatesConfigurationRevisionCargoType>(SelectedCargoType.Model), this);
      }
    }

    #endregion
  }
}

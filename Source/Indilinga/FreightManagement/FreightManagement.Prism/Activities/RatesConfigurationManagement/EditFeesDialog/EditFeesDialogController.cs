﻿using Afx.Business.Collections;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using FreightManagement.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace FreightManagement.Prism.Activities.RatesConfigurationManagement.EditFeesDialog
{
  public class EditFeesDialogController : MdiDialogController<EditFeesDialogContext, EditFeesDialogViewModel>
  {
    public const string EditFeesDialogControllerKey = "FreightManagement.Prism.Activities.RatesConfigurationManagement.EditFeesDialog.EditFeesDialogController";

    public EditFeesDialogController(IController controller)
      : base(EditFeesDialogControllerKey, controller)
    {
    }

    protected override bool OnRun()
    {
      if (DataContext == null) DataContext = new EditFeesDialogContext();
      ViewModel.Caption = "EditFeesDialog";
      return base.OnRun();
    }

    protected override void ApplyChanges()
    {
      foreach (var f in Table.Columns.OfType<RateTableFee>())
      {
        if (!ApplicableFees.Contains(f.Fee))
        {
          f.IsDeleted = true;
        }
      }

      foreach (var f in ApplicableFees)
      {
        RateTableFee ff = Table.Columns.OfType<RateTableFee>().FirstOrDefault(f1 => f1.Fee.Equals(f));
        if (ff == null)
        {
          Table.AddColumn(new RateTableFee(f));
        }
        else
        {
          ff.IsDeleted = false;
        }
      }

      base.ApplyChanges();
    }

    BasicCollection<TransportFee> mApplicableFees;
    public BasicCollection<TransportFee> ApplicableFees
    {
      get { return mApplicableFees ?? (mApplicableFees = new BasicCollection<TransportFee>()); }
    }

    #region RatesConfigurationRevisionCargoTypeTable Table

    public const string TableProperty = "Table";
    RateTable mTable;
    public RateTable Table
    {
      get { return mTable; }
      set
      {
        mTable = value;
        ApplicableFees.Clear();
        foreach (var f in Table.Columns.OfType<RateTableFee>().Where(f => !f.IsDeleted))
        {
          ApplicableFees.Add(f.Fee);
        }
      }
    }

    #endregion
  }
}

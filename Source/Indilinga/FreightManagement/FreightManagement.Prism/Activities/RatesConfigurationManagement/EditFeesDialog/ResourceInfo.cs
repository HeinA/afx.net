﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Activities.RatesConfigurationManagement.EditFeesDialog
{
  [Export(typeof(Afx.Prism.ResourceInfo))]
  public class ResourceInfo : Afx.Prism.ResourceInfo
  {
    public override Uri GetUri()
    {
      return new Uri("pack://application:,,,/FreightManagement.Prism;component/Activities/RatesConfigurationManagement/EditFeesDialog/Resources.xaml", UriKind.Absolute);
    }
  }
}
﻿using Afx.Prism;
using FreightManagement.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Activities.RatesConfigurationManagement.EditFeesDialog
{
  public class FeeItemViewModel : SelectableViewModel<TransportFee>
  {
    [InjectionConstructor]
    public FeeItemViewModel(IController controller)
      : base(controller)
    {
    }

    [Dependency]
    public EditFeesDialogController EditFeesDialogController { get; set; }

    #region bool HasFee

    public bool HasFee
    {
      get { return EditFeesDialogController.ApplicableFees.Contains(Model); }
      set
      {
        if (value)
        {
          if (!EditFeesDialogController.ApplicableFees.Contains(Model))
          {
            EditFeesDialogController.ApplicableFees.Add(Model);
          }
        }
        else
        {
          if (EditFeesDialogController.ApplicableFees.Contains(Model))
          {
            EditFeesDialogController.ApplicableFees.Remove(Model);
          }
        }
      }
    }

    #endregion
  }
}

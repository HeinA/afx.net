﻿using Afx.Business.Activities;
using FreightManagement.Business;
using FreightManagement.Business.Service;
using Afx.Prism.RibbonMdi.Activities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Afx.Business;

namespace FreightManagement.Prism.Activities.RatesConfigurationManagement
{
  public partial class RatesConfigurationManagement : CustomActivityContext
  {
    public const string RevisionRegion = "RevisionRegion";
    public const string RevisionDetailsRegion = "RevisionDetailsRegion";

    #region Constructors

    public RatesConfigurationManagement()
    {
    }

    public RatesConfigurationManagement(Activity activity)
      : base(activity)
    {
    }

    #endregion

    #region string TitleText

    public override string TitleText
    {
      get
      {
        if (BusinessObject.IsNull(Data))
        {
          return "Invalid";
        }
        else
        {
          //if (BusinessObject.IsNull(Data.Account))
          //{
            return "Standard Rates Configuration";
          //}
          //else
          //{
          //  return string.Format("{0} Rates Configuration", Data.Account.Name);
          //}
        }
      }
    }

    #endregion

    #region RatesConfiguration Data

    public const string DataProperty = "Model";
    RatesConfiguration mData;
    public RatesConfiguration Data
    {
      get { return mData; }
      set
      {
        if (SetProperty<RatesConfiguration>(ref mData, value))
        {
          if (value != null) WeakEventManager<BusinessObject, CompositionChangedEventArgs>.AddHandler(mData, "CompositionChanged", OnDataCompositionChanged);
          OnDataChanged();
        }
      }
    }

    void OnDataCompositionChanged(object sender, CompositionChangedEventArgs e)
    {
      IsDirty = true;
      if (e.PropertyName != ErrorProperty && e.PropertyName != IsValidProperty && e.PropertyName != HasErrorProperty)
      {
        Validate();
      }
      OnPropertyChanged("Title");
    }

    protected virtual void OnDataChanged()
    {
    }

    #endregion

    #region bool Validate()

    public override bool Validate()
    {
      if (Data == null) return true;
      return Data.Validate();
    }

    #endregion
  }
}

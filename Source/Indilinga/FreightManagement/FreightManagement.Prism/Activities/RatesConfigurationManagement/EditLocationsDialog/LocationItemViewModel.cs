﻿using Afx.Prism;
using Geographics.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Activities.RatesConfigurationManagement.EditLocationsDialog
{
  public class LocationItemViewModel : SelectableViewModel<Location>
  {
    [InjectionConstructor]
    public LocationItemViewModel(IController controller)
      : base(controller)
    {
    }

    [Dependency]
    public EditLocationsDialogController EditLocationsDialogController { get; set; }

    #region bool HasLocation

    public bool HasLocation
    {
      get { return EditLocationsDialogController.ApplicableLocations.Contains(Model); }
      set
      {
        if (value)
        {
          if (!EditLocationsDialogController.ApplicableLocations.Contains(Model))
          {
            EditLocationsDialogController.ApplicableLocations.Add(Model);
          }
        }
        else
        {
          if (EditLocationsDialogController.ApplicableLocations.Contains(Model))
          {
            EditLocationsDialogController.ApplicableLocations.Remove(Model);
          }
        }
      }
    }

    #endregion
  }
}

﻿using Afx.Business.Security;
using Afx.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Activities;
using FreightManagement.Business;
using System.Windows;
using System.Collections.ObjectModel;

namespace FreightManagement.Prism.Activities.RatesConfigurationManagement
{
  public partial class RatesConfigurationManagementViewModel : MdiCustomActivityViewModel<RatesConfigurationManagement>
  {
    #region Constructors

    [InjectionConstructor]
    public RatesConfigurationManagementViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    [Dependency]
    public RatesConfigurationManagementController RatesConfigurationManagementController { get; set; }

    #region RatesConfiguration Data

    public RatesConfiguration Data
    {
      get { return Model == null ? GetDefaultValue<RatesConfiguration>() : Model.Data; }
    }

    #endregion

    #region string Error

    public new string Error
    {
      get
      {
        if (Data == null) return null;
        return Data.Error;
      }
    }

    #endregion

    #region bool IsValid

    public override bool IsValid
    {
      get { return string.IsNullOrWhiteSpace(Error); }
    }

    #endregion

    #region void RaiseErrorEvents()

    public void RaiseErrorEvents()
    {
      OnPropertyChanged(BusinessObject.ErrorProperty);
      OnPropertyChanged("ErrorVisibility");
    }

    #endregion

    #region void Refresh()

    public void Refresh()
    {
      OnPropertyChanged(null);
    }

    #endregion 

    #region RatesConfigurationRevisionViewModel RevisionViewModel

    public const string RevisionViewModelProperty = "RevisionViewModel";
    RatesConfigurationRevisionViewModel mRevisionViewModel;
    public RatesConfigurationRevisionViewModel RevisionViewModel
    {
      get { return mRevisionViewModel; }
      set { SetProperty<RatesConfigurationRevisionViewModel>(ref mRevisionViewModel, value); }
    }

    #endregion

    #region RatesConfigurationRevision SelectedRevision

    RatesConfigurationRevision mSelectedRevision;
    public RatesConfigurationRevision SelectedRevision
    {
      get
      {
        try
        {
          if (Data == null) 
            return null;

          if (mSelectedRevision == null)
          {
            SelectedRevision = Data.Revisions.Where(r => r.EffectiveDate.Date <= DateTime.Now.Date).OrderByDescending(r => r.EffectiveDate).FirstOrDefault();
          }
          return mSelectedRevision;
        }
        catch
        {
          throw;
        }
      }
      set
      {
        if (SetProperty<RatesConfigurationRevision>(ref mSelectedRevision, value))
        {
          if (SelectedRevision != null)
          {
            RevisionViewModel = RatesConfigurationManagementController.GetCreateViewModel<RatesConfigurationRevisionViewModel>(SelectedRevision);
          }
        }
      }
    }

    #endregion
  }
}

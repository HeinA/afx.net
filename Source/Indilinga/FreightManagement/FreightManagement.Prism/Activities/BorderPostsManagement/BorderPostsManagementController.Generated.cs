﻿using FreightManagement.Business;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.Composition;
using Microsoft.Practices.Prism;

namespace FreightManagement.Prism.Activities.BorderPostsManagement
{
  //**************************************************************************************************************
  //**************************************************************************************************************
  //*                                                                                                            *
  //*     This code is auto-generated.  Do not place any cutom code here, it will be lost on re-generation.      *
  //*                                                                                                            *
  //**************************************************************************************************************
  //**************************************************************************************************************
  [Export("Controller:" + global::FreightManagement.Prism.Activities.BorderPostsManagement.BorderPostsManagement.Key)]
  public partial class BorderPostsManagementController
  {
    void CreateRootItem()
    {
      BorderPosts o = new BorderPosts();
      DataContext.Data.Add(o);
      SelectContextViewModel(o);
      FocusViewModel<BorderPostsDetailViewModel>(o);
    }

    public void AddDetailsViewModel(object viewModel)
    {
      RegionManager.Regions[BorderPostsManagement.DetailsRegion].Add(viewModel);
    }

    public void AddTabViewModel(object viewModel)
    {
      IRegion tabRegion = RegionManager.Regions[BorderPostsManagement.TabRegion];
      tabRegion.Add(viewModel);
      IActiveAware aw = viewModel as IActiveAware;
      if (aw != null && aw.IsActive) tabRegion.Activate(viewModel);
    }
  }
}

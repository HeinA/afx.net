﻿using Afx.Business;
using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using FreightManagement.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Activities.BorderPostsManagement
{
  public partial class BorderPostsItemViewModel : MdiNavigationListItemViewModel<BorderPosts>
  {
    #region Constructors

    [InjectionConstructor]
    public BorderPostsItemViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    #region string Text

    public string Text
    {
      get
      {
        if (Model == null) return string.Empty;
        if (string.IsNullOrWhiteSpace(Model.Text)) return "*** Unnamed ***";
        return Model.Text;
      }
    }

    #endregion
  }
}

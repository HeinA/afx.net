﻿using Afx.Business;
using Afx.Business.Activities;
using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Activities;
using FreightManagement.Business;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Activities.BorderPostsManagement
{
  public partial class BorderPostsManagementController : MdiContextualActivityController<BorderPostsManagement, BorderPostsManagementViewModel>
  {
    #region Constructors

    [InjectionConstructor]
    public BorderPostsManagementController(IController controller)
      : base(controller)
    {
    }

    #endregion

    #region OnLoaded
    
    protected override void OnLoaded()
    {
      if (DataContext.Data.Count > 0)
      {
        //Focus the first element 
        BorderPostsItemViewModel ivm = GetCreateViewModel<BorderPostsItemViewModel>(DataContext.Data[0], ViewModel);
        ivm.IsFocused = true;
      }

      RegisterContextRegion(RegionManager.Regions[BorderPostsManagement.DetailsRegion]);
      RegisterContextRegion(RegionManager.Regions[BorderPostsManagement.TabRegion]);

      base.OnLoaded();
    }

    #endregion
    
    #region OnContextViewModelChanged
    
    protected override void OnContextViewModelChanged(ISelectableViewModel context)
    {
      if (context.Model is BorderPosts)
      {
        BorderPostsDetailViewModel vm = GetCreateViewModel<BorderPostsDetailViewModel>(context.Model, ViewModel);
        AddDetailsViewModel(vm);
      }

      base.OnContextViewModelChanged(context);
    }

    #endregion
    
    #region ExecuteOperation
    
    protected override void ExecuteOperation(Operation op, BusinessObject argument)
    {
      switch (op.Identifier)
      {
        case Operations.AddBorderPost:
          CreateRootItem();
          break;
      }
      base.ExecuteOperation(op, argument);
    }
    
    #endregion
  }
}

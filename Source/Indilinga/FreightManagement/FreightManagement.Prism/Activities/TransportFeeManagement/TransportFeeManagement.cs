﻿using Afx.Business.Activities;
using FreightManagement.Business;
using FreightManagement.Business.Service;
using Afx.Prism.RibbonMdi.Activities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Activities.TransportFeeManagement
{
  public partial class TransportFeeManagement : SimpleActivityContext<TransportFee>
  {
    public TransportFeeManagement()
    {
    }

    public TransportFeeManagement(Activity activity)
      : base(activity)
    {
    }
  }
}

﻿using FreightManagement.Business;
using FreightManagement.Business.Service;
using Afx.Business.Service;
using Afx.Prism.RibbonMdi.Activities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.Composition;
using Afx.Business.Security;

namespace FreightManagement.Prism.Activities.TransportFeeManagement
{
  //**************************************************************************************************************
  //**************************************************************************************************************
  //*                                                                                                            *
  //*     This code is auto-generated.  Do not place any cutom code here, it will be lost on re-generation.      *
  //*                                                                                                            *
  //**************************************************************************************************************
  //**************************************************************************************************************
  [Export("Context:" + TransportFeeManagement.Key)]
  public partial class TransportFeeManagement
  {
    public const string Key = "{a1a08987-e6d0-4689-bd62-3c57d8c70255}";

    public override void LoadData()
    {
      using (var svc = ProxyFactory.GetService<IFreightManagementService>(SecurityContext.MasterServer.Name))
      {
        Data = svc.LoadTransportFeeCollection();
      }
      base.LoadData();
    }

    public override void SaveData()
    {
      using (var svc = ProxyFactory.GetService<IFreightManagementService>(SecurityContext.MasterServer.Name))
      {
        Data = svc.SaveTransportFeeCollection(Data);
      }
      base.SaveData();
    }
  }
}

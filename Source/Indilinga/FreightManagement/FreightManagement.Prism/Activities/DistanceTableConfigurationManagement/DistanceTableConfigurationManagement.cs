﻿using Afx.Business.Activities;
using FreightManagement.Business;
using FreightManagement.Business.Service;
using Afx.Prism.RibbonMdi.Activities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Afx.Business.Service;
using Afx.Business.Security;
using Afx.Business;

namespace FreightManagement.Prism.Activities.DistanceTableConfigurationManagement
{
  public partial class DistanceTableConfigurationManagement : CustomActivityContext
  {
    public DistanceTableConfigurationManagement()
    {
    }

    public DistanceTableConfigurationManagement(Activity activity)
      : base(activity)
    {
    }

    #region DistanceTable DistanceTable

    public const string DistanceTableProperty = "DistanceTable";
    DistanceTable mDistanceTable;
    public DistanceTable DistanceTable
    {
      get { return mDistanceTable; }
      set
      {
        if (mDistanceTable != null) mDistanceTable.CompositionChanged -= mDistanceTable_CompositionChanged;
        SetProperty<DistanceTable>(ref mDistanceTable, value);
        if (mDistanceTable != null) mDistanceTable.CompositionChanged += mDistanceTable_CompositionChanged;
      }
    }

    void mDistanceTable_CompositionChanged(object sender, CompositionChangedEventArgs e)
    {
      IsDirty = true;
      OnPropertyChanged("Title");
    }

    #endregion

    public override void LoadData()
    {
      using (var svc = ServiceFactory.GetService<IFreightManagementService>(SecurityContext.MasterServer.Name))
      {
        DistanceTable = svc.Instance.LoadDistanceTable();
      }
      IsDirty = false;
      OnPropertyChanged("Title");
    }

    public override void SaveData()
    {
      using (var svc = ServiceFactory.GetService<IFreightManagementService>(SecurityContext.MasterServer.Name))
      {
        DistanceTable = svc.Instance.SaveDistanceTable(DistanceTable);
      }
      IsDirty = false;
      OnPropertyChanged("Title");
    }

    public override bool IsDirty
    {
      get
      {
        return DistanceTable == null ? false : DistanceTable.IsDirty;
      }
      set
      {
        if (DistanceTable == null) return;
        DistanceTable.IsDirty = value;
      }
    }
  }
}

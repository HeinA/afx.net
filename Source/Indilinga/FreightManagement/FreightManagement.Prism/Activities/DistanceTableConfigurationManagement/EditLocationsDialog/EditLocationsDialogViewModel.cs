﻿using Afx.Business.Data;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using FreightManagement.Business;
using Geographics.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Activities.DistanceTableConfigurationManagement.EditLocationsDialog
{
  public class EditLocationsDialogViewModel : MdiDialogViewModel<EditLocationsDialogContext>
  {
    [InjectionConstructor]
    public EditLocationsDialogViewModel(IController controller)
      : base(controller)
    {
    }

    public IEnumerable<LocationItemViewModel> Locations
    {
      get { return ResolveViewModels<LocationItemViewModel, Location>(Cache.Instance.GetObjects<Location>(l => l.GetExtensionObject<DeliveryLocation>() != null && l.GetExtensionObject<DeliveryLocation>().IsWaypoint, l => l.FullName, true)); }
    }
  }
}

﻿using Afx.Prism;
using Afx.Prism.RibbonMdi.Documents;
using FreightManagement.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Activities.DistributionCenterManagement
{
  public class RouteDurationViewModel : ExtensionViewModel<RouteExtension>
  {
    #region Constructors

    [InjectionConstructor]
    public RouteDurationViewModel(IController controller)
      : base(controller)
    {
    }
    
    #endregion
  }
}

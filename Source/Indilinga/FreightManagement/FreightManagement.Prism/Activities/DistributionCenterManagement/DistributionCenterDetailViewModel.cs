﻿using Afx.Business;
using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Activities;
using FreightManagement.Business;
using Geographics.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Activities.DistributionCenterManagement
{
  public partial class DistributionCenterDetailViewModel : ActivityDetailViewModel<DistributionCenter>
  {
    #region Constructors

    [InjectionConstructor]
    public DistributionCenterDetailViewModel(IController controller)
      : base(controller)
    {
    }
    
    #endregion

    public IEnumerable<Location> Locations
    {
      get { return Cache.Instance.GetObjects<Location>(true, l => l.GetExtensionObject<DeliveryLocation>() != null && l.GetExtensionObject<DeliveryLocation>().DeliveryPointType != DeliveryPointType.None && l.GetExtensionObject<DeliveryLocation>().IsWaypoint, l => l.FullName, true); }
    }

    public IEnumerable<OrganizationalUnit> OrganizationalUnits
    {
      get { return Cache.Instance.GetObjects<OrganizationalUnit>(true, ou => ou.UserAssignable, ou => ou.Name, true); }
    }
  }
}

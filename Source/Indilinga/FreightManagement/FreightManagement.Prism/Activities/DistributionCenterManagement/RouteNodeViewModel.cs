﻿using Afx.Prism;
using Afx.Prism.RibbonMdi;
using FreightManagement.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Activities.DistributionCenterManagement
{
  public class RouteNodeViewModel : MdiNavigationTreeNodeViewModel<Route>
  {
    #region Constructors

    [InjectionConstructor]
    public RouteNodeViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    #region string Name

    public string Name
    {
      get
      {
        if (Model == null) return "*** Invalid ***";
        if (string.IsNullOrWhiteSpace(Model.Name)) return "*** Unnamed ***";
        return Model.Name;
      }
    }

    #endregion
  }
}

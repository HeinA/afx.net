﻿using Afx.Business;
using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using FreightManagement.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Activities.DistributionCenterManagement
{
  public partial class DistributionCenterNodeViewModel : MdiNavigationTreeNodeViewModel<DistributionCenter>
  {
    #region Constructors

    [InjectionConstructor]
    public DistributionCenterNodeViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    #region string Name

    public string Name
    {
      get
      {
        if (Model == null) return "*** Invalid ***";
        if (string.IsNullOrWhiteSpace(Model.Name)) return "*** Unnamed ***";
        return Model.Name;
      }
    }

    #endregion

    public IEnumerable<RouteNodeViewModel> Routes
    {
      get { return this.ResolveViewModels<RouteNodeViewModel, Route>(Model.Routes); }
    }

  }
}

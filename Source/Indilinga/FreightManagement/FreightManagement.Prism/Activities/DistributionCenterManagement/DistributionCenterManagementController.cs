﻿using Afx.Business;
using Afx.Business.Activities;
using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Activities;
using FreightManagement.Business;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Activities.DistributionCenterManagement
{
  public partial class DistributionCenterManagementController : MdiContextualActivityController<DistributionCenterManagement, DistributionCenterManagementViewModel>
  {
    #region Constructors

    [InjectionConstructor]
    public DistributionCenterManagementController(IController controller)
      : base(controller)
    {
    }

    #endregion

    #region OnLoaded
    
    protected override void OnLoaded()
    {
      if (DataContext.Data.Count > 0)
      {
        //Focus the first element 
        DistributionCenterNodeViewModel ivm = GetCreateViewModel<DistributionCenterNodeViewModel>(DataContext.Data[0], ViewModel);
        ivm.IsFocused = true;
      }

      RegisterContextRegion(RegionManager.Regions[DistributionCenterManagement.DetailsRegion]);
      RegisterContextRegion(RegionManager.Regions[DistributionCenterManagement.TabRegion]);

      base.OnLoaded();
    }

    #endregion
    
    #region OnContextViewModelChanged
    
    protected override void OnContextViewModelChanged(ISelectableViewModel context)
    {
      if (context.Model is DistributionCenter)
      {
        DistributionCenterDetailViewModel vm = GetCreateViewModel<DistributionCenterDetailViewModel>(context.Model, ViewModel);
        AddDetailsViewModel(vm);
      }

      if (context.Model is Route)
      {
        RouteDetailViewModel vm = GetCreateViewModel<RouteDetailViewModel>(context.Model, ViewModel);
        AddDetailsViewModel(vm);

        RouteDurationViewModel vm2 = GetCreateViewModel<RouteDurationViewModel>(context.Model.GetExtensionObject<RouteExtension>(), ViewModel);
        AddDetailsViewModel(vm2);

        WaypointsViewModel vm1 = GetCreateViewModel<WaypointsViewModel>(context.Model, ViewModel);
        AddTabViewModel(vm1);
      }

      base.OnContextViewModelChanged(context);
    }

    #endregion
    
    #region ExecuteOperation
    
    protected override void ExecuteOperation(Operation op, BusinessObject argument)
    {
      switch (op.Identifier)
      {
        case Operations.AddDistributionCenter:
          {
            CreateRootItem();
          }
          break;

        case Operations.AddRoute:
          {
            DistributionCenter dc = (DistributionCenter)argument;
            Route newRt = new Route();
            dc.Routes.Add(newRt);
            SelectContextViewModel(newRt);
            FocusViewModel<RouteDetailViewModel>(newRt);
          }
          break;
      }

      base.ExecuteOperation(op, argument);
    }
    
    #endregion
  }
}

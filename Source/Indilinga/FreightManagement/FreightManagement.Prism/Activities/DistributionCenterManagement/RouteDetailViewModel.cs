﻿using Afx.Business.Data;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Activities;
using FreightManagement.Business;
using Geographics.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Activities.DistributionCenterManagement
{
  public class RouteDetailViewModel : ActivityDetailViewModel<Route>
  {
    #region Constructors

    [InjectionConstructor]
    public RouteDetailViewModel(IController controller)
      : base(controller)
    {
    }
    
    #endregion

    Collection<Location> mLocationsWithNull = null;
    Collection<Location> LocationsWithNull
    {
      get
      {
        return mLocationsWithNull ?? (mLocationsWithNull = Cache.Instance.GetObjects<Location>(true, (l) =>
          {
            DeliveryLocation dl = l.GetExtensionObject<DeliveryLocation>();
            if (dl != null && dl.IsWaypoint) return true;
            return false;
          }, l => l.FullName, true));
      }
    }

    public IEnumerable<Location> Locations
    {
      get { return LocationsWithNull; }
    }
  }
}

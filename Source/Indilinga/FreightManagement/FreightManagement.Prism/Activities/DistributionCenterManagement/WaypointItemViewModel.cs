﻿using Afx.Prism;
using FreightManagement.Business;
using Geographics.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Activities.DistributionCenterManagement
{
  public class WaypointItemViewModel : SelectableViewModel<Location>
  {
    #region Constructors

    [InjectionConstructor]
    public WaypointItemViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    new WaypointsViewModel Parent
    {
      get { return (WaypointsViewModel)base.Parent; }
    }

    #region bool IsChecked

    public bool IsChecked
    {
      get { return Parent.Model.Waypoints.Contains(Model); }
      set
      {
        if (value)
        {
          if (!Parent.Model.Waypoints.Contains(Model)) Parent.Model.Waypoints.Add(Model);
        }
        else
        {
          if (Parent.Model.Waypoints.Contains(Model)) Parent.Model.Waypoints.Remove(Model);
        }
      }
    }

    #endregion
  }
}

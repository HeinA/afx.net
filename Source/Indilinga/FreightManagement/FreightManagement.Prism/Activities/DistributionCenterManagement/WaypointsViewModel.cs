﻿using Afx.Business.Data;
using Afx.Prism;
using FreightManagement.Business;
using Geographics.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Activities.DistributionCenterManagement
{
  public class WaypointsViewModel : TabViewModel<Route>
  {
    #region Constructors

    [InjectionConstructor]
    public WaypointsViewModel(IController controller)
      : base(controller)
    {
      Title = "Waypoints";
    }
    
    #endregion

    Collection<Location> mLocationsWithoutNull = null;
    Collection<Location> LocationsWithoutNull
    {
      get
      {
        return mLocationsWithoutNull ?? (mLocationsWithoutNull = Cache.Instance.GetObjects<Location>((l) =>
        {
          DeliveryLocation dl = l.GetExtensionObject<DeliveryLocation>();
          if (dl != null && dl.IsWaypoint) return true;
          return false;
        }, l => l.FullName, true));
      }
    }

    public IEnumerable<WaypointItemViewModel> WaypointItemViewModels
    {
      get
      {
        return ResolveViewModels<WaypointItemViewModel, Location>(LocationsWithoutNull);
      }
    }
  }
}

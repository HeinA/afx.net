﻿using Afx.Prism;
using Afx.Prism.RibbonMdi.Documents;
using FreightManagement.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Activities.WaybillFlagGroupManagement
{
  public class ServiceDurationViewModel : ExtensionViewModel<WaybillFlagServiceDuration>
  {
    #region Constructors

    [InjectionConstructor]
    public ServiceDurationViewModel(IController controller)
      : base(controller)
    {
    }
    
    #endregion
  }
}

﻿using Afx.Business.Activities;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Business.Service;
using FreightManagement.Business;
using FreightManagement.Business.Service;
using Afx.Prism.RibbonMdi.Activities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Activities.WaybillFlagGroupManagement
{
  public partial class WaybillFlagGroupManagement : ContextualActivityContext<WaybillFlagGroup>
  {
    public const string DetailsRegion = "DetailsRegion";
    public const string TabRegion = "TabRegion";


    public WaybillFlagGroupManagement()
    {
    }

    public WaybillFlagGroupManagement(ContextualActivity activity)
      : base(activity)
    {
    }
  }
}

﻿using Afx.Business.Security;
using Afx.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Activities;
using FreightManagement.Business;

namespace FreightManagement.Prism.Activities.WaybillFlagGroupManagement
{
  public partial class WaybillFlagGroupManagementViewModel : MdiContextualActivityViewModel<WaybillFlagGroupManagement>
  {
    #region Constructors

    [InjectionConstructor]
    public WaybillFlagGroupManagementViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion
  }
}

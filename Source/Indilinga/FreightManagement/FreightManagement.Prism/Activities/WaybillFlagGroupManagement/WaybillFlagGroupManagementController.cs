﻿using Afx.Business;
using Afx.Business.Activities;
using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Activities;
using FreightManagement.Business;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Activities.WaybillFlagGroupManagement
{
  public partial class WaybillFlagGroupManagementController : MdiContextualActivityController<WaybillFlagGroupManagement, WaybillFlagGroupManagementViewModel>
  {
    #region Constructors

    [InjectionConstructor]
    public WaybillFlagGroupManagementController(IController controller)
      : base(controller)
    {
    }

    #endregion

    #region OnLoaded
    
    protected override void OnLoaded()
    {
      if (DataContext.Data.Count > 0)
      {
        //Focus the first element 
        WaybillFlagGroupNodeViewModel ivm = GetCreateViewModel<WaybillFlagGroupNodeViewModel>(DataContext.Data[0], ViewModel);
        ivm.IsFocused = true;
      }

      RegisterContextRegion(RegionManager.Regions[WaybillFlagGroupManagement.DetailsRegion]);
      RegisterContextRegion(RegionManager.Regions[WaybillFlagGroupManagement.TabRegion]);

      base.OnLoaded();
    }

    #endregion
    
    #region OnContextViewModelChanged
    
    protected override void OnContextViewModelChanged(ISelectableViewModel context)
    {
      if (context.Model is WaybillFlagGroup)
      {
        WaybillFlagGroupDetailViewModel vm = GetCreateViewModel<WaybillFlagGroupDetailViewModel>(context.Model, ViewModel);
        AddDetailsViewModel(vm);
      }

      if (context.Model is WaybillFlag)
      {
        var wf = (WaybillFlag)context.Model;

        WaybillFlagDetailViewModel vm = GetCreateViewModel<WaybillFlagDetailViewModel>(context.Model, ViewModel);
        AddDetailsViewModel(vm);

        if (wf.Owner.IsFlagged(WaybillFlagGroupFlags.CostingFlagGroup))
        {
          var vm1 = GetCreateViewModel<ServiceDurationViewModel>(wf.GetExtensionObject<WaybillFlagServiceDuration>(), ViewModel);
          AddDetailsViewModel(vm1);
        }
      }

      base.OnContextViewModelChanged(context);
    }

    #endregion
    
    #region ExecuteOperation
    
    protected override void ExecuteOperation(Operation op, BusinessObject argument)
    {
      switch (op.Identifier)
      {
        case Operations.AddGroup:
          CreateRootItem();
          break;

        case Operations.AddFlag:
          {
            var fg = (WaybillFlagGroup)argument;
            var f = new WaybillFlag();
            fg.Flags.Add(f);
            SelectContextViewModel(f);
            FocusViewModel<WaybillFlagDetailViewModel>(f);
          }
          break;
      }

      base.ExecuteOperation(op, argument);
    }
    
    #endregion
  }
}

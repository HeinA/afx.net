﻿using Afx.Prism;
using Afx.Prism.RibbonMdi;
using FreightManagement.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Activities.WaybillFlagGroupManagement
{
  public class WaybillFlagNodeViewModel : MdiNavigationTreeNodeViewModel<WaybillFlag>
  {
    #region Constructors

    [InjectionConstructor]
    public WaybillFlagNodeViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    #region string Text

    public string Text
    {
      get
      {
        if (Model == null) return string.Empty;
        if (string.IsNullOrWhiteSpace(Model.Text)) return "*** Unnamed ***";
        return Model.Text;
      }
    }

    #endregion
  }
}

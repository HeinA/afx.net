﻿using Afx.Prism;
using Afx.Prism.RibbonMdi.Activities;
using FreightManagement.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Activities.WaybillFlagGroupManagement
{
  public class WaybillFlagDetailViewModel : ActivityDetailViewModel<WaybillFlag>
  {
    #region Constructors

    [InjectionConstructor]
    public WaybillFlagDetailViewModel(IController controller)
      : base(controller)
    {
    }
    
    #endregion
  }
}

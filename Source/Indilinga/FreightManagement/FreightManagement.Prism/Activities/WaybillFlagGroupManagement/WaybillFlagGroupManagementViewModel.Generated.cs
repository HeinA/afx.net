﻿using Afx.Business.Security;
using Afx.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Afx.Prism.RibbonMdi.Activities;
using FreightManagement.Business;
using Microsoft.Practices.Prism;

namespace FreightManagement.Prism.Activities.WaybillFlagGroupManagement
{
  //**************************************************************************************************************
  //**************************************************************************************************************
  //*                                                                                                            *
  //*     This code is auto-generated.  Do not place any cutom code here, it will be lost on re-generation.      *
  //*                                                                                                            *
  //**************************************************************************************************************
  //**************************************************************************************************************
  public partial class WaybillFlagGroupManagementViewModel
  {
    public IEnumerable<WaybillFlagGroupNodeViewModel> Items
    {
      get
      {
        if (Model == null) return null;
        return this.ResolveViewModels<WaybillFlagGroupNodeViewModel, WaybillFlagGroup>(Model.Data);
      }
    }
  }
}

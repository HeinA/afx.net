﻿using FreightManagement.Business;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.Composition;
using Microsoft.Practices.Prism;

namespace FreightManagement.Prism.Activities.WaybillFlagGroupManagement
{
  //**************************************************************************************************************
  //**************************************************************************************************************
  //*                                                                                                            *
  //*     This code is auto-generated.  Do not place any cutom code here, it will be lost on re-generation.      *
  //*                                                                                                            *
  //**************************************************************************************************************
  //**************************************************************************************************************
  [Export("Controller:" + global::FreightManagement.Prism.Activities.WaybillFlagGroupManagement.WaybillFlagGroupManagement.Key)]
  public partial class WaybillFlagGroupManagementController
  {
    void CreateRootItem()
    {
      WaybillFlagGroup o = new WaybillFlagGroup();
      DataContext.Data.Add(o);
      SelectContextViewModel(o);
      FocusViewModel<WaybillFlagGroupDetailViewModel>(o);
    }

    public void AddDetailsViewModel(object viewModel)
    {
      RegionManager.Regions[WaybillFlagGroupManagement.DetailsRegion].Add(viewModel);
    }

    public void AddTabViewModel(object viewModel)
    {
      IRegion tabRegion = RegionManager.Regions[WaybillFlagGroupManagement.TabRegion];
      tabRegion.Add(viewModel);
      IActiveAware aw = viewModel as IActiveAware;
      if (aw != null && aw.IsActive) tabRegion.Activate(viewModel);
    }
  }
}

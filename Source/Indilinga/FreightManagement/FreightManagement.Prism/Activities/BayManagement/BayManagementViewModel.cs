﻿using Afx.Business.Security;
using Afx.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Activities;
using FreightManagement.Business;

namespace FreightManagement.Prism.Activities.BayManagement
{
  public partial class BayManagementViewModel : MdiSimpleActivityViewModel<BayManagement>
  {
    #region Constructors

    [InjectionConstructor]
    public BayManagementViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion
  }
}

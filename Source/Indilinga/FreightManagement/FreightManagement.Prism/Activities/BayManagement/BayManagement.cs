﻿using Afx.Business.Activities;
using FreightManagement.Business;
using FreightManagement.Business.Service;
using Afx.Prism.RibbonMdi.Activities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Activities.BayManagement
{
  public partial class BayManagement : SimpleActivityContext<Bay>
  {
    public BayManagement()
    {
    }

    public BayManagement(Activity activity)
      : base(activity)
    {
    }
  }
}

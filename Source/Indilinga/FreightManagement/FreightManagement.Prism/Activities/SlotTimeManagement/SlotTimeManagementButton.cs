﻿using Afx.Business.Activities;
using Afx.Business.Data;
using Afx.Prism.RibbonMdi;
using Afx.Prism.RibbonMdi.Ribbon;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Activities.SlotTimeManagement
{
  [Export(typeof(IRibbonItem))]
  public class SlotTimeManagementButton : RibbonButtonViewModel
  {
    public override string TabName
    {
      get { return FreightManagementTab.TabName; }
    }

    public override string GroupName
    {
      get { return ConfigurationGroup.GroupName; }
    }

    public override string ItemIdentifier
    {
      get { return "SlotTimeManagement"; }
    }

    public override int Index
    {
      get { return 5; }
    }

    #region Activity Activity

    public Activity Activity
    {
      get { return Cache.Instance.GetObject<Activity>(SlotTimeManagement.Key); }
    }

    #endregion

    public bool IsEnabled
    {
      get
      {
        try
        {
          if (Activity == null) return false;
          return Activity.CanView;
        }
        catch
        {
          return false;
        }
      }
    }

    protected override void OnExecute()
    {
      MdiApplicationController.Current.ExecuteActivity(Activity);
    }
  }
}

﻿using FreightManagement.Business;
using FreightManagement.Business.Service;
using Afx.Business.Service;
using Afx.Prism.RibbonMdi.Activities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.Composition;
using Afx.Business.Security;

namespace FreightManagement.Prism.Activities.SlotTimeManagement
{
  //**************************************************************************************************************
  //**************************************************************************************************************
  //*                                                                                                            *
  //*     This code is auto-generated.  Do not place any cutom code here, it will be lost on re-generation.      *
  //*                                                                                                            *
  //**************************************************************************************************************
  //**************************************************************************************************************
  [Export("Context:" + SlotTimeManagement.Key)]
  public partial class SlotTimeManagement
  {
    public const string Key = "{571d625c-6afe-46a0-b54d-761de1abb301}";

    public override void LoadData()
    {
      using (var svc = ProxyFactory.GetService<IFreightManagementService>(SecurityContext.MasterServer.Name))
      {
        Data = svc.LoadSlotTimeCollection();
      }
      base.LoadData();
    }

    public override void SaveData()
    {
      using (var svc = ProxyFactory.GetService<IFreightManagementService>(SecurityContext.MasterServer.Name))
      {
        Data = svc.SaveSlotTimeCollection(Data);
      }
      base.SaveData();
    }
  }
}

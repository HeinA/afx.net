﻿using Afx.Business.Activities;
using FreightManagement.Business;
using FreightManagement.Business.Service;
using Afx.Prism.RibbonMdi.Activities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Activities.SlotTimeManagement
{
  public partial class SlotTimeManagement : SimpleActivityContext<SlotTime>
  {
    public SlotTimeManagement()
    {
    }

    public SlotTimeManagement(Activity activity)
      : base(activity)
    {
    }
  }
}

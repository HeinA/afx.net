﻿using Afx.Business;
using Afx.Business.Activities;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Activities;
using FleetManagement.Business;
using FreightManagement.Business;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Activities.FloorStatusManagement
{
  public partial class FloorStatusManagementController : MdiCustomActivityController<FloorStatusManagement, FloorStatusManagementViewModel>
  {
    #region Constructors

    [InjectionConstructor]
    public FloorStatusManagementController(IController controller)
      : base(controller)
    {
    }

    #endregion

    #region OnLoaded
    
    protected override void OnLoaded()
    {
      ViewModel.IsFocused = true;
      base.OnLoaded();
      //HookWaybills();
    }

    #endregion
    
    #region ExecuteOperation
    
    protected override void ExecuteOperation(Operation op, BusinessObject argument)
    {

      base.ExecuteOperation(op, argument);
    }
    
    #endregion

    #region Hooks

    protected override void LoadDataContext()
    {
      UnhookWaybills();
      base.LoadDataContext();
      DataContext.FloorStatusManagementController = this;
      HookWaybills();
    }

    private void HookWaybills()
    {
      if (DataContext.Waybills == null) return;

      foreach (var w in DataContext.Waybills)
      {
        w.CompositionChanged += w_CompositionChanged;
      }
    }

    private void UnhookWaybills()
    {
      if (DataContext.Waybills == null) return;

      foreach (var w in DataContext.Waybills)
      {
        w.CompositionChanged -= w_CompositionChanged;
      }
    }

    //protected override void OnDataContextPropertyChanged(string propertyName)
    //{
    //  switch (propertyName)
    //  {
    //    case "DistributionCenter":
    //      UnhookWaybills();
    //      HookWaybills();
    //      break;
    //  }
    //  base.OnDataContextPropertyChanged(propertyName);
    //}

    void w_CompositionChanged(object sender, CompositionChangedEventArgs e)
    {
      Collection<VehicleSummary> vsCol = new Collection<VehicleSummary>();

      switch (e.PropertyName)
      {
        case WaybillFloorStatus.VehicleProperty:
          foreach (var w in DataContext.Waybills.Where(w1 => !BusinessObject.IsNull(w1.Vehicle)))
          {
            VehicleSummary vs = vsCol.Where(vs1 => vs1.Vehicle.Equals(w.Vehicle)).FirstOrDefault();
            if (vs == null)
            {
              vs = new VehicleSummary() { Vehicle = w.Vehicle };
              vsCol.Add(vs);
            }

            vs.Waybills++;
            vs.Items += w.Items;
            vs.Weight += w.OnFloor;
            vs.Volume += w.Volume;
            vs.DeclaredValue += w.DeclaredValue;
            vs.Income += w.ChargeTotal;
          }
          DataContext.VehicleSummary = vsCol;
          break;

        case WaybillFloorStatus.IsCheckedProperty:
          int waybills = 0;
          int items = 0;
          decimal weight = 0;
          decimal volume = 0;
          decimal income = 0;
          decimal value = 0;
          foreach (var w in DataContext.Waybills.Where(w1 => w1.IsChecked))
          {
            waybills++;
            items += w.Items;
            weight += w.OnFloor;
            volume += w.Volume;
            value += w.DeclaredValue;
            income += w.ChargeTotal;
          }

          DataContext.SelectedWaybills = waybills;
          DataContext.SelectedItems = items;
          DataContext.SelectedWeight = weight;
          DataContext.SelectedVolume = volume;
          DataContext.SelectedValue = value;
          DataContext.SelectedIncome = income;
          break;
      }

    }

    #endregion
  }
}
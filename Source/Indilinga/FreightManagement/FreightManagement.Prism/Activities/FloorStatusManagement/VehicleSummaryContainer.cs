﻿using Afx.Business;
using FleetManagement.Business;
using FreightManagement.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Activities.FloorStatusManagement
{
  public class VehicleSummaryContainer : BusinessObject
  {
    public VehicleSummaryContainer(VehicleSummary vehicleSummary)
    {
      VehicleSummary = vehicleSummary;
    }

    #region VehicleSummary VehicleSummarySummary

    public const string VehicleSummaryProperty = "VehicleSummary";
    VehicleSummary mVehicleSummary;
    public VehicleSummary VehicleSummary
    {
      get { return mVehicleSummary; }
      set
      {
        SetProperty(ref mVehicleSummary, value);
      }
    }

    #endregion

    #region Vehicle Vehicle

    public const string VehicleProperty = "Vehicle";
    Vehicle mVehicle;
    public Vehicle Vehicle
    {
      get { return VehicleSummary.Vehicle; }
    }

    #endregion

    #region IEnumerable<WaybillFloorStatus> Waybills

    public const string WaybillsProperty = "Waybills";
    IEnumerable<WaybillFloorStatus> mWaybills;
    public IEnumerable<WaybillFloorStatus> Waybills
    {
      get { return VehicleSummary.Waybills; }
    }

    #endregion


    #region "Totals"

    #region int TotalManifests

    public int TotalManifests
    {
      get { return mWaybills.Count(); }
    }

    #endregion

    #region int TotalWaybills

    public int TotalWaybills
    {
      get { return mWaybills.Count(); }
    }

    #endregion

    #region int TotalWaybillItems

    public int TotalWaybillItems
    {
      get { return mWaybills.Sum(m => m.Items); }
    }

    #endregion

    #region decimal TotalWaybillWeights

    public decimal TotalWaybillWeights
    {
      get { return mWaybills.Sum(m => m.OnFloor); }
    }

    #endregion

    #region decimal TotalWaybillWeightsVolume

    public decimal TotalWaybillWeightsVolume
    {
      get { return mWaybills.Sum(m => m.Volume); }
    }

    #endregion

    #region decimal TotalWaybillChargesDocumentation

    public decimal TotalWaybillChargesDocumentation
    {
      get { return mWaybills.Sum(m => m.ChargeDocumentation); }
    }

    #endregion

    #region decimal TotalWaybillChargesTransport

    public decimal TotalWaybillChargesTransport
    {
      get { return mWaybills.Sum(m => m.ChargeTransport); }
    }

    #endregion

    #region decimal TotalWaybillChargesCollectionDelivery

    public decimal TotalWaybillChargesCollectionDelivery
    {
      get { return mWaybills.Sum(m => m.ChargeCollectionDelivery); }
    }

    #endregion

    #region decimal TotalWaybillChargesVat

    public decimal TotalWaybillChargesVat
    {
      get { return mWaybills.Sum(m => m.ChargeVat); }
    }

    #endregion

    #region decimal TotalWaybillChargesOther

    public decimal TotalWaybillChargesOther
    {
      get { return mWaybills.Sum(m => m.ChargeOther); }
    }

    #endregion

    #region decimal TotalIncome

    public const string TotalIncomeProperty = "TotalIncome";
    public decimal TotalIncome
    {
      get { return TotalWaybillChargesDocumentation + TotalWaybillChargesCollectionDelivery + TotalWaybillChargesTransport + TotalWaybillChargesOther + TotalWaybillChargesVat; }
    }

    #endregion

    #endregion
  }
}

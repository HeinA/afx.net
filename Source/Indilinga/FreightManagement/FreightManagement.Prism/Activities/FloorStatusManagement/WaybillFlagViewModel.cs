﻿using Afx.Prism;
using FreightManagement.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Activities.FloorStatusManagement
{
  public class WaybillFlagViewModel : ViewModel<WaybillFlag>
  {
    #region Constructors

    [InjectionConstructor]
    public WaybillFlagViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    [Dependency]
    public FloorStatusManagementController FloorStatusManagementController { get; set; }

    #region bool IsChecked

    public const string IsCheckedProperty = "IsChecked";
    bool mIsChecked;
    public bool IsChecked
    {
      get { return mIsChecked; }
      set
      {
        if (SetProperty<bool>(ref mIsChecked, value))
        {
          FloorStatusManagementController.DataContext.FilterFlags(FloorStatusManagementController.ViewModel.Flags);
          //if (mIsChecked) ((FloorStatusManagementViewModel)Parent).SelectUnflagged = false;
        }
      }
    }

    #endregion
  }
}

﻿using Afx.Prism;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Activities.FloorStatusManagement
{
  public class VehicleSummaryViewModel : ViewModel<VehicleSummary>
  {
    #region Constructors

    [InjectionConstructor]
    public VehicleSummaryViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    protected override void OnModelPropertyChanged(string propertyName)
    {
      switch (propertyName)
      {
        case "IsChecked":
          //OnPropertyChanged("Routes");
          break;
      }

      base.OnModelPropertyChanged(propertyName);
    }
  }
}

﻿using Afx.Business.Activities;
using FreightManagement.Business;
using FreightManagement.Business.Service;
using Afx.Prism.RibbonMdi.Activities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Afx.Business.Service;
using System.Collections.ObjectModel;
using Afx.Business.Security;
using Afx.Business.Data;
using Afx.Business.Collections;
using AccountManagement.Business;
using Microsoft.Practices.Unity;
using FleetManagement.Business;
using System.Windows;
using Afx.Business;

namespace FreightManagement.Prism.Activities.FloorStatusManagement
{
  public partial class FloorStatusManagement : CustomActivityContext
  {
    public FloorStatusManagement()
    {
    }

    public FloorStatusManagement(Activity activity)
      : base(activity)
    {
    }

    public override bool IsDirty
    {
      get { return false; }
      set { }
    }

    public FloorStatusManagementController FloorStatusManagementController { get; set; }

    #region string InvoiceTo

    public const string InvoiceToProperty = "InvoiceTo";
    string mInvoiceTo;
    public string InvoiceTo
    {
      get { return mInvoiceTo; }
      set
      {
        if (SetProperty<string>(ref mInvoiceTo, value))
        {
          foreach (var rc in Routes)
          {
            rc.InvoiceTo = value;
          }
        }
      }
    }

    #endregion

    #region string Consigner

    public const string ConsignerProperty = "Consigner";
    string mConsigner;
    public string Consigner
    {
      get { return mConsigner; }
      set
      {
        if (SetProperty<string>(ref mConsigner, value))
        {
          foreach (var rc in Routes)
          {
            rc.Consigner = value;
          }
        }
      }
    }

    #endregion

    #region bool ShowPending

    public const string ShowPendingProperty = "ShowPending";
    bool mShowPending;
    public bool ShowPending
    {
      get { return mShowPending; }
      set
      {
        if (SetProperty<bool>(ref mShowPending, value))
        {
          foreach (var ac in Routes)
          {
            ac.ShowPending = value;
          }
        }
      }
    }

    #endregion

    #region Bay Bay

    public const string BayProperty = "Bay";
    Bay mBay = Setting.GetSetting<DefaultBaySetting>().FloorStatusBay;
    public Bay Bay
    {
      get { return mBay; }
      set
      {
        if (SetProperty<Bay>(ref mBay, value))
        {
          foreach (var rc in Routes)
          {
            rc.Bay = value;
          }
        }
      }
    }

    #endregion

    #region Collection<WaybillFloorStatus> Waybills

    public const string WaybillsProperty = "Waybills";
    BasicCollection<WaybillFloorStatus> mWaybills;
    public BasicCollection<WaybillFloorStatus> Waybills
    {
      get { return mWaybills; }
      set
      {
        if (SetProperty<BasicCollection<WaybillFloorStatus>>(ref mWaybills, value))
        {
          Collection<RouteContainer> routes = new Collection<RouteContainer>();
          foreach (var route in Waybills.Select(c => c.Route).Distinct().OrderBy(r => r.Name))
          {
            if (Waybills.Where(c => c.Route.Equals(route)).Count() > 0)
            {
              routes.Add(new RouteContainer(route, Waybills.Where(c => c.Route.Equals(route))) { InvoiceTo = this.InvoiceTo, Bay = this.Bay });
            }
          }
          Routes = routes;
          foreach (var rc in Routes)
          {
            rc.SelectBy = SelectBy;
          }
        }
      }
    }

    #endregion

    #region Collection<RouteContainer> Routes

    Collection<RouteContainer> mRoutes;
    public Collection<RouteContainer> Routes
    {
      get { return mRoutes; }
      set { SetProperty<Collection<RouteContainer>>(ref mRoutes, value); }
    }

    #endregion

    #region Collection<VehicleSummary> VehicleSummary

    public const string VehicleSummaryProperty = "VehicleSummary";
    Collection<VehicleSummary> mVehicleSummary;
    public Collection<VehicleSummary> VehicleSummary
    {
      get { return mVehicleSummary; }
      set { SetProperty<Collection<VehicleSummary>>(ref mVehicleSummary, value); }
    }

    #endregion

    #region DistributionCenter DistributionCenter

    public const string DistributionCenterProperty = "DistributionCenter";
    DistributionCenter mDistributionCenter = Cache.Instance.GetObjects<DistributionCenter>(dc => dc.OrganizationalUnit.Equals(SecurityContext.OrganizationalUnit)).FirstOrDefault();
    public DistributionCenter DistributionCenter
    {
      get { return mDistributionCenter; }
      set
      {
        if (SetProperty<DistributionCenter>(ref mDistributionCenter, value))
        {
          FloorStatusManagementController.Refresh(true);
          //LoadWaybills();
        }
      }
    }

    #endregion

    #region AccountReference Account

    public const string AccountProperty = "Account";
    AccountReference mAccount;
    /// <summary>
    /// Account reference object property.
    /// </summary>
    public AccountReference Account
    {
      get { return mAccount; }
      set
      {
        if (SetProperty<AccountReference>(ref mAccount, value))
        {
          LoadWaybills();
        }
      }
    }

    #endregion

    #region string SelectBy

    public const string SelectByProperty = "SelectBy";
    string mSelectBy = FloorStatusManagementViewModel.SelectWithCheck;
    public string SelectBy
    {
      get { return mSelectBy; }
      set
      {
        if (SetProperty(ref mSelectBy, value))
        {
          foreach (var rc in Routes)
          {
            rc.SelectBy = SelectBy;
          }
        }
      }
    }

    #endregion

    #region int SelectedWaybills

    public const string SelectedWaybillsProperty = "SelectedWaybills";
    int mSelectedWaybills;
    public int SelectedWaybills
    {
      get { return mSelectedWaybills; }
      set { SetProperty<int>(ref mSelectedWaybills, value); }
    }

    #endregion

    #region int SelectedItems

    public const string SelectedItemsProperty = "SelectedItems";
    int mSelectedItems;
    public int SelectedItems
    {
      get { return mSelectedItems; }
      set { SetProperty<int>(ref mSelectedItems, value); }
    }

    #endregion

    #region decimal SelectedWeight

    public const string SelectedWeightProperty = "SelectedWeight";
    decimal mSelectedWeight;
    public decimal SelectedWeight
    {
      get { return mSelectedWeight; }
      set { SetProperty<decimal>(ref mSelectedWeight, value); }
    }

    #endregion

    #region decimal SelectedVolume

    public const string SelectedVolumeProperty = "SelectedVolume";
    decimal mSelectedVolume;
    public decimal SelectedVolume
    {
      get { return mSelectedVolume; }
      set { SetProperty<decimal>(ref mSelectedVolume, value); }
    }

    #endregion

    #region decimal SelectedValue

    public const string SelectedValueProperty = "SelectedValue";
    decimal mSelectedValue;
    public decimal SelectedValue
    {
      get { return mSelectedValue; }
      set { SetProperty<decimal>(ref mSelectedValue, value); }
    }

    #endregion

    #region decimal SelectedIncome

    public const string SelectedIncomeProperty = "SelectedIncome";
    decimal mSelectedIncome;
    public decimal SelectedIncome
    {
      get { return mSelectedIncome; }
      set { SetProperty<decimal>(ref mSelectedIncome, value); }
    }

    #endregion

    public void FilterFlags(IEnumerable<WaybillFlagViewModel> flags)
    {
      foreach (var rc in Routes)
      {
        rc.FilterFlags(flags);
      }
    }

    public override void LoadData()
    {
      LoadWaybills();
      base.LoadData();
    }

    void LoadWaybills()
    {
      using (var svc = ProxyFactory.GetService<IFreightManagementService>(SecurityContext.ServerName))
      {
        Waybills = svc.LoadWaybillFloorStatus(DistributionCenter.Id);
      }
    }

  }
}

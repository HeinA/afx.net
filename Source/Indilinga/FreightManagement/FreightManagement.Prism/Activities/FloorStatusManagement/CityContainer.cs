﻿using Afx.Business;
using Afx.Business.Data;
using FleetManagement.Business;
using FreightManagement.Business;
using Geographics.Business;
using Microsoft.Practices.Prism.Commands;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace FreightManagement.Prism.Activities.FloorStatusManagement
{
  public class CityContainer : BusinessObject
  {
    public CityContainer(Location city, IEnumerable<WaybillFloorStatus> collections)
    {
      City = city;
      Waybills = collections;
    }

    public void FilterFlags(IEnumerable<WaybillFlagViewModel> flags)
    {
      foreach (var ac in Accounts)
      {
        ac.FilterFlags(flags);
      }
      OnPropertyChanged(VisibilityProperty);
      OnPropertyChanged(ItemsProperty);
      OnPropertyChanged(OnFloorProperty);
      OnPropertyChanged(PendingProperty);
    }

    #region bool IsExpanded

    public const string IsExpandedProperty = "IsExpanded";
    bool mIsExpanded;
    public bool IsExpanded
    {
      get { return mIsExpanded; }
      set { SetProperty<bool>(ref mIsExpanded, value); }
    }

    #endregion

    public void ExpandAll()
    {
      IsExpanded = true;
      foreach (var x in Accounts) x.ExpandAll();
    }

    #region string InvoiceTo

    public const string InvoiceToProperty = "InvoiceTo";
    string mInvoiceTo = string.Empty;
    public string InvoiceTo
    {
      get { return mInvoiceTo; }
      set
      {
        if (SetProperty<string>(ref mInvoiceTo, value))
        {
          foreach (var ac in Accounts)
          {
            ac.InvoiceTo = value;
          }
          OnPropertyChanged(VisibilityProperty);
          OnPropertyChanged(ItemsProperty);
          OnPropertyChanged(OnFloorProperty);
          OnPropertyChanged(PendingProperty);
        }
      }
    }

    #endregion

    #region string Consigner

    public const string ConsignerProperty = "Consigner";
    string mConsigner = string.Empty;
    public string Consigner
    {
      get { return mConsigner; }
      set
      {
        if (SetProperty<string>(ref mConsigner, value))
        {
          foreach (var ac in Accounts)
          {
            ac.Consigner = value;
          }
          OnPropertyChanged(VisibilityProperty);
          OnPropertyChanged(ItemsProperty);
          OnPropertyChanged(OnFloorProperty);
          OnPropertyChanged(PendingProperty);
        }
      }
    }

    #endregion

    #region Bay Bay

    public const string BayProperty = "Bay";
    Bay mBay;
    public Bay Bay
    {
      get { return mBay; }
      set
      {
        if (SetProperty<Bay>(ref mBay, value))
        {
          foreach (var ac in Accounts)
          {
            ac.Bay = value;
          }
          OnPropertyChanged(VisibilityProperty);
          OnPropertyChanged(ItemsProperty);
          OnPropertyChanged(OnFloorProperty);
          OnPropertyChanged(PendingProperty);
        }
      }
    }

    #endregion

    #region bool ShowPending

    public const string ShowPendingProperty = "ShowPending";
    bool mShowPending;
    public bool ShowPending
    {
      get { return mShowPending; }
      set
      {
        if (SetProperty<bool>(ref mShowPending, value))
        {
          foreach (var ac in Accounts)
          {
            ac.ShowPending = value;
          }
          OnPropertyChanged(VisibilityProperty);
          OnPropertyChanged(ItemsProperty);
          OnPropertyChanged(OnFloorProperty);
          OnPropertyChanged(PendingProperty);
        }
      }
    }

    #endregion

    #region string SelectBy

    public const string SelectByProperty = "SelectBy";
    string mSelectBy = FloorStatusManagementViewModel.SelectWithCheck;
    public string SelectBy
    {
      get { return mSelectBy; }
      set
      {
        if (SetProperty(ref mSelectBy, value))
        {
          foreach (var ac in Accounts)
          {
            ac.SelectBy = SelectBy;
          }
        }
      }
    }

    #endregion

    #region Visibility Visibility

    public const string VisibilityProperty = "Visibility";
    public Visibility Visibility
    {
      get
      {
        return Accounts.Count(ac => ac.Visibility == System.Windows.Visibility.Visible) == 0 ? Visibility.Collapsed : Visibility.Visible;
      }
    }

    #endregion

    #region Vehicle Vehicle

    public const string VehicleProperty = "Vehicle";
    Vehicle mVehicle;
    public Vehicle Vehicle
    {
      get { return mVehicle; }
      set
      {
        if (SetProperty(ref mVehicle, value))
        {
          foreach (AccountContainer c in Accounts)
          {
            c.Vehicle = Vehicle;
          }
        }
      }
    }

    #endregion

    #region bool Checked

    public const string CheckedProperty = "Checked";
    bool mChecked;
    public bool Checked
    {
      get { return mChecked; }
      set
      {
        if (SetProperty<bool>(ref mChecked, value))
        {
          foreach (AccountContainer c in Accounts.Where(c1 => c1.Visibility == System.Windows.Visibility.Visible))
          {
            c.Checked = Checked;
          }
        }
      }
    }

    #endregion

    public IEnumerable<Vehicle> Vehicles
    {
      get { return Cache.Instance.GetObjects<Vehicle>(true, v => v.GetExtensionObject<VehicleFreightDetails>().ShowInFloorStatusLoadList, v => v.Text, true); }
    }

    #region Location City

    public const string CityProperty = "City";
    Location mCity;
    public Location City
    {
      get { return mCity; }
      set { mCity = value; }
    }

    #endregion

    #region IEnumerable<WaybillFloorStatus> Waybills

    public const string WaybillsProperty = "Waybills";
    IEnumerable<WaybillFloorStatus> mWaybills;
    public IEnumerable<WaybillFloorStatus> Waybills
    {
      get { return mWaybills; }
      private set
      {
        mWaybills = value;

        Collection<AccountContainer> accounts = new Collection<AccountContainer>();
        foreach (var account in Waybills.Select(c => c.Account).Distinct().OrderBy(a => a.Name))
        {
          accounts.Add(new AccountContainer(account, Waybills.Where(c => c.Account.Equals(account)).OrderBy(c => c.Account.Name)));
        }
        Accounts = accounts;
      }
    }

    #endregion

    #region IEnumerable<AccountContainer> Accounts

    public const string AccountsProperty = "Accounts";
    IEnumerable<AccountContainer> mAccounts;
    public IEnumerable<AccountContainer> Accounts
    {
      get { return mAccounts; }
      private set { mAccounts = value; }
    }

    #endregion

    #region int Items

    public const string ItemsProperty = "Items";
    public int Items
    {
      get
      {
        if (Visibility == System.Windows.Visibility.Collapsed) return 0;
        int items = 0;
        foreach (var ac in Accounts)
        {
          items += ac.Items;
        }
        return items;
      }
    }

    #endregion

    #region decimal OnFloor

    public const string OnFloorProperty = "OnFloor";
    public decimal OnFloor
    {
      get
      {
        if (Visibility == System.Windows.Visibility.Collapsed) return 0;
        decimal onFloor = 0;
        foreach (var ac in Accounts)
        {
          onFloor += ac.OnFloor;
        }
        return onFloor;
      }
    }

    #endregion

    #region decimal Pending

    public const string PendingProperty = "Pending";
    public decimal Pending
    {
      get
      {
        if (Visibility == System.Windows.Visibility.Collapsed) return 0;
        decimal pending = 0;
        foreach (var ac in Accounts)
        {
          pending += ac.Pending;
        }
        return pending;
      }
    }

    #endregion
  }
}

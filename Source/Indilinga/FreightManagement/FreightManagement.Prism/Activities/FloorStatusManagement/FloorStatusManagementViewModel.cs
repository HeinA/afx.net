﻿using Afx.Business.Security;
using Afx.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Activities;
using FreightManagement.Business;
using Afx.Business.Data;
using AccountManagement.Business;
using FleetManagement.Business;
using Microsoft.Practices.Prism.Commands;

namespace FreightManagement.Prism.Activities.FloorStatusManagement
{
  public partial class FloorStatusManagementViewModel : MdiCustomActivityViewModel<FloorStatusManagement>
  {
    #region Constructors

    [InjectionConstructor]
    public FloorStatusManagementViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    public const string SelectWithCheck = "With Check";
    public const string SelectWithVehicle = "With Vehicle";

    #region IEnumerable<DistributionCenter> DistributionCenters

    public IEnumerable<DistributionCenter> DistributionCenters
    {
      get { return Cache.Instance.GetObjects<DistributionCenter>(true, dc => dc.Name, true); }
    }

    #endregion

    public IEnumerable<Bay> Bays
    {
      get { return Cache.Instance.GetObjects<Bay>(true, b => b.Text, true); }
    }

    #region IEnumerable<Route> Routes

    public IEnumerable<Route> Routes
    {
      get { return Cache.Instance.GetObjects<Route>(true, r => r.Owner.Equals(this.Model.DistributionCenter), r => r.Name, true); }
    }

    #endregion

    #region IEnumerable<string> SelectByList

    public IEnumerable<string> SelectByList
    {
      get
      {
        yield return SelectWithVehicle;
        yield return SelectWithCheck;
      }
    }

    #endregion

    #region IEnumerable<Vehicle> Vehicles

    public IEnumerable<Vehicle> Vehicles
    {
      get { return Cache.Instance.GetObjects<Vehicle>(true, v => v.GetExtensionObject<VehicleFreightDetails>().ShowInFloorStatusLoadList, v => v.Text, true); }
    }

    #endregion

    public IEnumerable<WaybillFlagViewModel> Flags
    {
      get { return ResolveViewModels<WaybillFlagViewModel, WaybillFlag>(Cache.Instance.GetObjects<WaybillFlag>((r) => r.Text, true)); }
    }

    #region DelegateCommand ExpandAllCommand

    DelegateCommand mExpandAllCommand;
    public DelegateCommand ExpandAllCommand
    {
      get { return mExpandAllCommand ?? (mExpandAllCommand = new DelegateCommand(ExecuteExpandAll, CanExecuteExpandAll)); }
    }

    bool CanExecuteExpandAll()
    {
      return true;
    }

    void ExecuteExpandAll()
    {
      foreach (var x in Model.Routes) x.ExpandAll();
    }

    #endregion

    //#region bool SelectAllFlags

    //public const string SelectUnflaggedProperty = "SelectUnflagged";
    //bool mSelectUnflagged = true;
    //public bool SelectUnflagged
    //{
    //  get { return mSelectUnflagged; }
    //  set
    //  {
    //    if (SetProperty<bool>(ref mSelectUnflagged, value))
    //    {
    //      if (mSelectUnflagged)
    //      {
    //        foreach (var f in Flags)
    //        {
    //          f.IsChecked = false;
    //        }
    //      }
    //    }
    //  }
    //}

    //#endregion
  }
}

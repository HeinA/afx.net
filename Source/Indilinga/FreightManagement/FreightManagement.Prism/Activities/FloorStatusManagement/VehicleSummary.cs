﻿using Afx.Business;
using Afx.Business.Collections;
using FleetManagement.Business;
using FreightManagement.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Activities.FloorStatusManagement
{
  public class VehicleSummary : BusinessObject
  {
    #region Vehicle Vehicle

    public const string VehicleProperty = "Vehicle";
    Vehicle mVehicle;
    public Vehicle Vehicle
    {
      get { return mVehicle; }
      set { SetProperty<Vehicle>(ref mVehicle, value); }
    }

    #endregion

    #region int Waybills

    public const string WaybillsProperty = "Waybills";
    int mWaybills;
    public int Waybills
    {
      get { return mWaybills; }
      set { SetProperty<int>(ref mWaybills, value); }
    }

    #endregion

    #region int Items

    public const string ItemsProperty = "Items";
    int mItems;
    public int Items
    {
      get { return mItems; }
      set { SetProperty<int>(ref mItems, value); }
    }

    #endregion

    #region decimal Weight

    public const string WeightProperty = "Weight";
    decimal mWeight;
    public decimal Weight
    {
      get { return mWeight; }
      set { SetProperty<decimal>(ref mWeight, value); }
    }

    #endregion

    #region decimal Volume

    public const string VolumeProperty = "Volume";
    decimal mVolume;
    public decimal Volume
    {
      get { return mVolume; }
      set { SetProperty<decimal>(ref mVolume, value); }
    }

    #endregion

    #region decimal DeclaredValue

    public const string DeclaredValueProperty = "DeclaredValue";
    decimal mDeclaredValue;
    public decimal DeclaredValue
    {
      get { return mDeclaredValue; }
      set { SetProperty<decimal>(ref mDeclaredValue, value); }
    }

    #endregion

    #region decimal Income

    public const string IncomeProperty = "Income";
    decimal mIncome;
    public decimal Income
    {
      get { return mIncome; }
      set { SetProperty<decimal>(ref mIncome, value); }
    }

    #endregion
  }
}

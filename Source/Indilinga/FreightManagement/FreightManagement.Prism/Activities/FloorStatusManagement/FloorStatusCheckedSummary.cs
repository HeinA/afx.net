﻿using Afx.Prism;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Activities.FloorStatusManagement
{
  public class FloorStatusCheckedSummary : ViewModel<FloorStatusManagement>
  {
    #region Constructors

    [InjectionConstructor]
    public FloorStatusCheckedSummary(IController controller)
      : base(controller)
    {
    }

    #endregion
  }
}

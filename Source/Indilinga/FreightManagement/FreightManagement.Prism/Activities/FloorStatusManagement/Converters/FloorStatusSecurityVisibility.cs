﻿using Afx.Business.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace FreightManagement.Prism.Activities.FloorStatusManagement.Converters
{
  public class FloorStatusSecurityVisibility : IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    {

      if (SecurityContext.User.HasRole(FreightManagement.Business.Roles.AccountsSupervisor, FreightManagement.Business.Roles.OperationsSupervisor, FreightManagement.Business.Roles.DispatchSupervisor, FreightManagement.Business.Roles.Dispatch, Afx.Business.Security.Roles.Administrator))
      {
        return Visibility.Visible;
      }
      else
      {
        return Visibility.Collapsed;
      }

      //string st = ((string)value).Replace("System.Windows.Controls.ComboBoxItem: ", "");
      //switch (st)
      //{
      //  case "By Routes":
      //    return Visibility.Visible;
      //  default:
      //    return Visibility.Collapsed;
      //}
    }

    public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    {
      throw new NotImplementedException();
    }
  }
}

﻿
using System;
using System.Windows;
using System.Windows.Data;

namespace FreightManagement.Prism.Activities.FloorStatusManagement.Converters
{
  public class FloorStatusStockCheckVisibilityConverter : IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    {
      string st = ((string)value);
      switch (st)
      {
        case FloorStatusManagementViewModel.SelectWithCheck:
          return Visibility.Visible;
        default:
          return Visibility.Collapsed;
      }
    }

    public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    {
      throw new NotImplementedException();
    }
  }
}

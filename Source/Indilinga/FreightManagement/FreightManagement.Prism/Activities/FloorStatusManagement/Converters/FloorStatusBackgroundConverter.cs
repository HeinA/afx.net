﻿using FreightManagement.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media;

namespace FreightManagement.Prism.Activities.FloorStatusManagement.Converters
{
  public class FloorStatusBackgroundConverter : IMultiValueConverter
  {
    public object Convert(object[]  value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    {
      decimal onFloor = 0;
      decimal pending = 0;
      decimal forCollection = 0;

      Decimal.TryParse((string)value[0].ToString(), out onFloor);
      Decimal.TryParse((string)value[1].ToString(), out pending);
      Decimal.TryParse((string)value[2].ToString(), out forCollection);

      if (onFloor != 0)
      {
        return new SolidColorBrush(Color.FromArgb(255, 232, 244, 198));
      }
      else if (pending != 0)
      {
        return new SolidColorBrush(Color.FromArgb(255, 255, 253, 207));
      }
      else if (forCollection != 0)
      {
        return new SolidColorBrush(Color.FromArgb(255, 153, 227, 255));
      }
      else
      {
        return new SolidColorBrush(Color.FromArgb(255, 255, 255, 255));
      }
    }

    public object[] ConvertBack(object value, Type[] targetType, object parameter, System.Globalization.CultureInfo culture)
    {
      throw new NotImplementedException();
    }
  }
}

﻿using AccountManagement.Business;
using Afx.Business;
using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Prism.RibbonMdi.Activities;
using FleetManagement.Business;
using FreightManagement.Business;
using Geographics.Business;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace FreightManagement.Prism.Activities.FloorStatusManagement
{
  public class RouteContainer : BusinessObject
  {
    public RouteContainer(Route route, IEnumerable<WaybillFloorStatus> collections)
    {
      Route = route;
      Waybills = collections;
    }

    public void FilterFlags(IEnumerable<WaybillFlagViewModel> flags)
    {
      foreach (var ac in Cities)
      {
        ac.FilterFlags(flags);
      }
      OnPropertyChanged(VisibilityProperty);
      OnPropertyChanged(ItemsProperty);
      OnPropertyChanged(OnFloorProperty);
      OnPropertyChanged(PendingProperty);
    }

    #region bool IsExpanded

    public const string IsExpandedProperty = "IsExpanded";
    bool mIsExpanded;
    public bool IsExpanded
    {
      get { return mIsExpanded; }
      set { SetProperty<bool>(ref mIsExpanded, value); }
    }

    #endregion

    public void ExpandAll()
    {
      IsExpanded = true;
      foreach (var x in Cities) x.ExpandAll();
    }

    #region Route Route

    public const string RouteProperty = "Route";
    Route mRoute;
    public Route Route
    {
      get { return mRoute; }
      private set { mRoute = value; }
    }

    #endregion

    #region string InvoiceTo

    public const string InvoiceToProperty = "InvoiceTo";
    string mInvoiceTo;
    public string InvoiceTo
    {
      get { return mInvoiceTo; }
      set
      {
        if (SetProperty<string>(ref mInvoiceTo, value))
        {
          foreach (var ac in Cities)
          {
            ac.InvoiceTo = value;
          }
          OnPropertyChanged(VisibilityProperty);
          OnPropertyChanged(ItemsProperty);
          OnPropertyChanged(OnFloorProperty);
          OnPropertyChanged(PendingProperty);
        }
      }
    }

    #endregion

    #region string Consigner

    public const string ConsignerProperty = "Consigner";
    string mConsigner;
    public string Consigner
    {
      get { return mConsigner; }
      set
      {
        if (SetProperty<string>(ref mConsigner, value))
        {
          foreach (var ac in Cities)
          {
            ac.Consigner = value;
          }
          OnPropertyChanged(VisibilityProperty);
          OnPropertyChanged(ItemsProperty);
          OnPropertyChanged(OnFloorProperty);
          OnPropertyChanged(PendingProperty);
        }
      }
    }

    #endregion

    #region bool ShowPending

    public const string ShowPendingProperty = "ShowPending";
    bool mShowPending;
    public bool ShowPending
    {
      get { return mShowPending; }
      set
      {
        if (SetProperty<bool>(ref mShowPending, value))
        {
          foreach (var ac in Cities)
          {
            ac.ShowPending = value;
          }
          OnPropertyChanged(VisibilityProperty);
          OnPropertyChanged(ItemsProperty);
          OnPropertyChanged(OnFloorProperty);
          OnPropertyChanged(PendingProperty);
        }
      }
    }

    #endregion

    #region Visibility Visibility

    public const string VisibilityProperty = "Visibility";
    public Visibility Visibility
    {
      get
      {
        return Cities.Count(ac => ac.Visibility == System.Windows.Visibility.Visible) == 0 ? Visibility.Collapsed : Visibility.Visible;
      }
    }

    #endregion

    #region Bay Bay

    public const string BayProperty = "Bay";
    Bay mBay;
    public Bay Bay
    {
      get { return mBay; }
      set
      {
        if (SetProperty<Bay>(ref mBay, value))
        {
          foreach (var ac in Cities)
          {
            ac.Bay = value;
          }
          OnPropertyChanged(VisibilityProperty);
          OnPropertyChanged(ItemsProperty);
          OnPropertyChanged(OnFloorProperty);
          OnPropertyChanged(PendingProperty);
        }
      }
    }

    #endregion

    public IEnumerable<Vehicle> Vehicles
    {
      get { return Cache.Instance.GetObjects<Vehicle>(true, v => v.GetExtensionObject<VehicleFreightDetails>().ShowInFloorStatusLoadList, v => v.Text, true); }
    }

    #region Vehicle Vehicle

    public const string VehicleProperty = "Vehicle";
    Vehicle mVehicle;
    public Vehicle Vehicle
    {
      get { return mVehicle; }
      set
      {
        if (SetProperty(ref mVehicle, value))
        {
          foreach (CityContainer c in Cities)
          {
            c.Vehicle = Vehicle;
          }
        }
      }
    }

    #endregion

    #region bool Checked

    public const string CheckedProperty = "Checked";
    bool mChecked;
    public bool Checked
    {
      get { return mChecked; }
      set
      {
        if (SetProperty<bool>(ref mChecked, value))
        {
          foreach (CityContainer c in Cities.Where(c1 => c1.Visibility == System.Windows.Visibility.Visible))
          {
            c.Checked = Checked;
          }
        }
      }
    }

    #endregion


    #region string SelectBy

    public const string SelectByProperty = "SelectBy";
    string mSelectBy = FloorStatusManagementViewModel.SelectWithCheck;
    public string SelectBy
    {
      get { return mSelectBy; }
      set
      {
        if (SetProperty(ref mSelectBy, value))
        {
          foreach (var ac in Cities)
          {
            ac.SelectBy = SelectBy;
          }
        }
      }
    }

    #endregion

    #region IEnumerable<WaybillFloorStatus> Waybills

    public const string WaybillsProperty = "Waybills";
    IEnumerable<WaybillFloorStatus> mWaybills;
    public IEnumerable<WaybillFloorStatus> Waybills
    {
      get { return mWaybills; }
      private set
      {
        mWaybills = value;
        Collection<CityContainer> cities = new Collection<CityContainer>();
        foreach (var city in Waybills.Select(c => c.DeliveryCity).Distinct().OrderBy(l => l.Name))
        {
          if (Waybills.Where(c => c.DeliveryCity != null && c.DeliveryCity.Equals(city)).Count() > 0)
          {
            cities.Add(new CityContainer(city, Waybills.Where(c => c.DeliveryCity != null && c.DeliveryCity.Equals(city)).OrderBy(c => c.DeliveryDate)) { Bay = this.Bay });
          }
        }
        Cities = cities;
      }
    }

    #endregion

    #region IEnumerable<CityContainer> Cities

    public const string CityProperty = "Cities";
    IEnumerable<CityContainer> mCities;
    public IEnumerable<CityContainer> Cities
    {
      get { return mCities; }
      private set { mCities = value; }
    }

    #endregion

    #region int Items

    public const string ItemsProperty = "Items";
    public int Items
    {
      get
      {
        if (Visibility == System.Windows.Visibility.Collapsed) return 0;
        int items = 0;
        foreach (var ac in Cities)
        {
          items += ac.Items;
        }
        return items;
      }
    }

    #endregion

    #region decimal OnFloor

    public const string OnFloorProperty = "OnFloor";
    public decimal OnFloor
    {
      get
      {
        if (Visibility == System.Windows.Visibility.Collapsed) return 0;
        decimal onFloor = 0;
        foreach (var ac in Cities)
        {
          onFloor += ac.OnFloor;
        }
        return onFloor;
      }
    }

    #endregion

    #region decimal Pending

    public const string PendingProperty = "Pending";
    public decimal Pending
    {
      get
      {
        if (Visibility == System.Windows.Visibility.Collapsed) return 0;
        decimal pending = 0;
        foreach (var ac in Cities)
        {
          pending += ac.Pending;
        }
        return pending;
      }
    }

    #endregion
  }
}

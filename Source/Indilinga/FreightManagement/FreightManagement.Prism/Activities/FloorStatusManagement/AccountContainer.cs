﻿using AccountManagement.Business;
using AccountManagement.Prism.Documents.Account;
using Afx.Business;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Documents;
using Afx.Business.Security;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using Afx.Prism.RibbonMdi.Events;
using FleetManagement.Business;
using FreightManagement.Business;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace FreightManagement.Prism.Activities.FloorStatusManagement
{
  public class AccountContainer : BusinessObject
  {
    public AccountContainer(AccountReference account, IEnumerable<WaybillFloorStatus> collections)
    {
      Account = account;
      Waybills = collections;
    }

    IEnumerable<WaybillFlagViewModel> Flags { get; set; }

    public void FilterFlags(IEnumerable<WaybillFlagViewModel> flags)
    {
      Flags = flags;
      mWaybillsInner = null;
      OnPropertyChanged(VisibilityProperty);
      OnPropertyChanged(WaybillsProperty);
      OnPropertyChanged(ItemsProperty);
      OnPropertyChanged(OnFloorProperty);
      OnPropertyChanged(PendingProperty);
    }

    public string AccountName
    {
      get
      {
        string s = (Account.Name ?? string.Empty).Replace("\r\n", " ").Replace("\n", " ").Replace("\r", " ");
        if (string.IsNullOrWhiteSpace(s)) return "None";
        return s;
      }
    }

    #region bool IsExpanded

    public const string IsExpandedProperty = "IsExpanded";
    bool mIsExpanded;
    public bool IsExpanded
    {
      get { return mIsExpanded; }
      set { SetProperty<bool>(ref mIsExpanded, value); }
    }

    #endregion

    public void ExpandAll()
    {
      IsExpanded = true;
    }

    #region string InvoiceTo

    public const string InvoiceToProperty = "InvoiceTo";
    string mInvoiceTo = string.Empty;
    public string InvoiceTo
    {
      get { return mInvoiceTo; }
      set
      {
        if (SetProperty<string>(ref mInvoiceTo, value))
        {
          mWaybillsInner = null;
          OnPropertyChanged(VisibilityProperty);
          OnPropertyChanged(WaybillsProperty);
          OnPropertyChanged(ItemsProperty);
          OnPropertyChanged(OnFloorProperty);
          OnPropertyChanged(PendingProperty);
        }
      }
    }

    #endregion

    #region string Consigner

    public const string ConsignerProperty = "Consigner";
    string mConsigner = string.Empty;
    public string Consigner
    {
      get { return mConsigner; }
      set
      {
        if (SetProperty<string>(ref mConsigner, value))
        {
          mWaybillsInner = null;
          OnPropertyChanged(VisibilityProperty);
          OnPropertyChanged(WaybillsProperty);
          OnPropertyChanged(ItemsProperty);
          OnPropertyChanged(OnFloorProperty);
          OnPropertyChanged(PendingProperty);
        }
      }
    }

    #endregion

    #region Bay Bay

    public const string BayProperty = "Bay";
    Bay mBay;
    public Bay Bay
    {
      get { return mBay; }
      set
      {
        if (SetProperty<Bay>(ref mBay, value))
        {
          mWaybillsInner = null;
          OnPropertyChanged(VisibilityProperty);
          OnPropertyChanged(WaybillsProperty);
          OnPropertyChanged(ItemsProperty);
          OnPropertyChanged(OnFloorProperty);
          OnPropertyChanged(PendingProperty);
        }
      }
    }

    #endregion

    public IEnumerable<Vehicle> Vehicles
    {
      get { return Cache.Instance.GetObjects<Vehicle>(true, v => v.GetExtensionObject<VehicleFreightDetails>().ShowInFloorStatusLoadList, v => v.Text, true); }
    }

    #region bool ShowPending

    public const string ShowPendingProperty = "ShowPending";
    bool mShowPending;
    public bool ShowPending
    {
      get { return mShowPending; }
      set
      {
        if (SetProperty<bool>(ref mShowPending, value))
        {
          mWaybillsInner = null;
          OnPropertyChanged(VisibilityProperty);
          OnPropertyChanged(WaybillsProperty);
          OnPropertyChanged(ItemsProperty);
          OnPropertyChanged(OnFloorProperty);
          OnPropertyChanged(PendingProperty);
        }
      }
    }

    #endregion

    #region string SelectBy

    public const string SelectByProperty = "SelectBy";
    string mSelectBy = FloorStatusManagementViewModel.SelectWithCheck;
    public string SelectBy
    {
      get { return mSelectBy; }
      set { SetProperty<string>(ref mSelectBy, value); }
    }

    #endregion

    #region Visibility Visibility

    public const string VisibilityProperty = "Visibility";
    public Visibility Visibility
    {
      get
      {
        return Waybills.Any() ? Visibility.Visible : Visibility.Collapsed;
      }
    }

    #endregion

    #region Vehicle Vehicle

    public const string VehicleProperty = "Vehicle";
    Vehicle mVehicle;
    public Vehicle Vehicle
    {
      get { return mVehicle; }
      set
      {
        if (SetProperty(ref mVehicle, value))
        {
          foreach (WaybillFloorStatus w in Waybills)
          {
            w.Vehicle = null;
            if (w.OnFloor > 0) w.Vehicle = Vehicle;
          }
        }
      }
    }

    #endregion

    #region bool Checked

    public const string CheckedProperty = "Checked";
    bool mChecked;
    public bool Checked
    {
      get { return mChecked; }
      set
      {
        if (SetProperty<bool>(ref mChecked, value))
        {
          foreach (WaybillFloorStatus w in Waybills)
          {
            w.IsChecked = false;
            if (w.OnFloor > 0) w.IsChecked = value;
          }
        }
      }
    }

    #endregion

    //#region IEnumerable<Account> Accounts

    //public IEnumerable<Account> Accounts
    //{
    //  get { return Cache.Instance.GetObjects<Account>(true, a => a.OrganizationalUnit.Equals(SecurityContext.OrganizationalUnit), a => a.Name, true); }
    //}

    //#endregion

    #region AccountReference Account

    public const string AccountProperty = "Account";
    AccountReference mAccount;
    public AccountReference Account
    {
      get { return mAccount; }
      private set { mAccount = value; }
    }

    #endregion

    #region IEnumerable<WaybillFloorStatus> Waybills

    public const string WaybillsProperty = "Waybills";
    IEnumerable<WaybillFloorStatus> mWaybills;
    List<WaybillFloorStatus> mWaybillsInner;
    public IEnumerable<WaybillFloorStatus> Waybills
    {
      get
      {
        if (mWaybillsInner != null) return mWaybillsInner;
        mWaybillsInner = new List<WaybillFloorStatus>();
        foreach (var w in mWaybills)
        {
          if (!ShowPending && w.Pending != 0) continue;
          if (!BusinessObject.IsNull(Bay) && !w.Bay.Equals(Bay)) continue;
          bool add = true;
          if (Flags != null)
          {
            //add = false;
            foreach (var wf in Flags)
            {
              if (wf.IsChecked && !w.WaybillFlags.Contains(wf.Model)) add = false;
              if (!wf.IsChecked && w.WaybillFlags.Contains(wf.Model)) add = false;
            }
          }
          if (!string.IsNullOrWhiteSpace(InvoiceTo) && !w.InvoiceTo.ToUpperInvariant().Contains(InvoiceTo.ToUpperInvariant())) continue;
          if (!string.IsNullOrWhiteSpace(Consigner) && !w.Consigner.ToUpperInvariant().Contains(Consigner.ToUpperInvariant())) continue;
          if (add) mWaybillsInner.Add(w);
        }
        return mWaybillsInner;
        //if (!ShowPending) return mWaybills.Where(w => w.Pending == 0 && w.WaybillFlags.Where(wf => Flags.Where(f => f.IsChecked).Select(f => f.Model).Contains(wf)).Any() && BusinessObject.IsNull(Bay) ? true : w.Bay.Equals(Bay)); // && w.WaybillFlags.Where(wf => Flags.Where(f =>!f.IsChecked).Select(f => f.Model).Contains(wf)).Any()
        //return mWaybills.Where(w => BusinessObject.IsNull(Bay) ? true : w.Bay.Equals(Bay) && w.WaybillFlags.Where(wf => Flags.Where(f => f.IsChecked).Select(f => f.Model).Contains(wf)).Any() && !w.WaybillFlags.Where(wf => Flags.Where(f => !f.IsChecked).Select(f => f.Model).Contains(wf)).Any());
      }
      private set
      {
        mWaybills = new BasicCollection<WaybillFloorStatus>(value);
      }
    }

    #endregion

    #region int Items

    public const string ItemsProperty = "Items";
    public int Items
    {
      get
      {
        if (Visibility == System.Windows.Visibility.Collapsed) return 0;
        int items = 0;
        foreach (var w in Waybills)
        {
          if (!ShowPending && w.Pending > 0) continue;
          items += w.Items;
        }
        return items;
      }
    }

    #endregion

    #region decimal OnFloor

    public const string OnFloorProperty = "OnFloor";
    public decimal OnFloor
    {
      get
      {
        if (Visibility == System.Windows.Visibility.Collapsed) return 0;
        decimal onFloor = 0;
        foreach (var w in Waybills)
        {
          if (!ShowPending && w.Pending > 0) continue;
          onFloor += w.OnFloor;
        }
        return onFloor;
      }
    }

    #endregion

    #region decimal Pending

    public const string PendingProperty = "Pending";
    public decimal Pending
    {
      get
      {
        if (Visibility == System.Windows.Visibility.Collapsed) return 0;
        decimal pending = 0;
        foreach (var w in Waybills)
        {
          if (!ShowPending && w.Pending > 0) continue;
          pending += w.Pending;
        }
        return pending;
      }
    }

    #endregion

    #region DelegateCommand<WaybillFloorStatus> OpenWaybillCommand

    DelegateCommand<WaybillFloorStatus> mOpenWaybillCommand;
    public DelegateCommand<WaybillFloorStatus> OpenWaybillCommand
    {
      get { return mOpenWaybillCommand ?? (mOpenWaybillCommand = new DelegateCommand<WaybillFloorStatus>(ExecuteOpenWaybill, CanExecuteOpenWaybill)); }
    }

    bool CanExecuteOpenWaybill(WaybillFloorStatus args)
    {
      return true;
    }

    void ExecuteOpenWaybill(WaybillFloorStatus args)
    {
      MdiApplicationController.Current.EventAggregator.GetEvent<EditDocumentEvent>().Publish(new EditDocumentEventArgs(Cache.Instance.GetObject<DocumentType>(Waybill.DocumentTypeIdentifier), args.GlobalIdentifier));
    }

    #endregion
  }
}

﻿using Afx.Business.Activities;
using Afx.Business.Data;
using Afx.Prism.RibbonMdi;
using Afx.Prism.RibbonMdi.Ribbon;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Activities.WaybillManagementManagement
{
  [Export(typeof(IRibbonItem))]
  public class WaybillManagementManagementButton : RibbonButtonViewModel
  {
    public override string TabName
    {
      get { return ReportingTab.TabName; }
    }

    public override string GroupName
    {
      get { return FreightManagement.Prism.ToolsGroup.GroupName; }
    }

    public override string ItemIdentifier
    {
      get { return "WaybillManagementManagement"; }
    }

    public override int Index
    {
      get { return 0; }
    }

    #region Activity Activity

    public Activity Activity
    {
      get { return Cache.Instance.GetObject<Activity>(WaybillManagementManagement.Key); }
    }

    #endregion

    public bool IsEnabled
    {
      get
      {
        try
        {
          if (Activity == null) return false;
          return Activity.CanView;
        }
        catch
        {
          return false;
        }
      }
    }

    protected override void OnExecute()
    {
      MdiApplicationController.Current.ExecuteActivity(Activity);
    }
  }
}

﻿using Afx.Business.Activities;
using FreightManagement.Business;
using FreightManagement.Business.Service;
using Afx.Prism.RibbonMdi.Activities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Afx.Business.Collections;
using Afx.Business.Security;
using Afx.Business.Service;
using Afx.Business.Data;
using AccountManagement.Business;

namespace FreightManagement.Prism.Activities.WaybillManagementManagement
{
  public partial class WaybillManagementManagement : CustomActivityContext
  {
    public WaybillManagementManagement()
    {
    }

    public WaybillManagementManagement(Activity activity)
      : base(activity)
    {
    }

    public override bool IsDirty
    {
      get { return false; }
      set { }
    }

    #region DistributionCenter DistributionCenter

    public const string DistributionCenterProperty = "DistributionCenter";
    DistributionCenter mDistributionCenter = new DistributionCenter(true); // Cache.Instance.GetObjects<DistributionCenter>(dc => dc.OrganizationalUnit.Equals(SecurityContext.OrganizationalUnit)).FirstOrDefault();
    public DistributionCenter DistributionCenter
    {
      get { return mDistributionCenter; }
      set
      {
        if (SetProperty<DistributionCenter>(ref mDistributionCenter, value))
        {
          LoadWaybills();
        }
      }
    }

    #endregion

    #region string AccountFilter

    public const string AccountFilterProperty = "AccountFilter";
    string mAccountFilter = string.Empty;
    public string AccountFilter
    {
      get { return mAccountFilter; }
      set
      {
        if (SetProperty<string>(ref mAccountFilter, value))
        {
          ApplyFilter();
        }
      }
    }

    #endregion


    #region AccountReference Account

    public const string AccountProperty = "Account";
    AccountReference mAccount = new AccountReference(true);
    public AccountReference Account
    {
      get { return mAccount; }
      set
      {
        if (SetProperty<AccountReference>(ref mAccount, value))
        {
          LoadWaybills();
        }
      }
    }

    #endregion

    #region DateTime StartDate

    public const string StartDateProperty = "StartDate";
    DateTime mStartDate = DateTime.Now.AddDays(-14);
    public DateTime StartDate
    {
      get { return mStartDate; }
      set
      {
        if (SetProperty<DateTime>(ref mStartDate, value))
        {
          LoadWaybills();
        }
      }
    }

    #endregion

    #region DateTime EndDate

    public const string EndDateProperty = "EndDate";
    DateTime mEndDate = DateTime.Now;
    public DateTime EndDate
    {
      get { return mEndDate; }
      set
      {
        if (SetProperty<DateTime>(ref mEndDate, value))
        {
          LoadWaybills();
        }
      }
    }

    #endregion

    #region BasicCollection<AccountContainer> Accounts

    BasicCollection<AccountContainer> mAccounts;
    public BasicCollection<AccountContainer> Accounts
    {
      get { return mAccounts; }
      set { SetProperty<BasicCollection<AccountContainer>>(ref mAccounts, value); }
    }

    #endregion

    #region BasicCollection<WaybillReference> Waybills

    public const string WaybillsProperty = "Waybills";
    BasicCollection<WaybillReference> mWaybills;
    public BasicCollection<WaybillReference> Waybills
    {
      get { return mWaybills; }
      set { SetProperty<BasicCollection<WaybillReference>>(ref mWaybills, value); }
    }

    #endregion

    void ApplyFilter()
    {
      try
      {
        BasicCollection<AccountContainer> accounts = new BasicCollection<AccountContainer>();

        foreach (var account in Waybills.Select(w => w.Account).Distinct().Where(a => a.Name.ToUpperInvariant().Contains(AccountFilter.ToUpperInvariant())).OrderBy(a => a.Name))
        {
          accounts.Add(new AccountContainer(this, account, Waybills.Where(w => w.Account.Equals(account)).OrderBy(w => w.WaybillDate)));
        }
        Accounts = accounts;
      }
      catch
      {
        throw;
      }
    }

    public override void LoadData()
    {
      LoadWaybills();
      base.LoadData();
    }

    void LoadWaybills()
    {
      using (var svc = ProxyFactory.GetService<IFreightManagementService>(SecurityContext.ServerName))
      {
        Waybills = svc.LoadWaybills(DistributionCenter.Id, StartDate.Date, EndDate.Date.AddDays(1).AddSeconds(-1));
      }
      ApplyFilter();
    }
  }
}

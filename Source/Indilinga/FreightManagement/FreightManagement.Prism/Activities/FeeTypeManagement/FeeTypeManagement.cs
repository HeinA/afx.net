﻿using Afx.Business.Activities;
using FreightManagement.Business;
using FreightManagement.Business.Service;
using Afx.Prism.RibbonMdi.Activities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Activities.FeeTypeManagement
{
  public partial class FeeTypeManagement : SimpleActivityContext<FeeType>
  {
    public FeeTypeManagement()
    {
    }

    public FeeTypeManagement(Activity activity)
      : base(activity)
    {
    }
  }
}

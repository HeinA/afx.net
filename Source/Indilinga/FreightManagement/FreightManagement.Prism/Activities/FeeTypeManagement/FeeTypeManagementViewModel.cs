﻿using Afx.Business.Security;
using Afx.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Activities;
using FreightManagement.Business;
using Microsoft.Practices.Prism.Commands;
using Afx.Prism.RibbonMdi.Dialogs.EditFlags;

namespace FreightManagement.Prism.Activities.FeeTypeManagement
{
  public partial class FeeTypeManagementViewModel : MdiSimpleActivityViewModel<FeeTypeManagement>
  {
    #region Constructors

    [InjectionConstructor]
    public FeeTypeManagementViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    #region DelegateCommand<AddressType> EditFlagsCommand

    DelegateCommand<FeeType> mEditFlagsCommand;
    public DelegateCommand<FeeType> EditFlagsCommand
    {
      get { return mEditFlagsCommand ?? (mEditFlagsCommand = new DelegateCommand<FeeType>(ExecuteEditFlags, CanExecuteEditFlags)); }
    }

    bool CanExecuteEditFlags(FeeType args)
    {
      return true;
    }

    void ExecuteEditFlags(FeeType args)
    {
      EditFlagsDialogController efc = Controller.CreateChildController<EditFlagsDialogController>();
      efc.DataContext = new EditFlagsDialogContext() { TargetObject = args };
      efc.Run();
    }

    #endregion
  }
}

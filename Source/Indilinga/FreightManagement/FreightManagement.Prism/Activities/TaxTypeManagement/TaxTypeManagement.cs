﻿using Afx.Business.Activities;
using FreightManagement.Business;
using FreightManagement.Business.Service;
using Afx.Prism.RibbonMdi.Activities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Activities.TaxTypeManagement
{
  public partial class TaxTypeManagement : SimpleActivityContext<TaxType>
  {
    public TaxTypeManagement()
    {
    }

    public TaxTypeManagement(Activity activity)
      : base(activity)
    {
    }
  }
}

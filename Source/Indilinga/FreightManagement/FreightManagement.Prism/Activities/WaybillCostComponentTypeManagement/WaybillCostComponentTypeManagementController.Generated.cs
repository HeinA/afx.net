﻿using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Prism.RibbonMdi.Activities;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.Composition;

namespace FreightManagement.Prism.Activities.WaybillCostComponentTypeManagement
{
  //**************************************************************************************************************
  //**************************************************************************************************************
  //*                                                                                                            *
  //*     This code is auto-generated.  Do not place any cutom code here, it will be lost on re-generation.      *
  //*                                                                                                            *
  //**************************************************************************************************************
  //**************************************************************************************************************
  [Export("Controller:" + global::FreightManagement.Prism.Activities.WaybillCostComponentTypeManagement.WaybillCostComponentTypeManagement.Key)]
  public partial class WaybillCostComponentTypeManagementController
  {
  }
}

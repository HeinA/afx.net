﻿using Afx.Business.Security;
using Afx.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Activities;
using FreightManagement.Business;
using Microsoft.Practices.Prism.Commands;
using Afx.Prism.RibbonMdi.Dialogs.EditFlags;

namespace FreightManagement.Prism.Activities.WaybillCostComponentTypeManagement
{
  public partial class WaybillCostComponentTypeManagementViewModel : MdiSimpleActivityViewModel<WaybillCostComponentTypeManagement>
  {
    #region Constructors

    [InjectionConstructor]
    public WaybillCostComponentTypeManagementViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    #region DelegateCommand<WaybillCostComponentType> EditFlagsCommand

    DelegateCommand<WaybillCostComponentType> mEditFlagsCommand;
    public DelegateCommand<WaybillCostComponentType> EditFlagsCommand
    {
      get { return mEditFlagsCommand ?? (mEditFlagsCommand = new DelegateCommand<WaybillCostComponentType>(ExecuteEditFlags, CanExecuteEditFlags)); }
    }

    bool CanExecuteEditFlags(WaybillCostComponentType args)
    {
      return true;
    }

    void ExecuteEditFlags(WaybillCostComponentType args)
    {
      EditFlagsDialogController efc = Controller.CreateChildController<EditFlagsDialogController>();
      efc.DataContext = new EditFlagsDialogContext() { TargetObject = args };
      efc.Run();
    }

    #endregion
  }
}

﻿using Afx.Business;
using Afx.Business.Activities;
using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Activities;
using Afx.Prism.Utilities;
using FreightManagement.Business;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Activities.OutstandingPODManagement
{
  public partial class OutstandingPODManagementController : MdiCustomActivityController<OutstandingPODManagement, OutstandingPODManagementViewModel>
  {
    #region Constructors

    [InjectionConstructor]
    public OutstandingPODManagementController(IController controller)
      : base(controller)
    {
    }

    #endregion

    #region OnLoaded
    
    protected override void OnLoaded()
    {
      ViewModel.IsFocused = true;
      base.OnLoaded();
    }

    #endregion
    
    #region ExecuteOperation
    
    protected override void ExecuteOperation(Operation op, BusinessObject argument)
    {
      switch (op.Identifier)
      {
        case Operations.ExcelExport:
          ExcelHelper.Export("Outstanding PODs", DataContext.Accounts.SelectMany(a => a.Waybills), "POD");
          break;
      }

      base.ExecuteOperation(op, argument);
    }
    
    #endregion
  }
}
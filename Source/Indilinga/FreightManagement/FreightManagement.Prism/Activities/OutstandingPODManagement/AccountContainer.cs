﻿using AccountManagement.Business;
using AccountManagement.Prism.Documents.Account;
using Afx.Business;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Documents;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using Afx.Prism.RibbonMdi.Events;
using FreightManagement.Business;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Activities.OutstandingPODManagement
{
  public class AccountContainer : BusinessObject
  {
    public AccountContainer(AccountReference account, IEnumerable<WaybillReference> collections)
    {
      Account = account;
      Waybills = new ObservableCollection<WaybillReference>(collections);
    }

    public string AccountName
    {
      get
      {
        string s = Account.Name;
        if (string.IsNullOrWhiteSpace(s)) return "None";
        return s;
      }
    }

    public string AccountNumber
    {
      get
      {
        string s = Account.AccountNumber;
        if (string.IsNullOrWhiteSpace(s)) return "None";
        return s;
      }
    }




    #region AccountReference Account

    public const string AccountProperty = "Account";
    AccountReference mAccount;
    public AccountReference Account
    {
      get { return mAccount; }
      private set { mAccount = value; }
    }

    #endregion

    #region IEnumerable<WaybillReference> Waybills

    public const string WaybillsProperty = "Waybills";
    IEnumerable<WaybillReference> mWaybills;
    public IEnumerable<WaybillReference> Waybills
    {
      get { return mWaybills; }
      private set { mWaybills = value; }
    }

    #endregion

    #region int TotalWaybills

    public int TotalWaybills
    {
      get { return mWaybills.Count(); }
    }

    #endregion

    #region int TotalWaybillItems

    public int TotalWaybillItems
    {
      get { return mWaybills.Sum(w => w.Items); }
    }

    #endregion

    #region decimal TotalWaybillWeights

    public decimal TotalWaybillWeights
    {
      get { return mWaybills.Sum(w => w.PhysicalWeight); }
    }

    #endregion

    #region decimal TotalWaybillDeclaredValue

    public decimal TotalWaybillDeclaredValue
    {
      get { return mWaybills.Sum(w => w.DeclaredValue); }
    }

    #endregion

    #region decimal TotalWaybillCharges

    public decimal TotalWaybillCharges
    {
      get { return mWaybills.Sum(w => w.Charge); }
    }

    #endregion


    #region DelegateCommand<WaybillReference> OpenWaybillCommand

    DelegateCommand<WaybillReference> mOpenWaybillCommand;
    public DelegateCommand<WaybillReference> OpenWaybillCommand
    {
      get
      {
        return mOpenWaybillCommand ?? (mOpenWaybillCommand = new DelegateCommand<WaybillReference>(ExecuteOpenDeliveryManifest, CanExecuteOpenDeliveryManifest));
      }
    }

    bool CanExecuteOpenDeliveryManifest(WaybillReference args)
    {
      return true;
    }

    void ExecuteOpenDeliveryManifest(WaybillReference args)
    {
      MdiApplicationController.Current.EventAggregator.GetEvent<EditDocumentEvent>().Publish(new EditDocumentEventArgs(Cache.Instance.GetObject<DocumentType>(Waybill.DocumentTypeIdentifier), args.GlobalIdentifier));
    }

    #endregion
  }
}

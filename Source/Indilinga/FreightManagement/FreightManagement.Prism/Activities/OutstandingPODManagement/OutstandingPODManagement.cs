﻿using Afx.Business.Activities;
using FreightManagement.Business;
using FreightManagement.Business.Service;
using Afx.Prism.RibbonMdi.Activities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using Afx.Business.Collections;
using Afx.Business;
using Afx.Business.Data;
using Afx.Business.Security;
using AccountManagement.Business;
using Afx.Business.Service;

namespace FreightManagement.Prism.Activities.OutstandingPODManagement
{
  public partial class OutstandingPODManagement : CustomActivityContext
  {
    public OutstandingPODManagement()
    {
    }

    public OutstandingPODManagement(Activity activity)
      : base(activity)
    {
    }

    public override bool IsDirty
    {
      get { return false; }
      set { }
    }

    #region int OlderThan

    public const string OlderThanProperty = "OlderThan";
    int mOlderThan = 14;
    public int OlderThan
    {
      get { return mOlderThan; }
      set
      {
        if (SetProperty<int>(ref mOlderThan, value))
        {
          ApplyFilter();
        }
      }
    }

    #endregion

    #region string AccountFilter

    public const string AccountFilterProperty = "AccountFilter";
    string mAccountFilter;
    public string AccountFilter
    {
      get { return mAccountFilter; }
      set
      {
        if (SetProperty<string>(ref mAccountFilter, value))
        {
          ApplyFilter();
        }
      }
    }

    #endregion

    public IEnumerable<Route> Routes
    {
      get { return Cache.Instance.GetObjects<Route>(true, r => r.Name, true); }
    }

    #region Route Route

    public const string RouteProperty = "Route";
    Route mRoute;
    public Route Route
    {
      get { return mRoute; }
      set
      {
        if (SetProperty<Route>(ref mRoute, value))
        {
          ApplyFilter();
        }
      }
    }

    #endregion

    #region BasicCollection<AccountContainer> Accounts

    BasicCollection<AccountContainer> mAccounts;
    public BasicCollection<AccountContainer> Accounts
    {
      get { return mAccounts; }
      set { SetProperty<BasicCollection<AccountContainer>>(ref mAccounts, value); }
    }

    #endregion

    #region BasicCollection<WaybillReference> Waybills

    public const string WaybillsProperty = "Waybills";
    BasicCollection<WaybillReference> mWaybills;
    public BasicCollection<WaybillReference> Waybills
    {
      get { return mWaybills; }
      set
      {
        if (SetProperty<BasicCollection<WaybillReference>>(ref mWaybills, value))
        {
          ApplyFilter();
        }
      }
    }

    #endregion

    void ApplyFilter()
    {
      try
      {
        BasicCollection<AccountContainer> accounts = new BasicCollection<AccountContainer>();

        IEnumerable<WaybillReference> waybills = null;
        if (BusinessObject.IsNull(Route)) waybills = Waybills.Where(w => w.WaybillDate.AddDays(OlderThan) <= DateTime.Now && (string.IsNullOrWhiteSpace(AccountFilter) ? true : w.Account.Name != null ? w.Account.Name.ToUpperInvariant().Contains(AccountFilter.ToUpperInvariant()) : false));
        else waybills = Waybills.Where(w => w.Routes.Any(wr => wr.Route.Equals(Route)) && w.WaybillDate.AddDays(OlderThan) <= DateTime.Now && (string.IsNullOrWhiteSpace(AccountFilter) ? true : w.Account.Name != null ? w.Account.Name.ToUpperInvariant().Contains(AccountFilter.ToUpperInvariant()) : false));

        foreach (var account in waybills.Select(w => w.Account).Distinct().OrderBy(a => a.Name))
        {
          accounts.Add(new AccountContainer(account, waybills.Where(w => w.Account.Equals(account)).OrderBy(w => w.WaybillDate)));
        }
        Accounts = accounts;
      }
      catch
      {
        throw;
      }
    }

    public override void LoadData()
    {
      LoadWaybills();
      base.LoadData();
    }

    void LoadWaybills()
    {
      using (var svc = ProxyFactory.GetService<IFreightManagementService>(SecurityContext.ServerName))
      {
        Waybills = svc.LoadOutstandingPODs();
      }
    }
  }
}

﻿using FreightManagement.Business;
using FreightManagement.Business.Service;
using Afx.Business.Service;
using Afx.Prism.RibbonMdi.Activities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.Composition;
using Afx.Business.Security;

namespace FreightManagement.Prism.Activities.OutstandingPODManagement
{
  //**************************************************************************************************************
  //**************************************************************************************************************
  //*                                                                                                            *
  //*     This code is auto-generated.  Do not place any cutom code here, it will be lost on re-generation.      *
  //*                                                                                                            *
  //**************************************************************************************************************
  //**************************************************************************************************************
  [Export("Context:" + global::FreightManagement.Prism.Activities.OutstandingPODManagement.OutstandingPODManagement.Key)]
  public partial class OutstandingPODManagement
  {
    public const string Key = "{c665ca9d-cbc7-4bb8-b574-a10e772f8f2f}";
  }
}

﻿using Afx.Business.Activities;
using FreightManagement.Business;
using FreightManagement.Business.Service;
using Afx.Prism.RibbonMdi.Activities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Activities.WaybillFlagManagement
{
  public partial class WaybillFlagManagement : SimpleActivityContext<WaybillFlag>
  {
    public WaybillFlagManagement()
    {
    }

    public WaybillFlagManagement(Activity activity)
      : base(activity)
    {
    }
  }
}

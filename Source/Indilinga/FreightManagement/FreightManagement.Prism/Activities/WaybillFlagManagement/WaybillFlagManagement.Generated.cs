﻿using FreightManagement.Business;
using FreightManagement.Business.Service;
using Afx.Business.Service;
using Afx.Prism.RibbonMdi.Activities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.Composition;
using Afx.Business.Security;

namespace FreightManagement.Prism.Activities.WaybillFlagManagement
{
  //**************************************************************************************************************
  //**************************************************************************************************************
  //*                                                                                                            *
  //*     This code is auto-generated.  Do not place any cutom code here, it will be lost on re-generation.      *
  //*                                                                                                            *
  //**************************************************************************************************************
  //**************************************************************************************************************
  [Export("Context:" + WaybillFlagManagement.Key)]
  public partial class WaybillFlagManagement
  {
    public const string Key = "{967c32a1-5954-404a-8b5b-8bcf10c7c515}";

    public override void LoadData()
    {
      using (var svc = ProxyFactory.GetService<IFreightManagementService>(SecurityContext.MasterServer.Name))
      {
        Data = svc.LoadWaybillFlagCollection();
      }
      base.LoadData();
    }

    public override void SaveData()
    {
      using (var svc = ProxyFactory.GetService<IFreightManagementService>(SecurityContext.MasterServer.Name))
      {
        Data = svc.SaveWaybillFlagCollection(Data);
      }
      base.SaveData();
    }
  }
}

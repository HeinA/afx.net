﻿using Afx.Business.Security;
using Afx.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Activities;
using FreightManagement.Business;
using Afx.Business.Collections;

namespace FreightManagement.Prism.Activities.InvoicingManagement
{
  public partial class InvoicingManagementViewModel : MdiCustomActivityViewModel<InvoicingManagement>
  {
    #region Constructors

    [InjectionConstructor]
    public InvoicingManagementViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    public IEnumerable<WaybillViewModel> Waybills
    {
      get { return ResolveViewModels<WaybillViewModel, WaybillInvoiceReference>(Model.Waybills); }
    }

    public void RemoveCharges(BusinessObjectCollection<WaybillInvoiceReferenceCharge> charges)
    {
      foreach (var c in charges)
      {
        var c1 = Model.CheckedWaybillCharges.FirstOrDefault(c2 => c2.ChargeType.Equals(c.ChargeType));
        if (c1 != null)
        {
          c1.Charge -= c.Charge;
        }
      }

      foreach (var c1 in Model.CheckedWaybillCharges.Where(c2 => c2.Charge == 0).ToArray())
      {
        Model.CheckedWaybillCharges.Remove(c1);
      }
    }

    public void AddCharges(BusinessObjectCollection<WaybillInvoiceReferenceCharge> charges)
    {
      foreach (var c in charges)
      {
        var c1 = Model.CheckedWaybillCharges.FirstOrDefault(c2 => c2.ChargeType.Equals(c.ChargeType));
        if (c1 == null)
        {
          c1 = new WaybillInvoiceReferenceCharge() { ChargeType = c.ChargeType };
          Model.CheckedWaybillCharges.Add(c1);
        }
        c1.Charge += c.Charge;
      }
    }
  }
}

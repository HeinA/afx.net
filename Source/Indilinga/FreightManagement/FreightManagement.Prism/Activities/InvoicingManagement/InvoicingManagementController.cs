﻿using Afx.Business;
using Afx.Business.Activities;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Business.Service;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Activities;
using FreightManagement.Business;
using FreightManagement.Business.Service;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace FreightManagement.Prism.Activities.InvoicingManagement
{
  public partial class InvoicingManagementController : MdiCustomActivityController<InvoicingManagement, InvoicingManagementViewModel>
  {
    #region Constructors

    [InjectionConstructor]
    public InvoicingManagementController(IController controller)
      : base(controller)
    {
    }

    #endregion

    #region OnLoaded
    
    protected override void OnLoaded()
    {
      ViewModel.IsFocused = true;
      base.OnLoaded();
    }

    #endregion
    
    #region ExecuteOperation
    
    protected override void ExecuteOperation(Operation op, BusinessObject argument)
    {
      switch (op.Identifier)
      {
        case Operations.Invoice:
          if (string.IsNullOrWhiteSpace(DataContext.InvoiceNumber)) throw new MessageException("Invoice Number is mandatory.");
          var waybills = new BasicCollection<Guid>(ViewModel.Waybills.Where(w => w.IsChecked).Select(w => w.Model.GlobalIdentifier));
          if (waybills.Count == 0) throw new MessageException("No Waybills are selected.");
          if (MessageBox.Show("Are you sure you want to invoice the selected waybills", "Invoice", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No) return;

          using (var svc = ServiceFactory.GetService<IFreightManagementService>(SecurityContext.ServerName))
          {
            svc.Instance.InvoiceWaybills(waybills, DataContext.InvoiceDate, DataContext.InvoiceNumber);
          }
          Refresh(true);
          break;

        case Operations.SelectAll:
          foreach (var w in ViewModel.Waybills)
          {
            w.IsChecked = true;
          }
          break;
      }
      base.ExecuteOperation(op, argument);
    }
    
    #endregion
  }
}
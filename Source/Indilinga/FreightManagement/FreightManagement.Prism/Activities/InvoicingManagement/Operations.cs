﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Activities.InvoicingManagement
{
  //**************************************************************************************************************
  //**************************************************************************************************************
  //*                                                                                                            *
  //*     This code is auto-generated.  Do not place any cutom code here, it will be lost on re-generation.      *
  //*                                                                                                            *
  //**************************************************************************************************************
  //**************************************************************************************************************
  public class Operations : Afx.Prism.RibbonMdi.StandardOperations
  {
    public const string Invoice = "{6ce50765-d469-42f3-a651-bbe2bab8b1e5}";
    public const string SelectAll = "{bb8ce0e6-fd1e-4e3c-b591-d5a9bcc2ce59}";
  }
}

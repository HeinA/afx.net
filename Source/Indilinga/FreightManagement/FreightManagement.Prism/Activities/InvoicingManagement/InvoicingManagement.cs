﻿using Afx.Business.Activities;
using FreightManagement.Business;
using FreightManagement.Business.Service;
using Afx.Prism.RibbonMdi.Activities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Afx.Business.Service;
using Afx.Business.Security;
using Afx.Business.Collections;
using AccountManagement.Business;
using Afx.Business;

namespace FreightManagement.Prism.Activities.InvoicingManagement
{
  public partial class InvoicingManagement : CustomActivityContext
  {
    public InvoicingManagement()
    {
    }

    public InvoicingManagement(Activity activity)
      : base(activity)
    {
    }

    public override bool IsDirty
    {
      get { return false; }
      set { }
    }

    public override void LoadData()
    {
      Refresh();
      base.LoadData();
    }

    #region AccountReference Account

    public const string AccountProperty = "Account";
    AccountReference mAccount = new AccountReference(true);
    public AccountReference Account
    {
      get { return mAccount; }
      set
      {
        if (SetProperty<AccountReference>(ref mAccount, value))
        {
          Refresh();
        }
      }
    }

    #endregion

    #region BasicCollection<WaybillInvoiceReference> Waybills

    public const string WaybillsProperty = "Waybills";
    BasicCollection<WaybillInvoiceReference> mWaybills;
    public BasicCollection<WaybillInvoiceReference> Waybills
    {
      get { return mWaybills; }
      set { SetProperty<BasicCollection<WaybillInvoiceReference>>(ref mWaybills, value); }
    }

    #endregion

    #region BasicCollection<WaybillInvoiceReferenceCharge> SelectedWaybillCharges

    public const string SelectedWaybillChargesProperty = "SelectedWaybillCharges";
    BasicCollection<WaybillInvoiceReferenceCharge> mSelectedWaybillCharges;
    public BasicCollection<WaybillInvoiceReferenceCharge> SelectedWaybillCharges
    {
      get { return mSelectedWaybillCharges; }
      set { SetProperty<BasicCollection<WaybillInvoiceReferenceCharge>>(ref mSelectedWaybillCharges, value); }
    }

    #endregion

    #region BasicCollection<WaybillInvoiceReferenceCharge> CheckedWaybillCharges

    public const string CheckedWaybillChargesProperty = "CheckedWaybillCharges";
    BasicCollection<WaybillInvoiceReferenceCharge> mCheckedWaybillCharges = new BasicCollection<WaybillInvoiceReferenceCharge>();
    public BasicCollection<WaybillInvoiceReferenceCharge> CheckedWaybillCharges
    {
      get { return mCheckedWaybillCharges; }
      set { SetProperty<BasicCollection<WaybillInvoiceReferenceCharge>>(ref mCheckedWaybillCharges, value); }
    }

    #endregion

    #region DateTime InvoiceDate

    public const string InvoiceDateProperty = "InvoiceDate";
    DateTime mInvoiceDate = DateTime.Now;
    public DateTime InvoiceDate
    {
      get { return mInvoiceDate; }
      set { SetProperty<DateTime>(ref mInvoiceDate, value); }
    }

    #endregion

    #region string InvoiceNumber

    public const string InvoiceNumberProperty = "InvoiceNumber";
    string mInvoiceNumber;
    public string InvoiceNumber
    {
      get { return mInvoiceNumber; }
      set { SetProperty<string>(ref mInvoiceNumber, value); }
    }

    #endregion

    void Refresh()
    {
      if (BusinessObject.IsNull(Account))
      {
        Waybills = new BasicCollection<WaybillInvoiceReference>();
        return;
      }

      using (var svc = ProxyFactory.GetService<IFreightManagementService>(SecurityContext.ServerName))
      {
        Waybills = svc.LoadReadyForInvoicing(Account.GlobalIdentifier);
      }

      SelectedWaybillCharges = null;
      CheckedWaybillCharges.Clear();
    }
  }
}

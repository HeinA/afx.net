﻿using Afx.Prism;
using FreightManagement.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Activities.InvoicingManagement
{
  public class WaybillViewModel : SelectableViewModel<WaybillInvoiceReference>
  {
    #region Constructors

    [InjectionConstructor]
    public WaybillViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    public new InvoicingManagementViewModel Parent
    {
      get { return base.Parent as InvoicingManagementViewModel; }
    }

    #region bool IsChecked

    public const string IsCheckedProperty = "IsChecked";
    bool mIsChecked;
    public bool IsChecked
    {
      get { return mIsChecked; }
      set
      {
        if (SetProperty<bool>(ref mIsChecked, value))
        {
          if (value)
          {
            Parent.AddCharges(Model.Charges);
          }
          else
          {
            Parent.RemoveCharges(Model.Charges);
          }
        }
      }
    }

    #endregion

    public override bool IsSelected
    {
      get
      {
        return base.IsSelected;
      }
      set
      {
        base.IsSelected = value;
        if (value)
        {
          Parent.Model.SelectedWaybillCharges = Model.Charges;
        }
        else
        {
          Parent.Model.SelectedWaybillCharges = null;
        }
      }
    }
  }
}

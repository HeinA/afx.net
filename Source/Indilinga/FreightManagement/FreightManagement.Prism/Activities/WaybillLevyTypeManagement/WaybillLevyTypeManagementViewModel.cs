﻿using Afx.Business.Security;
using Afx.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Activities;
using FreightManagement.Business;

namespace FreightManagement.Prism.Activities.WaybillLevyTypeManagement
{
  public partial class WaybillLevyTypeManagementViewModel : MdiContextualActivityViewModel<WaybillLevyTypeManagement>
  {
    #region Constructors

    [InjectionConstructor]
    public WaybillLevyTypeManagementViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion
  }
}

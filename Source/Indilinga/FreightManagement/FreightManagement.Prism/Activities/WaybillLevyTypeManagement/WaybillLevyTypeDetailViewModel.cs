﻿using Afx.Business;
using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Activities;
using FreightManagement.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

namespace FreightManagement.Prism.Activities.WaybillLevyTypeManagement
{
  public partial class WaybillLevyTypeDetailViewModel : ActivityDetailViewModel<WaybillLevyType>
  {
    #region Constructors

    [InjectionConstructor]
    public WaybillLevyTypeDetailViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    Collection<WaybillCostComponentType> CostComponentTypes
    {
      get { return Cache.Instance.GetObjects<WaybillCostComponentType>(cc => cc.Text, true); }
    }

    public IEnumerable<CostComponentTypeItemViewModel> CostComponentTypeViewModels
    {
      get { return ResolveViewModels<CostComponentTypeItemViewModel, WaybillCostComponentType>(CostComponentTypes); }
    }

    public class LevyCondition
    {
      public LevyCondition(Type type)
      {
        if (type == null) return;

        TypeName = TypeHelper.GetTypeName(type);
        DisplayNameAttribute dna = type.GetCustomAttribute<DisplayNameAttribute>();
        if (dna != null)
        {
          DisplayName = dna.DisplayName;
        }
        else
        {
          DisplayName = type.Name;
        }
      }

      public string TypeName { get; private set; }
      public string DisplayName { get; private set; }
    }

    public IEnumerable<LevyCondition> Conditions
    {
      get
      {
        List<LevyCondition> list = new List<LevyCondition>();

        list.Add(new LevyCondition(null));

        foreach (var lc in ExtensibilityManager.GetExportedTypes<ILevyCondition>())
        {
          list.Add(new LevyCondition(lc));
        }

        return list;
      }
    }
  }
}

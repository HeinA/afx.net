﻿using Afx.Prism;
using FreightManagement.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Activities.WaybillLevyTypeManagement
{
  public class CostComponentTypeItemViewModel : SelectableViewModel<WaybillCostComponentType>
  {
    #region Constructors

    [InjectionConstructor]
    public CostComponentTypeItemViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    new WaybillLevyTypeDetailViewModel Parent
    {
      get { return (WaybillLevyTypeDetailViewModel)base.Parent; }
    }

    #region bool IsChecked

    public bool IsChecked
    {
      get { return Parent.Model.ApplicableCostComponents.Contains(Model); }
      set
      {
        if (value)
        {
          if (!Parent.Model.ApplicableCostComponents.Contains(Model)) Parent.Model.ApplicableCostComponents.Add(Model);
        }
        else
        {
          if (Parent.Model.ApplicableCostComponents.Contains(Model)) Parent.Model.ApplicableCostComponents.Remove(Model);
        }
      }
    }

    #endregion
  }
}

﻿using Afx.Business;
using Afx.Business.Activities;
using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Activities;
using FreightManagement.Business;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Activities.WaybillLevyTypeManagement
{
  public partial class WaybillLevyTypeManagementController : MdiContextualActivityController<WaybillLevyTypeManagement, WaybillLevyTypeManagementViewModel>
  {
    #region Constructors

    [InjectionConstructor]
    public WaybillLevyTypeManagementController(IController controller)
      : base(controller)
    {
    }

    #endregion

    #region OnLoaded
    
    protected override void OnLoaded()
    {
      if (DataContext.Data.Count > 0)
      {
        //Focus the first element 
        WaybillLevyTypeItemViewModel ivm = GetCreateViewModel<WaybillLevyTypeItemViewModel>(DataContext.Data[0], ViewModel);
        ivm.IsFocused = true;
      }

      RegisterContextRegion(RegionManager.Regions[WaybillLevyTypeManagement.DetailsRegion]);
      RegisterContextRegion(RegionManager.Regions[WaybillLevyTypeManagement.TabRegion]);

      base.OnLoaded();
    }

    #endregion
    
    #region OnContextViewModelChanged
    
    protected override void OnContextViewModelChanged(ISelectableViewModel context)
    {
      if (context.Model is WaybillLevyType)
      {
        WaybillLevyTypeDetailViewModel vm = GetCreateViewModel<WaybillLevyTypeDetailViewModel>(context.Model, ViewModel);
        AddDetailsViewModel(vm);
      }

      base.OnContextViewModelChanged(context);
    }

    #endregion
    
    #region ExecuteOperation
    
    protected override void ExecuteOperation(Operation op, BusinessObject argument)
    {
      switch (op.Identifier)
      {
        case Operations.AddLevy:
          {
            CreateRootItem();
          }
          break;
      }

      base.ExecuteOperation(op, argument);
    }
    
    #endregion
  }
}

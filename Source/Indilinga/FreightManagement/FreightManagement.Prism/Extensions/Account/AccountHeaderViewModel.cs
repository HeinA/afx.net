﻿using Afx.Business.Data;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Documents;
using ContactManagement.Business;
using FreightManagement.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Extensions.Account
{
  public class AccountHeaderViewModel : ExtensionViewModel<FreightManagement.Business.AccountExtension>
  {
    #region Constructors

    [InjectionConstructor]
    public AccountHeaderViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    public IEnumerable<Address> ShipFromAddresses
    {
      get { return Model.Owner.Addresses.Where(a => a.AddressType.IsFlagged(FreightManagement.Business.AddressTypeFlags.ShipFromAddress)); }
    }

    //public IEnumerable<TaxType> TaxTypes
    //{
    //  get { return Cache.Instance.GetObjects<TaxType>(true, tt => tt.Text, true); }
    //}

  }
}

﻿using Afx.Business.Data;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Documents;
using ContactManagement.Business;
using FreightManagement.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Extensions.Account
{
  public class AccountRatesViewModel : ExtensionViewModel<FreightManagement.Business.AccountExtension>
  {
    #region Constructors

    [InjectionConstructor]
    public AccountRatesViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    #region string Title

    public string Title
    {
      get { return "Applicable Rates"; }
    }

    #endregion

    Collection<RatesConfigurationReference> Rates
    {
      get { return Cache.Instance.GetObjects<RatesConfigurationReference>(rc => !rc.IsStandardConfiguration, rc => rc.Name, true); }
    }

    public IEnumerable<RatesConfigurationItemViewModel> RatesViewModels
    {
      get { return ResolveViewModels<RatesConfigurationItemViewModel, RatesConfigurationReference>(Rates); }
    }
  }
}

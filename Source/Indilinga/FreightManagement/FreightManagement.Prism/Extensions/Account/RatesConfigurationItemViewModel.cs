﻿using Afx.Prism;
using FreightManagement.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Extensions.Account
{
  public class RatesConfigurationItemViewModel : SelectableViewModel<RatesConfigurationReference>
  {
    #region Constructors

    [InjectionConstructor]
    public RatesConfigurationItemViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    new AccountRatesViewModel Parent
    {
      get { return (AccountRatesViewModel)base.Parent; }
    }

    #region bool IsChecked

    public bool IsChecked
    {
      get { return Parent.Model.Rates.Contains(Model); }
      set
      {
        if (value)
        {
          if (!Parent.Model.Rates.Contains(Model)) Parent.Model.Rates.Add(Model);
        }
        else
        {
          if (Parent.Model.Rates.Contains(Model)) Parent.Model.Rates.Remove(Model);
        }
      }
    }

    #endregion
  }
}

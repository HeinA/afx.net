﻿using Afx.Prism;
using Afx.Prism.RibbonMdi.Documents;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Extensions.VehicleFreightDetails
{
  public class VehicleFreightDetailsViewModel : ExtensionViewModel<FreightManagement.Business.VehicleFreightDetails>
  {
    #region Constructors

    [InjectionConstructor]
    public VehicleFreightDetailsViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion
  }
}

﻿using Afx.Business;
using FreightManagement.Business;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Extensions.EditProFormaTaxTypeDialog
{
  public class EditProFormaTaxTypeDialogContext : BusinessObject
  {
    #region TaxType TaxType

    public const string TaxTypeProperty = "TaxType";
    TaxType mTaxType;
    public TaxType TaxType
    {
      get { return mTaxType; }
      set { SetProperty<TaxType>(ref mTaxType, value); }
    }

    #endregion

    #region bool AllowOverride

    public const string AllowOverrideProperty = "AllowOverride";
    bool mAllowOverride;
    public bool AllowOverride
    {
      get { return mAllowOverride; }
      set { SetProperty<bool>(ref mAllowOverride, value); }
    }

    #endregion
  }
}
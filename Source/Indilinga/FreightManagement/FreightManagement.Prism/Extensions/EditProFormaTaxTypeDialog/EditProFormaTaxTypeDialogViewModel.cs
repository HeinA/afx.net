﻿using Afx.Business.Data;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using FreightManagement.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Extensions.EditProFormaTaxTypeDialog
{
  public class EditProFormaTaxTypeDialogViewModel : MdiDialogViewModel<EditProFormaTaxTypeDialogContext>
  {
    [InjectionConstructor]
    public EditProFormaTaxTypeDialogViewModel(IController controller)
      : base(controller)
    {
    }

    public IEnumerable<TaxType> TaxTypes
    {
      get { return Cache.Instance.GetObjects<TaxType>(true, tt => tt.Text, true); }
    }
  }
}

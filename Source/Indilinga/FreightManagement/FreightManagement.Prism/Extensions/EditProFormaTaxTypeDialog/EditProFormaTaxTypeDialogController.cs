﻿using Afx.Prism;
using Afx.Prism.RibbonMdi;
using FreightManagement.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace FreightManagement.Prism.Extensions.EditProFormaTaxTypeDialog
{
  public class EditProFormaTaxTypeDialogController : MdiDialogController<EditProFormaTaxTypeDialogContext, EditProFormaTaxTypeDialogViewModel>
  {
    public const string EditProFormaTaxTypeDialogControllerKey = "FreightManagement.Prism.Extensions.EditProFormaTaxTypeDialog.EditProFormaTaxTypeDialogController";

    public EditProFormaTaxTypeDialogController(IController controller)
      : base(EditProFormaTaxTypeDialogControllerKey, controller)
    {
    }

    protected override bool OnRun()
    {
      if (DataContext == null) DataContext = new EditProFormaTaxTypeDialogContext();
      ViewModel.Caption = "Edit Pro Forma Tax Type";
      if (WaybillChargeType != null)
      {
        //DataContext.TaxType = WaybillChargeType.TaxType;
        //DataContext.AllowOverride = WaybillChargeType.AllowTaxOverride;
      }
      return base.OnRun();
    }

    #region WaybillChargeType WaybillChargeType

    public const string WaybillChargeTypeProperty = "WaybillChargeType";
    WaybillChargeType mWaybillChargeType;
    public WaybillChargeType WaybillChargeType
    {
      get { return mWaybillChargeType; }
      set
      {
        mWaybillChargeType = value;
      }
    }

    #endregion

    protected override void ApplyChanges()
    {
      //WaybillChargeType.AllowTaxOverride = DataContext.AllowOverride;
      //WaybillChargeType.TaxType = DataContext.TaxType;
      base.ApplyChanges();
    }
  }
}

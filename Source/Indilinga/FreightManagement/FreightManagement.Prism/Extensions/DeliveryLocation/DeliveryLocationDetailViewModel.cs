﻿using Afx.Prism;
using Afx.Prism.RibbonMdi.Documents;
using Geographics.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Extensions.DeliveryLocation
{
  public class DeliveryLocationDetailViewModel : ExtensionViewModel<FreightManagement.Business.DeliveryLocation>
  {
    #region Constructors

    [InjectionConstructor]
    public DeliveryLocationDetailViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion
  }
}

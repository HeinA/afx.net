﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Extensions.LocationImportVAT
{
  [Export(typeof(Afx.Prism.ResourceInfo))]
  public class ResourceInfo : Afx.Prism.ResourceInfo
  {
    public override Uri GetUri()
    {
      return new Uri("pack://application:,,,/FreightManagement.Prism;component/Extensions/LocationImportVAT/Resources.xaml", UriKind.Absolute);
    }

    public override int Priority
    {
      get { return 50; }
    }
  }
}

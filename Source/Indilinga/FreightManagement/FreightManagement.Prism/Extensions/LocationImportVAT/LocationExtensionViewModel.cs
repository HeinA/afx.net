﻿using Afx.Prism;
using Afx.Prism.RibbonMdi.Documents;
using Geographics.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Extensions.LocationImportVAT
{
  public class LocationExtensionViewModel : ExtensionViewModel<FreightManagement.Business.LocationExtension>
  {
    #region Constructors

    [InjectionConstructor]
    public LocationExtensionViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion
  }
}

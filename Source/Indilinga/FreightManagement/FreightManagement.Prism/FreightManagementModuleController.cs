﻿using AccountManagement.Prism.Documents.Account;
using Afx.Business;
using Afx.Business.Activities;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Documents;
using Afx.Business.Security;
using Afx.Business.Service;
using Afx.Prism;
using Afx.Prism.Events;
using Afx.Prism.RibbonMdi;
using Afx.Prism.RibbonMdi.Documents;
using Afx.Prism.RibbonMdi.Events;
using FleetManagement.Business;
using FleetManagement.Prism.Activities.OwnedVehicleManagement;
using FreightManagement.Business;
using FreightManagement.Business.Service;
using FreightManagement.Prism.Activities.WaybillCostComponentTypeManagement;
using FreightManagement.Prism.Extensions.Account;
using FreightManagement.Prism.Extensions.DeliveryLocation;
using FreightManagement.Prism.Extensions.EditProFormaTaxTypeDialog;
using FreightManagement.Prism.Extensions.LocationImportVAT;
using FreightManagement.Prism.Extensions.VehicleFreightDetails;
using FreightManagement.Prism.Tools.ManifestRoute;
using FreightManagement.Prism.Tools.NextWaybillRoute;
using FreightManagement.Prism.Tools.PODHandoverWaybills;
using Geographics.Business;
using Geographics.Prism.Activities.LocationManagement;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace FreightManagement.Prism
{
  public class FreightManagementModuleController : MdiModuleController
  {
    [InjectionConstructor]
    public FreightManagementModuleController(IController controller)
      : base("FreightManagement Module Controller", controller)
    {
    }

    protected override void OnUserAuthenticated(EventArgs obj)
    {
      base.OnUserAuthenticated(obj);

      //using (var svc = ServiceFactory.GetService<AccountManagement.Business.Service.IAccountManagementService>(SecurityContext.ServerName))
      //{
      //  svc.Instance.Import();
      //}
    }

    protected override void AfterContainerConfigured()
    {
      EventAggregator.GetEvent<ContextChangedEvent>().Subscribe(OnExtendActivityContext);
      EventAggregator.GetEvent<ExtendDocumentEvent>().Subscribe(OnExtendDocumentEvent);
      //EventAggregator.GetEvent<GetGridButtonsEvent>().Subscribe(OnGetGridButtons);
      base.AfterContainerConfigured();
    }

    private void OnExtendDocumentEvent(ExtendDocumentEventArgs obj)
    {
      if (obj.Controller is AccountController)
      {
        obj.Controller.ExtendDocument<AccountExtension, AccountRatesViewModel>(Regions.TabRegion);
        obj.Controller.ExtendDocument<AccountExtension, FreightManagement.Prism.Extensions.Account.AccountHeaderViewModel>(Regions.HeaderRegion);
      }
    }

    //private void OnGetGridButtons(GetGridButtonsEventArgs obj)
    //{
    //  if (obj.ViewModel is WaybillCostComponentTypeManagementViewModel)
    //  {
    //    obj.Buttons.Add(new GridButtonViewModel("Set Pro Forma Tax Type", OnSetProFormaTaxTypeClick));
    //  }
    //}

    //private void OnSetProFormaTaxTypeClick(object obj)
    //{
    //  var ct = obj as FreightManagement.Business.WaybillChargeType;
    //  var c = CreateChildController<EditProFormaTaxTypeDialogController>();
    //  c.WaybillChargeType = ct;
    //  c.Run();
    //}

    #region NextWaybillRouteController NextWaybillRouteController

    public const string NextWaybillRouteControllerProperty = "NextWaybillRouteController";
    NextWaybillRouteController mNextWaybillRouteController;
    public NextWaybillRouteController NextWaybillRouteController
    {
      get { return mNextWaybillRouteController; }
      set { mNextWaybillRouteController = value; }
    }

    #endregion

    #region ManifestRouteController ManifestRouteController

    public const string ManifestRouteControllerProperty = "ManifestRouteController";
    ManifestRouteController mManifestRouteController;
    public ManifestRouteController ManifestRouteController
    {
      get { return mManifestRouteController; }
      set { mManifestRouteController = value; }
    }

    #endregion

    #region PODHandoverWaybillsController PODHandoverWaybillsController

    public const string PODHandoverWaybillsControllerProperty = "PODHandoverWaybillsController";
    PODHandoverWaybillsController mPODHandoverWaybillsController;
    public PODHandoverWaybillsController PODHandoverWaybillsController
    {
      get { return mPODHandoverWaybillsController; }
      set { mPODHandoverWaybillsController = value; }
    }

    #endregion

    protected override void OnRunning()
    {
      NextWaybillRouteController = GetCreateChildController<NextWaybillRouteController>(NextWaybillRouteContext.ControllerKey);
      NextWaybillRouteController.Run();

      ManifestRouteController = GetCreateChildController<ManifestRouteController>(ManifestRouteContext.ControllerKey);
      ManifestRouteController.Run();

      PODHandoverWaybillsController = GetCreateChildController<PODHandoverWaybillsController>(PODHandoverWaybillsContext.ControllerKey);
      PODHandoverWaybillsController.Run();

      base.OnRunning();
    }

    private void OnExtendActivityContext(ContextChangedEventArgs obj)
    {
      OwnedVehicleManagementController vmc = obj.Controller as OwnedVehicleManagementController;
      if (vmc != null)
      {
        Vehicle v = obj.Context as Vehicle;
        if (v != null)
        {
          vmc.AddDetailsViewModel(vmc.GetCreateViewModel<VehicleFreightDetailsViewModel>(v.GetExtensionObject<VehicleFreightDetails>(), vmc.ViewModel));
        }
      }

      LocationManagementController lmc = obj.Controller as LocationManagementController;
      if (lmc != null)
      {
        Location l = obj.Context as Location;
        if (l != null)
        {
          try
          {
            DeliveryLocation dl = l.GetExtensionObject<DeliveryLocation>();
            if (dl != null) lmc.AddDetailsViewModel(lmc.GetCreateViewModel<DeliveryLocationDetailViewModel>(dl, lmc.ViewModel));
          }
          catch
          {
            throw;
          }

          try
          {
            if (l.LocationType.SystemType == SystemLocationType.Country)
            {
              LocationExtension dl = l.GetExtensionObject<LocationExtension>();
              if (dl != null) lmc.AddDetailsViewModel(lmc.GetCreateViewModel<LocationExtensionViewModel>(dl, lmc.ViewModel));
            }
          }
          catch
          {
            throw;
          }
        }
      }
    }
  }
}

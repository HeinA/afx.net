﻿using Afx.Prism.RibbonMdi.Ribbon;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism
{
  [Export(typeof(IRibbonGroup))]
  public class ToolsGroup : RibbonGroup
  {
    public const string GroupName = "Tools";

    public override string TabName
    {
      get { return ReportingTab.TabName; }
    }

    public override string Name
    {
      get { return GroupName; }
    }

    public override int Index
    {
      get
      {
        return 1;
      }
    }
  }
}

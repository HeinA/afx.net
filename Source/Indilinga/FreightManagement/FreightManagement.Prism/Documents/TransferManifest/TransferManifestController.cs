﻿using Afx.Business;
using Afx.Business.Data;
using Afx.Business.Documents;
using Afx.Business.Security;
using Afx.Business.Service;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Documents;
using ClosedXML.Excel;
using DocumentManagement.Business;
using DocumentManagement.Prism.Extensions.DocumentRepository;
using FleetManagement.Prism.Dialogs.EditVehiclesDialog;
using FreightManagement.Business;
using FreightManagement.Business.Service;
using FreightManagement.Prism.Reports.TransferManifest;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace FreightManagement.Prism.Documents.TransferManifest
{
  public partial class TransferManifestController : MdiDocumentController<TransferManifestContext, TransferManifestViewModel>, IDocumentRepositoryAware
  {
    [InjectionConstructor]
    public TransferManifestController(IController controller)
      : base(controller)
    {
    }

    #region DocumentRepositoryExtensionViewModel DocumentRepositoryExtensionViewModel

    public const string DocumentRepositoryExtensionViewModelProperty = "DocumentRepositoryExtensionViewModel";
    DocumentRepositoryExtensionViewModel mDocumentRepositoryExtensionViewModel;
    public DocumentRepositoryExtensionViewModel DocumentRepositoryExtensionViewModel
    {
      get { return mDocumentRepositoryExtensionViewModel; }
      set { mDocumentRepositoryExtensionViewModel = value; }
    }

    #endregion

    public void AddScannedDocument(ScannedDocument document)
    {
      DocumentRepositoryExtension dre = DataContext.Document.GetExtensionObject<DocumentRepositoryExtension>();
      if (dre != null)
      {
        dre.ScannedDocuments.Add(document);
        RegionManager.Regions[TabRegion].Activate(DocumentRepositoryExtensionViewModel);
      }
    }

    #region TransferManifestScanViewModel ScanViewModel

    public const string ScanViewModelProperty = "ScanViewModel";
    TransferManifestScanViewModel mScanViewModel;
    public TransferManifestScanViewModel ScanViewModel
    {
      get { return mScanViewModel; }
      set { mScanViewModel = value; }
    }

    #endregion

    #region TransferManifestHeaderViewModel TransferManifestHeaderViewModel

    public const string TransferManifestHeaderViewModelProperty = "TransferManifestHeaderViewModel";
    TransferManifestHeaderViewModel mTransferManifestHeaderViewModel;
    public TransferManifestHeaderViewModel TransferManifestHeaderViewModel
    {
      get { return mTransferManifestHeaderViewModel; }
      set { mTransferManifestHeaderViewModel = value; }
    }

    #endregion

    protected override void ExtendDocument()
    {
      TransferManifestHeaderViewModel = ExtendDocument<TransferManifestHeaderViewModel>(HeaderRegion);
      ScanViewModel = ExtendDocument<TransferManifestScanViewModel>(HeaderRegion);

      ExtendDocument<TransferManifestWaybillsViewModel>(DockRegion);
      ExtendDocument<TransferManifestFooterViewModel>(FooterRegion);

      DocumentRepositoryExtensionViewModel = ExtendDocument<DocumentRepositoryExtension, DocumentRepositoryExtensionViewModel>(TabRegion);

      base.ExtendDocument();
    }

    public Document Document { get { return DataContext.Document; } }

    protected override void ExecuteOperation(Operation op, BusinessObject argument)
    {
      switch (op.Identifier)
      {
        case Operations.Verify:
          SetDocumentState(Business.TransferManifest.States.Verified);
          break;

        case Operations.EditVehicles:
          EditVehiclesDialogController evdc = GetCreateChildController<EditVehiclesDialogController>(EditVehiclesDialogController.EditVehiclesDialogControllerKey);
          evdc.VehicleCollection = DataContext.Document;
          if (!BusinessObject.IsNull(DataContext.Document.Courier)) evdc.Contractor = DataContext.Document.Courier;
          evdc.Run();
          break;

        case Operations.SBSRoadManifest:
          PrintRoadManifest();
          break;

        case Operations.PrintTripsheet:
          Print(preview: true, argument: TransferManifestInfo.PrintTripsheet);
          break;

        case Operations.PrintSummary:
          Print(preview: true, argument: TransferManifestInfo.PrintRestrictedSummary);
          break;

        case Operations.Cancel:
          SetDocumentState(Business.TransferManifest.States.Cancelled);
          break;
      }
      base.ExecuteOperation(op, argument);
    }

    public void AttachWaybill(string waybillNumber)
    {
      try
      {
        using (var svc = ProxyFactory.GetService<IFreightManagementService>(SecurityContext.ServerName))
        {
          var wr = svc.LoadWaybillReferenceByDocumentNumber(waybillNumber);
          if (wr != null)
          {
            if (wr.Parcels.Count > 0)
            {
              ScanViewModel.ErrorMessage = "Document must be loaded using parcel barcodes.";
              System.Media.SystemSounds.Exclamation.Play();
              return;
            }
            AttachWaybill(wr);
          }
          else
          {
            wr = svc.LoadWaybillReferenceByParcelNumber(waybillNumber);
            if (!VerifyWaybill(wr)) return;

            foreach (var p1 in DataContext.Document.Parcels)
            {
              if (!wr.Parcels.Any(p3 => p3.Barcode == p1.Barcode && p3.Owner.WaybillNumber == wr.WaybillNumber && !p3.IsIgnored))
              {
                //Delete parcels no longer on waybill / that is set to be ignored
                p1.IsDeleted = true;
              }
            }


            foreach (var p1 in wr.Parcels)
            {
              var p2 = DataContext.Document.Parcels.FirstOrDefault(p3 => p3.Barcode == p1.Barcode);
              if (p2 == null)
              {
                DataContext.Document.Parcels.Add(new TransferManifestParcel() { WaybillNumber = wr.WaybillNumber, Barcode = p1.Barcode, IsLoaded = p1.Barcode == waybillNumber });
              }
              else
              {
                if (p2.Barcode == waybillNumber)
                {
                  p2.IsDeleted = false;

                  if (p2.IsLoaded && MessageBox.Show("Package is already scanned.  Do you want to offload it?", "Offload?", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                  { 
                    p2.IsLoaded = false;

                  }
                  else
                  {
                    p2.IsLoaded = true;
                  }
                }
              }
            }


            if (DataContext.Document.Parcels.Any(p2 => !p2.IsDeleted && !p2.IsLoaded) && DataContext.Document.Waybills.Contains(wr))
            {
              AttachWaybill(wr);
              return;
            }

            if (!DataContext.Document.Parcels.Any(p2 => !p2.IsDeleted && !p2.IsLoaded) && !DataContext.Document.Waybills.Contains(wr))
            {
              AttachWaybill(wr);
            }
          }
        }
      }
      catch (Exception ex)
      {
        if (ExceptionHelper.HandleException(ex)) throw ex;
      }
      finally
      {
        TransferManifestHeaderViewModel.Refresh();
      }
    }

    void PrintRoadManifest()
    {
      Stream s = Assembly.GetExecutingAssembly().GetManifestResourceStream("FreightManagement.Prism.Documents.RoadManifest.xlsx");
      FileInfo fiFile = new FileInfo(string.Format("{0}{1}.xlsx", Path.GetTempPath(), Guid.NewGuid()));

      using (var wb = new XLWorkbook(s))
      {
        var worksheet = wb.Worksheets.FirstOrDefault();

        worksheet.Cell(8, 3).Value = DataContext.Document.DocumentDate;
        if (DataContext.Document.Vehicles.Count > 0)
        {
          worksheet.Cell(9, 3).Value = DataContext.Document.Vehicles[0].FleetNumber;
          worksheet.Cell(10, 3).Value = DataContext.Document.Vehicles[0].RegistrationNumber;
        }
        if (DataContext.Document.Driver != null) worksheet.Cell(13, 3).Value = DataContext.Document.Driver.Fullname;

        int line = 15;
        foreach (var l in DataContext.Document.Waybills)
        {
          worksheet.Cell(line, 1).Value = l.WaybillNumber;
          worksheet.Cell(line, 2).Value = l.Consigner;
          worksheet.Cell(line, 3).Value = l.Consignee;
          worksheet.Cell(line, 4).Value = l.DeliveryCity.Name;
          worksheet.Cell(line, 7).Value = l.PhysicalWeight;
          worksheet.Cell(line, 8).Value = l.Volume;
          worksheet.Cell(line, 9).Value = l.ClientReferenceNumber;
          worksheet.Cell(line, 10).Value = l.DeclaredValue;
          worksheet.Cell(line, 11).Value = l.ConsignerExportReference;
          worksheet.Cell(line, 12).Value = l.ConsigneeImportReference;
          line++;
        }

        wb.SaveAs(fiFile.FullName);
      }

      Process.Start(fiFile.FullName);
    }

    public bool VerifyWaybill(WaybillReference waybill)
    {
      ScanViewModel.ErrorMessage = string.Empty;

      try
      {
        if (BusinessObject.IsNull(waybill))
        {
          ScanViewModel.ErrorMessage = "Waybill not found.";
          System.Media.SystemSounds.Exclamation.Play();
          return false;
        }

        if (!waybill.Bay.AllowTransfer)
        {
          ScanViewModel.ErrorMessage = "Waybill may not be transferred.";
          System.Media.SystemSounds.Exclamation.Play();
          return false;
        }

        if (DataContext.Document.IsReadOnly)
        {
          ScanViewModel.ErrorMessage = "Document may not be edited.";
          System.Media.SystemSounds.Exclamation.Play();
          return false;
        }

        if (waybill.WaybillState.Identifier != Business.Waybill.States.OnFloor)
        {
          ScanViewModel.ErrorMessage = "Waybill must be on floor.";
          System.Media.SystemSounds.Exclamation.Play();
          return false;
        }

        if (DataContext.Document.State.DocumentTypeState.Identifier != Business.TransferManifest.States.Open)
        {
          ScanViewModel.ErrorMessage = "Document may not be edited.";
          System.Media.SystemSounds.Exclamation.Play();
          return false;
        }

        if (!BusinessObject.IsNull(DataContext.Document.OriginDC) && !DataContext.Document.OriginDC.Equals(waybill.CurrentDistributionCenter))
        {
          ScanViewModel.ErrorMessage = "Origin Distribution Centers do not match.";
          return false;
        }
        if (!BusinessObject.IsNull(DataContext.Document.DestinationDC) && !DataContext.Document.DestinationDC.Equals(waybill.NextDistributionCenter))
        {
          ScanViewModel.ErrorMessage = "Destination Distribution Centers do not match.";
          return false;
        }
      }
      catch (Exception ex)
      {
        if (ExceptionHelper.HandleException(ex)) throw ex;
      }

      return true;
    }

    public void AttachWaybill(WaybillReference waybill)
    {
      if (VerifyWaybill(waybill))
      {
        if (DataContext.Document.Waybills.Contains(waybill))
        {
          DataContext.Document.Waybills.Remove(waybill);
          if (DataContext.Document.Waybills.Count == 0)
          {
            DataContext.Document.OriginDC = null;
            DataContext.Document.DestinationDC = null;
          }
        }
        else
        {
          try
          {
            if (DataContext.Document.Waybills.Count == 0)
            {
              DataContext.Document.OriginDC = waybill.CurrentDistributionCenter;
              DataContext.Document.DestinationDC = waybill.NextDistributionCenter;
            }

            DataContext.Document.Waybills.Add(waybill);
          }
          catch (InvalidWaybillException ex)
          {
            ScanViewModel.ErrorMessage = ex.Message;
            System.Media.SystemSounds.Exclamation.Play();
          }
        }
      }
    }
  }
}


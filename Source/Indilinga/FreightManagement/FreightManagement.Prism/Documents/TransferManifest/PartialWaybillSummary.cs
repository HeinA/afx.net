﻿using Afx.Prism;
using FreightManagement.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Documents.TransferManifest
{
  public class PartialWaybillSummary : ViewModel
  {
    [InjectionConstructor]
    public PartialWaybillSummary(IController controller)
      : base(controller)
    {
    }

    public string WaybillNumber { get; set; }

    public IEnumerable<TransferManifestParcel> Parcels { get; set; }

    public int LoadedItems
    {
      get { return Parcels.Count(wr => !wr.IsDeleted && wr.IsLoaded); }
    }

    public int TotalItems
    {
      get { return Parcels.Count(wr => !wr.IsDeleted); }
    }

    public string OutstandingParcels
    {
      get { return string.Join(", ", Parcels.Where(p => !p.IsDeleted && !p.IsLoaded).Select(p => p.Barcode)); }
    }
  }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Documents.TransferManifest
{
  public class Operations : Afx.Prism.RibbonMdi.StandardOperations
  {
    public const string Cancel = "{355a5a69-0a68-4e19-9f1e-e9b32d4cc579}";
    public const string EditVehicles = "{9ec6d64c-ab89-45d4-9d1c-a8cf6299c5e0}";
    public const string PrintTripsheet = "{bbb7fb39-52e4-4879-8d99-7daf1f48f57d}";
    public const string PrintSummary = "{953ce768-c05c-4f65-976a-93b147159752}";
    public const string SBSRoadManifest = "{c68653b0-4e1c-4b49-b034-a1ef75bbb574}";
    public const string Verify = "{ffa6fd68-68e0-4316-abcc-dd2c896bf910}";
  }
}
﻿using Afx.Prism;
using Afx.Prism.RibbonMdi.Documents;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Documents.TransferManifest
{
  public class TransferManifestFooterViewModel : ExtensionViewModel<Business.TransferManifest>
  {
    #region Constructors

    [InjectionConstructor]
    public TransferManifestFooterViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

  }
}

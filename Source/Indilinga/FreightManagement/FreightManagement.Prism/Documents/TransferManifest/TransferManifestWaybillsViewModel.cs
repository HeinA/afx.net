﻿using Afx.Prism;
using Afx.Prism.RibbonMdi;
using Afx.Prism.RibbonMdi.Documents;
using FreightManagement.Business;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Documents.TransferManifest
{
  public class TransferManifestWaybillsViewModel : ExtensionViewModel<FreightManagement.Business.TransferManifest>
  {
    #region Constructors

    [InjectionConstructor]
    public TransferManifestWaybillsViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    #region WaybillReference SelectedWaybill

    public const string SelectedWaybillProperty = "SelectedWaybill";
    WaybillReference mSelectedWaybill;
    public WaybillReference SelectedWaybill
    {
      get { return mSelectedWaybill; }
      set { SetProperty<WaybillReference>(ref mSelectedWaybill, value); }
    }

    #endregion

    #region DelegateCommand<WaybillReference> OpenWaybillCommand
    
    DelegateCommand<WaybillReference> mOpenWaybillCommand;
    public DelegateCommand<WaybillReference> OpenWaybillCommand
    {
      get { return mOpenWaybillCommand ?? ( mOpenWaybillCommand = new DelegateCommand<WaybillReference>(ExecuteOpenWaybill, CanExecuteOpenWaybill)); }
    }
    
    bool CanExecuteOpenWaybill(WaybillReference args)
    {
      return true;
    }
    
    void ExecuteOpenWaybill(WaybillReference args)
    {
      MdiApplicationController.Current.EditDocument(Business.Waybill.DocumentTypeIdentifier, args.GlobalIdentifier);
    }
    
    #endregion

  }
}

﻿using Afx.Business;
using Afx.Business.Activities;
using Afx.Business.Data;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using Afx.Prism.RibbonMdi.Documents;
using Afx.Prism.RibbonMdi.Events;
using ContactManagement.Business;
using FleetManagement.Business;
using FreightManagement.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace FreightManagement.Prism.Documents.TransferManifest
{
  public class TransferManifestHeaderViewModel : DocumentHeaderViewModel<FreightManagement.Business.TransferManifest>
  {
    #region Constructors

    [InjectionConstructor]
    public TransferManifestHeaderViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    //TransferManifestController mTransferManifestController;
    //[Dependency]
    //public TransferManifestController TransferManifestController
    //{
    //  get { return mTransferManifestController; }
    //  set
    //  {
    //    mTransferManifestController = value;
    //    MdiApplicationController.Current.EventAggregator.GetEvent<DocumentSavedEvent>().Subscribe(OnSaved);
    //  }
    //}

    //private void OnSaved(DocumentSavedEventArgs obj)
    //{
    //  OnPropertyChanged(null);
    //}

    public IEnumerable<Contractor> Couriers
    {
      get { return Cache.Instance.GetObjects<Contractor>(true, c => (!BusinessObject.IsNull(c.ContractorType) && c.ContractorType.IsFlagged(ContractorTypeFlags.Is3rdPartyCourier)), c => c.CompanyName, true); }
    }

    protected override void OnModelPropertyChanged(string propertyName)
    {
      switch (propertyName)
      {
        case Business.DeliveryManifest.CourierProperty:
          Model.Vehicles.Clear();
          OnPropertyChanged("Drivers");
          OnPropertyChanged("Vehicles");
          break;
      }

      base.OnModelPropertyChanged(propertyName);
    }

    public IEnumerable<DistributionCenter> DistributionCenters
    {
      get
      {
        return Cache.Instance.GetObjects<DistributionCenter>(true, dc => dc.Name, true);
      }
    }

    public IEnumerable<SlotTime> SlotTimes
    {
      get { return Cache.Instance.GetObjects<SlotTime>(true, st => st.Text, true); }
    }

    public IEnumerable<Contact> Drivers
    {
      get
      {
        IEnumerable<Contact> drivers = null;
        if (BusinessObject.IsNull(Model.Courier)) drivers = Cache.Instance.GetObjects<Employee>(true, e => e.ContactType.IsFlagged(ContactTypeFlags.Driver), e => e.Fullname, true);
        else drivers = new Contact[] { new SimpleContact(true) }.Union(Model.Courier.Contacts.Where(e => e.ContactType.IsFlagged(ContactTypeFlags.Driver)).OrderBy(e => e.Fullname));
        return drivers;
      }
    }

    public IEnumerable<PartialWaybillSummary> PartialWaybillSummary
    {
      get
      {
        List<PartialWaybillSummary> list = new List<PartialWaybillSummary>();
        foreach (var w in Model.PartiallyLoadedWaybillParcels.Select(p1 => p1.WaybillNumber).Distinct())
        {
          list.Add(new PartialWaybillSummary(this.Controller) { WaybillNumber = w, Parcels = Model.PartiallyLoadedWaybillParcels.Where(p1 => p1.WaybillNumber == w) });
        }
        return list;
      }
    }

    public void Refresh()
    {
      OnPropertyChanged("PartialWaybillSummary");
    }
  }
}


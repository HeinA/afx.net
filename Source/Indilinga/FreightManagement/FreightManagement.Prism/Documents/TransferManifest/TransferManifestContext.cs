﻿using Afx.Business.Security;
using Afx.Business.Service;
using Afx.Prism.RibbonMdi.Documents;
using FreightManagement.Business;
using FreightManagement.Business.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Documents.TransferManifest
{
  public partial class TransferManifestContext : DocumentContext<FreightManagement.Business.TransferManifest>
  {
    public TransferManifestContext()
      : base(FreightManagement.Business.TransferManifest.DocumentTypeIdentifier)
    {
    }

    public TransferManifestContext(Guid documentIdentifier)
      : base(FreightManagement.Business.TransferManifest.DocumentTypeIdentifier, documentIdentifier)
    {
    }

    public override void SaveData()
    {
      using (var svc = ProxyFactory.GetService<IFreightManagementService>(SecurityContext.ServerName))
      {
        Document = svc.SaveTransferManifest(Document);
      }
      base.SaveData();
    }

    public override void LoadData()
    {
      using (var svc = ProxyFactory.GetService<IFreightManagementService>(SecurityContext.ServerName))
      {
        Document = svc.LoadTransferManifest(DocumentIdentifier);
      }
      base.LoadData();
    }
  }
}


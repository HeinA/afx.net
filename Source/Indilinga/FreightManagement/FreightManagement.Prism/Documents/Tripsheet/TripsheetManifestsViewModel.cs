﻿using Afx.Prism;
using Afx.Prism.RibbonMdi.Documents;
using FreightManagement.Business;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using FreightManagement.Business;
using FleetManagement.Business;
using Afx.Prism.RibbonMdi;
using FleetManagement.Prism.Dialogs.EditVehiclesDialog;
using Afx.Prism.Controls;

namespace FreightManagement.Prism.Documents.Tripsheet
{
  public class TripsheetManifestsViewModel : ExtensionViewModel<FreightManagement.Business.Tripsheet>
  {
    #region Constructors

    [InjectionConstructor]
    public TripsheetManifestsViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    [Dependency]
    public TripsheetController TripsheetController { get; set; }

    #region DelegateCommand<ItemActivatedEventArgs> ManifestActivatedCommand

    DelegateCommand<ItemActivatedEventArgs> mManifestActivatedCommand;
    public DelegateCommand<ItemActivatedEventArgs> ManifestActivatedCommand
    {
      get { return mManifestActivatedCommand ?? (mManifestActivatedCommand = new DelegateCommand<ItemActivatedEventArgs>(ExecuteManifestActivated, CanExecuteManifestActivated)); }
    }

    bool CanExecuteManifestActivated(ItemActivatedEventArgs args)
    {
      return true;
    }

    void ExecuteManifestActivated(ItemActivatedEventArgs args)
    {
    }

    #endregion

    #region string TitleText

    public string TitleText
    {
      get { return "Manifests"; }
    }

    #endregion
  }
}

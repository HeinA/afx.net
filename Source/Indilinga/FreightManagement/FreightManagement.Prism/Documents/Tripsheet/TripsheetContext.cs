﻿using Afx.Business.Security;
using Afx.Business.Service;
using Afx.Prism.RibbonMdi.Documents;
using FreightManagement.Business;
using FreightManagement.Business.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Documents.Tripsheet
{
  public partial class TripsheetContext : DocumentContext<FreightManagement.Business.Tripsheet>
  {
    public TripsheetContext()
      : base(FreightManagement.Business.Tripsheet.DocumentTypeIdentifier)
    {
    }

    public TripsheetContext(Guid documentIdentifier)
      : base(FreightManagement.Business.Tripsheet.DocumentTypeIdentifier, documentIdentifier)
    {
    }

    public override bool IsDirty
    {
      get
      {
        return base.IsDirty;
      }
      set
      {
        base.IsDirty = value;
      }
    }

    public override void SaveData()
    {
      using (var svc = ProxyFactory.GetService<IFreightManagementService>(SecurityContext.ServerName))
      {
        Document = svc.SaveTripsheet(Document);
      }
      base.SaveData();
    }

    public override void LoadData()
    {
      using (var svc = ProxyFactory.GetService<IFreightManagementService>(SecurityContext.ServerName))
      {
        Document = svc.LoadTripsheet(DocumentIdentifier);
      }
      base.LoadData();
    }
  }
}


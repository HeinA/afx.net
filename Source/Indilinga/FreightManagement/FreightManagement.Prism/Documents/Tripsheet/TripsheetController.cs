﻿using Afx.Business;
using Afx.Business.Security;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Documents;
using DocumentManagement.Business;
using DocumentManagement.Prism.Extensions.DocumentRepository;
using FreightManagement.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FreightManagement.Business;
using FleetManagement.Prism.Dialogs.EditVehiclesDialog;
using Afx.Business.Service;
using FreightManagement.Business.Service;
using Afx.Business.Documents;

namespace FreightManagement.Prism.Documents.Tripsheet
{
  public partial class TripsheetController : MdiDocumentController<TripsheetContext, TripsheetViewModel>, IDocumentRepositoryAware
  {
    [InjectionConstructor]
    public TripsheetController(IController controller)
      : base(controller)
    {
    }


    #region TripsheetManifestsViewModel TripsheetManifestsViewModel

    public const string TripsheetRoutesViewModelProperty = "TripsheetManifestsViewModel";
    TripsheetManifestsViewModel mTripsheetManifestsViewModel;
    public TripsheetManifestsViewModel TripsheetManifestsViewModel
    {
      get { return mTripsheetManifestsViewModel; }
      set { mTripsheetManifestsViewModel = value; }
    }

    #endregion

    #region TripsheetManifestScanViewModel TripsheetManifestScanViewModel

    public const string TripsheetManifestScanViewModelProperty = "TripsheetManifestScanViewModel";
    TripsheetManifestScanViewModel mTripsheetManifestScanViewModel;
    public TripsheetManifestScanViewModel TripsheetManifestScanViewModel
    {
      get { return mTripsheetManifestScanViewModel; }
      set { mTripsheetManifestScanViewModel = value; }
    }

    #endregion

    protected override void OnDataContextPropertyChanged(string propertyName)
    {
      switch (propertyName)
      {
        case "Document":
          //if (TripsheetViewModel != null) TripsheetViewModel.Refresh();
          break;
      }

      base.OnDataContextPropertyChanged(propertyName);
    }

    protected override void ExtendDocument()
    {
      ExtendDocument<TripsheetHeaderViewModel>(HeaderRegion);
      TripsheetManifestScanViewModel = ExtendDocument<TripsheetManifestScanViewModel>(HeaderRegion);

      TripsheetManifestsViewModel = ExtendDocument<TripsheetManifestsViewModel>(DockRegion);
      ExtendDocument<DocumentNotesViewModel>(TabRegion);

      DocumentRepositoryExtensionViewModel = ExtendDocument<DocumentRepositoryExtension, DocumentRepositoryExtensionViewModel>(TabRegion);


      base.ExtendDocument();
    }

    protected override void ExecuteOperation(Operation op, BusinessObject argument)
    {
      switch (op.Identifier)
      {
        case Operations.Finalize:
          SetDocumentState(Business.Tripsheet.States.Closed);
          break;

        case Operations.Reopen:
          SetDocumentState(Business.Tripsheet.States.Open);
          break;

        case Operations.EditVehicles:
          EditVehiclesDialogController evdc = GetCreateChildController<EditVehiclesDialogController>(EditVehiclesDialogController.EditVehiclesDialogControllerKey);
          evdc.VehicleCollection = DataContext.Document;
          evdc.Run();
          break;
      }

      base.ExecuteOperation(op, argument);
    }

    #region DocumentRepositoryExtensionViewModel DocumentRepositoryExtensionViewModel

    public const string DocumentRepositoryExtensionViewModelProperty = "DocumentRepositoryExtensionViewModel";
    DocumentRepositoryExtensionViewModel mDocumentRepositoryExtensionViewModel;
    public DocumentRepositoryExtensionViewModel DocumentRepositoryExtensionViewModel
    {
      get { return mDocumentRepositoryExtensionViewModel; }
      set { mDocumentRepositoryExtensionViewModel = value; }
    }

    #endregion

    public void AddScannedDocument(ScannedDocument document)
    {
      DocumentRepositoryExtension dre = DataContext.Document.GetExtensionObject<DocumentRepositoryExtension>();
      if (dre != null)
      {
        dre.ScannedDocuments.Add(document);
        RegionManager.Regions[TabRegion].Activate(DocumentRepositoryExtensionViewModel);
      }
    }

    public Document Document { get { return DataContext.Document; } }

    public void AttachManifest(string manifestNumber)
    {
      try
      {
        using (var svc = ServiceFactory.GetService<IFreightManagementService>(SecurityContext.ServerName))
        {
          AttachManifest(svc.Instance.LoadManifestReferenceByDocumentNumber(manifestNumber));
        }
      }
      catch (Exception ex)
      {
        if (ExceptionHelper.HandleException(ex)) throw ex;
      }
    }

    public void AttachManifest(ManifestReference manifest)
    {
      try
      {
        if (DataContext.Document.Manifests.Contains(manifest))
        {
          DataContext.Document.Manifests.Remove(manifest);
          return;
        }

        TripsheetManifestScanViewModel.ErrorMessage = string.Empty;

        if (BusinessObject.IsNull(manifest))
        {
          TripsheetManifestScanViewModel.ErrorMessage = "Invalid Manifest";
          System.Media.SystemSounds.Exclamation.Play();
          return;
        }

        if (DataContext.Document.IsReadOnly)
        {
          System.Media.SystemSounds.Exclamation.Play();
          return;
        }

        DataContext.Document.Manifests.Add(manifest);

      }
      catch (Exception ex)
      {
        if (ExceptionHelper.HandleException(ex)) throw ex;
      }
    }
  }
}


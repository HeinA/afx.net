﻿using Afx.Business;
using Afx.Business.Activities;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Documents;
using FreightManagement.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace FreightManagement.Prism.Documents.RatesConfiguration
{
  public class RatesConfigurationHeaderViewModel : ExtensionViewModel<FreightManagement.Business.RatesConfiguration>
  {
    [Dependency]
    public RatesConfigurationController RatesConfigurationController { get; set; }

    #region Constructors

    [InjectionConstructor]
    public RatesConfigurationHeaderViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    #region void Refresh()

    public void Refresh()
    {
      mSelectedRevision = null;
      OnPropertyChanged(null);
    }

    #endregion 

    #region RatesConfigurationRevision SelectedRevision

    bool hastried = false;
    RatesConfigurationRevision mSelectedRevision;
    public RatesConfigurationRevision SelectedRevision
    {
      get
      {
        try
        {
          if (Model == null)
            return null;

          if (mSelectedRevision == null && !hastried)
          {
            if (!Model.Revisions.Any(r => r.EffectiveDate.Date <= DateTime.Now.Date))
            {
              SelectedRevision = Model.Revisions.OrderByDescending(r => r.EffectiveDate).FirstOrDefault();
            }
            else
            {
              SelectedRevision = Model.Revisions.Where(r => r.EffectiveDate.Date <= DateTime.Now.Date).OrderByDescending(r => r.EffectiveDate).FirstOrDefault();
            }
            hastried = true;
          }
          return mSelectedRevision;
        }
        catch
        {
          throw;
        }
      }
      set
      {
        mSelectedRevision = value;
        if (SelectedRevision != null)
        {
          RatesConfigurationController.RatesConfigurationContentViewModel.RevisionViewModel = RatesConfigurationController.GetCreateViewModel<RatesConfigurationRevisionViewModel>(SelectedRevision);
        }
      }
    }

    #endregion
  }
}


﻿using Afx.Prism;
using FreightManagement.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace FreightManagement.Prism.Documents.RatesConfiguration
{
  public class RatesConfigurationRevisionLevyTypeViewModel : ViewModel<RatesConfigurationRevisionLevyType>
  {
    #region Constructors

    [InjectionConstructor]
    public RatesConfigurationRevisionLevyTypeViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    public Visibility RateVisibility
    {
      get
      {
        if (Model.Reference.LevyCalculationType == LevyCalculationType.FixedPercentage) return Visibility.Visible;
        return Visibility.Collapsed;
      }
    }
  }
}

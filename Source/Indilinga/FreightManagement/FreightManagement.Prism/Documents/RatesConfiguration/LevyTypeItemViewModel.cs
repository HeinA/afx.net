﻿using Afx.Prism;
using FreightManagement.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Documents.RatesConfiguration
{
  public class LevyTypeItemViewModel : SelectableViewModel<WaybillLevyType>
  {
    #region Constructors

    [InjectionConstructor]
    public LevyTypeItemViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    new RatesConfigurationRevisionViewModel Parent
    {
      get { return (RatesConfigurationRevisionViewModel)base.Parent; }
    }

    #region bool IsChecked

    public bool IsChecked
    {
      get { return Parent.Model.LevyTypes.Contains(Model); }
      set
      {
        if (value)
        {
          if (!Parent.Model.LevyTypes.Contains(Model)) Parent.Model.LevyTypes.Add(Model);
        }
        else
        {
          if (Parent.Model.LevyTypes.Contains(Model)) Parent.Model.LevyTypes.Remove(Model);
        }
      }
    }

    #endregion
  }
}

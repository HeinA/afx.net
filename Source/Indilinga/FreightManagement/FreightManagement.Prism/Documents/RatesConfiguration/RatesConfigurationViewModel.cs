﻿using Afx.Business;
using Afx.Business.Data;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Documents;
using FreightManagement.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows;

namespace FreightManagement.Prism.Documents.RatesConfiguration
{
  public partial class RatesConfigurationViewModel : MdiDocumentViewModel<RatesConfigurationContext>
  {
    [InjectionConstructor]
    public RatesConfigurationViewModel(IController controller)
      : base(controller)
    {
    }

    //public override System.Windows.Visibility DocumentNumberVisibility
    //{
    //  get { return Visibility.Collapsed; }
    //}

    //public override Visibility DocumentDateVisibility
    //{
    //  get { return Visibility.Collapsed; }
    //}

    //public override Visibility OrganizationalUnitVisibility
    //{
    //  get { return Visibility.Collapsed; }
    //}
  }
}


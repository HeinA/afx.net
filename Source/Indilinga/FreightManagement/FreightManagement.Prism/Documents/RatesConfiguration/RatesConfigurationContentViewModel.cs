﻿using Afx.Prism;
using Afx.Prism.RibbonMdi.Documents;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Documents.RatesConfiguration
{
  public class RatesConfigurationContentViewModel : ExtensionViewModel<FreightManagement.Business.RatesConfiguration>
  {
    #region Constructors

    [InjectionConstructor]
    public RatesConfigurationContentViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    #region RatesConfigurationRevisionViewModel RevisionViewModel

    public const string RevisionViewModelProperty = "RevisionViewModel";
    RatesConfigurationRevisionViewModel mRevisionViewModel;
    public RatesConfigurationRevisionViewModel RevisionViewModel
    {
      get { return mRevisionViewModel; }
      set { SetProperty<RatesConfigurationRevisionViewModel>(ref mRevisionViewModel, value); }
    }

    #endregion
  }
}

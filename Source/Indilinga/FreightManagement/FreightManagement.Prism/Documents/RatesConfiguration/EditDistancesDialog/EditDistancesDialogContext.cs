﻿using Afx.Business;
using Afx.Business.Collections;
using FreightManagement.Business;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Documents.RatesConfiguration.EditDistancesDialog
{
  public class EditDistancesDialogContext : BusinessObject
  {
    BasicCollection<DistanceRateTableDistanceCategory> mDistances;
    public BasicCollection<DistanceRateTableDistanceCategory> Distances
    {
      get { return mDistances ?? (mDistances = new BasicCollection<DistanceRateTableDistanceCategory>()); }
    }
  }
}
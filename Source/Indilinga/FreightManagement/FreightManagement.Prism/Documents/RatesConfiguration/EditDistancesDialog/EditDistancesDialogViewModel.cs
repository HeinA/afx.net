﻿using Afx.Prism;
using Afx.Prism.RibbonMdi;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Documents.RatesConfiguration.EditDistancesDialog
{
  public class EditDistancesDialogViewModel : MdiDialogViewModel<EditDistancesDialogContext>
  {
    [InjectionConstructor]
    public EditDistancesDialogViewModel(IController controller)
      : base(controller)
    {
    }
  }
}

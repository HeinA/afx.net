﻿using Afx.Prism;
using Afx.Prism.RibbonMdi;
using FreightManagement.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace FreightManagement.Prism.Documents.RatesConfiguration.EditDistancesDialog
{
  public class EditDistancesDialogController : MdiDialogController<EditDistancesDialogContext, EditDistancesDialogViewModel>
  {
    public const string EditDistancesDialogControllerKey = "FreightManagement.Prism.Documents.RatesConfiguration.EditDistancesDialog.EditDistancesDialogController";

    public EditDistancesDialogController(IController controller)
      : base(EditDistancesDialogControllerKey, controller)
    {
    }

    protected override bool OnRun()
    {
      if (DataContext == null) DataContext = new EditDistancesDialogContext();
      ViewModel.Caption = "Edit Distances";

      foreach (var c in DistanceRateTable.Rows.OfType<DistanceRateTableDistanceCategory>())
      {
        DataContext.Distances.Add((DistanceRateTableDistanceCategory)c.Clone());
      }

      return base.OnRun();
    }

    protected override void ApplyChanges()
    {
      foreach (var c in DistanceRateTable.Rows)
      {
        if (!DataContext.Distances.Where(c1 => !c1.IsDeleted).Contains(c))
        {
          c.IsDeleted = true;
        }
      }

      foreach (var c in DataContext.Distances)
      {
        var c1 = DistanceRateTable.Rows.FirstOrDefault(c2 => c2.Equals(c));
        if (c1 != null)
        {
          c1.Distance = c.Distance;
          c1.IsDeleted = c.IsDeleted;
        }
        else
        {
          DistanceRateTable.AddRow(c);
        }
      }

      base.ApplyChanges();
    }

    #region DistanceRateTable DistanceRateTable

    public const string DistanceRateTableProperty = "DistanceRateTable";
    DistanceRateTable mDistanceRateTable;
    public DistanceRateTable DistanceRateTable
    {
      get { return mDistanceRateTable; }
      set { mDistanceRateTable = value; }
    }

    #endregion
  }
}

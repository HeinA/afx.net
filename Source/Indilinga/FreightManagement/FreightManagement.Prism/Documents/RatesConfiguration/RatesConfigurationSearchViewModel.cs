﻿using Afx.Business;
using Afx.Business.Data;
using Afx.Business.Documents;
using Afx.Business.Security;
using Afx.Business.Service;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Tools;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Documents.RatesConfiguration
{
  [Export(typeof(SearchItemViewModel))]
  public partial class RatesConfigurationSearchViewModel : SearchItemViewModel
  {
    #region Constructors

    [InjectionConstructor]
    public RatesConfigurationSearchViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    #region DocumentType DocumentType

    public override DocumentType DocumentType
    {
      get { return Cache.Instance.GetObject<DocumentType>(FreightManagement.Business.RatesConfiguration.DocumentTypeIdentifier); }
    }

    #endregion

    #region string Name

    string mName;
    public string ConfigurationName
    {
      get { return mName; }
      set { SetProperty<string>(ref mName, value); }
    }

    #endregion

    #region DocumentTypeState DocumentTypeState

    public IEnumerable<DocumentTypeState> DocumentTypeStates
    {
      get { return new List<DocumentTypeState>(new DocumentTypeState[] { new DocumentTypeState(true) }).Union(Cache.Instance.GetObject<DocumentType>(FreightManagement.Business.RatesConfiguration.DocumentTypeIdentifier).HeirarchyStates.OrderBy(dts => dts.Name)); }
    }

    DocumentTypeState mDocumentTypeState = new DocumentTypeState(true);
    public DocumentTypeState DocumentTypeState
    {
      get { return mDocumentTypeState; }
      set { SetProperty<DocumentTypeState>(ref mDocumentTypeState, value); }
    }

    #endregion

    #region SearchResults ExecuteSearch()

    protected override SearchResults ExecuteSearch()
    {
      using (var svc = ServiceFactory.GetService<IAfxService>(SecurityContext.Server.Name))
      {
        SearchDatagraph sg = new SearchDatagraph(typeof(FreightManagement.Business.RatesConfiguration))
          .Select(Business.RatesConfiguration.NameProperty, "Name")
          .Join(Document.StateProperty)
            .Join(DocumentState.DocumentTypeStateProperty)
              .Select(DocumentTypeState.NameProperty, "State")
              .Where(DocumentTypeState.NameProperty, FilterType.Equals, DocumentTypeState.Name, !string.IsNullOrWhiteSpace(DocumentTypeState.Name))
              .EndJoin()
            .EndJoin()
          .Select(Business.RatesConfiguration.IsStandardConfigurationProperty)
          .Where(Business.RatesConfiguration.NameProperty, FilterType.Like, string.Format("%{0}%", ConfigurationName), !string.IsNullOrWhiteSpace(ConfigurationName))
          .Where(Business.RatesConfiguration.IsStandardConfigurationProperty, FilterType.Equals, false, true);

        return svc.Instance.Search(sg);
      }
    }

    #endregion
  }
}

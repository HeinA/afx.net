﻿using Afx.Business;
using Afx.Business.Security;
using Afx.Business.Service;
using Afx.Prism.RibbonMdi.Documents;
using FreightManagement.Business;
using FreightManagement.Business.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Documents.RatesConfiguration
{
  public partial class RatesConfigurationContext : DocumentContext<FreightManagement.Business.RatesConfiguration>
  {
    public RatesConfigurationContext()
      : base(FreightManagement.Business.RatesConfiguration.DocumentTypeIdentifier)
    {
    }

    public RatesConfigurationContext(Guid documentIdentifier)
      : base(FreightManagement.Business.RatesConfiguration.DocumentTypeIdentifier, documentIdentifier)
    {
    }

    protected override Business.RatesConfiguration CreateDocument()
    {
      Business.RatesConfiguration doc = base.CreateDocument();
      if (doc.Revisions.Count == 0) doc.Revisions.Add(new RatesConfigurationRevision());

      if (Document != null && Document.IsStandardConfiguration)
      {
        doc.IsStandardConfiguration = true;
        doc.Name = "Standard Rates";
      }

      return doc;
    }

    public override void SaveData()
    {
      using (var svc = ProxyFactory.GetService<IFreightManagementService>(SecurityContext.Server.Name))
      {
        Document = svc.SaveRatesConfiguration(Document);
      }
      base.SaveData();
    }

    public override void LoadData()
    {
      using (var svc = ProxyFactory.GetService<IFreightManagementService>(SecurityContext.Server.Name))
      {
        Document = svc.LoadRatesConfiguration(DocumentIdentifier);
      }
      base.LoadData();
    }

    public override string TitleText
    {
      get
      {
        if (BusinessObject.IsNull(Document)) return "Rates Configuration";
        if (string.IsNullOrWhiteSpace(Document.Name)) return "Rates Configuration";
        return Document.Name;
      }
    }
  }
}


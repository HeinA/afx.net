﻿using Afx.Business;
using Afx.Business.Data;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using Afx.Prism.RibbonMdi.Documents;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using FreightManagement.Business;
using Geographics.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Documents.RatesConfiguration
{
  public partial class RatesConfigurationController : MdiDocumentController<RatesConfigurationContext, RatesConfigurationViewModel>
  {
    [InjectionConstructor]
    public RatesConfigurationController(IController controller)
      : base(controller)
    {
    }

    [Dependency]
    public FreightManagementModuleController FreightManagementModuleController { get; set; }

    #region RatesConfigurationContentViewModel RatesConfigurationContentViewModel

    public const string RatesConfigurationContentViewModelProperty = "RatesConfigurationContentViewModel";
    RatesConfigurationContentViewModel mRatesConfigurationContentViewModel;
    public RatesConfigurationContentViewModel RatesConfigurationContentViewModel
    {
      get { return mRatesConfigurationContentViewModel; }
      set { mRatesConfigurationContentViewModel = value; }
    }

    #endregion

    #region RatesConfigurationHeaderViewModel RatesConfigurationHeaderViewModel

    public const string RatesConfigurationHeaderViewModelProperty = "RatesConfigurationHeaderViewModel";
    RatesConfigurationHeaderViewModel mRatesConfigurationHeaderViewModel;
    public RatesConfigurationHeaderViewModel RatesConfigurationHeaderViewModel
    {
      get { return mRatesConfigurationHeaderViewModel; }
      set { mRatesConfigurationHeaderViewModel = value; }
    }

    #endregion

    protected override void ExtendDocument()
    {
      RatesConfigurationHeaderViewModel = ExtendDocument<RatesConfigurationHeaderViewModel>(HeaderRegion);
      RatesConfigurationContentViewModel = ExtendDocument<RatesConfigurationContentViewModel>(DockRegion);

      base.ExtendDocument();
    }

    public override void OnRefresh()
    {
      base.OnRefresh();
      RatesConfigurationHeaderViewModel.Refresh();
    }

    protected override void OnSaved(bool userClickedSaved)
    {
      base.OnSaved(userClickedSaved);
      RatesConfigurationHeaderViewModel.Refresh();
    }

    protected override void ExecuteOperation(Operation op, BusinessObject argument)
    {
      switch (op.Identifier)
      {
        case Operations.AddRevision:
          AddRevisionDialog.AddRevisionDialogController c = CreateChildController<AddRevisionDialog.AddRevisionDialogController>();
          c.RatesConfiguration = DataContext.Document;
          c.SelectedRevision = RatesConfigurationHeaderViewModel.SelectedRevision;
          c.Run();
          break;

        case Operations.Copy:
          Business.RatesConfiguration rc = new Business.RatesConfiguration(DataContext.Document);
          MdiApplicationController.Current.EditDocument(rc);
          break;
      }

      base.ExecuteOperation(op, argument);
    }

    #region Printing

    #region bool OnBeforePrint(...)

    protected override bool OnBeforePrint()
    {
      string filepath = string.Format("{0}{1}.docx", Path.GetTempPath(), DataContext.Document.Name);

      string name = DataContext.Document.Name;
      var selectedRevision = RatesConfigurationHeaderViewModel.SelectedRevision;

      using (WordprocessingDocument wordprocessingDocument = WordprocessingDocument.Create(filepath, DocumentFormat.OpenXml.WordprocessingDocumentType.Document, true))
      {
        var mdp = wordprocessingDocument.AddMainDocumentPart();
        var doc = mdp.Document = new Document();
        var body = doc.Body = new Body();

        Paragraph para = body.AppendChild(new Paragraph());
        para.AppendChild(new ParagraphProperties(new Justification() { Val = JustificationValues.Right }));

        Run run = para.AppendChild(new Run());
        run.AppendChild(new RunProperties(new Bold()));
        run.AppendChild(new Text(string.Format("{0}{1}", name, name.Substring(name.Length - 5).ToUpperInvariant() == "RATES" ? string.Empty : "Rates")));

        run = para.AppendChild(new Run());
        run.AppendChild(new Text(string.Format(", {0:D}", selectedRevision.EffectiveDate)) { Space = SpaceProcessingModeValues.Preserve });

        Collection<WaybillCostComponentType> fees = new Collection<WaybillCostComponentType>();

        foreach (var ct in selectedRevision.CargoTypes.OrderBy(ct1 => ct1.Text))
        {
          para = body.AppendChild(new Paragraph());
          run = para.AppendChild(new Run());
          run.AppendChild(new RunProperties(new Bold(), new FontSize() { Val = new StringValue("10pt") }));
          run.AppendChild(new Text(ct.Text));

          var rcrct = selectedRevision.GetAssociativeObject<RatesConfigurationRevisionCargoType>(ct);

          foreach (var rt in rcrct.Tables)
          {
            PrintTable(rt, body, fees);
          }
        }

        para = body.AppendChild(new Paragraph());
        run = para.AppendChild(new Run());
        run.AppendChild(new RunProperties(new Bold(), new FontSize() { Val = new StringValue("10pt") }));
        run.AppendChild(new Text("Total Waybill Weight"));

        foreach (var rt in selectedRevision.TablesByWeight)
        {
          PrintTable(rt, body, fees);
        }

        foreach (var lt in selectedRevision.LevyTypes)
        {
          if (lt.LevyCalculationType == LevyCalculationType.FixedPercentage)
          {
            para = body.AppendChild(new Paragraph());
            run = para.AppendChild(new Run());
            run.AppendChild(new RunProperties(new FontSize() { Val = new StringValue("10pt") }));
            run.AppendChild(new Text(string.Format("A {0} of {1:0.00}% will be applied to the transport cost.", lt.Text, selectedRevision.GetAssociativeObject<RatesConfigurationRevisionLevyType>(lt).Rate)));
          }
        }

        foreach (var f in fees)
        {
          para = body.AppendChild(new Paragraph());
          run = para.AppendChild(new Run());
          run.AppendChild(new RunProperties(new FontSize() { Val = new StringValue("10pt") }));
          run.AppendChild(new Text("1: Fee is only charged once per waybill."));
        }

        foreach (var location in Cache.Instance.GetObjects<Location>(l => l.LocationType.SystemType == SystemLocationType.Zone).Select(l => l.Owner).Distinct())
        {
          PrintZone(location, body);
        }
      }

      Process.Start(filepath);

      return false;
    }

    #endregion

    #region void PrintZone(...)

    void PrintZone(Location zoneOwner, Body body)
    {
      Paragraph para = body.AppendChild(new Paragraph());
      SpacingBetweenLines spacing = new SpacingBetweenLines() { Line = "240", LineRule = LineSpacingRuleValues.Auto, Before = "0", After = "0" };
      ParagraphProperties paragraphProperties = new ParagraphProperties();
      paragraphProperties.Append(spacing);
      para.Append(paragraphProperties);

      Run run = para.AppendChild(new Run());
      run.AppendChild(new RunProperties(new FontSize() { Val = new StringValue("10pt") }));
      run.AppendChild(new Text(string.Format("Zone definitions for {0}", zoneOwner.Name)));

      para = body.AppendChild(new Paragraph());
      run = para.AppendChild(new Run());
      Table tbl = new Table();

      TableProperties tableProp = new TableProperties();
      TableStyle tableStyle = new TableStyle() { Val = "TableGrid" };
      TableWidth tableWidth = new TableWidth() { Width = "5000", Type = TableWidthUnitValues.Pct };

      TableBorders tblBorders = new TableBorders();

      TopBorder topBorder = new TopBorder();
      topBorder.Val = new EnumValue<BorderValues>(BorderValues.Thick);
      topBorder.Color = "000000";
      tblBorders.AppendChild(topBorder);

      BottomBorder bottomBorder = new BottomBorder();
      bottomBorder.Val = new EnumValue<BorderValues>(BorderValues.Thick);
      bottomBorder.Color = "000000";
      tblBorders.AppendChild(bottomBorder);

      RightBorder rightBorder = new RightBorder();
      rightBorder.Val = new EnumValue<BorderValues>(BorderValues.Thick);
      rightBorder.Color = "000000";
      tblBorders.AppendChild(rightBorder);

      LeftBorder leftBorder = new LeftBorder();
      leftBorder.Val = new EnumValue<BorderValues>(BorderValues.Thick);
      leftBorder.Color = "000000";
      tblBorders.AppendChild(leftBorder);

      InsideHorizontalBorder insideHBorder = new InsideHorizontalBorder();
      insideHBorder.Val = new EnumValue<BorderValues>(BorderValues.Thick);
      insideHBorder.Color = "000000";
      tblBorders.AppendChild(insideHBorder);

      InsideVerticalBorder insideVBorder = new InsideVerticalBorder();
      insideVBorder.Val = new EnumValue<BorderValues>(BorderValues.Thick);
      insideVBorder.Color = "000000";
      tblBorders.AppendChild(insideVBorder);

      TableGrid tg = new TableGrid(new GridColumn(), new GridColumn());
      tbl.AppendChild(tg);

      tableProp.Append(tableStyle, tableWidth, tblBorders);
      tbl.AppendChild(tableProp);


      foreach (var zone in zoneOwner.SubLocations.Where(l => l.LocationType.SystemType == SystemLocationType.Zone).OrderBy(l => l.Name))
      {
        TableRow tr1 = new TableRow();
        string zoneLocations = string.Join(", ", zone.SubLocations.OrderBy(z => z.Name).Select(z => z.Name));
        tr1.Append(new TableCell(CreateCompactParagraphSmallText(zone.Name)), new TableCell(CreateCompactParagraphSmallText(zoneLocations)));
        tbl.AppendChild(tr1);
      }

      run.AppendChild(tbl);
    }

    #endregion

    #region void PrintTable(...)

    void PrintTable(RateTable rt, Body body, Collection<WaybillCostComponentType> fees)
    {
      Paragraph para = body.AppendChild(new Paragraph());
      SpacingBetweenLines spacing = new SpacingBetweenLines() { Line = "240", LineRule = LineSpacingRuleValues.Auto, Before = "0", After = "0" };
      ParagraphProperties paragraphProperties = new ParagraphProperties();
      paragraphProperties.Append(spacing);
      para.Append(paragraphProperties);

      Run run = para.AppendChild(new Run());
      run.AppendChild(new RunProperties(new FontSize() { Val = new StringValue("10pt") }));

      if (rt.IsOmnidirectional)
      {
        run.AppendChild(new Text(string.Format("Between {0} and the following:", GetShortLocationName(rt.Origin))));
      }
      else
      {
        run.AppendChild(new Text(string.Format("From {0} to the following:", GetShortLocationName(rt.Origin))));
      }

      para = body.AppendChild(new Paragraph());
      run = para.AppendChild(new Run());

      Table tbl = new Table();

      TableProperties tableProp = new TableProperties();
      TableStyle tableStyle = new TableStyle() { Val = "TableGrid" };
      TableWidth tableWidth = new TableWidth() { Width = "5000", Type = TableWidthUnitValues.Pct };

      TableBorders tblBorders = new TableBorders();

      TopBorder topBorder = new TopBorder();
      topBorder.Val = new EnumValue<BorderValues>(BorderValues.Thick);
      topBorder.Color = "000000";
      tblBorders.AppendChild(topBorder);

      BottomBorder bottomBorder = new BottomBorder();
      bottomBorder.Val = new EnumValue<BorderValues>(BorderValues.Thick);
      bottomBorder.Color = "000000";
      tblBorders.AppendChild(bottomBorder);

      RightBorder rightBorder = new RightBorder();
      rightBorder.Val = new EnumValue<BorderValues>(BorderValues.Thick);
      rightBorder.Color = "000000";
      tblBorders.AppendChild(rightBorder);

      LeftBorder leftBorder = new LeftBorder();
      leftBorder.Val = new EnumValue<BorderValues>(BorderValues.Thick);
      leftBorder.Color = "000000";
      tblBorders.AppendChild(leftBorder);

      InsideHorizontalBorder insideHBorder = new InsideHorizontalBorder();
      insideHBorder.Val = new EnumValue<BorderValues>(BorderValues.Thick);
      insideHBorder.Color = "000000";
      tblBorders.AppendChild(insideHBorder);

      InsideVerticalBorder insideVBorder = new InsideVerticalBorder();
      insideVBorder.Val = new EnumValue<BorderValues>(BorderValues.Thick);
      insideVBorder.Color = "000000";
      tblBorders.AppendChild(insideVBorder);

      tableProp.Append(tableStyle, tableWidth, tblBorders);
      tbl.AppendChild(tableProp);

      Collection<OpenXmlElement> columns = new Collection<OpenXmlElement>();
      int columnCount = rt.Columns.Count() + 1;
      for (int i = 0; i < columnCount; i++)
      {
        GridColumn gc = new GridColumn();
        columns.Add(gc);
      }

      TableGrid tg = new TableGrid(columns);
      tbl.AppendChild(tg);


      TableRow tr1 = new TableRow();
      Collection<OpenXmlElement> cells = new Collection<OpenXmlElement>();
      cells.Add(new TableCell(CreateCompactParagraph()));

      foreach (var tc in rt.Columns)
      {
        RateTableCategory rtc = tc as RateTableCategory;
        RateTableFee rtf = tc as RateTableFee;
        if (rtc != null)
        {
          if (rt.Columns.OfType<RateTableCategory>().Count() == 1)
          {
            cells.Add(new TableCell(CreateCompactParagraphSmallText(string.Format("Per {0}", rtc.Owner.UnitOfMeasure))));
          }
          else
          {
            if (rtc.IsMinimumCharge)
            {
              cells.Add(new TableCell(CreateCompactParagraphSmallText("Minimum Charge")));
            }
            else
            {
              cells.Add(new TableCell(CreateCompactParagraphSmallText(string.Format("≥ {0} {1}{2}{3}", rtc.StartUnit, rtc.Owner.UnitOfMeasure, rtc.IsFixedRate ? " Fixed" : string.Empty, rtc.ThereAfter ? " There After" : string.Empty))));
            }
          }
        }

        if (rtf != null)
        {
          if (!fees.Contains(rtf.ChargeType) && rtf.ChargeType.ChargeOnce) fees.Add(rtf.ChargeType);
          cells.Add(new TableCell(CreateCompactParagraphSmallText(string.Format("{0}{1}", tc.Text, rtf.ChargeType.ChargeOnce ? "¹" : string.Empty))));
        }
      }

      tr1.Append(cells);
      tbl.AppendChild(tr1);

      foreach (var row in rt.Rows)
      {
        tr1 = new TableRow();
        cells = new Collection<OpenXmlElement>();
        cells.Add(new TableCell(CreateCompactParagraphSmallText(GetShortLocationName(row.Location))));

        foreach (var tc in rt.Columns)
        {
          RateTableCategory rtc = tc as RateTableCategory;
          if (rtc != null)
          {
            if (rtc.IsFixedRate)
            {
              cells.Add(new TableCell(CreateCompactParagraphSmallText(rt[row, tc].Rate.ToString("#,##0.00"))));
            }
            else
            {
              cells.Add(new TableCell(CreateCompactParagraphSmallText(string.Format("{0:#,##0.00} / {1}", rt[row, tc].Rate, rtc.Owner.UnitOfMeasure))));
            }
          }
          else
          {
            cells.Add(new TableCell(CreateCompactParagraphSmallText(rt[row, tc].Rate.ToString("#,##0.00"))));
          }
        }

        tr1.Append(cells);
        tbl.AppendChild(tr1);
      }

      run.AppendChild(tbl);
    }

    #endregion

    #region string GetShortLocationName(...)

    string GetShortLocationName(Location location)
    {
      if (location.LocationType.SystemType == SystemLocationType.City) return location.Name;
      else if (location.LocationType.SystemType == SystemLocationType.Zone) return string.Format("{0}, {1}", location.Name, location.Owner.Name);

      throw new InvalidOperationException("Location must be of type City or Zone");
    }

    #endregion

    #region Paragraph CreateCompactParagraph(...)

    Paragraph CreateCompactParagraph(string text)
    {
      Run run = new Run();
      run.AppendChild(new RunProperties(new FontSize() { Val = new StringValue("10pt") }));
      run.AppendChild(new Text(text) { Space = SpaceProcessingModeValues.Preserve });

      return CreateCompactParagraph(run);
    }

    #endregion

    #region Paragraph CreateCompactParagraphSmallText(...)

    Paragraph CreateCompactParagraphSmallText(string text)
    {
      Run run = new Run();
      run.AppendChild(new RunProperties(new FontSize() { Val = new StringValue("8pt") }));
      run.AppendChild(new Text(text) { Space = SpaceProcessingModeValues.Preserve });

      return CreateCompactParagraph(run);
    }

    #endregion

    #region Paragraph CreateCompactParagraph(...)

    Paragraph CreateCompactParagraph(params OpenXmlElement[] newChildren)
    {
      SpacingBetweenLines spacing = new SpacingBetweenLines() { Line = "240", LineRule = LineSpacingRuleValues.Auto, Before = "0", After = "0" };
      ParagraphProperties paragraphProperties = new ParagraphProperties();
      Paragraph paragraph = new Paragraph();

      paragraphProperties.Append(spacing);
      paragraph.Append(paragraphProperties);
      paragraph.Append(newChildren);
      return paragraph;
    }

    #endregion

    #endregion
  }
}


﻿using Afx.Business.Data;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using FreightManagement.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace FreightManagement.Prism.Documents.RatesConfiguration.AddRevisionDialog
{
  public class AddRevisionDialogController : MdiDialogController<AddRevisionDialogContext, AddRevisionDialogViewModel>
  {
    public const string AddRevisionDialogControllerKey = "FreightManagement.Prism.Documents.RatesConfiguration.AddRevisionDialog.AddRevisionDialogController";

    public AddRevisionDialogController(IController controller)
      : base(AddRevisionDialogControllerKey, controller)
    {
    }

    protected override bool OnRun()
    {
      if (DataContext == null) DataContext = new AddRevisionDialogContext();
      DataContext.ChargeTypes.Clear();
      foreach (var ct in Cache.Instance.GetObjects<WaybillChargeType>(ct1 => ct1.Text, true))
      {
        DataContext.ChargeTypes.Add(new ChargeTypeIncrease(ct));
      }
      ViewModel.Caption = "Add Revision";
      return base.OnRun();
    }

    #region RatesConfigurationRevision SelectedRevision

    public const string RatesConfigurationRevisionProperty = "SelectedRevision";
    RatesConfigurationRevision mSelectedRevision;
    public RatesConfigurationRevision SelectedRevision
    {
      get { return mSelectedRevision; }
      set { mSelectedRevision = value; }
    }
    
    #endregion

    #region RatesConfiguration RatesConfiguration

    public const string RatesConfigurationProperty = "RatesConfiguration";
    Business.RatesConfiguration mRatesConfiguration;
    public Business.RatesConfiguration RatesConfiguration
    {
      get { return mRatesConfiguration; }
      set { mRatesConfiguration = value; }
    }

    #endregion

    protected override void ApplyChanges()
    {
      base.ApplyChanges();

      RatesConfiguration.Revisions.Add(CreateRevision());
    }

    RatesConfigurationRevision CreateRevision()
    {
      Dictionary<Business.WaybillChargeType, decimal> dict = new Dictionary<WaybillChargeType, decimal>();
      foreach (var ri in DataContext.ChargeTypes)
      {
        dict.Add(ri.ChargeType, ri.IncreaseRate);
      }
      return SelectedRevision.CreateRevision(DataContext.RevisionStartDate, dict);
    }
  }
}

﻿using Afx.Business;
using Afx.Business.Collections;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Documents.RatesConfiguration.AddRevisionDialog
{
  public class AddRevisionDialogContext : BusinessObject
  {
    #region DateTime RevisionStartDate

    public const string RevisionStartDateProperty = "RevisionStartDate";
    DateTime mRevisionStartDate = DateTime.Now;
    public DateTime RevisionStartDate
    {
      get { return mRevisionStartDate; }
      set { SetProperty<DateTime>(ref mRevisionStartDate, value); }
    }

    #endregion

    #region BusinessObjectCollection<ChargeTypeIncrease> ChargeTypes

    public const string ChargeTypesProperty = "ChargeTypes";
    BusinessObjectCollection<ChargeTypeIncrease> mChargeTypes;
    public BusinessObjectCollection<ChargeTypeIncrease> ChargeTypes
    {
      get { return mChargeTypes ?? (mChargeTypes = new BusinessObjectCollection<ChargeTypeIncrease>(this)); }
    }

    #endregion
  }
}
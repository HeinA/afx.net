﻿using Afx.Prism;
using Afx.Prism.RibbonMdi;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Documents.RatesConfiguration.AddRevisionDialog
{
  public class AddRevisionDialogViewModel : MdiDialogViewModel<AddRevisionDialogContext>
  {
    [InjectionConstructor]
    public AddRevisionDialogViewModel(IController controller)
      : base(controller)
    {
    }
  }
}

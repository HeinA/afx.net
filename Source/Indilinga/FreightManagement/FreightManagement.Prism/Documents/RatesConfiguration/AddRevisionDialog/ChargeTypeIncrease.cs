﻿using Afx.Business;
using FreightManagement.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Documents.RatesConfiguration.AddRevisionDialog
{
  public class ChargeTypeIncrease : BusinessObject<AddRevisionDialogContext>
  {
    public ChargeTypeIncrease(WaybillChargeType ct)
    {
      ChargeType = ct;
    }

    #region WaybillChargeType ChargeType

    public const string ChargeTypeProperty = "ChargeType";
    WaybillChargeType mChargeType;
    public WaybillChargeType ChargeType
    {
      get { return mChargeType; }
      private set { mChargeType = value; }
    }

    #endregion

    #region decimal IncreaseRate

    public const string IncreaseRateProperty = "IncreaseRate";
    decimal mIncreaseRate;
    public decimal IncreaseRate
    {
      get { return mIncreaseRate; }
      set { SetProperty<decimal>(ref mIncreaseRate, value); }
    }

    #endregion
  }
}

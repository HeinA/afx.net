﻿using Afx.Business;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using FreightManagement.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace FreightManagement.Prism.Documents.RatesConfiguration.EditRateCategoriesDialog
{
  public class EditRateCategoriesDialogController : MdiDialogController<EditRateCategoriesDialogContext, EditRateCategoriesDialogViewModel>
  {
    public const string EditRateCategoriesDialogControllerKey = "FreightManagement.Prism.Documents.RatesConfiguration.EditRateCategoriesDialog.EditRateCategoriesDialogController";

    public EditRateCategoriesDialogController(IController controller)
      : base(EditRateCategoriesDialogControllerKey, controller)
    {
    }

    protected override bool OnRun()
    {
      if (DataContext == null) DataContext = new EditRateCategoriesDialogContext();
      ViewModel.Caption = "Edit Rate Categories";

      if (RateTable != null)
      {
        foreach (var c in RateTable.Columns.OfType<RateTableCategory>())
        {
          DataContext.Categories.Add(new RateTableCategoryProxy(c));
          DataContext.CostComponentType = c.CostComponentType;
        }
      }

      if (DistanceRateTable != null)
      {
        foreach (var c in DistanceRateTable.Columns.OfType<DistanceRateTableColumnCategory>())
        {
          DataContext.Categories.Add(new RateTableCategoryProxy(c));
          DataContext.CostComponentType = c.CostComponentType;
        }
      }

      return base.OnRun();
    }

    protected override void ApplyChanges()
    {
      try
      {
        if (RateTable != null)
        {
          foreach (var c in RateTable.Columns.OfType<RateTableCategory>())
          {
            if (!DataContext.Categories.Where(c1 => !c1.IsDeleted).Select(c1 => c1.GlobalIdentifier).Contains(c.GlobalIdentifier))
            {
              c.IsDeleted = true;
            }
          }

          foreach (var c in DataContext.Categories)
          {
            c.CostComponentType = DataContext.CostComponentType;

            var c1 = RateTable.Columns.OfType<RateTableCategory>().FirstOrDefault(c2 => c2.GlobalIdentifier.Equals(c.GlobalIdentifier));
            if (c1 != null)
            {
              c1.StartUnit = c.StartUnit;
              c1.IsFixedRate = c.IsFixedRate;
              c1.IsPercentage = c.IsPercentage;
              c1.ThereAfter = c.ThereAfter;
              c1.IsMinimumCharge = c.IsMinimumCharge;
              c1.CostComponentType = c.CostComponentType;
              c1.IsDeleted = c.IsDeleted;
            }
            else
            {
              RateTable.AddColumn(new RateTableCategory(c));
            }
          }
        }

        if (DistanceRateTable != null)
        {
          foreach (var c in DistanceRateTable.Columns.OfType<DistanceRateTableColumnCategory>())
          {
            if (!DataContext.Categories.Where(c1 => !c1.IsDeleted).Select(c1 => c1.GlobalIdentifier).Contains(c.GlobalIdentifier))
            {
              c.IsDeleted = true;
            }
          }

          foreach (var c in DataContext.Categories)
          {
            c.CostComponentType = DataContext.CostComponentType;

            var c1 = DistanceRateTable.Columns.OfType<DistanceRateTableColumnCategory>().FirstOrDefault(c2 => c2.GlobalIdentifier.Equals(c.GlobalIdentifier));
            if (c1 != null)
            {
              c1.StartUnit = c.StartUnit;
              c1.IsFixedRate = c.IsFixedRate;
              c1.IsPercentage = c.IsPercentage;
              c1.IsMinimumCharge = c.IsMinimumCharge;
              c1.CostComponentType = c.CostComponentType;
              c1.ThereAfter = c.ThereAfter;
              c1.IsDeleted = c.IsDeleted;
            }
            else
            {
              DistanceRateTable.AddColumn(new DistanceRateTableColumnCategory(c));
            }
          }
        }
      }
      catch (Exception ex)
      {
        if (ExceptionHelper.HandleException(ex)) throw;
      }

      base.ApplyChanges();
    }

    #region RateTable RateTable

    public const string RateTableProperty = "RateTable";
    RateTable mRateTable;
    public RateTable RateTable
    {
      get { return mRateTable; }
      set { mRateTable = value; }
    }

    #endregion

    #region DistanceRateTable DistanceRateTable

    public const string DistanceRateTableProperty = "DistanceRateTable";
    DistanceRateTable mDistanceRateTable;
    public DistanceRateTable DistanceRateTable
    {
      get { return mDistanceRateTable; }
      set { mDistanceRateTable = value; }
    }

    #endregion
  }
}

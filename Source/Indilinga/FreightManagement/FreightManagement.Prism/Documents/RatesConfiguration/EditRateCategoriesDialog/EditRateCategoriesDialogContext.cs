﻿using Afx.Business;
using Afx.Business.Collections;
using Afx.Business.Validation;
using FreightManagement.Business;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Documents.RatesConfiguration.EditRateCategoriesDialog
{
  public class EditRateCategoriesDialogContext : BusinessObject
  {
    BasicCollection<RateTableCategoryProxy> mCategories;
    public BasicCollection<RateTableCategoryProxy> Categories
    {
      get { return mCategories ?? (mCategories = new BasicCollection<RateTableCategoryProxy>()); }
    }

    #region WaybillCostComponentType CostComponentType

    public const string CostComponentTypeProperty = "CostComponentType";
    WaybillCostComponentType mCostComponentType;
    [Mandatory("Cost Component Type is mandatory")]
    public WaybillCostComponentType CostComponentType
    {
      get { return mCostComponentType; }
      set { SetProperty<WaybillCostComponentType>(ref mCostComponentType, value); }
    }

    #endregion
  }
}
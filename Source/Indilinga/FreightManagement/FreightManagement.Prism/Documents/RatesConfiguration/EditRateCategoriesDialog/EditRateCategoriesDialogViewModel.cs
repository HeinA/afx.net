﻿using Afx.Business.Data;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using FreightManagement.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Documents.RatesConfiguration.EditRateCategoriesDialog
{
  public class EditRateCategoriesDialogViewModel : MdiDialogViewModel<EditRateCategoriesDialogContext>
  {
    [InjectionConstructor]
    public EditRateCategoriesDialogViewModel(IController controller)
      : base(controller)
    {
    }

    public IEnumerable<WaybillCostComponentType> CostComponentTypes
    {
      get { return Cache.Instance.GetObjects<WaybillCostComponentType>(true, cc => !cc.IsFixedFee && !cc.IsInsurance, cc => cc.Text, true); }
    }
  }
}

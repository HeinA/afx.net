﻿using Afx.Business;
using FreightManagement.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Documents.RatesConfiguration.EditRateCategoriesDialog
{
  public class RateTableCategoryProxy : BusinessObject, IRateTableCategory
  {
    public RateTableCategoryProxy()
    {
    }

    public RateTableCategoryProxy(IRateTableCategory rtc)
    {
      StartUnit = rtc.StartUnit;
      IsFixedRate = rtc.IsFixedRate;
      IsPercentage = rtc.IsPercentage;
      IsMinimumCharge = rtc.IsMinimumCharge;
      CostComponentType = rtc.CostComponentType;
      ThereAfter = rtc.ThereAfter;
      GlobalIdentifier = rtc.GlobalIdentifier;
    }

    #region int StartUnit

    public const string StartUnitProperty = "StartUnit";
    int mStartUnit;
    public int StartUnit
    {
      get { return mStartUnit; }
      set { SetProperty<int>(ref mStartUnit, value); }
    }

    #endregion

    #region bool IsFixedRate

    public const string IsFixedRateProperty = "IsFixedRate";
    bool mIsFixedRate;
    public bool IsFixedRate
    {
      get { return mIsFixedRate; }
      set { SetProperty<bool>(ref mIsFixedRate, value); }
    }

    #endregion

    #region bool IsPercentage

    public const string IsPercentageProperty = "IsPercentage";
    bool mIsPercentage;
    public bool IsPercentage
    {
      get { return mIsPercentage; }
      set { SetProperty<bool>(ref mIsPercentage, value); }
    }

    #endregion

    #region bool IsMinimumCharge

    public const string IsMinimumChargeProperty = "IsMinimumCharge";
    bool mIsMinimumCharge;
    public bool IsMinimumCharge
    {
      get { return mIsMinimumCharge; }
      set { SetProperty<bool>(ref mIsMinimumCharge, value); }
    }

    #endregion

    #region bool ThereAfter

    public const string ThereAfterProperty = "ThereAfter";
    bool mThereAfter;
    public bool ThereAfter
    {
      get { return mThereAfter; }
      set { SetProperty<bool>(ref mThereAfter, value); }
    }

    #endregion

    #region WaybillCostComponentType CostComponentType

    public const string CostComponentTypeProperty = "CostComponentType";
    WaybillCostComponentType mCostComponentType;
    public WaybillCostComponentType CostComponentType
    {
      get { return mCostComponentType; }
      set { SetProperty<WaybillCostComponentType>(ref mCostComponentType, value); }
    }

    #endregion
  }
}

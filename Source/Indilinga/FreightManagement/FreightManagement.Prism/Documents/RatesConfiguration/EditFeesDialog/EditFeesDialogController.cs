﻿using Afx.Business.Collections;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using FreightManagement.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace FreightManagement.Prism.Documents.RatesConfiguration.EditFeesDialog
{
  public class EditFeesDialogController : MdiDialogController<EditFeesDialogContext, EditFeesDialogViewModel>
  {
    public const string EditFeesDialogControllerKey = "FreightManagement.Prism.Documents.RatesConfiguration.EditFeesDialog.EditFeesDialogController";

    public EditFeesDialogController(IController controller)
      : base(EditFeesDialogControllerKey, controller)
    {
    }

    protected override bool OnRun()
    {
      if (DataContext == null) DataContext = new EditFeesDialogContext();
      ViewModel.Caption = "Edit Fees";
      return base.OnRun();
    }

    protected override void ApplyChanges()
    {
      if (RateTable != null)
      {
        foreach (var f in RateTable.Columns.OfType<RateTableFee>())
        {
          if (!ApplicableFees.Contains(f.ChargeType))
          {
            f.IsDeleted = true;
          }
        }

        foreach (var f in ApplicableFees)
        {
          RateTableFee ff = RateTable.Columns.OfType<RateTableFee>().FirstOrDefault(f1 => f1.ChargeType.Equals(f));
          if (ff == null)
          {
            RateTable.AddColumn(new RateTableFee(f));
          }
          else
          {
            ff.IsDeleted = false;
          }
        }
      }

      if (DistanceRateTable != null)
      {
        foreach (var f in DistanceRateTable.Columns.OfType<DistanceRateTableColumnFee>())
        {
          if (!ApplicableFees.Contains(f.ChargeType))
          {
            f.IsDeleted = true;
          }
        }

        foreach (var f in ApplicableFees)
        {
          DistanceRateTableColumnFee ff = DistanceRateTable.Columns.OfType<DistanceRateTableColumnFee>().FirstOrDefault(f1 => f1.ChargeType.Equals(f));
          if (ff == null)
          {
            DistanceRateTable.AddColumn(new DistanceRateTableColumnFee(f));
          }
          else
          {
            ff.IsDeleted = false;
          }
        }
      }

      base.ApplyChanges();
    }

    BasicCollection<WaybillCostComponentType> mApplicableFees;
    public BasicCollection<WaybillCostComponentType> ApplicableFees
    {
      get { return mApplicableFees ?? (mApplicableFees = new BasicCollection<WaybillCostComponentType>()); }
    }

    #region RateTable RateTable

    public const string RateTableProperty = "RateTable";
    RateTable mRateTable;
    public RateTable RateTable
    {
      get { return mRateTable; }
      set
      {
        mRateTable = value;
        ApplicableFees.Clear();
        foreach (var f in RateTable.Columns.OfType<RateTableFee>().Where(f => !f.IsDeleted))
        {
          ApplicableFees.Add(f.ChargeType);
        }
      }
    }

    #endregion

    #region DistanceRateTable DistanceRateTable

    public const string DistanceRateTableProperty = "DistanceRateTable";
    DistanceRateTable mDistanceRateTable;
    public DistanceRateTable DistanceRateTable
    {
      get { return mDistanceRateTable; }
      set
      {
        mDistanceRateTable = value;
        ApplicableFees.Clear();
        foreach (var f in DistanceRateTable.Columns.OfType<DistanceRateTableColumnFee>().Where(f => !f.IsDeleted))
        {
          ApplicableFees.Add(f.ChargeType);
        }
      }
    }

    #endregion
  }
}

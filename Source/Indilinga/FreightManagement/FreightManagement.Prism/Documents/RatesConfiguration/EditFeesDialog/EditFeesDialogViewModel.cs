﻿using Afx.Business.Data;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using FreightManagement.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Documents.RatesConfiguration.EditFeesDialog
{
  public class EditFeesDialogViewModel : MdiDialogViewModel<EditFeesDialogContext>
  {
    [InjectionConstructor]
    public EditFeesDialogViewModel(IController controller)
      : base(controller)
    {
    }

    public IEnumerable<FeeItemViewModel> Fees
    {
      get { return ResolveViewModels<FeeItemViewModel, WaybillCostComponentType>(Cache.Instance.GetObjects<WaybillCostComponentType>(f => f.IsFixedFee && !f.IsInsurance, f => f.Text, true)); }
    }
  }
}

﻿using Afx.Business.Activities;
using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Business.Service;
using Afx.Prism.RibbonMdi;
using Afx.Prism.RibbonMdi.Events;
using Afx.Prism.RibbonMdi.Ribbon;
using FreightManagement.Business.Service;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Documents.RatesConfiguration
{
  [Export(typeof(IRibbonItem))]
  public class StandardRatesButton : RibbonButtonViewModel
  {
    public override string TabName
    {
      get { return FreightManagementTab.TabName ; }
    }

    public override string GroupName
    {
      get { return ConfigurationGroup.GroupName; }
    }

    public override string ItemIdentifier
    {
      get { return "StandardRates"; }
    }

    public override int Index
    {
      get { return 0; }
    }

    protected override void OnExecute()
    {
      using (var svc = ProxyFactory.GetService<IFreightManagementService>(SecurityContext.ServerName))
      {
        Business.RatesConfiguration rc = svc.LoadStandardRatesConfiguration();
        MdiApplicationController.Current.EventAggregator.GetEvent<EditDocumentEvent>().Publish(new EditDocumentEventArgs(rc));
      }
    }
  }
}

﻿using Afx.Prism;
using FreightManagement.Business;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Documents.RatesConfiguration
{
  public class CargoTypeDetailViewModel : ViewModel<RatesConfigurationRevisionCargoType>, IRateTableContainer
  {
    #region Constructors

    [InjectionConstructor]
    public CargoTypeDetailViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    public IEnumerable<ViewModel> Tables
    {
      get
      {
        IEnumerable<RateTableViewModel> vm1 = ResolveViewModels<RateTableViewModel, RateTable>(Model.Tables).Where(vm => !vm.Model.IsDeleted);
        IEnumerable<DistanceRateTableViewModel> vm2 = ResolveViewModels<DistanceRateTableViewModel, DistanceRateTable>(Model.DistanceTables).Where(vm => !vm.Model.IsDeleted);
        return vm1.OfType<ViewModel>().Union(vm2);
      }
    }

    #region DelegateCommand AddTableCommand

    DelegateCommand mAddTableCommand;
    public DelegateCommand AddTableCommand
    {
      get { return mAddTableCommand ?? (mAddTableCommand = new DelegateCommand(ExecuteAddTable, CanExecuteAddTable)); }
    }

    bool CanExecuteAddTable()
    {
      return true;
    }

    void ExecuteAddTable()
    {
      RateTable rt = new RateTable();
      rt.BillingUnit = this.Model.Reference.BillingUnit;
      rt.UnitOfMeasure = this.Model.Reference.UnitOfMeasure;
      AddTable(rt);
    }

    #endregion

    #region DelegateCommand AddDistanceTableCommand

    DelegateCommand mAddDistanceTableCommand;
    public DelegateCommand AddDistanceTableCommand
    {
      get { return mAddDistanceTableCommand ?? (mAddDistanceTableCommand = new DelegateCommand(ExecuteAddDistanceTable, CanExecuteAddDistanceTable)); }
    }

    bool CanExecuteAddDistanceTable()
    {
      return true;
    }

    void ExecuteAddDistanceTable()
    {
      DistanceRateTable rt = new DistanceRateTable();
      rt.BillingUnit = this.Model.Reference.BillingUnit;
      rt.UnitOfMeasure = this.Model.Reference.UnitOfMeasure;
      AddDistanceTable(rt);
    }

    #endregion

    public void RefreshTables()
    {
      OnPropertyChanged("Tables");
    }

    public void AddTable(RateTable table)
    {
      Model.Tables.Add(table);
      RefreshTables();
    }

    public void AddDistanceTable(DistanceRateTable table)
    {
      Model.DistanceTables.Add(table);
      RefreshTables();
    }
  }
}

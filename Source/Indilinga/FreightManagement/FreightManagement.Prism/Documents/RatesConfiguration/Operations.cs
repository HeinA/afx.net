﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Documents.RatesConfiguration
{
  public class Operations : Afx.Prism.RibbonMdi.StandardOperations
  {
      public const string AddRevision = "{14506e3e-66c5-4789-bda8-4f8b4879e328}";
      public const string Copy = "{50236cac-ec5f-4a5c-abf9-11a9f9ccdf9d}";
  }
}
﻿using Afx.Business.Data;
using Afx.Prism;
using FreightManagement.Business;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Documents.RatesConfiguration
{
  public class RatesConfigurationRevisionViewModel : ViewModel<RatesConfigurationRevision>, IRateTableContainer
  {
    #region Constructors

    [InjectionConstructor]
    public RatesConfigurationRevisionViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    #region string TitleText

    public string TitleText
    {
      get { return "Rates Configuration"; }
    }

    #endregion

    [Dependency]
    public RatesConfigurationController RatesConfigurationController { get; set; }

    protected override void OnModelCompositionChanged(Afx.Business.CompositionChangedEventArgs e)
    {
      if (e.CompositionChangedType == Afx.Business.CompositionChangedType.ChildAdded || e.CompositionChangedType == Afx.Business.CompositionChangedType.ChildRemoved && e.PropertyName == RatesConfigurationRevision.LevyTypesProperty)
      {
        OnPropertyChanged("RatesConfigurationRevisionLevyTypes");
      }

      base.OnModelCompositionChanged(e);
    }

    public IEnumerable<CostingFlagViewModel> CostingFlags
    {
      get { return ResolveViewModels<CostingFlagViewModel, WaybillFlag>(Cache.Instance.GetObjects<WaybillFlag>(f => f.Owner.IsFlagged(WaybillFlagGroupFlags.CostingFlagGroup))); }
    }

    #region IEnumerable<CargoTypeItemViewModel> CargoTypes

    Collection<CargoType> mCargoTypes;
    public IEnumerable<CargoTypeItemViewModel> CargoTypes
    {
      get
      {
        if (mCargoTypes == null)
        {
          mCargoTypes = Cache.Instance.GetObjects<CargoType>(ct => ct.Text, true);
        }
        IEnumerable<CargoTypeItemViewModel> col = ResolveViewModels<CargoTypeItemViewModel, CargoType>(mCargoTypes);
        SelectedCargoType = col.FirstOrDefault();
        return col;
      }
    }

    #endregion

    #region CargoTypeItemViewModel SelectedCargoType

    public const string SelectedCargoTypeProperty = "SelectedCargoType";
    CargoTypeItemViewModel mSelectedCargoType;
    public CargoTypeItemViewModel SelectedCargoType
    {
      get { return mSelectedCargoType; }
      set
      {
        if (SetProperty<CargoTypeItemViewModel>(ref mSelectedCargoType, value))
        {
          DisplayCargoTypeTables();
        }
      }
    }

    #endregion

    #region object CargoTypeDetailsViewModel

    public const string CargoTypeDetailsViewModelProperty = "CargoTypeDetailsViewModel";
    object mCargoTypeDetailsViewModel;
    public object CargoTypeDetailsViewModel
    {
      get { return mCargoTypeDetailsViewModel; }
      set { SetProperty<object>(ref mCargoTypeDetailsViewModel, value); }
    }

    #endregion

    #region DelegateCommand AddTableByWeightCommand

    DelegateCommand mAddTableByWeightCommand;
    public DelegateCommand AddTableByWeightCommand
    {
      get { return mAddTableByWeightCommand ?? (mAddTableByWeightCommand = new DelegateCommand(ExecuteAddTableByWeight, CanExecuteAddTableByWeight)); }
    }

    bool CanExecuteAddTableByWeight()
    {
      return true;
    }

    void ExecuteAddTableByWeight()
    {
      RateTable rt = new RateTable();
      rt.BillingUnit = BillingUnit.Weight;
      rt.UnitOfMeasure = "kg";
      AddTable(rt);
    }

    #endregion

    #region DelegateCommand AddDistanceTableByWeightCommand

    DelegateCommand mAddDistanceTableByWeightCommand;
    public DelegateCommand AddDistanceTableByWeightCommand
    {
      get { return mAddDistanceTableByWeightCommand ?? (mAddDistanceTableByWeightCommand = new DelegateCommand(ExecuteAddDistanceTableByWeight, CanExecuteAddDistanceTableByWeight)); }
    }

    bool CanExecuteAddDistanceTableByWeight()
    {
      return true;
    }

    void ExecuteAddDistanceTableByWeight()
    {
      DistanceRateTable rt = new DistanceRateTable();
      rt.BillingUnit = BillingUnit.Weight;
      rt.UnitOfMeasure = "kg";
      AddDistanceTable(rt);
    }

    #endregion

    public IEnumerable<ViewModel> TablesByWeight
    {
      get
      {
        IEnumerable<RateTableViewModel> vm1 = ResolveViewModels<RateTableViewModel, RateTable>(Model.TablesByWeight).Where(vm => !vm.Model.IsDeleted);
        IEnumerable<DistanceRateTableViewModel> vm2 = ResolveViewModels<DistanceRateTableViewModel, DistanceRateTable>(Model.DistanceTablesByWeight).Where(vm => !vm.Model.IsDeleted);
        return vm1.OfType<ViewModel>().Union(vm2);
      }
    }

    public void RefreshTables()
    {
      OnPropertyChanged("TablesByWeight");
    }

    public void AddTable(RateTable table)
    {
      Model.TablesByWeight.Add(table);
      RefreshTables();
    }

    public void AddDistanceTable(DistanceRateTable table)
    {
      Model.DistanceTablesByWeight.Add(table);
      RefreshTables();
    }

    #region void DisplayCargoTypeTables()

    public void DisplayCargoTypeTables()
    {
      if (SelectedCargoType != null)
      {
        if (!SelectedCargoType.HasCargoType)
        {
          CargoTypeDetailsViewModel = RatesConfigurationController.GetCreateViewModel<NotActivatedViewModel>(this);
        }
        else
        {
          CargoTypeDetailsViewModel = RatesConfigurationController.GetCreateViewModel<CargoTypeDetailViewModel>(Model.GetAssociativeObject<RatesConfigurationRevisionCargoType>(SelectedCargoType.Model), this);
        }
      }
    }

    #endregion


    Collection<WaybillLevyType> LevyTypes
    {
      get { return Cache.Instance.GetObjects<WaybillLevyType>(cc => cc.Text, true); }
    }

    public IEnumerable<LevyTypeItemViewModel> LevyTypeViewModels
    {
      get { return ResolveViewModels<LevyTypeItemViewModel, WaybillLevyType>(LevyTypes); }
    }

    public IEnumerable<RatesConfigurationRevisionLevyTypeViewModel> RatesConfigurationRevisionLevyTypes
    {
      get { return ResolveViewModels<RatesConfigurationRevisionLevyTypeViewModel, RatesConfigurationRevisionLevyType>(Model.GetAssociativeObjects<RatesConfigurationRevisionLevyType>().Where(lt => !lt.IsDeleted).ToList()); }
    }
  }
}

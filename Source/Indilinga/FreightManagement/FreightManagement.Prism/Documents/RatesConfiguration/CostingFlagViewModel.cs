﻿using Afx.Prism;
using FreightManagement.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Documents.RatesConfiguration
{
  public class CostingFlagViewModel : ViewModel<WaybillFlag>
  {
    #region Constructors

    [InjectionConstructor]
    public CostingFlagViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    public new RatesConfigurationRevisionViewModel Parent
    {
      get { return (RatesConfigurationRevisionViewModel)base.Parent; }
    }

    public bool IsChecked
    {
      get { return Parent.Model.FlaggedParameters.Contains(Model); }
      set
      {
        if (value)
        {
          if (!Parent.Model.FlaggedParameters.Contains(Model))
          {
            Parent.Model.FlaggedParameters.Add(Model);
            OnPropertyChanged("IsChecked");
            OnPropertyChanged("VolumetricFactor");
            OnPropertyChanged("InsuranceRate");
          }
        }
        else
        {
          if (Parent.Model.FlaggedParameters.Contains(Model))
          {
            Parent.Model.FlaggedParameters.Remove(Model);
            OnPropertyChanged("IsChecked");
          }
        }
      }
    }

    public int VolumetricFactor
    {
      get
      {
        var cf = Parent.Model.GetAssociativeObject<RatesConfigurationRevisionCostingFlag>(Model);
        if (cf == null) return 0;
        return cf.VolumetricFactor;
      }
      set
      {
        var cf = Parent.Model.GetAssociativeObject<RatesConfigurationRevisionCostingFlag>(Model);
        cf.VolumetricFactor = value;
        OnPropertyChanged("VolumetricFactor");
      }
    }

    public decimal InsuranceRate
    {
      get
      {
        var cf = Parent.Model.GetAssociativeObject<RatesConfigurationRevisionCostingFlag>(Model);
        if (cf == null) return 0;
        return cf.InsuranceRate;
      }
      set
      {
        var cf = Parent.Model.GetAssociativeObject<RatesConfigurationRevisionCostingFlag>(Model);
        cf.InsuranceRate = value;
        OnPropertyChanged("InsuranceRate");
      }
    }
  }
}

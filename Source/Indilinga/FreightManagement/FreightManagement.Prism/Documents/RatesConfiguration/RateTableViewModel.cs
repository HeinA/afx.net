﻿using Afx.Business.Data;
using Afx.Prism;
using FreightManagement.Business;
using FreightManagement.Prism.Documents.RatesConfiguration.EditFeesDialog;
using FreightManagement.Prism.Documents.RatesConfiguration.EditLocationsDialog;
using FreightManagement.Prism.Documents.RatesConfiguration.EditRateCategoriesDialog;
using Geographics.Business;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace FreightManagement.Prism.Documents.RatesConfiguration
{
  public class RateTableViewModel : ViewModel<RateTable>
  {
    #region Constructors

    [InjectionConstructor]
    public RateTableViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    new IRateTableContainer Parent
    {
      get { return (IRateTableContainer)base.Parent; }
    }

    #region RateTableLocation CurrentRow

    public const string CurrentRowProperty = "CurrentRow";
    RateTableLocation mCurrentRow;
    public RateTableLocation CurrentRow
    {
      get { return mCurrentRow; }
      set { SetProperty<RateTableLocation>(ref mCurrentRow, value); }
    }

    #endregion

    #region DelegateCommand EditFeesCommand

    DelegateCommand mEditFeesCommand;
    public DelegateCommand EditFeesCommand
    {
      get { return mEditFeesCommand ?? (mEditFeesCommand = new DelegateCommand(ExecuteEditFees, CanExecuteEditFees)); }
    }

    bool CanExecuteEditFees()
    {
      return true;
    }

    void ExecuteEditFees()
    {
      EditFeesDialogController c = Controller.CreateChildController<EditFeesDialogController>();
      c.RateTable = Model;
      c.Run();
    }

    #endregion

    #region DelegateCommand EditRateCategoriesCommand

    DelegateCommand mEditRateCategoriesCommand;
    public DelegateCommand EditRateCategoriesCommand
    {
      get { return mEditRateCategoriesCommand ?? (mEditRateCategoriesCommand = new DelegateCommand(ExecuteEditRateCategories, CanExecuteEditRateCategories)); }
    }

    bool CanExecuteEditRateCategories()
    {
      return true;
    }

    void ExecuteEditRateCategories()
    {
      EditRateCategoriesDialogController c = Controller.CreateChildController<EditRateCategoriesDialogController>();
      c.RateTable = Model;
      c.Run();
    }

    #endregion

    #region DelegateCommand EditLocationsCommand

    DelegateCommand mEditLocationsCommand;
    public DelegateCommand EditLocationsCommand
    {
      get { return mEditLocationsCommand ?? (mEditLocationsCommand = new DelegateCommand(ExecuteEditLocations, CanExecuteEditLocations)); }
    }

    bool CanExecuteEditLocations()
    {
      return true;
    }

    void ExecuteEditLocations()
    {
      EditLocationsDialogController c = Controller.CreateChildController<EditLocationsDialogController>();
      c.Table = Model;
      c.Run();
    }

    #endregion

    #region DelegateCommand CopyRowCommand

    DelegateCommand mCopyRowCommand;
    public DelegateCommand CopyRowCommand
    {
      get { return mCopyRowCommand ?? (mCopyRowCommand = new DelegateCommand(ExecuteCopyRow, CanExecuteCopyRow)); }
    }

    bool CanExecuteCopyRow()
    {
      return true;
    }

    void ExecuteCopyRow()
    {
      string row = string.Join("\t", CurrentRow.Cells.Where(c => !c.IsDeleted).Select(c => c.Rate));
      Clipboard.SetData(DataFormats.Text, row);
    }

    #endregion

    #region DelegateCommand PasteRowCommand

    DelegateCommand mPasteRowCommand;
    public DelegateCommand PasteRowCommand
    {
      get { return mPasteRowCommand ?? (mPasteRowCommand = new DelegateCommand(ExecutePasteRow, CanExecutePasteRow)); }
    }

    bool CanExecutePasteRow()
    {
      return true;
    }

    void ExecutePasteRow()
    {
      if (!Clipboard.ContainsData(DataFormats.Text)) return;
      string row = (string)Clipboard.GetData(DataFormats.Text);
      if (string.IsNullOrWhiteSpace(row)) return;
      string[] cells = row.Split('\t');
      if (CurrentRow.Cells.Where(c => !c.IsDeleted).Count() == cells.Count())
      {
        for (int i = 0; i < CurrentRow.Cells.Count(); i++)
        {
          CurrentRow.Cells.Where(c => !c.IsDeleted).ElementAt(i).Rate = decimal.Parse(cells.ElementAt(i));
        }
      }
    }

    #endregion

    #region DelegateCommand CopyTableCommand

    DelegateCommand mCopyTableCommand;
    public DelegateCommand CopyTableCommand
    {
      get { return mCopyTableCommand ?? (mCopyTableCommand = new DelegateCommand(ExecuteCopyTable, CanExecuteCopyTable)); }
    }

    bool CanExecuteCopyTable()
    {
      return true;
    }

    void ExecuteCopyTable()
    {
      Parent.AddTable(new RateTable(this.Model));
    }

    #endregion

    #region DelegateCommand ToggleDeleteCommand

    DelegateCommand mToggleDeleteCommand;
    public DelegateCommand ToggleDeleteCommand
    {
      get { return mToggleDeleteCommand ?? (mToggleDeleteCommand = new DelegateCommand(ExecuteToggleDelete, CanExecuteToggleDelete)); }
    }

    bool CanExecuteToggleDelete()
    {
      return true;
    }

    void ExecuteToggleDelete()
    {
      if (System.Windows.MessageBox.Show("Are you sure you want to delete this table?", "Delete Table", System.Windows.MessageBoxButton.YesNo, System.Windows.MessageBoxImage.Question) == System.Windows.MessageBoxResult.Yes)
      {
        Model.IsDeleted = !Model.IsDeleted;
        Parent.RefreshTables();
      }
    }

    #endregion

    public IEnumerable<Location> Locations
    {
      get
      {
        return Cache.Instance.GetObjects<Location>(true, (l) =>
        {
          DeliveryLocation dl = l.GetExtensionObject<DeliveryLocation>();
          if (dl != null && (dl.DeliveryPointType != DeliveryPointType.None || dl.IsWaypoint))
          {
            return true;
          }
          return false;
        }, l => l.FullNameWithDistributionCenter(), true);
      }
    }

    public IEnumerable<WaybillFlag> Flags
    {
      get
      {
        return Cache.Instance.GetObjects<WaybillFlag>(true, f => f.Text, true);
      }
    }

    public IEnumerable<DistributionCenter> DistributionCenters
    {
      get { return Cache.Instance.GetObjects<DistributionCenter>(true, dc => dc.Name, true); }
    }

  }
}

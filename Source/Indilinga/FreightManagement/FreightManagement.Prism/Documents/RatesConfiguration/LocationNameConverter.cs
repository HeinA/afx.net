﻿using FreightManagement.Business;
using Geographics.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace FreightManagement.Prism.Documents.RatesConfiguration
{
  public class LocationNameConverter : IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    {
      Location l = value as Location;
      if (l == null) return null;
      return l.FullNameWithDistributionCenter();
    }

    public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    {
      throw new NotImplementedException();
    }
  }
}

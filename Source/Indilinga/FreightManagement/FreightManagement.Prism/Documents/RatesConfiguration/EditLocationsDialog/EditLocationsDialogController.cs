﻿using Afx.Business;
using Afx.Business.Collections;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using FreightManagement.Business;
using Geographics.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace FreightManagement.Prism.Documents.RatesConfiguration.EditLocationsDialog
{
  public class EditLocationsDialogController : MdiDialogController<EditLocationsDialogContext, EditLocationsDialogViewModel>
  {
    public const string EditLocationsDialogControllerKey = "FreightManagement.Prism.Documents.RatesConfiguration.EditLocationsDialog.EditLocationsDialogController";

    public EditLocationsDialogController(IController controller)
      : base(EditLocationsDialogControllerKey, controller)
    {
    }

    protected override bool OnRun()
    {
      if (DataContext == null) DataContext = new EditLocationsDialogContext();
      ViewModel.Caption = "Edit Locations";
      return base.OnRun();
    }

    protected override void ApplyChanges()
    {
      foreach (var l in Table.Rows)
      {
        if (!ApplicableLocations.Contains(l.Location))
        {
          l.IsDeleted = true;
        }
      }

      foreach (var l in ApplicableLocations)
      {
        RateTableLocation ll = Table.Rows.FirstOrDefault(r => r.Location.Equals(l));
        if (ll == null)
        {
          Table.AddRow(new RateTableLocation(l));
        }
        else
        {
          ll.IsDeleted = false;
        }
      }
      base.ApplyChanges();
    }

    BasicCollection<Location> mApplicableLocations;
    public BasicCollection<Location> ApplicableLocations
    {
      get { return mApplicableLocations ?? (mApplicableLocations = new BasicCollection<Location>()); }
    }

    #region RatesConfigurationRevisionCargoTypeTable Table

    public const string TableProperty = "Table";
    RateTable mTable;
    public RateTable Table
    {
      get { return mTable; }
      set
      {
        mTable = value;
        ApplicableLocations.Clear();
        foreach (var r in Table.Rows.OfType<RateTableLocation>().Where(r => !r.IsDeleted))
        {
          ApplicableLocations.Add(r.Location);
        }
      }
    }

    #endregion
  }
}

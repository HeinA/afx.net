﻿using Afx.Business.Data;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using FreightManagement.Business;
using Geographics.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Documents.RatesConfiguration.EditLocationsDialog
{
  public class EditLocationsDialogViewModel : MdiDialogViewModel<EditLocationsDialogContext>
  {
    [InjectionConstructor]
    public EditLocationsDialogViewModel(IController controller)
      : base(controller)
    {
    }

    public IEnumerable<LocationItemViewModel> Locations
    {
      get { return ResolveViewModels<LocationItemViewModel, Location>(Cache.Instance.GetObjects<Location>(l => l.GetExtensionObject<DeliveryLocation>() != null && l.GetExtensionObject<DeliveryLocation>().DeliveryPointType != DeliveryPointType.None, l => l.FullName, true)); }
    }
  }
}

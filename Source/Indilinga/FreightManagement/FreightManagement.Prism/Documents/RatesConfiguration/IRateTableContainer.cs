﻿using FreightManagement.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Documents.RatesConfiguration
{
  public interface IRateTableContainer
  {
    void RefreshTables();
    void AddTable(RateTable table);
    void AddDistanceTable(DistanceRateTable table);
  }
}

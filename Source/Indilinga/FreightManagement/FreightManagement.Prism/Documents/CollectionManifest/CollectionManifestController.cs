﻿using Afx.Business;
using Afx.Business.Documents;
using Afx.Business.Security;
using Afx.Business.Service;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Documents;
using DocumentManagement.Business;
using DocumentManagement.Prism.Extensions.DocumentRepository;
using FleetManagement.Prism.Dialogs.EditVehiclesDialog;
using FreightManagement.Business;
using FreightManagement.Business.Service;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Documents.CollectionManifest
{
  public partial class CollectionManifestController : MdiDocumentController<CollectionManifestContext, CollectionManifestViewModel>, IDocumentRepositoryAware
  {
    [InjectionConstructor]
    public CollectionManifestController(IController controller)
      : base(controller)
    {
    }

    #region DocumentRepositoryExtensionViewModel DocumentRepositoryExtensionViewModel

    public const string DocumentRepositoryExtensionViewModelProperty = "DocumentRepositoryExtensionViewModel";
    DocumentRepositoryExtensionViewModel mDocumentRepositoryExtensionViewModel;
    public DocumentRepositoryExtensionViewModel DocumentRepositoryExtensionViewModel
    {
      get { return mDocumentRepositoryExtensionViewModel; }
      set { mDocumentRepositoryExtensionViewModel = value; }
    }

    #endregion

    public void AddScannedDocument(ScannedDocument document)
    {
      DocumentRepositoryExtension dre = DataContext.Document.GetExtensionObject<DocumentRepositoryExtension>();
      if (dre != null)
      {
        dre.ScannedDocuments.Add(document);
        RegionManager.Regions[TabRegion].Activate(DocumentRepositoryExtensionViewModel);
      }
    }

    public Document Document { get { return DataContext.Document; } }

    #region CollectionManifestScanViewModel ScanViewModel

    public const string ScanViewModelProperty = "ScanViewModel";
    CollectionManifestScanViewModel mScanViewModel;
    public CollectionManifestScanViewModel ScanViewModel
    {
      get { return mScanViewModel; }
      set { mScanViewModel = value; }
    }

    #endregion

    protected override void ExtendDocument()
    {
      ExtendDocument<CollectionManifestHeaderViewModel>(HeaderRegion);
      ScanViewModel = ExtendDocument<CollectionManifestScanViewModel>(HeaderRegion);

      ExtendDocument<CollectionManifestWaybillsViewModel>(DockRegion);

      DocumentRepositoryExtensionViewModel = ExtendDocument<DocumentRepositoryExtension, DocumentRepositoryExtensionViewModel>(TabRegion);

      base.ExtendDocument();
    }

    protected override void ExecuteOperation(Operation op, BusinessObject argument)
    {
      switch (op.Identifier)
      {
        case Operations.Verify:
          SetDocumentState(Business.CollectionManifest.States.Verified);
          break;

        case Operations.EditVehicles:
          EditVehiclesDialogController evdc = GetCreateChildController<EditVehiclesDialogController>(EditVehiclesDialogController.EditVehiclesDialogControllerKey);
          evdc.VehicleCollection = DataContext.Document;
          if (!BusinessObject.IsNull(DataContext.Document.Courier) && DataContext.Document.Courier != null) evdc.Contractor = DataContext.Document.Courier;
          evdc.Run();

          break;
        case Operations.Print:
          base.ExecuteOperation(op, argument);
          base.Print(argument: "Management");
          return;
      }
      base.ExecuteOperation(op, argument);
    }

    protected override void OnPrinted()
    {
      base.OnPrinted();


    }

    protected override bool OnBeforePrint()
    {
      return base.OnBeforePrint();
    }

    public void AttachWaybill(string waybillNumber)
    {
      using (var svc = ProxyFactory.GetService<IFreightManagementService>(SecurityContext.ServerName))
      {
        AttachWaybill(svc.LoadWaybillReferenceByDocumentNumber(waybillNumber));
      }
    }

    public void AttachWaybill(WaybillReference waybill)
    {
      ScanViewModel.ErrorMessage = string.Empty;
       
      if (BusinessObject.IsNull(waybill))
      {
        ScanViewModel.ErrorMessage = "Waybill not found.";
        System.Media.SystemSounds.Exclamation.Play();
        return;
      }

      if (DataContext.Document.IsReadOnly)
      {
        ScanViewModel.ErrorMessage = "Document may not be edited.";
        System.Media.SystemSounds.Exclamation.Play();
        return;
      }

      //if (DataContext.Document.State.DocumentTypeState.Identifier != Business.CollectionManifest.States.Open)
      //{
      //  ScanViewModel.ErrorMessage = "Document may not be edited.";
      //  System.Media.SystemSounds.Exclamation.Play();
      //  return;
      //}

      if (BusinessObject.IsNull(DataContext.Document.Route) || DataContext.Document.Waybills.Count == 0)
      {
        DataContext.Document.Route = waybill.Route;
      }
      else
      {
        if (!DataContext.Document.Route.Equals(waybill.Route))
        {
          ScanViewModel.ErrorMessage = "Waybill is for incoorect route.";
          System.Media.SystemSounds.Exclamation.Play();
          return;
        }
      }

      if (DataContext.Document.Waybills.Contains(waybill))
      {
        DataContext.Document.Waybills.Remove(waybill);
        if (DataContext.Document.Waybills.Count == 0)
        {
          DataContext.Document.Route = null;
        }
      }
      else
      {
        DataContext.Document.Waybills.Add(waybill);
      }
    }
  }
}


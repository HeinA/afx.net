﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Documents.CollectionManifest
{
  public class Operations : Afx.Prism.RibbonMdi.StandardOperations
  {
      public const string EditVehicles = "{ddffef5a-24a2-4d09-952e-f0b367180111}";
      public const string Verify = "{abeaf7fa-3cfe-41fa-b680-eba65a207013}";
  }
}
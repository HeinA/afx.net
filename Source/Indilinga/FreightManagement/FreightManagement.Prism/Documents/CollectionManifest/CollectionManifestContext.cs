﻿using Afx.Business.Security;
using Afx.Business.Service;
using Afx.Prism.RibbonMdi.Documents;
using FreightManagement.Business;
using FreightManagement.Business.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Documents.CollectionManifest
{
  public partial class CollectionManifestContext : DocumentContext<FreightManagement.Business.CollectionManifest>
  {
    public CollectionManifestContext()
      : base(FreightManagement.Business.CollectionManifest.DocumentTypeIdentifier)
    {
    }

    public CollectionManifestContext(Guid documentIdentifier)
      : base(FreightManagement.Business.CollectionManifest.DocumentTypeIdentifier, documentIdentifier)
    {
    }

    public override void SaveData()
    {
      using (var svc = ProxyFactory.GetService<IFreightManagementService>(SecurityContext.ServerName))
      {
        Document = svc.SaveCollectionManifest(Document);
      }
      base.SaveData();
    }

    public override void LoadData()
    {
      using (var svc = ProxyFactory.GetService<IFreightManagementService>(SecurityContext.ServerName))
      {
        Document = svc.LoadCollectionManifest(DocumentIdentifier);
      }
      base.LoadData();
    }
  }
}


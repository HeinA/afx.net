﻿using Afx.Prism;
using Afx.Prism.RibbonMdi;
using Afx.Prism.RibbonMdi.Documents;
using FreightManagement.Business;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Documents.PODHandoverManifest
{
  public class PODHandoverManifestPODHandoversViewModel : ExtensionViewModel<FreightManagement.Business.PODHandoverManifest>
  {
    #region Constructors

    [InjectionConstructor]
    public PODHandoverManifestPODHandoversViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    #region PODHandoverReference SelectedPODHandover

    public const string SelectedPODHandoverProperty = "SelectedPODHandover";
    PODHandoverReference mSelectedPODHandover;
    public PODHandoverReference SelectedPODHandover
    {
      get { return mSelectedPODHandover; }
      set { SetProperty<PODHandoverReference>(ref mSelectedPODHandover, value); }
    }

    #endregion

    #region DelegateCommand<PODHandoverReference> OpenPODHandoverCommand

    DelegateCommand<PODHandoverReference> mOpenPODHandoverCommand;
    public DelegateCommand<PODHandoverReference> OpenPODHandoverCommand
    {
      get { return mOpenPODHandoverCommand ?? (mOpenPODHandoverCommand = new DelegateCommand<PODHandoverReference>(ExecuteOpenPODHandover, CanExecuteOpenPODHandover)); }
    }

    bool CanExecuteOpenPODHandover(PODHandoverReference args)
    {
      return true;
    }

    void ExecuteOpenPODHandover(PODHandoverReference args)
    {
      MdiApplicationController.Current.EditDocument(Business.PODHandover.DocumentTypeIdentifier, args.GlobalIdentifier);
    }
    
    #endregion

  }
}

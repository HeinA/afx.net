﻿using Afx.Business;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Documents;
using FreightManagement.Business;
using Afx.Business.Security;
using Afx.Business.Service;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.Composition;
using Afx.Prism.RibbonMdi;
using Afx.Prism.RibbonMdi.Events;
using Afx.Business.Documents;

namespace FreightManagement.Prism.Documents.PODHandoverManifest
{
  //**************************************************************************************************************
  //**************************************************************************************************************
  //*                                                                                                            *
  //*     This code is auto-generated.  Do not place any cutom code here, it will be lost on re-generation.      *
  //*                                                                                                            *
  //**************************************************************************************************************
  //**************************************************************************************************************
  [Export("Controller:" + FreightManagement.Business.PODHandoverManifest.DocumentTypeIdentifier)]
  public partial class PODHandoverManifestController
  {
    public void SetDocumentState(string stateIdentifier)
    {
      if (!DataContext.Document.Validate()) throw new MessageException("The document has validation errors.");

      using (var svc = ProxyFactory.GetService<IAfxService>(SecurityContext.ServerName))
      {
        DataContext.Document = (FreightManagement.Business.PODHandoverManifest)svc.SetDocumentState(DataContext.Document, stateIdentifier);
        MdiApplicationController.Current.EventAggregator.GetEvent<DocumentSavedEvent>().Publish(new DocumentSavedEventArgs(this.DataContext.Document));
      }
    }

    public void SetDocumentState(string stateIdentifier, DocumentStateEventScope scope)
    {
      if (!DataContext.Document.Validate()) throw new MessageException("The document has validation errors.");

      using (var svc = ProxyFactory.GetService<IAfxService>(SecurityContext.ServerName))
      {
        DataContext.Document = (FreightManagement.Business.PODHandoverManifest)svc.SetDocumentStateWithScope(DataContext.Document, stateIdentifier, scope);
        MdiApplicationController.Current.EventAggregator.GetEvent<DocumentSavedEvent>().Publish(new DocumentSavedEventArgs(this.DataContext.Document));
      }
    }
}
}


﻿using Afx.Business;
using Afx.Business.Documents;
using Afx.Business.Security;
using Afx.Business.Service;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Documents;
using DocumentManagement.Business;
using DocumentManagement.Prism.Extensions.DocumentRepository;
using FleetManagement.Prism.Dialogs.EditVehiclesDialog;
using FreightManagement.Business;
using FreightManagement.Business.Service;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Documents.PODHandoverManifest
{
  public partial class PODHandoverManifestController : MdiDocumentController<PODHandoverManifestContext, PODHandoverManifestViewModel>, IDocumentRepositoryAware
  {
    [InjectionConstructor]
    public PODHandoverManifestController(IController controller)
      : base(controller)
    {
    }

    #region DocumentRepositoryExtensionViewModel DocumentRepositoryExtensionViewModel

    public const string DocumentRepositoryExtensionViewModelProperty = "DocumentRepositoryExtensionViewModel";
    DocumentRepositoryExtensionViewModel mDocumentRepositoryExtensionViewModel;
    public DocumentRepositoryExtensionViewModel DocumentRepositoryExtensionViewModel
    {
      get { return mDocumentRepositoryExtensionViewModel; }
      set { mDocumentRepositoryExtensionViewModel = value; }
    }

    #endregion

    public void AddScannedDocument(ScannedDocument document)
    {
      DocumentRepositoryExtension dre = DataContext.Document.GetExtensionObject<DocumentRepositoryExtension>();
      if (dre != null)
      {
        dre.ScannedDocuments.Add(document);
        RegionManager.Regions[TabRegion].Activate(DocumentRepositoryExtensionViewModel);
      }
    }

    public Document Document { get { return DataContext.Document; } }

    #region PODHandoverManifestScanViewModel ScanViewModel

    public const string ScanViewModelProperty = "ScanViewModel";
    PODHandoverManifestScanViewModel mScanViewModel;
    public PODHandoverManifestScanViewModel ScanViewModel
    {
      get { return mScanViewModel; }
      set { mScanViewModel = value; }
    }

    #endregion

    protected override void ExtendDocument()
    {
      ExtendDocument<PODHandoverManifestHeaderViewModel>(HeaderRegion);
      ScanViewModel = ExtendDocument<PODHandoverManifestScanViewModel>(HeaderRegion);

      ExtendDocument<PODHandoverManifestPODHandoversViewModel>(DockRegion);

      DocumentRepositoryExtensionViewModel = ExtendDocument<DocumentRepositoryExtension, DocumentRepositoryExtensionViewModel>(TabRegion);

      base.ExtendDocument();
    }

    protected override void ExecuteOperation(Operation op, BusinessObject argument)
    {
      switch (op.Identifier)
      {
        case Operations.Verify:
          SetDocumentState(Business.PODHandoverManifest.States.Verified);
          break;

        case Operations.EditVehicles:
          EditVehiclesDialogController evdc = GetCreateChildController<EditVehiclesDialogController>(EditVehiclesDialogController.EditVehiclesDialogControllerKey);
          evdc.VehicleCollection = DataContext.Document;
          if (!BusinessObject.IsNull(DataContext.Document.Courier) && DataContext.Document.Courier != null) evdc.Contractor = DataContext.Document.Courier;
          evdc.Run();
          break;

        case Operations.Close:
          SetDocumentState(Business.PODHandoverManifest.States.Closed);
          break;
      }

      base.ExecuteOperation(op, argument);
    }

    public void AttachPODHandover(string podHandoverNumber)
    {
      try
      {
        using (var svc = ProxyFactory.GetService<IFreightManagementService>(SecurityContext.ServerName))
        {
          AttachPODHandover(svc.LoadPODHandoverReferenceByDocumentNumber(podHandoverNumber));
        }
      }
      catch (Exception ex)
      {
        if (ExceptionHelper.HandleException(ex)) throw ex;
      }
    }

    public void AttachPODHandover(PODHandoverReference podHandover)
    {
      ScanViewModel.ErrorMessage = string.Empty;

      try
      {
        if (BusinessObject.IsNull(podHandover))
        {
          ScanViewModel.ErrorMessage = "POD Handover not found.";
          System.Media.SystemSounds.Exclamation.Play();
          return;
        }

        if (DataContext.Document.IsReadOnly)
        {
          ScanViewModel.ErrorMessage = "Document may not be edited.";
          System.Media.SystemSounds.Exclamation.Play();
          return;
        }

        if (DataContext.Document.State.DocumentTypeState.Identifier != Business.PODHandoverManifest.States.Open)
        {
          ScanViewModel.ErrorMessage = "Document may not be edited.";
          System.Media.SystemSounds.Exclamation.Play();
          return;
        }

        if (DataContext.Document.PODHandovers.Contains(podHandover))
        {
          DataContext.Document.PODHandovers.Remove(podHandover);
        }
        else
        {
          DataContext.Document.PODHandovers.Add(podHandover);
        }
      }
      catch (Exception ex)
      {
        if (ExceptionHelper.HandleException(ex)) throw ex;
      }
    }
  }
}


﻿using Afx.Business.Security;
using Afx.Business.Service;
using Afx.Prism.RibbonMdi.Documents;
using FreightManagement.Business;
using FreightManagement.Business.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Documents.PODHandoverManifest
{
  public partial class PODHandoverManifestContext : DocumentContext<FreightManagement.Business.PODHandoverManifest>
  {
    public PODHandoverManifestContext()
      : base(FreightManagement.Business.PODHandoverManifest.DocumentTypeIdentifier)
    {
    }

    public PODHandoverManifestContext(Guid documentIdentifier)
      : base(FreightManagement.Business.PODHandoverManifest.DocumentTypeIdentifier, documentIdentifier)
    {
    }

    public override void SaveData()
    {
      using (var svc = ProxyFactory.GetService<IFreightManagementService>(SecurityContext.ServerName))
      {
        Document = svc.SavePODHandoverManifest(Document);
      }
      base.SaveData();
    }

    public override void LoadData()
    {
      using (var svc = ProxyFactory.GetService<IFreightManagementService>(SecurityContext.ServerName))
      {
        Document = svc.LoadPODHandoverManifest(DocumentIdentifier);
      }
      base.LoadData();
    }
  }
}


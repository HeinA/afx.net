﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Documents.PODHandoverManifest
{
  public class Operations : Afx.Prism.RibbonMdi.StandardOperations
  {
      public const string Close = "{15eb02c8-f996-43cc-9b52-d64480c492a7}";
      public const string EditVehicles = "{94ae03a0-a3cc-4883-aab3-2f89189b3df7}";
      public const string Verify = "{56d0ec31-0853-4ad6-adb2-f6c9d1e820fb}";
  }
}
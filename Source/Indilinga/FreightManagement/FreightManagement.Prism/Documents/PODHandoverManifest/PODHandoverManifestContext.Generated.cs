﻿using Afx.Business.Security;
using Afx.Business.Service;
using Afx.Prism.RibbonMdi.Documents;
using FreightManagement.Business;
using FreightManagement.Business.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.Composition;

namespace FreightManagement.Prism.Documents.PODHandoverManifest
{
  [Export("Context:" + FreightManagement.Business.PODHandoverManifest.DocumentTypeIdentifier)]
  public partial class PODHandoverManifestContext
  {
  }
}


﻿using Afx.Business;
using Afx.Business.Activities;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Documents;
using FreightManagement.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace FreightManagement.Prism.Documents.PODHandover
{
  public class PODHandoverHeaderViewModel : DocumentHeaderViewModel<FreightManagement.Business.PODHandover>
  {
    #region Constructors

    [InjectionConstructor]
    public PODHandoverHeaderViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion
  }
}


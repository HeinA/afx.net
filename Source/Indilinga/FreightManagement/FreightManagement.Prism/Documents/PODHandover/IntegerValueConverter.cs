﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace FreightManagement.Prism.Documents.PODHandover
{
  public class IntegerValueConverter : IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    {
      int i = (int)value;
      if (i == 0) return string.Empty;
      return i.ToString("#,##0");
    }

    public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    {
      string s = (string)value;
      if (s == string.Empty) return 0;
      try
      {
        return int.Parse(s);
      }
      catch
      {
        return 0;
      }
    }
  }
}

﻿using Afx.Business.Security;
using Afx.Business.Service;
using Afx.Prism.RibbonMdi.Documents;
using FreightManagement.Business;
using FreightManagement.Business.Service;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Documents.PODHandover
{
  public partial class PODHandoverContext : DocumentContext<FreightManagement.Business.PODHandover>
  {
    public PODHandoverContext()
      : base(FreightManagement.Business.PODHandover.DocumentTypeIdentifier)
    {
    }

    public PODHandoverContext(Guid documentIdentifier)
      : base(FreightManagement.Business.PODHandover.DocumentTypeIdentifier, documentIdentifier)
    {
    }

    public override void SaveData()
    {
      using (var svc = ProxyFactory.GetService<IFreightManagementService>(SecurityContext.ServerName))
      {
        Document = svc.SavePODHandover(Document);
      }
      base.SaveData();
    }

    public override void LoadData()
    {
      using (var svc = ProxyFactory.GetService<IFreightManagementService>(SecurityContext.ServerName))
      {
        Document = svc.LoadPODHandover(DocumentIdentifier);
      }
      base.LoadData();
    }
  }
}


﻿using Afx.Business;
using Afx.Business.Documents;
using Afx.Business.Security;
using Afx.Business.Service;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Documents;
using DocumentManagement.Business;
using DocumentManagement.Prism.Extensions.DocumentRepository;
using FreightManagement.Business;
using FreightManagement.Business.Service;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Documents.PODHandover
{
  public partial class PODHandoverController : MdiDocumentController<PODHandoverContext, PODHandoverViewModel>, IDocumentRepositoryAware
  {
    [InjectionConstructor]
    public PODHandoverController(IController controller)
      : base(controller)
    {
    }

    #region DocumentRepositoryExtensionViewModel DocumentRepositoryExtensionViewModel

    public const string DocumentRepositoryExtensionViewModelProperty = "DocumentRepositoryExtensionViewModel";
    DocumentRepositoryExtensionViewModel mDocumentRepositoryExtensionViewModel;
    public DocumentRepositoryExtensionViewModel DocumentRepositoryExtensionViewModel
    {
      get { return mDocumentRepositoryExtensionViewModel; }
      set { mDocumentRepositoryExtensionViewModel = value; }
    }

    #endregion

    #region PODHandoverScanViewModel ScanViewModel

    public const string ScanViewModelProperty = "ScanViewModel";
    PODHandoverScanViewModel mScanViewModel;
    public PODHandoverScanViewModel ScanViewModel
    {
      get { return mScanViewModel; }
      set { mScanViewModel = value; }
    }

    #endregion

    protected override void ExtendDocument()
    {
      ExtendDocument<PODHandoverHeaderViewModel>(HeaderRegion);
      ScanViewModel = ExtendDocument<PODHandoverScanViewModel>(HeaderRegion);

      ExtendDocument<PODHandoverWaybillsViewModel>(DockRegion);

      DocumentRepositoryExtensionViewModel = ExtendDocument<DocumentRepositoryExtension, DocumentRepositoryExtensionViewModel>(TabRegion);

      base.ExtendDocument();
    }

    public Document Document { get { return DataContext.Document; } }

    protected override void ExecuteOperation(Operation op, BusinessObject argument)
    {
      switch (op.Identifier)
      {
        case Operations.Finalize:
          SetDocumentState(Business.PODHandover.States.Closed);
          break;

      }
      base.ExecuteOperation(op, argument);
    }

    public void AttachWaybill(string waybillNumber)
    {
      using (var svc = ProxyFactory.GetService<IFreightManagementService>(SecurityContext.ServerName))
      {
        AttachWaybill(svc.LoadWaybillReferenceByDocumentNumber(waybillNumber));
      }
    }

    public void AttachWaybill(WaybillReference waybill)
    {
      ScanViewModel.ErrorMessage = string.Empty;

      ScanViewModel.ErrorMessage = string.Empty;

      if (BusinessObject.IsNull(waybill))
      {
        ScanViewModel.ErrorMessage = "Waybill not found.";
        System.Media.SystemSounds.Exclamation.Play();
        return;
      }

      if (!(waybill.WaybillState.Identifier == Business.Waybill.States.Invoiced || waybill.WaybillState.Identifier == Business.Waybill.States.PendingInvoice || waybill.WaybillState.Identifier == Business.Waybill.States.Complete))
      {
        ScanViewModel.ErrorMessage = "Waybill has not been invoiced";
        System.Media.SystemSounds.Exclamation.Play();
        return;
      }

      if (DataContext.Document.IsReadOnly)
      {
        ScanViewModel.ErrorMessage = "Document may not be edited.";
        System.Media.SystemSounds.Exclamation.Play();
        return;
      }

      if (DataContext.Document.State.DocumentTypeState.Identifier != Business.PODHandover.States.Open)
      {
        ScanViewModel.ErrorMessage = "Document may not be edited.";
        System.Media.SystemSounds.Exclamation.Play();
        return;
      }

      if (waybill.PODHandoverCount != 0)
      {
        ScanViewModel.ErrorMessage = "Waybill already on a POD Handover.";
        System.Media.SystemSounds.Exclamation.Play();
        return;
      }

      if (BusinessObject.IsNull(DataContext.Document.Account))
      {
        DataContext.Document.Account = waybill.Account;
      }
      else
      {
        if (!DataContext.Document.Account.Equals(waybill.Account))
        {
          ScanViewModel.ErrorMessage = "Waybill account incorrect.";
          System.Media.SystemSounds.Exclamation.Play();
          return;
        }
      }

      if (DataContext.Document.Waybills.Contains(waybill))
      {
        DataContext.Document.Waybills.Remove(waybill);
        if (DataContext.Document.Waybills.Count == 0)
        {
          DataContext.Document.Account = null;
        }
      }
      else
      {
        DataContext.Document.Waybills.Add(waybill);
      }
    }

    public void AddScannedDocument(ScannedDocument document)
    {
      DocumentRepositoryExtension dre = DataContext.Document.GetExtensionObject<DocumentRepositoryExtension>();
      if (dre != null)
      {
        dre.ScannedDocuments.Add(document);
        RegionManager.Regions[TabRegion].Activate(DocumentRepositoryExtensionViewModel);
      }
    }
  }
}


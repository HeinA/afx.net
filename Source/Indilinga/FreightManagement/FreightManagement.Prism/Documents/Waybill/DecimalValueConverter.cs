﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace FreightManagement.Prism.Documents.Waybill
{
  public class DecimalValueConverter : IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    {
      decimal d = (decimal)value;
      if (d == 0) return string.Empty;
      return d.ToString("#,##0.00");
    }

    public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    {
      string s = (string)value;
      if (s == string.Empty) return 0;
      try
      {
        return decimal.Parse(s.Replace("..", "."));
      }
      catch
      {
        return 0;
      }
    }
  }
}

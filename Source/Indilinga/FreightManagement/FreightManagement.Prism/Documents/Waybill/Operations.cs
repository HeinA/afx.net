﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Documents.Waybill
{
  public class Operations : Afx.Prism.RibbonMdi.StandardOperations
  {
    public const string AcceptQuote = "{c3b572ec-4b5b-415d-b7d1-e6eaeaf4ade3}";
    public const string AttachParcel = "{8419b0a5-a1e4-4ba5-ad52-e7b6631f0828}";
    public const string Cancel = "{59551dde-36b9-4736-aa78-02faa94e7ff0}";
    public const string Collected = "{a915789b-a865-483b-a203-04b78bf6e24c}";
    public const string Cost_Route = "{869e748c-c86f-4202-9065-65ec372d3f0e}";
    public const string Deliver = "{38dffbab-0686-4a5e-a22f-ccdc537b0230}";
    public const string Finalize = "{6cd98607-54af-41fd-a930-49206a051d58}";
    public const string Invoiced = "{65fab246-8e88-4727-bf2d-be7262578cdf}";
    public const string MissedCollection = "{094cb207-3b8c-45bd-811d-0b1f867fe8a6}";
    public const string PODImaged = "{ea01f6b3-c72b-4c1c-a6e1-6d90aa209cfb}";
    public const string PrintLabels = "{be2a2cfb-ec16-45c3-9b56-41774d5c50c5}";
    public const string PrintProFormaInvoice = "{2dc1e1a0-28ef-4850-8b5b-b886f2722107}";
    public const string PrintText = "{2e55975b-b940-424b-b435-430eab72c59a}";
    public const string ReadyforInvoicing = "{e6b7d87a-9f29-4472-be98-680f8327144a}";
    public const string SetBay = "{a23391d1-78da-4746-a7fa-4cf3c7c1db9b}";
    public const string Verify_Receive = "{2f79d71f-d85a-48d2-b15a-7af7b5606da8}";
  }
}
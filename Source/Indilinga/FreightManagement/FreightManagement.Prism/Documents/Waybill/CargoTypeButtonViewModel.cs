﻿using Afx.Prism;
using FreightManagement.Business;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Documents.Waybill
{
  public class CargoTypeButtonViewModel : ViewModel<CargoType>
  {
    #region Constructors

    [InjectionConstructor]
    public CargoTypeButtonViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    [Dependency]
    public WaybillController WaybillController { get; set; }

    #region DelegateCommand AddCommand

    DelegateCommand mAddCommand;
    public DelegateCommand AddCommand
    {
      get { return mAddCommand ?? (mAddCommand = new DelegateCommand(ExecuteAdd, CanExecuteAdd)); }
    }

    bool CanExecuteAdd()
    {
      return true;
    }

    void ExecuteAdd()
    {
      WaybillController.DataContext.Document.Items.Add(new WaybillCargoItem() { CargoType = Model });
    }

    #endregion

  }
}

﻿using Afx.Prism;
using Afx.Prism.RibbonMdi.Documents;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Documents.Waybill
{
  public class WaybillBayMovementViewModel : ExtensionViewModel<FreightManagement.Business.Waybill>
  {
    #region Constructors

    [InjectionConstructor]
    public WaybillBayMovementViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    #region string Title

    public string Title
    {
      get { return "Bay Movements"; }
    }

    #endregion

  }
}

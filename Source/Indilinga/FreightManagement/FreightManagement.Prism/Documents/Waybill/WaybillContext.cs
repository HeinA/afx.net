﻿using Afx.Business.Security;
using Afx.Business.Service;
using Afx.Prism.RibbonMdi.Documents;
using FreightManagement.Business;
using FreightManagement.Business.Service;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Documents.Waybill
{
  public partial class WaybillContext : DocumentContext<FreightManagement.Business.Waybill>
  {
    public WaybillContext()
      : base(FreightManagement.Business.Waybill.DocumentTypeIdentifier)
    {
    }

    public WaybillContext(Guid documentIdentifier)
      : base(FreightManagement.Business.Waybill.DocumentTypeIdentifier, documentIdentifier)
    {
    }

    public override void SaveData()
    {
      using (var svc = ProxyFactory.GetService<IFreightManagementService>(SecurityContext.ServerName))
      {
        Document = svc.SaveWaybill(Document);
      }
      base.SaveData();
    }

    public override void LoadData()
    {
      using (var svc = ProxyFactory.GetService<IFreightManagementService>(SecurityContext.ServerName))
      {
        Document = svc.LoadWaybill(DocumentIdentifier);
      }
      base.LoadData();
    }
  }
}


﻿using Afx.Business.Data;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Documents;
using FreightManagement.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Documents.Waybill
{
  public class WaybillFlagsViewModel : ExtensionViewModel<FreightManagement.Business.Waybill>
  {
    #region Constructors

    [InjectionConstructor]
    public WaybillFlagsViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    #region string Title

    public string Title
    {
      get { return "Flags"; }
    }

    #endregion

    public IEnumerable<WaybillFlagGroupViewModel> Flags
    {
      get { return ResolveViewModels<WaybillFlagGroupViewModel, WaybillFlagGroup>(Cache.Instance.GetObjects<WaybillFlagGroup>((r) => r.Text, true)); }
    }
  }
}

﻿using Afx.Business;
using Afx.Business.Documents;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace FreightManagement.Prism.Documents.Waybill.ReceiveWaybillDialog
{
  public class ReceiveWaybillDialogController : MdiDialogController<ReceiveWaybillDialogContext, ReceiveWaybillDialogViewModel>
  {
    public const string ReceiveWaybillDialogControllerKey = "FreightManagement.Prism.Documents.Waybill.ReceiveWaybillDialog.ReceiveWaybillDialogController";

    public ReceiveWaybillDialogController(IController controller)
      : base(ReceiveWaybillDialogControllerKey, controller)
    {
    }

    [Dependency]
    public WaybillController WaybillController { get; set; }

    protected override bool OnRun()
    {
      if (DataContext == null) DataContext = new ReceiveWaybillDialogContext();
      ViewModel.Caption = "Receive Waybill";
      return base.OnRun();
    }

    protected override void ApplyChanges()
    {
      if (!BusinessObject.IsNull(ViewModel.SelectedDistributionCenter) && ViewModel.SelectedDistributionCenter.OrganizationalUnit != null)
      {
        try
        {
          WaybillController.SetDocumentState(Business.Waybill.States.OnFloor, new DocumentStateEventScope(ViewModel.SelectedDistributionCenter.OrganizationalUnit));
        }
        catch (Exception ex)
        {
          if (ExceptionHelper.HandleException(ex)) throw ex;
        }
      }
      else
      {
        ///TODO:  Fix this up.
        System.Windows.MessageBox.Show("Organizational Unit cannot be blank!", ViewModel.Caption, System.Windows.MessageBoxButton.OK);
        return;
      }
      base.ApplyChanges();
    }
  }
}

﻿using Afx.Business.Security;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using FreightManagement.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Documents.Waybill.ReceiveWaybillDialog
{
  public class ReceiveWaybillDialogViewModel : MdiDialogViewModel<ReceiveWaybillDialogContext>
  {
    [InjectionConstructor]
    public ReceiveWaybillDialogViewModel(IController controller)
      : base(controller)
    {
    }

    #region ReceiveWaybillDialogController ReceiveWaybillDialogController

    public const string ReceiveWaybillDialogControllerProperty = "ReceiveWaybillDialogController";
    ReceiveWaybillDialogController mReceiveWaybillDialogController;
    [Dependency]
    public ReceiveWaybillDialogController ReceiveWaybillDialogController
    {
      get { return mReceiveWaybillDialogController; }
      set
      {
        mReceiveWaybillDialogController = value;
        if (ReceiveWaybillDialogController.WaybillController.DataContext.Document.NextDistributionCenter == null)
        {
          
        }
        else
        {
          if (ReceiveWaybillDialogController.WaybillController.DataContext.Document.NextDistributionCenter.OrganizationalUnit == SecurityContext.OrganizationalUnit)
          {
            SelectedDistributionCenter = ReceiveWaybillDialogController.WaybillController.DataContext.Document.NextDistributionCenter;
          }
        }
      }
    }

    #endregion

    public IEnumerable<DistributionCenter> DistributionCenters
    {
      get { return ReceiveWaybillDialogController.WaybillController.DataContext.Document.Routes.Where(r => !r.IsDeleted).Select(r => r.DistributionCenter); } //.Where(r => r.ArrivalTimestamp == null && r.DepartureTimestamp == null)
    }

    #region DistributionCenter SelectedDistributionCenter

    public const string SelectedDistributionCenterProperty = "SelectedDistributionCenter";
    DistributionCenter mSelectedDistributionCenter;
    public DistributionCenter SelectedDistributionCenter
    {
      get { return mSelectedDistributionCenter; }
      set { SetProperty<DistributionCenter>(ref mSelectedDistributionCenter, value); }
    }

    #endregion
  }
}

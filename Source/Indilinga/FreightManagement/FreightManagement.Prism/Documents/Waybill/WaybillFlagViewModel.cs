﻿using Afx.Prism;
using FreightManagement.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Documents.Waybill
{
  public class WaybillFlagViewModel : SelectableViewModel<WaybillFlag>
  {
    [InjectionConstructor]
    public WaybillFlagViewModel(IController controller)
      : base(controller)
    {
    }

    public new WaybillFlagGroupViewModel Parent
    {
      get { return (WaybillFlagGroupViewModel)base.Parent; }
    }


    public bool IsFlagged
    {
      get
      {
        if (Parent.Parent.Model.WaybillFlags.Contains(Model)) return true;
        return false;
      }
      set
      {
        if (value)
        {
          if (!Parent.Parent.Model.WaybillFlags.Contains(Model))
          {
            Parent.Parent.Model.WaybillFlags.Add(Model);
            OnPropertyChanged("IsFlagged");
          }
        }
        else
        {
          if (Parent.Parent.Model.WaybillFlags.Contains(Model))
          {
            Parent.Parent.Model.WaybillFlags.Remove(Model);
            OnPropertyChanged("IsFlagged");
          }
        }
      }
    }
  }
}

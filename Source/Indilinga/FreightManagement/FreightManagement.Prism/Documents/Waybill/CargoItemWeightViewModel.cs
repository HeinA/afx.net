﻿using Afx.Business.Security;
using Afx.Prism;
using FreightManagement.Business;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Documents.Waybill
{
  public class CargoItemWeightViewModel : ViewModel<WaybillCargoItem>
  {
    #region Constructors

    [InjectionConstructor]
    public CargoItemWeightViewModel(IController controller)
      : base(controller)
    {
    }
    
    #endregion

    public override bool IsReadOnly
    {
      get
      {
        return Model.Owner.IsReadOnly;
      }
    }

    //public IEnumerable<string> Categories
    //{
    //  get
    //  {
    //    Collection<string> col = new Collection<string>();
    //    foreach (var ci in Model.CargoType.Categories.OrderBy(c => c.Text))
    //    {
    //      col.Add(ci.Text);
    //    }
    //    return col;
    //  }
    //}

    //public IEnumerable<CargoTypeCategory> Categories
    //{
    //  get
    //  {
    //    Collection<CargoTypeCategory> col = new Collection<CargoTypeCategory>();
    //    col.Add(new CargoTypeCategory(true));
    //    foreach(var ci in Model.CargoType.Categories.OrderBy(c => c.Text))
    //    {
    //      col.Add(ci);
    //    }
    //    return col;
    //  }
    //}

    new WaybillItemsViewModel Parent
    {
      get { return (WaybillItemsViewModel)base.Parent; }
    }

    #region DelegateCommand DeleteCommand

    DelegateCommand mDeleteCommand;
    public DelegateCommand DeleteCommand
    {
      get { return mDeleteCommand ?? (mDeleteCommand = new DelegateCommand(ExecuteDelete, CanExecuteDelete)); }
    }

    bool CanExecuteDelete()
    {
      return true;
    }

    void ExecuteDelete()
    {
      //Parent.Model.Items.Remove(Model);
      Model.IsDeleted = true;
      Parent.RefreshItems();
    }

    #endregion

  }
}

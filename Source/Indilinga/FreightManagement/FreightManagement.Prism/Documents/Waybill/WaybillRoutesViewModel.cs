﻿using Afx.Prism;
using Afx.Prism.RibbonMdi.Documents;
using FreightManagement.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace FreightManagement.Prism.Documents.Waybill
{
  public class WaybillRoutesViewModel : ExtensionViewModel<FreightManagement.Business.Waybill>
  {
    #region Constructors

    [InjectionConstructor]
    public WaybillRoutesViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    #region string Title

    public string Title
    {
      get { return "Routing"; }
    }

    #endregion

    protected override void OnModelChanged()
    {
      base.OnModelChanged();
      CollectionViewSource cvs = new CollectionViewSource();
      cvs.Source = Model.Routes;
      cvs.Filter += (s, e) =>
      {
        WaybillRoute wr = e.Item as WaybillRoute;
        e.Accepted = !wr.IsDeleted;
      };
      Routes = cvs;
    }

    #region CollectionViewSource Routes

    public const string RoutesProperty = "Routes";
    CollectionViewSource mRoutes;
    public CollectionViewSource Routes
    {
      get { return mRoutes; }
      private set { SetProperty<CollectionViewSource>(ref mRoutes, value); }
    }

    #endregion
  }
}

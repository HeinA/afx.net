﻿using Afx.Prism;
using Afx.Prism.RibbonMdi;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace FreightManagement.Prism.Documents.Waybill.PrintProFormaInvoiceDialog
{
  public class PrintProFormaInvoiceDialogController : MdiDialogController<PrintProFormaInvoiceDialogContext, PrintProFormaInvoiceDialogViewModel>
  {
    public const string PrintProFormaInvoiceDialogControllerKey = "FreightManagement.Prism.Documents.Waybill.PrintProFormaInvoiceDialog.PrintProFormaInvoiceDialogController";

    public PrintProFormaInvoiceDialogController(IController controller)
      : base(PrintProFormaInvoiceDialogControllerKey, controller)
    {
    }

    [Dependency]
    public WaybillController WaybillController { get; set; }

    protected override bool OnRun()
    {
      if (DataContext == null) DataContext = new PrintProFormaInvoiceDialogContext();
      ViewModel.Caption = "Pro-Forma Invoice";
      DataContext.VatRate = ViewModel.IsCrossBorder ? 0m : ViewModel.DeliveryCountryVatRate;
      return base.OnRun();
    }

    protected override void ApplyChanges()
    {
      WaybillController.PrintProForma(DataContext.VatRate);
      base.ApplyChanges();
    }
  }
}

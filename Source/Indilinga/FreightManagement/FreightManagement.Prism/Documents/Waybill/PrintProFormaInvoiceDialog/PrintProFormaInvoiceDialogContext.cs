﻿using Afx.Business;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Documents.Waybill.PrintProFormaInvoiceDialog
{
  public class PrintProFormaInvoiceDialogContext : BusinessObject
  {
    #region decimal VatRate

    public const string VatRateProperty = "VatRate";
    decimal mVatRate;
    public decimal VatRate
    {
      get { return mVatRate; }
      set { SetProperty<decimal>(ref mVatRate, value); }
    }

    #endregion
  }
}
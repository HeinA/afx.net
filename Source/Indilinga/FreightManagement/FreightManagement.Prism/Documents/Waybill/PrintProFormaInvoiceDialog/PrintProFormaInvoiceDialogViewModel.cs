﻿using Afx.Prism;
using Afx.Prism.RibbonMdi;
using FreightManagement.Business;
using Geographics.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Documents.Waybill.PrintProFormaInvoiceDialog
{
  public class PrintProFormaInvoiceDialogViewModel : MdiDialogViewModel<PrintProFormaInvoiceDialogContext>
  {
    [InjectionConstructor]
    public PrintProFormaInvoiceDialogViewModel(IController controller)
      : base(controller)
    {
    }

    [Dependency]
    public WaybillController WaybillController { get; set; }

    public bool IsCrossBorder
    {
      get { return !CollectionCountry.Equals(DeliveryCountry); }
    }

    public Location CollectionCountry
    {
      get { return WaybillController.DataContext.Document.CollectionLocation.Country; }
    }

    public Location DeliveryCountry
    {
      get { return WaybillController.DataContext.Document.DeliveryLocation.Country; }
    }

    public decimal DeliveryCountryVatRate
    {
      get { return DeliveryCountry.GetExtensionObject<LocationExtension>().StandardVATRate; }
    }

    public string Message
    {
      get
      {
        string message = string.Empty;
        if (IsCrossBorder)
        {
          message = "Waybill is cross-border.  No Vat will be charged.";
        }
        else
        {
          message = string.Format("Waybill is within {0}, Vat will be charged at {1:#,##0.00}%", DeliveryCountry.Name, DeliveryCountryVatRate);
        }
        return message;
      }
    }
  }
}

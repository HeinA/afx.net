﻿using Afx.Business;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Documents;
using FreightManagement.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace FreightManagement.Prism.Documents.Waybill
{
  public class WaybillItemsViewModel : ExtensionViewModel<FreightManagement.Business.Waybill>
  {
    #region Constructors

    [InjectionConstructor]
    public WaybillItemsViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    [Dependency]
    public WaybillController WaybillController { get; set; }

    List<CargoType> CargoTypes
    {
      get
      {
        if (!BusinessObject.IsNull(WaybillController.DataContext.Document.RatesConfiguration))
        {
          if (Model.IsPointToPoint)
          {
            return WaybillController.DataContext.Document.RatesConfiguration.GetRevision(Model.DocumentDate).CargoTypes
              .Union(WaybillController.StandardRatesConfiguration.GetRevision(Model.DocumentDate).CargoTypes)
              .Distinct()
              .Where(ct => ct.IsPointToPointType == true)
              .OrderBy(i => i.Text)
              .ToList();
          }
          else
          {
            return WaybillController.DataContext.Document.RatesConfiguration.GetRevision(Model.DocumentDate).CargoTypes
              .Union(WaybillController.StandardRatesConfiguration.GetRevision(Model.DocumentDate).CargoTypes)
              .Distinct()
              .Where(ct => ct.IsDistributionType == true)
              .OrderBy(i => i.Text)
              .ToList();
          }
        }
        else
        {
          if (Model.IsPointToPoint)
          {
            return WaybillController.StandardRatesConfiguration.GetRevision(Model.DocumentDate).CargoTypes
              .Distinct()
              .Where(ct => ct.IsPointToPointType == true)
              .OrderBy(i => i.Text)
              .ToList();
          }
          else
          {
            return WaybillController.StandardRatesConfiguration.GetRevision(Model.DocumentDate).CargoTypes
              .Distinct()
              .Where(ct => ct.IsDistributionType == true)
              .OrderBy(i => i.Text)
              .ToList();
          }
        }
      }
    }

    public IEnumerable<CargoTypeButtonViewModel> CargoTypeButtonViewModels
    {
      get { return ResolveViewModels<CargoTypeButtonViewModel, CargoType>(CargoTypes); }
    }

    public void Refresh()
    {
      OnPropertyChanged("CargoTypeButtonViewModels");
    }

    public IEnumerable<ViewModel> CargoItems
    {
      get
      {
        Collection<ViewModel> col = new Collection<ViewModel>();
        foreach (var i in Model.Items)
        {
          if (i.IsDeleted) continue;

          ViewModel vm = null;
          switch (i.CargoType.BillingUnit)
          {
            case BillingUnit.Weight:
              vm = WaybillController.GetCreateViewModel<CargoItemWeightViewModel>(i, this);
              break;

            case BillingUnit.Quantity:
              if (i.CargoType.IsSingleUnit)
              {
                vm = WaybillController.GetCreateViewModel<CargoItemQuantitySingleViewModel>(i, this);
              }
              else
              {
                vm = WaybillController.GetCreateViewModel<CargoItemQuantityMultipleViewModel>(i, this);
              }
              break;

            case BillingUnit.Value:
              vm = WaybillController.GetCreateViewModel<CargoItemValueViewModel>(i, this);
              break;
          }

          if (vm != null) col.Add(vm);
        }
        return col;
      }
    }

    protected override void OnModelChanged()
    {
      WeakEventManager<INotifyCollectionChanged, NotifyCollectionChangedEventArgs>.AddHandler(Model.Items, "CollectionChanged", Items_CollectionChanged);

      base.OnModelChanged();
    }

    void Items_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
    {
      OnPropertyChanged("CargoItems");
    }

    public void RefreshItems()
    {
      OnPropertyChanged("CargoItems");
    }
  }
}

﻿using Afx.Prism;
using Afx.Prism.RibbonMdi.Documents;
using FreightManagement.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace FreightManagement.Prism.Documents.Waybill
{
  public class WaybillParcelViewModel : ExtensionViewModel<FreightManagement.Business.Waybill>
  {
    #region Constructors

    [InjectionConstructor]
    public WaybillParcelViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    #region string Title

    public string Title
    {
      get { return "P_arcels"; }
    }

    #endregion  

    protected override void OnModelChanged()
    {
      base.OnModelChanged();
      CollectionViewSource cvs = new CollectionViewSource();
      cvs.Source = Model.Parcels;
      cvs.Filter += (s, e) =>
      {
        WaybillParcel wr = e.Item as WaybillParcel;
        e.Accepted = !wr.IsDeleted;
      };
      Parcels = cvs;
    }

    #region CollectionViewSource Parcels

    public const string ParcelsProperty = "Parcels";
    CollectionViewSource mParcels;
    public CollectionViewSource Parcels
    {
      get { return mParcels; }
      private set { SetProperty<CollectionViewSource>(ref mParcels, value); }
    }

    #endregion  
  }
}

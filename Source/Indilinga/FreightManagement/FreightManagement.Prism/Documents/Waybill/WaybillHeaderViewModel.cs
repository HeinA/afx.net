﻿using AccountManagement.Business.Service;
using Afx.Business;
using Afx.Business.Activities;
using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Business.Service;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using Afx.Prism.RibbonMdi.Documents;
using ContactManagement.Business;
using FreightManagement.Business;
using FreightManagement.Prism.Documents.Waybill.EditAddressDialog;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace FreightManagement.Prism.Documents.Waybill
{
  public class WaybillHeaderViewModel : DocumentHeaderViewModel<FreightManagement.Business.Waybill>
  {
    #region Constructors

    [InjectionConstructor]
    public WaybillHeaderViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    [Dependency]
    public WaybillController WaybillController { get; set; }

    protected override void OnModelPropertyChanged(string propertyName)
    {
      try
      {
        switch (propertyName)
        {
          case Business.Waybill.AccountProperty:
            OnPropertyChanged(RatesConfigurationVisibilityProperty);
            OnPropertyChanged(AccountAddressVisibilityProperty);
            OnPropertyChanged(RatesConfigurationsProperty);
            if (RatesConfigurations != null && RatesConfigurations.Count() != 0)
            {
              Model.RatesConfiguration = RatesConfigurations.FirstOrDefault();
            }
            else
            {
              Model.RatesConfiguration = null;
            }
            UpdateAddresses();
            break;

          case Business.Waybill.RatesConfigurationProperty:
            WaybillController.WaybillItemsViewModel.Refresh();
            break;

          case Business.Waybill.IsPointToPointProperty:
            WaybillController.WaybillItemsViewModel.Refresh();
            break;
        }
      }
      catch (Exception ex)
      {
        if (ExceptionHelper.HandleException(ex)) throw ex;
      }

      base.OnModelPropertyChanged(propertyName);
    }

    protected override void OnModelChanged()
    {
      base.OnModelChanged();
      UpdateAddresses();
    }

    private void UpdateAddresses()
    {
      if (BusinessObject.IsNull(Model.Account)) return;

      Task.Run(() =>
      {
        try
        {
          if (String.IsNullOrEmpty(SecurityContext.ServerName)) { SystemSounds.Exclamation.Play(); return; }
          using (var svc = ProxyFactory.GetService<IAccountManagementService>(SecurityContext.ServerName))
          {
            var add = svc.LoadAccountAddresses(Model.Account.GlobalIdentifier);
            if (add == null) return;

            MdiApplicationController.Current.Dispatcher.BeginInvoke(new Action(() =>
            {
              try
              {
                ShipFromAddresses = add.Where(a => a.AddressType.IsFlagged(FreightManagement.Business.AddressTypeFlags.ShipFromAddress)).OrderBy(a => a.AddressTextWithNameSingleLine);
                ShipToAddresses = add.Where(a => a.AddressType.IsFlagged(FreightManagement.Business.AddressTypeFlags.ShipToAddress)).OrderBy(a => a.AddressTextWithNameSingleLine);

                if (string.IsNullOrWhiteSpace(Model.CollectionAddress))
                {
                  SelectedShipFromAddress = ShipFromAddresses.Where(a => a.Id == Model.Account.GetExtensionObject<AccountReferenceExtension>().DefaultShipFromAddress).FirstOrDefault();
                }
              }
              catch (Exception ex)
              {
                if (ExceptionHelper.HandleException(ex)) throw;
              }
            }), System.Windows.Threading.DispatcherPriority.Normal);
          }

        }
        catch (Exception ex)
        {
          ExceptionHelper.HandleException(ex);
        }
      });
    }

    #region IEnumerable<Address> ShipFromAddresses

    public const string ShipFromAddressesProperty = "ShipFromAddresses";
    IEnumerable<Address> mShipFromAddresses;
    public IEnumerable<Address> ShipFromAddresses
    {
      get { return mShipFromAddresses; }
      set { SetProperty<IEnumerable<Address>>(ref mShipFromAddresses, value); }
    }

    #endregion

    #region IEnumerable<Address> ShipToAddresses

    public const string ShipToAddressesProperty = "ShipToAddresses";
    IEnumerable<Address> mShipToAddresses;
    public IEnumerable<Address> ShipToAddresses
    {
      get { return mShipToAddresses; }
      set { SetProperty<IEnumerable<Address>>(ref mShipToAddresses, value); }
    }

    #endregion

    #region Address SelectedShipFromAddress

    public const string SelectedShipFromAddressProperty = "SelectedShipFromAddress";
    Address mSelectedShipFromAddress;
    public Address SelectedShipFromAddress
    {
      get { return mSelectedShipFromAddress; }
      set
      {
        SetProperty<Address>(ref mSelectedShipFromAddress, value);
        if (value != null)
        {
          Model.CollectionAddress = value.AddressTextWithContact;
          Model.Consigner = value.Name;
          Model.ConsignerImportReference = value.ImportReference;
          Model.ConsignerExportReference = value.ExportReference;
        }
      }
    }

    #endregion

    #region Address SelectedShipToAddress

    public const string SelectedShipToAddressProperty = "SelectedShipToAddress";
    Address mSelectedShipToAddress;
    public Address SelectedShipToAddress
    {
      get { return mSelectedShipToAddress; }
      set
      {
        SetProperty<Address>(ref mSelectedShipToAddress, value);
        if (value != null)
        {
          Model.DeliveryAddress = value.AddressTextWithContact;
          Model.Consignee = value.Name;
          Model.ConsigneeImportReference = value.ImportReference;
          Model.ConsigneeExportReference = value.ExportReference;
        }
      }
    }

    #endregion

    #region DelegateCommand AddShipFromAddressCommand

    DelegateCommand mAddShipFromAddressCommand;
    public DelegateCommand AddShipFromAddressCommand
    {
      get { return mAddShipFromAddressCommand ?? (mAddShipFromAddressCommand = new DelegateCommand(ExecuteAddShipFromAddress, CanExecuteAddShipFromAddress)); }
    }

    bool CanExecuteAddShipFromAddress()
    {
      return true;
    }

    void ExecuteAddShipFromAddress()
    {
      EditAddressDialogController ac = Controller.CreateChildController<EditAddressDialogController>();
      ac.AddressType = EditAddressDialog.AddressType.ShipFrom;
      ac.WaybillHeaderViewModel = this;
      ac.Run();
    }

    #endregion

    #region DelegateCommand EditShipFromAddressCommand

    DelegateCommand mEditShipFromAddressCommand;
    public DelegateCommand EditShipFromAddressCommand
    {
      get { return mEditShipFromAddressCommand ?? (mEditShipFromAddressCommand = new DelegateCommand(ExecuteEditShipFromAddress, CanExecuteEditShipFromAddress)); }
    }

    bool CanExecuteEditShipFromAddress()
    {
      return true;
    }

    void ExecuteEditShipFromAddress()
    {
      try
      {
        if (BusinessObject.IsNull(SelectedShipFromAddress)) throw new MessageException("No Ship From address is selected");
        EditAddressDialogController ac = Controller.CreateChildController<EditAddressDialogController>();
        ac.AddressType = EditAddressDialog.AddressType.ShipFrom;
        ac.Address = SelectedShipFromAddress;
        ac.WaybillHeaderViewModel = this;
        ac.Run();
      }
      catch (Exception ex)
      {
        if (ExceptionHelper.HandleException(ex)) throw ex;
      }
    }

    #endregion

    #region DelegateCommand AddShipToAddressCommand

    DelegateCommand mAddShipToAddressCommand;
    public DelegateCommand AddShipToAddressCommand
    {
      get { return mAddShipToAddressCommand ?? (mAddShipToAddressCommand = new DelegateCommand(ExecuteAddShipToAddress, CanExecuteAddShipToAddress)); }
    }

    bool CanExecuteAddShipToAddress()
    {
      return true;
    }

    void ExecuteAddShipToAddress()
    {
      EditAddressDialogController ac = Controller.CreateChildController<EditAddressDialogController>();
      ac.AddressType = EditAddressDialog.AddressType.ShipTo;
      ac.WaybillHeaderViewModel = this;
      ac.Run();
    }

    #endregion

    #region DelegateCommand EditShipToAddressCommand

    DelegateCommand mEditShipToAddressCommand;
    public DelegateCommand EditShipToAddressCommand
    {
      get { return mEditShipToAddressCommand ?? (mEditShipToAddressCommand = new DelegateCommand(ExecuteEditShipToAddress, CanExecuteEditShipToAddress)); }
    }

    bool CanExecuteEditShipToAddress()
    {
      return true;
    }

    void ExecuteEditShipToAddress()
    {
      try
      {
        if (BusinessObject.IsNull(SelectedShipToAddress)) throw new MessageException("No Ship To address is selected");
        EditAddressDialogController ac = Controller.CreateChildController<EditAddressDialogController>();
        ac.AddressType = EditAddressDialog.AddressType.ShipTo;
        ac.Address = SelectedShipToAddress;
        ac.WaybillHeaderViewModel = this;
        ac.Run();
      }
      catch (Exception ex)
      {
        if (ExceptionHelper.HandleException(ex)) throw ex;
      }
    }

    #endregion

    const string RatesConfigurationsProperty = "RatesConfigurations";
    public IEnumerable<RatesConfigurationReference> RatesConfigurations
    {
      get
      {
        if (BusinessObject.IsNull(Model.Account)) return null;
        return Model.Account.GetExtensionObject<AccountReferenceExtension>().Rates;
      }
    }

    const string RatesConfigurationVisibilityProperty = "RatesConfigurationVisibility";
    public Visibility RatesConfigurationVisibility
    {
      get
      {
        if (BusinessObject.IsNull(Model.Account)) return Visibility.Collapsed;
        return RatesConfigurations.Count() == 0 ? Visibility.Collapsed : Visibility.Visible;
      }
    }

    #region Visibility AccountAddressVisibility

    const string AccountAddressVisibilityProperty = "AccountAddressVisibility";
    public Visibility AccountAddressVisibility
    {
      get
      {
        if (BusinessObject.IsNull(Model.Account)) return Visibility.Collapsed;
        return Visibility.Visible;
      }
    }

    #endregion

    public IEnumerable<WaybillFlag> ServiceLevels
    {
      get { return Cache.Instance.GetObjects<WaybillFlag>(true, f => f.Owner.IsFlagged(WaybillFlagGroupFlags.CostingFlagGroup), f => f.Text, true); }
    }

    public Visibility ServiceLevelsVisible
    {
      get { return Cache.Instance.GetObjects<WaybillFlag>().Any(f => f.Owner.IsFlagged(WaybillFlagGroupFlags.CostingFlagGroup)) ? Visibility.Visible : Visibility.Hidden; }
    }

    public WaybillFlag ServiceLevel
    {
      get
      {
        return Model.WaybillFlags.FirstOrDefault(f1 => f1.Owner.IsFlagged(WaybillFlagGroupFlags.CostingFlagGroup)) ?? new WaybillFlag(true);
      }
      set
      {
        if (value == null) return;
        foreach (var f in Model.WaybillFlags.Where(f1 => f1.Owner.IsFlagged(WaybillFlagGroupFlags.CostingFlagGroup)).ToArray())
        {
          Model.WaybillFlags.Remove(f);
        }
        if (!value.Owner.IsFlagged(WaybillFlagGroupFlags.CostingFlagGroup)) return;
        Model.WaybillFlags.Add(value);
      }
    }

    protected override string GetError(string propertyName)
    {
      if (ServiceLevelsVisible == Visibility.Visible && (string.IsNullOrWhiteSpace(propertyName) || propertyName == "ServiceLevel"))
      {
        var g = Cache.Instance.GetObjects<WaybillFlagGroup>().FirstOrDefault(g1 => g1.IsFlagged(WaybillFlagGroupFlags.CostingFlagGroup));
        if (g != null && g.IsMandatory)
        {
          if (BusinessObject.IsNull(ServiceLevel)) return "Service Level is mandatory.";
        }
      }

      return base.GetError(propertyName);
    }
  }
}


﻿using Afx.Prism;
using Afx.Prism.RibbonMdi;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Documents.Waybill.EditAddressDialog
{
  public class EditAddressDialogViewModel : MdiDialogViewModel<EditAddressDialogContext>
  {
    [InjectionConstructor]
    public EditAddressDialogViewModel(IController controller)
      : base(controller)
    {
    }
  }
}

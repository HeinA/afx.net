﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Documents.Waybill.EditAddressDialog
{
  public enum AddressType
  {
    ShipFrom
    , ShipTo
  }
}

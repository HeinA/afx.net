﻿using Afx.Business;
using Afx.Business.Validation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Documents.Waybill.EditAddressDialog
{
  public class EditAddressDialogContext : BusinessObject
  {
    #region string Name

    public const string NameProperty = "Name";
    string mName;
    [Mandatory("Name is mandatory.")]
    public string Name
    {
      get { return mName; }
      set { SetProperty<string>(ref mName, value); }
    }

    #endregion

    #region string AddressText

    public const string AddressTextProperty = "AddressText";
    string mAddressText;
    [Mandatory("Address is mandatory.")]
    [MinimumStringLength("Address length must be a minimum of 3 characters.")]
    public string AddressText
    {
      get { return mAddressText; }
      set { SetProperty<string>(ref mAddressText, value); }
    }

    #endregion

    #region string Contact

    public const string ContactProperty = "Contact";
    string mContact;
    public string Contact
    {
      get { return mContact; }
      set { SetProperty<string>(ref mContact, value); }
    }

    #endregion

    #region string ContactNumber

    public const string ContactNumberProperty = "ContactNumber";
    string mContactNumber;
    public string ContactNumber
    {
      get { return mContactNumber; }
      set { SetProperty<string>(ref mContactNumber, value); }
    }

    #endregion

    #region string ImportReference

    public const string ImportReferenceProperty = "ImportReference";
    string mImportReference;
    public string ImportReference
    {
      get { return mImportReference; }
      set { SetProperty<string>(ref mImportReference, value); }
    }

    #endregion

    #region string ExportReference

    public const string ExportReferenceProperty = "ExportReference";
    string mExportReference;
    public string ExportReference
    {
      get { return mExportReference; }
      set { SetProperty<string>(ref mExportReference, value); }
    }

    #endregion
  }
}
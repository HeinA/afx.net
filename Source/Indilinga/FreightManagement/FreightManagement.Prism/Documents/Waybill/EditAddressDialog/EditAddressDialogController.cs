﻿using AccountManagement.Business.Service;
using Afx.Business;
using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Business.Service;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using ContactManagement.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace FreightManagement.Prism.Documents.Waybill.EditAddressDialog
{
  public class EditAddressDialogController : MdiDialogController<EditAddressDialogContext, EditAddressDialogViewModel>
  {
    public const string EditAddressDialogControllerKey = "FreightManagement.Prism.Documents.Waybill.AddAddressDialog.EditAddressDialogController";

    public EditAddressDialogController(IController controller)
      : base(EditAddressDialogControllerKey, controller)
    {
    }

    #region AddressType AddressType

    public const string AddressTypeProperty = "AddressType";
    AddressType mAddressType;
    public AddressType AddressType
    {
      get { return mAddressType; }
      set { mAddressType = value; }
    }

    #endregion

    #region WaybillHeaderViewModel WaybillHeaderViewModel

    public const string WaybillHeaderViewModelProperty = "WaybillHeaderViewModel";
    WaybillHeaderViewModel mWaybillHeaderViewModel;
    public WaybillHeaderViewModel WaybillHeaderViewModel
    {
      get { return mWaybillHeaderViewModel; }
      set { mWaybillHeaderViewModel = value; }
    }

    #endregion

    protected override bool OnRun()
    {
      if (DataContext == null) DataContext = new EditAddressDialogContext();
      if (AddressType == EditAddressDialog.AddressType.ShipFrom)
      {
        ViewModel.Caption = "Ship From Address";
      }
      if (AddressType == EditAddressDialog.AddressType.ShipTo)
      {
        ViewModel.Caption = "Ship To Address";
      }

      DataContext.AddressText = Address.AddressText;
      DataContext.Name = Address.Name;
      DataContext.ImportReference = Address.ImportReference;
      DataContext.Contact = Address.Contact;
      DataContext.ContactNumber = Address.ContactNumber;
      DataContext.ExportReference = Address.ExportReference;

      return base.OnRun();
    }

    public override bool Validate()
    {
      return base.Validate();
    }

    #region Address Address

    public const string AddressProperty = "Address";
    Address mAddress;
    public Address Address
    {
      get { return mAddress ?? (mAddress = new Address()); }
      set { mAddress = value; }
    }

    #endregion

    protected override void ApplyChanges()
    {
      try
      {
        Address.AddressText = DataContext.AddressText;
        Address.Name = DataContext.Name;
        Address.ImportReference = DataContext.ImportReference;
        Address.ExportReference = DataContext.ExportReference;
        Address.Contact = DataContext.Contact;
        Address.ContactNumber = DataContext.ContactNumber;

        if (String.IsNullOrEmpty(SecurityContext.ServerName)) { SystemSounds.Exclamation.Play(); return; }

        if (AddressType == EditAddressDialog.AddressType.ShipFrom)
        {
          Address.AddressType = Cache.Instance.GetObjects<ContactManagement.Business.AddressType>(at => at.IsFlagged(FreightManagement.Business.AddressTypeFlags.ShipFromAddress)).FirstOrDefault();

          if (!WaybillHeaderViewModel.ShipFromAddresses.Contains(Address)) WaybillHeaderViewModel.ShipFromAddresses = WaybillHeaderViewModel.ShipFromAddresses.Union(new Address[] { Address }).OrderBy(a1 => a1.AddressTextSingleLine);
          WaybillHeaderViewModel.SelectedShipFromAddress = Address;
        }

        if (AddressType == EditAddressDialog.AddressType.ShipTo)
        {
          Address.AddressType = Cache.Instance.GetObjects<ContactManagement.Business.AddressType>(at => at.IsFlagged(FreightManagement.Business.AddressTypeFlags.ShipToAddress)).FirstOrDefault();

          if (!WaybillHeaderViewModel.ShipToAddresses.Contains(Address)) WaybillHeaderViewModel.ShipToAddresses = WaybillHeaderViewModel.ShipToAddresses.Union(new Address[] { Address }).OrderBy(a1 => a1.AddressTextSingleLine);
          WaybillHeaderViewModel.SelectedShipToAddress = Address;
        }

        using (var svc = ProxyFactory.GetService<IAccountManagementService>(SecurityContext.ServerName))
        {
          svc.EditAccountAddresses(WaybillHeaderViewModel.Model.Account.GlobalIdentifier, Address);
        }
      }
      catch (Exception ex)
      {
        if (ExceptionHelper.HandleException(ex)) throw ex;
      }

      base.ApplyChanges();
    }
  }
}

﻿using Afx.Business.Data;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Documents;
using FreightManagement.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace FreightManagement.Prism.Documents.Waybill
{
  public class WaybillChargesViewModel : ExtensionViewModel<FreightManagement.Business.Waybill>
  {
    #region Constructors

    [InjectionConstructor]
    public WaybillChargesViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    #region string Title

    public string Title
    {
      get { return "Charges"; }
    }

    #endregion

    protected override void OnModelChanged()
    {
      base.OnModelChanged();
      CollectionViewSource cvs = new CollectionViewSource();
      cvs.Source = Model.Charges;
      cvs.Filter += (s, e) =>
      {
        WaybillCharge wc = e.Item as WaybillCharge;
        e.Accepted = !wc.IsDeleted;
      };
      Charges = cvs;
    }

    #region CollectionViewSource Charges

    public const string ChargesProperty = "Charges";
    CollectionViewSource mCharges;
    public CollectionViewSource Charges
    {
      get { return mCharges; }
      private set { SetProperty<CollectionViewSource>(ref mCharges, value); }
    }

    #endregion

    public IEnumerable<WaybillChargeType> ChargeTypes
    {
      get
      {
        var col = Cache.Instance.GetObjects<WaybillChargeType>(ct => ct.Text, true);
        return col;
      }
    }
  }
}

﻿using Afx.Prism;
using Afx.Prism.RibbonMdi;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Documents.Waybill.AttachParcelsDialog
{
  public class AttachParcelsDialogViewModel : MdiDialogViewModel<AttachParcelsDialogContext>
  {
    [InjectionConstructor]
    public AttachParcelsDialogViewModel(IController controller)
      : base(controller)
    {
    }

    [Dependency]
    public WaybillController WaybillController { get; set; }

    #region string Barcode

    public const string BarcodeProperty = "Barcode";
    string mBarcode;
    public string Barcode
    {
      get { return mBarcode; }
      set { SetProperty<string>(ref mBarcode, value); }
    }

    #endregion

    public override void OnEnterPressed()
    {
      base.OnEnterPressed();

      WaybillController.DataContext.Document.Parcels.Add(new Business.WaybillParcel() { Barcode = this.Barcode });
      Barcode = string.Empty;
    }
  }
}

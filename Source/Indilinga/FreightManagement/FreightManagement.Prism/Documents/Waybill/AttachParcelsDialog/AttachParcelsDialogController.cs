﻿using Afx.Prism;
using Afx.Prism.RibbonMdi;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace FreightManagement.Prism.Documents.Waybill.AttachParcelsDialog
{
  public class AttachParcelsDialogController : MdiDialogController<AttachParcelsDialogContext, AttachParcelsDialogViewModel>
  {
    public const string AttachParcelsDialogControllerKey = "FreightManagement.Prism.Documents.Waybill.AttachParcelsDialog.AttachParcelsDialogController";

    public AttachParcelsDialogController(IController controller)
      : base(AttachParcelsDialogControllerKey, controller)
    {
    }

    protected override bool OnRun()
    {
      if (DataContext == null) DataContext = new AttachParcelsDialogContext();
      ViewModel.Caption = "Attach Parcels";
      return base.OnRun();
    }

    protected override void ApplyChanges()
    {
      base.ApplyChanges();
    }
  }
}

﻿using Afx.Prism;
using FreightManagement.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Documents.Waybill
{
  public class WaybillFlagGroupViewModel : ViewModel<WaybillFlagGroup>
  {
    [InjectionConstructor]
    public WaybillFlagGroupViewModel(IController controller)
      : base(controller)
    {
    }

    public new WaybillFlagsViewModel Parent
    {
      get { return (WaybillFlagsViewModel)base.Parent; }
    }

    public IEnumerable<WaybillFlagViewModel> Flags
    {
      get { return this.ResolveViewModels<WaybillFlagViewModel, WaybillFlag>(Model.Flags); }
    }
  }
}

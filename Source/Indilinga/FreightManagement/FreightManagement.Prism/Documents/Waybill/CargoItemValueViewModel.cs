﻿using Afx.Business.Security;
using Afx.Prism;
using FreightManagement.Business;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Documents.Waybill
{
  public class CargoItemValueViewModel : ViewModel<WaybillCargoItem>
  {
    #region Constructors

    [InjectionConstructor]
    public CargoItemValueViewModel(IController controller)
      : base(controller)
    {
    }
    
    #endregion

    public override bool IsReadOnly
    {
      get
      {
        return Model.Owner.IsReadOnly;
      }
    }

    new WaybillItemsViewModel Parent
    {
      get { return (WaybillItemsViewModel)base.Parent; }
    }

    #region DelegateCommand DeleteCommand

    DelegateCommand mDeleteCommand;
    public DelegateCommand DeleteCommand
    {
      get { return mDeleteCommand ?? (mDeleteCommand = new DelegateCommand(ExecuteDelete, CanExecuteDelete)); }
    }

    bool CanExecuteDelete()
    {
      return true;
    }

    void ExecuteDelete()
    {
      //Parent.Model.Items.Remove(Model);
      Model.IsDeleted = true;
      Parent.RefreshItems();
    }

    #endregion
  }
}

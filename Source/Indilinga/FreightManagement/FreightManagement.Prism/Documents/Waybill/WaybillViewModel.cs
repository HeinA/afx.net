﻿using Afx.Business;
using Afx.Business.Activities;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Documents;
using FreightManagement.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace FreightManagement.Prism.Documents.Waybill
{
  public partial class WaybillViewModel : MdiDocumentViewModel<WaybillContext>
  {
    [InjectionConstructor]
    public WaybillViewModel(IController controller)
      : base(controller)
    {
    }
  }
}


﻿using Afx.Prism;
using Afx.Prism.RibbonMdi;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Documents.Waybill.PrintLabelsDialog
{
  public class PrintLabelsDialogViewModel : MdiDialogViewModel<PrintLabelsDialogContext>
  {
    [InjectionConstructor]
    public PrintLabelsDialogViewModel(IController controller)
      : base(controller)
    {
    }

    #region int StartPage

    public const string StartPageProperty = "StartPage";
    int mStartPage = 1;
    public int StartPage
    {
      get { return mStartPage; }
      set { SetProperty<int>(ref mStartPage, value); }
    }

    #endregion

    #region int EndPage

    public const string EndPageProperty = "EndPage";
    int mEndPage = 1;
    public int EndPage
    {
      get { return mEndPage; }
      set { SetProperty<int>(ref mEndPage, value); }
    }

    #endregion

    #region bool AttachLabels

    public const string AttachLabelsProperty = "AttachLabels";
    bool mAttachLabels = false;
    public bool AttachLabels
    {
      get { return mAttachLabels; }
      set { SetProperty<bool>(ref mAttachLabels, value); }
    }

    #endregion
  }
}

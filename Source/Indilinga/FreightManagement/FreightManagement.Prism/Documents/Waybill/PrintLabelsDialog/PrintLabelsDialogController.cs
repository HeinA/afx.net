﻿using Afx.Business;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using FreightManagement.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace FreightManagement.Prism.Documents.Waybill.PrintLabelsDialog
{
  public class PrintLabelsDialogController : MdiDialogController<PrintLabelsDialogContext, PrintLabelsDialogViewModel>
  {
    public const string PrintLabelsDialogControllerKey = "FreightManagement.Prism.Documents.Waybill.PrintLabelsDialog.PrintLabelsDialogController";

    public PrintLabelsDialogController(IController controller)
      : base(PrintLabelsDialogControllerKey, controller)
    {
    }

    [Dependency]
    public WaybillController WaybillController { get; set; }

    protected override bool OnRun()
    {
      if (DataContext == null) DataContext = new PrintLabelsDialogContext();
      ViewModel.Caption = "Print Labels";
      ViewModel.EndPage = WaybillController.DataContext.Document.TotalItems;
      return base.OnRun();
    }

    protected override void ApplyChanges()
    {
      var l = ExtensibilityManager.CompositionContainer.GetExportedValue<IParcelLabel>();
      l.WaybillNumber = WaybillController.DataContext.Document.DocumentNumber;
      l.Date = WaybillController.DataContext.Document.DocumentDate;
      l.Consignee = WaybillController.DataContext.Document.Consignee;
      l.Consigner = WaybillController.DataContext.Document.Consigner;
      l.StartIndex = ViewModel.StartPage;
      l.EndIndex = ViewModel.EndPage;
      l.TotalLabels = WaybillController.DataContext.Document.TotalItems;
      l.DeliveryCity = WaybillController.DataContext.Document.DeliveryCity.Name;
      l.OrganizationalUnit = WaybillController.DataContext.Document.OrganizationalUnit.Name;
      l.Print(Setting.GetSetting<LabelPrinterSetting>().PrinterName);

      if (ViewModel.AttachLabels)
      {
        foreach (var p in WaybillController.DataContext.Document.Parcels.ToArray())
        {
          WaybillController.DataContext.Document.Parcels.Remove(p);
          p.IsDeleted = true;
          WaybillController.DataContext.Document.Parcels.Add(p);
        }
        for (int i = 1; i <= WaybillController.DataContext.Document.TotalItems; i++)
        {
          WaybillController.DataContext.Document.Parcels.Add(new WaybillParcel() { Barcode = string.Format("{0}{1:000}{2:000}", WaybillController.DataContext.Document.DocumentNumber, WaybillController.DataContext.Document.TotalItems, i) });
        }
      }

      base.ApplyChanges();
    }
  }
}

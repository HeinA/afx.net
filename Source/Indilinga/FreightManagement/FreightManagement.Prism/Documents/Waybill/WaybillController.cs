﻿using Afx.Business;
using Afx.Business.Data;
using Afx.Business.Documents;
using Afx.Business.Security;
using Afx.Business.Service;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using Afx.Prism.RibbonMdi.Documents;
using Afx.Prism.RibbonMdi.Events;
using DocumentManagement.Business;
using DocumentManagement.Business.Service;
using DocumentManagement.Prism;
using DocumentManagement.Prism.Extensions.DocumentRepository;
using FreightManagement.Business;
using FreightManagement.Business.Service;
using FreightManagement.Prism.Documents.Waybill.PrintProFormaInvoiceDialog;
using FreightManagement.Prism.Documents.Waybill.ReceiveWaybillDialog;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace FreightManagement.Prism.Documents.Waybill
{
  public partial class WaybillController : MdiDocumentController<WaybillContext, WaybillViewModel>, IDocumentRepositoryAware
  {
    [InjectionConstructor]
    public WaybillController(IController controller)
      : base(controller)
    {
    }

    protected override void OnRunning()
    {
      Cache.Instance.CacheUpdated += Instance_CacheUpdated;
      base.OnRunning();
    }

    protected override void OnTerminated()
    {
      Cache.Instance.CacheUpdated -= Instance_CacheUpdated;
      base.OnTerminated();
    }

    void Instance_CacheUpdated(object sender, EventArgs e)
    {
      SetRatesConfigurations();
    }

    void SetRatesConfigurations()
    {
      RatesConfigurationReference rc = Cache.Instance.GetObjects<RatesConfigurationReference>(rcr1 => rcr1.IsStandardConfiguration).FirstOrDefault();
      if (rc == null) throw new RatesConfigurationException("No standard Rates have been configured");
      StandardRatesConfiguration = rc;

      if (DataContext.Document.RatesConfiguration != null) DataContext.Document.RatesConfiguration = Cache.Instance.GetObject<RatesConfigurationReference>(DataContext.Document.RatesConfiguration.GlobalIdentifier);
    }

    protected override void OnLoaded()
    {
      base.OnLoaded();
    }

    public override string AdditionalStateText
    {
      get
      {
        if (DataContext == null) return null;
        if (DataContext.Document == null) return null;
        if (DataContext.Document.CurrentBay == null) return null;

        string flags = string.Join(", ", DataContext.Document.WaybillFlags.Select(wf => wf.Text));
        if (string.IsNullOrWhiteSpace(flags)) return DataContext.Document.CurrentBay.Text;

        return string.Format("{0} ({1})", DataContext.Document.CurrentBay.Text, flags);
      }
    }

    #region RatesConfigurationReference StandardRatesConfiguration

    RatesConfigurationReference mStandardRatesConfiguration;
    public RatesConfigurationReference StandardRatesConfiguration
    {
      get
      {
        if (mStandardRatesConfiguration == null)
        {
          SetRatesConfigurations();
        }
        return mStandardRatesConfiguration;
      }
      set
      {
        mStandardRatesConfiguration = value;

        if (DataContext.Document.RatesConfiguration == null)
        {
          if (StandardRatesConfiguration == null) throw new RatesConfigurationException("No standard Rates have been configured");
          var rcr = StandardRatesConfiguration.GetRevision(DataContext.Document.DocumentDate);
          if (rcr == null) throw new RatesConfigurationException("The Standard Rates Configuration does not have a revision for this date.");
          DataContext.Document.VolumetricFactor = rcr.VolumetricFactor;
        }
      }
    }

    #endregion

    protected override void LoadDataContext()
    {
      base.LoadDataContext();
      using (new EventStateSuppressor(DataContext.Document))
      {
        SetRatesConfigurations();
      }
    }

    #region DocumentRepositoryExtensionViewModel DocumentRepositoryExtensionViewModel

    public const string DocumentRepositoryExtensionViewModelProperty = "DocumentRepositoryExtensionViewModel";
    DocumentRepositoryExtensionViewModel mDocumentRepositoryExtensionViewModel;
    public DocumentRepositoryExtensionViewModel DocumentRepositoryExtensionViewModel
    {
      get { return mDocumentRepositoryExtensionViewModel; }
      set { mDocumentRepositoryExtensionViewModel = value; }
    }

    #endregion

    public Document Document { get { return DataContext.Document; } }

    #region WaybillItemsViewModel WaybillItemsViewModel

    public const string WaybillItemsViewModelProperty = "WaybillItemsViewModel";
    WaybillItemsViewModel mWaybillItemsViewModel;
    public WaybillItemsViewModel WaybillItemsViewModel
    {
      get { return mWaybillItemsViewModel; }
      set { mWaybillItemsViewModel = value; }
    }

    #endregion

    protected override void ExtendDocument()
    {
      ExtendDocument<WaybillHeaderViewModel>(HeaderRegion);
      ExtendDocument<DocumentReferencesViewModel>(TabRegion);

      WaybillItemsViewModel = ExtendDocument<WaybillItemsViewModel>(DockRegion);
      ExtendDocument<DocumentNotesViewModel>(TabRegion);
      ExtendDocument<WaybillChargesViewModel>(TabRegion);
      ExtendDocument<WaybillRoutesViewModel>(TabRegion);
      ExtendDocument<WaybillPODViewModel>(TabRegion);
      ExtendDocument<WaybillTraceViewModel>(TabRegion);
      ExtendDocument<WaybillParcelViewModel>(TabRegion);
      ExtendDocument<WaybillBayMovementViewModel>(TabRegion);
      ExtendDocument<WaybillFlagsViewModel>(TabRegion);
      ExtendDocument<WaybillFooterViewModel>(FooterRegion);

      DocumentRepositoryExtensionViewModel = ExtendDocument<DocumentRepositoryExtension, DocumentRepositoryExtensionViewModel>(TabRegion);

      base.ExtendDocument();
    }

    protected override void ExecuteOperation(Operation op, BusinessObject argument)
    {
      switch (op.Identifier)
      {
        case Operations.AcceptQuote:
          SetDocumentState(Business.Waybill.States.QuoteAccepted);
          break;

        case Operations.MissedCollection:
          SetDocumentState(Business.Waybill.States.Quoted);
          break;

        case Operations.Collected:
          SetDocumentState(Business.Waybill.States.Collected);
          break;

        case Operations.Cost_Route:
          using (new WaitCursor())
          {
            if (DataContext.Document.CollectionLocation.Name == null) { System.Media.SystemSounds.Beep.Play(); return; }
            if (DataContext.Document.DeliveryLocation.Name == null) { System.Media.SystemSounds.Beep.Play(); return; }
            if (DataContext.Document.Items.Count == 0) { System.Media.SystemSounds.Beep.Play(); return; }

            try
            {
              using(var svc = ServiceFactory.GetService<IFreightManagementService>(SecurityContext.ServerName))
              {
                DataContext.Document = svc.Instance.CostWaybill(DataContext.Document);
              }
              //using (new RatesConfigurationCache())
              //{
              //  CostingHelper.Cost(DataContext.Document);
              //}
            }
            catch (Exception ex)
            {
              if (ExceptionHelper.HandleException(ex)) throw ex;
            }

            try
            {
              if (!DataContext.Document.IsPointToPoint) RoutingHelper.Route(DataContext.Document);
            }
            catch (Exception ex)
            {
              if (ExceptionHelper.HandleException(ex)) throw ex;
            }

            MdiApplicationController.Current.EventAggregator.GetEvent<DisplayOutputEvent>().Publish(new DisplayOutputEventArgs(DataContext.Document.ChargeDescription, true));
          }
          break;

        case Operations.Verify_Receive:
          ReceiveWaybillDialogController c = CreateChildController<ReceiveWaybillDialogController>();
          c.Run();
          break;

        case Operations.Deliver:
          SetDocumentState(Business.Waybill.States.Delivered);
          break;

        case Operations.PODImaged:
          SetDocumentState(Business.Waybill.States.PODCaptured);
          break;

        case Operations.ReadyforInvoicing:
          SetDocumentState(Business.Waybill.States.PendingInvoice);
          break;

        case Operations.Invoiced:
          SetDocumentState(Business.Waybill.States.Invoiced);
          break;

        case Operations.Finalize:
          if (MessageBox.Show("Are you sure you want to complete the waybill", "Complete", MessageBoxButton.YesNo, MessageBoxImage.Exclamation) == MessageBoxResult.Yes)
          {
            SetDocumentState(Business.Waybill.States.Complete);
          }
          break;


        case Operations.Cancel:
          if (MessageBox.Show("Are you sure you want to cancel the waybill", "Cancel", MessageBoxButton.YesNo, MessageBoxImage.Exclamation) == MessageBoxResult.Yes)
          {
            SetDocumentState(Business.Waybill.States.Cancelled);
          }
          break;

        case Operations.CaptureReferences:
          DocumentManagement.Prism.Dialogs.CaptureReferencesDialog.CaptureReferencesDialogController crd = CreateChildController<DocumentManagement.Prism.Dialogs.CaptureReferencesDialog.CaptureReferencesDialogController>();
          crd.Document = this.DataContext.Document;
          crd.Run();
          break;

        case Operations.PrintLabels:
          PrintLabelsDialog.PrintLabelsDialogController plc = CreateChildController<PrintLabelsDialog.PrintLabelsDialogController>();
          plc.Run();
          break;

        case Operations.AttachParcel:
          AttachParcelsDialog.AttachParcelsDialogController apc = CreateChildController<AttachParcelsDialog.AttachParcelsDialogController>();
          apc.Run();
          break;

        case Operations.PrintText:
          RawPrinterHelper.SendStringToPrinter(Setting.GetSetting<TextPrinterSetting>().PrinterName, new TextWaybill(DataContext.Document).ToString());
          break;

        case Operations.SetBay:
          SetBayDialog.SetBayDialogController sbc = CreateChildController<SetBayDialog.SetBayDialogController>();
          sbc.Run();
          break;

        case Operations.AttachFile:
          AttachFileHelper.AttachFile(this);
          break;

        case Operations.PrintProFormaInvoice:
          var c1 = CreateChildController<PrintProFormaInvoiceDialogController>();
          c1.Run();
          break;

        default:
          base.ExecuteOperation(op, argument);
          break;
      }
    }

    public void PrintProForma(decimal vatRate)
    {
      var filename = PrintCrystal(Operations.PrintProFormaInvoice, vatRate);

      ScannedDocument sd = new ScannedDocument();
      sd.Description = string.Format("Pro Forma Invoice {0:dd/MM/yyyy}", DateTime.Now);
      sd.User = Cache.Instance.GetObject<UserReference>(SecurityContext.User.GlobalIdentifier);
      sd.FileExtension = filename.Substring(filename.LastIndexOf('.'));
      sd.ScanType = DataContext.Document.DocumentType.ScanTypes.FirstOrDefault(st => st.Text.ToUpperInvariant().Replace(" ", string.Empty).StartsWith("PROFORMA"));
      
      using (var svc = ProxyFactory.GetService<IDocumentManagementService>(SecurityContext.ServerName))
      {
        byte[] bytes = File.ReadAllBytes(filename);
        svc.FileDocument(sd.GlobalIdentifier, bytes);
      }

      AddScannedDocument(sd);
      Save(false);
    }

    public void AddScannedDocument(ScannedDocument document)
    {
      DocumentRepositoryExtension dre = DataContext.Document.GetExtensionObject<DocumentRepositoryExtension>();
      if (dre != null)
      {
        dre.ScannedDocuments.Add(document);
        RegionManager.Regions[TabRegion].Activate(DocumentRepositoryExtensionViewModel);
      }
    }

    public void SaveWaybill()
    {
      Save();
    }
  }
}


﻿using Afx.Business;
using Afx.Business.Data;
using Afx.Business.Documents;
using Afx.Business.Security;
using Afx.Business.Service;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Tools;
using Geographics.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Documents.Waybill
{
  [Export(typeof(SearchItemViewModel))]
  public partial class WaybillSearchViewModel : SearchItemViewModel
  {
    #region Constructors

    [InjectionConstructor]
    public WaybillSearchViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    #region DocumentType DocumentType

    public override DocumentType DocumentType
    {
      get { return Cache.Instance.GetObject<DocumentType>(FreightManagement.Business.Waybill.DocumentTypeIdentifier); }
    }

    #endregion

    #region string DocumentNumber

    string mDocumentNumber;
    public string DocumentNumber
    {
      get { return mDocumentNumber; }
      set { SetProperty<string>(ref mDocumentNumber, value); }
    }

    #endregion

    #region DocumentTypeState DocumentTypeState

    public IEnumerable<DocumentTypeState> DocumentTypeStates
    {
      get { return new List<DocumentTypeState>(new DocumentTypeState[] { new DocumentTypeState(true) }).Union(Cache.Instance.GetObject<DocumentType>(FreightManagement.Business.Waybill.DocumentTypeIdentifier).HeirarchyStates.OrderBy(dts => dts.Name)); }
    }

    DocumentTypeState mDocumentTypeState = new DocumentTypeState(true);
    public DocumentTypeState DocumentTypeState
    {
      get { return mDocumentTypeState; }
      set { SetProperty<DocumentTypeState>(ref mDocumentTypeState, value); }
    }

    #endregion

    #region bool UseSingleDate

    bool mUseSingleDate = true;
    public bool UseSingleDate
    {
      get { return mUseSingleDate; }
      set { SetProperty<bool>(ref mUseSingleDate, value); }
    }

    #endregion

    #region bool UseDateRange

    bool mUseDateRange = false;
    public bool UseDateRange
    {
      get { return mUseDateRange; }
      set { SetProperty<bool>(ref mUseDateRange, value); }
    }

    #endregion

    #region Nullable<DateTime> Date1

    Nullable<DateTime> mDate1 = null;
    public Nullable<DateTime> Date1
    {
      get { return mDate1; }
      set { SetProperty<Nullable<DateTime>>(ref mDate1, value); }
    }

    #endregion

    #region Nullable<DateTime> Date2

    Nullable<DateTime> mDate2;
    public Nullable<DateTime> Date2
    {
      get { return mDate2; }
      set { SetProperty<Nullable<DateTime>>(ref mDate2, value); }
    }

    #endregion

    #region OrganizationalUnit OrganizationalUnit

    public IEnumerable<OrganizationalUnit> OrganizationalUnits
    {
      get { return Cache.Instance.GetObjects<OrganizationalUnit>(true, ou => ou.UserAssignable, ou => ou.Name, true); }
    }

    OrganizationalUnit mOrganizationalUnit = new OrganizationalUnit(true);
    public OrganizationalUnit OrganizationalUnit
    {
      get { return mOrganizationalUnit; }
      set { SetProperty<OrganizationalUnit>(ref mOrganizationalUnit, value); }
    }

    #endregion


    #region string AccountingOrderNumber

    public const string AccountingOrderNumberProperty = "AccountingOrderNumber";
    string mAccountingOrderNumber;
    public string AccountingOrderNumber
    {
      get { return mAccountingOrderNumber; }
      set { SetProperty<string>(ref mAccountingOrderNumber, value); }
    }

    #endregion

    #region string InvoiceNumber
    
    public const string InvoiceNumberProperty = "InvoiceNumber";
    string mInvoiceNumber;
    public string InvoiceNumber
    {
      get { return mInvoiceNumber; }
      set { SetProperty<string>(ref mInvoiceNumber, value); }
    }
    
    #endregion

    #region string ClientReferenceNumber
    
    public const string ClientReferenceNumberProperty = "ClientReferenceNumber";
    string mClientReferenceNumber;
    public string ClientReferenceNumber
    {
      get { return mClientReferenceNumber; }
      set { SetProperty<string>(ref mClientReferenceNumber, value); }
    }
    
    #endregion

    #region string Consigner
    
    public const string ConsignerProperty = "Consigner";
    string mConsigner;
    public string Consigner
    {
      get { return mConsigner; }
      set { SetProperty<string>(ref mConsigner, value); }
    }
    
    #endregion

    #region string Consignee
    
    public const string ConsigneeProperty = "Consignee";
    string mConsignee;
    public string Consignee
    {
      get { return mConsignee; }
      set { SetProperty<string>(ref mConsignee, value); }
    }
    
    #endregion

    #region string DocumentReference

    public const string DocumentReferenceProperty = "DocumentReference";
    string mDocumentReference;
    public string DocumentReference
    {
      get { return mDocumentReference; }
      set { mDocumentReference = value; }
    }

    #endregion

    #region SearchResults ExecuteSearch()

    protected override SearchResults ExecuteSearch()
    {
      using (var svc = ServiceFactory.GetService<IAfxService>(SecurityContext.ServerName))
      {
        SearchDatagraph sg = new SearchDatagraph(typeof(FreightManagement.Business.Waybill))
          .Select(Document.DocumentDateProperty, "Date", "dd MMM yyyy")
          .Select(Document.DocumentNumberProperty, "Document")
          .Select(FreightManagement.Business.Waybill.ConsignerProperty, "Consigner")
          .Select(FreightManagement.Business.Waybill.ConsigneeProperty, "Consignee")
          .Join(FreightManagement.Business.Waybill.CollectionCityProperty)
              .Select(Geographics.Business.Location.NameProperty, "Collection City")
              //.Where(Geographics.Business.Location.IdProperty, FilterType.Equals, DocumentTypeState.Name, !string.IsNullOrWhiteSpace(DocumentTypeState.Name))
              .EndJoin()
          .Join(FreightManagement.Business.Waybill.DeliveryCityProperty)
              .Select(Geographics.Business.Location.NameProperty, "Delivery City")
              //.Where(Geographics.Business.Location.IdProperty, FilterType.Equals, DocumentTypeState.Name, !string.IsNullOrWhiteSpace(DocumentTypeState.Name))
              .EndJoin()
          .Select(FreightManagement.Business.Waybill.AccountingOrderNumberProperty, "Accounting Order Number")
          .Select(FreightManagement.Business.Waybill.InvoiceNumberProperty, "Invoice Number")
          .Select(FreightManagement.Business.Waybill.ClientReferenceNumberProperty, "Client Reference Number")
          .Join(Document.StateProperty)
            .Join(DocumentState.DocumentTypeStateProperty)
              .Select(DocumentTypeState.NameProperty, "State")
              .Where(DocumentTypeState.NameProperty, FilterType.Equals, DocumentTypeState.Name, !string.IsNullOrWhiteSpace(DocumentTypeState.Name))
              .EndJoin()
            .EndJoin()
          .Join(Document.OrganizationalUnitProperty)
            .Select(OrganizationalUnit.NameProperty, "Organizational Unit")
            .Where(OrganizationalUnit.NameProperty, FilterType.Equals, OrganizationalUnit.Name, !string.IsNullOrWhiteSpace(OrganizationalUnit.Name))
            .EndJoin()
          .Join(Document.ReferencesProperty)
            .Where(Afx.Business.Documents.DocumentReference.ReferenceProperty, FilterType.Like, string.Format("%{0}%", DocumentReference), !string.IsNullOrWhiteSpace(DocumentReference))
            .EndJoin()
          .Where(Document.DocumentNumberProperty, FilterType.Like, string.Format("%{0}%", DocumentNumber), !string.IsNullOrWhiteSpace(DocumentNumber))
          .Where(FreightManagement.Business.Waybill.AccountingOrderNumberProperty, FilterType.Like, string.Format("%{0}%", AccountingOrderNumber), !string.IsNullOrWhiteSpace(AccountingOrderNumber))
          .Where(FreightManagement.Business.Waybill.InvoiceNumberProperty, FilterType.Like, string.Format("%{0}%", InvoiceNumber), !string.IsNullOrWhiteSpace(InvoiceNumber))
          .Where(FreightManagement.Business.Waybill.ClientReferenceNumberProperty, FilterType.Like, string.Format("%{0}%", ClientReferenceNumber), !string.IsNullOrWhiteSpace(ClientReferenceNumber))
          .Where(FreightManagement.Business.Waybill.ConsignerProperty, FilterType.Like, string.Format("%{0}%", Consigner), !string.IsNullOrWhiteSpace(Consigner))
          .Where(FreightManagement.Business.Waybill.ConsigneeProperty, FilterType.Like, string.Format("%{0}%", Consignee), !string.IsNullOrWhiteSpace(Consignee))
          .WhereBetween(Document.DocumentDateProperty, Date1, Date2, Date1 != null && UseDateRange, Date2 != null && UseDateRange)
          .Where(Document.DocumentDateProperty, FilterType.Equals, Date1, Date1 != null && UseSingleDate);

        return svc.Instance.Search(sg);
      }
    }

    #endregion
  }
}

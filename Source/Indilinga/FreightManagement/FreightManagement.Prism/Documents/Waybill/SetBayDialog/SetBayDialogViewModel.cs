﻿using Afx.Business.Data;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using FreightManagement.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Documents.Waybill.SetBayDialog
{
  public class SetBayDialogViewModel : MdiDialogViewModel<SetBayDialogContext>
  {
    [InjectionConstructor]
    public SetBayDialogViewModel(IController controller)
      : base(controller)
    {
    }

    #region Bay Bay

    public const string BayProperty = "Bay";
    Bay mBay;
    public Bay Bay
    {
      get { return mBay; }
      set { SetProperty<Bay>(ref mBay, value); }
    }

    #endregion

    public IEnumerable<Bay> Bays
    {
      get { return Cache.Instance.GetObjects<Bay>(true, b => b.Text, true); }
    }

    #region string Comment

    public const string CommentProperty = "Comment";
    string mComment;
    public string Comment
    {
      get { return mComment; }
      set { SetProperty<string>(ref mComment, value); }
    }

    #endregion
  }
}

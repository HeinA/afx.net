﻿using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using Afx.Prism.RibbonMdi.Documents;
using FreightManagement.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace FreightManagement.Prism.Documents.Waybill.SetBayDialog
{
  public class SetBayDialogController : MdiDialogController<SetBayDialogContext, SetBayDialogViewModel>
  {
    public const string SetBayDialogControllerKey = "FreightManagement.Prism.Documents.Waybill.SetBayDialog.SetBayDialogController";

    public SetBayDialogController(IController controller)
      : base(SetBayDialogControllerKey, controller)
    {
    }

    [Dependency]
    public WaybillController WaybillController { get; set; }

    protected override bool OnRun()
    {
      if (DataContext == null) DataContext = new SetBayDialogContext();
      ViewModel.Caption = "Set Bay";
      return base.OnRun();
    }

    protected override void ApplyChanges()
    {
      WaybillController.DataContext.Document.SetBay(ViewModel.Bay, ViewModel.Comment);
      WaybillController.EventAggregator.GetEvent<UpdateDocumentInfoEvent>().Publish(EventArgs.Empty);
      WaybillController.SaveWaybill();
      base.ApplyChanges();
    }
  }
}

﻿using Afx.Prism;
using Afx.Prism.RibbonMdi;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Documents.Waybill.AddAddressDialog
{
  public class AddAddressDialogViewModel : MdiDialogViewModel<AddAddressDialogContext>
  {
    [InjectionConstructor]
    public AddAddressDialogViewModel(IController controller)
      : base(controller)
    {
    }
  }
}

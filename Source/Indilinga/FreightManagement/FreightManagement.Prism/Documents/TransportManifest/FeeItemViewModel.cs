﻿using Afx.Business.Security;
using Afx.Prism;
using FreightManagement.Business;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Documents.TransportManifest
{
  public class FeeItemViewModel : ViewModel<TransportManifestFee>
  {
    #region Constructors

    [InjectionConstructor]
    public FeeItemViewModel(IController controller)
      : base(controller)
    {
    }
    
    #endregion

    public override bool IsReadOnly
    {
      get
      {
        return Model.Owner.IsReadOnly;
      }
    }

    #region bool IsDeleted

    public const string IsDeletedProperty = "IsDeleted";
    bool mIsDeleted;
    public bool IsDeleted
    {
      get { return mIsDeleted; }
      set { mIsDeleted = value; }
    }

    #endregion

    new TransportManifestFeesViewModel Parent
    {
      get { return (TransportManifestFeesViewModel)base.Parent; }
    }

    #region DelegateCommand DeleteCommand

    DelegateCommand mDeleteCommand;
    public DelegateCommand DeleteCommand
    {
      get { return mDeleteCommand ?? (mDeleteCommand = new DelegateCommand(ExecuteDelete, CanExecuteDelete)); }
    }

    bool CanExecuteDelete()
    {
      return true;
    }

    void ExecuteDelete()
    {
      //Parent.Model.Items.Remove(Model);
      Model.IsDeleted = true;
      Parent.RefreshFees();
    }

    #endregion

  }
}

﻿using Afx.Business.Security;
using Afx.Business.Service;
using Afx.Prism.RibbonMdi.Documents;
using FreightManagement.Business;
using FreightManagement.Business.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Documents.TransportManifest
{
  public partial class TransportManifestContext : DocumentContext<FreightManagement.Business.TransportManifest>
  {
    public TransportManifestContext()
      : base(FreightManagement.Business.TransportManifest.DocumentTypeIdentifier)
    {
    }

    public TransportManifestContext(Guid documentIdentifier)
      : base(FreightManagement.Business.TransportManifest.DocumentTypeIdentifier, documentIdentifier)
    {
    }

    public override void SaveData()
    {
      using (var svc = ProxyFactory.GetService<IFreightManagementService>(SecurityContext.ServerName))
      {
        Document = svc.SaveTransportManifest(Document);
      }
      base.SaveData();
    }

    public override void LoadData()
    {
      using (var svc = ProxyFactory.GetService<IFreightManagementService>(SecurityContext.ServerName))
      {
        Document = svc.LoadTransportManifest(DocumentIdentifier);
      }
      base.LoadData();
    }
  }
}


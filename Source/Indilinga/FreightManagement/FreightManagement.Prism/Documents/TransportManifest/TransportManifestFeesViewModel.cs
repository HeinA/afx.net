﻿using Afx.Business.Data;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using Afx.Prism.RibbonMdi.Documents;
using FreightManagement.Business;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace FreightManagement.Prism.Documents.TransportManifest
{
  public class TransportManifestFeesViewModel : ExtensionViewModel<FreightManagement.Business.TransportManifest>
  {
    #region Constructors

    [InjectionConstructor]
    public TransportManifestFeesViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    [Dependency]
    public TransportManifestController TransportManifestController { get; set; }

    public string Title
    {
      get { return "Fees & Expenses"; }
    }

    public IEnumerable<FeeType> FeeTypesCollection
    {
      get { return Cache.Instance.GetObjects<FeeType>(true, st => st.Text, true); }
    }
    

    public IEnumerable<ViewModel> FeeItems
    {
      get
      {
        Collection<ViewModel> col = new Collection<ViewModel>();
        foreach (var i in Model.Fees)
        {
          if (i.IsDeleted) continue;

          ViewModel vm = null;
          vm = TransportManifestController.GetCreateViewModel<FeeItemViewModel>(i, this);

          if (vm != null) col.Add(vm);
        }
        return col;
      }
    }

    protected override void OnModelChanged()
    {
      WeakEventManager<INotifyCollectionChanged, NotifyCollectionChangedEventArgs>.AddHandler(Model.Fees, "CollectionChanged", Fees_CollectionChanged);

      base.OnModelChanged();
    }

    void Fees_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
    {
      OnPropertyChanged("FeeItems");
    }

    public void RefreshFees()
    {
      OnPropertyChanged("FeeItems");
    }


    #region DelegateCommand AddFeeCommand

    DelegateCommand mAddFeeCommand;
    public DelegateCommand AddFeeCommand
    {
      get { return mAddFeeCommand ?? (mAddFeeCommand = new DelegateCommand(ExecuteAddFee, CanExecuteAddFee)); }
    }

    bool CanExecuteAddFee()
    {
      return true;
    }

    void ExecuteAddFee()
    {
      TransportManifestController.DataContext.Document.Fees.Add(new TransportManifestFee());
    }

    #endregion


  }
}

﻿using AccountManagement.Business;
using Afx.Business;
using Afx.Business.Security;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Documents;
using ContactManagement.Business;
using FleetManagement.Business;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Media;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace FreightManagement.Prism.Documents.TransportManifest
{
  public class TransportManifestBookingTextViewModel : ExtensionViewModel<FreightManagement.Business.TransportManifest>
  {
    #region Constructors

    [InjectionConstructor]
    public TransportManifestBookingTextViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    [Dependency]
    public TransportManifestController TransportManifestController { get; set; }

    public string Title
    {
      get { return "Booking Text"; }
    }

    #region string ExportBooking

    public const string ExportBookingProperty = "ExportBooking";
    string mExportBooking;
    public string ExportBooking
    {
      get { return mExportBooking; }
      set { SetProperty<string>(ref mExportBooking, value); }
    }

    #endregion

    #region DelegateCommand CopyToClipboardCommand

    DelegateCommand mCopyToClipboardCommand;
    public DelegateCommand CopyToClipboardCommand
    {
      get { return mCopyToClipboardCommand ?? (mCopyToClipboardCommand = new DelegateCommand(ExecuteCopyToClipboard, CanExecuteCopyToClipboard)); }
    }

    bool CanExecuteCopyToClipboard()
    {
      return true;
    }

    void ExecuteCopyToClipboard()
    {

      IDataObject obj = Clipboard.GetDataObject();
      foreach (var o in obj.GetFormats().ToList())
      {
        object oo = obj.GetData("Excel");
        Console.WriteLine(o);
      }



      string exportBookingXml = @"<?xml version=""1.0""?>
      <?mso-application progid=""Excel.Sheet""?>
      <Workbook xmlns=""urn:schemas-microsoft-com:office:spreadsheet""
       xmlns:o=""urn:schemas-microsoft-com:office:office""
       xmlns:x=""urn:schemas-microsoft-com:office:excel""
       xmlns:ss=""urn:schemas-microsoft-com:office:spreadsheet""
       xmlns:html=""http://www.w3.org/TR/REC-html40"">
       <Styles>
        <Style ss:ID=""Default"" ss:Name=""Normal"">
         <Alignment ss:Vertical=""Bottom""/>
         <Borders/>
         <Font ss:FontName=""Calibri"" x:Family=""Swiss"" ss:Size=""11"" ss:Color=""#000000""/>
         <Interior/>
         <NumberFormat/>
         <Protection/>
        </Style>
        <Style ss:ID=""s63"">
         <NumberFormat ss:Format=""@""/>
        </Style>
       </Styles>
       <Worksheet ss:Name=""Booking"">
        <Table ss:ExpandedColumnCount=""2"" ss:ExpandedRowCount=""2""
         ss:DefaultRowHeight=""15"">

         <Row>
          <Cell><Data ss:Type=""String"">Horse Nr:</Data></Cell>
          <Cell><Data ss:Type=""String"">{0}</Data></Cell>
         </Row>
         <Row>
          <Cell><Data ss:Type=""String"">Account:</Data></Cell>
          <Cell><Data ss:Type=""String"">{1}</Data></Cell>
         </Row>
         <Row>
          <Cell><Data ss:Type=""String"">Loading:</Data></Cell>
          <Cell><Data ss:Type=""String"">{2}</Data></Cell>
         </Row>
         <Row>
          <Cell><Data ss:Type=""String"">Loading Date:</Data></Cell>
          <Cell><Data ss:Type=""String"">{3}</Data></Cell>
         </Row>
         <Row>
          <Cell><Data ss:Type=""String"">Off-Loading:</Data></Cell>
          <Cell><Data ss:Type=""String"">{4}</Data></Cell>
         </Row>
         <Row>
          <Cell><Data ss:Type=""String"">Off-Loading Date:</Data></Cell>
          <Cell><Data ss:Type=""String"">{5}</Data></Cell>
         </Row>
         <Row>
          <Cell><Data ss:Type=""String"">Transporter:</Data></Cell>
          <Cell><Data ss:Type=""String"">{6}</Data></Cell>
         </Row>
         <Row>
          <Cell><Data ss:Type=""String"">Horse:</Data></Cell>
          <Cell><Data ss:Type=""String"">{7}</Data></Cell>
         </Row>
         <Row>
          <Cell><Data ss:Type=""String"">Trailer 1:</Data></Cell>
          <Cell><Data ss:Type=""String"">{8} - {9}</Data></Cell>
         </Row>
         <Row>
          <Cell><Data ss:Type=""String"">Trailer 2:</Data></Cell>
          <Cell><Data ss:Type=""String"">{10} - {11}</Data></Cell>
         </Row>
         <Row>
          <Cell><Data ss:Type=""String"">Driver:</Data></Cell>
          <Cell><Data ss:Type=""String"">{12}</Data></Cell>
         </Row>
         <Row>
          <Cell><Data ss:Type=""String"">Initials, Surname:</Data></Cell>
          <Cell><Data ss:Type=""String"">{13}, {14}</Data></Cell>
         </Row>
         <Row>
          <Cell><Data ss:Type=""String"">Passport:</Data></Cell>
          <Cell><Data ss:Type=""String"">{15}</Data></Cell>
         </Row>
         <Row>
          <Cell><Data ss:Type=""String"">Exp Date:</Data></Cell>
          <Cell><Data ss:Type=""String"">{16}</Data></Cell>
         </Row>
         <Row>
          <Cell><Data ss:Type=""String"">ID:</Data></Cell>
          <Cell><Data ss:Type=""String"">{17}</Data></Cell>
         </Row>
         <Row>
          <Cell><Data ss:Type=""String"">Nam Cell:</Data></Cell>
          <Cell><Data ss:Type=""String"">{18}</Data></Cell>
         </Row>
         <Row>
          <Cell><Data ss:Type=""String"">Foreign Cell:</Data></Cell>
          <Cell><Data ss:Type=""String"">{19}</Data></Cell>
         </Row>
         <Row>
          <Cell><Data ss:Type=""String"">Border Post:</Data></Cell>
          <Cell><Data ss:Type=""String"">{20}</Data></Cell>
         </Row>
         <Row>
          <Cell><Data ss:Type=""String"">Rate:</Data></Cell>
          <Cell><Data ss:Type=""Number"">{21}</Data></Cell>
         </Row>
         <Row>
          <Cell><Data ss:Type=""String"">Invoice To:</Data></Cell>
          <Cell><Data ss:Type=""String"">{22}</Data></Cell>
         </Row>
         <Row>
          <Cell><Data ss:Type=""String"">Comments:</Data></Cell>
          <Cell><Data ss:Type=""String"">{23}</Data></Cell>
         </Row>

        </Table>
       </Worksheet>
      </Workbook>";

      string exportBooking;
      exportBooking = "Horse No:\t{0}\r\n";
      exportBooking += "Client:\t{1}\r\n";
      exportBooking += "Loading:\t{2}\r\n";
      exportBooking += "Loading Date:\t{3}\r\n";
      exportBooking += "Off-Loading:\t{4}\r\n";
      exportBooking += "Off-Loading Date:\t{5}\r\n";
      exportBooking += "Transporter:\t{6}\r\n";
      exportBooking += "Horse:\t{7}\r\n";
      exportBooking += "Trailer 1:\t{8} - {9}\r\n";
      exportBooking += "Trailer 2:\t{10} - {11}\r\n";
      exportBooking += "Driver:\t{12}\r\n";
      exportBooking += "Initials, Surname:\t{13}, {14}\r\n";
      exportBooking += "Passport:\t{15}\r\n";
      exportBooking += "Exp Date:\t{16}\r\n";
      exportBooking += "ID:\t{17}\r\n";
      exportBooking += "Nam Cell:\t{18}\r\n";
      exportBooking += "RSA Cell:\t{19}\r\n";
      exportBooking += "Rate:\t{20}\r\n";
      exportBooking += "Border Post:\t{21}\r\n";
      exportBooking += "Invoice To:\t{22}\r\n";
      exportBooking += "Comments:\t{23}";




      exportBooking = @"{{\rtf1\ansi\ansicpg1252\deff0\nouicompat\deflang7177{{\fonttbl{{\f0\froman\fprq2\fcharset0 Times New Roman;}}{{\f1\fswiss\fprq2\fcharset0 Calibri;}}{{\f2\fnil\fcharset0 Calibri;}}}}
      {{\colortbl ;\red0\green0\blue0;\red192\green192\blue192;}}
      {{\*\generator Riched20 10.0.10586}}\viewkind4\uc1\trowd\trgaph108\trleft-30\trrh290\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
      \clbrdrl\brdrw30\brdrs\clbrdrt\brdrw30\brdrs\clbrdrr\brdrw15\brdrs\clbrdrb\brdrw15\brdrs \cellx2920\clbrdrl\brdrw15\brdrs\clbrdrt\brdrw30\brdrs\clbrdrr\brdrw30\brdrs\clbrdrb\brdrw15\brdrs \cellx6868 
      \pard\intbl\widctlpar\cf1\f1\fs22\lang1033 Horse No:\cell {0}\cell\row\trowd\trgaph108\trleft-30\trrh290\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
      \clbrdrl\brdrw30\brdrs\clbrdrt\brdrw15\brdrs\clbrdrr\brdrw15\brdrs\clbrdrb\brdrw15\brdrs \cellx2920\clcfpat2\clshdng10000\clbrdrl\brdrw15\brdrs\clbrdrt\brdrw15\brdrs\clbrdrr\brdrw30\brdrs\clbrdrb\brdrw15\brdrs \cellx6868 
      \pard\intbl\widctlpar Client:\cell {1}\cell\row\trowd\trgaph108\trleft-30\trrh290\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
      \clbrdrl\brdrw30\brdrs\clbrdrt\brdrw15\brdrs\clbrdrr\brdrw15\brdrs\clbrdrb\brdrw15\brdrs \cellx2920\clcfpat2\clshdng10000\clbrdrl\brdrw15\brdrs\clbrdrt\brdrw15\brdrs\clbrdrr\brdrw30\brdrs\clbrdrb\brdrw15\brdrs \cellx6868 
      \pard\intbl\widctlpar Loading:\cell {2}\cell\row\trowd\trgaph108\trleft-30\trrh290\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
      \clbrdrl\brdrw30\brdrs\clbrdrt\brdrw15\brdrs\clbrdrr\brdrw15\brdrs\clbrdrb\brdrw15\brdrs \cellx2920\clbrdrl\brdrw15\brdrs\clbrdrt\brdrw15\brdrs\clbrdrr\brdrw30\brdrs\clbrdrb\brdrw15\brdrs \cellx6868 
      \pard\intbl\widctlpar Loading Date:\cell 
      \pard\intbl\widctlpar\qr {3}\cell\row\trowd\trgaph108\trleft-30\trrh290\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
      \clbrdrl\brdrw30\brdrs\clbrdrt\brdrw15\brdrs\clbrdrr\brdrw15\brdrs\clbrdrb\brdrw15\brdrs \cellx2920\clcfpat2\clshdng10000\clbrdrl\brdrw15\brdrs\clbrdrt\brdrw15\brdrs\clbrdrr\brdrw30\brdrs\clbrdrb\brdrw15\brdrs \cellx6868 
      \pard\intbl\widctlpar Off-Loading:\cell {4}\cell\row\trowd\trgaph108\trleft-30\trrh290\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
      \clbrdrl\brdrw30\brdrs\clbrdrt\brdrw15\brdrs\clbrdrr\brdrw15\brdrs\clbrdrb\brdrw15\brdrs \cellx2920\clbrdrl\brdrw15\brdrs\clbrdrt\brdrw15\brdrs\clbrdrr\brdrw30\brdrs\clbrdrb\brdrw15\brdrs \cellx6868 
      \pard\intbl\widctlpar Off-Loading Date:\cell 
      \pard\intbl\widctlpar\qr {5}\cell\row\trowd\trgaph108\trleft-30\trrh290\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
      \clbrdrl\brdrw30\brdrs\clbrdrt\brdrw15\brdrs\clbrdrr\brdrw15\brdrs\clbrdrb\brdrw15\brdrs \cellx2920\clbrdrl\brdrw15\brdrs\clbrdrt\brdrw15\brdrs\clbrdrr\brdrw30\brdrs\clbrdrb\brdrw15\brdrs \cellx6868 
      \pard\intbl\widctlpar Transporter:\cell {6}\cell\row\trowd\trgaph108\trleft-30\trrh290\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
      \clbrdrl\brdrw30\brdrs\clbrdrt\brdrw15\brdrs\clbrdrr\brdrw15\brdrs\clbrdrb\brdrw15\brdrs \cellx2920\clcfpat2\clshdng10000\clbrdrl\brdrw15\brdrs\clbrdrt\brdrw15\brdrs\clbrdrr\brdrw30\brdrs\clbrdrb\brdrw15\brdrs \cellx6868 
      \pard\intbl\widctlpar Horse:\cell {7}\cell\row\trowd\trgaph108\trleft-30\trrh290\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
      \clbrdrl\brdrw30\brdrs\clbrdrt\brdrw15\brdrs\clbrdrr\brdrw15\brdrs\clbrdrb\brdrw15\brdrs \cellx2920\clcfpat2\clshdng10000\clbrdrl\brdrw15\brdrs\clbrdrt\brdrw15\brdrs\clbrdrr\brdrw30\brdrs\clbrdrb\brdrw15\brdrs \cellx6868 
      \pard\intbl\widctlpar Trailer 1:\cell {8} - {9}\cell\row\trowd\trgaph108\trleft-30\trrh290\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
      \clbrdrl\brdrw30\brdrs\clbrdrt\brdrw15\brdrs\clbrdrr\brdrw15\brdrs\clbrdrb\brdrw15\brdrs \cellx2920\clcfpat2\clshdng10000\clbrdrl\brdrw15\brdrs\clbrdrt\brdrw15\brdrs\clbrdrr\brdrw30\brdrs\clbrdrb\brdrw15\brdrs \cellx6868 
      \pard\intbl\widctlpar Trailer 2:\cell {10} - {11}\cell\row\trowd\trgaph108\trleft-30\trrh290\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
      \clbrdrl\brdrw30\brdrs\clbrdrt\brdrw15\brdrs\clbrdrr\brdrw15\brdrs\clbrdrb\brdrw15\brdrs \cellx2920\clcfpat2\clshdng10000\clbrdrl\brdrw15\brdrs\clbrdrt\brdrw15\brdrs\clbrdrr\brdrw30\brdrs\clbrdrb\brdrw15\brdrs \cellx6868 
      \pard\intbl\widctlpar Driver:\cell {12}\cell\row\trowd\trgaph108\trleft-30\trrh290\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
      \clbrdrl\brdrw30\brdrs\clbrdrt\brdrw15\brdrs\clbrdrr\brdrw15\brdrs\clbrdrb\brdrw15\brdrs \cellx2920\clcfpat2\clshdng10000\clbrdrl\brdrw15\brdrs\clbrdrt\brdrw15\brdrs\clbrdrr\brdrw30\brdrs\clbrdrb\brdrw15\brdrs \cellx6868 
      \pard\intbl\widctlpar Initials, Surname:\cell {13}, {14}\cell\row\trowd\trgaph108\trleft-30\trrh290\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
      \clbrdrl\brdrw30\brdrs\clbrdrt\brdrw15\brdrs\clbrdrr\brdrw15\brdrs\clbrdrb\brdrw15\brdrs \cellx2920\clcfpat2\clshdng10000\clbrdrl\brdrw15\brdrs\clbrdrt\brdrw15\brdrs\clbrdrr\brdrw30\brdrs\clbrdrb\brdrw15\brdrs \cellx6868 
      \pard\intbl\widctlpar Passport:\cell {15}\cell\row\trowd\trgaph108\trleft-30\trrh290\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
      \clbrdrl\brdrw30\brdrs\clbrdrt\brdrw15\brdrs\clbrdrr\brdrw15\brdrs\clbrdrb\brdrw15\brdrs \cellx2920\clcfpat2\clshdng10000\clbrdrl\brdrw15\brdrs\clbrdrt\brdrw15\brdrs\clbrdrr\brdrw30\brdrs\clbrdrb\brdrw15\brdrs \cellx6868 
      \pard\intbl\widctlpar Exp Date:\cell 
      \pard\intbl\widctlpar\qr {16}\cell\row\trowd\trgaph108\trleft-30\trrh290\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
      \clbrdrl\brdrw30\brdrs\clbrdrt\brdrw15\brdrs\clbrdrr\brdrw15\brdrs\clbrdrb\brdrw15\brdrs \cellx2920\clcfpat2\clshdng10000\clbrdrl\brdrw15\brdrs\clbrdrt\brdrw15\brdrs\clbrdrr\brdrw30\brdrs\clbrdrb\brdrw15\brdrs \cellx6868 
      \pard\intbl\widctlpar ID:\cell 
      \pard\intbl\widctlpar\qr {17}\cell\row\trowd\trgaph108\trleft-30\trrh290\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
      \clbrdrl\brdrw30\brdrs\clbrdrt\brdrw15\brdrs\clbrdrr\brdrw15\brdrs\clbrdrb\brdrw15\brdrs \cellx2920\clcfpat2\clshdng10000\clbrdrl\brdrw15\brdrs\clbrdrt\brdrw15\brdrs\clbrdrr\brdrw30\brdrs\clbrdrb\brdrw15\brdrs \cellx6868 
      \pard\intbl\widctlpar Nam Cell:\cell {18}\cell\row\trowd\trgaph108\trleft-30\trrh290\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
      \clbrdrl\brdrw30\brdrs\clbrdrt\brdrw15\brdrs\clbrdrr\brdrw15\brdrs\clbrdrb\brdrw15\brdrs \cellx2920\clcfpat2\clshdng10000\clbrdrl\brdrw15\brdrs\clbrdrt\brdrw15\brdrs\clbrdrr\brdrw30\brdrs\clbrdrb\brdrw15\brdrs \cellx6868 
      \pard\intbl\widctlpar RSA Cell:\cell {19}\cell\row\trowd\trgaph108\trleft-30\trrh290\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
      \clbrdrl\brdrw30\brdrs\clbrdrt\brdrw15\brdrs\clbrdrr\brdrw15\brdrs\clbrdrb\brdrw15\brdrs \cellx2920\clbrdrl\brdrw15\brdrs\clbrdrt\brdrw15\brdrs\clbrdrr\brdrw30\brdrs\clbrdrb\brdrw15\brdrs \cellx6868 
      \pard\intbl\widctlpar Rate:\cell 
      \pard\intbl\widctlpar\qr {20}\cell\row\trowd\trgaph108\trleft-30\trrh290\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
      \clbrdrl\brdrw30\brdrs\clbrdrt\brdrw15\brdrs\clbrdrr\brdrw15\brdrs\clbrdrb\brdrw15\brdrs \cellx2920\clbrdrl\brdrw15\brdrs\clbrdrt\brdrw15\brdrs\clbrdrr\brdrw30\brdrs\clbrdrb\brdrw15\brdrs \cellx6868 
      \pard\intbl\widctlpar Border Post:\cell 
      \pard\intbl\widctlpar\qr {21}\cell\row\trowd\trgaph108\trleft-30\trrh290\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
      \clbrdrl\brdrw30\brdrs\clbrdrt\brdrw15\brdrs\clbrdrr\brdrw15\brdrs\clbrdrb\brdrw15\brdrs \cellx2920\clbrdrl\brdrw15\brdrs\clbrdrt\brdrw15\brdrs\clbrdrr\brdrw30\brdrs\clbrdrb\brdrw15\brdrs \cellx6868 
      \pard\intbl\widctlpar Invoice To:\cell {22}\cell\row\trowd\trgaph108\trleft-30\trrh305\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
      \clbrdrl\brdrw30\brdrs\clbrdrt\brdrw15\brdrs\clbrdrr\brdrw15\brdrs\clbrdrb\brdrw30\brdrs \cellx2920\clbrdrl\brdrw15\brdrs\clbrdrt\brdrw15\brdrs\clbrdrr\brdrw30\brdrs\clbrdrb\brdrw30\brdrs \cellx6868 
      \pard\intbl\widctlpar Comments:\cell 
      \pard\intbl\widctlpar\qr {23}\cell\row 
      \pard\widctlpar\sa160\sl252\slmult1\cf0\par

      \pard\sa200\sl276\slmult1\f2\lang9\par
      }}";







      Vehicle Horse = TransportManifestController.DataContext.Document.Vehicles.Where(v => v.IsTruck.Equals(true)).First();
      Vehicle Trailer1 = TransportManifestController.DataContext.Document.Vehicles.Where(v => v.IsTrailer.Equals(true) && !v.Equals(Horse)).FirstOrDefault();
      Vehicle Trailer2 = TransportManifestController.DataContext.Document.Vehicles.Where(v => v.IsTrailer.Equals(true) && (!v.Equals(Horse) && !v.Equals(Trailer1))).FirstOrDefault();
      AccountReference Account = TransportManifestController.DataContext.Document.Waybills.Select(w => w.Account).First();
      Employee Driver = TransportManifestController.DataContext.Document.Driver as Employee;

      exportBooking = String.Format(exportBooking,
        BusinessObject.IsNull(Horse) ? "N/A" : Horse.FleetNumber,
        BusinessObject.IsNull(Account) ? "N/A" : Account.Name,
        TransportManifestController.DataContext.Document.Origin.City.Name,
        ((DateTime)TransportManifestController.DataContext.Document.ScheduledLoadingDate).ToString("D"),
        TransportManifestController.DataContext.Document.Destination.City.Name,
        ((DateTime)TransportManifestController.DataContext.Document.ScheduledOffloadingDate).ToString("D"),
        SecurityContext.OrganizationalUnit.Owner.Name,
        BusinessObject.IsNull(Horse) ? "N/A" : Horse.RegistrationNumber,
        BusinessObject.IsNull(Trailer1) ? "N/A" : Trailer1.RegistrationNumber,
        BusinessObject.IsNull(Trailer1) ? "N/A" : Trailer1.FleetNumber,
        BusinessObject.IsNull(Trailer2) ? "N/A" : Trailer2.RegistrationNumber,
        BusinessObject.IsNull(Trailer2) ? "N/A" : Trailer2.FleetNumber,
        BusinessObject.IsNull(Driver) ? "N/A" : Driver.Nickname,
        BusinessObject.IsNull(Driver) ? "N/A" : Driver.Initials,
        BusinessObject.IsNull(Driver) ? "N/A" : Driver.Lastname,
        BusinessObject.IsNull(Driver) ? "N/A" : Driver.PassportNumber,
        BusinessObject.IsNull(Driver) ? "N/A" : (Driver.PassportExpiryDate == null ? "" : ((DateTime)Driver.PassportExpiryDate).ToString("D")),
        BusinessObject.IsNull(Driver) ? "N/A" : Driver.IDNumber,
        BusinessObject.IsNull(Driver.TelephoneNumbers.Where(t => t.TelephoneType.SystemTelephoneType == ContactManagement.Business.SystemTelephoneType.Mobile).FirstOrDefault()) ? "N/A" : Driver.TelephoneNumbers.Where(t => t.TelephoneType.SystemTelephoneType == ContactManagement.Business.SystemTelephoneType.Mobile).FirstOrDefault().TelephoneNumber,
        BusinessObject.IsNull(Driver.TelephoneNumbers.Where(t => t.TelephoneType.SystemTelephoneType == ContactManagement.Business.SystemTelephoneType.Foreign).FirstOrDefault()) ? "N/A" : Driver.TelephoneNumbers.Where(t => t.TelephoneType.SystemTelephoneType == ContactManagement.Business.SystemTelephoneType.Foreign).FirstOrDefault().TelephoneNumber,
        TransportManifestController.DataContext.Document.Waybills.Sum(w => w.Charge).ToString("N2"),
        BusinessObject.IsNull(TransportManifestController.DataContext.Document.BorderPost) ? "" : TransportManifestController.DataContext.Document.BorderPost.Text,
        TransportManifestController.DataContext.Document.Waybills.Select(w => w.InvoiceTo).First(),
        TransportManifestController.DataContext.Document.Notes);

      ExportBooking = exportBooking;

      //XMLSpreadSheetToClipboard(ExportBooking);
      //Clipboard.SetData("BIFF8", exportBooking.Replace("||\\", "{\\").Replace(";||", ";}").Replace("|||", "}").Replace("/", "\\"));
      Clipboard.SetData(DataFormats.Rtf, exportBooking);

      SystemSounds.Beep.Play();

      //ExceptionHelper.HandleException(new MessageException("BOOKING\r\n\r\nCOPIED TO CLIPBOARD"));
    }

    //[DllImport("user32.dll", SetLastError = true)]
    //static extern uint RegisterClipboardFormat(string lpszFormat);
    //[DllImport("user32.dll")]
    //static extern IntPtr SetClipboardData(uint uFormat, IntPtr hMem);
    //[DllImport("user32.dll", SetLastError = true)]
    //static extern bool CloseClipboard();
    //[DllImport("user32.dll", SetLastError = true)]
    //static extern bool OpenClipboard(IntPtr hWndNewOwner);

    //private static void XMLSpreadSheetToClipboard(String S)
    //{
    //  var HGlob = Marshal.StringToHGlobalAnsi(S);
    //  uint Format = RegisterClipboardFormat("XML SpreadSheet");
    //  OpenClipboard(IntPtr.Zero);
    //  SetClipboardData(Format, HGlob);
    //  CloseClipboard();
    //  Marshal.FreeHGlobal(HGlob);
    //}


    #endregion

  }
}

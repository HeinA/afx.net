﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Documents.TransportManifest
{
  public class Operations : Afx.Prism.RibbonMdi.StandardOperations
  {
      public const string Cancel = "{42ef4a85-c9f2-4562-a7e7-c1fc829feeaa}";
      public const string EditVehicles = "{c7f59ab9-6f84-49c0-bc06-cacf53237a85}";
      public const string NewWaybill = "{bda589aa-4aa8-4039-9fd7-8c2b9272cb74}";
      public const string Verify = "{eb21155f-7212-4213-9cb8-47af39bb9927}";
  }
}
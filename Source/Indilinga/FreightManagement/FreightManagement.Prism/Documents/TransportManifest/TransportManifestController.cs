﻿using Afx.Business;
using Afx.Business.Security;
using Afx.Business.Service;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Documents;
using FleetManagement.Prism.Dialogs.EditVehiclesDialog;
using FreightManagement.Business;
using FreightManagement.Business.Service;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Documents.TransportManifest
{
  public partial class TransportManifestController : MdiDocumentController<TransportManifestContext, TransportManifestViewModel>
  {
    [InjectionConstructor]
    public TransportManifestController(IController controller)
      : base(controller)
    {
    }

    #region TransportManifestScanViewModel ScanViewModel

    public const string ScanViewModelProperty = "ScanViewModel";
    TransportManifestScanViewModel mScanViewModel;
    public TransportManifestScanViewModel ScanViewModel
    {
      get { return mScanViewModel; }
      set { mScanViewModel = value; }
    }

    #endregion

    protected override void ExtendDocument()
    {
      ExtendDocument<TransportManifestHeaderViewModel>(HeaderRegion);
      ScanViewModel = ExtendDocument<TransportManifestScanViewModel>(HeaderRegion);

      ExtendDocument<TransportManifestWaybillsViewModel>(DockRegion);

      ExtendDocument<TransportManifestFeesViewModel>(TabRegion);

      ExtendDocument<TransportManifestBookingTextViewModel>(TabRegion);

      base.ExtendDocument();
    }

    protected override void ExecuteOperation(Operation op, BusinessObject argument)
    {
      switch (op.Identifier)
      {
        case Operations.Verify:
          SetDocumentState(Business.TransportManifest.States.Verified);
          break;
        case Operations.EditVehicles:
          EditVehiclesDialogController evdc = GetCreateChildController<EditVehiclesDialogController>(EditVehiclesDialogController.EditVehiclesDialogControllerKey);
          evdc.VehicleCollection = DataContext.Document;
          //if (DataContext.Document.VehicleHorse != null) evdc.Vehicles.Add(DataContext.Document.VehicleHorse);
          //if (DataContext.Document.VehicleTrailer1 != null) evdc.Vehicles.Add(DataContext.Document.VehicleTrailer1);
          //if (DataContext.Document.VehicleTrailer2 != null) evdc.Vehicles.Add(DataContext.Document.VehicleTrailer2);
          //if (DataContext.Document.VehicleTrailer3 != null) evdc.Vehicles.Add(DataContext.Document.VehicleTrailer3);
          //if (DataContext.Document.VehicleTrailer4 != null) evdc.Vehicles.Add(DataContext.Document.VehicleTrailer4);
          evdc.Run();

          break;

        case Operations.Cancel:
          SetDocumentState(Business.TransportManifest.States.Cancelled);
          break;
      }
      base.ExecuteOperation(op, argument);
    }

    public void AttachWaybill(string waybillNumber)
    {
      try
      {
        using (var svc = ProxyFactory.GetService<IFreightManagementService>(SecurityContext.ServerName))
        {
          AttachWaybill(svc.LoadWaybillReferenceByDocumentNumber(waybillNumber));
        }
      }
      catch (Exception ex)
      {
        if (ExceptionHelper.HandleException(ex)) throw ex;
      }
    }

    public void AttachWaybill(WaybillReference waybill)
    {
      ScanViewModel.ErrorMessage = string.Empty;

      try
      {
        if (BusinessObject.IsNull(waybill))
        {
          ScanViewModel.ErrorMessage = "Waybill not found.";
          System.Media.SystemSounds.Exclamation.Play();
          return;
        }

        if (DataContext.Document.IsReadOnly)
        {
          ScanViewModel.ErrorMessage = "Document may not be edited.";
          System.Media.SystemSounds.Exclamation.Play();
          return;
        }

        if (waybill.WaybillState.Identifier != Business.Waybill.States.OnFloor)
        {
          ScanViewModel.ErrorMessage = "Waybill must be on floor.";
          System.Media.SystemSounds.Exclamation.Play();
          return;
        }

        if (DataContext.Document.State.DocumentTypeState.Identifier != Business.TransportManifest.States.Open)
        {
          ScanViewModel.ErrorMessage = "Document may not be edited.";
          System.Media.SystemSounds.Exclamation.Play();
          return;
        }

        if (DataContext.Document.Waybills.Contains(waybill))
        {
          DataContext.Document.Waybills.Remove(waybill);
          if (DataContext.Document.Waybills.Count == 0)
          {
            DataContext.Document.Origin = null;
            DataContext.Document.Destination = null;
          }
        }
        else
        {
          if (DataContext.Document.Waybills.Count == 0)
          {
            DataContext.Document.Origin = waybill.CollectionCity;
            DataContext.Document.Destination = waybill.DeliveryCity;

            DataContext.Document.Route = String.Format("{0}->{1}", DataContext.Document.Origin.City.Name, DataContext.Document.Destination.City.Name);
          }
          try
          {
            DataContext.Document.Waybills.Add(waybill);
          }
          catch (InvalidWaybillException ex)
          {
            ScanViewModel.ErrorMessage = ex.Message;
            System.Media.SystemSounds.Exclamation.Play();
            return;
          }
        }
      }
      catch (Exception ex)
      {
        if (ExceptionHelper.HandleException(ex)) throw ex;
      }
    }
  }
}


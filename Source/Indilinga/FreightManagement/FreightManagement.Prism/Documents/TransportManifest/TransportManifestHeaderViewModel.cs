﻿using Afx.Business;
using Afx.Business.Activities;
using Afx.Business.Data;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Documents;
using ContactManagement.Business;
using FleetManagement.Business;
using FreightManagement.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace FreightManagement.Prism.Documents.TransportManifest
{
  public class TransportManifestHeaderViewModel : DocumentHeaderViewModel<FreightManagement.Business.TransportManifest>
  {
    #region Constructors

    [InjectionConstructor]
    public TransportManifestHeaderViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    public IEnumerable<SlotTime> SlotTimes
    {
      get { return Cache.Instance.GetObjects<SlotTime>(true, st => st.Text, true); }
    }

    public IEnumerable<Contact> Drivers
    {
      get
      {
        IEnumerable<Contact> drivers = null;
        drivers = Cache.Instance.GetObjects<Employee>(true, e => e.ContactType.IsFlagged(ContactTypeFlags.Driver), e => e.Fullname, true);
        return drivers;
      }
    }
  }
}


﻿using Afx.Business.Security;
using Afx.Business.Service;
using Afx.Prism.RibbonMdi.Documents;
using FreightManagement.Business;
using FreightManagement.Business.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Documents.WaybillReceipt
{
  public partial class WaybillReceiptContext : DocumentContext<FreightManagement.Business.WaybillReceipt>
  {
    public WaybillReceiptContext()
      : base(FreightManagement.Business.WaybillReceipt.DocumentTypeIdentifier)
    {
    }

    public WaybillReceiptContext(Guid documentIdentifier)
      : base(FreightManagement.Business.WaybillReceipt.DocumentTypeIdentifier, documentIdentifier)
    {
    }

    public override void SaveData()
    {
      using (var svc = ProxyFactory.GetService<IFreightManagementService>(SecurityContext.ServerName))
      {
        Document = svc.SaveWaybillReceipt(Document);
      }
      base.SaveData();
    }

    public override void LoadData()
    {
      using (var svc = ProxyFactory.GetService<IFreightManagementService>(SecurityContext.ServerName))
      {
        Document = svc.LoadWaybillReceipt(DocumentIdentifier);
      }
      base.LoadData();
    }
  }
}


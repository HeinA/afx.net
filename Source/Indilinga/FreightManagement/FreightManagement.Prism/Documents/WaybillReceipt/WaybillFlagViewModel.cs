﻿using Afx.Business.Collections;
using Afx.Prism;
using FreightManagement.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Documents.WaybillReceipt
{
  public class WaybillFlagViewModel : ViewModel<WaybillFlag>
  {
    #region Constructors

    [InjectionConstructor]
    public WaybillFlagViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    [Dependency]
    public WaybillReceiptController WaybillReceiptController { get; set; }

    public new WaybillReceiptWaybillViewModel Parent
    {
      get { return (WaybillReceiptWaybillViewModel)base.Parent; }
    }

    BusinessObjectCollection<WaybillFlag> WaybillFlags
    {
      get { return WaybillReceiptController.DataContext.Document.GetAssociativeObject<WaybillReceiptWaybill>(Parent.Model).WaybillFlags; }
    }

    #region bool IsChecked

    public const string IsCheckedProperty = "IsChecked";
    public bool IsChecked
    {
      get { return WaybillFlags.Contains(Model); }
      set
      {
        if (value)
        {
          if (!WaybillFlags.Contains(Model)) WaybillFlags.Add(Model);
        }
        else
        {
          if (WaybillFlags.Contains(Model)) WaybillFlags.Remove(Model);
        }
      }
    }

    #endregion
  }
}

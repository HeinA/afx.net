﻿using Afx.Prism;
using Afx.Prism.Controls;
using Afx.Prism.RibbonMdi;
using Afx.Prism.RibbonMdi.Documents;
using FreightManagement.Business;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Documents.WaybillReceipt
{
  public class WaybillReceiptWaybillsViewModel : ExtensionViewModel<FreightManagement.Business.WaybillReceipt>
  {
    #region Constructors

    [InjectionConstructor]
    public WaybillReceiptWaybillsViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    public IEnumerable<WaybillReceiptWaybillViewModel> Waybills
    {
      get { return ResolveViewModels<WaybillReceiptWaybillViewModel, WaybillReference>(Model.Waybills); }
    }

    //#region WaybillReference SelectedWaybill

    //public const string SelectedWaybillProperty = "SelectedWaybill";
    //WaybillReference mSelectedWaybill;
    //public WaybillReference SelectedWaybill
    //{
    //  get { return mSelectedWaybill; }
    //  set { SetProperty<WaybillReference>(ref mSelectedWaybill, value); }
    //}

    //#endregion

    //#region DelegateCommand<WaybillReference> OpenWaybillCommand
    
    //DelegateCommand<WaybillReference> mOpenWaybillCommand;
    //public DelegateCommand<WaybillReference> OpenWaybillCommand
    //{
    //  get { return mOpenWaybillCommand ?? ( mOpenWaybillCommand = new DelegateCommand<WaybillReference>(ExecuteOpenWaybill, CanExecuteOpenWaybill)); }
    //}
    
    //bool CanExecuteOpenWaybill(WaybillReference args)
    //{
    //  return true;
    //}
    
    //void ExecuteOpenWaybill(WaybillReference args)
    //{
    //  MdiApplicationController.Current.EditDocument(Business.Waybill.DocumentTypeIdentifier, args.GlobalIdentifier);
    //}
    
    //#endregion

    //#region DelegateCommand<ItemDeletingEventArgs> ItemDeletingCommand

    //DelegateCommand<ItemDeletingEventArgs> mItemDeletingCommand;
    //public DelegateCommand<ItemDeletingEventArgs> ItemDeletingCommand
    //{
    //  get { return mItemDeletingCommand ?? (mItemDeletingCommand = new DelegateCommand<ItemDeletingEventArgs>(ExecuteItemDeleting, CanExecuteItemDeleting)); }
    //}

    //bool CanExecuteItemDeleting(ItemDeletingEventArgs args)
    //{
    //  return true;
    //}

    //void ExecuteItemDeleting(ItemDeletingEventArgs args)
    //{
    //  WaybillReference wr = args.Item as WaybillReference;
    //  if (wr == null) return;
    //  WaybillReceiptWaybill wrw = Model.GetAssociativeObject<WaybillReceiptWaybill>(wr);
    //  args.Cancel = !wrw.IsNew;
    //  if (args.Cancel)
    //  {
    //    System.Media.SystemSounds.Exclamation.Play();
    //    return;
    //  }
    //}

    //#endregion

  }
}

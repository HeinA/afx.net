﻿using Afx.Prism;
using Afx.Prism.RibbonMdi.Documents;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Documents.WaybillReceipt
{
  public class WaybillReceiptFooterViewModel : ExtensionViewModel<Business.WaybillReceipt>
  {
    #region Constructors

    [InjectionConstructor]
    public WaybillReceiptFooterViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

  }
}

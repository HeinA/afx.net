﻿using Afx.Business.Data;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using FreightManagement.Business;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Documents.WaybillReceipt
{
  public class WaybillReceiptWaybillViewModel : ViewModel<WaybillReference>
  {
    #region Constructors

    [InjectionConstructor]
    public WaybillReceiptWaybillViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    #region DelegateCommand OpenWaybillCommand

    DelegateCommand mOpenWaybillCommand;
    public DelegateCommand OpenWaybillCommand
    {
      get { return mOpenWaybillCommand ?? (mOpenWaybillCommand = new DelegateCommand(ExecuteOpenWaybill, CanExecuteOpenWaybill)); }
    }

    bool CanExecuteOpenWaybill()
    {
      return true;
    }

    void ExecuteOpenWaybill()
    {
      MdiApplicationController.Current.EditDocument(Business.Waybill.DocumentTypeIdentifier, Model.GlobalIdentifier);
    }

    #endregion

    [Dependency]
    public WaybillReceiptController WaybillReceiptController { get; set; }

    public IEnumerable<Bay> Bays
    {
      get { return Cache.Instance.GetObjects<Bay>(b => b.Text, true); }
    }

    #region Bay Bay

    public const string BayProperty = "Bay";
    public Bay Bay
    {
      get { return WaybillReceiptController.DataContext.Document.GetAssociativeObject<Business.WaybillReceiptWaybill>(Model).Bay; }
      set { WaybillReceiptController.DataContext.Document.GetAssociativeObject<Business.WaybillReceiptWaybill>(Model).Bay = value; }
    }

    #endregion

    #region string Comment

    public const string CommentProperty = "Comment";
    public string Comment
    {
      get { return WaybillReceiptController.DataContext.Document.GetAssociativeObject<Business.WaybillReceiptWaybill>(Model).Comment; }
      set { WaybillReceiptController.DataContext.Document.GetAssociativeObject<Business.WaybillReceiptWaybill>(Model).Comment = value; }
    }

    #endregion

    public IEnumerable<WaybillFlagViewModel> Flags
    {
      get { return ResolveViewModels<WaybillFlagViewModel, WaybillFlag>(Cache.Instance.GetObjects<WaybillFlag>((r) => r.Text, true)); }
    }
  }
}

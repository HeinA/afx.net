﻿using Afx.Business;
using Afx.Business.Security;
using Afx.Business.Service;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Documents;
using FreightManagement.Business;
using FreightManagement.Business.Service;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Documents.WaybillReceipt
{
  public partial class WaybillReceiptController : MdiDocumentController<WaybillReceiptContext, WaybillReceiptViewModel>
  {
    [InjectionConstructor]
    public WaybillReceiptController(IController controller)
      : base(controller)
    {
    }

    #region WaybillReceiptScanViewModel ScanViewModel

    public const string ScanViewModelProperty = "ScanViewModel";
    WaybillReceiptScanViewModel mScanViewModel;
    public WaybillReceiptScanViewModel ScanViewModel
    {
      get { return mScanViewModel; }
      set { mScanViewModel = value; }
    }

    #endregion

    protected override void ExtendDocument()
    {
      ExtendDocument<WaybillReceiptHeaderViewModel>(HeaderRegion);
      ScanViewModel = ExtendDocument<WaybillReceiptScanViewModel>(HeaderRegion);

      ExtendDocument<WaybillReceiptWaybillsViewModel>(DockRegion);
      ExtendDocument<WaybillReceiptFooterViewModel>(FooterRegion);

      base.ExtendDocument();
    }

    protected override void ExecuteOperation(Operation op, BusinessObject argument)
    {
      switch (op.Identifier)
      {
        case Operations.Verify:
          SetDocumentState(Business.WaybillReceipt.States.Verified);
          break;
      }
      base.ExecuteOperation(op, argument);
    }

    public void AttachWaybill(string waybillNumber)
    {
      using (var svc = ProxyFactory.GetService<IFreightManagementService>(SecurityContext.ServerName))
      {
        AttachWaybill(svc.LoadWaybillReferenceByDocumentNumber(waybillNumber));
      }
    }

    public void AttachWaybill(WaybillReference waybill)
    {
      ScanViewModel.ErrorMessage = string.Empty;

      if (BusinessObject.IsNull(waybill))
      {
        ScanViewModel.ErrorMessage = "Waybill not found.";
        System.Media.SystemSounds.Exclamation.Play();
        return;
      }

      if (DataContext.Document.IsReadOnly)
      {
        ScanViewModel.ErrorMessage = "Document may not be edited.";
        System.Media.SystemSounds.Exclamation.Play();
        return;
      }

      if (DataContext.Document.State.DocumentTypeState.Identifier != Business.WaybillReceipt.States.Open)
      {
        ScanViewModel.ErrorMessage = "Document may not be edited.";
        System.Media.SystemSounds.Exclamation.Play();
        return;
      }

      if (waybill.IsPointToPoint)
      {
        ScanViewModel.ErrorMessage = "Waybill is Point to Point.";
        System.Media.SystemSounds.Exclamation.Play();
        return;
      }

      if (DataContext.Document.Waybills.Contains(waybill))
      {
        //WaybillReceiptWaybill wrw = DataContext.Document.GetAssociativeObject<WaybillReceiptWaybill>(waybill);
        //if (!wrw.IsNew)
        //{
        //  System.Media.SystemSounds.Exclamation.Play();
        //  return;
        //}
        
        DataContext.Document.Waybills.Remove(waybill);
      }
      else
      {
        DataContext.Document.Waybills.Add(waybill);
      }
    }
  }
}


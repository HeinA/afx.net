﻿using Afx.Prism;
using Afx.Prism.RibbonMdi.Documents;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Documents.DeliveryManifest
{
  public class DeliveryManifestFooterViewModel : ExtensionViewModel<Business.DeliveryManifest>
  {
    #region Constructors

    [InjectionConstructor]
    public DeliveryManifestFooterViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

  }
}

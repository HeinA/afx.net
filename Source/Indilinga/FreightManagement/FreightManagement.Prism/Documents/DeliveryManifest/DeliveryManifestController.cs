﻿using Afx.Business;
using Afx.Business.Documents;
using Afx.Business.Security;
using Afx.Business.Service;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Documents;
using DocumentManagement.Business;
using DocumentManagement.Prism.Extensions.DocumentRepository;
using FleetManagement.Prism.Dialogs.EditVehiclesDialog;
using FreightManagement.Business;
using FreightManagement.Business.Service;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace FreightManagement.Prism.Documents.DeliveryManifest
{
  public partial class DeliveryManifestController : MdiDocumentController<DeliveryManifestContext, DeliveryManifestViewModel>, IDocumentRepositoryAware
  {
    [InjectionConstructor]
    public DeliveryManifestController(IController controller)
      : base(controller)
    {
    }

    #region DocumentRepositoryExtensionViewModel DocumentRepositoryExtensionViewModel

    public const string DocumentRepositoryExtensionViewModelProperty = "DocumentRepositoryExtensionViewModel";
    DocumentRepositoryExtensionViewModel mDocumentRepositoryExtensionViewModel;
    public DocumentRepositoryExtensionViewModel DocumentRepositoryExtensionViewModel
    {
      get { return mDocumentRepositoryExtensionViewModel; }
      set { mDocumentRepositoryExtensionViewModel = value; }
    }

    #endregion

    public void AddScannedDocument(ScannedDocument document)
    {
      DocumentRepositoryExtension dre = DataContext.Document.GetExtensionObject<DocumentRepositoryExtension>();
      if (dre != null)
      {
        dre.ScannedDocuments.Add(document);
        RegionManager.Regions[TabRegion].Activate(DocumentRepositoryExtensionViewModel);
      }
    }

    public Document Document { get { return DataContext.Document; } }

    #region DeliveryManifestScanViewModel ScanViewModel

    public const string ScanViewModelProperty = "ScanViewModel";
    DeliveryManifestScanViewModel mScanViewModel;
    public DeliveryManifestScanViewModel ScanViewModel
    {
      get { return mScanViewModel; }
      set { mScanViewModel = value; }
    }

    #endregion

    #region DeliveryManifestHeaderViewModel DeliveryManifestHeaderViewModel

    public const string DeliveryManifestHeaderViewModelProperty = "DeliveryManifestHeaderViewModel";
    DeliveryManifestHeaderViewModel mDeliveryManifestHeaderViewModel;
    public DeliveryManifestHeaderViewModel DeliveryManifestHeaderViewModel
    {
      get { return mDeliveryManifestHeaderViewModel; }
      set { mDeliveryManifestHeaderViewModel = value; }
    }

    #endregion

    protected override void ExtendDocument()
    {
      DeliveryManifestHeaderViewModel = ExtendDocument<DeliveryManifestHeaderViewModel>(HeaderRegion);
      ScanViewModel = ExtendDocument<DeliveryManifestScanViewModel>(HeaderRegion);

      ExtendDocument<DeliveryManifestWaybillsViewModel>(DockRegion);
      ExtendDocument<DeliveryManifestFooterViewModel>(FooterRegion);

      DocumentRepositoryExtensionViewModel = ExtendDocument<DocumentRepositoryExtension, DocumentRepositoryExtensionViewModel>(TabRegion);

      base.ExtendDocument();
    }

    protected override void ExecuteOperation(Operation op, BusinessObject argument)
    {
      switch (op.Identifier)
      {
        case Operations.Verify:
          SetDocumentState(Business.DeliveryManifest.States.Verified);
          break;

        case Operations.EditVehicles:
          EditVehiclesDialogController evdc = GetCreateChildController<EditVehiclesDialogController>(EditVehiclesDialogController.EditVehiclesDialogControllerKey);
          evdc.VehicleCollection = DataContext.Document;
          if (!BusinessObject.IsNull(DataContext.Document.Courier)) evdc.Contractor = DataContext.Document.Courier;
          evdc.Run();
          break;

        case Operations.PrintTripsheet:
          Print(preview: true, argument: true);
          break;

        case Operations.Cancel:
          SetDocumentState(Business.DeliveryManifest.States.Cancelled);
          break;
      }
      base.ExecuteOperation(op, argument);
    }

    //public void AttachWaybill(string waybillNumber)
    //{
    //  try
    //  {
    //    using (var svc = ProxyFactory.GetService<IFreightManagementService>(SecurityContext.ServerName))
    //    {
    //      AttachWaybill(svc.LoadWaybillReferenceByDocumentNumber(waybillNumber));
    //    }
    //  }
    //  catch (Exception ex)
    //  {
    //    if (ExceptionHelper.HandleException(ex)) throw ex;
    //  }
    //}

    public void AttachWaybill(string waybillNumber)
    {
      try
      {
        using (var svc = ProxyFactory.GetService<IFreightManagementService>(SecurityContext.ServerName))
        {
          var wr = svc.LoadWaybillReferenceByDocumentNumber(waybillNumber);
          if (wr != null)
          {
            if (wr.Parcels.Count > 0)
            {
              ScanViewModel.ErrorMessage = "Document must be loaded using parcel barcodes.";
              System.Media.SystemSounds.Exclamation.Play();
              return;
            }
            AttachWaybill(wr);
          }
          else
          {
            wr = svc.LoadWaybillReferenceByParcelNumber(waybillNumber);
            if (!VerifyWaybill(wr)) return;

            foreach (var p1 in DataContext.Document.Parcels)
            {
              if (!wr.Parcels.Any(p3 => p3.Barcode == p1.Barcode && p3.Owner.WaybillNumber == wr.WaybillNumber && !p3.IsIgnored))
              {
                //Delete parcels no longer on waybill / that is set to be ignored
                p1.IsDeleted = true;
              }
            }


            foreach (var p1 in wr.Parcels)
            {
              var p2 = DataContext.Document.Parcels.FirstOrDefault(p3 => p3.Barcode == p1.Barcode);
              if (p2 == null)
              {
                DataContext.Document.Parcels.Add(new DeliveryManifestParcel() { WaybillNumber = wr.WaybillNumber, Barcode = p1.Barcode, IsLoaded = p1.Barcode == waybillNumber });
              }
              else
              {
                if (p2.Barcode == waybillNumber)
                {
                  p2.IsDeleted = false;

                  if (p2.IsLoaded && MessageBox.Show("Package is already scanned.  Do you want to offload it?", "Offload?", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                  {
                    p2.IsLoaded = false;

                  }
                  else
                  {
                    p2.IsLoaded = true;
                  }
                }
              }
            }


            if (DataContext.Document.Parcels.Any(p2 => !p2.IsDeleted && !p2.IsLoaded) && DataContext.Document.Waybills.Contains(wr))
            {
              AttachWaybill(wr);
              return;
            }

            if (!DataContext.Document.Parcels.Any(p2 => !p2.IsDeleted && !p2.IsLoaded) && !DataContext.Document.Waybills.Contains(wr))
            {
              AttachWaybill(wr);
            }
          }
        }
      }
      catch (Exception ex)
      {
        if (ExceptionHelper.HandleException(ex)) throw ex;
      }
      finally
      {
        DeliveryManifestHeaderViewModel.Refresh();
      }
    }

    public bool VerifyWaybill(WaybillReference waybill)
    {
      ScanViewModel.ErrorMessage = string.Empty;

      try
      {
        if (BusinessObject.IsNull(waybill))
        {
          ScanViewModel.ErrorMessage = "Waybill not found.";
          System.Media.SystemSounds.Exclamation.Play();
          return false;
        }

        if (!waybill.Bay.AllowDelivery)
        {
          ScanViewModel.ErrorMessage = "Waybill may not be delivered.";
          System.Media.SystemSounds.Exclamation.Play();
          return false;
        }

        if (DataContext.Document.IsReadOnly)
        {
          ScanViewModel.ErrorMessage = "Document may not be edited.";
          System.Media.SystemSounds.Exclamation.Play();
          return false;
        }

        if (waybill.WaybillState.Identifier != Business.Waybill.States.OnFloor)
        {
          ScanViewModel.ErrorMessage = "Waybill must be on floor.";
          System.Media.SystemSounds.Exclamation.Play();
          return false;
        }

        if (DataContext.Document.State.DocumentTypeState.Identifier != Business.DeliveryManifest.States.Open)
        {
          ScanViewModel.ErrorMessage = "Document may not be edited.";
          System.Media.SystemSounds.Exclamation.Play();
          return false;
        }

        if (DataContext.Document.Waybills.Count == 0)
        {
          DataContext.Document.IsPointToPoint = waybill.IsPointToPoint;
        }

        if (DataContext.Document.IsPointToPoint != waybill.IsPointToPoint)
        {
          if (DataContext.Document.IsPointToPoint) ScanViewModel.ErrorMessage = "Waybill must be Point to Point.";
          else ScanViewModel.ErrorMessage = "Waybill must not be Point to Point.";
          System.Media.SystemSounds.Exclamation.Play();
          return false;
        }

        if (!waybill.IsPointToPoint)
        {
          if (!(BusinessObject.IsNull(DataContext.Document.Route) || DataContext.Document.Waybills.Count == 0))
          {
            if (!DataContext.Document.Route.Equals(waybill.Route))
            {
              ScanViewModel.ErrorMessage = "Waybill is for incorrect route.";
              System.Media.SystemSounds.Exclamation.Play();
              return false;
            }
          }
        }
      }
      catch (Exception ex)
      {
        if (ExceptionHelper.HandleException(ex)) throw ex;
      }

      return true;
    }

    public void AttachWaybill(WaybillReference waybill)
    {
      if (VerifyWaybill(waybill))
      {
        if (DataContext.Document.Waybills.Contains(waybill))
        {
          DataContext.Document.Waybills.Remove(waybill);
          if (DataContext.Document.Waybills.Count == 0)
          {
            DataContext.Document.Route = null;
            DataContext.Document.IsPointToPoint = false;
          }
        }
        else
        {
          try
          {
            if (!waybill.IsPointToPoint)
            {
              if (BusinessObject.IsNull(DataContext.Document.Route) || DataContext.Document.Waybills.Count == 0)
              {
                DataContext.Document.Route = waybill.Route;
              }
            }
            else
            {
              if (DataContext.Document.Waybills.Count == 0)
              {
                DataContext.Document.Description = string.Format("{0} -> {1}", waybill.FullCollectionLocation.FullNameWithoutZone, waybill.FullDeliveryLocation.FullNameWithoutZone);
              }
            }

            DataContext.Document.Waybills.Add(waybill);
          }
          catch (InvalidWaybillException ex)
          {
            ScanViewModel.ErrorMessage = ex.Message;
            System.Media.SystemSounds.Exclamation.Play();
          }
        }
      }
    }
  }
}


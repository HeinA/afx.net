﻿using Afx.Business;
using Afx.Business.Activities;
using Afx.Business.Security;
using Afx.Business.Service;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using Afx.Prism.RibbonMdi.Documents;
using FreightManagement.Business;
using FreightManagement.Business.Service;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace FreightManagement.Prism.Documents.DeliveryManifest
{
  public partial class DeliveryManifestViewModel : MdiDocumentViewModel<DeliveryManifestContext>, IDropTarget
  {
    [InjectionConstructor]
    public DeliveryManifestViewModel(IController controller)
      : base(controller)
    {
    }

    [Dependency]
    public DeliveryManifestController DeliveryManifestController { get; set; }

    public bool CanDrop(object data)
    {
      Collection<DraggedDocumentReference> refs = data as Collection<DraggedDocumentReference>;
      if (refs == null) return false;

      foreach (var r in refs)
      {
        if (r.DocumentType.Identifier == Business.Waybill.DocumentTypeIdentifier) return true;
      }

      return false;
    }

    public void DoDrop(object data)
    {
      try
      {
        Collection<DraggedDocumentReference> refs = data as Collection<DraggedDocumentReference>;
        if (refs == null) return;

        using (var svc = ServiceFactory.GetService<IFreightManagementService>(SecurityContext.ServerName))
        {
          foreach (var r in refs)
          {
            if (r.DocumentType.Identifier == Business.Waybill.DocumentTypeIdentifier)
            {
              DeliveryManifestController.AttachWaybill(svc.Instance.LoadWaybillReference(r.DocumentGuid));
            }
          }
        }
      }
      catch (Exception ex)
      {
        if (ExceptionHelper.HandleException(ex)) throw ex;
      }
    }
  }
}


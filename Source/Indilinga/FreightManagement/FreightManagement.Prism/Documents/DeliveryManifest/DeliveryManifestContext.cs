﻿using Afx.Business.Security;
using Afx.Business.Service;
using Afx.Prism.RibbonMdi.Documents;
using FreightManagement.Business;
using FreightManagement.Business.Service;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Documents.DeliveryManifest
{
  public partial class DeliveryManifestContext : DocumentContext<FreightManagement.Business.DeliveryManifest>
  {
    public DeliveryManifestContext()
      : base(FreightManagement.Business.DeliveryManifest.DocumentTypeIdentifier)
    {
    }

    public DeliveryManifestContext(Guid documentIdentifier)
      : base(FreightManagement.Business.DeliveryManifest.DocumentTypeIdentifier, documentIdentifier)
    {
    }

    public override void SaveData()
    {
      using (var svc = ProxyFactory.GetService<IFreightManagementService>(SecurityContext.ServerName))
      {
        Document = svc.SaveDeliveryManifest(Document);
      }
      base.SaveData();
    }

    public override void LoadData()
    {
      using (var svc = ProxyFactory.GetService<IFreightManagementService>(SecurityContext.ServerName))
      {
        Document = svc.LoadDeliveryManifest(DocumentIdentifier);
      }
      base.LoadData();
    }
  }
}


﻿using Afx.Business;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Documents;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace FreightManagement.Prism.Documents.DeliveryManifest
{
  public class DeliveryManifestScanViewModel : ExtensionViewModel<FreightManagement.Business.DeliveryManifest>
  {
    #region Constructors

    [InjectionConstructor]
    public DeliveryManifestScanViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    [Dependency]
    public DeliveryManifestController DeliveryManifestController { get; set; }

    #region string WaybillNumber

    public const string WaybillNumberProperty = "WaybillNumber";
    string mWaybillNumber;
    public string WaybillNumber
    {
      get { return mWaybillNumber; }
      set { SetProperty<string>(ref mWaybillNumber, value); }
    }

    #endregion

    #region string ErrorMessage

    public const string ErrorMessageProperty = "ErrorMessage";
    string mErrorMessage;
    public string ErrorMessage
    {
      get { return mErrorMessage; }
      set
      {
        if (SetProperty<string>(ref mErrorMessage, value))
        {
          OnPropertyChanged(VisibilityProperty);
        }
      }
    }

    #endregion

    #region Visibility Visibility

    public const string VisibilityProperty = "Visibility";
    public Visibility Visibility
    {
      get { return string.IsNullOrWhiteSpace(ErrorMessage) ? Visibility.Collapsed : Visibility.Visible; }
    }

    #endregion

    #region DelegateCommand KeyDownCommand

    DelegateCommand<KeyEventArgs> mKeyDownCommand;
    public DelegateCommand<KeyEventArgs> KeyDownCommand
    {
      get { return mKeyDownCommand ?? (mKeyDownCommand = new DelegateCommand<KeyEventArgs>(ExecuteKeyDown)); }
    }

    void ExecuteKeyDown(KeyEventArgs e)
    {
      if (e.Key != Key.Enter) return;

      try
      {
        DeliveryManifestController.AttachWaybill(WaybillNumber);
        WaybillNumber = string.Empty;
      }
      catch (Exception ex)
      {
        if (ExceptionHelper.HandleException(ex)) throw ex;
      }

      e.Handled = true;
    }

    #endregion
  }
}

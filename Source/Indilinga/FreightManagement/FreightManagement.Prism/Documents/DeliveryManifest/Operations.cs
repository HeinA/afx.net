﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Prism.Documents.DeliveryManifest
{
  public class Operations : Afx.Prism.RibbonMdi.StandardOperations
  {
      public const string Cancel = "{d73e4b4d-a386-412d-905d-e743eb34da7d}";
      public const string EditVehicles = "{76f24ae3-6e30-43ff-b35f-a4ee2b22a3da}";
      public const string PrintTripsheet = "{e6ee248c-4dd0-45e5-9ec8-ef897725a1fc}";
      public const string Verify = "{894d9a8c-f53c-4a19-95a2-78775dc60e0e}";
  }
}
﻿using Afx.Business.Data;
using Afx.Business.Documents;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Business
{
  [DataContract(IsReference = true, Namespace = Namespace.FreightManagement)]
  public partial class PODHandover
  {
    public const string DocumentTypeIdentifier = "{ba054a7b-a6f4-4265-a6ad-29674783554d}";
    public class States
    {
      public const string Closed = "{9d0e9b5f-4de9-4230-9f47-51b6554f3b38}";
      public const string Open = "{c9024a5e-527d-42ac-9709-3db9ad0aedc9}";
    }

    protected PODHandover(DocumentType documentType)
      : base(documentType)
    {
    }
  }
}

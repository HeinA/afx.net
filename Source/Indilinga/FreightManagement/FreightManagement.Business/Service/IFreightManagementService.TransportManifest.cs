﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Business.Service
{
  public partial interface IFreightManagementService
  {
    [OperationContract]
    FreightManagement.Business.TransportManifest LoadTransportManifest(Guid globalIdentifier);

    [OperationContract]
    FreightManagement.Business.TransportManifest SaveTransportManifest(FreightManagement.Business.TransportManifest obj);

    [OperationContract]
    EnumTransportManifestDateFields SaveTransportManifestDates(Guid globalIdentifier, EnumTransportManifestDateFields whichDate, DateTime? date);
  }
}

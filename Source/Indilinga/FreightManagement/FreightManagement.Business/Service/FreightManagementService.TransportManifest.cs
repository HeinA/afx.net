﻿using Afx.Business.Service;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.WCF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Business.Service
{
  public partial class FreightManagementService
  {
    public FreightManagement.Business.TransportManifest LoadTransportManifest(Guid globalIdentifier)
    {
      try
      {
        return GetInstance<FreightManagement.Business.TransportManifest>(globalIdentifier);
      }
      catch
      {
        throw;
      }
    }

    public FreightManagement.Business.TransportManifest SaveTransportManifest(FreightManagement.Business.TransportManifest obj)
    {
      try
      {
        return Persist<FreightManagement.Business.TransportManifest>(obj);
      }
      catch
      {
        throw;
      }
    }

    public EnumTransportManifestDateFields SaveTransportManifestDates(Guid globalIdentifier, EnumTransportManifestDateFields whichDate, DateTime? date)
    {
      if (globalIdentifier == null) throw (new Exception("No GlobalIdentifier Specified."));
      TransportManifest tm = LoadTransportManifest(globalIdentifier);

      switch (whichDate)
      {
        case EnumTransportManifestDateFields.ActualLoadingTime:
          tm.ActualLoadingTime = date;
          break;
        case EnumTransportManifestDateFields.ActualLoadingCompleted:
          tm.ActualLoadingCompleted = date;
          break;
        case EnumTransportManifestDateFields.ActualOffloadingTime:
          tm.ActualOffloadingTime = date;
          break;
        case EnumTransportManifestDateFields.ActualOffloadingCompleted:
          tm.ActualOffloadingCompleted = date;
          break;
        case EnumTransportManifestDateFields.DepartureTime:
          tm.DepartureTime = date;
          break;
        case EnumTransportManifestDateFields.ArrivalTime:
          tm.ArrivalTime = date;
          break;
      }

      SaveTransportManifest(tm);
      return whichDate;
    }
  }
}
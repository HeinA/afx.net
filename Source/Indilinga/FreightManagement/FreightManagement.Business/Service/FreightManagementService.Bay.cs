﻿using Afx.Business.Collections;
using Afx.Business.Service;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.WCF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Business.Service
{
  public partial class FreightManagementService
  {
    public BasicCollection<FreightManagement.Business.Bay> LoadBayCollection()
    {
      try
      {
        return GetAllInstances<FreightManagement.Business.Bay>();
      }
      catch
      {
        throw;
      }
    }

    public BasicCollection<FreightManagement.Business.Bay> SaveBayCollection(BasicCollection<FreightManagement.Business.Bay> col)
    {
      try
      {
        return Persist<FreightManagement.Business.Bay>(col);
      }
      catch
      {
        throw;
      }
    }
  }
}
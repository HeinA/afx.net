﻿using Afx.Business.Collections;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Business.Service
{
  public partial interface IFreightManagementService
  {
    [OperationContract]
    BasicCollection<FreightManagement.Business.WaybillCostComponentType> LoadWaybillCostComponentTypeCollection();

    [OperationContract]
    BasicCollection<FreightManagement.Business.WaybillCostComponentType> SaveWaybillCostComponentTypeCollection(BasicCollection<FreightManagement.Business.WaybillCostComponentType> col);
  }
}


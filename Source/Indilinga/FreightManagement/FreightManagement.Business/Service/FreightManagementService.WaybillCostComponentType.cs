﻿using Afx.Business.Collections;
using Afx.Business.Service;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.WCF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Business.Service
{
  public partial class FreightManagementService
  {
    public BasicCollection<FreightManagement.Business.WaybillCostComponentType> LoadWaybillCostComponentTypeCollection()
    {
      try
      {
        return GetAllInstances<FreightManagement.Business.WaybillCostComponentType>();
      }
      catch
      {
        throw;
      }
    }

    public BasicCollection<FreightManagement.Business.WaybillCostComponentType> SaveWaybillCostComponentTypeCollection(BasicCollection<FreightManagement.Business.WaybillCostComponentType> col)
    {
      try
      {
        return Persist<FreightManagement.Business.WaybillCostComponentType>(col);
      }
      catch
      {
        throw;
      }
    }
  }
}
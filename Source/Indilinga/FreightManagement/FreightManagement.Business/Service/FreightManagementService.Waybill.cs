﻿using Afx.Business.Data;
using Afx.Business.Service;
using Afx.Business.ServiceModel;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.WCF;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Business.Service
{
  public partial class FreightManagementService
  {
    public FreightManagement.Business.Waybill LoadWaybill(Guid globalIdentifier)
    {
      Stopwatch sw = new Stopwatch();
      sw.Start();

      try
      {
        return GetInstance<FreightManagement.Business.Waybill>(globalIdentifier);
      }
      catch
      {
        throw;
      }
      finally
      {
        Debug.WriteLine("Waybill Load Milliseconds {0:#,##0}", sw.ElapsedMilliseconds);
      }
    }

    public FreightManagement.Business.Waybill SaveWaybill(FreightManagement.Business.Waybill obj)
    {
      try
      {
        //CacheExtension.RefreshCache = true; // TODO: Why????
        return Persist<FreightManagement.Business.Waybill>(obj);
      }
      catch
      {
        throw;
      }
    }
  }
}
﻿using AccountManagement.Business;
using Afx.Business;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Documents;
using Afx.Business.Service;
using ContactManagement.Business;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.WCF;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.Data;
using System.Data.Odbc;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace FreightManagement.Business.Service
{
  [ServiceBehavior(Namespace = Namespace.FreightManagement, ConcurrencyMode = ConcurrencyMode.Multiple)]
  [ExceptionShielding]
  [Export(typeof(IService))]
  public partial class FreightManagementService : ServiceBase, IFreightManagementService
  {
    public Waybill CostWaybill(Waybill waybill)
    {
      using (new RatesConfigurationCache())
      {
        CostingHelper.Cost(waybill);
      }
      return waybill;
    }

    #region void ImportAccounts(ExternalSystem es)

    public void ImportAccounts(ExternalSystem es)
    {
      try
      {
        using (new SqlScope("Import Accounts"))
        using (var ts = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
        using (new ConnectionScope())
        using (SqlConnection con = new SqlConnection(es.ConnectionString))
        {
          con.Open();
          DataSet dsAccounts = null;
          using (SqlCommand cmd = new SqlCommand("select D.id, D.Guid, D.DocumentNumber as AccountNumber, A.Name, A.EMail, A.DefaultAddress from IFMS_Account A inner join Document D on A.id=D.id", con))
          {
            dsAccounts = DataHelper.ExecuteDataSet(cmd);
          }

          foreach (DataRow dr in dsAccounts.Tables[0].Rows)
          {
            Guid guid = (Guid)dr["Guid"];
            int id = (int)dr["id"];
            int defaultAddress = dr["DefaultAddress"] == DBNull.Value ? 0 : (int)dr["DefaultAddress"];

            Account acc = GetInstance<Account>(guid);
            if (acc == null)
            {
              acc = new Account();
              acc.GlobalIdentifier = guid;
              acc.DocumentNumber = (string)dr["AccountNumber"];
            }

            acc.Name = (string)dr["Name"];

            DataSet dsAddresses = null;
            AddressType at = Cache.Instance.GetObjects<AddressType>().FirstOrDefault(at1 => at1.IsFlagged(AddressTypeFlags.ShipFromAddress));
            using (SqlCommand cmd = new SqlCommand("select A.id, A.Guid, Account,	Name, CompoundAddress, Postal, Physical, Residential, Business from IFMS_AccountAddress AA inner join [Address] A on AA.id=A.id where Account=@a", con))
            {
              cmd.Parameters.AddWithValue("@a", id);
              dsAddresses = DataHelper.ExecuteDataSet(cmd);
            }

            foreach (DataRow drAddress in dsAddresses.Tables[0].Rows)
            {
              Guid guidAddress = (Guid)drAddress["Guid"];
              Address a = acc.Addresses.FirstOrDefault(a1 => a1.GlobalIdentifier.Equals(guidAddress));
              if (a == null)
              {
                a = new Address();
                a.GlobalIdentifier = guidAddress;
                acc.Addresses.Add(a);
              }

              a.Name = (string)drAddress["Name"];
              string[] addresslines = ((string)drAddress["CompoundAddress"]).Split(',').Select(s => s.Trim()).ToArray();
              a.AddressText = string.Join("\r\n", addresslines);
              a.AddressType = at;

              if (defaultAddress == (int)drAddress["id"])
              {
                AccountExtension ae = acc.GetExtensionObject<AccountExtension>();
                ae.DefaultShipFromAddress = a;
              }
            }

            at = Cache.Instance.GetObjects<AddressType>().FirstOrDefault(at1 => at1.IsFlagged(AddressTypeFlags.ShipToAddress));
            using (SqlCommand cmd = new SqlCommand("select A.id, A.Guid, Account, DisplayName, Ad.CompoundAddress, Postal, Physical, Residential, Business from IFMS_AccountContact AC inner join Affiliate A on A.id = AC.id inner join [Address] Ad on Ad.id=A.DefaultAddress where Account=@a", con))
            {
              cmd.Parameters.AddWithValue("@a", id);
              dsAddresses = DataHelper.ExecuteDataSet(cmd);
            }

            foreach (DataRow drAddress in dsAddresses.Tables[0].Rows)
            {
              Guid guidAddress = (Guid)drAddress["Guid"];
              Address a = acc.Addresses.FirstOrDefault(a1 => a1.GlobalIdentifier.Equals(guidAddress));
              if (a == null)
              {
                a = new Address();
                a.GlobalIdentifier = guidAddress;
                acc.Addresses.Add(a);
              }

              a.Name = (string)drAddress["DisplayName"];
              string[] addresslines = ((string)drAddress["CompoundAddress"]).Split(',').Select(s => s.Trim()).ToArray();
              a.AddressText = string.Join("\r\n", addresslines);
              a.AddressType = at;
            }

            ObjectRepository<Account> repo = PersistanceManager.GetRepository<Account>();
            repo.Persist(acc);
            //Persist<Account>(acc);
          }

          ts.Complete();
        }
      }
      catch (Exception ex)
      {
        throw new ImportException("An error occured during the import.", ex);
      }
    }

    #endregion

    #region Guid GetStandardRatesConfiguration()

    public Guid GetStandardRatesConfiguration()
    {
      try
      {
        SearchResults sr = null;

        using (new ConnectionScope())
        using (new SqlScope("Search Rate Configuration"))
        {
          SearchDatagraph sg = new SearchDatagraph(typeof(RatesConfiguration))
            .Select(RatesConfiguration.IsStandardConfigurationProperty)
            .Where(RatesConfiguration.IsStandardConfigurationProperty, FilterType.Equals, true, true);
          sr = ExecuteSearch(sg);
        }

        if (sr.Results.Length == 0)
        {
          return Guid.Empty;
        }
        else
        {
          return sr.Results[0].GlobalIdentifier;
        }
      }
      catch
      {
        throw;
      }
    }

    #endregion

    #region RatesConfiguration LoadStandardRatesConfiguration()

    public RatesConfiguration LoadStandardRatesConfiguration()
    {
      Guid guid = GetStandardRatesConfiguration();
      if (guid == Guid.Empty)
      {
        RatesConfiguration rc = new RatesConfiguration();
        rc.Revisions.Add(new RatesConfigurationRevision());
        rc.IsStandardConfiguration = true;
        rc.Name = "Standard Rates";
        return rc;
      }
      else
      {
        RatesConfiguration rc = GetInstance<RatesConfiguration>(guid);
        return rc;
      }
    }

    #endregion

    const string DistanceTableGuidString = "{5C3012A4-7906-4746-8E02-801FA964D222}";

    public DistanceTable LoadDistanceTable()
    {
      Guid guid = Guid.Parse(DistanceTableGuidString);
      DistanceTable dt = GetInstance<DistanceTable>(guid);
      if (dt == null)
      {
        dt = new DistanceTable();
        dt.GlobalIdentifier = guid;
      }
      return dt;
    }

    public DistanceTable SaveDistanceTable(DistanceTable table)
    {
      try
      {
        return Persist<DistanceTable>(table);
      }
      catch
      {
        throw;
      }
    }

    #region WaybillReference LoadWaybillReferenceByDocumentNumber(string documentNumber)

    public WaybillReference LoadWaybillReferenceByDocumentNumber(string documentNumber)
    {
      try
      {
        SearchResults sr = null;

        using (new ConnectionScope())
        using (new SqlScope("Search Waybill Reference"))
        {
          SearchDatagraph sg = new SearchDatagraph(typeof(WaybillReference))
            .Select(WaybillReference.WaybillNumberProperty)
            .Where(WaybillReference.WaybillNumberProperty, FilterType.Equals, documentNumber, true);
          sr = ExecuteSearch(sg);
        }

        if (sr.Results.Length == 0)
        {
          return null;
        }
        else
        {
          return GetInstance<WaybillReference>(sr.Results[0].GlobalIdentifier);
        }
      }
      catch
      {
        throw;
      }
    }

    #endregion

    #region WaybillReference LoadWaybillReferenceByParcelNumber(string parcelNumber)

    public WaybillReference LoadWaybillReferenceByParcelNumber(string parcelNumber)
    {
      try
      {
        using (new ConnectionScope())
        using (new SqlScope("Search Waybill Reference"))
        {
          IDbCommand cmdUpdate = PersistanceManager.GetCommand();
          cmdUpdate.CommandText = string.Format("select distinct D.GlobalIdentifier from FreightManagement.WaybillParcel P inner join Afx.Document D on P.Waybill=D.id where P.Barcode={0}", PersistanceManager.AddParameter(1, parcelNumber, cmdUpdate));
          object oid = cmdUpdate.ExecuteScalar();
          if (oid == null || oid == DBNull.Value) return null;

          return GetInstance<FreightManagement.Business.WaybillReference>((Guid)oid);
        }
      }
      catch
      {
        throw;
      }
    }

    #endregion

    #region WaybillReference LoadWaybillReference(Guid documentGuid)

    public WaybillReference LoadWaybillReference(Guid documentGuid)
    {
      try
      {
        return GetInstance<WaybillReference>(documentGuid);
      }
      catch
      {
        throw;
      }
    }

    #endregion

    #region BasicCollection<WaybillCollection> LoadWaybillCollections(int dc)

    public BasicCollection<WaybillCollection> LoadWaybillCollections(int dc)
    {
      try
      {
        Collection<IDataFilter> filters = new Collection<IDataFilter>();
        filters.Add(new DataFilter<WaybillCollection>(WaybillCollection.CollectionDCProperty, FilterType.Equals, dc));
        return GetInstances<WaybillCollection>(filters);
      }
      catch
      {
        throw;
      }
    }

    #endregion

    #region BasicCollection<WaybillFloorStatus> LoadWaybillFloorStatus(int dc)

    public BasicCollection<WaybillFloorStatus> LoadWaybillFloorStatus(int dc)
    {
      try
      {
        Collection<IDataFilter> filters = new Collection<IDataFilter>();
        filters.Add(new DataFilter<WaybillFloorStatus>(WaybillFloorStatus.DistributionCenterProperty, FilterType.Equals, dc));
        return GetInstances<WaybillFloorStatus>(filters);
      }
      catch
      {
        throw;
      }
    }

    #endregion

    #region BasicCollection<WaybillFloorStatus> LoadWaybillFloorStatusFiltered(int dc, int route, int account, DateTime fromDate, DateTime toDate)

    public BasicCollection<WaybillFloorStatus> LoadWaybillFloorStatusFiltered(int dc, int route, int account, DateTime fromDate, DateTime toDate)
    {
      try
      {
        Collection<IDataFilter> filters = new Collection<IDataFilter>();
        filters.Add(new DataFilter<WaybillFloorStatus>(WaybillFloorStatus.DistributionCenterProperty, FilterType.Equals, dc));
        if (route != 0) filters.Add(new DataFilter<WaybillFloorStatus>(WaybillFloorStatus.RouteProperty, FilterType.Equals, route));
        if (account != 0) filters.Add(new DataFilter<WaybillFloorStatus>(WaybillFloorStatus.AccountProperty, FilterType.Equals, account));
        filters.Add(new DataFilter<WaybillFloorStatus>(WaybillFloorStatus.WaybillDateProperty, FilterType.GreaterThanEqual, fromDate));
        filters.Add(new DataFilter<WaybillFloorStatus>(WaybillFloorStatus.WaybillDateProperty, FilterType.LessThanEqual, toDate));
        //filters.Add(new DataFilter<WaybillReference>(WaybillReference.WaybillStateProperty, FilterType.NotEquals, Waybill.States.Cancelled));
        return GetInstances<WaybillFloorStatus>(filters);
      }
      catch
      {
        throw;
      }
    }

    #endregion

    #region BasicCollection<WaybillReference> LoadWaybillsForRoute(DistributionCenter distributionCenter, Route route)

    public BasicCollection<WaybillReference> LoadWaybillsForRoute(DistributionCenter distributionCenter, Route route)
    {
      if (BusinessObject.IsNull(distributionCenter)) throw new ArgumentNullException("distributionCenter");

      try
      {
        SearchResults sr = null;

        if (route == null) route = new Route(true);

        using (new ConnectionScope())
        using (new SqlScope("Search Waybill Reference for route"))
        {
          SearchDatagraph sg = new SearchDatagraph(typeof(WaybillReference))
            .Select(WaybillReference.DistributionCenterProperty)
            .Select(WaybillReference.RouteProperty)
            .Where(WaybillReference.DistributionCenterProperty, FilterType.Equals, distributionCenter.Id, true)
            .Where(WaybillReference.RouteProperty, FilterType.Equals, route.Id, !BusinessObject.IsNull(route));
          sr = ExecuteSearch(sg);
        }

        return GetInstances<WaybillReference>(GetIdentifierCollection(sr));
      }
      catch
      {
        throw;
      }
    }

    #endregion

    #region BasicCollection<DeliveryManifestReference> LoadDeliveryManifestsForRoute(Route route)

    public BasicCollection<DeliveryManifestReference> LoadDeliveryManifestsForRoute(Route route)
    {
      if (BusinessObject.IsNull(route)) throw new ArgumentNullException("route");

      try
      {
        SearchResults sr = null;

        if (route == null) route = new Route(true);

        using (new ConnectionScope())
        using (new SqlScope("Search Delivery Manifest Reference for route"))
        {
          SearchDatagraph sg = new SearchDatagraph(typeof(DeliveryManifestReference))
            .Select(DeliveryManifestReference.RouteProperty)
            .Select(DeliveryManifestReference.TripsheetCountProperty)
            .Join(DeliveryManifestReference.ManifestStateProperty)
              .Select(DocumentTypeState.GlobalIdentifierProperty, "ManifestState")
              .Where(DocumentTypeState.GlobalIdentifierProperty, FilterType.Equals, Guid.Parse(DeliveryManifest.States.Verified))
              .EndJoin()
            .Where(DeliveryManifestReference.RouteProperty, FilterType.Equals, route.Id, true)
            .Where(DeliveryManifestReference.TripsheetCountProperty, FilterType.Equals, 0, true);
          sr = ExecuteSearch(sg);
        }

        return GetInstances<DeliveryManifestReference>(GetIdentifierCollection(sr));
      }
      catch
      {
        throw;
      }
    }

    #endregion

    #region BasicCollection<CollectionManifestReference> LoadCollectionManifestsForRoute(Route route)

    public BasicCollection<CollectionManifestReference> LoadCollectionManifestsForRoute(Route route)
    {
      if (BusinessObject.IsNull(route)) throw new ArgumentNullException("route");

      try
      {
        SearchResults sr = null;

        if (route == null) route = new Route(true);

        using (new ConnectionScope())
        using (new SqlScope("Search Delivery Manifest Reference for route"))
        {
          SearchDatagraph sg = new SearchDatagraph(typeof(CollectionManifestReference))
            .Select(CollectionManifestReference.RouteProperty)
            .Select(CollectionManifestReference.TripsheetCountProperty)
            .Join(CollectionManifestReference.ManifestStateProperty)
              .Select(CollectionManifestReference.GlobalIdentifierProperty, "ManifestState")
              .Where(CollectionManifestReference.GlobalIdentifierProperty, FilterType.Equals, Guid.Parse(CollectionManifest.States.Verified))
              .EndJoin()
            .Where(CollectionManifestReference.RouteProperty, FilterType.Equals, route.Id, true)
            .Where(CollectionManifestReference.TripsheetCountProperty, FilterType.Equals, 0, true);
          sr = ExecuteSearch(sg);
        }

        return GetInstances<CollectionManifestReference>(GetIdentifierCollection(sr));
      }
      catch
      {
        throw;
      }
    }

    #endregion

    #region BasicCollection<TransferManifestReference> LoadTransferManifestsForRoute(Route route)

    public BasicCollection<TransferManifestReference> LoadTransferManifestsForRoute(Route route)
    {
      if (BusinessObject.IsNull(route)) throw new ArgumentNullException("route");

      try
      {
        SearchResults sr = null;

        if (route == null) route = new Route(true);

        using (new ConnectionScope())
        using (new SqlScope("Search Delivery Manifest Reference for route"))
        {
          SearchDatagraph sg = new SearchDatagraph(typeof(TransferManifestReference))
            .Select(TransferManifestReference.RouteProperty)
            .Select(TransferManifestReference.TripsheetCountProperty)
            .Join(TransferManifestReference.ManifestStateProperty)
              .Select(TransferManifestReference.GlobalIdentifierProperty, "ManifestState")
              .Where(TransferManifestReference.GlobalIdentifierProperty, FilterType.Equals, Guid.Parse(TransferManifest.States.Verified))
              .EndJoin()
            .Where(TransferManifestReference.RouteProperty, FilterType.Equals, route.Id, true)
            .Where(TransferManifestReference.TripsheetCountProperty, FilterType.Equals, 0, true);
          sr = ExecuteSearch(sg);
        }

        return GetInstances<TransferManifestReference>(GetIdentifierCollection(sr));
      }
      catch
      {
        throw;
      }
    }

    #endregion

    #region BasicCollection<WaybillReference> LoadWaybillsForAccount(AccountReference account)

    public BasicCollection<WaybillReference> LoadWaybillsForAccount(AccountReference account)
    {
      //if (BusinessObject.IsNull(account)) throw new ArgumentNullException("account");
      if (account == null) account = new AccountReference(true);

      try
      {
        SearchResults sr = null;

        using (new ConnectionScope())
        using (new SqlScope("Search Waybill Reference for account"))
        {
          SearchDatagraph sg = new SearchDatagraph(typeof(WaybillReference))
            .Select(WaybillReference.AccountProperty)
            .Select(WaybillReference.PODHandoverCountProperty)
            .Join(WaybillReference.WaybillStateProperty)
              .Select(DocumentTypeState.GlobalIdentifierProperty, "WaybillState")
              .Where(DocumentTypeState.GlobalIdentifierProperty, FilterType.In, new Object[] { Waybill.States.PODReceived, Waybill.States.Invoiced }, true)
              .EndJoin()
            .Where(WaybillReference.AccountProperty, FilterType.Equals, account.Id, !BusinessObject.IsNull(account))
            .Where(WaybillReference.AccountProperty, FilterType.IsNotNull, true, true)
            .Where(WaybillReference.PODHandoverCountProperty, FilterType.Equals, 0, true);
          sr = ExecuteSearch(sg);
        }

        if (sr.Results.Length == 0)
        {
          return null;
        }
        else
        {
          return GetInstances<WaybillReference>(GetIdentifierCollection(sr));
        }
      }
      catch
      {
        throw;
      }
    }

    #endregion

    #region PODHandoverReference LoadPODHandoverReferenceByDocumentNumber(string documentNumber)

    public PODHandoverReference LoadPODHandoverReferenceByDocumentNumber(string documentNumber)
    {
      try
      {
        SearchResults sr = null;

        using (new ConnectionScope())
        using (new SqlScope("Search POD Handover Reference"))
        {
          SearchDatagraph sg = new SearchDatagraph(typeof(PODHandoverReference))
            .Select(PODHandoverReference.PODHandoverNumberProperty)
            .Where(PODHandoverReference.PODHandoverNumberProperty, FilterType.Equals, documentNumber, true);
          sr = ExecuteSearch(sg);
        }

        if (sr.Results.Length == 0)
        {
          return null;
        }
        else
        {
          return GetInstance<PODHandoverReference>(sr.Results[0].GlobalIdentifier);
        }
      }
      catch
      {
        throw;
      }
    }

    #endregion

    #region PODHandoverReference LoadPODHandoverReferenceByDocumentNumber(string documentNumber)

    public PODHandoverReference LoadPODHandoverReferenceByIdentifier(Guid id)
    {
      try
      {
        return GetInstance<PODHandoverReference>(id);
      }
      catch
      {
        throw;
      }
    }

    #endregion

    public ManifestReference LoadManifestReferenceByDocumentNumber(string documentNumber)
    {
      try
      {
        SearchResults sr = null;

        using (new ConnectionScope())
        using (new SqlScope("Search Manifest Reference"))
        {
          SearchDatagraph sg = new SearchDatagraph(typeof(ManifestReference))
            .Select(ManifestReference.ManifestNumberProperty)
            .Where(ManifestReference.ManifestNumberProperty, FilterType.Equals, documentNumber, true);
          sr = ExecuteSearch(sg);
        }

        if (sr.Results.Length == 0)
        {
          return null;
        }
        else
        {
          return GetInstance<ManifestReference>(sr.Results[0].GlobalIdentifier);
        }
      }
      catch
      {
        throw;
      }
    }

    public ManifestReference LoadManifestReference(Guid documentGuid)
    {
      try
      {
        return GetInstance<ManifestReference>(documentGuid);
      }
      catch
      {
        throw;
      }
    }

    public BasicCollection<WaybillReference> LoadOutstandingPODs()
    {
      try
      {
        Collection<IDataFilter> filters = new Collection<IDataFilter>();
        filters.Add(new DataFilter<WaybillReference>(WaybillReference.WaybillStateProperty, FilterType.Equals, Cache.Instance.GetObject<DocumentTypeState>(Guid.Parse(Waybill.States.InTransit)).Id));
        BasicCollection<WaybillReference> results1 = GetInstances<WaybillReference>(filters);

        filters = new Collection<IDataFilter>();

        filters.Add(new DataFilter<WaybillReference>(WaybillReference.WaybillStateProperty, FilterType.Equals, Cache.Instance.GetObject<DocumentTypeState>(Guid.Parse(Waybill.States.Delivered)).Id));
        BasicCollection<WaybillReference> results2 = GetInstances<WaybillReference>(filters);

        filters = new Collection<IDataFilter>();

        filters.Add(new DataFilter<WaybillReference>(WaybillReference.WaybillStateProperty, FilterType.Equals, Cache.Instance.GetObject<DocumentTypeState>(Guid.Parse(Waybill.States.OnDelivery)).Id));
        BasicCollection<WaybillReference> results3 = GetInstances<WaybillReference>(filters);

        return new BasicCollection<WaybillReference>(results1.Union(results2.Union(results3)));
      }
      catch
      {
        throw;
      }
    }

    public BasicCollection<WaybillReference> LoadOutstandingInvoices()
    {
      try
      {
        Collection<IDataFilter> filters = new Collection<IDataFilter>();
        filters.Add(new DataFilter<WaybillReference>(WaybillReference.PODSignatureProperty, FilterType.NotEquals, string.Empty));
        filters.Add(new DataFilter<WaybillReference>(WaybillReference.InvoiceNumberProperty, FilterType.Equals, string.Empty));
        BasicCollection<WaybillReference> results1 = GetInstances<WaybillReference>(filters);

        return results1;
      }
      catch
      {
        throw;
      }
    }

    public BasicCollection<WaybillReference> LoadWaybills(int dc, DateTime fromDate, DateTime toDate)
    {
      try
      {
        List<WaybillReference> list = new List<WaybillReference>();
        Collection<IDataFilter> filters = new Collection<IDataFilter>();
        if (dc != 0) filters.Add(new DataFilter<WaybillReference>(WaybillReference.DistributionCenterProperty, FilterType.Equals, dc));
        filters.Add(new DataFilter<WaybillReference>(WaybillReference.WaybillDateProperty, FilterType.GreaterThanEqual, fromDate));
        filters.Add(new DataFilter<WaybillReference>(WaybillReference.WaybillDateProperty, FilterType.LessThanEqual, toDate));

        return GetInstances<WaybillReference>(filters);
      }
      catch
      {
        throw;
      }
    }

    public void ReCostWaybills(Guid account, DateTime startDate, DateTime endDate)
    {
      try
      {
        using (new ConnectionScope())
        using (new SqlScope("ReCostWaybills"))
        {
          using (SqlCommand cmd = (SqlCommand)PersistanceManager.GetCommand())
          {
            if (account == Guid.Empty)
            {
              cmd.CommandText = "select W.id from [FreightManagement].[Waybill] W inner join Afx.Document D on W.id=D.id where D.DocumentDate>=@sd and D.DocumentDate<=@ed";
            }
            else
            {
              cmd.CommandText = "select W.id from [FreightManagement].[Waybill] W inner join [Afx].[Document] A on W.Account=A.id inner join Afx.Document D on W.id=D.id where A.GlobalIdentifier=@guid and D.DocumentDate>=@sd and D.DocumentDate<=@ed";
            }
            cmd.Parameters.AddWithValue("@guid", account);
            cmd.Parameters.AddWithValue("@sd", startDate.Date);
            cmd.Parameters.AddWithValue("@ed", endDate.Date);
            DataSet ds = DataHelper.ExecuteDataSet(cmd);
            Debug.WriteLine(ds.Tables[0].Rows.Count);
            using (new RatesConfigurationCache())
            {
              foreach (DataRow dr in ds.Tables[0].Rows)
              {
                try
                {
                  ObjectRepository<Waybill> repo = PersistanceManager.GetRepository<Waybill>();
                  var wb = repo.GetInstance((int)dr[0]);

                  var oldCharge = wb.Charge;

                  CostingHelper.Cost(wb);

                  if (oldCharge != wb.Charge)
                  {
                    using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
                    {
                      repo.Persist(wb);
                      Replicate();
                      scope.Complete();
                    }
                  }
                }
                catch (MessageException)
                {
                  throw;
                }
              }
            }
          }
        }
      }
      catch
      {
        throw;
      }
    }

    public byte[] GetInvoicedWaybillExport(DateTime? startDate, DateTime? endDate, string account, string consignee, string invoiceNumber)
    {
      using (var con = new ConnectionScope())
      {
        using (var cmd = (SqlCommand)PersistanceManager.GetCommand())
        {
          cmd.CommandText = "[FreightManagement].[rptInvoicedWaybills]";
          cmd.CommandType = CommandType.StoredProcedure;
          cmd.Parameters.AddWithValue("@StartDate", startDate);
          cmd.Parameters.AddWithValue("@EndDate", endDate);
          cmd.Parameters.AddWithValue("@Account", account ?? string.Empty);
          cmd.Parameters.AddWithValue("@Consignee", consignee ?? string.Empty);
          cmd.Parameters.AddWithValue("@InvoiceNumber", invoiceNumber ?? string.Empty);

          return DataHelper.ExecuteExcel(cmd);
        }
      }
    }

    public byte[] GetDeliveredWaybillExport(DateTime? startDate, DateTime? endDate, string account, string consignee)
    {
      using (var con = new ConnectionScope())
      {
        using (var cmd = (SqlCommand)PersistanceManager.GetCommand())
        {
          cmd.CommandText = "[FreightManagement].[rptLeadTimes]";
          cmd.CommandType = CommandType.StoredProcedure;
          cmd.Parameters.AddWithValue("@StartDate", startDate);
          cmd.Parameters.AddWithValue("@EndDate", endDate);
          cmd.Parameters.AddWithValue("@Account", account ?? string.Empty);
          cmd.Parameters.AddWithValue("@Consignee", consignee ?? string.Empty);

          return DataHelper.ExecuteExcel(cmd);
        }
      }
    }

    public byte[] GetWaybillManifestExport(DateTime startDate, DateTime endDate)
    {
      using (var con = new ConnectionScope())
      {
        using (var cmd = (SqlCommand)PersistanceManager.GetCommand())
        {
          cmd.CommandText = "SELECT * FROM [FreightManagement].[vrptWaybillManifest] WHERE [Manifest Date] >= @StartDate AND [Manifest Date] < @EndDate";
          cmd.CommandType = CommandType.Text;
          cmd.Parameters.AddWithValue("@StartDate", startDate.Date);
          cmd.Parameters.AddWithValue("@EndDate", endDate.Date);

          return DataHelper.ExecuteExcel(cmd);
        }
      }
    }

    public byte[] GetOutstandingPodExport(DateTime? startDate, DateTime? endDate, string account, string consignee)
    {
      using (var con = new ConnectionScope())
      {
        using (var cmd = (SqlCommand)PersistanceManager.GetCommand())
        {
          cmd.CommandText = "[FreightManagement].[rptOutstandingPODManagement]";
          cmd.CommandType = CommandType.StoredProcedure;
          cmd.Parameters.AddWithValue("@StartDate", startDate);
          cmd.Parameters.AddWithValue("@EndDate", endDate);
          cmd.Parameters.AddWithValue("@Account", account ?? string.Empty);
          cmd.Parameters.AddWithValue("@Consignee", consignee ?? string.Empty);

          return DataHelper.ExecuteExcel(cmd);
        }
      }
    }

    public BasicCollection<WaybillInvoiceReference> LoadReadyForInvoicing(Guid account)
    {
      try
      {
        SearchResults sr = null;

        using (new ConnectionScope())
        {
          SearchDatagraph sg = new SearchDatagraph(typeof(WaybillInvoiceReference))
            .Join(WaybillInvoiceReference.AccountProperty)
              .Where(AccountReference.GlobalIdentifierProperty, FilterType.Equals, account)
              .EndJoin()
            .Join(WaybillInvoiceReference.WaybillStateProperty)
              .Where(DocumentTypeState.GlobalIdentifierProperty, FilterType.Equals, Waybill.States.PendingInvoice)
              .EndJoin();
          sr = ExecuteSearch(sg);
        }

        return GetInstances<WaybillInvoiceReference>(GetIdentifierCollection(sr));
      }
      catch
      {
        throw;
      }
    }

    public void InvoiceWaybills(BasicCollection<Guid> ids, DateTime invoiceDate, string invoiceNumber)
    {
      try
      {
        var waybills = GetInstances<Waybill>(ids);

        using (new ConnectionScope())
        using (var scope = new TransactionScope(TransactionScopeOption.Required))
        {
          foreach (var w in waybills)
          {
            w.InvoiceDate = invoiceDate;
            w.InvoiceNumber = invoiceNumber;
            DocumentHelper.SetState(w, Waybill.States.Invoiced);
          }

          scope.Complete();
        }
      }
      catch
      {
        throw;
      }
    }
  }
}

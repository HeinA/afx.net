﻿using Afx.Business.Collections;
using Afx.Business.Service;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.WCF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Business.Service
{
  public partial class FreightManagementService
  {
    public BasicCollection<FreightManagement.Business.BorderPosts> LoadBorderPostsCollection()
    {
      try
      {
        return GetAllInstances<FreightManagement.Business.BorderPosts>();
      }
      catch
      {
        throw;
      }
    }

    public BasicCollection<FreightManagement.Business.BorderPosts> SaveBorderPostsCollection(BasicCollection<FreightManagement.Business.BorderPosts> col)
    {
      try
      {
        return Persist<FreightManagement.Business.BorderPosts>(col);
      }
      catch
      {
        throw;
      }
    }
  }
}
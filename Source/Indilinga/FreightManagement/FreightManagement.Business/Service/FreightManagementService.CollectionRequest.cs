﻿using Afx.Business.Service;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.WCF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Business.Service
{
  public partial class FreightManagementService
  {
    public FreightManagement.Business.CollectionRequest LoadCollectionRequest(Guid globalIdentifier)
    {
      try
      {
        return GetInstance<FreightManagement.Business.CollectionRequest>(globalIdentifier);
      }
      catch
      {
        throw;
      }
    }

    public FreightManagement.Business.CollectionRequest SaveCollectionRequest(FreightManagement.Business.CollectionRequest obj)
    {
      try
      {
        return Persist<FreightManagement.Business.CollectionRequest>(obj);
      }
      catch
      {
        throw;
      }
    }
  }
}
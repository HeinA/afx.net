﻿using Afx.Business.Service;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.WCF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Business.Service
{
  public partial class FreightManagementService
  {
    public FreightManagement.Business.PODHandover LoadPODHandover(Guid globalIdentifier)
    {
      try
      {
        return GetInstance<FreightManagement.Business.PODHandover>(globalIdentifier);
      }
      catch
      {
        throw;
      }
    }

    public FreightManagement.Business.PODHandover SavePODHandover(FreightManagement.Business.PODHandover obj)
    {
      try
      {
        return Persist<FreightManagement.Business.PODHandover>(obj);
      }
      catch
      {
        throw;
      }
    }
  }
}
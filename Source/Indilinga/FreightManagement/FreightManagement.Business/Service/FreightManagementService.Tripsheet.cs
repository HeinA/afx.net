﻿using Afx.Business.Service;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.WCF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Business.Service
{
  public partial class FreightManagementService
  {
    public FreightManagement.Business.Tripsheet LoadTripsheet(Guid globalIdentifier)
    {
      try
      {
        return GetInstance<FreightManagement.Business.Tripsheet>(globalIdentifier);
      }
      catch
      {
        throw;
      }
    }

    public FreightManagement.Business.Tripsheet SaveTripsheet(FreightManagement.Business.Tripsheet obj)
    {
      try
      {
        return Persist<FreightManagement.Business.Tripsheet>(obj);
      }
      catch
      {
        throw;
      }
    }
  }
}
﻿using Afx.Business.Service;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.WCF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Business.Service
{
  public partial class FreightManagementService
  {
    public FreightManagement.Business.DeliveryManifest LoadDeliveryManifest(Guid globalIdentifier)
    {
      try
      {
        return GetInstance<FreightManagement.Business.DeliveryManifest>(globalIdentifier);
      }
      catch
      {
        throw;
      }
    }

    public FreightManagement.Business.DeliveryManifest SaveDeliveryManifest(FreightManagement.Business.DeliveryManifest obj)
    {
      try
      {
        return Persist<FreightManagement.Business.DeliveryManifest>(obj);
      }
      catch
      {
        throw;
      }
    }
  }
}
﻿using Afx.Business.Service;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.WCF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Business.Service
{
  public partial class FreightManagementService
  {
    public FreightManagement.Business.WaybillReceipt LoadWaybillReceipt(Guid globalIdentifier)
    {
      try
      {
        return GetInstance<FreightManagement.Business.WaybillReceipt>(globalIdentifier);
      }
      catch
      {
        throw;
      }
    }

    public FreightManagement.Business.WaybillReceipt SaveWaybillReceipt(FreightManagement.Business.WaybillReceipt obj)
    {
      try
      {
        return Persist<FreightManagement.Business.WaybillReceipt>(obj);
      }
      catch
      {
        throw;
      }
    }
  }
}
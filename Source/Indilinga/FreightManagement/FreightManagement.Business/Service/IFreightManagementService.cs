﻿using AccountManagement.Business;
using Afx.Business;
using Afx.Business.Collections;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Business.Service
{
  [ServiceContract(Namespace = Namespace.FreightManagement)]
  public partial interface IFreightManagementService : IDisposable
  {
    [OperationContract]
    void ImportAccounts(ExternalSystem es);

    [OperationContract]
    Guid GetStandardRatesConfiguration();

    [OperationContract]
    RatesConfiguration LoadStandardRatesConfiguration();

    [OperationContract]
    DistanceTable LoadDistanceTable();

    [OperationContract]
    DistanceTable SaveDistanceTable(DistanceTable table);

    [OperationContract]
    WaybillReference LoadWaybillReferenceByDocumentNumber(string documentNumber);

    [OperationContract]
    WaybillReference LoadWaybillReferenceByParcelNumber(string parcelNumber);

    [OperationContract]
    WaybillReference LoadWaybillReference(Guid documentGuid);

    [OperationContract]
    Waybill CostWaybill(Waybill waybill);

    [OperationContract]
    ManifestReference LoadManifestReferenceByDocumentNumber(string documentNumber);

    [OperationContract]
    ManifestReference LoadManifestReference(Guid documentGuid);

    [OperationContract]
    BasicCollection<WaybillCollection> LoadWaybillCollections(int dc);

    [OperationContract]
    BasicCollection<WaybillInvoiceReference> LoadReadyForInvoicing(Guid account);

    [OperationContract]
    void InvoiceWaybills(BasicCollection<Guid> ids, DateTime invoiceDate, string invoiceNumber);

    [OperationContract]
    BasicCollection<WaybillFloorStatus> LoadWaybillFloorStatus(int dc);

    [OperationContract]
    BasicCollection<WaybillFloorStatus> LoadWaybillFloorStatusFiltered(int dc, int route, int account, DateTime fromDate, DateTime toDate);

    [OperationContract]
    BasicCollection<WaybillReference> LoadWaybillsForRoute(DistributionCenter distributionCenter, Route route);

    [OperationContract]
    BasicCollection<WaybillReference> LoadWaybillsForAccount(AccountReference account);

    [OperationContract]
    BasicCollection<DeliveryManifestReference> LoadDeliveryManifestsForRoute(Route route);

    [OperationContract]
    BasicCollection<CollectionManifestReference> LoadCollectionManifestsForRoute(Route route);

    [OperationContract]
    BasicCollection<TransferManifestReference> LoadTransferManifestsForRoute(Route route);


    [OperationContract]
    PODHandoverReference LoadPODHandoverReferenceByDocumentNumber(string documentNumber);

    [OperationContract]
    PODHandoverReference LoadPODHandoverReferenceByIdentifier(Guid id);

    [OperationContract]
    BasicCollection<WaybillReference> LoadOutstandingPODs();

    [OperationContract]
    BasicCollection<WaybillReference> LoadOutstandingInvoices();

    [OperationContract]
    BasicCollection<WaybillReference> LoadWaybills(int dc, DateTime fromDate, DateTime toDate);
    [OperationContract] //(IsOneWay = true)
    void ReCostWaybills(Guid account, DateTime startDate, DateTime endDate);

    [OperationContract]
    byte[] GetInvoicedWaybillExport(DateTime? startDate, DateTime? endDate, string account, string consignee, string invoiceNumber);

    [OperationContract]
    byte[] GetDeliveredWaybillExport(DateTime? startDate, DateTime? endDate, string account, string consignee);

    [OperationContract]
    byte[] GetOutstandingPodExport(DateTime? startDate, DateTime? endDate, string account, string consignee);


    [OperationContract]
    byte[] GetWaybillManifestExport(DateTime startDate, DateTime endDate);

  }
}

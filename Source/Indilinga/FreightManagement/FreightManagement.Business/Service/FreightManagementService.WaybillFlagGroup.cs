﻿using Afx.Business.Collections;
using Afx.Business.Service;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.WCF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Business.Service
{
  public partial class FreightManagementService
  {
    public BasicCollection<FreightManagement.Business.WaybillFlagGroup> LoadWaybillFlagGroupCollection()
    {
      try
      {
        return GetAllInstances<FreightManagement.Business.WaybillFlagGroup>();
      }
      catch
      {
        throw;
      }
    }

    public BasicCollection<FreightManagement.Business.WaybillFlagGroup> SaveWaybillFlagGroupCollection(BasicCollection<FreightManagement.Business.WaybillFlagGroup> col)
    {
      try
      {
        return Persist<FreightManagement.Business.WaybillFlagGroup>(col);
      }
      catch
      {
        throw;
      }
    }
  }
}
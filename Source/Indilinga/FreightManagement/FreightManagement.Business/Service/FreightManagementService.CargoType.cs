﻿using Afx.Business.Collections;
using Afx.Business.Service;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.WCF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Business.Service
{
  public partial class FreightManagementService
  {
    public BasicCollection<FreightManagement.Business.CargoType> LoadCargoTypeCollection()
    {
      try
      {
        return GetAllInstances<FreightManagement.Business.CargoType>();
      }
      catch
      {
        throw;
      }
    }

    public BasicCollection<FreightManagement.Business.CargoType> SaveCargoTypeCollection(BasicCollection<FreightManagement.Business.CargoType> col)
    {
      try
      {
        return Persist<FreightManagement.Business.CargoType>(col);
      }
      catch
      {
        throw;
      }
    }
  }
}
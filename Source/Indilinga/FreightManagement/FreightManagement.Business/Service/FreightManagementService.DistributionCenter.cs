﻿using Afx.Business.Collections;
using Afx.Business.Service;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.WCF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Business.Service
{
  public partial class FreightManagementService
  {
    public BasicCollection<FreightManagement.Business.DistributionCenter> LoadDistributionCenterCollection()
    {
      try
      {
        return GetAllInstances<FreightManagement.Business.DistributionCenter>();
      }
      catch
      {
        throw;
      }
    }

    public BasicCollection<FreightManagement.Business.DistributionCenter> SaveDistributionCenterCollection(BasicCollection<FreightManagement.Business.DistributionCenter> col)
    {
      try
      {
        return Persist<FreightManagement.Business.DistributionCenter>(col);
      }
      catch
      {
        throw;
      }
    }
  }
}
﻿using Afx.Business.Service;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.WCF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Business.Service
{
  public partial class FreightManagementService
  {
    public FreightManagement.Business.CollectionManifest LoadCollectionManifest(Guid globalIdentifier)
    {
      try
      {
        return GetInstance<FreightManagement.Business.CollectionManifest>(globalIdentifier);
      }
      catch
      {
        throw;
      }
    }

    public FreightManagement.Business.CollectionManifest SaveCollectionManifest(FreightManagement.Business.CollectionManifest obj)
    {
      try
      {
        return Persist<FreightManagement.Business.CollectionManifest>(obj);
      }
      catch
      {
        throw;
      }
    }
  }
}
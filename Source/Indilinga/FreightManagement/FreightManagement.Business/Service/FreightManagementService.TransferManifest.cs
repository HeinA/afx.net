﻿using Afx.Business.Service;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.WCF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Business.Service
{
  public partial class FreightManagementService
  {
    public FreightManagement.Business.TransferManifest LoadTransferManifest(Guid globalIdentifier)
    {
      try
      {
        return GetInstance<FreightManagement.Business.TransferManifest>(globalIdentifier);
      }
      catch
      {
        throw;
      }
    }

    public FreightManagement.Business.TransferManifest SaveTransferManifest(FreightManagement.Business.TransferManifest obj)
    {
      try
      {
        return Persist<FreightManagement.Business.TransferManifest>(obj);
      }
      catch
      {
        throw;
      }
    }
  }
}
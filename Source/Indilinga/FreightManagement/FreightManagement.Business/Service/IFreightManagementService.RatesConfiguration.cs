﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Business.Service
{
  public partial interface IFreightManagementService
  {
    [OperationContract]
    FreightManagement.Business.RatesConfiguration LoadRatesConfiguration(Guid globalIdentifier);

    [OperationContract]
    FreightManagement.Business.RatesConfiguration SaveRatesConfiguration(FreightManagement.Business.RatesConfiguration obj);
  }
}

﻿using Afx.Business.Service;
using Afx.Business.ServiceModel;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.WCF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Business.Service
{
  public partial class FreightManagementService
  {
    public FreightManagement.Business.RatesConfiguration LoadRatesConfiguration(Guid globalIdentifier)
    {
      try
      {
        return GetInstance<FreightManagement.Business.RatesConfiguration>(globalIdentifier);
      }
      catch
      {
        throw;
      }
    }

    public FreightManagement.Business.RatesConfiguration SaveRatesConfiguration(FreightManagement.Business.RatesConfiguration obj)
    {
      try
      {
        CacheExtension.RefreshCache = true;
        return Persist<FreightManagement.Business.RatesConfiguration>(obj);
      }
      catch
      {
        throw;
      }
    }
  }
}
﻿using Afx.Business.Collections;
using Afx.Business.Service;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.WCF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Business.Service
{
  public partial class FreightManagementService
  {
    public BasicCollection<FreightManagement.Business.ExternalUser> LoadExternalUserCollection()
    {
      try
      {
        return GetAllInstances<FreightManagement.Business.ExternalUser>();
      }
      catch
      {
        throw;
      }
    }

    public BasicCollection<FreightManagement.Business.ExternalUser> SaveExternalUserCollection(BasicCollection<FreightManagement.Business.ExternalUser> col)
    {
      try
      {
        return Persist<FreightManagement.Business.ExternalUser>(col);
      }
      catch
      {
        throw;
      }
    }
  }
}
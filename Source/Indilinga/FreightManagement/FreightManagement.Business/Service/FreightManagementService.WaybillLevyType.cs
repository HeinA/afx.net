﻿using Afx.Business.Collections;
using Afx.Business.Service;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.WCF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Business.Service
{
  public partial class FreightManagementService
  {
    public BasicCollection<FreightManagement.Business.WaybillLevyType> LoadWaybillLevyTypeCollection()
    {
      try
      {
        return GetAllInstances<FreightManagement.Business.WaybillLevyType>();
      }
      catch
      {
        throw;
      }
    }

    public BasicCollection<FreightManagement.Business.WaybillLevyType> SaveWaybillLevyTypeCollection(BasicCollection<FreightManagement.Business.WaybillLevyType> col)
    {
      try
      {
        return Persist<FreightManagement.Business.WaybillLevyType>(col);
      }
      catch
      {
        throw;
      }
    }
  }
}
﻿using Afx.Business.Collections;
using Afx.Business.Service;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.WCF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Business.Service
{
  public partial class FreightManagementService
  {
    public BasicCollection<FreightManagement.Business.ExternalRole> LoadExternalRoleCollection()
    {
      try
      {
        return GetAllInstances<FreightManagement.Business.ExternalRole>();
      }
      catch
      {
        throw;
      }
    }

    public BasicCollection<FreightManagement.Business.ExternalRole> SaveExternalRoleCollection(BasicCollection<FreightManagement.Business.ExternalRole> col)
    {
      try
      {
        return Persist<FreightManagement.Business.ExternalRole>(col);
      }
      catch
      {
        throw;
      }
    }
  }
}
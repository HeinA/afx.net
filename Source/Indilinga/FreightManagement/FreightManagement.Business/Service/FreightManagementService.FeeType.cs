﻿using Afx.Business.Collections;
using Afx.Business.Service;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.WCF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Business.Service
{
  public partial class FreightManagementService
  {
    public BasicCollection<FreightManagement.Business.FeeType> LoadFeeTypeCollection()
    {
      try
      {
        return GetAllInstances<FreightManagement.Business.FeeType>();
      }
      catch
      {
        throw;
      }
    }

    public BasicCollection<FreightManagement.Business.FeeType> SaveFeeTypeCollection(BasicCollection<FreightManagement.Business.FeeType> col)
    {
      try
      {
        return Persist<FreightManagement.Business.FeeType>(col);
      }
      catch
      {
        throw;
      }
    }
  }
}
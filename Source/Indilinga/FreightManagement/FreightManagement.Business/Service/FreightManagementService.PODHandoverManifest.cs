﻿using Afx.Business.Service;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.WCF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Business.Service
{
  public partial class FreightManagementService
  {
    public FreightManagement.Business.PODHandoverManifest LoadPODHandoverManifest(Guid globalIdentifier)
    {
      try
      {
        return GetInstance<FreightManagement.Business.PODHandoverManifest>(globalIdentifier);
      }
      catch
      {
        throw;
      }
    }

    public FreightManagement.Business.PODHandoverManifest SavePODHandoverManifest(FreightManagement.Business.PODHandoverManifest obj)
    {
      try
      {
        return Persist<FreightManagement.Business.PODHandoverManifest>(obj);
      }
      catch
      {
        throw;
      }
    }
  }
}
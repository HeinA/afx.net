﻿using Afx.Business.Collections;
using Afx.Business.Service;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.WCF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Business.Service
{
  public partial class FreightManagementService
  {
    public BasicCollection<FreightManagement.Business.SlotTime> LoadSlotTimeCollection()
    {
      try
      {
        return GetAllInstances<FreightManagement.Business.SlotTime>();
      }
      catch
      {
        throw;
      }
    }

    public BasicCollection<FreightManagement.Business.SlotTime> SaveSlotTimeCollection(BasicCollection<FreightManagement.Business.SlotTime> col)
    {
      try
      {
        return Persist<FreightManagement.Business.SlotTime>(col);
      }
      catch
      {
        throw;
      }
    }
  }
}
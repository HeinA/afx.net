﻿using Afx.Business;
using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using Geographics.Business;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace FreightManagement.Business
{
  [PersistantObject(Schema = "FreightManagement")]
  public partial class DeliveryLocation : ExtensionObject<Location>
  {
    #region Constructors

    public DeliveryLocation()
    {
    }

    #endregion

    #region DeliveryPointType DeliveryPointType

    public const string DeliveryPointTypeProperty = "DeliveryPointType";
    [DataMember(Name = DeliveryPointTypeProperty, EmitDefaultValue = false)]
    DeliveryPointType mDeliveryPointType;
    [PersistantProperty]
    public DeliveryPointType DeliveryPointType
    {
      get { return mDeliveryPointType; }
      set { SetProperty<DeliveryPointType>(ref mDeliveryPointType, value); }
    }

    #endregion

    #region bool IsWaypoint

    public const string IsWaypointProperty = "IsWaypoint";
    [DataMember(Name = IsWaypointProperty, EmitDefaultValue = false)]
    bool mIsWaypoint;
    [PersistantProperty]
    public bool IsWaypoint
    {
      get { return mIsWaypoint; }
      set { SetProperty<bool>(ref mIsWaypoint, value); }
    }

    #endregion
  }
}

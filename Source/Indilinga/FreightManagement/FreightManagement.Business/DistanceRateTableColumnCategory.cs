﻿using Afx.Business.Data;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Business
{
  [PersistantObject(Schema = "FreightManagement")]
  [DataContract(IsReference = true, Namespace = FreightManagement.Business.Namespace.FreightManagement)]
  public class DistanceRateTableColumnCategory : DistanceRateTableColumn, IRateTableCategory
  {
    #region Constructors

    public DistanceRateTableColumnCategory()
    {
    }

    public DistanceRateTableColumnCategory(IRateTableCategory category)
      : this(category, true)
    {
    }

    public DistanceRateTableColumnCategory(IRateTableCategory category, bool clone)
    {
      if (clone) GlobalIdentifier = category.GlobalIdentifier;
      StartUnit = category.StartUnit;
      IsFixedRate = category.IsFixedRate;
      IsPercentage = category.IsPercentage;
      CostComponentType = category.CostComponentType;
      ThereAfter = category.ThereAfter;
    }

    #endregion

    #region string Text

    public override string Text
    {
      get
      {
        string unit = Owner.UnitOfMeasure;
        if (IsMinimumCharge) return string.Format("Minimum\r\nCharge");
        if (IsPercentage) return string.Format("\u2265 {0}\r\n% of {1}", StartUnit, unit, CultureInfo.CurrentCulture.NumberFormat.CurrencySymbol);  
        if (IsFixedRate)
        {
          return string.Format("\u2265 {0} {1}\r\n{2} Fixed", StartUnit, unit, CultureInfo.CurrentCulture.NumberFormat.CurrencySymbol);
        }
        else
        {
          return string.Format("\u2265 {0} {1}\r\n{2}/{1}{3}", StartUnit, unit, CultureInfo.CurrentCulture.NumberFormat.CurrencySymbol, ThereAfter ? "\r\nThere After" : string.Empty);
        }
      }
    }

    #endregion

    #region int StartUnit

    public const string StartUnitProperty = "StartUnit";
    [DataMember(Name = StartUnitProperty, EmitDefaultValue = false)]
    int mStartUnit;
    [PersistantProperty]
    public int StartUnit
    {
      get { return mStartUnit; }
      set
      {
        if (IsMinimumCharge) return;
        SetProperty<int>(ref mStartUnit, value);
      }
    }

    #endregion

    #region bool IsFixedRate

    public const string IsFixedRateProperty = "IsFixedRate";
    [DataMember(Name = IsFixedRateProperty, EmitDefaultValue = false)]
    bool mIsFixedRate;
    [PersistantProperty]
    public bool IsFixedRate
    {
      get { return mIsFixedRate; }
      set
      {
        if (IsMinimumCharge) return;
        SetProperty<bool>(ref mIsFixedRate, value);
      }
    }

    #endregion

    #region bool IsPercentage

    public const string IsPercentageProperty = "IsPercentage";
    [DataMember(Name = IsPercentageProperty, EmitDefaultValue = false)]
    bool mIsPercentage;
    [PersistantProperty]
    public bool IsPercentage
    {
      get { return mIsPercentage; }
      set
      {
        if (IsMinimumCharge) return;
        SetProperty<bool>(ref mIsPercentage, value);
      }
    }

    #endregion

    #region bool IsMinimumCharge

    public const string IsMinimumChargeProperty = "IsMinimumCharge";
    [DataMember(Name = IsMinimumChargeProperty, EmitDefaultValue = false)]
    bool mIsMinimumCharge;
    [PersistantProperty]
    public bool IsMinimumCharge
    {
      get { return mIsMinimumCharge; }
      set
      {
        if (SetProperty<bool>(ref mIsMinimumCharge, value))
        {
          IsFixedRate = true;
          IsPercentage = false;
          StartUnit = 0;
        } 
      }
    }

    #endregion

    #region bool ThereAfter

    public const string ThereAfterProperty = "ThereAfter";
    [DataMember(Name = ThereAfterProperty, EmitDefaultValue = false)]
    bool mThereAfter;
    [PersistantProperty]
    public bool ThereAfter
    {
      get { return mThereAfter; }
      set { SetProperty<bool>(ref mThereAfter, value); }
    }

    #endregion

    #region WaybillCostComponentType CostComponentType

    public const string CostComponentTypeProperty = "CostComponentType";
    [PersistantProperty(IsCached = true)]
    public WaybillCostComponentType CostComponentType
    {
      get { return GetCachedObject<WaybillCostComponentType>(); }
      set { SetCachedObject<WaybillCostComponentType>(value); }
    }

    #endregion

    public override object Index
    {
      get
      {
        if (IsMinimumCharge) return "ZZZZZZ";
        return string.Format("ZZZZZZ - {0:000000}", StartUnit);
      }
    }
  }
}

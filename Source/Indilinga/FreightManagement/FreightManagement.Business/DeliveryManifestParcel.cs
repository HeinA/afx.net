﻿using Afx.Business;
using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Validation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace FreightManagement.Business
{
  [PersistantObject(Schema = "FreightManagement")]
  [DataContract(IsReference = true, Namespace = FreightManagement.Business.Namespace.FreightManagement)]
  public partial class DeliveryManifestParcel : BusinessObject<DeliveryManifest>
  {
    #region Constructors

    public DeliveryManifestParcel()
    {
    }

    #endregion

    #region string WaybillNumber

    public const string WaybillNumberProperty = "WaybillNumber";
    [DataMember(Name = WaybillNumberProperty, EmitDefaultValue = false)]
    string mWaybillNumber;
    [PersistantProperty]
    public string WaybillNumber
    {
      get { return mWaybillNumber; }
      set { SetProperty<string>(ref mWaybillNumber, value); }
    }

    #endregion

    #region string Barcode

    public const string BarcodeProperty = "Barcode";
    [DataMember(Name = BarcodeProperty, EmitDefaultValue = false)]
    string mBarcode;
    [PersistantProperty]
    public string Barcode
    {
      get { return mBarcode; }
      set { SetProperty<string>(ref mBarcode, value); }
    }

    #endregion

    #region bool IsLoaded

    public const string IsLoadedProperty = "IsLoaded";
    [DataMember(Name = IsLoadedProperty, EmitDefaultValue = false)]
    bool mIsLoaded;
    [PersistantProperty]
    public bool IsLoaded
    {
      get { return mIsLoaded; }
      set { SetProperty<bool>(ref mIsLoaded, value); }
    }

    #endregion
  }
}

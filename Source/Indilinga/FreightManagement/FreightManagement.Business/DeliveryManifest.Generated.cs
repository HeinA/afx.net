﻿using Afx.Business.Data;
using Afx.Business.Documents;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Business
{
  [DataContract(IsReference = true, Namespace = Namespace.FreightManagement)]
  public partial class DeliveryManifest
  {
    public const string DocumentTypeIdentifier = "{f9bdc119-1ca8-4085-9e48-6b3a0405d821}";
    public class States
    {
      public const string Cancelled = "{1f93d15b-3a88-4751-ad78-595cad859581}";
      public const string Closed = "{7babb78f-3d09-4f1f-9871-da1193864efb}";
      public const string Open = "{545c81f1-3b7c-4263-a20e-71844bd24318}";
      public const string Verified = "{0294f444-4786-48cb-9c8e-ccb924e2d8ed}";
    }

    protected DeliveryManifest(DocumentType documentType)
      : base(documentType)
    {
    }
  }
}

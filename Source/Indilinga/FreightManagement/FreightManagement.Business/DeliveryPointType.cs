﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Business
{
  public enum DeliveryPointType
  {
    None = 0
    , Primary = 1
    , Secondary = 2
  }
}

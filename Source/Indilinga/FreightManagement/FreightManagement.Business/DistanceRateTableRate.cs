﻿using Afx.Business.Data;
using Afx.Business.Tabular;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Business
{
  [PersistantObject(Schema = "FreightManagement")]
  [DataContract(IsReference = true, Namespace = FreightManagement.Business.Namespace.FreightManagement)]
  public class DistanceRateTableRate : TableCell<DistanceRateTable, DistanceRateTableDistanceCategory, DistanceRateTableColumn>
  {
    #region decimal Rate

    public const string RateProperty = "Rate";
    [DataMember(Name = RateProperty, EmitDefaultValue = false)]
    decimal mRate;
    [PersistantProperty]
    public decimal Rate
    {
      get { return mRate; }
      set { SetProperty<decimal>(ref mRate, value); }
    }

    #endregion
  }
}

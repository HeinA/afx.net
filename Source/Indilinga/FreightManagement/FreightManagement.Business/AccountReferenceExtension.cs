﻿using AccountManagement.Business;
using Afx.Business;
using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace FreightManagement.Business
{
  [PersistantObject(Schema = "FreightManagement", TableName = "vAccountReferenceExtension", IsReadOnly = true)]
  public partial class AccountReferenceExtension : ExtensionObject<AccountReference>
  {
    #region Constructors

    public AccountReferenceExtension()
    {
    }

    #endregion

    #region Associative BusinessObjectCollection<RatesConfigurationReference> Rates

    public const string RatesProperty = "Rates";
    AssociativeObjectCollection<AccountReferenceRatesConfiguration, RatesConfigurationReference> mRates;
    [PersistantCollection(AssociativeType = typeof(AccountReferenceRatesConfiguration))]
    public BusinessObjectCollection<RatesConfigurationReference> Rates
    {
      get
      {
        if (mRates == null) using (new EventStateSuppressor(this)) { mRates = new AssociativeObjectCollection<AccountReferenceRatesConfiguration, RatesConfigurationReference>(this); }
        return mRates;
      }
    }

    #endregion

    #region int DefaultShipFromAddress

    public const string DefaultShipFromAddressProperty = "DefaultShipFromAddress";
    [DataMember(Name = DefaultShipFromAddressProperty, EmitDefaultValue = false)]
    int mDefaultShipFromAddress;
    [PersistantProperty]
    public int DefaultShipFromAddress
    {
      get { return mDefaultShipFromAddress; }
      set { SetProperty<int>(ref mDefaultShipFromAddress, value); }
    }

    #endregion

    #region TaxType TaxType

    public const string TaxTypeProperty = "TaxType";
    [PersistantProperty(IsCached = true)]
    public TaxType TaxType
    {
      get { return GetCachedObject<TaxType>(); }
      set { SetCachedObject<TaxType>(value); }
    }

    #endregion
  }
}

﻿using Afx.Business.Data;
using Afx.Business.Tabular;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Business
{
  [PersistantObject(Schema = "FreightManagement")]
  [DataContract(IsReference = true, Namespace = FreightManagement.Business.Namespace.FreightManagement)]
  public abstract class RateTableColumn : TableColumn<RateTable> 
  {
    #region string Text

    public const string TextProperty = "Text";
    public abstract string Text
    {
      get;
    }

    #endregion
  }
}

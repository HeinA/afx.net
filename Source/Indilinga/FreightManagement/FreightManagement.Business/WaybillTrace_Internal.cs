﻿using Afx.Business;
using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Business.Validation;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace FreightManagement.Business
{
  [PersistantObject(Schema = "FreightManagement", TableName = "WaybillTrace")]
  partial class WaybillTrace_Internal : BusinessObject 
  {
    #region Constructors

    public WaybillTrace_Internal(WaybillReference waybill, string text)
    {
      Waybill = waybill;
      Text = text;
      User = Cache.Instance.GetObjects<UserReference>().Where(u => u.GlobalIdentifier == SecurityContext.User.GlobalIdentifier).FirstOrDefault();
    }

    #endregion

    #region WaybillReference Waybill

    public const string WaybillProperty = "Waybill";
    [PersistantProperty(IsCached = true)]
    public WaybillReference Waybill
    {
      get { return GetCachedObject<WaybillReference>(); }
      set { SetCachedObject<WaybillReference>(value); }
    }

    #endregion

    //#region int Waybill

    //public const string WaybillProperty = "Waybill";
    //[DataMember(Name = WaybillProperty, EmitDefaultValue = false)]
    //int mWaybill;
    //[PersistantProperty]
    //public int Waybill
    //{
    //  get { return mWaybill; }
    //  set { SetProperty<int>(ref mWaybill, value); }
    //}

    //#endregion

    #region DateTime Timestamp

    public const string TimestampProperty = "Timestamp";
    [DataMember(Name = TimestampProperty, EmitDefaultValue = false)]
    DateTime mTimestamp = DateTime.Now;
    [PersistantProperty]
    public DateTime Timestamp
    {
      get { return mTimestamp; }
      set { SetProperty<DateTime>(ref mTimestamp, value); }
    }

    #endregion

    #region string Text

    public const string TextProperty = "Text";
    [DataMember(Name = TextProperty, EmitDefaultValue = false)]
    string mText;
    [PersistantProperty]
    [Mandatory("Text is mandatory.")]
    public string Text
    {
      get { return mText; }
      set { SetProperty<string>(ref mText, value); }
    }

    #endregion

    #region UserReference User

    public const string UserProperty = "User";
    [DataMember(Name = UserProperty, EmitDefaultValue = false)]
    UserReference mUser;
    [PersistantProperty]
    public UserReference User
    {
      get { return mUser; }
      set { SetProperty<UserReference>(ref mUser, value); }
    }

    #endregion
  }
}

﻿using Afx.Business;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Business
{
  [Export(typeof(IObjectFlags))]
  public class WaybillFlagGroupFlags : IObjectFlags
  {
    public const string CostingFlagGroup = "FreightManagement.CostingFlagGroup";
    public const string ProblemFlagGroup = "FreightManagement.ProblemFlagGroup";

    public Type ObjectType
    {
      get { return typeof(WaybillFlagGroup); }
    }

    public IEnumerable<string> Flags
    {
      get { return new string[] { CostingFlagGroup, ProblemFlagGroup }; }
    }
  }
}

﻿using Afx.Business;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Documents;
using Afx.Business.Validation;
using ContactManagement.Business;
using FleetManagement.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Business
{
  [PersistantObject(Schema = "FreightManagement")]
  public partial class CollectionManifest : Document, IVehicleCollection
  {
    public CollectionManifest()
      : base(Cache.Instance.GetObject<DocumentType>(DocumentTypeIdentifier))
    {
    }

    #region Route Route

    public const string RouteProperty = "Route";
    [PersistantProperty(IsCached = true)]
    public Route Route
    {
      get { return GetCachedObject<Route>(); }
      set { SetCachedObject<Route>(value); }
    }

    #endregion

    #region Contractor Courier

    public const string CourierProperty = "Courier";
    [PersistantProperty(IsCached = true)]
    public Contractor Courier
    {
      get { return GetCachedObject<Contractor>(); }
      set { SetCachedObject<Contractor>(value); }
    }

    #endregion

    #region Contact Driver

    public const string DriverProperty = "Driver";
    [PersistantProperty(IsCached = true)]
    public Contact Driver
    {
      get { return GetCachedObject<Contact>(); }
      set { SetCachedObject<Contact>(value); }
    }

    #endregion

    #region Contact CoDriver

    public const string CoDriverProperty = "CoDriver";
    [DataMember(Name = CoDriverProperty, EmitDefaultValue = false)]
    Contact mCoDriver;
    [PersistantProperty]
    public Contact CoDriver
    {
      get { return mCoDriver; }
      set { SetProperty<Contact>(ref mCoDriver, value); }
    }

    #endregion

    #region Contact BackupDriver

    public const string BackupDriverProperty = "BackupDriver";
    [DataMember(Name = BackupDriverProperty, EmitDefaultValue = false)]
    Contact mBackupDriver;
    [PersistantProperty]
    public Contact BackupDriver
    {
      get { return mBackupDriver; }
      set { SetProperty<Contact>(ref mBackupDriver, value); }
    }

    #endregion

    #region Associative BusinessObjectCollection<WaybillReference> Waybills

    public const string WaybillsProperty = "Waybills";
    AssociativeObjectCollection<CollectionManifestWaybill, WaybillReference> mWaybills;
    [PersistantCollection(AssociativeType = typeof(CollectionManifestWaybill))]
    public BusinessObjectCollection<WaybillReference> Waybills
    {
      get
      {
        if (mWaybills == null) using (new EventStateSuppressor(this)) { mWaybills = new AssociativeObjectCollection<CollectionManifestWaybill, WaybillReference>(this); }
        return mWaybills;
      }
    }

    #endregion

    #region void OnCompositionChanged(...)

    protected override void OnCompositionChanged(CompositionChangedType compositionChangedType, string propertyName, object originalSource, BusinessObject item)
    {
      if ((compositionChangedType == CompositionChangedType.ChildAdded || compositionChangedType == CompositionChangedType.ChildRemoved) && propertyName == WaybillsProperty)
      {
        RecalculateItems();
        RecalculateWeight();

        if (Waybills.Count == 0)
        {
          Route = null;
        }
      }

      OnPropertyChanged(WaybillCountProperty);

      base.OnCompositionChanged(compositionChangedType, propertyName, originalSource, item);
    }

    #endregion

    #region int WaybillCount

    public const string WaybillCountProperty = "WaybillCount";
    [PersistantProperty(IgnoreConcurrency = true)]
    public int WaybillCount
    {
      get { return Waybills.Count; }
    }

    #endregion

    #region int Items

    void RecalculateItems()
    {
      int i = 0;
      foreach (var wr in Waybills)
      {
        i += wr.Items;
      }
      Items = i;
    }

    public const string ItemsProperty = "Items";
    [DataMember(Name = ItemsProperty, EmitDefaultValue = false)]
    int mItems;
    [PersistantProperty(IgnoreConcurrency = true)]
    public int Items
    {
      get { return mItems; }
      private set { SetProperty<int>(ref mItems, value); }
    }

    #endregion

    #region decimal Weight

    void RecalculateWeight()
    {
      decimal i = 0;
      foreach (var wr in Waybills)
      {
        i += wr.PhysicalWeight;
      }
      Weight = i;
    }

    public const string WeightProperty = "Weight";
    [DataMember(Name = WeightProperty, EmitDefaultValue = false)]
    decimal mWeight;
    [PersistantProperty(IgnoreConcurrency = true)]
    public decimal Weight
    {
      get { return mWeight; }
      private set { SetProperty<decimal>(ref mWeight, value); }
    }

    #endregion

    #region DateTime? ScheduledLoadingDate

    public const string ScheduledLoadingDateProperty = "ScheduledLoadingDate";
    [DataMember(Name = ScheduledLoadingDateProperty, EmitDefaultValue = false)]
    DateTime? mScheduledLoadingDate;
    [PersistantProperty]
    public DateTime? ScheduledLoadingDate
    {
      get { return mScheduledLoadingDate; }
      set { SetProperty<DateTime?>(ref mScheduledLoadingDate, value == null ? null : (DateTime?)value.Value.Date); }
    }

    #endregion

    #region SlotTime ScheduledLoadingSlot

    public const string ScheduledLoadingSlotProperty = "ScheduledLoadingSlot";
    [PersistantProperty(IsCached = true)]
    public SlotTime ScheduledLoadingSlot
    {
      get { return GetCachedObject<SlotTime>(); }
      set { SetCachedObject<SlotTime>(value); }
    }

    #endregion

    #region DateTime? ScheduledOffloadingDate

    public const string ScheduledOffloadingDateProperty = "ScheduledOffloadingDate";
    [DataMember(Name = ScheduledOffloadingDateProperty, EmitDefaultValue = false)]
    DateTime? mScheduledOffloadingDate;
    [PersistantProperty]
    public DateTime? ScheduledOffloadingDate
    {
      get { return mScheduledOffloadingDate; }
      set { SetProperty<DateTime?>(ref mScheduledOffloadingDate, value == null ? null : (DateTime?)value.Value.Date); }
    }

    #endregion

    #region SlotTime ScheduledOffloadingSlot

    public const string ScheduledOffloadingSlotProperty = "ScheduledOffloadingSlot";
    [PersistantProperty(IsCached = true)]
    public SlotTime ScheduledOffloadingSlot
    {
      get { return GetCachedObject<SlotTime>(); }
      set { SetCachedObject<SlotTime>(value); }
    }

    #endregion

    #region DateTime? ActualLoadingTime

    public const string ActualLoadingTimeProperty = "ActualLoadingTime";
    [DataMember(Name = ActualLoadingTimeProperty, EmitDefaultValue = false)]
    DateTime? mActualLoadingTime;
    [PersistantProperty]
    public DateTime? ActualLoadingTime
    {
      get { return mActualLoadingTime; }
      set { SetProperty<DateTime?>(ref mActualLoadingTime, value); }
    }

    #endregion

    #region DateTime? ActualOffloadingTime

    public const string ActualOffloadingTimeProperty = "ActualOffloadingTime";
    [DataMember(Name = ActualOffloadingTimeProperty, EmitDefaultValue = false)]
    DateTime? mActualOffloadingTime;
    [PersistantProperty]
    public DateTime? ActualOffloadingTime
    {
      get { return mActualOffloadingTime; }
      set { SetProperty<DateTime?>(ref mActualOffloadingTime, value); }
    }

    #endregion

    #region DateTime? DepartureTime

    public const string DepartureTimeProperty = "DepartureTime";
    [DataMember(Name = DepartureTimeProperty, EmitDefaultValue = false)]
    DateTime? mDepartureTime;
    [PersistantProperty]
    public DateTime? DepartureTime
    {
      get { return mDepartureTime; }
      set { SetProperty<DateTime?>(ref mDepartureTime, value); }
    }

    #endregion

    #region Associative BusinessObjectCollection<Vehicle> Vehicles

    public const string VehiclesProperty = "Vehicles";
    AssociativeObjectCollection<CollectionManifestVehicle, Vehicle> mVehicles;
    [PersistantCollection(AssociativeType = typeof(CollectionManifestVehicle))]
    [Mandatory("Vehicles are mandatory")]
    public BusinessObjectCollection<Vehicle> Vehicles
    {
      get
      {
        if (mVehicles == null) using (new EventStateSuppressor(this)) { mVehicles = new AssociativeObjectCollection<CollectionManifestVehicle, Vehicle>(this); }
        return mVehicles;
      }
    }

    #endregion
  }
}

﻿using Afx.Business.Data;
using Afx.Business.Documents;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Business
{
  [DataContract(IsReference = true, Namespace = Namespace.FreightManagement)]
  public partial class TransportManifest
  {
    public const string DocumentTypeIdentifier = "{09540969-6401-4feb-b1ea-a8afff88d1a1}";
    public class States
    {
      public const string Cancelled = "{de1824e3-1b9c-40a7-8a59-488d170359e5}";
      public const string Closed = "{aa827611-7c92-41c2-999c-74080559bcf6}";
      public const string Open = "{acd6d12e-29d7-4a4c-aadb-57eec41d895d}";
      public const string Verified = "{f8cf97de-0e51-4b3c-a7cd-799014d3e04d}";
    }

    protected TransportManifest(DocumentType documentType)
      : base(documentType)
    {
    }
  }
}

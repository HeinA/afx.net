﻿using Afx.Business.Data;
using Afx.Business.Documents;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Business
{
  [DataContract(IsReference = true, Namespace = Namespace.FreightManagement)]
  public partial class Waybill
  {
    public const string DocumentTypeIdentifier = "{c23d8300-5247-4e80-8a27-98d22397007d}";
    public class States
    {
      public const string Cancelled = "{0b4092b1-0e40-4d09-958e-c510c4a5d23c}";
      public const string Collected = "{60b01a90-023e-4e28-bfa9-ed767a4d263a}";
      public const string Complete = "{f0c7db86-e5a9-45cf-a155-384e0f1eec35}";
      public const string Delivered = "{5e177c2b-96a6-4ae1-a83f-d30bb3cb52cd}";
      public const string InTransit = "{e8873a46-42a9-4c9d-9488-8e5c7879e547}";
      public const string InterDepotTransfer = "{f60369bb-0a79-4f3d-9ae1-06cf388f6ea0}";
      public const string Invoiced = "{013e6849-2f28-4138-bb72-0af9620fd96a}";
      public const string OnDelivery = "{2269be41-ee3b-4616-81c7-c0fea38cd1dc}";
      public const string OnFloor = "{261cf228-96b6-43a2-b51b-d0360542e001}";
      public const string PendingCollection = "{eae66a8a-d2e3-43bb-b031-3ce1a7f49bf9}";
      public const string PendingInduction = "{f2a547d1-53eb-492f-9476-939e48753cb2}";
      public const string PendingInvoice = "{97eeffca-7106-45bf-abf8-bdfcfd4cf46e}";
      public const string PODCaptured = "{4ee6af21-1d6e-46d8-b2fb-8d46d5ebaafc}";
      public const string PODReceived = "{e9f8d4df-0a72-42a3-9b2c-a3782c141e85}";
      public const string PODTransfer = "{088bb00c-8254-4b5c-9889-385fd0b83cfc}";
      public const string QuoteAccepted = "{7a3e2a17-34f0-421f-b067-00e1054b8665}";
      public const string Quoted = "{a73a890b-1b14-408b-bf39-1253bb7365a6}";
    }

    protected Waybill(DocumentType documentType)
      : base(documentType)
    {
    }
  }
}

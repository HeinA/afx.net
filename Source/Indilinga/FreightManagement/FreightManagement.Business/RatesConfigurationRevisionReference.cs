﻿using Afx.Business;
using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Validation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace FreightManagement.Business
{
  [PersistantObject(Schema = "FreightManagement", TableName = "vRatesConfigurationRevisionReference", IsReadOnly = true)]
  public partial class RatesConfigurationRevisionReference : BusinessObject<RatesConfigurationReference> 
  {
    #region Constructors

    public RatesConfigurationRevisionReference()
    {
    }

    #endregion

    #region DateTime EffectiveDate

    public const string EffectiveDateProperty = "EffectiveDate";
    [DataMember(Name = EffectiveDateProperty, EmitDefaultValue = false)]
    DateTime mEffectiveDate = DateTime.Now;
    [PersistantProperty]
    [Mandatory("Effective Date is mandatory.")]
    public DateTime EffectiveDate
    {
      get { return mEffectiveDate; }
      set { SetProperty<DateTime>(ref mEffectiveDate, value); }
    }

    #endregion

    #region int VolumetricFactor

    public const string VolumetricFactorProperty = "VolumetricFactor";
    [DataMember(Name = VolumetricFactorProperty, EmitDefaultValue = false)]
    int mVolumetricFactor;
    [PersistantProperty]
    [Mandatory("Volumetric Factor is mandatory.")]
    public int VolumetricFactor
    {
      get { return mVolumetricFactor; }
      set { SetProperty<int>(ref mVolumetricFactor, value); }
    }

    #endregion

    #region Associative BusinessObjectCollection<CargoType> CargoTypes

    public const string CargoTypesProperty = "CargoTypes";
    AssociativeObjectCollection<RatesConfigurationRevisionReferenceCargoType, CargoType> mCargoTypes;
    [PersistantCollection(AssociativeType = typeof(RatesConfigurationRevisionReferenceCargoType))]
    public BusinessObjectCollection<CargoType> CargoTypes
    {
      get
      {
        if (mCargoTypes == null) using (new EventStateSuppressor(this)) { mCargoTypes = new AssociativeObjectCollection<RatesConfigurationRevisionReferenceCargoType, CargoType>(this); }
        return mCargoTypes;
      }
    }

    #endregion

    #region Associative BusinessObjectCollection<WaybillFlag> FlaggedParameters

    public const string FlaggedParametersProperty = "FlaggedParameters";
    AssociativeObjectCollection<RatesConfigurationRevisionReferenceCostingFlag, WaybillFlag> mFlaggedParameters;
    [PersistantCollection(AssociativeType = typeof(RatesConfigurationRevisionReferenceCostingFlag))]
    public BusinessObjectCollection<WaybillFlag> FlaggedParameters
    {
      get
      {
        if (mFlaggedParameters == null) using (new EventStateSuppressor(this)) { mFlaggedParameters = new AssociativeObjectCollection<RatesConfigurationRevisionReferenceCostingFlag, WaybillFlag>(this); }
        return mFlaggedParameters;
      }
    }

    #endregion
  }
}

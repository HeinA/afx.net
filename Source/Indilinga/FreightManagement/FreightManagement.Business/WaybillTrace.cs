﻿using Afx.Business;
using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Business.Validation;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace FreightManagement.Business
{
  [PersistantObject(Schema = "FreightManagement")]
  public partial class WaybillTrace : BusinessObject<Waybill> 
  {
    #region Constructors

    public WaybillTrace()
    {
      User = Cache.Instance.GetObjects<UserReference>().Where(u => u.GlobalIdentifier == SecurityContext.User.GlobalIdentifier).FirstOrDefault();
    }

    public WaybillTrace(string text)
    {
      Timestamp = DateTime.Now;
      Text = text;
      User = Cache.Instance.GetObjects<UserReference>().Where(u => u.GlobalIdentifier == SecurityContext.User.GlobalIdentifier).FirstOrDefault();
    }

    #endregion

    #region DateTime Timestamp

    public const string TimestampProperty = "Timestamp";
    [DataMember(Name = TimestampProperty, EmitDefaultValue = false)]
    DateTime mTimestamp = DateTime.Now;
    [PersistantProperty]
    public DateTime Timestamp
    {
      get { return mTimestamp; }
      set { SetProperty<DateTime>(ref mTimestamp, value); }
    }

    #endregion

    #region string Text

    public const string TextProperty = "Text";
    [DataMember(Name = TextProperty, EmitDefaultValue = false)]
    string mText;
    [PersistantProperty]
    [Mandatory("Text is mandatory.")]
    public string Text
    {
      get { return mText; }
      set { SetProperty<string>(ref mText, value); }
    }

    #endregion

    #region UserReference User

    public const string UserProperty = "User";
    [PersistantProperty(IsCached = true)]
    public UserReference User
    {
      get { return GetCachedObject<UserReference>(); }
      set { SetCachedObject<UserReference>(value); }
    }

    #endregion
  }
}

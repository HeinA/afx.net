﻿using Afx.Business;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Documents;
using Afx.Business.Security;
using Afx.Business.Validation;
using ContactManagement.Business;
using FleetManagement.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Business
{
  [PersistantObject(Schema = "FreightManagement")]
  public partial class TransferManifest : Document, IVehicleCollection
  {
    public TransferManifest()
      : base(Cache.Instance.GetObject<DocumentType>(DocumentTypeIdentifier))
    {
    }

    #region DistributionCenter OriginDC

    public const string OriginDCProperty = "OriginDC";
    [PersistantProperty(IsCached = true)]
    [Mandatory("Origin Distribution Center is mandatory.")]
    public DistributionCenter OriginDC
    {
      get { return GetCachedObject<DistributionCenter>(); }
      set { SetCachedObject<DistributionCenter>(value); }
    }

    #endregion

    #region DistributionCenter DestinationDC

    public const string DestinationDCProperty = "DestinationDC";
    [PersistantProperty(IsCached = true)]
    [Mandatory("Destination Distribution Center is mandatory.")]
    public DistributionCenter DestinationDC
    {
      get { return GetCachedObject<DistributionCenter>(); }
      set { SetCachedObject<DistributionCenter>(value); }
    }

    #endregion

    #region Contractor Courier

    public const string CourierProperty = "Courier";
    [PersistantProperty(IsCached = true)]
    public Contractor Courier
    {
      get { return GetCachedObject<Contractor>(); }
      set { SetCachedObject<Contractor>(value); }
    }

    #endregion

    #region Contact Driver

    public const string DriverProperty = "Driver";
    [PersistantProperty(IsCached = true)]
    public Contact Driver
    {
      get
      {
        var d = GetCachedObject<Contact>();
        return d;
      }
      set { SetCachedObject<Contact>(value); }
    }

    #endregion

    #region Contact CoDriver

    public const string CoDriverProperty = "CoDriver";
    [PersistantProperty(IsCached = true)]
    public Contact CoDriver
    {
      get { return GetCachedObject<Contact>(); }
      set { SetCachedObject<Contact>(value); }
    }

    #endregion

    #region Contact BackupDriver

    public const string BackupDriverProperty = "BackupDriver";
    [PersistantProperty(IsCached = true)]
    public Contact BackupDriver
    {
      get { return GetCachedObject<Contact>(); }
      set { SetCachedObject<Contact>(value); }
    }

    #endregion

    #region string SealNumber

    public const string SealNumberProperty = "SealNumber";
    [DataMember(Name = SealNumberProperty, EmitDefaultValue = false)]
    string mSealNumber;
    [PersistantProperty]
    public string SealNumber
    {
      get { return mSealNumber; }
      set { SetProperty<string>(ref mSealNumber, value); }
    }

    #endregion

    #region Associative BusinessObjectCollection<WaybillReferenceWithCargo> Waybills

    public const string WaybillsProperty = "Waybills";
    AssociativeObjectCollection<TransferManifestWaybill, WaybillReference> mWaybills;
    [PersistantCollection(AssociativeType = typeof(TransferManifestWaybill))]
    public BusinessObjectCollection<WaybillReference> Waybills
    {
      get
      {
        if (mWaybills == null) using (new EventStateSuppressor(this)) { mWaybills = new AssociativeObjectCollection<TransferManifestWaybill, WaybillReference>(this); }
        return mWaybills;
      }
    }

    #endregion

    #region void OnCompositionChanged(...)

    protected override void OnCompositionChanged(CompositionChangedType compositionChangedType, string propertyName, object originalSource, BusinessObject item)
    {
      if ((compositionChangedType == CompositionChangedType.ChildAdded || compositionChangedType == CompositionChangedType.ChildRemoved) && propertyName == WaybillsProperty)
      {
        RecalculateItems();
        RecalculateWeight();
        RecalculateIncome();
      }

      OnPropertyChanged(WaybillCountProperty);

      base.OnCompositionChanged(compositionChangedType, propertyName, originalSource, item);
    }

    #endregion

    #region int WaybillCount

    public const string WaybillCountProperty = "WaybillCount";
    [PersistantProperty(IgnoreConcurrency = true)]
    public int WaybillCount
    {
      get { return Waybills.Count; }
    }

    #endregion

    #region int Items

    void RecalculateItems()
    {
      int i = 0;
      foreach (var wr in Waybills)
      {
        i += wr.Items;
      }
      Items = i;
    }

    public const string ItemsProperty = "Items";
    [DataMember(Name = ItemsProperty, EmitDefaultValue = false)]
    int mItems;
    [PersistantProperty(IgnoreConcurrency = true)]
    public int Items
    {
      get { return mItems; }
      private set { SetProperty<int>(ref mItems, value); }
    }

    #endregion

    #region decimal Weight

    void RecalculateWeight()
    {
      decimal i = 0;
      foreach (var wr in Waybills)
      {
        i += wr.PhysicalWeight;
      }
      Weight = i;
    }

    public const string WeightProperty = "Weight";
    [DataMember(Name = WeightProperty, EmitDefaultValue = false)]
    decimal mWeight;
    [PersistantProperty(IgnoreConcurrency = true)]
    public decimal Weight
    {
      get { return mWeight; }
      private set { SetProperty<decimal>(ref mWeight, value); }
    }

    #endregion

    #region decimal Income

    void RecalculateIncome()
    {
      OnPropertyChanged(IncomeProperty);
    }

    public const string IncomeProperty = "Income";
    public decimal Income
    {
      get { return Waybills.Sum(w => w.Charge); }
    }

    #endregion

    #region DateTime? ScheduledLoadingDate

    public const string ScheduledLoadingDateProperty = "ScheduledLoadingDate";
    [DataMember(Name = ScheduledLoadingDateProperty, EmitDefaultValue = false)]
    DateTime? mScheduledLoadingDate;
    [PersistantProperty]
    public DateTime? ScheduledLoadingDate
    {
      get { return mScheduledLoadingDate; }
      set { SetProperty<DateTime?>(ref mScheduledLoadingDate, value == null ? null : (DateTime?)value.Value.Date); }
    }

    #endregion

    #region SlotTime ScheduledLoadingSlot

    public const string ScheduledLoadingSlotProperty = "ScheduledLoadingSlot";
    [PersistantProperty(IsCached = true)]
    public SlotTime ScheduledLoadingSlot
    {
      get { return GetCachedObject<SlotTime>(); }
      set { SetCachedObject<SlotTime>(value); }
    }

    #endregion

    #region DateTime? ScheduledOffLoadingDate

    public const string ScheduledOffLoadingDateProperty = "ScheduledOffLoadingDate";
    [DataMember(Name = ScheduledOffLoadingDateProperty, EmitDefaultValue = false)]
    DateTime? mScheduledOffLoadingDate;
    [PersistantProperty]
    public DateTime? ScheduledOffLoadingDate
    {
      get { return mScheduledOffLoadingDate; }
      set { SetProperty<DateTime?>(ref mScheduledOffLoadingDate, value == null ? null : (DateTime?)value.Value.Date); }
    }

    #endregion

    #region SlotTime ScheduledOffloadingSlot

    public const string ScheduledOffloadingSlotProperty = "ScheduledOffloadingSlot";
    [PersistantProperty(IsCached = true)]
    public SlotTime ScheduledOffloadingSlot
    {
      get { return GetCachedObject<SlotTime>(); }
      set { SetCachedObject<SlotTime>(value); }
    }

    #endregion

    #region DateTime? ActualLoadingTime

    public const string ActualLoadingTimeProperty = "ActualLoadingTime";
    [DataMember(Name = ActualLoadingTimeProperty, EmitDefaultValue = false)]
    DateTime? mActualLoadingTime;
    [PersistantProperty]
    public DateTime? ActualLoadingTime
    {
      get { return mActualLoadingTime; }
      set { SetProperty<DateTime?>(ref mActualLoadingTime, value); }
    }

    #endregion

    #region DateTime? ActualOffloadingTime

    public const string ActualOffloadingTimeProperty = "ActualOffloadingTime";
    [DataMember(Name = ActualOffloadingTimeProperty, EmitDefaultValue = false)]
    DateTime? mActualOffloadingTime;
    [PersistantProperty]
    public DateTime? ActualOffloadingTime
    {
      get { return mActualOffloadingTime; }
      set { SetProperty<DateTime?>(ref mActualOffloadingTime, value); }
    }

    #endregion

    #region DateTime? DepartureTime

    public const string DepartureTimeProperty = "DepartureTime";
    [DataMember(Name = DepartureTimeProperty, EmitDefaultValue = false)]
    DateTime? mDepartureTime;
    [PersistantProperty]
    public DateTime? DepartureTime
    {
      get { return mDepartureTime; }
      set { SetProperty<DateTime?>(ref mDepartureTime, value); }
    }

    #endregion

    #region Associative BusinessObjectCollection<Vehicle> Vehicles

    public const string VehiclesProperty = "Vehicles";
    AssociativeObjectCollection<TransferManifestVehicle, Vehicle> mVehicles;
    [PersistantCollection(AssociativeType = typeof(TransferManifestVehicle))]
    //[Mandatory("Vehicles are mandatory")]
    public BusinessObjectCollection<Vehicle> Vehicles
    {
      get
      {
        if (mVehicles == null) using (new EventStateSuppressor(this)) { mVehicles = new AssociativeObjectCollection<TransferManifestVehicle, Vehicle>(this); }
        return mVehicles;
      }
    }

    #endregion

    #region BusinessObjectCollection<TransferManifestParcel> Parcels

    public const string ParcelsProperty = "Parcels";
    BusinessObjectCollection<TransferManifestParcel> mParcels;
    [PersistantCollection]
    [DataMember]
    public BusinessObjectCollection<TransferManifestParcel> Parcels
    {
      get { return mParcels ?? (mParcels = new BusinessObjectCollection<TransferManifestParcel>(this)); }
    }

    #endregion

    public IEnumerable<TransferManifestParcel> PartiallyLoadedWaybillParcels
    {
      get
      {
        return Parcels.Where(p => !p.IsDeleted && !Waybills.Any(wr => wr.WaybillNumber == p.WaybillNumber));
      }
    }
  }
}

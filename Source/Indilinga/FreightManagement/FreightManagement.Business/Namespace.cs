﻿using Afx.Business;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Business
{
  [Export(typeof(INamespace))]
  public class Namespace : INamespace
  {
    public const string FreightManagement = "http://indilinga.com/FreightManagement/Business";

    public string GetUri()
    {
      return FreightManagement;
    }
  }
}

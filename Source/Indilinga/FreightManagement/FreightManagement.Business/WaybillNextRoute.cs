﻿using Afx.Business;
using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Validation;
using Geographics.Business;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace FreightManagement.Business
{
  [PersistantObject(Schema = "FreightManagement", TableName = "vWaybillNextRoute", IsReadOnly = true)]
  public partial class WaybillNextRoute : BusinessObject 
  {
    #region Constructors

    public WaybillNextRoute()
    {
    }

    #endregion

    #region string WaybillNumber

    public const string WaybillNumberProperty = "WaybillNumber";
    [DataMember(Name = WaybillNumberProperty, EmitDefaultValue = false)]
    string mWaybillNumber;
    [PersistantProperty]
    public string WaybillNumber
    {
      get { return mWaybillNumber; }
      set { SetProperty<string>(ref mWaybillNumber, value); }
    }

    #endregion

    #region DateTime WaybillDate

    public const string WaybillDateProperty = "WaybillDate";
    [DataMember(Name = WaybillDateProperty, EmitDefaultValue = false)]
    DateTime mWaybillDate;
    [PersistantProperty]
    public DateTime WaybillDate
    {
      get { return mWaybillDate; }
      set { SetProperty<DateTime>(ref mWaybillDate, value); }
    }

    #endregion

    #region DateTime DeliveryDate

    public const string DeliveryDateProperty = "DeliveryDate";
    [DataMember(Name = DeliveryDateProperty, EmitDefaultValue = false)]
    DateTime mDeliveryDate;
    [PersistantProperty]
    public DateTime DeliveryDate
    {
      get { return mDeliveryDate; }
      set { SetProperty<DateTime>(ref mDeliveryDate, value); }
    }

    #endregion

    #region Location DeliveryCity

    public const string DeliveryCityProperty = "DeliveryCity";
    [PersistantProperty(IsCached = true)]
    public Location DeliveryCity
    {
      get { return GetCachedObject<Location>(); }
      set { SetCachedObject<Location>(value); }
    }

    #endregion

    #region int Items

    public const string ItemsProperty = "Items";
    [DataMember(Name = ItemsProperty, EmitDefaultValue = false)]
    int mItems;
    [PersistantProperty]
    public int Items
    {
      get { return mItems; }
      set { SetProperty<int>(ref mItems, value); }
    }

    #endregion

    #region decimal Weight

    public const string WeightProperty = "Weight";
    [DataMember(Name = WeightProperty, EmitDefaultValue = false)]
    decimal mWeight;
    [PersistantProperty]
    public decimal Weight
    {
      get { return mWeight; }
      set { SetProperty<decimal>(ref mWeight, value); }
    }

    #endregion

    #region DistributionCenter DistributionCenter

    public const string DistributionCenterProperty = "DistributionCenter";
    [PersistantProperty(IsCached = true)]
    public DistributionCenter DistributionCenter
    {
      get { return GetCachedObject<DistributionCenter>(); }
      set { SetCachedObject<DistributionCenter>(value); }
    }

    #endregion

    #region Route Route

    public const string RouteProperty = "Route";
    [PersistantProperty(IsCached = true)]
    public Route Route
    {
      get { return GetCachedObject<Route>(); }
      set { SetCachedObject<Route>(value); }
    }

    #endregion
  }
}

﻿using Afx.Business;
using Afx.Business.Data;
using Geographics.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Business
{
  public static class LocationExtensions
  {
    public static Location PrimaryDeliveryPoint(this Location location)
    {
      if (location.GetExtensionObject<DeliveryLocation>().DeliveryPointType == DeliveryPointType.Primary) return location;
      if (BusinessObject.IsNull(location.Owner)) return null;
      return location.Owner.PrimaryDeliveryPoint();
    }

    public static Location SecondaryDeliveryPoint(this Location location)
    {
      if (location.GetExtensionObject<DeliveryLocation>().DeliveryPointType == DeliveryPointType.Secondary) return location;
      if (BusinessObject.IsNull(location.Owner)) return null;
      return location.Owner.SecondaryDeliveryPoint();
    }

    public static Location Waypoint(this Location location)
    {
      if (location.GetExtensionObject<DeliveryLocation>().IsWaypoint) return location;
      if (BusinessObject.IsNull(location.Owner)) return null;
      return location.Owner.Waypoint();
    }

    public static DistributionCenter DistributionCenter(this Location location)
    {
      Location pdp = location.PrimaryDeliveryPoint();
      if (pdp != null)
      {
        DistributionCenter dc = Cache.Instance.GetObjects<DistributionCenter>(d => d.Location.Equals(pdp)).FirstOrDefault();
        if (dc != null) return dc;
      }

      pdp = location.SecondaryDeliveryPoint();
      if (pdp != null)
      {
        DistributionCenter dc = Cache.Instance.GetObjects<DistributionCenter>(d => d.Location.Equals(pdp)).FirstOrDefault();
        if (dc != null) return dc;
      }

      return null;
    }

    public static string FullNameWithDistributionCenter(this Location location)
    {
      DistributionCenter dc = location.DistributionCenter();
      if (dc != null)
      {
        return string.Format("{0} (DC)", location.FullName);
      }
      return location.FullName;
    }

    public static string FullNameWithDistributionCenterWithoutZone(this Location location)
    {
      DistributionCenter dc = location.DistributionCenter();
      if (dc != null)
      {
        return string.Format("{0} (DC)", location.FullNameWithoutZone);
      }
      return location.FullNameWithoutZone;
    }
  }
}

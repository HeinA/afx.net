﻿using Afx.Business;
using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Validation;
using Geographics.Business;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace FreightManagement.Business
{
  [PersistantObject(Schema = "FreightManagement")]
  public partial class RatesConfigurationRevision : BusinessObject<RatesConfiguration>
  {
    #region Constructors

    public RatesConfigurationRevision()
    {
    }

    private RatesConfigurationRevision(RatesConfigurationRevision source, DateTime effectiveDate, Dictionary<WaybillChargeType, decimal> cargoTypeIncreases)
    {
      this.EffectiveDate = effectiveDate;
      this.VolumetricFactor = source.VolumetricFactor;
      this.InsuranceRate = source.InsuranceRate;

      foreach (var ct in source.CargoTypes)
      {
        this.CargoTypes.Add(ct);

        var oRevisionCargoType = source.GetAssociativeObject<Business.RatesConfigurationRevisionCargoType>(ct);
        var rRevisionCargoType = this.GetAssociativeObject<Business.RatesConfigurationRevisionCargoType>(ct);

        foreach (var rt in oRevisionCargoType.Tables)
        {
          rRevisionCargoType.Tables.Add(new RateTable(rt, cargoTypeIncreases));
        }

        foreach (var rt in oRevisionCargoType.DistanceTables)
        {
          rRevisionCargoType.DistanceTables.Add(new DistanceRateTable(rt, cargoTypeIncreases));
        }
      }

      foreach (var rt in source.TablesByWeight)
      {
        this.TablesByWeight.Add(new RateTable(rt, cargoTypeIncreases));
      }

      foreach (var lt in source.LevyTypes)
      {
        this.LevyTypes.Add(lt);

        var oRevisionLevyType = source.GetAssociativeObject<Business.RatesConfigurationRevisionLevyType>(lt);
        var rRevisionLevyType = this.GetAssociativeObject<Business.RatesConfigurationRevisionLevyType>(lt);

        decimal increase = GetIncrease(lt, cargoTypeIncreases);
        rRevisionLevyType.Rate = oRevisionLevyType.Rate + (oRevisionLevyType.Rate * increase / 100);
      }
    }

    decimal GetIncrease(WaybillChargeType ct, Dictionary<WaybillChargeType, decimal> cargoTypeIncreases)
    {
      if (cargoTypeIncreases == null) return 0;
      if (!cargoTypeIncreases.ContainsKey(ct)) return 0;
      return cargoTypeIncreases[ct];
    }

    #endregion

    #region DateTime EffectiveDate

    public const string EffectiveDateProperty = "EffectiveDate";
    [DataMember(Name = EffectiveDateProperty, EmitDefaultValue = false)]
    DateTime mEffectiveDate = DateTime.Now;
    [PersistantProperty]
    [Mandatory("Effective Date is mandatory.")]
    public DateTime EffectiveDate
    {
      get { return mEffectiveDate; }
      set { SetProperty<DateTime>(ref mEffectiveDate, value); }
    }

    #endregion

    #region int VolumetricFactor

    public const string VolumetricFactorProperty = "VolumetricFactor";
    [DataMember(Name = VolumetricFactorProperty, EmitDefaultValue = false)]
    int mVolumetricFactor;
    [PersistantProperty]
    public int VolumetricFactor
    {
      get { return mVolumetricFactor; }
      set { SetProperty<int>(ref mVolumetricFactor, value); }
    }

    #endregion

    #region decimal InsuranceRate

    public const string InsuranceRateProperty = "InsuranceRate";
    [DataMember(Name = InsuranceRateProperty, EmitDefaultValue = false)]
    decimal mInsuranceRate;
    [PersistantProperty]
    public decimal InsuranceRate
    {
      get { return mInsuranceRate; }
      set { SetProperty<decimal>(ref mInsuranceRate, value); }
    }

    #endregion

    #region Associative BusinessObjectCollection<CargoType> CargoTypes

    public const string CargoTypesProperty = "CargoTypes";
    AssociativeObjectCollection<RatesConfigurationRevisionCargoType, CargoType> mCargoTypes;
    [PersistantCollection(AssociativeType = typeof(RatesConfigurationRevisionCargoType))]
    public BusinessObjectCollection<CargoType> CargoTypes
    {
      get
      {
        if (mCargoTypes == null) using (new EventStateSuppressor(this)) { mCargoTypes = new AssociativeObjectCollection<RatesConfigurationRevisionCargoType, CargoType>(this); }
        return mCargoTypes;
      }
    }

    #endregion

    #region Associative BusinessObjectCollection<RateTable> TablesByWeight

    public const string TablesByWeightProperty = "TablesByWeight";
    AssociativeObjectCollection<RatesConfigurationRevisionWeightTable, RateTable> mTablesByWeight;
    [PersistantCollection(AssociativeType = typeof(RatesConfigurationRevisionWeightTable))]
    public BusinessObjectCollection<RateTable> TablesByWeight
    {
      get
      {
        if (mTablesByWeight == null) using (new EventStateSuppressor(this)) { mTablesByWeight = new AssociativeObjectCollection<RatesConfigurationRevisionWeightTable, RateTable>(this); }
        return mTablesByWeight;
      }
    }

    #endregion

    #region Associative BusinessObjectCollection<RateTable> DistanceTablesByWeight

    public const string DistanceTablesByWeightProperty = "DistanceTablesByWeight";
    AssociativeObjectCollection<RatesConfigurationRevisionCargoTypeDistanceTable, DistanceRateTable> mDistanceTablesByWeight;
    [PersistantCollection(AssociativeType = typeof(RatesConfigurationRevisionCargoTypeDistanceTable))]
    public BusinessObjectCollection<DistanceRateTable> DistanceTablesByWeight
    {
      get
      {
        if (mDistanceTablesByWeight == null) using (new EventStateSuppressor(this)) { mDistanceTablesByWeight = new AssociativeObjectCollection<RatesConfigurationRevisionCargoTypeDistanceTable, DistanceRateTable>(this); }
        return mDistanceTablesByWeight;
      }
    }

    #endregion

    #region Associative BusinessObjectCollection<WaybillLevyType> LevyTypes

    public const string LevyTypesProperty = "LevyTypes";
    AssociativeObjectCollection<RatesConfigurationRevisionLevyType, WaybillLevyType> mLevyTypes;
    [PersistantCollection(AssociativeType = typeof(RatesConfigurationRevisionLevyType))]
    public BusinessObjectCollection<WaybillLevyType> LevyTypes
    {
      get
      {
        if (mLevyTypes == null) using (new EventStateSuppressor(this)) { mLevyTypes = new AssociativeObjectCollection<RatesConfigurationRevisionLevyType, WaybillLevyType>(this); }
        return mLevyTypes;
      }
    }

    #endregion

    #region Associative BusinessObjectCollection<WaybillFlag> FlaggedParameters

    public const string FlaggedParametersProperty = "FlaggedParameters";
    AssociativeObjectCollection<RatesConfigurationRevisionCostingFlag, WaybillFlag> mFlaggedParameters;
    [PersistantCollection(AssociativeType = typeof(RatesConfigurationRevisionCostingFlag))]
    public BusinessObjectCollection<WaybillFlag> FlaggedParameters
    {
      get
      {
        if (mFlaggedParameters == null) using (new EventStateSuppressor(this)) { mFlaggedParameters = new AssociativeObjectCollection<RatesConfigurationRevisionCostingFlag, WaybillFlag>(this); }
        return mFlaggedParameters;
      }
    }

    #endregion

    #region RatesConfigurationRevision CreateRevision(...)

    public RatesConfigurationRevision CreateRevision(DateTime effectiveDate, Dictionary<WaybillChargeType, decimal> cargoTypeIncreases)
    {
      return new RatesConfigurationRevision(this, effectiveDate, cargoTypeIncreases);
    }

    public RatesConfigurationRevision CreateRevision(DateTime effectiveDate)
    {
      return new RatesConfigurationRevision(this, effectiveDate, null);
    }

    #endregion
  }
}

﻿using Afx.Business;
using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Validation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace FreightManagement.Business
{
  [PersistantObject(Schema = "FreightManagement")]
  //[Cache]
  public partial class WaybillFlag : BusinessObject<WaybillFlagGroup> //, IRevisable 
  {
    #region Constructors

    public WaybillFlag()
    {
    }

    public WaybillFlag(bool isNull)
      : base(isNull)
    {
    }

    #endregion

    //#region int Revision

    //public const string RevisionProperty = "Revision";
    //[DataMember(Name = RevisionProperty, EmitDefaultValue = false)]
    //int mRevision;
    //[PersistantProperty]
    //public int Revision
    //{
    //  get { return mRevision; }
    //  set { SetProperty<int>(ref mRevision, value); }
    //}

    //#endregion
    
    #region string Text

    public const string TextProperty = "Text";
    [DataMember(Name = TextProperty, EmitDefaultValue = false)]
    string mText;
    [PersistantProperty]
    [Mandatory("Text is mandatory.")]
    public string Text
    {
      get { return mText; }
      set { SetProperty<string>(ref mText, value); }
    }

    #endregion

    //#region bool IsCostingFlag

    //public const string IsCostingFlagProperty = "IsCostingFlag";
    //[DataMember(Name = IsCostingFlagProperty, EmitDefaultValue = false)]
    //bool mIsCostingFlag;
    //[PersistantProperty]
    //public bool IsCostingFlag
    //{
    //  get { return mIsCostingFlag; }
    //  set { SetProperty<bool>(ref mIsCostingFlag, value); }
    //}

    //#endregion
  }
}

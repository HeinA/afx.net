﻿using Afx.Business;
using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Validation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace FreightManagement.Business
{
  [PersistantObject(Schema = "FreightManagement", OwnerColumn = "Waybill", TableName = "WaybillCargoItem", IsReadOnly = true)]
  public partial class WaybillReferenceCargoItem : BusinessObject<WaybillReference>
  {
    #region Constructors

    public WaybillReferenceCargoItem()
    {
    }

    #endregion

    #region int Items

    public const string ItemsProperty = "Items";
    [DataMember(Name = ItemsProperty, EmitDefaultValue = false)]
    int mItems = 1;
    [PersistantProperty]
    public int Items
    {
      get { return mItems; }
      set { SetProperty<int>(ref mItems, value); }
    }

    #endregion

    #region string Description

    public const string DescriptionProperty = "Description";
    [DataMember(Name = DescriptionProperty, EmitDefaultValue = false)]
    string mDescription;
    [PersistantProperty]
    public string Description
    {
      get { return mDescription; }
      set { SetProperty<string>(ref mDescription, value); }
    }

    #endregion

    #region decimal PhysicalWeight

    public const string PhysicalWeightProperty = "PhysicalWeight";
    [DataMember(Name = PhysicalWeightProperty, EmitDefaultValue = false)]
    decimal mPhysicalWeight;
    [PersistantProperty]
    [Mandatory("Physical Weight is mandatory")]
    public decimal PhysicalWeight
    {
      get { return mPhysicalWeight; }
      set { SetProperty<decimal>(ref mPhysicalWeight, value); }
    }

    #endregion

    #region decimal VolumetricWeight

    public const string VolumetricWeightProperty = "VolumetricWeight";
    [DataMember(Name = VolumetricWeightProperty, EmitDefaultValue = false)]
    decimal mVolumetricWeight;
    [PersistantProperty]
    public decimal VolumetricWeight
    {
      get { return mVolumetricWeight; }
      set { SetProperty<decimal>(ref mVolumetricWeight, decimal.Round(value, 2)); }
    }

    #endregion

    #region decimal DeclaredValue

    public const string DeclaredValueProperty = "DeclaredValue";
    [DataMember(Name = DeclaredValueProperty, EmitDefaultValue = false)]
    decimal mDeclaredValue;
    [PersistantProperty]
    public decimal DeclaredValue
    {
      get { return mDeclaredValue; }
      set { SetProperty<decimal>(ref mDeclaredValue, value); }
    }

    #endregion
  }
}

﻿using Afx.Business;
using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Validation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace FreightManagement.Business
{
  [PersistantObject(Schema = "FreightManagement")]
  public partial class WaybillCharge : BusinessObject<Waybill> 
  {
    #region Constructors

    public WaybillCharge()
    {
    }

    public WaybillCharge(WaybillChargeType chargeType, decimal charge)
    {
      ChargeType = chargeType;
      Charge = charge;
    }

    #endregion

    #region WaybillChargeType ChargeType

    public const string ChargeTypeProperty = "ChargeType";
    [PersistantProperty(IsCached = true)]
    [Mandatory("Charget Type is mandatory.")]
    public WaybillChargeType ChargeType
    {
      get { return GetCachedObject<WaybillChargeType>(); }
      set { SetCachedObject<WaybillChargeType>(value); }
    }

    #endregion

    #region decimal Charge

    public const string ChargeProperty = "Charge";
    [DataMember(Name = ChargeProperty, EmitDefaultValue = false)]
    decimal mCharge;
    [PersistantProperty]
    public decimal Charge
    {
      get { return mCharge; }
      set { SetProperty<decimal>(ref mCharge, decimal.Round(value, 2)); }
    }

    #endregion

    #region bool SystemGenerated

    public const string SystemGeneratedProperty = "SystemGenerated";
    [DataMember(Name = SystemGeneratedProperty, EmitDefaultValue = false)]
    bool mSystemGenerated;
    [PersistantProperty]
    public bool SystemGenerated
    {
      get { return mSystemGenerated; }
      set { SetProperty<bool>(ref mSystemGenerated, value); }
    }

    #endregion

    #region string Description

    public const string DescriptionProperty = "Description";
    [DataMember(Name = DescriptionProperty, EmitDefaultValue = false)]
    string mDescription;
    [PersistantProperty]
    public string Description
    {
      get { return mDescription; }
      set { SetProperty<string>(ref mDescription, value); }
    }

    #endregion
  }
}

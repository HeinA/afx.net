﻿using AccountManagement.Business;
using Afx.Business;
using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Validation;
using Geographics.Business;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace FreightManagement.Business
{
  [PersistantObject(Schema = "FreightManagement", TableName = "vWaybillCollection", IsReadOnly = true)]
  public partial class WaybillCollection : BusinessObject 
  {
    #region Constructors

    public WaybillCollection()
    {
    }

    #endregion

    #region DateTime WaybillDate

    public const string WaybillDateProperty = "WaybillDate";
    [DataMember(Name = WaybillDateProperty, EmitDefaultValue = false)]
    DateTime mWaybillDate;
    [PersistantProperty]
    public DateTime WaybillDate
    {
      get { return mWaybillDate; }
      set { SetProperty<DateTime>(ref mWaybillDate, value); }
    }

    #endregion

    #region string WaybillNumber

    public const string WaybillNumberProperty = "WaybillNumber";
    [DataMember(Name = WaybillNumberProperty, EmitDefaultValue = false)]
    string mWaybillNumber;
    [PersistantProperty]
    public string WaybillNumber
    {
      get { return mWaybillNumber; }
      set { SetProperty<string>(ref mWaybillNumber, value); }
    }

    #endregion

    #region AccountReference Account

    public const string AccountProperty = "Account";
    [PersistantProperty(IsCached = true)]
    public AccountReference Account
    {
      get { return GetCachedObject<AccountReference>(); }
      set { SetCachedObject<AccountReference>(value); }
    }

    #endregion

    #region string Consigner

    public const string ConsignerProperty = "Consigner";
    [DataMember(Name = ConsignerProperty, EmitDefaultValue = false)]
    string mConsigner;
    [PersistantProperty]
    public string Consigner
    {
      get { return mConsigner; }
      set { SetProperty<string>(ref mConsigner, value); }
    }

    #endregion

    #region string Consignee

    public const string ConsigneeProperty = "Consignee";
    [DataMember(Name = ConsigneeProperty, EmitDefaultValue = false)]
    string mConsignee;
    [PersistantProperty]
    public string Consignee
    {
      get { return mConsignee; }
      set { SetProperty<string>(ref mConsignee, value); }
    }

    #endregion

    #region DateTime CollectionDate

    public const string CollectionDateProperty = "CollectionDate";
    [DataMember(Name = CollectionDateProperty, EmitDefaultValue = false)]
    DateTime mCollectionDate;
    [PersistantProperty]
    public DateTime CollectionDate
    {
      get { return mCollectionDate; }
      set { SetProperty<DateTime>(ref mCollectionDate, value); }
    }

    #endregion

    #region Route CollectionRoute

    public const string CollectionRouteProperty = "CollectionRoute";
    [PersistantProperty(IsCached = true)]
    public Route CollectionRoute
    {
      get { return GetCachedObject<Route>(); }
      set { SetCachedObject<Route>(value); }
    }

    #endregion

    #region Location CollectionCity

    public const string CollectionCityProperty = "CollectionCity";
    [PersistantProperty(IsCached = true)]
    public Location CollectionCity
    {
      get { return GetCachedObject<Location>(); }
      set { SetCachedObject<Location>(value); }
    }

    #endregion

    #region Location CollectionSuburb

    public const string CollectionSuburbProperty = "CollectionSuburb";
    [PersistantProperty(IsCached = true)]
    public Location CollectionSuburb
    {
      get { return GetCachedObject<Location>(); }
      set { SetCachedObject<Location>(value); }
    }

    #endregion

    #region string CollectionAddress

    public const string CollectionAddressProperty = "CollectionAddress";
    [DataMember(Name = CollectionAddressProperty, EmitDefaultValue = false)]
    string mCollectionAddress;
    [PersistantProperty]
    public string CollectionAddress
    {
      get { return mCollectionAddress; }
      set { SetProperty<string>(ref mCollectionAddress, value); }
    }

    #endregion

    #region string CollectionAddressSingleLine

    public string CollectionAddressSingleLine
    {
      get { return CollectionAddress.Replace("\r\n", ", ").Replace("\n", ", "); }
    }

    #endregion

    #region int Items

    public const string ItemsProperty = "Items";
    [DataMember(Name = ItemsProperty, EmitDefaultValue = false)]
    int mItems;
    [PersistantProperty]
    public int Items
    {
      get { return mItems; }
      set { SetProperty<int>(ref mItems, value); }
    }

    #endregion

    #region decimal PhysicalWeight

    public const string PhysicalWeightProperty = "PhysicalWeight";
    [DataMember(Name = PhysicalWeightProperty, EmitDefaultValue = false)]
    decimal mPhysicalWeight;
    [PersistantProperty]
    public decimal PhysicalWeight
    {
      get { return mPhysicalWeight; }
      set { SetProperty<decimal>(ref mPhysicalWeight, value); }
    }

    #endregion

    #region DistributionCenter CollectionDC

    public const string CollectionDCProperty = "CollectionDC";
    [PersistantProperty(IsCached = true)]
    public DistributionCenter CollectionDC
    {
      get { return GetCachedObject<DistributionCenter>(); }
      set { SetCachedObject<DistributionCenter>(value); }
    }

    #endregion
  }
}

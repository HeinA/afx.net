﻿using Afx.Business.Data;
using Afx.Business.Documents;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Business
{
  [DataContract(IsReference = true, Namespace = Namespace.FreightManagement)]
  public partial class PODHandoverManifest
  {
    public const string DocumentTypeIdentifier = "{5f7c5472-b191-4351-93e2-0bd3aa06245a}";
    public class States
    {
      public const string Closed = "{397e4562-72e8-4fa9-b336-03c702df07d2}";
      public const string Open = "{2ee306ca-fcee-4c93-b053-48d58b98e58b}";
      public const string Verified = "{6ceda54e-0ee1-4929-9c20-d17372a728ec}";
    }

    protected PODHandoverManifest(DocumentType documentType)
      : base(documentType)
    {
    }
  }
}

﻿using Afx.Business;
using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Validation;
using Geographics.Business;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace FreightManagement.Business
{
  [PersistantObject(Schema = "FreightManagement")]
  public partial class Route : BusinessObject<DistributionCenter> 
  {
    #region Constructors

    public Route()
    {
    }

    public Route(bool isNull) : base(isNull)
    {

    }

    #endregion

    #region string Name

    public const string NameProperty = "Name";
    [DataMember(Name = NameProperty, EmitDefaultValue = false)]
    string mName;
    [PersistantProperty]
    public string Name
    {
      get { return mName; }
      set { SetProperty<string>(ref mName, value); }
    }

    #endregion

    #region Location Origin

    public const string OriginProperty = "Origin";
    public Location Origin
    {
      get { return Owner.Location; }
    }

    #endregion

    #region Location Destination

    public const string DestinationProperty = "Destination";
    [PersistantProperty(IsCached = true)]
    public Location Destination
    {
      get { return GetCachedObject<Location>(); }
      set { SetCachedObject<Location>(value); }
    }

    #endregion

    #region bool IsOmnidirectional

    public const string IsOmnidirectionalProperty = "IsOmnidirectional";
    [DataMember(Name = IsOmnidirectionalProperty, EmitDefaultValue = false)]
    bool mIsOmnidirectional;
    [PersistantProperty]
    public bool IsOmnidirectional
    {
      get { return mIsOmnidirectional; }
      set { SetProperty<bool>(ref mIsOmnidirectional, value); }
    }

    #endregion

    #region bool IsActive

    public const string IsActiveProperty = "IsActive";
    [DataMember(Name = IsActiveProperty, EmitDefaultValue = false)]
    bool mIsActive = true;
    [PersistantProperty]
    public bool IsActive
    {
      get { return mIsActive; }
      set { SetProperty<bool>(ref mIsActive, value); }
    }

    #endregion

    #region Associative BusinessObjectCollection<Location> Waypoints

    public const string WaypointsProperty = "Waypoints";
    AssociativeObjectCollection<Waypoint, Location> mWaypoints;
    [PersistantCollection(AssociativeType = typeof(Waypoint))]
    public BusinessObjectCollection<Location> Waypoints
    {
      get
      {
        if (mWaypoints == null) using (new EventStateSuppressor(this)) { mWaypoints = new AssociativeObjectCollection<Waypoint, Location>(this); }
        return mWaypoints;
      }
    }

    #endregion
  }
}

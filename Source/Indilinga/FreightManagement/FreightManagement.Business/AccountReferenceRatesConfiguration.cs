﻿using Afx.Business;
using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace FreightManagement.Business
{
  [PersistantObject(Schema = "FreightManagement", TableName = "vAccountReferenceRatesConfiguration", IsReadOnly = true)]
  public partial class AccountReferenceRatesConfiguration : AssociativeObject<AccountReferenceExtension, RatesConfigurationReference>
  {
    public AccountReferenceRatesConfiguration()
    {
    }

    protected override bool ReferenceIsOwned
    {
      get { return true; }
    }
  }
}

﻿using Afx.Business;
using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Validation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace FreightManagement.Business
{
  [PersistantObject(Schema = "FreightManagement")]
  public partial class CollectionRequestItem : BusinessObject<CollectionRequest> 
  {
    #region Constructors

    public CollectionRequestItem()
    {
    }

    #endregion

    #region int Quantity

    public const string QuantityProperty = "Quantity";
    [DataMember(Name = QuantityProperty, EmitDefaultValue = false)]
    int mQuantity;
    [PersistantProperty]
    [Mandatory("Quantity is mandatory")]
    public int Quantity
    {
      get { return mQuantity; }
      set { SetProperty<int>(ref mQuantity, value); }
    }

    #endregion

    #region string Description

    public const string DescriptionProperty = "Description";
    [DataMember(Name = DescriptionProperty, EmitDefaultValue = false)]
    string mDescription;
    [PersistantProperty]
    [Mandatory("Description is mandatory")]
    public string Description
    {
      get { return mDescription; }
      set { SetProperty<string>(ref mDescription, value); }
    }

    #endregion

    #region decimal PhysicalWeight

    public const string PhysicalWeightProperty = "PhysicalWeight";
    [DataMember(Name = PhysicalWeightProperty, EmitDefaultValue = false)]
    decimal mPhysicalWeight;
    [PersistantProperty]
    [Mandatory("PhysicalWeight is mandatory")]
    public decimal PhysicalWeight
    {
      get { return mPhysicalWeight; }
      set { SetProperty<decimal>(ref mPhysicalWeight, value); }
    }

    #endregion

    #region decimal VolumeX

    public const string VolumeXProperty = "VolumeX";
    [DataMember(Name = VolumeXProperty, EmitDefaultValue = false)]
    decimal mVolumeX;
    [PersistantProperty]
    public decimal VolumeX
    {
      get { return mVolumeX; }
      set { SetProperty<decimal>(ref mVolumeX, value); }
    }

    #endregion

    #region decimal VolumeY

    public const string VolumeYProperty = "VolumeY";
    [DataMember(Name = VolumeYProperty, EmitDefaultValue = false)]
    decimal mVolumeY;
    [PersistantProperty]
    public decimal VolumeY
    {
      get { return mVolumeY; }
      set { SetProperty<decimal>(ref mVolumeY, value); }
    }

    #endregion

    #region decimal VolumeZ

    public const string VolumeZProperty = "VolumeZ";
    [DataMember(Name = VolumeZProperty, EmitDefaultValue = false)]
    decimal mVolumeZ;
    [PersistantProperty]
    public decimal VolumeZ
    {
      get { return mVolumeZ; }
      set { SetProperty<decimal>(ref mVolumeZ, value); }
    }

    #endregion
  }
}

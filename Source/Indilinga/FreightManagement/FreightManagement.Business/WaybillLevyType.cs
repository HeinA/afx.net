﻿using Afx.Business;
using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Validation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace FreightManagement.Business
{
  [PersistantObject(Schema = "FreightManagement", HasFlags = true)]
  [Cache]
  public partial class WaybillLevyType : WaybillChargeType, IRevisable 
  {
    #region Constructors

    public WaybillLevyType()
    {
    }

    public WaybillLevyType(bool isNull)
      : base(isNull)
    {
    }

    #endregion

    #region int Revision

    public const string RevisionProperty = "Revision";
    [DataMember(Name = RevisionProperty, EmitDefaultValue = false)]
    int mRevision;
    [PersistantProperty]
    public int Revision
    {
      get { return mRevision; }
      set { SetProperty<int>(ref mRevision, value); }
    }

    #endregion

    #region LevyCalculationType LevyCalculationType

    public const string LevyCalculationTypeProperty = "LevyCalculationType";
    [DataMember(Name = LevyCalculationTypeProperty, EmitDefaultValue = false)]
    LevyCalculationType mLevyCalculationType;
    [Mandatory("Levy Calculation Type is mandatory")]
    [PersistantProperty]
    public LevyCalculationType LevyCalculationType
    {
      get { return mLevyCalculationType; }
      set { SetProperty<LevyCalculationType>(ref mLevyCalculationType, value); }
    }

    #endregion

    #region string Condition

    public const string ConditionProperty = "Condition";
    [DataMember(Name = ConditionProperty, EmitDefaultValue = false)]
    string mCondition;
    [PersistantProperty]
    public string Condition
    {
      get { return mCondition; }
      set { SetProperty<string>(ref mCondition, value); }
    }

    #endregion

    public bool IsApplicable(Waybill waybill)
    {
      if (string.IsNullOrWhiteSpace(Condition)) return true;
      ILevyCondition lc = (ILevyCondition)Activator.CreateInstance(Type.GetType(Condition));
      return lc.IsApplicable(waybill);
    }

    #region Associative BusinessObjectCollection<WaybillCostComponentType> ApplicableCostComponents

    public const string ApplicableCostComponentsProperty = "ApplicableCostComponents";
    AssociativeObjectCollection<WaybillLevyTypeCostComponentType, WaybillCostComponentType> mApplicableCostComponents;
    [PersistantCollection(AssociativeType = typeof(WaybillLevyTypeCostComponentType))]
    public BusinessObjectCollection<WaybillCostComponentType> ApplicableCostComponents
    {
      get
      {
        if (mApplicableCostComponents == null) using (new EventStateSuppressor(this)) { mApplicableCostComponents = new AssociativeObjectCollection<WaybillLevyTypeCostComponentType, WaybillCostComponentType>(this); }
        return mApplicableCostComponents;
      }
    }

    #endregion
  }
}

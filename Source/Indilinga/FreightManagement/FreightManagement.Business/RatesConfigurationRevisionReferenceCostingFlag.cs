﻿using Afx.Business;
using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace FreightManagement.Business
{
  [PersistantObject(Schema = "FreightManagement", TableName = "RatesConfigurationRevisionCostingFlag", OwnerColumn = "RatesConfigurationRevision", IsReadOnly = true)]
  public partial class RatesConfigurationRevisionReferenceCostingFlag : AssociativeObject<RatesConfigurationRevisionReference, WaybillFlag>
  {
    #region int VolumetricFactor

    public const string VolumetricFactorProperty = "VolumetricFactor";
    [DataMember(Name = VolumetricFactorProperty, EmitDefaultValue = false)]
    int mVolumetricFactor;
    [PersistantProperty]
    public int VolumetricFactor
    {
      get { return mVolumetricFactor; }
      set { SetProperty<int>(ref mVolumetricFactor, value); }
    }

    #endregion

    #region decimal InsuranceRate

    public const string InsuranceRateProperty = "InsuranceRate";
    [DataMember(Name = InsuranceRateProperty, EmitDefaultValue = false)]
    decimal mInsuranceRate;
    [PersistantProperty]
    public decimal InsuranceRate
    {
      get { return mInsuranceRate; }
      set { SetProperty<decimal>(ref mInsuranceRate, value); }
    }

    #endregion
  }
}

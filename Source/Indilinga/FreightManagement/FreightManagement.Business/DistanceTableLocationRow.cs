﻿using Afx.Business.Data;
using Afx.Business.Tabular;
using Geographics.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Business
{
  [PersistantObject(Schema = "FreightManagement")]
  [DataContract(IsReference = true, Namespace = FreightManagement.Business.Namespace.FreightManagement)]
  public class DistanceTableLocationRow : TableRow<DistanceTable, DistanceTableDistance>
  {
    public DistanceTableLocationRow()
    {
    }

    public DistanceTableLocationRow(Location location)
    {
      Location = location;
    }

    #region Location Location

    public const string LocationProperty = "Location";
    [PersistantProperty]
    public Location Location
    {
      get { return GetCachedObject<Location>(); }
      set { SetCachedObject<Location>(value); }
    }

    #endregion

    public override object Index
    {
      get { return Location.FullName; }
    }
  }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Business
{
  [Serializable]
  public class RatesConfigurationException : Exception
  {
    protected RatesConfigurationException(SerializationInfo info, StreamingContext context)
      :base(info, context)
    {
    }

    public RatesConfigurationException(string message)
      : base(message)
    {
    }

    public RatesConfigurationException(string message, Exception innerException)
      : base(message, innerException)
    {
    }
  }
}

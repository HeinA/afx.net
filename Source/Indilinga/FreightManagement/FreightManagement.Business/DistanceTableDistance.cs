﻿using Afx.Business.Data;
using Afx.Business.Tabular;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Business
{
  [PersistantObject(Schema = "FreightManagement")]
  [DataContract(IsReference = true, Namespace = FreightManagement.Business.Namespace.FreightManagement)]
  public class DistanceTableDistance : TableCell<DistanceTable, DistanceTableLocationRow, DistanceTableLocationColumn>
  {
    #region Constructors

    public DistanceTableDistance()
    {
    }

    public DistanceTableDistance(int distance)
    {
      Distance = distance;
    }

    #endregion

    #region int Distance

    public const string DistanceProperty = "Distance";
    [DataMember(Name = DistanceProperty, EmitDefaultValue = false)]
    int mDistance;
    [PersistantProperty]
    public int Distance
    {
      get { return mDistance; }
      set { SetProperty<int>(ref mDistance, value); }
    }

    #endregion
  }
}

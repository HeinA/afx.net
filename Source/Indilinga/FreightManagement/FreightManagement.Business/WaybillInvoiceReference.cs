﻿using AccountManagement.Business;
using Afx.Business;
using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Documents;
using Afx.Business.Validation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace FreightManagement.Business
{
  [PersistantObject(Schema = "FreightManagement", TableName = "vWaybillReference", IsReadOnly = true)]
  public partial class WaybillInvoiceReference : BusinessObject 
  {
    #region Constructors

    public WaybillInvoiceReference()
    {
    }

    #endregion

    #region string WaybillNumber

    public const string WaybillNumberProperty = "WaybillNumber";
    [DataMember(Name = WaybillNumberProperty, EmitDefaultValue = false)]
    string mWaybillNumber;
    [PersistantProperty]
    public string WaybillNumber
    {
      get { return mWaybillNumber; }
      set { SetProperty<string>(ref mWaybillNumber, value); }
    }

    #endregion

    #region DateTime WaybillDate

    public const string WaybillDateProperty = "WaybillDate";
    [DataMember(Name = WaybillDateProperty, EmitDefaultValue = false)]
    DateTime mWaybillDate;
    [PersistantProperty]
    public DateTime WaybillDate
    {
      get { return mWaybillDate; }
      set { SetProperty<DateTime>(ref mWaybillDate, value); }
    }

    #endregion

    #region DocumentTypeState WaybillState

    public const string WaybillStateProperty = "WaybillState";
    [PersistantProperty(IsCached = true)]
    public DocumentTypeState WaybillState
    {
      get { return GetCachedObject<DocumentTypeState>(); }
      set { SetCachedObject<DocumentTypeState>(value); }
    }

    #endregion

    #region DateTime WaybillStateTimestamp

    public const string WaybillStateTimestampProperty = "WaybillStateTimestamp";
    [DataMember(Name = WaybillStateTimestampProperty, EmitDefaultValue = false)]
    DateTime mWaybillStateTimestamp;
    [PersistantProperty]
    public DateTime WaybillStateTimestamp
    {
      get { return mWaybillStateTimestamp; }
      set { SetProperty<DateTime>(ref mWaybillStateTimestamp, value); }
    }

    #endregion

    #region AccountReference Account

    public const string AccountProperty = "Account";
    [PersistantProperty(IsCached = true)]
    public AccountReference Account
    {
      get { return GetCachedObject<AccountReference>(); }
      set { SetCachedObject<AccountReference>(value); }
    }

    #endregion

    #region decimal Charge

    public const string ChargeProperty = "Charge";
    [DataMember(Name = ChargeProperty, EmitDefaultValue = false)]
    decimal mCharge;
    [PersistantProperty]
    public decimal Charge
    {
      get { return mCharge; }
      set { SetProperty<decimal>(ref mCharge, value); }
    }

    #endregion

    #region BusinessObjectCollection<WaybillInvoiceReferenceCharge> Charges

    public const string ChargesProperty = "Charges";
    BusinessObjectCollection<WaybillInvoiceReferenceCharge> mCharges;
    [PersistantCollection]
    [DataMember]
    public BusinessObjectCollection<WaybillInvoiceReferenceCharge> Charges
    {
      get { return mCharges ?? (mCharges = new BusinessObjectCollection<WaybillInvoiceReferenceCharge>(this)); }
    }

    #endregion
  }
}

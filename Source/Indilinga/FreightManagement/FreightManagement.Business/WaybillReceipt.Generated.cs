﻿using Afx.Business.Data;
using Afx.Business.Documents;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Business
{
  [DataContract(IsReference = true, Namespace = Namespace.FreightManagement)]
  public partial class WaybillReceipt
  {
    public const string DocumentTypeIdentifier = "{b3e59efa-5e40-4148-a727-a94501902880}";
    public class States
    {
      public const string Open = "{1ae5b924-c139-4dc1-8c53-4eeeb7fe8d92}";
      public const string Verified = "{3eca779c-a69a-4993-b7c0-d9ae832b14f3}";
    }

    protected WaybillReceipt(DocumentType documentType)
      : base(documentType)
    {
    }
  }
}

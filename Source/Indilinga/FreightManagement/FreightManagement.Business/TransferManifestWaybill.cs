﻿using Afx.Business;
using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace FreightManagement.Business
{
  [PersistantObject(Schema = "FreightManagement", ReferenceColumn = "Waybill")]
  public partial class TransferManifestWaybill : AssociativeObject<TransferManifest, WaybillReference>
  {
  }
}

﻿using Afx.Business;
using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Documents;
using Afx.Business.Validation;
using ContactManagement.Business;
using FleetManagement.Business;
using Geographics.Business;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace FreightManagement.Business
{
  [PersistantObject(Schema = "FreightManagement", TableName = "vTransportManifestReference", IsReadOnly = true)]
  public partial class TransportManifestReference : BusinessObject 
  {
    #region Constructors

    public TransportManifestReference()
    {
    }

    #endregion

    

    #region string ManifestNumber

    public const string ManifestNumberProperty = "ManifestNumber";
    [DataMember(Name = ManifestNumberProperty, EmitDefaultValue = false)]
    string mManifestNumber;
    [PersistantProperty]
    public string ManifestNumber
    {
      get { return mManifestNumber; }
      private set { SetProperty<string>(ref mManifestNumber, value); }
    }

    #endregion

    #region DateTime ManifestDate

    public const string ManifestDateProperty = "ManifestDate";
    [DataMember(Name = ManifestDateProperty, EmitDefaultValue = false)]
    DateTime mManifestDate;
    [PersistantProperty]
    public DateTime ManifestDate
    {
      get { return mManifestDate; }
      private set { SetProperty<DateTime>(ref mManifestDate, value); }
    }

    #endregion

    #region DocumentTypeState ManifestState

    public const string ManifestStateProperty = "ManifestState";
    [PersistantProperty(IsCached = true)]
    public DocumentTypeState ManifestState
    {
      get { return GetCachedObject<DocumentTypeState>(); }
      set { SetCachedObject<DocumentTypeState>(value); }
    }

    #endregion


    #region string Route

    public const string RouteProperty = "Route";
    public string Route
    {
      get { return String.Format("{0}->{1}", Origin.City.Name, Destination.City.Name); }
    }

    #endregion

    #region Location Origin

    public const string OriginProperty = "Origin";
    [PersistantProperty(IsCached = true)]
    public Location Origin
    {
      get { return GetCachedObject<Location>(); }
      set { SetCachedObject<Location>(value); }
    }

    #endregion

    #region Location Destination

    public const string DestinationProperty = "Destination";
    [PersistantProperty(IsCached = true)]
    public Location Destination
    {
      get { return GetCachedObject<Location>(); }
      set { SetCachedObject<Location>(value); }
    }

    #endregion

    #region Contact Driver

    public const string DriverProperty = "Driver";
    [DataMember(Name = DriverProperty, EmitDefaultValue = false)]
    Contact mDriver;
    [PersistantProperty]
    public Contact Driver
    {
      get { return mDriver; }
      set { SetProperty<Contact>(ref mDriver, value); }
    }

    #endregion

    #region Contact CoDriver
    
    public const string CoDriverProperty = "CoDriver";
    [DataMember(Name = CoDriverProperty, EmitDefaultValue=false)]
    Contact mCoDriver;
    [PersistantProperty]
    public Contact CoDriver
    {
      get { return mCoDriver; }
      set { SetProperty<Contact>(ref mCoDriver, value); }
    }
    
    #endregion

    #region Contact BackupDriver

    public const string BackupDriverProperty = "BackupDriver";
    [DataMember(Name = BackupDriverProperty, EmitDefaultValue = false)]
    Contact mBackupDriver;
    [PersistantProperty]
    public Contact BackupDriver
    {
      get { return mBackupDriver; }
      set { SetProperty<Contact>(ref mBackupDriver, value); }
    }

    #endregion


    #region int WaybillCount

    public const string WaybillCountProperty = "WaybillCount";
    [DataMember(Name = WaybillCountProperty, EmitDefaultValue = false)]
    int mWaybillCount;
    [PersistantProperty]
    public int WaybillCount
    {
      get { return mWaybillCount; }
      private set { SetProperty<int>(ref mWaybillCount, value); }
    }

    #endregion

    #region int Items
    
    public const string ItemsProperty = "Items";
    [DataMember(Name = ItemsProperty, EmitDefaultValue = false)]
    int mItems;
    [PersistantProperty]
    public int Items
    {
      get { return mItems; }
      private set { SetProperty<int>(ref mItems, value); }
    }

    #endregion

    #region decimal Weight

    public const string WeightProperty = "Weight";
    [DataMember(Name = WeightProperty, EmitDefaultValue = false)]
    decimal mWeight;
    [PersistantProperty]
    public decimal Weight
    {
      get { return mWeight; }
      private set { SetProperty<decimal>(ref mWeight, value); }
    }

    #endregion

    #region int TripsheetCount

    public const string TripsheetCountProperty = "TripsheetCount";
    [DataMember(Name = TripsheetCountProperty, EmitDefaultValue = false)]
    int mTripsheetCount;
    [PersistantProperty]
    public int TripsheetCount
    {
      get { return mTripsheetCount; }
      private set { SetProperty<int>(ref mTripsheetCount, value); }
    }

    #endregion

    #region Vehicles

    #region Vehicle VehicleHorse

    public const string VehicleHorseProperty = "VehicleHorse";
    [DataMember(Name = VehicleHorseProperty, EmitDefaultValue = false)]
    Vehicle mVehicleHorse;
    [PersistantProperty]
    public Vehicle VehicleHorse
    {
      get { return mVehicleHorse; }
      set { SetProperty<Vehicle>(ref mVehicleHorse, value); }
    }

    #endregion


    #region Vehicle VehicleTrailer1

    public const string VehicleTrailer1Property = "VehicleTrailer1";
    [DataMember(Name = VehicleTrailer1Property, EmitDefaultValue = false)]
    Vehicle mVehicleTrailer1;
    [PersistantProperty]
    public Vehicle VehicleTrailer1
    {
      get { return mVehicleTrailer1; }
      set { SetProperty<Vehicle>(ref mVehicleTrailer1, value); }
    }

    #endregion

    #region Vehicle VehicleTrailer2

    public const string VehicleTrailer2Property = "VehicleTrailer2";
    [DataMember(Name = VehicleTrailer2Property, EmitDefaultValue = false)]
    Vehicle mVehicleTrailer2;
    [PersistantProperty]
    public Vehicle VehicleTrailer2
    {
      get { return mVehicleTrailer2; }
      set { SetProperty<Vehicle>(ref mVehicleTrailer2, value); }
    }

    #endregion

    #region Vehicle VehicleTrailer3

    public const string VehicleTrailer3Property = "VehicleTrailer3";
    [DataMember(Name = VehicleTrailer3Property, EmitDefaultValue = false)]
    Vehicle mVehicleTrailer3;
    [PersistantProperty]
    public Vehicle VehicleTrailer3
    {
      get { return mVehicleTrailer3; }
      set { SetProperty<Vehicle>(ref mVehicleTrailer3, value); }
    }

    #endregion

    #region Vehicle VehicleTrailer4

    public const string VehicleTrailer4Property = "VehicleTrailer4";
    [DataMember(Name = VehicleTrailer4Property, EmitDefaultValue = false)]
    Vehicle mVehicleTrailer4;
    [PersistantProperty]
    public Vehicle VehicleTrailer4
    {
      get { return mVehicleTrailer4; }
      set { SetProperty<Vehicle>(ref mVehicleTrailer4, value); }
    }

    #endregion

    #endregion

    #region string Notes

    public const string NotesProperty = "Notes";
    [DataMember(Name = NotesProperty, EmitDefaultValue = false)]
    string mNotes;
    [PersistantProperty]
    public string Notes
    {
      get { return mNotes; }
      set { SetProperty<string>(ref mNotes, value); }
    }

    #endregion

    #region string Reference

    public const string ReferenceProperty = "Reference";
    [DataMember(Name = ReferenceProperty, EmitDefaultValue = false)]
    string mReference;
    [PersistantProperty]
    public string Reference
    {
      get { return mReference; }
      set { SetProperty<string>(ref mReference, value); }
    }

    #endregion
  }
}

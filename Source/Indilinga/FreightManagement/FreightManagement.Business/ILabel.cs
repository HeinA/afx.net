﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Business
{
  public interface ILabel
  {
    PointF Offset { get; set; }
    void Print(string printer);
  }
}

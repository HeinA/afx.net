﻿using AccountManagement.Business;
using Afx.Business;
using Afx.Business.Collections;
using Afx.Business.Data;
using ContactManagement.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Business
{
  [PersistantObject(Schema = "FreightManagement")]
  public partial class AccountExtension : ExtensionObject<Account>
  {
    #region Associative BusinessObjectCollection<RatesConfigurationReference> Rates

    public const string RatesProperty = "Rates";
    AssociativeObjectCollection<AccountRatesConfiguration, RatesConfigurationReference> mRates;
    [PersistantCollection(AssociativeType = typeof(AccountRatesConfiguration))]
    public BusinessObjectCollection<RatesConfigurationReference> Rates
    {
      get
      {
        if (mRates == null) using (new EventStateSuppressor(this)) { mRates = new AssociativeObjectCollection<AccountRatesConfiguration, RatesConfigurationReference>(this); }
        return mRates;
      }
    }

    #endregion

    #region Address DefaultShipFromAddress

    public const string DefaultShipFromAddressProperty = "DefaultShipFromAddress";
    [DataMember(Name = DefaultShipFromAddressProperty, EmitDefaultValue = false)]
    [PersistantProperty]
    public Address DefaultShipFromAddress
    {
      get 
      {
        Address tempAddress = GetChildObject<Address>(Owner.Addresses);
        if (BusinessObject.IsNull(tempAddress)) 
        {
          tempAddress = Owner.Addresses.FirstOrDefault(x => x.AddressType.IsFlagged(AddressTypeFlags.ShipFromAddress));
        } 

        return tempAddress; 
      }
      set { SetChildObject<Address>(value); }
    }

    #endregion

    #region Address DefaultShipToAddress

    public const string DefaultShipToAddressProperty = "DefaultShipToAddress";
    [DataMember(Name = DefaultShipToAddressProperty, EmitDefaultValue = false)]
    [PersistantProperty]
    public Address DefaultShipToAddress
    {
      get
      {
        Address tempAddress = GetChildObject<Address>(Owner.Addresses);
        if (BusinessObject.IsNull(tempAddress))
        {
          tempAddress = Owner.Addresses.FirstOrDefault(x => x.AddressType.IsFlagged(AddressTypeFlags.ShipToAddress));
        }

        return tempAddress;
      }
      set { SetChildObject<Address>(value); }
    }

    #endregion

    #region TaxType TaxType

    public const string TaxTypeProperty = "TaxType";
    [PersistantProperty(IsCached = true)]
    public TaxType TaxType
    {
      get { return GetCachedObject<TaxType>(); }
      set { SetCachedObject<TaxType>(value); }
    }

    #endregion
  }
}

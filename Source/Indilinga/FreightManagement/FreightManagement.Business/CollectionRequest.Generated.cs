﻿using Afx.Business.Data;
using Afx.Business.Documents;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Business
{
  [DataContract(IsReference = true, Namespace = Namespace.FreightManagement)]
  public partial class CollectionRequest
  {
    public const string DocumentTypeIdentifier = "{953051e8-0444-4ed0-866d-4dcebb0d6803}";
    public class States
    {
      public const string Confirmed = "{9968848b-1973-41ef-9cd0-2f45de2c885b}";
      public const string Requested = "{991d7ed3-ab6f-4d07-8c2e-97d31ae2c6bb}";
    }

    protected CollectionRequest(DocumentType documentType)
      : base(documentType)
    {
    }
  }
}

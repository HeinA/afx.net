﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Business
{
  public interface ILevyCondition
  {
    bool IsApplicable(Waybill waybill);
  }
}

﻿using Afx.Business;
using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace FreightManagement.Business
{
  [PersistantObject(Schema = "FreightManagement")]
  public partial class RouteExtension : ExtensionObject<Route>
  {
    #region Constructors

    public RouteExtension()
    {
    }

    #endregion

    #region int ExpectedServiceDuration

    public const string ExpectedServiceDurationProperty = "ExpectedServiceDuration";
    [DataMember(Name = ExpectedServiceDurationProperty, EmitDefaultValue = false)]
    int mExpectedServiceDuration;
    [PersistantProperty]
    public int ExpectedServiceDuration
    {
      get { return mExpectedServiceDuration; }
      set { SetProperty<int>(ref mExpectedServiceDuration, value); }
    }

    #endregion
  }
}

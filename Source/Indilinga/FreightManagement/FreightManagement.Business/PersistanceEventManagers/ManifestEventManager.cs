﻿//using Afx.Business.Collections;
//using Afx.Business.Data;
//using System;
//using System.Collections.Generic;
//using System.ComponentModel.Composition;
//using System.Diagnostics;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;

//namespace FreightManagement.Business.PersistanceEventManagers
//{
//  [Export(typeof(IPersistanceEventManager))]
//  public class ManifestEventManager : PersistanceEventManager<Manifest>
//  {
//    public override void AfterPersist(Manifest obj, PersistanceFlags flags)
//    {
//      var col = obj.GetAssociativeObjects<ManifestWaybill>();

//      BasicCollection<WaybillTrace_Internal> traces = new BasicCollection<WaybillTrace_Internal>();

//      foreach(var o in col.Where(bo => bo.IsNew))
//      {
//        traces.Add(new WaybillTrace_Internal(o.Reference.Id, string.Format("Loaded on to Manifest {1}", o.Reference.WaybillNumber, obj.DocumentNumber)));
//      }

//      foreach (var o in col.Where(bo => bo.IsDeleted))
//      {
//        traces.Add(new WaybillTrace_Internal(o.Reference.Id, string.Format("Unloaded from Manifest {1}", o.Reference.WaybillNumber, obj.DocumentNumber)));
//      }

//      if (traces.Count > 0)
//      {
//        ObjectRepository<WaybillTrace_Internal> or = PersistanceManager.GetRepository<WaybillTrace_Internal>();
//        or.Persist(traces);
//      }

//      base.AfterPersist(obj, flags);
//    }
//  }
//}

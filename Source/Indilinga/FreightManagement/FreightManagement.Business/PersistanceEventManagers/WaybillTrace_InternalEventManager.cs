﻿using Afx.Business.Data;
using Afx.Business.ServiceModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Business.PersistanceEventManagers
{
  [Export(typeof(IPersistanceEventManager))]
  class WaybillTrace_InternalEventManager : PersistanceEventManager<WaybillTrace_Internal>
  {
    public override void AfterPersist(WaybillTrace_Internal obj, PersistanceFlags flags)
    {
      if (flags == PersistanceFlags.None && ObjectReplicationExtension.Current != null)
      {
        ObjectReplicationExtension.Current.ObjectQueue.Enqueue(obj);
      }

      base.AfterPersist(obj, flags);
    }
  }
}

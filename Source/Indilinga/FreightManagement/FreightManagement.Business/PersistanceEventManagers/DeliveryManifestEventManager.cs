﻿using Afx.Business.Data;
using Afx.Business.Documents;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Business.PersistanceEventManagers
{
  [Export(typeof(IPersistanceEventManager))]
  public class DeliveryManifestEventManager : PersistanceEventManager<DeliveryManifest>
  {
    public override void AfterPersist(DeliveryManifest obj, PersistanceFlags flags)
    {
      foreach (var wb in obj.Waybills)
      {
        if (wb.IsDirty && !string.IsNullOrWhiteSpace(wb.PODSignature))
        {
          var or = PersistanceManager.GetRepository<Waybill>();
          var waybill = or.GetInstance(wb.GlobalIdentifier);
          waybill.PODDate = wb.PODDate;
          waybill.PODSignature = wb.PODSignature;
          DocumentHelper.SetState(waybill, Waybill.States.Delivered);
          wb.IsDirty = false;
        }
      }

      base.AfterPersist(obj, flags);
    }
  }
}

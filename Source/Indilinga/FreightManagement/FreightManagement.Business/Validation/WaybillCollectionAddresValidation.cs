﻿using Afx.Business;
using Afx.Business.Validation;
using Geographics.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Business.Validation
{
  public class AddresValidation : ValidationAttribute
  {
    public AddresValidation(string message)
      :base(message)
    {
    }

    public override bool Validate(object value)
    {
      try
      {
        string address = (string)value;
        var l = Location.GetLocation(address);
        if (!BusinessObject.IsNull(l)) return true;
      }
      catch
      {
        return false;
      }
      return false;
    }
  }
}

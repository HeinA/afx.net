﻿using Afx.Business;
using Afx.Business.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Business
{
  [PersistantObject(Schema = "FreightManagement")]
  [Export(typeof(Setting))]
  [DataContract(IsReference = true, Namespace = FreightManagement.Business.Namespace.FreightManagement)]
  public class TextPrinterSetting : MachineSetting
  {
    #region Constructors

    public TextPrinterSetting()
    {
    }

    #endregion

    #region string Name

    public override string Name
    {
      get { return "Text Printer"; }
    }

    #endregion

    #region string SettingGroupIdentifier

    protected override string SettingGroupIdentifier
    {
      get { return "da82bb79-1b21-4186-832e-08a39476c141"; }
    }

    #endregion

    #region string PrinterName

    public const string PrinterNameProperty = "PrinterName";
    [DataMember(Name = PrinterNameProperty, EmitDefaultValue = false)]
    string mPrinterName;
    [PersistantProperty]
    public string PrinterName
    {
      get { return mPrinterName ?? string.Empty; }
      set { SetProperty<string>(ref mPrinterName, value); }
    }

    #endregion  }
  }
}

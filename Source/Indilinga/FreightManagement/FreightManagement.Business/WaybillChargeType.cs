﻿using Afx.Business;
using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Validation;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace FreightManagement.Business
{
  [PersistantObject(Schema = "FreightManagement")]
  public abstract partial class WaybillChargeType : BusinessObject, IExternalReferenceCollection
  {
    #region Constructors

    public WaybillChargeType()
    {
    }

    public WaybillChargeType(bool isNull)
      : base(isNull)
    {
    }

    #endregion
    
    #region string Text

    public const string TextProperty = "Text";
    [DataMember(Name = TextProperty, EmitDefaultValue = false)]
    string mText;
    [PersistantProperty]
    [Mandatory("Text is mandatory.")]
    public string Text
    {
      get { return mText; }
      set { SetProperty<string>(ref mText, value); }
    }

    #endregion

    #region bool IsVatApplicable

    public const string IsVatApplicableProperty = "IsVatApplicable";
    [DataMember(Name = IsVatApplicableProperty, EmitDefaultValue = false)]
    bool mIsVatApplicable;
    [PersistantProperty]
    public bool IsVatApplicable
    {
      get { return mIsVatApplicable; }
      set { SetProperty<bool>(ref mIsVatApplicable, value); }
    }

    #endregion

    //#region TaxType TaxType

    //public const string TaxTypeProperty = "TaxType";
    //[PersistantProperty(IsCached = true)]
    //public TaxType TaxType
    //{
    //  get { return GetCachedObject<TaxType>(); }
    //  set { SetCachedObject<TaxType>(value); }
    //}

    //#endregion

    //#region bool AllowTaxOverride

    //public const string AllowTaxOverrideProperty = "AllowTaxOverride";
    //[DataMember(Name = AllowTaxOverrideProperty, EmitDefaultValue = false)]
    //bool mAllowTaxOverride;
    //[PersistantProperty]
    //public bool AllowTaxOverride
    //{
    //  get { return mAllowTaxOverride; }
    //  set { SetProperty<bool>(ref mAllowTaxOverride, value); }
    //}

    //#endregion

    #region BusinessObjectCollection<WaybillChargeTypeReference> ExternalReferences

    public const string ExternalReferencesProperty = "ExternalReferences";
    BusinessObjectCollection<WaybillChargeTypeReference> mExternalReferences;
    [PersistantCollection]
    [DataMember]
    public BusinessObjectCollection<WaybillChargeTypeReference> ExternalReferences
    {
      get { return mExternalReferences ?? (mExternalReferences = new BusinessObjectCollection<WaybillChargeTypeReference>(this)); }
    }

    #endregion

    #region IExternalReferenceCollection

    public string ExternalReferenceText
    {
      get { return ExternalReferenceHelper.GetReferenceText(this); }
    }

    IList IExternalReferenceCollection.ExternalReferences
    {
      get { return ExternalReferences; }
    }

    IExternalReference IExternalReferenceCollection.CreateReference(ExternalSystem es, string reference)
    {
      return new WaybillChargeTypeReference() { ExternalSystem = es, Reference = reference };
    }

    #endregion
  }
}

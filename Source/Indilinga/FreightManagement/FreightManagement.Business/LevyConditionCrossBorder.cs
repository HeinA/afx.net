﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FreightManagement.Business;

namespace FreightManagement.Business
{
  [DisplayName("Cross Border")]
  [Export(typeof(ILevyCondition))]
  public class LevyConditionCrossBorder : ILevyCondition
  {
    public bool IsApplicable(Waybill waybill)
    {
      return !waybill.CollectionLocation.Country.Equals(waybill.DeliveryLocation.Country);
    }
  }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Business
{
  public interface IParcelLabel : ILabel
  {
    string WaybillNumber { get; set; }
    int StartIndex { get; set; }
    int EndIndex { get; set; }
    int TotalLabels { get; set; }
    DateTime Date { get; set; }
    string Consigner { get; set; }
    string Consignee { get; set; }
    string DeliveryCity { get; set; }
    string OrganizationalUnit { get; set; }
  }
}

﻿using Afx.Business;
using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace FreightManagement.Business
{
  [PersistantObject(Schema = "FreightManagement")]
  public partial class TransportManifestFee : BusinessObject<TransportManifest>
  {
    #region Constructors

    public TransportManifestFee()
    {
    }

    #endregion

    #region FeeType FeeType

    public const string FeeTypeProperty = "FeeType";
    [DataMember(Name = FeeTypeProperty, EmitDefaultValue = false)]
    [PersistantProperty]
    public FeeType FeeType
    {
      get { return GetCachedObject<FeeType>(); }
      set { SetCachedObject<FeeType>(value); }
    }

    #endregion


    #region DateTime PaymentDate

    public const string PaymentDateProperty = "PaymentDate";
    [DataMember(Name = PaymentDateProperty, EmitDefaultValue = false)]
    DateTime mPaymentDate = DateTime.Today;
    [PersistantProperty]
    public DateTime PaymentDate
    {
      get { return mPaymentDate; }
      set { SetProperty<DateTime>(ref mPaymentDate, value); }
    }

    #endregion

    #region decimal Amount

    public const string AmountProperty = "Amount";
    [DataMember(Name = AmountProperty, EmitDefaultValue = false)]
    decimal mAmount;
    [PersistantProperty]
    public decimal Amount
    {
      get { return mAmount; }
      set { SetProperty<decimal>(ref mAmount, value); }
    }

    #endregion

    #region bool Paid

    public const string PaidProperty = "Paid";
    [DataMember(Name = PaidProperty, EmitDefaultValue = false)]
    bool mPaid;
    [PersistantProperty]
    public bool Paid
    {
      get { return mPaid; }
      set { SetProperty<bool>(ref mPaid, value); }
    }

    #endregion

    #region bool ReceiptReceived

    public const string ReceiptReceivedProperty = "ReceiptReceived";
    [DataMember(Name = ReceiptReceivedProperty, EmitDefaultValue = false)]
    bool mReceiptReceived;
    [PersistantProperty]
    public bool ReceiptReceived
    {
      get { return mReceiptReceived; }
      set { SetProperty<bool>(ref mReceiptReceived, value); }
    }

    #endregion

    #region string ReceiptNumber

    public const string ReceiptNumberProperty = "ReceiptNumber";
    [DataMember(Name = ReceiptNumberProperty, EmitDefaultValue = false)]
    string mReceiptNumber;
    [PersistantProperty]
    public string ReceiptNumber
    {
      get { return mReceiptNumber; }
      set { SetProperty<string>(ref mReceiptNumber, value); }
    }

    #endregion

    #region bool MainDriver

    public const string MainDriverProperty = "MainDriver";
    [DataMember(Name = MainDriverProperty, EmitDefaultValue = false)]
    bool mMainDriver;
    [PersistantProperty]
    public bool MainDriver
    {
      get { return mMainDriver; }
      set { SetProperty<bool>(ref mMainDriver, value); }
    }

    #endregion
  
  }
}

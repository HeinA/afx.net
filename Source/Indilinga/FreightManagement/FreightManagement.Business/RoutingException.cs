﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Business
{
  [Serializable]
  public class RoutingException : Exception
  {
    protected RoutingException(SerializationInfo info, StreamingContext context)
      :base(info, context)
    {
    }

    public RoutingException(string message)
      : base(message)
    {
    }

    public RoutingException(string message, Exception innerException)
      : base(message, innerException)
    {
    }    
  }
}

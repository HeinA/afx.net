﻿using Afx.Business.Data;
using Afx.Business.Tabular;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Business
{
  [PersistantObject(Schema = "FreightManagement")]
  [DataContract(IsReference = true, Namespace = FreightManagement.Business.Namespace.FreightManagement)]
  public class DistanceRateTableDistanceCategory : TableRow<DistanceRateTable, DistanceRateTableRate> 
  {
    #region Constructors

    public DistanceRateTableDistanceCategory()
    {
    }

    public DistanceRateTableDistanceCategory(int distance)
    {
      Distance = distance;
    }

    #endregion

    #region int Distance

    public const string DistanceProperty = "Distance";
    [DataMember(Name = DistanceProperty, EmitDefaultValue = false)]
    int mDistance;
    [PersistantProperty]
    public int Distance
    {
      get { return mDistance; }
      set { SetProperty<int>(ref mDistance, value); }
    }

    #endregion

    #region string Text

    public const string TextProperty = "Text";
    public string Text
    {
      get { return string.Format("\u2265 {0} Km", Distance); }
    }

    #endregion

    public override object Index
    {
      get { return Distance; }
    }

    public bool GetCost(ref int units, ref decimal cost, ref decimal baseCharge, ref decimal rate, ref bool isFixed, ref bool isPercentage, ref WaybillChargeType chargeType)
    {
      int u = units;
      DistanceRateTableColumnCategory rtc = Owner.Columns.OfType<DistanceRateTableColumnCategory>().Where(c => c.StartUnit <= u && !c.IsMinimumCharge).OrderByDescending(c => c.StartUnit).FirstOrDefault();
      if (rtc == null) return false;

      DistanceRateTableRate rtr = Cells.FirstOrDefault(c => c.Column.Equals(rtc));
      if (rtc == null) return false;

      decimal costPrevious = 0;
      if (rtc.ThereAfter && !(rtc.IsFixedRate || rtc.IsPercentage))
      {
        int previousUnits = rtc.StartUnit-1;
        if (previousUnits < 0) return false;

        units = units-previousUnits;

        decimal ratePrevious = 0;
        decimal baseChargePrevious = 0;
        bool isFixedPrevious = false;
        bool isPercentagePrevious = false;
        WaybillChargeType chargeTypePrevious = null;
        if (!this.GetCost(ref previousUnits, ref costPrevious, ref baseChargePrevious, ref ratePrevious, ref isFixedPrevious, ref isPercentagePrevious, ref chargeTypePrevious))
        {
          return false;
        }
        baseCharge = costPrevious;
      }

      isFixed = rtc.IsFixedRate;
      isPercentage = rtc.IsPercentage;
      chargeType = rtc.CostComponentType;
      rate = rtr.Rate;
      if (rtc.IsPercentage)
      {
        cost = (decimal)units * (rtr.Rate / 100);
      }
      else
      {
        if (rtc.IsFixedRate) cost = rtr.Rate;
        else cost = rtr.Rate * units;
      }

      cost += costPrevious;

      DistanceRateTableColumnCategory rtcMin = Owner.Columns.OfType<DistanceRateTableColumnCategory>().Where(c => c.IsMinimumCharge).FirstOrDefault();
      if (rtcMin != null)
      {
        DistanceRateTableRate rtrMin = Cells.FirstOrDefault(c => c.Column.Equals(rtcMin));
        if (cost < rtrMin.Rate && cost != 0) cost = rtrMin.Rate;
      }

      return true;
    }
  }
}

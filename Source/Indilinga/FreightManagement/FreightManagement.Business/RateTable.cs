﻿using Afx.Business;
using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Tabular;
using Afx.Business.Validation;
using Geographics.Business;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace FreightManagement.Business
{
  [PersistantObject(Schema = "FreightManagement")]
  [DataContract(IsReference = true, Namespace = FreightManagement.Business.Namespace.FreightManagement)]
  public partial class RateTable : TabularBusinessObject<RateTableLocation, RateTableColumn, RateTableRate> 
  {
    #region Constructors

    public RateTable()
    {
    }

    public RateTable(RateTable table)
      : this(table, null)
    {
    }

    internal RateTable(RateTable table, Dictionary<WaybillChargeType, decimal> cargoTypeIncreases)
    {
      Origin = table.Origin;
      IsOmnidirectional = table.IsOmnidirectional;
      BillingUnit = table.BillingUnit;
      UnitOfMeasure = table.UnitOfMeasure;
      MinimumCharge = table.MinimumCharge;

      foreach (var l in table.Rows)
      {
        AddRow(new RateTableLocation(l.Location));
      }

      foreach (var c in table.Columns)
      {
        RateTableFee fee = c as RateTableFee;
        if (fee != null)
        {
          AddColumn(new RateTableFee(fee.ChargeType));
        }

        RateTableCategory cat = c as RateTableCategory;
        if (cat != null)
        {
          AddColumn(new RateTableCategory(cat, false));
        }
      }

      for (int iRow = 0; iRow < RowCollection.Count; iRow++)
      {
        for (int iColumn = 0; iColumn < ColumnCollection.Count; iColumn++)
        {
          var rtr = table[iRow, iColumn];
          decimal increase = 0;
          RateTableCategory rtc = rtr.Column as RateTableCategory;
          if (rtc != null)
          {
            increase = GetIncrease(rtc.CostComponentType, cargoTypeIncreases);
          }
          RateTableFee rtf = rtr.Column as RateTableFee;
          if (rtf != null)
          {
            increase = GetIncrease(rtf.ChargeType, cargoTypeIncreases);
          }
          this[iRow, iColumn].Rate = rtr.Rate + (rtr.Rate * increase / 100m);
        }
      }
    }

    #endregion

    decimal GetIncrease(WaybillChargeType ct, Dictionary<WaybillChargeType, decimal> cargoTypeIncreases)
    {
      if (cargoTypeIncreases == null) return 0;
      if (!cargoTypeIncreases.ContainsKey(ct)) return 0;
      return cargoTypeIncreases[ct];
    }

    #region Location Origin

    public const string OriginProperty = "Origin";
    [PersistantProperty]
    [Mandatory("Origin is mandatory.")]
    public Location Origin
    {
      get { return GetCachedObject<Location>(); }
      set { SetCachedObject<Location>(value); }
    }

    #endregion

    #region bool IsOmnidirectional

    public const string IsOmnidirectionalProperty = "IsOmnidirectional";
    [DataMember(Name = IsOmnidirectionalProperty, EmitDefaultValue = false)]
    bool mIsOmnidirectional;
    [PersistantProperty]
    public bool IsOmnidirectional
    {
      get { return mIsOmnidirectional; }
      set { SetProperty<bool>(ref mIsOmnidirectional, value); }
    }

    #endregion

    #region BillingUnit BillingUnit

    public const string BillingUnitProperty = "BillingUnit";
    [DataMember(Name = BillingUnitProperty, EmitDefaultValue = false)]
    BillingUnit mBillingUnit;
    [PersistantProperty]
    public BillingUnit BillingUnit
    {
      get { return mBillingUnit; }
      set { SetProperty<BillingUnit>(ref mBillingUnit, value); }
    }

    #endregion

    #region string UnitOfMeasure

    public const string UnitOfMeasureProperty = "UnitOfMeasure";
    [DataMember(Name = UnitOfMeasureProperty, EmitDefaultValue = false)]
    string mUnitOfMeasure;
    [PersistantProperty]
    public string UnitOfMeasure
    {
      get { return mUnitOfMeasure; }
      set { SetProperty<string>(ref mUnitOfMeasure, value); }
    }

    #endregion

    #region decimal MinimumCharge

    public const string MinimumChargeProperty = "MinimumCharge";
    [DataMember(Name = MinimumChargeProperty, EmitDefaultValue = false)]
    decimal mMinimumCharge;
    [PersistantProperty]
    public decimal MinimumCharge
    {
      get { return mMinimumCharge; }
      set { SetProperty<decimal>(ref mMinimumCharge, value); }
    }

    #endregion

    #region WaybillFlag RequiredFlag

    public const string RequiredFlagProperty = "RequiredFlag";
    [PersistantProperty(IsCached = true)]
    public WaybillFlag RequiredFlag
    {
      get { return GetCachedObject<WaybillFlag>(); }
      set { SetCachedObject<WaybillFlag>(value); }
    }

    #endregion
  }
}

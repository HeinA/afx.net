﻿using Afx.Business;
using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Documents;
using Afx.Business.Validation;
using FleetManagement.Business;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace FreightManagement.Business
{
  [PersistantObject(Schema = "FreightManagement", TableName = "vTransferManifestReference", IsReadOnly = true)]
  public partial class TransferManifestReference : BusinessObject 
  {
    #region Constructors

    public TransferManifestReference()
    {
    }

    #endregion

    #region string ManifestNumber

    public const string ManifestNumberProperty = "ManifestNumber";
    [DataMember(Name = ManifestNumberProperty, EmitDefaultValue = false)]
    string mManifestNumber;
    [PersistantProperty]
    public string ManifestNumber
    {
      get { return mManifestNumber; }
      private set { SetProperty<string>(ref mManifestNumber, value); }
    }

    #endregion

    #region DateTime ManifestDate

    public const string ManifestDateProperty = "ManifestDate";
    [DataMember(Name = ManifestDateProperty, EmitDefaultValue = false)]
    DateTime mManifestDate;
    [PersistantProperty]
    public DateTime ManifestDate
    {
      get { return mManifestDate; }
      private set { SetProperty<DateTime>(ref mManifestDate, value); }
    }

    #endregion

    #region DocumentTypeState ManifestState

    public const string ManifestStateProperty = "ManifestState";
    [PersistantProperty(IsCached = true)]
    public DocumentTypeState ManifestState
    {
      get { return GetCachedObject<DocumentTypeState>(); }
      set { SetCachedObject<DocumentTypeState>(value); }
    }

    #endregion

    #region Route Route

    public const string RouteProperty = "Route";
    [PersistantProperty(IsCached = true)]
    public Route Route
    {
      get { return GetCachedObject<Route>(); }
      set { SetCachedObject<Route>(value); }
    }

    #endregion

    #region int WaybillCount

    public const string WaybillCountProperty = "WaybillCount";
    [DataMember(Name = WaybillCountProperty, EmitDefaultValue = false)]
    int mWaybillCount;
    [PersistantProperty]
    public int WaybillCount
    {
      get { return mWaybillCount; }
      private set { SetProperty<int>(ref mWaybillCount, value); }
    }

    #endregion

    #region int Items
    
    public const string ItemsProperty = "Items";
    [DataMember(Name = ItemsProperty, EmitDefaultValue = false)]
    int mItems;
    [PersistantProperty]
    public int Items
    {
      get { return mItems; }
      private set { SetProperty<int>(ref mItems, value); }
    }

    #endregion

    #region decimal Weight

    public const string WeightProperty = "Weight";
    [DataMember(Name = WeightProperty, EmitDefaultValue = false)]
    decimal mWeight;
    [PersistantProperty]
    public decimal Weight
    {
      get { return mWeight; }
      private set { SetProperty<decimal>(ref mWeight, value); }
    }

    #endregion

    #region int TripsheetCount

    public const string TripsheetCountProperty = "TripsheetCount";
    [DataMember(Name = TripsheetCountProperty, EmitDefaultValue = false)]
    int mTripsheetCount;
    [PersistantProperty]
    public int TripsheetCount
    {
      get { return mTripsheetCount; }
      private set { SetProperty<int>(ref mTripsheetCount, value); }
    }

    #endregion
  }
}

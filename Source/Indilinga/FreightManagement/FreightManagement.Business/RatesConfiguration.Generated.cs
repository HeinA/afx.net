﻿using Afx.Business.Data;
using Afx.Business.Documents;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Business
{
  [DataContract(IsReference = true, Namespace = Namespace.FreightManagement)]
  public partial class RatesConfiguration
  {
    public const string DocumentTypeIdentifier = "{cef72918-4719-4479-a239-6832fedaa1ad}";
    public class States
    {
      public const string Active = "{04be82f2-3680-4639-8213-d849ade2a9da}";
      public const string Inactive = "{1ec71058-79df-4504-8130-19167852e2de}";
    }

    protected RatesConfiguration(DocumentType documentType)
      : base(documentType)
    {
    }
  }
}

﻿using Afx.Business;
using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Validation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace FreightManagement.Business
{
  [PersistantObject(Schema = "FreightManagement")]
  public partial class WaybillRoute : BusinessObject<Waybill> 
  {
    #region Constructors

    public WaybillRoute()
    {
    }

    public WaybillRoute(Route route, DistributionCenter dc)
    {
      Route = route;
      DistributionCenter = dc;
    }

    #endregion

    #region DistributionCenter DistributionCenter

    public const string DistributionCenterProperty = "DistributionCenter";
    [PersistantProperty(IsCached = true)]
    public DistributionCenter DistributionCenter
    {
      get { return GetCachedObject<DistributionCenter>(); }
      set { SetCachedObject<DistributionCenter>(value); }
    }

    #endregion

    #region DateTime? ArrivalTimestamp

    public const string ArrivalTimestampProperty = "ArrivalTimestamp";
    [DataMember(Name = ArrivalTimestampProperty, EmitDefaultValue = false)]
    DateTime? mArrivalTimestamp;
    [PersistantProperty]
    public DateTime? ArrivalTimestamp
    {
      get { return mArrivalTimestamp; }
      set { SetProperty<DateTime?>(ref mArrivalTimestamp, value); }
    }

    #endregion

    #region DateTime? DepartureTimestamp

    public const string DepartureTimestampProperty = "DepartureTimestamp";
    [DataMember(Name = DepartureTimestampProperty, EmitDefaultValue = false)]
    DateTime? mDepartureTimestamp;
    [PersistantProperty]
    public DateTime? DepartureTimestamp
    {
      get { return mDepartureTimestamp; }
      set { SetProperty<DateTime?>(ref mDepartureTimestamp, value); }
    }

    #endregion

    #region Route Route

    public const string RouteProperty = "Route";
    [PersistantProperty(IsCached = true)]
    public Route Route
    {
      get { return GetCachedObject<Route>(); }
      set { SetCachedObject<Route>(value); }
    }

    #endregion
  }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Business
{
  public class TextWaybill
  {
    public TextWaybill(Waybill wb)
    {
      mWaybill = wb;
    }

    Waybill mWaybill = null;

    string Center(string text, int columns)
    {
      if (text == null) text = string.Empty;
      int left = (columns - text.Length) / 2;
      return text.PadLeft(left + text.Length, ' ').PadRight(columns, ' ');
    }

    string Fit(string text, int columns)
    {
      if (text == null) text = string.Empty;
      return text.PadRight(columns, ' ').Substring(0, columns);
    }

    string[] GetLines(string text)
    {
      if (string.IsNullOrWhiteSpace(text)) return new string[0];
      return text.Replace("\r\n", "\n").Split(new char[] { '\n' }, StringSplitOptions.RemoveEmptyEntries);
    }

    public override string ToString()
    {
      using (StringWriter sw = new StringWriter())
      {
        sw.Write((char)27);
        sw.Write('@');
        sw.Write((char)27);
        sw.Write((char)67);
        sw.Write((char)66);
        sw.Write("{0}{1}", Center("Consignment Note", 78), "\r\n\r\n");
        sw.Write("{0}{1}{2}", "Waybill".PadRight(15), mWaybill.DocumentNumber, "\r\n");
        sw.Write("{0}{1:dd/MM/yyyy}{2}", "Date".PadRight(15), mWaybill.DocumentDate, "\r\n");
        sw.Write("{0}{1}{2}", "Account".PadRight(15), mWaybill.Account.AccountNumber, "\r\n");
        sw.Write("{0}{1}{2}", "Reference".PadRight(15), mWaybill.ClientReferenceNumber, "\r\n");
        sw.Write("{0}{1:0.00}{2}", "Charge Mass".PadRight(15), mWaybill.BillingWeight, "\r\n"); 
        sw.Write("{0}{1:0.00}{2}", "Total".PadRight(15), mWaybill.Charge, "\r\n\r\n");
        sw.Write("Shipper".PadRight(39));
        sw.Write("{0}{1}", "Consignee", "\r\n");
        sw.Write("-------".PadRight(39));
        sw.Write("{0}{1}", "---------", "\r\n");

        string[] shipAddress = GetLines(mWaybill.CollectionAddress);
        string[] deliveryAddress = GetLines(mWaybill.DeliveryAddress);
        int max = shipAddress.Length;
        if (deliveryAddress.Length > max) max = deliveryAddress.Length;

        sw.Write(mWaybill.Consigner.PadRight(39));
        sw.Write("{0}{1}", mWaybill.Consignee, "\r\n");

        for (int i = 0; i < max; i++)
        {
          if (i < shipAddress.Length) sw.Write(Fit(shipAddress[i], 39));
          else sw.Write(string.Empty.PadRight(39));

          if (i < deliveryAddress.Length) sw.Write("{0}{1}", Fit(deliveryAddress[i], 39), "\r\n");
          else sw.Write("{0}{1}", string.Empty.PadRight(39), "\r\n");
        }

        sw.Write("\r\nInstruction\r\n");
        sw.Write("-----------\r\n");

        string[] instructions = GetLines(mWaybill.Notes);
        for (int i = 0; i < instructions.Length; i++)
        {
          sw.Write("{0}{1}", Fit(instructions[i], 78), "\r\n");
        }

        sw.Write("\r\n{0}{1}{2}{3}{4}{5}{6}\r\n", "Pieces".PadRight(8), "Description".PadRight(30), "X".PadLeft(8), "Y".PadLeft(8), "Z".PadLeft(8), "Volume".PadLeft(8), "Mass".PadLeft(8));
        sw.Write("{0}{1}{2}{3}{4}{5}{6}\r\n", "------".PadRight(8), "-----------".PadRight(30), "-".PadLeft(8), "-".PadLeft(8), "-".PadLeft(8), "------".PadLeft(8), "----".PadLeft(8));
        foreach (var item in mWaybill.Items)
        {
          if (!string.IsNullOrWhiteSpace(item.Description)) sw.Write("{0}{1}{2}{3}{4}{5}{6}\r\n", Fit(string.Format("{0:0}", item.Items), 8), Fit(item.Description, 30), Fit(item.VolumeX.ToString("0.00").PadLeft(8), 8), Fit(item.VolumeY.ToString("0.00").PadLeft(8), 8), Fit(item.VolumeZ.ToString("0.00").PadLeft(8), 8), Fit(item.VolumetricWeight.ToString("0.00").PadLeft(8), 8), Fit(item.PhysicalWeight.ToString("0.00").PadLeft(8), 8));
          else sw.Write("{0}{1}{2}{3}{4}{5}{6}\r\n", Fit(string.Format("{0:0}", item.Items), 8), Fit(item.CargoType.Text, 30), Fit(item.VolumeX.ToString("0.00").PadLeft(8), 8), Fit(item.VolumeY.ToString("0.00").PadLeft(8), 8), Fit(item.VolumeZ.ToString("0.00").PadLeft(8), 8), Fit(item.VolumetricWeight.ToString("0.00").PadLeft(8), 8), Fit(item.PhysicalWeight.ToString("0.00").PadLeft(8), 8));
        }

        sw.Write("\r\n\r\n\r\n{0}{1}\r\n", Fit("Sender", 39), Fit("Carrier", 39));
        sw.Write("{0}{1}\r\n\r\n", Fit("------", 39), Fit("-------", 39));
        sw.Write("{0}{1}{2}{3}\r\n\r\n", Fit("Sender Name", 12), Fit("________________________", 27), Fit("Driver Name", 12), Fit("________________________", 27));
        sw.Write("{0}{1}{2}{3}\r\n\r\n", Fit("Date", 12), Fit("________________________", 27), Fit("Driver Sign", 12), Fit("________________________", 27));
        sw.Write("{0}{1}\r\n\r\n\r\n", Fit("Signiture", 12), Fit("________________________", 27));

        sw.Write("{0}\r\n", Fit("Proof of delivery", 78));
        sw.Write("{0}\r\n\r\n", Fit("-----------------", 39));
        sw.Write("{0}{1}{2}{3}\r\n\r\n", Fit("Name", 12), Fit("________________________", 27), Fit("Signature", 12), Fit("________________________", 27));
        sw.Write("{0}{1}{2}{3}\r\n\r\n", Fit("Date", 12), Fit("________________________", 27), Fit("Time", 12), Fit("________________________", 27));
        sw.Write("{0}{1}\r\n\r\n", Fit("Remarks", 12), "".PadRight(66, '_'));
        sw.Write("{0}{1}\r\n\r\n", Fit("       ", 12), "".PadRight(66, '_'));

        sw.Write("\f");

        return sw.ToString();
      }
    }
  }
}

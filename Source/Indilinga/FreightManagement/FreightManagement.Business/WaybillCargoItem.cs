﻿using Afx.Business;
using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Validation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace FreightManagement.Business
{
  [PersistantObject(Schema = "FreightManagement")]
  public partial class WaybillCargoItem : BusinessObject<Waybill> 
  {
    bool mSetVolumetricDimensions;
    bool SetVolumetricDimensions
    {
      get { return mSetVolumetricDimensions; }
      set { mSetVolumetricDimensions = value; }
    }

    bool mSetVolumetricWeight;
    bool SetVolumetricWeight
    {
      get { return mSetVolumetricWeight; }
      set { mSetVolumetricWeight = value; }
    }

    #region Constructors

    public WaybillCargoItem()
    {
      SetVolumetricDimensions = true;
      SetVolumetricWeight = true;
    }

    #endregion

    protected override void OnDeserialized()
    {
      SetVolumetricDimensions = true;
      SetVolumetricWeight = true;

      base.OnDeserialized();
    }

    #region CargoType CargoType

    public const string CargoTypeProperty = "CargoType";
    [PersistantProperty]
    public CargoType CargoType
    {
      get { return GetCachedObject<CargoType>(); }
      set { SetCachedObject<CargoType>(value); }
    }

    #endregion

    #region int Items

    public const string ItemsProperty = "Items";
    [DataMember(Name = ItemsProperty, EmitDefaultValue = false)]
    int mItems = 1;
    [PersistantProperty]

    public int Items
    {
      get { return mItems; }
      set
      {
        if (!SetVolumetricDimensions) return;
        if (SetProperty<int>(ref mItems, value))
        {
          SetVolumetricDimensions = false;
          if (Owner.VolumetricFactor == 0) VolumetricWeight = 0;
          else VolumetricWeight = (Items * VolumeX * VolumeY * VolumeZ) / Owner.VolumetricFactor;
          SetVolumetricDimensions = true;
        }
      }
    }

    #endregion

    #region string Description

    public const string DescriptionProperty = "Description";
    [DataMember(Name = DescriptionProperty, EmitDefaultValue = false)]
    string mDescription;
    [PersistantProperty]
    public string Description
    {
      get { return mDescription; }
      set { SetProperty<string>(ref mDescription, value); }
    }

    #endregion

    #region decimal PhysicalWeight

    public const string PhysicalWeightProperty = "PhysicalWeight";
    [DataMember(Name = PhysicalWeightProperty, EmitDefaultValue = false)]
    decimal mPhysicalWeight;
    [PersistantProperty]
    [Mandatory("Physical Weight is mandatory")]
    public decimal PhysicalWeight
    {
      get { return mPhysicalWeight; }
      set { SetProperty<decimal>(ref mPhysicalWeight, value); }
    }

    #endregion

    #region decimal VolumetricWeight

    public const string VolumetricWeightProperty = "VolumetricWeight";
    [DataMember(Name = VolumetricWeightProperty, EmitDefaultValue = false)]
    decimal mVolumetricWeight;
    [PersistantProperty]
    public decimal VolumetricWeight
    {
      get { return mVolumetricWeight; }
      set
      {
        if (!SetVolumetricWeight) return;
        if (SetProperty<decimal>(ref mVolumetricWeight, decimal.Round(value, 2)))
        {
          SetVolumetricWeight = false;
          VolumeX = 0;
          VolumeY = 0;
          VolumeZ = 0;
          SetVolumetricWeight = true;
        }
      }
    }

    #endregion

    #region decimal VolumeX

    public const string VolumeXProperty = "VolumeX";
    [DataMember(Name = VolumeXProperty, EmitDefaultValue = false)]
    decimal mVolumeX;
    [PersistantProperty]
    public decimal VolumeX
    {
      get { return mVolumeX; }
      set
      {
        if (!SetVolumetricDimensions) return;
        if (SetProperty<decimal>(ref mVolumeX, value))
        {
          CalculateVolumetricWeight();
        }
      }
    }

    #endregion

    #region decimal VolumeY

    public const string VolumeYProperty = "VolumeY";
    [DataMember(Name = VolumeYProperty, EmitDefaultValue = false)]
    decimal mVolumeY;
    [PersistantProperty]
    public decimal VolumeY
    {
      get { return mVolumeY; }
      set
      {
        if (!SetVolumetricDimensions) return;
        if (SetProperty<decimal>(ref mVolumeY, value))
        {
          CalculateVolumetricWeight();
        }
      }
    }

    #endregion

    #region decimal VolumeZ

    public const string VolumeZProperty = "VolumeZ";
    [DataMember(Name = VolumeZProperty, EmitDefaultValue = false)]
    decimal mVolumeZ;
    [PersistantProperty]
    public decimal VolumeZ
    {
      get { return mVolumeZ; }
      set
      {
        if (!SetVolumetricDimensions) return;
        if (SetProperty<decimal>(ref mVolumeZ, value))
        {
          CalculateVolumetricWeight();
        }
      }
    }

    #endregion

    #region decimal DeclaredValue

    public const string DeclaredValueProperty = "DeclaredValue";
    [DataMember(Name = DeclaredValueProperty, EmitDefaultValue = false)]
    decimal mDeclaredValue;
    [PersistantProperty]
    public decimal DeclaredValue
    {
      get { return mDeclaredValue; }
      set { SetProperty<decimal>(ref mDeclaredValue, value); }
    }

    #endregion

    #region decimal BillableUnits

    [PersistantProperty(Write = true)]
    public decimal BillableUnits
    {
      get
      {
        switch (CargoType.BillingUnit)
        {
          case BillingUnit.Weight:
            if (VolumetricWeight > PhysicalWeight) return VolumetricWeight;
            return PhysicalWeight;

          case BillingUnit.Quantity:
            if (CargoType.IsSingleUnit) return 1;
            return Items;

          case BillingUnit.Value:
            return DeclaredValue;

          default:
            throw new InvalidOperationException("Cargo Type has an invalid Billing Unit.");
        }
      }
    }

    #endregion

    #region void CalculateVolumetricWeight(...)

    public void CalculateVolumetricWeight()
    {
      if (Owner.VolumetricFactor == 0) return; // throw new InvalidOperationException("Volumetric Factor may not be zero.");

      if (VolumeX > 0 && VolumeY > 0 && VolumeZ > 0)
      {
        SetVolumetricDimensions = false;
        VolumetricWeight = (Items * VolumeX * VolumeY * VolumeZ) / Owner.VolumetricFactor;
        SetVolumetricDimensions = true;
      }
    }

    #endregion
  }
}

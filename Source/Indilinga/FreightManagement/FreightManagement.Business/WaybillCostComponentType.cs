﻿using Afx.Business;
using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Validation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace FreightManagement.Business
{
  [PersistantObject(Schema = "FreightManagement", HasFlags=true)]
  [Cache]
  public partial class WaybillCostComponentType : WaybillChargeType, IRevisable 
  {
    #region Constructors

    public WaybillCostComponentType()
    {
    }

    public WaybillCostComponentType(bool isNull)
      : base(isNull)
    {
    }

    #endregion

    #region int Revision

    public const string RevisionProperty = "Revision";
    [DataMember(Name = RevisionProperty, EmitDefaultValue = false)]
    int mRevision;
    [PersistantProperty]
    public int Revision
    {
      get { return mRevision; }
      set { SetProperty<int>(ref mRevision, value); }
    }

    #endregion

    #region bool IsFixedFee

    public const string IsFixedFeeProperty = "IsFixedFee";
    [DataMember(Name = IsFixedFeeProperty, EmitDefaultValue = false)]
    bool mIsFixedFee;
    [PersistantProperty]
    public bool IsFixedFee
    {
      get { return mIsFixedFee; }
      set { SetProperty<bool>(ref mIsFixedFee, value); }
    }

    #endregion

    #region bool ChargeOnce

    public const string ChargeOnceProperty = "ChargeOnce";
    [DataMember(Name = ChargeOnceProperty, EmitDefaultValue = false)]
    bool mChargeOnce;
    [PersistantProperty]
    public bool ChargeOnce
    {
      get { return mChargeOnce; }
      set { SetProperty<bool>(ref mChargeOnce, value); }
    }

    #endregion

    #region bool IsInsurance

    public const string IsInsuranceProperty = "IsInsurance";
    [DataMember(Name = IsInsuranceProperty, EmitDefaultValue = false)]
    bool mIsInsurance;
    [PersistantProperty]
    public bool IsInsurance
    {
      get { return mIsInsurance; }
      set { SetProperty<bool>(ref mIsInsurance, value); }
    }

    #endregion
  }
}

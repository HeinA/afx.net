﻿using Afx.Business;
using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace FreightManagement.Business
{
  [PersistantObject(Schema = "FreightManagement")]
  public partial class WaybillReceiptWaybill : AssociativeObject<WaybillReceipt, WaybillReference>
  {
    #region Associative BusinessObjectCollection<WaybillFlag> WaybillFlags

    public const string WaybillFlagsProperty = "WaybillFlags";
    AssociativeObjectCollection<WaybillReceiptWaybillWaybillFlag, WaybillFlag> mWaybillFlags;
    [PersistantCollection(AssociativeType = typeof(WaybillReceiptWaybillWaybillFlag))]
    public BusinessObjectCollection<WaybillFlag> WaybillFlags
    {
      get
      {
        if (mWaybillFlags == null) using (new EventStateSuppressor(this)) { mWaybillFlags = new AssociativeObjectCollection<WaybillReceiptWaybillWaybillFlag, WaybillFlag>(this); }
        return mWaybillFlags;
      }
    }

    #endregion

    #region Bay Bay

    public const string BayProperty = "Bay";
    [PersistantProperty(IsCached = true)]
    public Bay Bay
    {
      get { return GetCachedObject<Bay>(); }
      set { SetCachedObject<Bay>(value); }
    }

    #endregion

    #region string Comment

    public const string CommentProperty = "Comment";
    [DataMember(Name = CommentProperty, EmitDefaultValue = false)]
    string mComment;
    [PersistantProperty]
    public string Comment
    {
      get { return mComment; }
      set { SetProperty<string>(ref mComment, value); }
    }

    #endregion

    protected override void OnReferenceChanged()
    {
      foreach (var wf in Reference.WaybillFlags)
      {
        WaybillFlags.Add(wf);
      }

      Bay = Setting.GetSetting<DefaultBaySetting>().ReceiptingBay;

      base.OnReferenceChanged();
    }
  }
}

﻿using Afx.Business;
using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace FreightManagement.Business
{
  [PersistantObject(Schema = "FreightManagement")]
  public partial class RatesConfigurationRevisionCargoType : AssociativeObject<RatesConfigurationRevision, CargoType>
  {
    #region Associative BusinessObjectCollection<RateTable> Tables

    public const string TablesProperty = "Tables";
    AssociativeObjectCollection<RatesConfigurationRevisionCargoTypeTable, RateTable> mTables;
    [PersistantCollection(AssociativeType = typeof(RatesConfigurationRevisionCargoTypeTable))]
    public BusinessObjectCollection<RateTable> Tables
    {
      get
      {
        if (mTables == null) using (new EventStateSuppressor(this)) { mTables = new AssociativeObjectCollection<RatesConfigurationRevisionCargoTypeTable, RateTable>(this); }
        return mTables;
      }
    }

    #endregion

    #region Associative BusinessObjectCollection<DistanceRateTable> DistanceTables

    public const string DistanceTablesProperty = "DistanceTables";
    AssociativeObjectCollection<RatesConfigurationRevisionCargoTypeDistanceTable, DistanceRateTable> mDistanceTables;
    [PersistantCollection(AssociativeType = typeof(RatesConfigurationRevisionCargoTypeDistanceTable))]
    public BusinessObjectCollection<DistanceRateTable> DistanceTables
    {
      get
      {
        if (mDistanceTables == null) using (new EventStateSuppressor(this)) { mDistanceTables = new AssociativeObjectCollection<RatesConfigurationRevisionCargoTypeDistanceTable, DistanceRateTable>(this); }
        return mDistanceTables;
      }
    }

    #endregion
  }
}

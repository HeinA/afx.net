﻿using Afx.Business;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Documents;
using Afx.Business.Security;
using Afx.Business.Validation;
using ContactManagement.Business;
using FleetManagement.Business;
using Geographics.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Business
{
  [PersistantObject(Schema = "FreightManagement")]
  public partial class TransportManifest : Document, IVehicleCollection
  {
    public TransportManifest()
      : base(Cache.Instance.GetObject<DocumentType>(DocumentTypeIdentifier))
    {
    }

    #region Location Origin

    public const string OriginProperty = "Origin";
    [PersistantProperty(IsCached = true)]
    [Mandatory("Origin Location is mandatory.")]
    public Location Origin
    {
      get { return GetCachedObject<Location>(); }
      set { SetCachedObject<Location>(value); }
    }

    #endregion

    #region Location Destination

    public const string DestinationProperty = "Destination";
    [PersistantProperty(IsCached = true)]
    [Mandatory("Destination Location is mandatory.")]
    public Location Destination
    {
      get { return GetCachedObject<Location>(); }
      set { SetCachedObject<Location>(value); }
    }

    #endregion

    #region string Route

    public const string RouteProperty = "Route";
    [DataMember(Name = RouteProperty, EmitDefaultValue = false)]
    string mRoute;
    [PersistantProperty]
    public string Route
    {
      get { return mRoute; }
      set { SetProperty<string>(ref mRoute, value); }
    }

    #endregion

    #region Contact Driver

    public const string DriverProperty = "Driver";
    [PersistantProperty(IsCached = true)]
    public Contact Driver
    {
      get { return GetCachedObject<Contact>(); }
      set { SetCachedObject<Contact>(value); }
    }

    #endregion

    #region Contact CoDriver

    public const string CoDriverProperty = "CoDriver";
    [DataMember(Name = CoDriverProperty, EmitDefaultValue = false)]
    Contact mCoDriver;
    [PersistantProperty]
    public Contact CoDriver
    {
      get { return mCoDriver; }
      set { SetProperty<Contact>(ref mCoDriver, value); }
    }

    #endregion

    #region Contact BackupDriver

    public const string BackupDriverProperty = "BackupDriver";
    [DataMember(Name = BackupDriverProperty, EmitDefaultValue = false)]
    Contact mBackupDriver;
    [PersistantProperty]
    public Contact BackupDriver
    {
      get { return mBackupDriver; }
      set { SetProperty<Contact>(ref mBackupDriver, value); }
    }

    #endregion

    #region Associative BusinessObjectCollection<WaybillReference> Waybills

    public const string WaybillsProperty = "Waybills";
    AssociativeObjectCollection<TransportManifestWaybill, WaybillReference> mWaybills;
    [PersistantCollection(AssociativeType = typeof(TransportManifestWaybill))]
    public BusinessObjectCollection<WaybillReference> Waybills
    {
      get
      {
        if (mWaybills == null) using (new EventStateSuppressor(this)) { mWaybills = new AssociativeObjectCollection<TransportManifestWaybill, WaybillReference>(this); }
        return mWaybills;
      }
    }

    #endregion

    #region void OnCompositionChanged(...)

    protected override void OnCompositionChanged(CompositionChangedType compositionChangedType, string propertyName, object originalSource, BusinessObject item)
    {
      if ((compositionChangedType == CompositionChangedType.ChildAdded || compositionChangedType == CompositionChangedType.ChildRemoved) && propertyName == WaybillsProperty)
      {
        RecalculateItems();
        RecalculateWeight();

        if (Waybills.Count == 0)
        {
          Origin = null;
          Destination = null;
        }

        if (compositionChangedType == CompositionChangedType.ChildAdded)
        {
          WaybillReference wb = item as WaybillReference;

          if (this.Waybills.Count == 1)
          {
            this.Origin = wb.FullCollectionLocation.City;
            this.Destination = wb.FullDeliveryLocation.City;
          }
          else
          {
            if (this.Origin != wb.FullCollectionLocation.City)
            {
              throw new InvalidWaybillException("The origin does not match.");
            }

            if (this.Destination != wb.FullDeliveryLocation.City)
            {
              throw new InvalidWaybillException("The destination does not match.");
            }
          }
        }
      }

      OnPropertyChanged(WaybillCountProperty);

      base.OnCompositionChanged(compositionChangedType, propertyName, originalSource, item);
    }

    #endregion

    #region int WaybillCount

    public const string WaybillCountProperty = "WaybillCount";
    [PersistantProperty(IgnoreConcurrency = true)]
    public int WaybillCount
    {
      get { return Waybills.Count; }
    }

    #endregion

    #region int Items

    void RecalculateItems()
    {
      int i = 0;
      foreach (var wr in Waybills)
      {
        i += wr.Items;
      }
      Items = i;
    }

    public const string ItemsProperty = "Items";
    [DataMember(Name = ItemsProperty, EmitDefaultValue = false)]
    int mItems;
    [PersistantProperty(IgnoreConcurrency = true)]
    public int Items
    {
      get { return mItems; }
      private set { SetProperty<int>(ref mItems, value); }
    }

    #endregion

    #region decimal Weight

    void RecalculateWeight()
    {
      decimal i = 0;
      foreach (var wr in Waybills)
      {
        i += wr.PhysicalWeight;
      }
      Weight = i;
    }

    public const string WeightProperty = "Weight";
    [DataMember(Name = WeightProperty, EmitDefaultValue = false)]
    decimal mWeight;
    [PersistantProperty(IgnoreConcurrency = true)]
    public decimal Weight
    {
      get { return mWeight; }
      private set { SetProperty<decimal>(ref mWeight, value); }
    }

    #endregion

    #region DateTime? ScheduledLoadingDate

    public const string ScheduledLoadingDateProperty = "ScheduledLoadingDate";
    [DataMember(Name = ScheduledLoadingDateProperty, EmitDefaultValue = false)]
    DateTime? mScheduledLoadingDate;
    [PersistantProperty]
    public DateTime? ScheduledLoadingDate
    {
      get { return mScheduledLoadingDate; }
      set { SetProperty<DateTime?>(ref mScheduledLoadingDate, value == null ? null : (DateTime?)value.Value.Date); }
    }

    #endregion

    #region SlotTime ScheduledLoadingSlot

    public const string ScheduledLoadingSlotProperty = "ScheduledLoadingSlot";
    [PersistantProperty(IsCached = true)]
    public SlotTime ScheduledLoadingSlot
    {
      get { return GetCachedObject<SlotTime>(); }
      set { SetCachedObject<SlotTime>(value); }
    }

    #endregion

    #region DateTime? ScheduledOffloadingDate

    public const string ScheduledOffloadingDateProperty = "ScheduledOffloadingDate";
    [DataMember(Name = ScheduledOffloadingDateProperty, EmitDefaultValue = false)]
    DateTime? mScheduledOffloadingDate;
    [PersistantProperty]
    public DateTime? ScheduledOffloadingDate
    {
      get { return mScheduledOffloadingDate; }
      set { SetProperty<DateTime?>(ref mScheduledOffloadingDate, value == null ? null : (DateTime?)value.Value.Date); }
    }

    #endregion

    #region SlotTime ScheduledOffloadingSlot

    public const string ScheduledOffloadingSlotProperty = "ScheduledOffloadingSlot";
    [PersistantProperty(IsCached = true)]
    public SlotTime ScheduledOffloadingSlot
    {
      get { return GetCachedObject<SlotTime>(); }
      set { SetCachedObject<SlotTime>(value); }
    }

    #endregion

    #region DateTime? ActualLoadingTime

    public const string ActualLoadingTimeProperty = "ActualLoadingTime";
    [DataMember(Name = ActualLoadingTimeProperty, EmitDefaultValue = false)]
    DateTime? mActualLoadingTime;
    [PersistantProperty]
    public DateTime? ActualLoadingTime
    {
      get { return mActualLoadingTime; }
      set { SetProperty<DateTime?>(ref mActualLoadingTime, value); }
    }

    #endregion

    #region DateTime? ActualOffloadingTime

    public const string ActualOffloadingTimeProperty = "ActualOffloadingTime";
    [DataMember(Name = ActualOffloadingTimeProperty, EmitDefaultValue = false)]
    DateTime? mActualOffloadingTime;
    [PersistantProperty]
    public DateTime? ActualOffloadingTime
    {
      get { return mActualOffloadingTime; }
      set { SetProperty<DateTime?>(ref mActualOffloadingTime, value); }
    }

    #endregion

    #region DateTime? DepartureTime

    public const string DepartureTimeProperty = "DepartureTime";
    [DataMember(Name = DepartureTimeProperty, EmitDefaultValue = false)]
    DateTime? mDepartureTime;
    [PersistantProperty]
    public DateTime? DepartureTime
    {
      get { return mDepartureTime; }
      set { SetProperty<DateTime?>(ref mDepartureTime, value); }
    }

    #endregion

    #region DateTime? ArrivalTime

    public const string ArrivalTimeProperty = "ArrivalTime";
    [DataMember(Name = ArrivalTimeProperty, EmitDefaultValue = false)]
    DateTime? mArrivalTime;
    [PersistantProperty]
    public DateTime? ArrivalTime
    {
      get { return mArrivalTime; }
      set { SetProperty<DateTime?>(ref mArrivalTime, value); }
    }

    #endregion

    #region DateTime? ActualLoadingCompleted

    public const string ActualLoadingCompletedProperty = "ActualLoadingCompleted";
    [DataMember(Name = ActualLoadingCompletedProperty, EmitDefaultValue = false)]
    DateTime? mActualLoadingCompleted;
    [PersistantProperty]
    public DateTime? ActualLoadingCompleted
    {
      get { return mActualLoadingCompleted; }
      set { SetProperty<DateTime?>(ref mActualLoadingCompleted, value); }
    }

    #endregion

    #region DateTime? ActualOffloadingCompleted
    
    public const string ActualOffloadingCompletedProperty = "ActualOffloadingCompleted";
    [DataMember(Name = ActualOffloadingCompletedProperty, EmitDefaultValue=false)]
    DateTime? mActualOffloadingCompleted;
    [PersistantProperty]
    public DateTime? ActualOffloadingCompleted
    {
      get { return mActualOffloadingCompleted; }
      set { SetProperty<DateTime?>(ref mActualOffloadingCompleted, value); }
    }
    
    #endregion

    #region Associative BusinessObjectCollection<Vehicle> Vehicles

    public const string VehiclesProperty = "Vehicles";
    AssociativeObjectCollection<TransportManifestVehicle, Vehicle> mVehicles;
    [PersistantCollection(AssociativeType = typeof(TransportManifestVehicle))]
    [Mandatory("Vehicles are mandatory")]
    public BusinessObjectCollection<Vehicle> Vehicles
    {
      get
      {
        if (mVehicles == null) using (new EventStateSuppressor(this)) { mVehicles = new AssociativeObjectCollection<TransportManifestVehicle, Vehicle>(this); }
        return mVehicles;
      }
    }

    #endregion

    #region BusinessObjectCollection<TransportManifestFee> Fees

    public const string FeesProperty = "Fees";
    BusinessObjectCollection<TransportManifestFee> mFees;
    [PersistantCollection]
    [DataMember]
    public BusinessObjectCollection<TransportManifestFee> Fees
    {
      get { return mFees ?? (mFees = new BusinessObjectCollection<TransportManifestFee>(this)); }
    }

    #endregion

    #region string Notes

    public const string NotesProperty = "Notes";
    [DataMember(Name = NotesProperty, EmitDefaultValue = false)]
    string mNotes;
    [PersistantProperty]
    public string Notes
    {
      get { return mNotes; }
      set { SetProperty<string>(ref mNotes, value); }
    }

    #endregion

    #region string Reference

    public const string ReferenceProperty = "Reference";
    [DataMember(Name = ReferenceProperty, EmitDefaultValue = false)]
    string mReference;
    [PersistantProperty]
    public string Reference
    {
      get { return mReference; }
      set { SetProperty<string>(ref mReference, value); }
    }

    #endregion


    #region decimal FeesNightout

    public const string FeesNightoutProperty = "FeesNightout";
    [DataMember(Name = FeesNightoutProperty, EmitDefaultValue = false)]
    decimal mFeesNightout;
    [PersistantProperty]
    public decimal FeesNightout
    {
      get { return mFeesNightout; }
      set { SetProperty<decimal>(ref mFeesNightout, value); }
    }

    #endregion

    #region decimal FeesTarpaullin

    public const string FeesTarpaullinProperty = "FeesTarpaullin";
    [DataMember(Name = FeesTarpaullinProperty, EmitDefaultValue = false)]
    decimal mFeesTarpaullin;
    [PersistantProperty]
    public decimal FeesTarpaullin
    {
      get { return mFeesTarpaullin; }
      set { SetProperty<decimal>(ref mFeesTarpaullin, value); }
    }

    #endregion

    #region decimal FeesToll

    public const string FeesTollProperty = "FeesToll";
    [DataMember(Name = FeesTollProperty, EmitDefaultValue = false)]
    decimal mFeesToll;
    [PersistantProperty]
    public decimal FeesToll
    {
      get { return mFeesToll; }
      set { SetProperty<decimal>(ref mFeesToll, value); }
    }

    #endregion


    #region BorderPosts BorderPost

    public const string BorderPostProperty = "BorderPost";
    [PersistantProperty]
    public BorderPosts BorderPost
    {
      get { return GetCachedObject<BorderPosts>(); }
      set { SetCachedObject<BorderPosts>(value); }
    }

    #endregion

  }
}

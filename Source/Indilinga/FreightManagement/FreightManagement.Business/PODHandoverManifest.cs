﻿using AccountManagement.Business;
using Afx.Business;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Documents;
using Afx.Business.Validation;
using ContactManagement.Business;
using FleetManagement.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Business
{
  [PersistantObject(Schema = "FreightManagement")]
  public partial class PODHandoverManifest : Document, IVehicleCollection
  {
    public PODHandoverManifest()
      : base(Cache.Instance.GetObject<DocumentType>(DocumentTypeIdentifier))
    {
    }

    #region string Description

    public const string DescriptionProperty = "Description";
    [DataMember(Name = DescriptionProperty, EmitDefaultValue = false)]
    string mDescription;
    [PersistantProperty]
    public string Description
    {
      get { return mDescription; }
      set { SetProperty<string>(ref mDescription, value); }
    }

    #endregion

    #region Contractor Courier

    public const string CourierProperty = "Courier";
    [PersistantProperty(IsCached = true)]
    public Contractor Courier
    {
      get { return GetCachedObject<Contractor>(); }
      set { SetCachedObject<Contractor>(value); }
    }

    #endregion

    #region Contact Driver

    public const string DriverProperty = "Driver";
    [PersistantProperty(IsCached = true)]
    public Contact Driver
    {
      get { return GetCachedObject<Contact>(); }
      set { SetCachedObject<Contact>(value); }
    }

    #endregion




    #region Associative BusinessObjectCollection<PODHandoverReference> PODHandovers

    public const string PODHandoversProperty = "PODHandovers";
    AssociativeObjectCollection<PODHandoverManifestPODHandover, PODHandoverReference> mPODHandovers;
    [PersistantCollection(AssociativeType = typeof(PODHandoverManifestPODHandover))]
    public BusinessObjectCollection<PODHandoverReference> PODHandovers
    {
      get
      {
        if (mPODHandovers == null) using (new EventStateSuppressor(this)) { mPODHandovers = new AssociativeObjectCollection<PODHandoverManifestPODHandover, PODHandoverReference>(this); }
        return mPODHandovers;
      }
    }

    #endregion

    #region void OnCompositionChanged(...)

    protected override void OnCompositionChanged(CompositionChangedType compositionChangedType, string propertyName, object originalSource, BusinessObject item)
    {
      OnPropertyChanged(PODHandoverCountProperty);
      base.OnCompositionChanged(compositionChangedType, propertyName, originalSource, item);
    }

    #endregion

    #region int PODHandoverCount

    public const string PODHandoverCountProperty = "PODHandoverCount";
    [PersistantProperty(IgnoreConcurrency = true)]
    public int PODHandoverCount
    {
      get { return PODHandovers.Count; }
    }

    #endregion

    #region Associative BusinessObjectCollection<Vehicle> Vehicles

    public const string VehiclesProperty = "Vehicles";
    AssociativeObjectCollection<PODHandoverManifestVehicle, Vehicle> mVehicles;
    [PersistantCollection(AssociativeType = typeof(PODHandoverManifestVehicle))]
    public BusinessObjectCollection<Vehicle> Vehicles
    {
      get
      {
        if (mVehicles == null) using (new EventStateSuppressor(this)) { mVehicles = new AssociativeObjectCollection<PODHandoverManifestVehicle, Vehicle>(this); }
        return mVehicles;
      }
    }

    #endregion
  }
}

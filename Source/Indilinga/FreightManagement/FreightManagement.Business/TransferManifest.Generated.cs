﻿using Afx.Business.Data;
using Afx.Business.Documents;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Business
{
  [DataContract(IsReference = true, Namespace = Namespace.FreightManagement)]
  public partial class TransferManifest
  {
    public const string DocumentTypeIdentifier = "{0959344b-86e8-4b09-978f-a5b2a6a29860}";
    public class States
    {
      public const string Cancelled = "{12133856-b47d-4fd8-8c45-6e4a4932a1d6}";
      public const string Closed = "{d2ccef42-d15d-4dc7-bed4-0cb0f3537ad3}";
      public const string Open = "{f7e95a1a-3a09-45ee-a2d7-9c1060f025b9}";
      public const string Verified = "{4238662b-71fd-45b2-9ba5-79913eed8e83}";
    }

    protected TransferManifest(DocumentType documentType)
      : base(documentType)
    {
    }
  }
}

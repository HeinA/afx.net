﻿using Afx.Business;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Business
{
  [Export(typeof(IObjectFlags))]
  public class FeeTypeFlags : IObjectFlags
  {
    public const string NightOutFee = "FreightManagement.NightOutFee";
    public const string TollFee = "FreightManagement.TollFee";
    public const string TarpaullingFee = "FreightManagement.TarpaullingFee";
    public const string CasualFee = "FreightManagement.CasualFee";
    public const string OtherFee = "FreightManagement.OtherFee";

    public Type ObjectType
    {
      get { return typeof(FeeType); }
    }

    public IEnumerable<string> Flags
    {
      get { return new string[] { NightOutFee, TollFee, TarpaullingFee, CasualFee, OtherFee }; }
    }
  }
}

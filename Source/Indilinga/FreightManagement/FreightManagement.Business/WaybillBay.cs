﻿using Afx.Business;
using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Business.Validation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace FreightManagement.Business
{
  [PersistantObject(Schema = "FreightManagement")]
  public partial class WaybillBay : BusinessObject<Waybill> 
  {
    #region Constructors

    public WaybillBay()
    {
    }

    #endregion

    #region DateTime Timestamp

    public const string TimestampProperty = "Timestamp";
    [DataMember(Name = TimestampProperty, EmitDefaultValue = false)]
    DateTime mTimestamp;
    [PersistantProperty]
    public DateTime Timestamp
    {
      get { return mTimestamp; }
      set { SetProperty<DateTime>(ref mTimestamp, value); }
    }

    #endregion

    #region UserReference User

    public const string UserProperty = "User";
    [PersistantProperty(IsCached = true)]
    public UserReference User
    {
      get { return GetCachedObject<UserReference>(); }
      set { SetCachedObject<UserReference>(value); }
    }

    #endregion

    #region string Comment

    public const string CommentProperty = "Comment";
    [DataMember(Name = CommentProperty, EmitDefaultValue = false)]
    string mComment;
    [PersistantProperty]
    public string Comment
    {
      get { return mComment; }
      set { SetProperty<string>(ref mComment, value); }
    }

    #endregion

    #region Bay Bay

    public const string BayProperty = "Bay";
    [PersistantProperty(IsCached = true)]
    public Bay Bay
    {
      get { return GetCachedObject<Bay>(); }
      set { SetCachedObject<Bay>(value); }
    }

    #endregion
  }
}

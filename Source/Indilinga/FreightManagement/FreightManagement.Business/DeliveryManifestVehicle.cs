﻿using Afx.Business;
using Afx.Business.Data;
using FleetManagement.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Business
{
  [PersistantObject(Schema = "FreightManagement")]
  public partial class DeliveryManifestVehicle : AssociativeObject<DeliveryManifest, Vehicle>
  {
    #region Constructors

    public DeliveryManifestVehicle()
    {
    }

    #endregion

    //#region Vehicle Vehicle

    //public const string VehicleProperty = "Vehicle";
    //[PersistantProperty]
    //public Vehicle Vehicle
    //{
    //  get { return GetCachedObject<Vehicle>(); }
    //  set { SetCachedObject<Vehicle>(value); }
    //}

    //#endregion
  }
}

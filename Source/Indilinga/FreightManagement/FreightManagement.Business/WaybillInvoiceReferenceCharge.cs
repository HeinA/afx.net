﻿using Afx.Business;
using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Validation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace FreightManagement.Business
{
  [PersistantObject(Schema = "FreightManagement", TableName = "WaybillCharge", OwnerColumn = "Waybill", IsReadOnly = true)]
  public partial class WaybillInvoiceReferenceCharge : BusinessObject<WaybillInvoiceReference>
  {
    #region Constructors

    public WaybillInvoiceReferenceCharge()
    {
    }

    #endregion

    #region WaybillChargeType ChargeType

    public const string ChargeTypeProperty = "ChargeType";
    [PersistantProperty(IsCached = true)]
    [Mandatory("Charget Type is mandatory.")]
    public WaybillChargeType ChargeType
    {
      get { return GetCachedObject<WaybillChargeType>(); }
      set { SetCachedObject<WaybillChargeType>(value); }
    }

    #endregion

    #region decimal Charge

    public const string ChargeProperty = "Charge";
    [DataMember(Name = ChargeProperty, EmitDefaultValue = false)]
    decimal mCharge;
    [PersistantProperty]
    public decimal Charge
    {
      get { return mCharge; }
      set { SetProperty<decimal>(ref mCharge, decimal.Round(value, 2)); }
    }

    #endregion
  }
}

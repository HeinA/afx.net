﻿using Afx.Business;
using Afx.Business.Data;
using FleetManagement.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Business
{
  [PersistantObject(Schema = "FreightManagement")]
  public partial class TransportManifestVehicle : AssociativeObject<TransportManifest, Vehicle>
  {
    #region Constructors

    public TransportManifestVehicle()
    {
    }

    #endregion
  }
}

﻿using AccountManagement.Business;
using Afx.Business;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Documents;
using Afx.Business.Security;
using Afx.Business.Validation;
using FreightManagement.Business.Validation;
using Geographics.Business;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Business
{
  [PersistantObject(Schema = "FreightManagement")]
  public partial class Waybill : Document
  {
    public Waybill()
      : base(Cache.Instance.GetObject<DocumentType>(DocumentTypeIdentifier))
    {
      using (new EventStateSuppressor(this))
      {
        CollectionDate = DocumentDate.AddDays(1);
        DeliveryDate = CollectionDate.AddDays(7);
      }
    }

    #region AccountReference Account

    public const string AccountProperty = "Account";
    [PersistantProperty(IsCached = true)]
    public AccountReference Account
    {
      get { return GetCachedObject<AccountReference>(); }
      set
      {
        if (SetCachedObject<AccountReference>(value))
        {
          try
          {
            if (!BusinessObject.IsNull(value)) InvoiceTo = value.Name;
          }
          catch (Exception ex)
          {
            if (ExceptionHelper.HandleException(ex)) throw ex;
          }
        }
      }
    }

    #endregion

    //#region string ConsignerCustomsCode

    //public const string ConsignerCustomsCodeProperty = "ConsignerCustomsCode";
    //[DataMember(Name = ConsignerCustomsCodeProperty, EmitDefaultValue = false)]
    //string mConsignerCustomsCode;
    //[PersistantProperty]
    //public string ConsignerCustomsCode
    //{
    //  get { return mConsignerCustomsCode; }
    //  set { SetProperty<string>(ref mConsignerCustomsCode, value); }
    //}

    //#endregion

    //#region string ConsignerVatNumber

    //public const string ConsignerVatNumberProperty = "ConsignerVatNumber";
    //[DataMember(Name = ConsignerVatNumberProperty, EmitDefaultValue = false)]
    //string mConsignerVatNumber;
    //[PersistantProperty]
    //public string ConsignerVatNumber
    //{
    //  get { return mConsignerVatNumber; }
    //  set { SetProperty<string>(ref mConsignerVatNumber, value); }
    //}

    //#endregion

    //#region string ConsigneeCustomsCode

    //public const string ConsigneeCustomsCodeProperty = "ConsigneeCustomsCode";
    //[DataMember(Name = ConsigneeCustomsCodeProperty, EmitDefaultValue = false)]
    //string mConsigneeCustomsCode;
    //[PersistantProperty]
    //public string ConsigneeCustomsCode
    //{
    //  get { return mConsigneeCustomsCode; }
    //  set { SetProperty<string>(ref mConsigneeCustomsCode, value); }
    //}

    //#endregion

    //#region string ConsigneeVatNumber

    //public const string ConsigneeVatNumberProperty = "ConsigneeVatNumber";
    //[DataMember(Name = ConsigneeVatNumberProperty, EmitDefaultValue = false)]
    //string mConsigneeVatNumber;
    //[PersistantProperty]
    //public string ConsigneeVatNumber
    //{
    //  get { return mConsigneeVatNumber; }
    //  set { SetProperty<string>(ref mConsigneeVatNumber, value); }
    //}

    //#endregion

    #region int VolumetricFactor

    public const string VolumetricFactorProperty = "VolumetricFactor";
    [DataMember(Name = VolumetricFactorProperty, EmitDefaultValue = false)]
    int mVolumetricFactor = 0;
    [PersistantProperty(IgnoreConcurrency = true)]
    public int VolumetricFactor
    {
      get { return mVolumetricFactor; }
      set
      {
        if (SetProperty<int>(ref mVolumetricFactor, value))
        {
          foreach (var item in Items)
          {
            item.CalculateVolumetricWeight();
          }
        }
      }
    }

    #endregion

    #region RatesConfigurationReference RatesConfiguration

    public const string RatesConfigurationProperty = "RatesConfiguration";
    [PersistantProperty(IsCached = true)]
    public RatesConfigurationReference RatesConfiguration
    {
      get { return GetCachedObject<RatesConfigurationReference>(); }
      set
      {
        if (SetCachedObject<RatesConfigurationReference>(value))
        {
          SetVolumetricFactor();
        }
      }
    }

    #endregion

    void SetVolumetricFactor()
    {
      if (SuppressEventState) return;

      if (RatesConfiguration != null)
      {
        try
        {
          VolumetricFactor = RatesConfiguration.GetVolumetricFactor(this);
        }
        catch
        {
          VolumetricFactor = 3000;
        }
        //var rcr = RatesConfiguration.GetRevision(DocumentDate);
        //if (rcr == null) throw new InvalidOperationException("The Selected Rates Configuration does not have a revision for this date.");
        //VolumetricFactor = rcr.VolumetricFactor;
      }
      else
      {
        var sr = Cache.Instance.GetObjects<RatesConfigurationReference>(rcr1 => rcr1.IsStandardConfiguration).FirstOrDefault();
        if (sr != null) VolumetricFactor = sr.GetVolumetricFactor(this);
      }
    }

    #region Bay CurrentBay

    public const string CurrentBayProperty = "CurrentBay";
    public Bay CurrentBay
    {
      get { return BusinessObject.IsNull(WaybillBay) ? Setting.GetSetting<DefaultBaySetting>().Bay : WaybillBay.Bay; }
    }

    #endregion

    #region BusinessObjectCollection<WaybillBay> BayMovements

    public const string BayMovementsProperty = "BayMovements";
    BusinessObjectCollection<WaybillBay> mBayMovements;
    [PersistantCollection]
    [DataMember]
    public BusinessObjectCollection<WaybillBay> BayMovements
    {
      get { return mBayMovements ?? (mBayMovements = new BusinessObjectCollection<WaybillBay>(this)); }
    }

    #endregion

    #region WaybillBay WaybillBay

    public const string WaybillBayProperty = "WaybillBay";
    [DataMember(Name = WaybillBayProperty, EmitDefaultValue = false)]
    WaybillBay mWaybillBay;
    [PersistantProperty(PostProcess = true)]
    public WaybillBay WaybillBay
    {
      get { return mWaybillBay; }
      set
      {
        if (SetOwnedReference<WaybillBay>(ref mWaybillBay, value))
        {
          OnPropertyChanged(CurrentBayProperty);
        }
      }
    }

    #endregion

    public void SetBay(Bay bay, string comment)
    {
      var wb = new WaybillBay() { Bay = bay, Comment = comment, Timestamp = DateTime.Now, User = Cache.Instance.GetObject<UserReference>(SecurityContext.User.GlobalIdentifier) };
      BayMovements.Add(wb);
      WaybillBay = wb;
      OnPropertyChanged(Document.StateProperty);
    }

    #region Associative BusinessObjectCollection<WaybillFlag> WaybillFlags

    public const string WaybillFlagsProperty = "WaybillFlags";
    AssociativeObjectCollection<WaybillWaybillFlag, WaybillFlag> mWaybillFlags;
    [PersistantCollection(AssociativeType = typeof(WaybillWaybillFlag))]
    public BusinessObjectCollection<WaybillFlag> WaybillFlags
    {
      get
      {
        if (mWaybillFlags == null)
        if (!SuppressEventState) using (new EventStateSuppressor(this))
        {
          mWaybillFlags = new AssociativeObjectCollection<WaybillWaybillFlag, WaybillFlag>(this);
        }
        return mWaybillFlags;
      }
    }

    #endregion

    #region Collection Details

    #region string Consigner

    public const string ConsignerProperty = "Consigner";
    [DataMember(Name = ConsignerProperty, EmitDefaultValue = false)]
    string mConsigner;
    [PersistantProperty]
    [Mandatory("Consigner is mandatory.")]
    public string Consigner
    {
      get { return mConsigner; }
      set { SetProperty<string>(ref mConsigner, value); }
    }

    #endregion

    #region string CollectionAddress

    public const string CollectionAddressProperty = "CollectionAddress";
    [DataMember(Name = CollectionAddressProperty, EmitDefaultValue = false)]
    string mCollectionAddress;
    [PersistantProperty]
    [AddresValidation("Collection addres could not be resolved")]
    public string CollectionAddress
    {
      get { return mCollectionAddress; }
      set
      {
        if (SetProperty<string>(ref mCollectionAddress, value))
        {
          SetCollectionLocationFromAddress();
        }
      }
    }

    #endregion

    #region bool IncludeLocalCollection

    public const string IncludeLocalCollectionProperty = "IncludeLocalCollection";
    [DataMember(Name = IncludeLocalCollectionProperty, EmitDefaultValue = false)]
    bool mIncludeLocalCollection;
    [PersistantProperty]
    public bool IncludeLocalCollection
    {
      get { return mIncludeLocalCollection; }
      set
      {
        if (SetProperty<bool>(ref mIncludeLocalCollection, value))
        {
          SetCollectionLocationFromAddress();
        }
      }
    }

    #endregion

    #region Location CollectionLocation

    public const string CollectionLocationProperty = "CollectionLocation";
    [PersistantProperty(IsCached = true)]
    [Mandatory("Collection location is mandatory.")]
    public Location CollectionLocation
    {
      get { return GetCachedObject<Location>(); }
      private set { SetCachedObject<Location>(value); }
    }

    #endregion

    #region Location FullCollectionLocation

    public const string FullCollectionLocationProperty = "FullCollectionLocation";
    [PersistantProperty(IsCached = true)]
    public Location FullCollectionLocation
    {
      get { return GetCachedObject<Location>(); }
      private set { SetCachedObject<Location>(value); }
    }

    #endregion

    #region Location CollectionCity

    public const string CollectionCityProperty = "CollectionCity";
    [PersistantProperty(IgnoreConcurrency = true)]
    public Location CollectionCity
    {
      get
      {
        if (BusinessObject.IsNull(FullCollectionLocation)) return null;
        return FullCollectionLocation.City;
      }
    }

    #endregion

    #region Location CollectionSuburb

    public const string CollectionSuburbProperty = "CollectionSuburb";
    [PersistantProperty(IgnoreConcurrency = true)]
    public Location CollectionSuburb
    {
      get
      {
        if (BusinessObject.IsNull(FullCollectionLocation)) return null;
        return FullCollectionLocation.Suburb;
      }
    }

    #endregion

    #region DateTime CollectionDate

    public const string CollectionDateProperty = "CollectionDate";
    [DataMember(Name = CollectionDateProperty, EmitDefaultValue = false)]
    DateTime mCollectionDate;
    [PersistantProperty]
    public DateTime CollectionDate
    {
      get { return mCollectionDate; }
      set { SetProperty<DateTime>(ref mCollectionDate, value); }
    }

    #endregion

    #region void SetCollectionLocationFromAddress(...)

    void SetCollectionLocationFromAddress()
    {
      FullCollectionLocation = Location.GetLocation(CollectionAddress);
      if (FullCollectionLocation != null)
      {
        if (IncludeLocalCollection)
        {
          CollectionLocation = FullCollectionLocation.SecondaryDeliveryPoint() ?? FullCollectionLocation.PrimaryDeliveryPoint();
        }
        else
        {
          CollectionLocation = FullCollectionLocation.PrimaryDeliveryPoint();
        }
      }
      else
      {
        CollectionLocation = null;
      }
    }

    #endregion

    #region string ConsignerImportReference

    public const string ConsignerImportReferenceProperty = "ConsignerImportReference";
    [DataMember(Name = ConsignerImportReferenceProperty, EmitDefaultValue = false)]
    string mConsignerImportReference;
    [PersistantProperty]
    public string ConsignerImportReference
    {
      get { return mConsignerImportReference; }
      set { SetProperty<string>(ref mConsignerImportReference, value); }
    }

    #endregion

    #region string ConsignerExportReference

    public const string ConsignerExportReferenceProperty = "ConsignerExportReference";
    [DataMember(Name = ConsignerExportReferenceProperty, EmitDefaultValue = false)]
    string mConsignerExportReference;
    [PersistantProperty]
    public string ConsignerExportReference
    {
      get { return mConsignerExportReference; }
      set { SetProperty<string>(ref mConsignerExportReference, value); }
    }

    #endregion

    #endregion

    #region Delivery Details

    #region string Consignee

    public const string ConsigneeProperty = "Consignee";
    [DataMember(Name = ConsigneeProperty, EmitDefaultValue = false)]
    string mConsignee;
    [PersistantProperty]
    [Mandatory("Consignee is mandatory.")]
    public string Consignee
    {
      get { return mConsignee; }
      set { SetProperty<string>(ref mConsignee, value); }
    }

    #endregion

    #region string DeliveryAddress

    public const string DeliveryAddressProperty = "DeliveryAddress";
    [DataMember(Name = DeliveryAddressProperty, EmitDefaultValue = false)]
    string mDeliveryAddress;
    [PersistantProperty]
    [AddresValidation("Delivery addres could not be resolved")]
    public string DeliveryAddress
    {
      get { return mDeliveryAddress; }
      set
      {
        if (SetProperty<string>(ref mDeliveryAddress, value))
        {
          SetDeliveryLocationFromAddress();
        }
      }
    }

    #endregion

    #region bool IncludeLocalDelivery

    public const string IncludeLocalDeliveryProperty = "IncludeLocalDelivery";
    [DataMember(Name = IncludeLocalDeliveryProperty, EmitDefaultValue = false)]
    bool mIncludeLocalDelivery;
    [PersistantProperty]
    public bool IncludeLocalDelivery
    {
      get { return mIncludeLocalDelivery; }
      set
      {
        if (SetProperty<bool>(ref mIncludeLocalDelivery, value))
        {
          SetDeliveryLocationFromAddress();
        }
      }
    }

    #endregion

    #region Location DeliveryLocation

    public const string DeliveryLocationProperty = "DeliveryLocation";
    [PersistantProperty(IsCached = true)]
    [Mandatory("Delivery location is mandatory.")]
    public Location DeliveryLocation
    {
      get { return GetCachedObject<Location>(); }
      private set { SetCachedObject<Location>(value); }
    }

    #endregion

    #region Location FullDeliveryLocation

    public const string FullDeliveryLocationProperty = "FullDeliveryLocation";
    [PersistantProperty(IsCached = true)]
    public Location FullDeliveryLocation
    {
      get { return GetCachedObject<Location>(); }
      private set { SetCachedObject<Location>(value); }
    }

    #endregion

    #region Location DeliveryCity

    public const string DeliveryCityProperty = "DeliveryCity";
    [PersistantProperty(IgnoreConcurrency = true)]
    public Location DeliveryCity
    {
      get
      {
        if (BusinessObject.IsNull(FullDeliveryLocation)) return null;
        return FullDeliveryLocation.City;
      }
    }

    #endregion

    #region Location DeliverySuburb

    public const string DeliverySuburbProperty = "DeliverySuburb";
    [PersistantProperty(IgnoreConcurrency = true)]
    public Location DeliverySuburb
    {
      get
      {
        if (BusinessObject.IsNull(FullDeliveryLocation)) return null;
        return FullDeliveryLocation.Suburb;
      }
    }

    #endregion

    #region DateTime DeliveryDate

    public const string DeliveryDateProperty = "DeliveryDate";
    [DataMember(Name = DeliveryDateProperty, EmitDefaultValue = false)]
    DateTime mDeliveryDate;
    [PersistantProperty]
    public DateTime DeliveryDate
    {
      get { return mDeliveryDate; }
      set { SetProperty<DateTime>(ref mDeliveryDate, value); }
    }

    #endregion

    #region void SetDeliveryLocationFromAddress(...)

    void SetDeliveryLocationFromAddress()
    {
      FullDeliveryLocation = Location.GetLocation(DeliveryAddress);
      if (FullDeliveryLocation != null)
      {
        if (IncludeLocalDelivery)
        {
          DeliveryLocation = FullDeliveryLocation.SecondaryDeliveryPoint() ?? FullDeliveryLocation.PrimaryDeliveryPoint();
        }
        else
        {
          DeliveryLocation = FullDeliveryLocation.PrimaryDeliveryPoint();
        }
      }
      else
      {
        DeliveryLocation = null;
      }
    }

    #endregion

    #region string ConsigneeImportReference

    public const string ConsigneeImportReferenceProperty = "ConsigneeImportReference";
    [DataMember(Name = ConsigneeImportReferenceProperty, EmitDefaultValue = false)]
    string mConsigneeImportReference;
    [PersistantProperty]
    public string ConsigneeImportReference
    {
      get { return mConsigneeImportReference; }
      set { SetProperty<string>(ref mConsigneeImportReference, value); }
    }

    #endregion

    #region string ConsigneeExportReference

    public const string ConsigneeExportReferenceProperty = "ConsigneeExportReference";
    [DataMember(Name = ConsigneeExportReferenceProperty, EmitDefaultValue = false)]
    string mConsigneeExportReference;
    [PersistantProperty]
    public string ConsigneeExportReference
    {
      get { return mConsigneeExportReference; }
      set { SetProperty<string>(ref mConsigneeExportReference, value); }
    }

    #endregion

    #endregion


    #region BusinessObjectCollection<WaybillCargoItem> Items

    public const string ItemsProperty = "Items";
    BusinessObjectCollection<WaybillCargoItem> mItems;
    [PersistantCollection]
    [DataMember]
    [Mandatory("At least one item is mandatory.")]
    public BusinessObjectCollection<WaybillCargoItem> Items
    {
      get { return mItems ?? (mItems = new BusinessObjectCollection<WaybillCargoItem>(this)); }
    }

    #endregion

    #region BusinessObjectCollection<WaybillCharge> Charges

    public const string ChargesProperty = "Charges";
    BusinessObjectCollection<WaybillCharge> mCharges;
    [PersistantCollection]
    [DataMember]
    public BusinessObjectCollection<WaybillCharge> Charges
    {
      get { return mCharges ?? (mCharges = new BusinessObjectCollection<WaybillCharge>(this)); }
    }

    #endregion

    #region Route CollectionRoute

    public const string CollectionRouteProperty = "CollectionRoute";
    [PersistantProperty(IsCached = true)]
    public Route CollectionRoute
    {
      get { return GetCachedObject<Route>(); }
      set { SetCachedObject<Route>(value); }
    }

    #endregion

    #region BusinessObjectCollection<WaybillRoute> Routes

    public const string RoutesProperty = "Routes";
    BusinessObjectCollection<WaybillRoute> mRoutes;
    [PersistantCollection]
    [DataMember]
    public BusinessObjectCollection<WaybillRoute> Routes
    {
      get { return mRoutes ?? (mRoutes = new BusinessObjectCollection<WaybillRoute>(this)); }
    }

    #endregion

    #region BusinessObjectCollection<WaybillTrace> TraceLog

    public const string TraceLogProperty = "TraceLog";
    BusinessObjectCollection<WaybillTrace> mTraceLog;
    [PersistantCollection]
    [DataMember]
    public BusinessObjectCollection<WaybillTrace> TraceLog
    {
      get { return mTraceLog ?? (mTraceLog = new BusinessObjectCollection<WaybillTrace>(this)); }
    }

    #endregion

    #region string ChargeDescription

    public const string ChargeDescriptionProperty = "ChargeDescription";
    [DataMember(Name = ChargeDescriptionProperty, EmitDefaultValue = false)]
    string mChargeDescription;
    [PersistantProperty]
    public string ChargeDescription
    {
      get { return mChargeDescription; }
      set { SetProperty<string>(ref mChargeDescription, value); }
    }

    #endregion

    #region int TotalItems

    public const string TotalItemsProperty = "TotalItems";
    [DataMember(Name = TotalItemsProperty, EmitDefaultValue = false)]
    int mTotalItems;
    [PersistantProperty]
    public int TotalItems
    {
      get { return mTotalItems; }
      private set { SetProperty<int>(ref mTotalItems, value); }
    }

    #endregion

    #region decimal Charge

    public const string ChargeProperty = "Charge";
    [DataMember(Name = ChargeProperty, EmitDefaultValue = false)]
    decimal mCharge;
    [PersistantProperty]
    //[Mandatory("Charges is mandatory.")]
    public decimal Charge
    {
      get { return mCharge; }
      private set { SetProperty<decimal>(ref mCharge, Math.Round(value, 2)); }
    }

    #endregion

    #region decimal PhysicalWeight

    public const string PhysicalWeightProperty = "PhysicalWeight";
    [DataMember(Name = PhysicalWeightProperty, EmitDefaultValue = false)]
    decimal mPhysicalWeight;
    [PersistantProperty]
    public decimal PhysicalWeight
    {
      get { return mPhysicalWeight; }
      private set { SetProperty<decimal>(ref mPhysicalWeight, value); }
    }

    #endregion

    #region decimal BillingWeight

    public const string BillingWeightProperty = "BillingWeight";
    public decimal BillingWeight
    {
      get
      {
        decimal w = 0;
        foreach (var i in Items)
        {
          w += i.PhysicalWeight > i.VolumetricWeight ? i.PhysicalWeight : i.VolumetricWeight;
        }
        return w;
      }
    }

    #endregion

    #region decimal VolumemetricWeight

    public const string VolumemetricWeightProperty = "VolumemetricWeight";
    decimal mVolumemetricWeight;
    public decimal VolumemetricWeight
    {
      get { return mVolumemetricWeight; }
      set { SetProperty<decimal>(ref mVolumemetricWeight, value); }
    }

    #endregion

    #region decimal DeclaredValue

    public const string DeclaredValueProperty = "DeclaredValue";
    [DataMember(Name = DeclaredValueProperty, EmitDefaultValue = false)]
    decimal mDeclaredValue;
    [PersistantProperty]
    public decimal DeclaredValue
    {
      get { return mDeclaredValue; }
      private set { SetProperty<decimal>(ref mDeclaredValue, value); }
    }

    #endregion

    #region DateTime? PODDate

    public const string PODDateProperty = "PODDate";
    [DataMember(Name = PODDateProperty, EmitDefaultValue = false)]
    DateTime? mPODDate;
    [PersistantProperty]
    public DateTime? PODDate
    {
      get { return mPODDate; }
      set { SetProperty<DateTime?>(ref mPODDate, value); }
    }

    #endregion

    #region string PODSignature

    public const string PODSignatureProperty = "PODSignature";
    [DataMember(Name = PODSignatureProperty, EmitDefaultValue = false)]
    string mPODSignature;
    [PersistantProperty]
    public string PODSignature
    {
      get { return mPODSignature; }
      set { SetProperty<string>(ref mPODSignature, value); }
    }

    #endregion

    #region string InvoiceNumber

    public const string InvoiceNumberProperty = "InvoiceNumber";
    [DataMember(Name = InvoiceNumberProperty, EmitDefaultValue = false)]
    string mInvoiceNumber;
    [PersistantProperty]
    public string InvoiceNumber
    {
      get { return mInvoiceNumber; }
      set { SetProperty<string>(ref mInvoiceNumber, value); }
    }

    #endregion

    #region DateTime? InvoiceDate

    public const string InvoiceDateProperty = "InvoiceDate";
    [DataMember(Name = InvoiceDateProperty, EmitDefaultValue = false)]
    DateTime? mInvoiceDate;
    [PersistantProperty]
    public DateTime? InvoiceDate
    {
      get { return mInvoiceDate; }
      set { SetProperty<DateTime?>(ref mInvoiceDate, value); }
    }

    #endregion

    #region string AccountingOrderNumber

    public const string AccountingOrderNumberProperty = "AccountingOrderNumber";
    [DataMember(Name = AccountingOrderNumberProperty, EmitDefaultValue = false)]
    string mAccountingOrderNumber;
    [PersistantProperty]
    public string AccountingOrderNumber
    {
      get { return mAccountingOrderNumber; }
      set { SetProperty<string>(ref mAccountingOrderNumber, value); }
    }

    #endregion

    #region string InvoiceTo

    public const string InvoiceToProperty = "InvoiceTo";
    [DataMember(Name = InvoiceToProperty, EmitDefaultValue = false)]
    string mInvoiceTo;
    [PersistantProperty]
    [Mandatory("Invoice To is mandatory")]
    public string InvoiceTo
    {
      get { return mInvoiceTo; }
      set { SetProperty<string>(ref mInvoiceTo, value); }
    }

    #endregion

    #region bool IsInsured

    public const string IsInsuredProperty = "IsInsured";
    [DataMember(Name = IsInsuredProperty, EmitDefaultValue = false)]
    bool mIsInsured;
    [PersistantProperty]
    public bool IsInsured
    {
      get { return mIsInsured; }
      set { SetProperty<bool>(ref mIsInsured, value); }
    }

    #endregion

    #region bool ApplyImportVAT

    public const string ApplyImportVATProperty = "ApplyImportVAT";
    [DataMember(Name = ApplyImportVATProperty, EmitDefaultValue = false)]
    bool mApplyImportVAT;
    [PersistantProperty]
    public bool ApplyImportVAT
    {
      get { return mApplyImportVAT; }
      set { SetProperty<bool>(ref mApplyImportVAT, value); }
    }

    #endregion

    #region bool IsPointToPoint

    public const string IsPointToPointProperty = "IsPointToPoint";
    [DataMember(Name = IsPointToPointProperty, EmitDefaultValue = false)]
    bool mIsPointToPoint;
    [PersistantProperty]
    public bool IsPointToPoint
    {
      get { return mIsPointToPoint; }
      set { SetProperty<bool>(ref mIsPointToPoint, value); }
    }

    #endregion

    #region string ClientReferenceNumber

    public const string ClientReferenceNumberProperty = "ClientReferenceNumber";
    [DataMember(Name = ClientReferenceNumberProperty, EmitDefaultValue = false)]
    string mClientReferenceNumber;
    [PersistantProperty]
    public string ClientReferenceNumber
    {
      get { return mClientReferenceNumber; }
      set { SetProperty<string>(ref mClientReferenceNumber, value); }
    }

    #endregion

    #region BusinessObjectCollection<WaybillParcel> Parcels

    public const string ParcelsProperty = "Parcels";
    BusinessObjectCollection<WaybillParcel> mParcels;
    [PersistantCollection]
    [DataMember]
    public BusinessObjectCollection<WaybillParcel> Parcels
    {
      get { return mParcels ?? (mParcels = new BusinessObjectCollection<WaybillParcel>(this)); }
    }

    #endregion

    protected override void OnCompositionChanged(Afx.Business.CompositionChangedType compositionChangedType, string propertyName, object originalSource, Afx.Business.BusinessObject item)
    {
      base.OnCompositionChanged(compositionChangedType, propertyName, originalSource, item);

      SetVolumetricFactor();

      CalculateTotalCharge();
      CalculateWeights();
      CalculateDeclaredValue();
      CalculateTotalItems();
    }

    private void CalculateDeclaredValue()
    {
      decimal total = 0;
      foreach (var c in Items)
      {
        if (!c.IsDeleted) total += c.DeclaredValue;
      }
      DeclaredValue = total;
    }

    private void CalculateWeights()
    {
      decimal totalP = 0;
      decimal totalV = 0;
      foreach (var c in Items)
      {
        if (!c.IsDeleted) totalP += c.PhysicalWeight;
        if (!c.IsDeleted) totalV += c.VolumetricWeight;
      }
      PhysicalWeight = totalP;
      VolumemetricWeight = totalV;
    }

    void CalculateTotalCharge()
    {
      decimal total = 0;
      foreach (var c in Charges)
      {
        if (!c.IsDeleted) total += c.Charge;
      }
      Charge = total;
    }

    void CalculateTotalItems()
    {
      int total = 0;
      foreach (var c in Items)
      {
        if (!c.IsDeleted) total += c.Items;
      }
      TotalItems = total;
    }

    public const string CurrentDistributionCenterProperty = "CurrentDistributionCenter";
    [PersistantProperty(IgnoreConcurrency = true)]
    public DistributionCenter CurrentDistributionCenter
    {
      get
      {
        WaybillRoute route = Routes.Where(r => !r.IsDeleted && r.ArrivalTimestamp != null && r.DepartureTimestamp == null).FirstOrDefault();
        if (route == null) return null;
        return route.DistributionCenter;
      }
    }

    public DistributionCenter NextDistributionCenter
    {
      get
      {
        WaybillRoute route = Routes.Where(r => !r.IsDeleted && r.ArrivalTimestamp == null && r.DepartureTimestamp == null).FirstOrDefault();
        if (route == null) return null;
        return route.DistributionCenter;
      }
    }

    public DistributionCenter LastDistributionCenter
    {
      get
      {
        WaybillRoute route = Routes.Where(r => !r.IsDeleted && r.ArrivalTimestamp != null && r.DepartureTimestamp != null).LastOrDefault();
        if (route == null) return null;
        return route.DistributionCenter;
      }
    }

    public bool HasBeenRecievedByDistributionCenter(DistributionCenter dc)
    {
      foreach (var r in this.Routes)
      {
        if (r.DistributionCenter.Equals(dc) && r.ArrivalTimestamp != null) return true;
      }
      return false;
    }

    protected override void GetErrors(System.Collections.ObjectModel.Collection<string> errors, string propertyName)
    {
      if (string.IsNullOrWhiteSpace(propertyName) || propertyName == RoutesProperty)
      {
        if (!IsPointToPoint && Routes.Where(r => !r.IsDeleted).Count() == 0) errors.Add("The waybill has not been routed.");
      }

      if (string.IsNullOrWhiteSpace(propertyName) || propertyName == WaybillFlagsProperty)
      {
        foreach (var fg in Cache.Instance.GetObjects<WaybillFlagGroup>().Where(fg1 => fg1.IsMutuallyExclusive))
        {
          if (WaybillFlags.Count(f => f.Owner.Equals(fg)) > 1)
          {
            errors.Add(string.Format("Only one flag from the group '{0}' may be selected at a time.", fg.Text));
          }
        }

        foreach (var fg in Cache.Instance.GetObjects<WaybillFlagGroup>().Where(fg1 => fg1.IsMandatory))
        {
          if (WaybillFlags.Count(f => f.Owner.Equals(fg)) == 0)
          {
            errors.Add(string.Format("One flag from the group '{0}' must be selected.", fg.Text));
          }
        }
      }

      base.GetErrors(errors, propertyName);
    }
  }
}

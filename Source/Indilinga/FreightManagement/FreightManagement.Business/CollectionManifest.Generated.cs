﻿using Afx.Business.Data;
using Afx.Business.Documents;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Business
{
  [DataContract(IsReference = true, Namespace = Namespace.FreightManagement)]
  public partial class CollectionManifest
  {
    public const string DocumentTypeIdentifier = "{29f11cc1-46eb-4331-a620-88f7c38f7f7b}";
    public class States
    {
      public const string Closed = "{a168ca1b-9c81-435c-8bf0-b98f20dffcae}";
      public const string Open = "{5ba1ac1d-7b04-43bb-8531-25997ecefe98}";
      public const string Verified = "{e59feb72-e9de-4459-ac51-6b560282bf2d}";
    }

    protected CollectionManifest(DocumentType documentType)
      : base(documentType)
    {
    }
  }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Business
{
  public enum BillingUnit
  {
    Invalid = 0
    , Weight = 1
    , Quantity = 2
    , Value = 3
  }
}

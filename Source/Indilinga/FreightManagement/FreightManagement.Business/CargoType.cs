﻿using Afx.Business;
using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Validation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace FreightManagement.Business
{
  [PersistantObject(Schema = "FreightManagement")]
  [Cache(Priority = 0)]
  public partial class CargoType : BusinessObject, IRevisable
  {
    #region Constructors

    public CargoType()
    {
    }

    public CargoType(bool isNull)
      : base(isNull)
    {
    }

    #endregion

    #region int Revision

    public const string RevisionProperty = "Revision";
    [DataMember(Name = RevisionProperty, EmitDefaultValue = false)]
    int mRevision;
    [PersistantProperty]
    public int Revision
    {
      get { return mRevision; }
      set { SetProperty<int>(ref mRevision, value); }
    }

    #endregion

    #region string Text

    public const string TextProperty = "Text";
    [DataMember(Name = TextProperty, EmitDefaultValue = false)]
    string mText;
    [Mandatory("Text is mandatory.")]
    [PersistantProperty]
    public string Text
    {
      get { return mText; }
      set { SetProperty<string>(ref mText, value); }
    }

    #endregion

    #region BillingUnit BillingUnit

    public const string BillingUnitProperty = "BillingUnit";
    [DataMember(Name = BillingUnitProperty, EmitDefaultValue = false)]
    BillingUnit mBillingUnit;
    [Mandatory("Billing Unit is mandatory.")]
    [PersistantProperty]
    public BillingUnit BillingUnit
    {
      get { return mBillingUnit; }
      set { SetProperty<BillingUnit>(ref mBillingUnit, value); }
    }

    #endregion

    #region bool IsSingleUnit

    public const string IsSingleUnitProperty = "IsSingleUnit";
    [DataMember(Name = IsSingleUnitProperty, EmitDefaultValue = false)]
    bool mIsSingleUnit;
    [PersistantProperty]
    public bool IsSingleUnit
    {
      get { return mIsSingleUnit; }
      set { SetProperty<bool>(ref mIsSingleUnit, value); }
    }

    #endregion

    #region string UnitOfMeasure

    public const string UnitOfMeasureProperty = "UnitOfMeasure";
    [DataMember(Name = UnitOfMeasureProperty, EmitDefaultValue = false)]
    string mUnitOfMeasure;
    [PersistantProperty]
    [Mandatory("Unit of Measure is mandatory.")]
    public string UnitOfMeasure
    {
      get { return mUnitOfMeasure; }
      set { SetProperty<string>(ref mUnitOfMeasure, value); }
    }

    #endregion

    #region BusinessObjectCollection<CargoTypeCategory> Categories

    public const string CategoriesProperty = "Categories";
    BusinessObjectCollection<CargoTypeCategory> mCategories;
    [PersistantCollection]
    [DataMember]
    public BusinessObjectCollection<CargoTypeCategory> Categories
    {
      get { return mCategories ?? (mCategories = new BusinessObjectCollection<CargoTypeCategory>(this)); }
    }

    #endregion

    #region bool IsPointToPointType

    public const string IsPointToPointTypeProperty = "IsPointToPointType";
    [DataMember(Name = IsPointToPointTypeProperty, EmitDefaultValue = false)]
    bool mIsPointToPointType;
    [PersistantProperty]
    public bool IsPointToPointType
    {
      get { return mIsPointToPointType; }
      set { SetProperty<bool>(ref mIsPointToPointType, value); }
    }

    #endregion

    #region bool IsDistributionType

    public const string IsDistributionTypeProperty = "IsDistributionType";
    [DataMember(Name = IsDistributionTypeProperty, EmitDefaultValue = false)]
    bool mIsDistributionType;
    [PersistantProperty]
    public bool IsDistributionType
    {
      get { return mIsDistributionType; }
      set { SetProperty<bool>(ref mIsDistributionType, value); }
    }

    #endregion
  }
}

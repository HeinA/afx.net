﻿using Afx.Business;
using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Validation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace FreightManagement.Business
{
  [PersistantObject(Schema = "FreightManagement")]
  [Cache]
  public partial class TaxType : BusinessObject, IRevisable 
  {
    #region Constructors

    public TaxType()
    {
    }

    public TaxType(bool isNull)
      : base(isNull)
    {
    }

    #endregion

    #region int Revision

    public const string RevisionProperty = "Revision";
    [DataMember(Name = RevisionProperty, EmitDefaultValue = false)]
    int mRevision;
    [PersistantProperty]
    public int Revision
    {
      get { return mRevision; }
      set { SetProperty<int>(ref mRevision, value); }
    }

    #endregion
    
    #region string Text

    public const string TextProperty = "Text";
    [DataMember(Name = TextProperty, EmitDefaultValue = false)]
    string mText;
    [PersistantProperty]
    [Mandatory("Text is mandatory.")]
    public string Text
    {
      get { return mText; }
      set { SetProperty<string>(ref mText, value); }
    }

    #endregion

    #region decimal TaxRate

    public const string TaxRateProperty = "TaxRate";
    [DataMember(Name = TaxRateProperty, EmitDefaultValue = false)]
    decimal mTaxRate;
    [PersistantProperty]
    public decimal TaxRate
    {
      get { return mTaxRate; }
      set { SetProperty<decimal>(ref mTaxRate, value); }
    }

    #endregion
  }
}

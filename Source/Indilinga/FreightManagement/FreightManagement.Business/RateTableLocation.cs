﻿using Afx.Business;
using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Tabular;
using Afx.Business.Validation;
using Geographics.Business;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace FreightManagement.Business
{
  [PersistantObject(Schema = "FreightManagement")]
  public partial class RateTableLocation : TableRow<RateTable, RateTableRate> 
  {
    #region Constructors

    public RateTableLocation()
    {
    }

    public RateTableLocation(Location location)
    {
      Location = location;
    }

    #endregion

    #region Location Location

    public const string LocationProperty = "Location";
    [PersistantProperty]
    public Location Location
    {
      get { return GetCachedObject<Location>(); }
      set { SetCachedObject<Location>(value); }
    }

    #endregion

    public override object Index
    {
      get { return Location.FullName; }
    }

    public bool GetCost(ref decimal units, ref decimal cost, ref decimal baseCharge, ref decimal rate, ref bool isFixed, ref bool isPercentage, ref WaybillChargeType chargeType)
    {
      decimal u = units;
      RateTableCategory rtc = Owner.Columns.OfType<RateTableCategory>().Where(c => c.StartUnit <= u && !c.IsMinimumCharge).OrderByDescending(c => c.StartUnit).FirstOrDefault();
      if (rtc == null) rtc = Owner.Columns.OfType<RateTableCategory>().Where(c => c.StartUnit <= u).OrderByDescending(c => c.StartUnit).FirstOrDefault();
      if (rtc == null) return false;

      RateTableRate rtr = Cells.FirstOrDefault(c => c.Column.Equals(rtc));
      if (rtr == null) return false;

      decimal costPrevious = 0;
      if (rtc.ThereAfter && !(rtc.IsFixedRate || rtc.IsPercentage))
      {
        decimal previousUnits = (int)rtc.StartUnit - 1;
        if (previousUnits < 0) return false;

        units = units - previousUnits;

        decimal ratePrevious = 0;
        decimal baseChargePrevious = 0;
        bool isFixedPrevious = false;
        bool isPercentagePrevious = false;
        WaybillChargeType chargeTypePrevious = null;
        if (!this.GetCost(ref previousUnits, ref costPrevious, ref baseChargePrevious, ref ratePrevious, ref isFixedPrevious, ref isPercentagePrevious, ref chargeTypePrevious))
        {
          return false;
        }
        baseCharge = costPrevious;
      }

      isFixed = rtc.IsFixedRate || rtc.IsMinimumCharge;
      isPercentage = rtc.IsPercentage;
      chargeType = rtc.CostComponentType;
      rate = rtr.Rate;
      if (rtc.IsPercentage)
      {
        cost = (decimal)units * (rtr.Rate / 100);
      }
      else
      {
        if (rtc.IsFixedRate || rtc.IsMinimumCharge) cost = rtr.Rate;
        else cost = rtr.Rate * units;
      }

      cost += costPrevious;

      RateTableCategory rtcMin = Owner.Columns.OfType<RateTableCategory>().Where(c => c.IsMinimumCharge).FirstOrDefault();
      if (rtcMin != null)
      {
        RateTableRate rtrMin = Cells.FirstOrDefault(c => c.Column.Equals(rtcMin));
        if (cost < rtrMin.Rate && cost != 0) cost = rtrMin.Rate;
      }

      return true;
    }
  }
}

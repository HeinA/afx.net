﻿using AccountManagement.Business;
using Afx.Business;
using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Validation;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace FreightManagement.Business
{
  [PersistantObject(Schema = "FreightManagement")]
  public partial class RatesConfiguration1 : BusinessObject, IRevisable
  {
    #region Constructors

    public RatesConfiguration1()
    {
    }

    #endregion

    #region int Revision

    public const string RevisionProperty = "Revision";
    [DataMember(Name = RevisionProperty, EmitDefaultValue = false)]
    int mRevision;
    [PersistantProperty]
    public int Revision
    {
      get { return mRevision; }
      set { SetProperty<int>(ref mRevision, value); }
    }

    #endregion

    #region AccountReference Account

    public const string AccountProperty = "Account";
    [PersistantProperty]
    public AccountReference Account
    {
      get { return GetCachedObject<AccountReference>(); }
      set { SetCachedObject<AccountReference>(value); }
    }

    #endregion

    #region BusinessObjectCollection<RatesConfigurationRevision> Revisions

    public const string RevisionsProperty = "Revisions";
    BusinessObjectCollection<RatesConfigurationRevision> mRevisions;
    [PersistantCollection]
    [DataMember]
    [Mandatory("Rates Configuration requires at least one revision.")]
    public BusinessObjectCollection<RatesConfigurationRevision> Revisions
    {
      get { return mRevisions ?? (mRevisions = new BusinessObjectCollection<RatesConfigurationRevision>(this)); }
    }

    #endregion

    public RatesConfigurationRevision GetRevision(DateTime date)
    {
      return Revisions.Where(r => r.EffectiveDate.Date <= date).OrderByDescending(r => r.EffectiveDate).FirstOrDefault();
    }
  }
}

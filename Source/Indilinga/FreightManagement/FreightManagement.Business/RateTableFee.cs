﻿using Afx.Business.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Business
{
  [PersistantObject(Schema = "FreightManagement")]
  [DataContract(IsReference = true, Namespace = FreightManagement.Business.Namespace.FreightManagement)]
  public class RateTableFee : RateTableColumn
  {
    public RateTableFee()
    {
    }

    public RateTableFee(WaybillCostComponentType chargeType)
    {
      ChargeType = chargeType;
    }

    #region WaybillCostComponentType ChargeType

    public const string ChargeTypeProperty = "ChargeType";
    [PersistantProperty(IsCached = true)]
    public WaybillCostComponentType ChargeType
    {
      get { return GetCachedObject<WaybillCostComponentType>(); }
      set { SetCachedObject<WaybillCostComponentType>(value); }
    }

    #endregion

    public override string Text
    {
      get { return ChargeType.Text; }
    }

    public override object Index
    {
      get { return ChargeType.Text; }
    }
  }
}

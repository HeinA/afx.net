﻿using Afx.Business;
using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Validation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.Runtime.Serialization;

namespace FreightManagement.Business
{
  [PersistantObject(Schema = "FreightManagement")]
  [Export(typeof(Setting))]
  public partial class CostingSetting : Setting 
  {
    #region Constructors

    public CostingSetting()
    {
    }

    #endregion

    #region string Name

    public override string Name
    {
      get { return "Costing"; }
    }

    #endregion

    #region string SettingGroupIdentifier

    protected override string SettingGroupIdentifier
    {
      get { return "46f1a0e8-072e-4eda-9217-56dbc928e9d5"; }
    }

    #endregion

    #region bool PreferCheapestRate

    public const string PreferCheapestRateProperty = "PreferCheapestRate";
    [DataMember(Name = PreferCheapestRateProperty, EmitDefaultValue = false)]
    bool mPreferCheapestRate = true;
    [PersistantProperty]
    public bool PreferCheapestRate
    {
      get { return mPreferCheapestRate; }
      set { SetProperty<bool>(ref mPreferCheapestRate, value); }
    }

    #endregion

    #region bool UseDistanceTables

    public const string UseDistanceTablesProperty = "UseDistanceTables";
    [DataMember(Name = UseDistanceTablesProperty, EmitDefaultValue = false)]
    bool mUseDistanceTables;
    [PersistantProperty]
    public bool UseDistanceTables
    {
      get { return mUseDistanceTables; }
      set { SetProperty<bool>(ref mUseDistanceTables, value); }
    }

    #endregion
  }
}

﻿using AccountManagement.Business;
using Afx.Business;
using Afx.Business.Activities;
using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Validation;
using FleetManagement.Business;
using Geographics.Business;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace FreightManagement.Business
{
  [PersistantObject(Schema = "FreightManagement", TableName = "vWaybillFloorStatus", IsReadOnly = true)]
  public partial class WaybillFloorStatus : BusinessObject 
  {
    #region Constructors

    public WaybillFloorStatus()
    {
    }

    #endregion

    #region bool IsChecked

    public const string IsCheckedProperty = "IsChecked";
    bool mIsChecked;
    public bool IsChecked
    {
      get { return mIsChecked; }
      set { SetProperty<bool>(ref mIsChecked, value); }
    }

    #endregion

    #region DateTime WaybillDate

    public const string WaybillDateProperty = "WaybillDate";
    [DataMember(Name = WaybillDateProperty, EmitDefaultValue = false)]
    DateTime mWaybillDate;
    [PersistantProperty]
    public DateTime WaybillDate
    {
      get { return mWaybillDate; }
      set { SetProperty<DateTime>(ref mWaybillDate, value); }
    }

    #endregion

    #region string WaybillNumber

    public const string WaybillNumberProperty = "WaybillNumber";
    [DataMember(Name = WaybillNumberProperty, EmitDefaultValue = false)]
    string mWaybillNumber;
    [PersistantProperty]
    public string WaybillNumber
    {
      get { return mWaybillNumber; }
      set { SetProperty<string>(ref mWaybillNumber, value); }
    }

    #endregion

    #region Location DeliveryCity

    public const string DeliveryCityProperty = "DeliveryCity";
    [PersistantProperty(IsCached = true)]
    public Location DeliveryCity
    {
      get { return GetCachedObject<Location>(); }
      set { SetCachedObject<Location>(value); }
    }

    #endregion

    #region string DeliveryAddress

    public const string DeliveryAddressProperty = "DeliveryAddress";
    [DataMember(Name = DeliveryAddressProperty, EmitDefaultValue = false)]
    string mDeliveryAddress;
    [PersistantProperty]
    public string DeliveryAddress
    {
      get { return mDeliveryAddress; }
      set { SetProperty<string>(ref mDeliveryAddress, value); }
    }

    #endregion

    #region string DeliveryAddressSingleLine

    public string DeliveryAddressSingleLine
    {
      get { return DeliveryAddress.Replace("\r\n", ", ").Replace("\n", ", "); }
    }

    #endregion

    #region DateTime DeliveryDate

    public const string DeliveryDateProperty = "DeliveryDate";
    [DataMember(Name = DeliveryDateProperty, EmitDefaultValue = false)]
    DateTime mDeliveryDate;
    [PersistantProperty]
    public DateTime DeliveryDate
    {
      get { return mDeliveryDate; }
      set { SetProperty<DateTime>(ref mDeliveryDate, value); }
    }

    #endregion

    #region AccountReference Account

    public const string AccountProperty = "Account";
    [PersistantProperty(IsCached = true)]
    public AccountReference Account
    {
      get { return GetCachedObject<AccountReference>(); }
      set { SetCachedObject<AccountReference>(value); }
    }

    #endregion

    #region string InvoiceTo

    public const string InvoiceToProperty = "InvoiceTo";
    [DataMember(Name = InvoiceToProperty, EmitDefaultValue = false)]
    string mInvoiceTo;
    [PersistantProperty]
    public string InvoiceTo
    {
      get { return mInvoiceTo; }
      set { SetProperty<string>(ref mInvoiceTo, value); }
    }

    #endregion

    #region string Consigner

    public const string ConsignerProperty = "Consigner";
    [DataMember(Name = ConsignerProperty, EmitDefaultValue = false)]
    string mConsigner;
    [PersistantProperty]
    public string Consigner
    {
      get { return mConsigner; }
      set { SetProperty<string>(ref mConsigner, value); }
    }

    #endregion

    #region string Consignee

    public const string ConsigneeProperty = "Consignee";
    [DataMember(Name = ConsigneeProperty, EmitDefaultValue = false)]
    string mConsignee;
    [PersistantProperty]
    public string Consignee
    {
      get { return mConsignee; }
      set { SetProperty<string>(ref mConsignee, value); }
    }

    #endregion

    #region int Items

    public const string ItemsProperty = "Items";
    [DataMember(Name = ItemsProperty, EmitDefaultValue = false)]
    int mItems;
    [PersistantProperty]
    public int Items
    {
      get { return mItems; }
      set { SetProperty<int>(ref mItems, value); }
    }

    #endregion

    #region decimal ForCollection

    public const string ForCollectionProperty = "ForCollection";
    [DataMember(Name = ForCollectionProperty, EmitDefaultValue = false)]
    decimal mForCollection;
    [PersistantProperty]
    public decimal ForCollection
    {
      get { return mForCollection; }
      set { SetProperty<decimal>(ref mForCollection, value); }
    }

    #endregion

    #region decimal OnFloor

    public const string OnFloorProperty = "OnFloor";
    [DataMember(Name = OnFloorProperty, EmitDefaultValue = false)]
    decimal mOnFloor;
    [PersistantProperty]
    public decimal OnFloor
    {
      get { return mOnFloor; }
      set { SetProperty<decimal>(ref mOnFloor, value); }
    }

    #endregion

    #region decimal Pending

    public const string PendingProperty = "Pending";
    [DataMember(Name = PendingProperty, EmitDefaultValue = false)]
    decimal mPending;
    [PersistantProperty]
    public decimal Pending
    {
      get { return mPending; }
      set { SetProperty<decimal>(ref mPending, value); }
    }

    #endregion

    #region Route Route

    public const string RouteProperty = "Route";
    [PersistantProperty(IsCached = true)]
    public Route Route
    {
      get { return GetCachedObject<Route>(); }
      set { SetCachedObject<Route>(value); }
    }

    #endregion

    #region DistributionCenter DistributionCenter

    public const string DistributionCenterProperty = "DistributionCenter";
    [PersistantProperty(IsCached = true)]
    public DistributionCenter DistributionCenter
    {
      get { return GetCachedObject<DistributionCenter>(); }
      set { SetCachedObject<DistributionCenter>(value); }
    }

    #endregion
    
    #region decimal Volume

    public const string VolumeProperty = "Volume";
    [DataMember(Name = VolumeProperty, EmitDefaultValue = false)]
    decimal mVolume;
    [PersistantProperty]
    public decimal Volume
    {
      get { return mVolume; }
      set { SetProperty<decimal>(ref mVolume, value); }
    }

    #endregion

    #region Bay Bay

    public const string BayProperty = "Bay";
    [PersistantProperty(IsCached = true)]
    public Bay Bay
    {
      get { return BusinessObject.IsNull(GetCachedObject<Bay>()) ? Setting.GetSetting<DefaultBaySetting>().Bay : GetCachedObject<Bay>(); }
      set { SetCachedObject<Bay>(value); }
    }

    #endregion

    #region Associative BusinessObjectCollection<WaybillFlag> WaybillFlags

    public const string WaybillFlagsProperty = "WaybillFlags";
    AssociativeObjectCollection<WaybillFloorStatusWaybillFlag, WaybillFlag> mWaybillFlags;
    [PersistantCollection(AssociativeType = typeof(WaybillFloorStatusWaybillFlag))]
    public BusinessObjectCollection<WaybillFlag> WaybillFlags
    {
      get
      {
        if (mWaybillFlags == null) using (new EventStateSuppressor(this)) { mWaybillFlags = new AssociativeObjectCollection<WaybillFloorStatusWaybillFlag, WaybillFlag>(this); }
        return mWaybillFlags;
      }
    }

    #endregion

    #region Vehicle Vehicle

    public const string VehicleProperty = "Vehicle";
    Vehicle mVehicle;
    public Vehicle Vehicle
    {
      get { return mVehicle; }
      set { SetProperty<Vehicle>(ref mVehicle, value); }
    }

    #endregion






    #region decimal ChargeDocumentation

    public const string ChargeDocumentationProperty = "ChargeDocumentation";
    [DataMember(Name = ChargeDocumentationProperty, EmitDefaultValue = false)]
    decimal mChargeDocumentation;
    [PersistantProperty]
    public decimal ChargeDocumentation
    {
      get { return mChargeDocumentation; }
      set { SetProperty<decimal>(ref mChargeDocumentation, value); }
    }

    #endregion

    #region decimal ChargeTransport

    public const string ChargeTransportProperty = "ChargeTransport";
    [DataMember(Name = ChargeTransportProperty, EmitDefaultValue = false)]
    decimal mChargeTransport;
    [PersistantProperty]
    public decimal ChargeTransport
    {
      get { return mChargeTransport; }
      set { SetProperty<decimal>(ref mChargeTransport, value); }
    }

    #endregion

    #region decimal ChargeCollectionDelivery

    public const string ChargeCollectionDeliveryProperty = "ChargeCollectionDelivery";
    [DataMember(Name = ChargeCollectionDeliveryProperty, EmitDefaultValue = false)]
    decimal mChargeCollectionDelivery;
    [PersistantProperty]
    public decimal ChargeCollectionDelivery
    {
      get { return mChargeCollectionDelivery; }
      set { SetProperty<decimal>(ref mChargeCollectionDelivery, value); }
    }

    #endregion

    #region decimal ChargeVat

    public const string ChargeVatProperty = "ChargeVat";
    [DataMember(Name = ChargeVatProperty, EmitDefaultValue = false)]
    decimal mChargeVat;
    [PersistantProperty]
    public decimal ChargeVat
    {
      get { return mChargeVat; }
      set { SetProperty<decimal>(ref mChargeVat, value); }
    }

    #endregion

    #region decimal ChargeOther

    public const string ChargeOtherProperty = "ChargeOther";
    [DataMember(Name = ChargeOtherProperty, EmitDefaultValue = false)]
    decimal mChargeOther;
    [PersistantProperty]
    public decimal ChargeOther
    {
      get { return mChargeOther; }
      set { SetProperty<decimal>(ref mChargeOther, value); }
    }

    #endregion

    #region decimal ChargeTotal

    public const string ChargeTotalProperty = "ChargeTotal";
    decimal mChargeTotal;
    public decimal ChargeTotal
    {
      get { return ChargeDocumentation + ChargeTransport + ChargeCollectionDelivery + ChargeVat + ChargeOther; }
    }

    #endregion

    #region decimal DeclaredValue

    public const string DeclaredValueProperty = "DeclaredValue";
    [DataMember(Name = DeclaredValueProperty, EmitDefaultValue = false)]
    decimal mDeclaredValue;
    [PersistantProperty]
    public decimal DeclaredValue
    {
      get { return mDeclaredValue; }
      set { SetProperty<decimal>(ref mDeclaredValue, value); }
    }

    #endregion
  }
}

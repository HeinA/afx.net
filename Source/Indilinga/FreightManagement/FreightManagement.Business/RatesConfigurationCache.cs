﻿using Afx.Business;
using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Business.Service;
using FreightManagement.Business.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Business
{
  public class RatesConfigurationCache : IDisposable
  {
    public RatesConfigurationCache()
    {
      PersistanceManager = ExtensibilityManager.PersistanceManager;
      if (PersistanceManager == null) throw new InvalidOperationException("No PersistanceManager defined.");
      Current = this;
    }

    IPersistanceManager PersistanceManager { get; set; }

    public static RatesConfigurationCache Current { get; private set; }

    public void Dispose()
    {
      Current = null;
    }

    DistanceTable mDistanceTable;
    public DistanceTable DistanceTable
    {
      get
      {
        if (mDistanceTable == null)
        {
          //using (var svc = ProxyFactory.GetService<IFreightManagementService>(SecurityContext.ServerName))
          //{
          mDistanceTable = LoadDistanceTable();
          //}
        }
        return mDistanceTable;
      }
    }

    RatesConfiguration mStandardRates;
    public RatesConfiguration StandardRates
    {
      get
      {
        if (mStandardRates == null)
        {
          //using (var svc = ProxyFactory.GetService<IFreightManagementService>(SecurityContext.ServerName))
          //{
            mStandardRates = LoadStandardRatesConfiguration();
            if (mStandardRates == null) throw new CostingException("No standard rates defined.");
          //}
        }
        return mStandardRates;
      }
    }

    Dictionary<Guid, RatesConfiguration> mRatesDictionary = new Dictionary<Guid, RatesConfiguration>();
    public RatesConfiguration GetRatesConfiguration(Guid guid)
    {
      if (!mRatesDictionary.ContainsKey(guid))
      {
        //using (var svc = ProxyFactory.GetService<IFreightManagementService>(SecurityContext.ServerName))
        //{
          mRatesDictionary.Add(guid, GetInstance<FreightManagement.Business.RatesConfiguration>(guid)); // svc.LoadRatesConfiguration(guid));
        //}
      }
      return mRatesDictionary[guid];
    }

    public Guid GetStandardRatesConfiguration()
    {
      try
      {
        SearchResults sr = null;

        using (new ConnectionScope())
        using (new SqlScope("Search Rate Configuration"))
        {
          SearchDatagraph sg = new SearchDatagraph(typeof(RatesConfiguration))
            .Select(RatesConfiguration.IsStandardConfigurationProperty)
            .Where(RatesConfiguration.IsStandardConfigurationProperty, FilterType.Equals, true, true);
          sr = sg.ExecuteQuery(PersistanceManager);
        }

        if (sr.Results.Length == 0)
        {
          return Guid.Empty;
        }
        else
        {
          return sr.Results[0].GlobalIdentifier;
        }
      }
      catch
      {
        throw;
      }
    }

    public RatesConfiguration LoadStandardRatesConfiguration()
    {
      Guid guid = GetStandardRatesConfiguration();
      if (guid == Guid.Empty)
      {
        RatesConfiguration rc = new RatesConfiguration();
        rc.Revisions.Add(new RatesConfigurationRevision());
        rc.IsStandardConfiguration = true;
        rc.Name = "Standard Rates";
        return rc;
      }
      else
      {
        RatesConfiguration rc = GetInstance<RatesConfiguration>(guid);
        return rc;
      }
    }

    const string DistanceTableGuidString = "{5C3012A4-7906-4746-8E02-801FA964D222}";
    DistanceTable LoadDistanceTable()
    {
      Guid guid = Guid.Parse(DistanceTableGuidString);
      DistanceTable dt = GetInstance<DistanceTable>(guid);
      if (dt == null)
      {
        dt = new DistanceTable();
        dt.GlobalIdentifier = guid;
      }
      return dt;
    }

    T GetInstance<T>(Guid globalIdentifier)
      where T : BusinessObject
    {
      using (new ConnectionScope())
      using (new SqlScope("GetInstance"))
      {
        return PersistanceManager.GetRepository<T>().GetInstance(globalIdentifier);
      }
    }
  }
}

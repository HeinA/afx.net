﻿using Afx.Business;
using Afx.Business.Data;
using Geographics.Business;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Business
{
  public class RoutingHelper
  {
    public static void Route(Waybill waybill)
    {
      #region Mark old Routes as deleted

      // Remove and re-add to collection to ensure CollectionViewSource in ViewModel updates correctly

      Stack<WaybillRoute> delete = new Stack<WaybillRoute>();
      foreach (var r in waybill.Routes)
      {
        delete.Push(r);
      }
      while (delete.Count > 0)
      {
        WaybillRoute r = delete.Pop();
        waybill.Routes.Remove(r);
        r.IsDeleted = true;
        waybill.Routes.Add(r);
      }

      waybill.CollectionRoute = null;

      #endregion

      //if (waybill.PointToPoint) return;

      Location sourceWP = waybill.FullCollectionLocation.Waypoint();
      Location destinationWP = waybill.FullDeliveryLocation.Waypoint();

      if (BusinessObject.IsNull(sourceWP))
      {
        throw new RoutingException(string.Format("Location {0} does not contain a waypoint.", waybill.FullCollectionLocation));
      }

      if (BusinessObject.IsNull(destinationWP))
      {
        throw new RoutingException(string.Format("Location {0} does not contain a waypoint.", waybill.FullDeliveryLocation));
      }
      
      #region Build Routes for shortest distance calculation

      Collection<RoutePath> routePaths = new Collection<RoutePath>();
      Collection<Route> routes = Cache.Instance.GetObjects<Route>();
      foreach (Route r in routes.Where(r1 => r1.IsActive))
      {
        if (r.Origin.Equals(r.Destination)) continue;

        //if (r.Origin.Equals(r.Destination)) throw new RoutingException(string.Format("Route {0} source ({1}) and destination ({2}) must differ", r.Name, r.Origin, r.Destination));
        routePaths.Add(new RoutePath(r.Origin, r.Destination, r, false));
        if (r.IsOmnidirectional)
        {
          routePaths.Add(new RoutePath(r.Destination, r.Origin, r, true));
        }

        foreach (Location wp in r.Waypoints)
        {
          if (!r.Origin.Equals(wp)) routePaths.Add(new RoutePath(r.Origin, wp, r, false));
          if (!r.Destination.Equals(wp)) routePaths.Add(new RoutePath(wp, r.Destination, r, false));
          if (r.IsOmnidirectional)
          {
            if (!r.Origin.Equals(wp)) routePaths.Add(new RoutePath(wp, r.Origin, r, true));
            if (!r.Destination.Equals(wp)) routePaths.Add(new RoutePath(r.Destination, wp, r, true));
          }
        }
      }

      #endregion

      try
      {
        DistributionCenter lastDestinationDC = null;

        if (!sourceWP.Equals(destinationWP))
        {
          foreach (RoutePath rp in Graph.CalculateShortestPathBetween<Location>(sourceWP, destinationWP, routePaths))
          {
            DistributionCenter dc = null;
            if (!rp.IsReversed)
            {
              dc = rp.Route.Origin.DistributionCenter();
              lastDestinationDC = rp.Route.Destination.DistributionCenter();
            }
            else
            {
              dc = rp.Route.Destination.DistributionCenter();
              lastDestinationDC = rp.Route.Origin.DistributionCenter();
            }

            if (dc != null)
            {
              WaybillRoute wr = new WaybillRoute(rp.Route, dc);
              WaybillRoute wrOld = null;

              // Assign Arrival / Departure Timestamps if waybill was recosted while in transit
              if (!rp.IsReversed) wrOld = waybill.Routes.Where(wr1 => wr1.IsDeleted && wr1.Route.Origin.Equals(wr.Route.Origin)).FirstOrDefault();
              else wrOld = waybill.Routes.Where(wr1 => wr1.IsDeleted && wr1.Route.Destination.Equals(wr.Route.Destination)).FirstOrDefault();
              if (wrOld != null)
              {
                wr.ArrivalTimestamp = wrOld.ArrivalTimestamp;
                wr.DepartureTimestamp = wrOld.DepartureTimestamp;
              }

              waybill.Routes.Add(wr);
            }
            else waybill.CollectionRoute = rp.Route;
          }
        }
        //else
        //{
        //  waybill.IncludeLocalDelivery = true;
        //}

        if (BusinessObject.IsNull(waybill.CollectionRoute) && waybill.IncludeLocalCollection)
        {
          waybill.CollectionRoute = Cache.Instance.GetObjects<Route>().Where(r => r.Origin.Equals(sourceWP) && r.Destination.Equals(sourceWP)).FirstOrDefault();
        }

        if (waybill.IncludeLocalDelivery || sourceWP.Equals(destinationWP) || waybill.DeliveryCity.GetExtensionObject<DeliveryLocation>().DeliveryPointType == DeliveryPointType.Secondary || (lastDestinationDC != null && lastDestinationDC.Equals(destinationWP.DistributionCenter())))
        {
          Route localDeliveryRoute = Cache.Instance.GetObjects<Route>().Where(r => r.Origin.Equals(destinationWP) && r.Destination.Equals(destinationWP)).FirstOrDefault();
          if (localDeliveryRoute == null) Cache.Instance.GetObjects<Route>().Where(r => r.Origin.Equals(sourceWP) && r.Destination.Equals(sourceWP)).FirstOrDefault();
          if (localDeliveryRoute != null)
          {
            DistributionCenter dc = localDeliveryRoute.Origin.DistributionCenter();
            var wr = new WaybillRoute(localDeliveryRoute, dc);
            var wrOld = waybill.Routes.Where(wr1 => wr1.IsDeleted && wr1.Route.Origin.Equals(wr.Route.Origin)).FirstOrDefault();
            if (wrOld != null)
            {
              wr.ArrivalTimestamp = wrOld.ArrivalTimestamp;
              wr.DepartureTimestamp = wrOld.DepartureTimestamp;
            }
            waybill.Routes.Add(wr);
          }
        }
      }
      catch
      {
        throw new RoutingException(string.Format("Could not calculate route between {0} and {1}", sourceWP.FullNameWithoutZone, destinationWP.FullNameWithoutZone));
      }
    }

    class RoutePath : Path<Location>
    {
      public RoutePath(Location source, Location destination, Route route, bool isReversed)
        : base(source, destination, 1)
      {
        Route = route;
        IsReversed = isReversed;
      }

      #region Route Route

      Route mRoute;
      public Route Route
      {
        get { return mRoute; }
        private set { mRoute = value; }
      }

      #endregion

      #region bool IsReversed

      public const string IsReversedProperty = "IsReversed";
      bool mIsReversed;
      public bool IsReversed
      {
        get { return mIsReversed; }
        set { mIsReversed = value; }
      }

      #endregion

      public override string ToString()
      {
        return string.Format("{0} -> {1} ({2})", Source.FullNameWithoutZone, Destination.FullNameWithoutZone, Route.Name);
      }
    }
  }
}

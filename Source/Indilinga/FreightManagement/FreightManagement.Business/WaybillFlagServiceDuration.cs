﻿using Afx.Business;
using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace FreightManagement.Business
{
  [PersistantObject(Schema = "FreightManagement")]
  public partial class WaybillFlagServiceDuration : ExtensionObject<WaybillFlag>
  {
    #region Constructors

    public WaybillFlagServiceDuration()
    {
    }

    #endregion

    //#region int ExpectedServiceDuration

    //public const string ExpectedServiceDurationProperty = "ExpectedServiceDuration";
    //[DataMember(Name = ExpectedServiceDurationProperty, EmitDefaultValue = false)]
    //int mExpectedServiceDuration;
    //[PersistantProperty]
    //public int ExpectedServiceDuration
    //{
    //  get { return mExpectedServiceDuration; }
    //  set { SetProperty<int>(ref mExpectedServiceDuration, value); }
    //}

    //#endregion

    #region int MinimumServiceDuration

    public const string MinimumServiceDurationProperty = "MinimumServiceDuration";
    [DataMember(Name = MinimumServiceDurationProperty, EmitDefaultValue = false)]
    int mMinimumServiceDuration;
    [PersistantProperty]
    public int MinimumServiceDuration
    {
      get { return mMinimumServiceDuration; }
      set { SetProperty<int>(ref mMinimumServiceDuration, value); }
    }

    #endregion

    #region int MaximumServiceDuration

    public const string MaximumServiceDurationProperty = "MaximumServiceDuration";
    [DataMember(Name = MaximumServiceDurationProperty, EmitDefaultValue = false)]
    int mMaximumServiceDuration;
    [PersistantProperty]
    public int MaximumServiceDuration
    {
      get { return mMaximumServiceDuration; }
      set { SetProperty<int>(ref mMaximumServiceDuration, value); }
    }

    #endregion
  }
}

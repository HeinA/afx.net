﻿using Afx.Business;
using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace FreightManagement.Business
{
  [PersistantObject(Schema = "FreightManagement")]
  public partial class RatesConfigurationRevisionLevyType : AssociativeObject<RatesConfigurationRevision, WaybillLevyType>
  {
    #region decimal Rate

    public const string RateProperty = "Rate";
    [DataMember(Name = RateProperty, EmitDefaultValue = false)]
    decimal mRate;
    [PersistantProperty]
    public decimal Rate
    {
      get { return mRate; }
      set { SetProperty<decimal>(ref mRate, value); }
    }

    #endregion
  }
}

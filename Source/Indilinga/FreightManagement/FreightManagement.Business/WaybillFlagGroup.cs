﻿using Afx.Business;
using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Validation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace FreightManagement.Business
{
  [PersistantObject(Schema = "FreightManagement", HasFlags = true)]
  [Cache]
  public partial class WaybillFlagGroup : BusinessObject, IRevisable 
  {
    #region Constructors

    public WaybillFlagGroup()
    {
    }

    public WaybillFlagGroup(bool isNull)
      : base(isNull)
    {
    }

    #endregion

    #region int Revision

    public const string RevisionProperty = "Revision";
    [DataMember(Name = RevisionProperty, EmitDefaultValue = false)]
    int mRevision;
    [PersistantProperty]
    public int Revision
    {
      get { return mRevision; }
      set { SetProperty<int>(ref mRevision, value); }
    }

    #endregion
    
    #region string Text

    public const string TextProperty = "Text";
    [DataMember(Name = TextProperty, EmitDefaultValue = false)]
    string mText;
    [PersistantProperty]
    [Mandatory("Text is mandatory.")]
    public string Text
    {
      get { return mText; }
      set { SetProperty<string>(ref mText, value); }
    }

    #endregion

    #region bool IsMutuallyExclusive

    public const string IsMutuallyExclusiveProperty = "IsMutuallyExclusive";
    [DataMember(Name = IsMutuallyExclusiveProperty, EmitDefaultValue = false)]
    bool mIsMutuallyExclusive;
    [PersistantProperty]
    public bool IsMutuallyExclusive
    {
      get { return mIsMutuallyExclusive; }
      set { SetProperty<bool>(ref mIsMutuallyExclusive, value); }
    }

    #endregion

    #region bool IsMandatory

    public const string IsMandatoryProperty = "IsMandatory";
    [DataMember(Name = IsMandatoryProperty, EmitDefaultValue = false)]
    bool mIsMandatory;
    [PersistantProperty]
    public bool IsMandatory
    {
      get { return mIsMandatory; }
      set { SetProperty<bool>(ref mIsMandatory, value); }
    }

    #endregion

    #region BusinessObjectCollection<WaybillFlag> Flags

    public const string FlagsProperty = "Flags";
    BusinessObjectCollection<WaybillFlag> mFlags;
    [PersistantCollection]
    [DataMember]
    public BusinessObjectCollection<WaybillFlag> Flags
    {
      get { return mFlags ?? (mFlags = new BusinessObjectCollection<WaybillFlag>(this)); }
    }

    #endregion
  }
}

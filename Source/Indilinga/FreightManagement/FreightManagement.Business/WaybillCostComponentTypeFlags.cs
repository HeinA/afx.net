﻿using Afx.Business;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Business
{
  [Export(typeof(IObjectFlags))]
  public class WaybillCostComponentTypeFlags : IObjectFlags
  {
    public const string DocumentationCharge = "FreightManagement.DocumentationCharge";
    public const string CollectionDeliveryCharge = "FreightManagement.CollectionDeliveryCharge";
    public const string TransportCharge = "FreightManagement.TransportCharge";
    public const string VatCharge = "FreightManagement.VatCharge";
    public const string ImportVatCharge = "FreightManagement.ImportVatCharge";

    public Type ObjectType
    {
      get { return typeof(WaybillCostComponentType); }
    }

    public IEnumerable<string> Flags
    {
      get { return new string[] { DocumentationCharge, CollectionDeliveryCharge, TransportCharge, VatCharge, ImportVatCharge }; } 
    }
  }
}

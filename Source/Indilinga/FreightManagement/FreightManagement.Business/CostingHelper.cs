﻿using Afx.Business;
using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Business.Service;
using FreightManagement.Business.Service;
using Geographics.Business;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Business
{
  public static class CostingHelper
  {
    #region Collection<WaybillCharge> Cost(...)

    public static void Cost(Waybill waybill)
    {
      if (RatesConfigurationCache.Current == null) throw new InvalidOperationException("RatesConfigurationCache has not been initialized");

      try
      {
        Collection<Business.RatesConfiguration> rates = new Collection<Business.RatesConfiguration>();
        DistanceTable dt = RatesConfigurationCache.Current.DistanceTable;
        rates.Add(RatesConfigurationCache.Current.StandardRates);
        if (waybill.RatesConfiguration != null) rates.Add(RatesConfigurationCache.Current.GetRatesConfiguration(waybill.RatesConfiguration.GlobalIdentifier));

        //using (var svc = ProxyFactory.GetService<IFreightManagementService>(SecurityContext.ServerName))
        //{
        //  dt = svc.LoadDistanceTable();
        //  RatesConfiguration sr = svc.LoadStandardRatesConfiguration();
        //  if (sr == null) throw new CostingException("No standard rates defined.");
        //  rates.Add(sr);
        //  if (waybill.RatesConfiguration != null) rates.Add(svc.LoadRatesConfiguration(waybill.RatesConfiguration.GlobalIdentifier));
        //}

        Collection<CostingHelper.CostSegment> segments = new Collection<CostSegment>();

        decimal totalweight = waybill.Items.Sum(i => i.PhysicalWeight);

        foreach (var ct in waybill.Items.Where(i => !i.IsDeleted).Select(ci => ci.CargoType).Distinct())
        {
          Collection<CostingHelper.ShippingCost> costs = new Collection<CostingHelper.ShippingCost>();

          Collection<string> segmentKeys = null;
          CostingSetting cs = Setting.GetSetting<CostingSetting>();
          if (!cs.PreferCheapestRate)
          {
            segmentKeys = new Collection<string>();
          }

          decimal units = 0;
          if (ct.BillingUnit == BillingUnit.Weight)
          {
            var pw = waybill.Items.Where(i => i.CargoType.Equals(ct) && !i.IsDeleted).Sum(x => x.PhysicalWeight);
            var vw = waybill.Items.Where(i => i.CargoType.Equals(ct) && !i.IsDeleted).Sum(x => x.VolumetricWeight);
            if (pw > vw) units = pw;
            else units = vw; ;
          }
          else
          {
            units = waybill.Items.Where(i => i.CargoType.Equals(ct) && !i.IsDeleted).Sum(x => x.BillableUnits);
            //foreach (var ci in waybill.Items.Where(i => i.CargoType.Equals(ct) && !i.IsDeleted))
            //{
            //  units += ci.BillableUnits;
            //}
          }

          try
          {
            #region Try Standard Tables

            LinkedList<Path<Location>> costList = null;

            try
            {
              IEnumerable<RatesConfiguration> applicableRates = null;
              if (cs.PreferCheapestRate) applicableRates = rates.OrderBy(rc => rc.IsStandardConfiguration);
              else applicableRates = rates.Where(rc => !rc.IsStandardConfiguration);

              foreach (var r in applicableRates)
              {
                var revision = r.GetRevision(waybill.DocumentDate);
                if (revision == null) throw new MessageException("There is no revision for the selected date.");

                //foreach (var rt in revision.TablesByWeight.Where(t => (BusinessObject.IsNull(t.RequiredFlag) && !waybill.WaybillFlags.Any(f => f.Owner.IsFlagged(WaybillFlagGroupFlags.CostingFlagGroup))) || waybill.WaybillFlags.Contains(t.RequiredFlag)))
                foreach (var rt in revision.TablesByWeight.Where(t => BusinessObject.IsNull(t.RequiredFlag) || waybill.WaybillFlags.Contains(t.RequiredFlag)))
                {
                  AddCosts(costs, rt, "Total Weight", r, totalweight, true, segmentKeys);
                }

                if (revision.GetAssociativeObject<RatesConfigurationRevisionCargoType>(ct) != null)
                {
                  //foreach (var rt in revision.GetAssociativeObject<RatesConfigurationRevisionCargoType>(ct).Tables.Where(t => (BusinessObject.IsNull(t.RequiredFlag) && !waybill.WaybillFlags.Any(f => f.Owner.IsFlagged(WaybillFlagGroupFlags.CostingFlagGroup))) || waybill.WaybillFlags.Contains(t.RequiredFlag)))
                  foreach (var rt in revision.GetAssociativeObject<RatesConfigurationRevisionCargoType>(ct).Tables.Where(t => BusinessObject.IsNull(t.RequiredFlag) || waybill.WaybillFlags.Contains(t.RequiredFlag)))
                  {
                    AddCosts(costs, rt, ct.Text, r, units, false, segmentKeys);
                  }
                }
              }

              costList = Graph.CalculateShortestPathBetween<Location>(waybill.CollectionLocation, waybill.DeliveryLocation, costs);
            }
            catch (RoutingException)
            {
              if (cs.PreferCheapestRate) throw; // Standard rates have alread been tried

              #region Retry with Standars Rates included

              foreach (var r in rates.OrderBy(rc => rc.IsStandardConfiguration))
              {
                var revision = r.GetRevision(waybill.DocumentDate);

                //foreach (var rt in revision.TablesByWeight.Where(t => (BusinessObject.IsNull(t.RequiredFlag) && !waybill.WaybillFlags.Any(f => f.Owner.IsFlagged(WaybillFlagGroupFlags.CostingFlagGroup))) || waybill.WaybillFlags.Contains(t.RequiredFlag)))
                foreach (var rt in revision.TablesByWeight.Where(t => BusinessObject.IsNull(t.RequiredFlag) || waybill.WaybillFlags.Contains(t.RequiredFlag)))
                {
                  AddCosts(costs, rt, "Total Weight", r, totalweight, true, segmentKeys);
                }

                if (revision.GetAssociativeObject<RatesConfigurationRevisionCargoType>(ct) != null)
                {
                  //foreach (var rt in revision.GetAssociativeObject<RatesConfigurationRevisionCargoType>(ct).Tables.Where(t => (BusinessObject.IsNull(t.RequiredFlag) && !waybill.WaybillFlags.Any(f => f.Owner.IsFlagged(WaybillFlagGroupFlags.CostingFlagGroup))) || waybill.WaybillFlags.Contains(t.RequiredFlag)))
                  foreach (var rt in revision.GetAssociativeObject<RatesConfigurationRevisionCargoType>(ct).Tables.Where(t => BusinessObject.IsNull(t.RequiredFlag) || waybill.WaybillFlags.Contains(t.RequiredFlag)))
                  {
                    AddCosts(costs, rt, ct.Text, r, units, false, segmentKeys);
                  }
                }
              }

              costList = Graph.CalculateShortestPathBetween<Location>(waybill.CollectionLocation, waybill.DeliveryLocation, costs);
            }

              #endregion

            foreach (CostingHelper.ShippingCost sc in costList)
            {
              if (sc.ChargeOnceOnly)
              {
                CostingHelper.CostSegment c = segments.FirstOrDefault(c1 => c1.ChargeOnceOnly && c1.Origin.Equals(sc.Source) && c1.Destination.Equals(sc.Destination));
                if (c == null) segments.Add(new CostSegment(sc.Source, sc.Destination, sc.Description, sc.RatesConfiguration, sc.Cost, sc.BaseCharge, sc.BillableUnits, sc.Rate, sc.IsFixed, sc.IsPercentage, sc.ChargeOnceOnly, sc.ChargeType, sc.Charges));
              }
              else
              {
                segments.Add(new CostSegment(sc.Source, sc.Destination, sc.Description, sc.RatesConfiguration, sc.Cost, sc.BaseCharge, sc.BillableUnits, sc.Rate, sc.IsFixed, sc.IsPercentage, sc.ChargeOnceOnly, sc.ChargeType, sc.Charges));
              }
            }

            #endregion
          }
          catch (RoutingException)
          {
            if (!cs.UseDistanceTables) throw; //Distance Table are not applicable

            #region Try Distance Tables

            try
            {
              if (dt == null)
              {
                throw new CostingException(string.Format("Could not cost document.  Please ensure rate tables are set up for {0} between orign and destination.", ct.Text));
              }

              DistanceTableLocationRow row = dt.Rows.FirstOrDefault(r => r.Location.Equals(waybill.CollectionCity));
              if (row == null)
              {
                throw new CostingException("Could not cost document.  Origin not found in Distance Table.");
              }
              DistanceTableDistance dtd = row.Cells.FirstOrDefault(c => c.Column.Location.Equals(waybill.DeliveryCity));
              if (dtd == null)
              {
                throw new CostingException("Could not cost document.  Destination not found in Distance Table.");
              }

              int distance = dtd.Distance;
              DistanceRateTableDistanceCategory drtdc = null;
              string description = null;
              RatesConfiguration rcUsed = null;

              foreach (var r in rates.OrderBy(rc => rc.IsStandardConfiguration))
              {
                var revision = r.GetRevision(waybill.DocumentDate);

                //foreach (var rt in revision.DistanceTablesByWeight.Where(t => (BusinessObject.IsNull(t.RequiredFlag) && !waybill.WaybillFlags.Any(f => f.Owner.IsFlagged(WaybillFlagGroupFlags.CostingFlagGroup))) || waybill.WaybillFlags.Contains(t.RequiredFlag)))
                foreach (var rt in revision.DistanceTablesByWeight.Where(t => BusinessObject.IsNull(t.RequiredFlag) || waybill.WaybillFlags.Contains(t.RequiredFlag)))
                {
                  drtdc = rt.Rows.LastOrDefault(rt1 => rt1.Distance < distance);
                  if (drtdc != null)
                  {
                    description = "Total Weight";
                    rcUsed = r;
                    units = totalweight;
                    break;
                  }
                }

                if (drtdc == null)
                {
                  if (revision.GetAssociativeObject<RatesConfigurationRevisionCargoType>(ct) != null)
                  {
                    //foreach (var rt in revision.GetAssociativeObject<RatesConfigurationRevisionCargoType>(ct).DistanceTables.Where(t => (BusinessObject.IsNull(t.RequiredFlag) && !waybill.WaybillFlags.Any(f => f.Owner.IsFlagged(WaybillFlagGroupFlags.CostingFlagGroup))) || waybill.WaybillFlags.Contains(t.RequiredFlag)))
                    foreach (var rt in revision.GetAssociativeObject<RatesConfigurationRevisionCargoType>(ct).DistanceTables.Where(t => BusinessObject.IsNull(t.RequiredFlag) || waybill.WaybillFlags.Contains(t.RequiredFlag)))
                    {
                      drtdc = rt.Rows.LastOrDefault(rt1 => rt1.Distance < distance);
                      if (drtdc != null)
                      {
                        description = ct.Text;
                        rcUsed = r;
                        break;
                      }
                    }
                  }
                }
              }

              if (drtdc == null)
              {
                throw new CostingException(string.Format("Could not cost document.  Please ensure rate tables are set up for {0} between orign and destination.", ct.Text));
              }

              decimal cost = 0;
              decimal rate = 0;
              decimal baseCharge1 = 0;
              bool isFixed = false;
              bool isPercentage = false;
              WaybillChargeType chargeType = null;
              int billableUnits = (int)Math.Ceiling(units);
              if (drtdc.GetCost(ref billableUnits, ref cost, ref baseCharge1, ref rate, ref isFixed, ref isPercentage, ref chargeType))
              {
                segments.Add(new CostSegment(waybill.CollectionCity, waybill.DeliveryCity, description, rcUsed, cost, baseCharge1, billableUnits, rate, isFixed, isPercentage, true, chargeType, GetCharges(drtdc)) { Distance = distance });
              }


            }
            catch (CostingException)
            {
              throw;
            }
            catch (Exception ex1)
            {
              throw new CostingException(string.Format("Could not cost document.  Please ensure rate tables are set up for {0} between orign and destination.", ct.Text), ex1);
            }

            #endregion
          }
        }

        Collection<CostingHelper.CostSegment> sortedSegments = new Collection<CostSegment>();
        SortSegments(waybill.CollectionLocation, segments, sortedSegments);
        string text = PrintCosts(sortedSegments);

        #region Mark old Charges as deleted

        // Remove and re-add to collection to ensure CollectionViewSource in ViewModel updates correctly

        Stack<WaybillCharge> delete = new Stack<WaybillCharge>();
        foreach (var r in waybill.Charges.Where(c => c.SystemGenerated))
        {
          delete.Push(r);
        }
        while (delete.Count > 0)
        {
          WaybillCharge r = delete.Pop();
          waybill.Charges.Remove(r);
          r.IsDeleted = true;
          waybill.Charges.Add(r);
        }

        waybill.CollectionRoute = null;

        #endregion

        Collection<WaybillCharge> charges = NormaliseCharges(sortedSegments);

        #region Calculate Insurance

        if (waybill.IsInsured)
        {
          decimal insurance = 0;

          WaybillCostComponentType cci = Cache.Instance.GetObjects<WaybillCostComponentType>().FirstOrDefault(cc1 => cc1.IsInsurance);
          if (cci == null) throw new CostingException("No Insurance Cost Component found.");
          decimal declared = waybill.Items.Sum(i => i.DeclaredValue);
          foreach (var r in rates.OrderByDescending(rc => rc.IsStandardConfiguration))
          {
            var revision = r.GetRevision(waybill.DocumentDate);
            if (revision.InsuranceRate > 0)
            {
              insurance = declared * revision.InsuranceRate / 100m;
              if (insurance > 0)
              {
                charges.Add(new WaybillCharge(cci, insurance));
                string line = string.Format("{0} ({1})", cci.Text, r.Name);
                string amount = insurance.ToString("#,##0.00");
                line = line.PadRight(Padding - amount.Length, '.');
                line = string.Format("\n{0} {1}", line, amount);
                text += line;
              }
              break;
            }
          }
        }

        #endregion

        #region Calculate Import VAT

        if (waybill.ApplyImportVAT && !waybill.CollectionLocation.Country.Equals(waybill.DeliveryLocation.Country))
        {
          decimal importVat = 0;

          WaybillCostComponentType cci = Cache.Instance.GetObjects<WaybillCostComponentType>().FirstOrDefault(cc1 => cc1.IsFlagged(WaybillCostComponentTypeFlags.ImportVatCharge));
          if (cci == null) throw new CostingException("No Import Vat Cost Component found.");
          decimal declared = waybill.Items.Sum(i => i.DeclaredValue);
          decimal vatRate = waybill.DeliveryLocation.Country.GetExtensionObject<LocationExtension>().ImportVATRate;
          if (vatRate > 0)
          {
            importVat = declared * vatRate / 100m;
            if (importVat > 0)
            {
              charges.Add(new WaybillCharge(cci, importVat));
              string line = string.Format("{0} @ {1:0}%", cci.Text, vatRate);
              string amount = importVat.ToString("#,##0.00");
              line = line.PadRight(Padding - amount.Length, '.');
              line = string.Format("\n{0} {1}", line, amount);
              text += line;
            }
          }
        }

        #endregion

        #region Calculate Applicable Levies

        RatesConfiguration lc = rates.Where(rc => !rc.IsStandardConfiguration).FirstOrDefault();
        if (lc == null) lc = rates.Where(rc => rc.IsStandardConfiguration).FirstOrDefault();
        RatesConfigurationRevision rcr = lc.GetRevision(waybill.DocumentDate);

        foreach (var levyType in rcr.LevyTypes)
        {
          RatesConfigurationRevisionLevyType rclv = rcr.GetAssociativeObject<RatesConfigurationRevisionLevyType>(levyType);

          if (levyType.IsApplicable(waybill))
          {
            decimal applicableCharges = 0;

            foreach (var wc in charges)
            {
              if (levyType.ApplicableCostComponents.Contains(wc.ChargeType))
              {
                applicableCharges += wc.Charge;
              }
            }

            if (levyType.LevyCalculationType == LevyCalculationType.FixedPercentage)
            {
              decimal levy = applicableCharges * (rclv.Rate / 100);
              if (levy > 0)
              {
                charges.Add(new WaybillCharge(levyType, levy));
                string line = string.Format("{0} ({1})", levyType.Text, lc.Name);
                string amount = levy.ToString("#,##0.00");
                line = line.PadRight(Padding - amount.Length, '.');
                line = string.Format("\n{0} {1}", line, amount);
                text += line;
              }
            }
          }
        }

        #endregion

        waybill.ChargeDescription = text;

        foreach (var c in charges)
        {
          c.SystemGenerated = true;
          waybill.Charges.Add(c);
        }
      }
      catch (RoutingException)
      {
        throw new MessageException(string.Format("Could not cost waybill {0}.", waybill.DocumentNumber));
      }
      catch
      {
        throw;
      }
    }

    #endregion

    #region Private

    static void SortSegments(Location origin, Collection<CostingHelper.CostSegment> segments, Collection<CostingHelper.CostSegment> sortedSegments)
    {
      Collection<WaybillCostComponentType> singleCharges = new Collection<WaybillCostComponentType>();

      foreach (var segment in segments.Where(r => r.Origin.Contains(origin)))
      {
        if (!sortedSegments.Contains(segment))
        {
          SortSegments(segment, segments, sortedSegments, singleCharges);
        }
      }

      foreach (var ct in sortedSegments.SelectMany(s => s.Charges).Select(s => s.ChargeType).Distinct().Where(ct1 => ((WaybillCostComponentType)ct1).ChargeOnce))
      {
        decimal max = 0;
        WaybillCharge c1 = null;
        try
        {
          max = segments.SelectMany(s => s.Charges).Where(c => (c.ChargeType as WaybillCostComponentType) != null && c.ChargeType.Equals(ct)).Max(c => c.Charge); //.Where(c => ((WaybillCostComponentType)c.ChargeType).ChargeOnce)
        }
        catch { }

        try
        {
          c1 = sortedSegments.SelectMany(s => s.Charges).Where(c => (c.ChargeType as WaybillCostComponentType) != null && c.ChargeType.Equals(ct)).Where(c => c.Charge == max).FirstOrDefault(); //((WaybillCostComponentType)c.ChargeType).ChargeOnce && 
        }
        catch { }

        if (c1 != null)
        {
          foreach (WaybillCharge c2 in sortedSegments.SelectMany(s => s.Charges).Where(c => (c.ChargeType as WaybillCostComponentType) != null && c.ChargeType.Equals(ct)).Where(c => c != c1)) //((WaybillCostComponentType)c.ChargeType).ChargeOnce && 
          {
            c2.Charge = 0;
          }
        }
      }
    }

    static void SortSegments(CostSegment segment, Collection<CostingHelper.CostSegment> segments, Collection<CostingHelper.CostSegment> sortedSegments, Collection<WaybillCostComponentType> singleCharges)
    {
      sortedSegments.Add(segment);

      foreach (var segment1 in segments.Where(r => r.Origin.Equals(segment.Destination) && !sortedSegments.Contains(r)))
      {
        SortSegments(segment1, segments, sortedSegments, singleCharges);
      }
    }

    #region class CostSegment

    class CostSegment
    {
      public CostSegment(Location origin, Location destination, string description, RatesConfiguration rc, decimal cost, decimal baseCharge, decimal billableUnits, decimal rate, bool isFixed, bool isPercentage, bool chargeOnceOnly, WaybillChargeType chargeType, Collection<WaybillCharge> charges)
      {
        Origin = origin;
        Destination = destination;
        Description = description;
        BillableUnits = billableUnits;
        Cost = cost;
        Rate = rate;
        BaseCharge = baseCharge;
        IsFixed = isFixed;
        IsPercentage = isPercentage;
        ChargeOnceOnly = chargeOnceOnly;
        Charges = charges;
        ChargeType = chargeType;
        RatesConfiguration = rc;
      }

      #region RatesConfiguration RatesConfiguration

      public const string RatesConfigurationProperty = "RatesConfiguration";
      RatesConfiguration mRatesConfiguration;
      public RatesConfiguration RatesConfiguration
      {
        get { return mRatesConfiguration; }
        set { mRatesConfiguration = value; }
      }

      #endregion

      #region Location Origin

      public const string OriginProperty = "Origin";
      Location mOrigin;
      public Location Origin
      {
        get { return mOrigin; }
        private set { mOrigin = value; }
      }

      #endregion

      #region Location Destination

      public const string DestinationProperty = "Destination";
      Location mDestination;
      public Location Destination
      {
        get { return mDestination; }
        private set { mDestination = value; }
      }

      #endregion

      #region string Description

      public const string DescriptionProperty = "Description";
      string mDescription;
      public string Description
      {
        get { return mDescription; }
        set { mDescription = value; }
      }

      #endregion

      #region decimal BillableUnits

      public const string BillableUnitsProperty = "BillableUnits";
      decimal mBillableUnits;
      public decimal BillableUnits
      {
        get { return mBillableUnits; }
        private set { mBillableUnits = value; }
      }

      #endregion

      #region decimal Cost

      public const string CostProperty = "Cost";
      decimal mCost;
      public decimal Cost
      {
        get { return mCost; }
        private set { mCost = value; }
      }

      #endregion

      #region decimal BaseCharge

      public const string BaseChargeProperty = "BaseCharge";
      decimal mBaseCharge;
      public decimal BaseCharge
      {
        get { return mBaseCharge; }
        set { mBaseCharge = value; }
      }

      #endregion

      #region decimal Rate

      public const string RateProperty = "Rate";
      decimal mRate;
      public decimal Rate
      {
        get { return mRate; }
        private set { mRate = value; }
      }

      #endregion

      #region bool IsFixed

      public const string IsFixedProperty = "IsFixed";
      bool mIsFixed;
      public bool IsFixed
      {
        get { return mIsFixed; }
        private set { mIsFixed = value; }
      }

      #endregion

      #region bool IsPercentage

      public const string IsPercentageProperty = "IsPercentage";
      bool mIsPercentage;
      public bool IsPercentage
      {
        get { return mIsPercentage; }
        set { mIsPercentage = value; }
      }

      #endregion

      #region bool ChargeOnceOnly

      public const string ChargeOnceOnlyProperty = "ChargeOnceOnly";
      bool mChargeOnceOnly;
      public bool ChargeOnceOnly
      {
        get { return mChargeOnceOnly; }
        private set { mChargeOnceOnly = value; }
      }

      #endregion

      #region WaybillChargeType ChargeType

      public const string ChargeTypeProperty = "ChargeType";
      WaybillChargeType mChargeType;
      public WaybillChargeType ChargeType
      {
        get { return mChargeType; }
        set { mChargeType = value; }
      }

      #endregion

      #region Collection<WaybillCharge> Charges

      Collection<WaybillCharge> mCharges = new Collection<WaybillCharge>();
      public Collection<WaybillCharge> Charges
      {
        get { return mCharges; }
        private set { mCharges = value; }
      }

      #endregion

      #region int? Distance

      public const string DistanceProperty = "Distance";
      int? mDistance;
      public int? Distance
      {
        get { return mDistance; }
        set { mDistance = value; }
      }

      #endregion
    }

    #endregion

    #region class ShippingCost : Path<Location>

    class ShippingCost : Path<Location>
    {
      public ShippingCost(Location source, Location destination, decimal cost, string description, RatesConfiguration rc, decimal billableUnits, decimal rate, decimal baseCharge, bool isFixed, bool isPercentage, bool chargeOnceOnly, WaybillChargeType chargeType, Collection<WaybillCharge> charges)
        : base(source, destination, cost)
      {
        Description = description;
        Rate = rate;
        BaseCharge = baseCharge;
        IsFixed = isFixed;
        IsPercentage = isPercentage;
        BillableUnits = billableUnits;
        ChargeOnceOnly = chargeOnceOnly;
        Charges = charges;
        ChargeType = chargeType;
        RatesConfiguration = rc;
      }

      #region RatesConfiguration RatesConfiguration

      public const string RatesConfigurationProperty = "RatesConfiguration";
      RatesConfiguration mRatesConfiguration;
      public RatesConfiguration RatesConfiguration
      {
        get { return mRatesConfiguration; }
        set { mRatesConfiguration = value; }
      }

      #endregion

      #region WaybillChargeType ChargeType

      public const string ChargeTypeProperty = "ChargeType";
      WaybillChargeType mChargeType;
      public WaybillChargeType ChargeType
      {
        get { return mChargeType; }
        set { mChargeType = value; }
      }

      #endregion

      #region string Description

      public const string DescriptionProperty = "Description";
      string mDescription;
      public string Description
      {
        get { return mDescription; }
        set { mDescription = value; }
      }

      #endregion

      #region decimal BillableUnits

      public const string BillableUnitsProperty = "BillableUnits";
      decimal mBillableUnits;
      public decimal BillableUnits
      {
        get { return mBillableUnits; }
        set { mBillableUnits = value; }
      }

      #endregion

      #region decimal BaseCharge

      public const string BaseChargeProperty = "BaseCharge";
      decimal mBaseCharge;
      public decimal BaseCharge
      {
        get { return mBaseCharge; }
        set { mBaseCharge = value; }
      }

      #endregion

      #region decimal Rate

      public const string RateProperty = "Rate";
      decimal mRate;
      public decimal Rate
      {
        get { return mRate; }
        set { mRate = value; }
      }

      #endregion

      #region bool IsFixed

      public const string IsFixedProperty = "IsFixed";
      bool mIsFixed;
      public bool IsFixed
      {
        get { return mIsFixed; }
        set { mIsFixed = value; }
      }

      #endregion

      #region bool IsPercentage

      public const string IsPercentageProperty = "IsPercentage";
      bool mIsPercentage;
      public bool IsPercentage
      {
        get { return mIsPercentage; }
        set { mIsPercentage = value; }
      }

      #endregion

      #region bool ChargeOnceOnly

      public const string ChargeOnceOnlyProperty = "ChargeOnceOnly";
      bool mChargeOnceOnly;
      public bool ChargeOnceOnly
      {
        get { return mChargeOnceOnly; }
        set { mChargeOnceOnly = value; }
      }

      #endregion

      #region Collection<CostingHelper.Charge> Charges

      Collection<WaybillCharge> mCharges = new Collection<WaybillCharge>();
      public Collection<WaybillCharge> Charges
      {
        get { return mCharges; }
        private set { mCharges = value; }
      }

      #endregion

      public override string ToString()
      {
        return string.Format("{3}: {0} == {1} ==> {2}", Source.FullName, Cost, Destination.FullName, RatesConfiguration.Name);
      }
    }

    #endregion


    #region string PrintCosts(...)

    const int Padding = 100;
    static string PrintCosts(IEnumerable<CostSegment> costs)
    {
      Collection<string> lines = new Collection<string>();

      foreach (var cs in costs)
      {
        lines.Add(string.Format("{0} from {1} to {2}{4} ({3})", cs.Description, cs.Origin.Name, cs.Destination.Name, cs.RatesConfiguration.Name, cs.Distance == null ? string.Empty : string.Format(" - {0} Km", cs.Distance)));
        string line = null;
        if (cs.IsPercentage)
        {
          line = string.Format("   {0:#,##0} unit(s) @ {1:#,##0.00} %", cs.BillableUnits, cs.Rate);
        }
        else
        {
          line = string.Format("   {3}{0:#,##0} unit(s) @ {1:#,##0.00} {2}", cs.BillableUnits, cs.Rate, cs.IsFixed ? "fixed" : "each", cs.BaseCharge == 0 ? string.Empty : string.Format("{0:#,##0.00} + ", cs.BaseCharge));
        }
        string amount = cs.Cost.ToString("#,##0.00");
        line = line.PadRight(Padding - amount.Length, '.');
        line = string.Format("{0} {1}", line, amount);
        lines.Add(line);
        foreach (var f in cs.Charges)
        {
          line = string.Format("   {0}", f.ChargeType.Text);
          amount = f.Charge.ToString("#,##0.00");
          line = line.PadRight(Padding - amount.Length, '.');
          line = string.Format("{0} {1}", line, amount);
          lines.Add(line);
        }
      }

      return string.Join("\n", lines);
    }


    #endregion

    #region Collection<WaybillCharge> NormaliseCharges(...)

    static Collection<WaybillCharge> NormaliseCharges(Collection<CostingHelper.CostSegment> costSegments)
    {
      Collection<WaybillCharge> charges = new Collection<WaybillCharge>();

      foreach (var cs in costSegments)
      {
        {
          WaybillCharge wc = charges.Where(wc1 => wc1.ChargeType.Equals(cs.ChargeType)).FirstOrDefault();
          if (wc == null)
          {
            wc = new WaybillCharge(cs.ChargeType, cs.Cost);
            charges.Add(wc);
          }
          else
          {
            wc.Charge += cs.Cost;
          }
        }

        foreach (var subCharge in cs.Charges)
        {
          WaybillCharge wc = charges.Where(wc1 => wc1.ChargeType.Equals(subCharge.ChargeType)).FirstOrDefault();
          if (wc == null)
          {
            wc = new WaybillCharge(subCharge.ChargeType, subCharge.Charge);
            charges.Add(wc);
          }
          else
          {
            wc.Charge += subCharge.Charge;
          }
        }
      }

      return charges;
    }

    #endregion

    #region void AddCosts(...)

    static string GetKey(CostingHelper.ShippingCost sc, WaybillFlag flag)
    {
      return string.Format("{0}:{1}->{2}{3}", sc.Description, sc.Source.FullName, sc.Destination.FullName, BusinessObject.IsNull(flag) ? string.Empty : string.Format(" [{0}]", flag.Text));
    }

    static void AddCosts(Collection<CostingHelper.ShippingCost> costs, RateTable rt, string description, RatesConfiguration rc, decimal units, bool chargeOnceOnly, Collection<string> segmentKeys)
    {
      foreach (var d in rt.Rows)
      {
        decimal cost = 0;
        decimal rate = 0;
        decimal baseCharge = 0;
        bool isFixed = false;
        bool isPercentage = false;
        WaybillChargeType chargeType = null;
        decimal billableUnits = 0;
        if (isPercentage)
        {
          billableUnits = units;
        }
        else
        {
          billableUnits = Math.Ceiling(units);
        }
        if (d.GetCost(ref billableUnits, ref cost, ref baseCharge, ref rate, ref isFixed, ref isPercentage, ref chargeType))
        {
          AddCostSegment(costs, segmentKeys, rc, rt, rt.Origin, d.Location, cost, description, billableUnits, baseCharge, rate, isFixed, isPercentage, chargeOnceOnly, chargeType, GetCharges(d));
        }
      }
    }

    static void AddCostSegment(Collection<CostingHelper.ShippingCost> costs, Collection<string> segmentKeys, RatesConfiguration rc, RateTable rt, Location origin, Location destination, decimal cost, string description, decimal billableUnits, decimal baseCharge, decimal rate, bool isFixed, bool isPercentage, bool chargeOnceOnly, WaybillChargeType chargeType, Collection<WaybillCharge> charges)
    {
      //This was commented out for some reason.  It broke Wings' costing...
      if (origin.Equals(destination)) return;

      CostingHelper.ShippingCost sc = new CostingHelper.ShippingCost(origin, destination, cost, description, rc, billableUnits, rate, baseCharge, isFixed, isPercentage, chargeOnceOnly, chargeType, charges);
      string key = GetKey(sc, rt.RequiredFlag);
      if (segmentKeys == null || !segmentKeys.Contains(key))
      {
        if (segmentKeys != null) segmentKeys.Add(key);
        costs.Add(sc);

        foreach (var d in destination.SubLocations)
        {
          DeliveryLocation dl = d.GetExtensionObject<DeliveryLocation>();
          if (dl.DeliveryPointType == DeliveryPointType.None && dl.IsWaypoint) AddCostSegment(costs, segmentKeys, rc, rt, origin, d, cost, description, billableUnits, rate, baseCharge, isFixed, isPercentage, chargeOnceOnly, chargeType, charges);
        }
      }

      if (rt.IsOmnidirectional)
      {
        sc = new CostingHelper.ShippingCost(destination, origin, cost, description, rc, billableUnits, rate, baseCharge, isFixed, isPercentage, chargeOnceOnly, chargeType, charges);
        key = GetKey(sc, rt.RequiredFlag);
        if (segmentKeys == null || !segmentKeys.Contains(key))
        {
          if (segmentKeys != null) segmentKeys.Add(key);
          costs.Add(sc);

          foreach (var d in origin.SubLocations)
          {
            DeliveryLocation dl = d.GetExtensionObject<DeliveryLocation>();
            if (dl.DeliveryPointType == DeliveryPointType.None && dl.IsWaypoint) AddCostSegment(costs, segmentKeys, rc, rt, destination, d, cost, description, billableUnits, baseCharge, rate, isFixed, isPercentage, chargeOnceOnly, chargeType, charges);
          }
        }
      }
    }


    #endregion

    #region Collection<WaybillCharge> GetCharges(...)

    static Collection<WaybillCharge> GetCharges(RateTableLocation d)
    {
      Collection<WaybillCharge> charges = new Collection<WaybillCharge>();
      foreach (var r in d.Cells.Where(r1 => typeof(RateTableFee).IsAssignableFrom(r1.Column.GetType())))
      {
        if (r.Rate >= 0) charges.Add(new WaybillCharge(((RateTableFee)r.Column).ChargeType, r.Rate));
      }
      return charges;
    }

    static Collection<WaybillCharge> GetCharges(DistanceRateTableDistanceCategory d)
    {
      Collection<WaybillCharge> charges = new Collection<WaybillCharge>();
      foreach (var r in d.Cells.Where(r1 => typeof(DistanceRateTableColumnFee).IsAssignableFrom(r1.Column.GetType())))
      {
        if (r.Rate >= 0) charges.Add(new WaybillCharge(((DistanceRateTableColumnFee)r.Column).ChargeType, r.Rate));
      }
      return charges;
    }

    #endregion

    #endregion
  }
}

﻿using Afx.Business;
using Afx.Business.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Business
{
  [PersistantObject(Schema = "FreightManagement", TableName = "vTransferManifestWaybill", IsReadOnly = true)]
  class TransferManifestWaybill_Internal : BusinessObject
  {
    #region Guid WaybillIdentifier

    public const string WaybillIdentifierProperty = "WaybillIdentifier";
    Guid mWaybillIdentifier;
    [PersistantProperty]
    public Guid WaybillIdentifier
    {
      get { return mWaybillIdentifier; }
      set { SetProperty<Guid>(ref mWaybillIdentifier, value); }
    }

    #endregion
  }
}

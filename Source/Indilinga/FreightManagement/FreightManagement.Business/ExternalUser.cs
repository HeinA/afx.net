﻿using AccountManagement.Business;
using Afx.Business;
using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Validation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace FreightManagement.Business
{
  [PersistantObject(Schema = "FreightManagement")]
  [Replicate]
  public partial class ExternalUser : BusinessObject, IRevisable 
  {
    #region Constructors

    public ExternalUser()
    {
    }

    public ExternalUser(bool isNull)
      : base(isNull)
    {
    }

    #endregion

    #region int Revision

    public const string RevisionProperty = "Revision";
    [DataMember(Name = RevisionProperty, EmitDefaultValue = false)]
    int mRevision;
    [PersistantProperty]
    public int Revision
    {
      get { return mRevision; }
      set { SetProperty<int>(ref mRevision, value); }
    }

    #endregion

    #region string Username

    public const string UsernameProperty = "Username";
    [DataMember(Name = UsernameProperty, EmitDefaultValue = false)]
    string mUsername;
    [PersistantProperty]
    public string Username
    {
      get { return mUsername; }
      set { SetProperty<string>(ref mUsername, value); }
    }

    #endregion

    #region string PasswordHash

    public const string PasswordHashProperty = "PasswordHash";
    [DataMember(Name = PasswordHashProperty, EmitDefaultValue = false)]
    string mPasswordHash;
    [PersistantProperty]
    [Mandatory("Password is mandatory.")]
    public string PasswordHash
    {
      get { return mPasswordHash; }
      set { SetProperty<string>(ref mPasswordHash, value); }
    }

    #endregion

    #region string EMail

    public const string EMailProperty = "EMail";
    [DataMember(Name = EMailProperty, EmitDefaultValue = false)]
    string mEMail;
    [PersistantProperty]
    [Mandatory("EMail is mandatory.")]
    public string EMail
    {
      get { return mEMail; }
      set { SetProperty<string>(ref mEMail, value); }
    }

    #endregion

    #region Associative BusinessObjectCollection<AccountReference> Accounts

    public const string AccountsProperty = "Accounts";
    AssociativeObjectCollection<ExternalUserAccount, AccountReference> mAccounts;
    [PersistantCollection(AssociativeType = typeof(ExternalUserAccount))]
    public BusinessObjectCollection<AccountReference> Accounts
    {
      get
      {
        if (mAccounts == null) using (new EventStateSuppressor(this)) { mAccounts = new AssociativeObjectCollection<ExternalUserAccount, AccountReference>(this); }
        return mAccounts;
      }
    }

    #endregion

    #region Associative BusinessObjectCollection<ExternalRole> Roles

    public const string RolesProperty = "Roles";
    AssociativeObjectCollection<ExternalUserRole, ExternalRole> mRoles;
    [PersistantCollection(AssociativeType = typeof(ExternalUserRole))]
    public BusinessObjectCollection<ExternalRole> Roles
    {
      get
      {
        if (mRoles == null) using (new EventStateSuppressor(this)) { mRoles = new AssociativeObjectCollection<ExternalUserRole, ExternalRole>(this); }
        return mRoles;
      }
    }

    #endregion
  }
}

﻿using Afx.Business;
using Afx.Business.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Business
{
  [PersistantObject(Schema = "FreightManagement", OwnerColumn = "Waybill", TableName = "WaybillRoute", IsReadOnly = true)]
  [DataContract(IsReference = true, Namespace = FreightManagement.Business.Namespace.FreightManagement)]
  public class WaybillReferenceRoute : BusinessObject<WaybillReference>
  {
    #region DistributionCenter DistributionCenter

    public const string DistributionCenterProperty = "DistributionCenter";
    [PersistantProperty(IsCached = true)]
    public DistributionCenter DistributionCenter
    {
      get { return GetCachedObject<DistributionCenter>(); }
      set { SetCachedObject<DistributionCenter>(value); }
    }

    #endregion

    #region DateTime? ArrivalTimestamp

    public const string ArrivalTimestampProperty = "ArrivalTimestamp";
    [DataMember(Name = ArrivalTimestampProperty, EmitDefaultValue = false)]
    DateTime? mArrivalTimestamp;
    [PersistantProperty]
    public DateTime? ArrivalTimestamp
    {
      get { return mArrivalTimestamp; }
      set { SetProperty<DateTime?>(ref mArrivalTimestamp, value); }
    }

    #endregion

    #region DateTime? DepartureTimestamp

    public const string DepartureTimestampProperty = "DepartureTimestamp";
    [DataMember(Name = DepartureTimestampProperty, EmitDefaultValue = false)]
    DateTime? mDepartureTimestamp;
    [PersistantProperty]
    public DateTime? DepartureTimestamp
    {
      get { return mDepartureTimestamp; }
      set { SetProperty<DateTime?>(ref mDepartureTimestamp, value); }
    }

    #endregion

    #region Route Route

    public const string RouteProperty = "Route";
    [PersistantProperty(IsCached = true)]
    public Route Route
    {
      get { return GetCachedObject<Route>(); }
      set { SetCachedObject<Route>(value); }
    }

    #endregion
  }
}

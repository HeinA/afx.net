﻿using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Documents;
using Afx.Business.Security;
using Afx.Business.ServiceModel;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Business.DocumentStateEventManagers
{
  [Export(typeof(IDocumentStateEventManager))]
  public class WaybillOnFloorEventManager : DocumentStateEventManager<Waybill>
  {
    public override string TargetStateIdentifier
    {
      get { return Waybill.States.OnFloor; }
    }

    public override void Process(Waybill doc, DocumentTypeState originalState, DocumentStateEventScope scope)
    {
      DistributionCenter dc = null;

      #region Get the last DC if a DeliveryManifest is returning the waybill to the floor

      ManifestStateEventScope ms = scope as ManifestStateEventScope;
      if (ms != null && ms.ReturnToFloor)
      {
        dc = doc.LastDistributionCenter;
      }

      #endregion

      #region Get the next or specified DC where the waybill should be placed on floor

      if (dc == null)
      {
        if (scope.TargetOU == null)
        {
          dc = doc.CurrentDistributionCenter;
          if (dc == null) dc = doc.NextDistributionCenter;
        }
        else
        {
          dc = Cache.Instance.GetObjects<DistributionCenter>(dc1 => dc1.OrganizationalUnit.Equals(scope.TargetOU)).FirstOrDefault();
        }
      }

      #endregion

      #region Get The route for the DC

      WaybillRoute wrCurrent = doc.Routes.Where(wr1 => !wr1.IsDeleted && wr1.DistributionCenter.Equals(dc)).FirstOrDefault();
      if (dc == null || wrCurrent == null) throw new RoutingException(string.Format("Invalid Distribution Center for Waybill {0}", doc.DocumentNumber));

      #endregion

      #region Set the Arrival/Departure times for appropriate routes

      if (!dc.Equals(doc.CurrentDistributionCenter))
      {
        //Traqce received message
        DateTime dt = DateTime.Now;
        WaybillTrace wt = new WaybillTrace();
        wt.Timestamp = dt;
        wt.Text = string.Format("Received at facility {0}", dc.Name);
        doc.TraceLog.Add(wt);

        //Mark route as current
        wrCurrent.ArrivalTimestamp = dt;
        wrCurrent.DepartureTimestamp = null;
        int index = doc.Routes.IndexOf(wrCurrent);

        //Ensure previous routes are mared as done
        foreach (var wr in doc.Routes.Where(wr1 => !wr1.IsDeleted && doc.Routes.IndexOf(wr1) < index))
        {
          if (wr.ArrivalTimestamp == null) wr.ArrivalTimestamp = dt;
          if (wr.DepartureTimestamp == null) wr.DepartureTimestamp = dt;
        }

        //Ensure future routes are marked as not yet received
        foreach (var wr in doc.Routes.Where(wr1 => !wr1.IsDeleted && doc.Routes.IndexOf(wr1) > index))
        {
          wr.ArrivalTimestamp = null;
          wr.DepartureTimestamp = null;
        }
      }

      #endregion
      
      #region Finalize Transfer Manifests

      //if (originalState.Identifier == Waybill.States.InTransit || originalState.Identifier == Waybill.States.OnDelivery || originalState.Identifier == Waybill.States.Delivered)
      {
        DataFilter<TransferManifestWaybill_Internal> df = new DataFilter<TransferManifestWaybill_Internal>(TransferManifestWaybill_Internal.WaybillIdentifierProperty, FilterType.Equals, doc.GlobalIdentifier);
        Collection<IDataFilter> filters = new Collection<IDataFilter>();
        filters.Add(df);
        ObjectRepository<TransferManifestWaybill_Internal> orMW = PersistanceManager.GetRepository<TransferManifestWaybill_Internal>();
        BasicCollection<TransferManifestWaybill_Internal> col = orMW.GetInstances(filters);

        ObjectRepository<TransferManifest> orM = PersistanceManager.GetRepository<TransferManifest>();
        foreach (var mw in col)
        {
          TransferManifest m = orM.GetInstance(mw.GlobalIdentifier);
          if (m.State.DocumentTypeState.Identifier == TransferManifest.States.Verified)
          {
            int i = m.Waybills.Where(w => !w.GlobalIdentifier.Equals(doc.GlobalIdentifier) && !w.HasBeenReceivedByDistributionCenter(m.DestinationDC)).Count();

            if (i == 0) // Transfer Manifest is complete
            {
              m = TransferIfRequired<TransferManifest>(m);
              DocumentHelper.SetState(m, TransferManifest.States.Closed);
            }
          }
        }
      }

      #endregion
    }
  }
}

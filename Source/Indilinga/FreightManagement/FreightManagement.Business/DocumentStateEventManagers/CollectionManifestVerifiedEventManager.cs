﻿using Afx.Business.Data;
using Afx.Business.Documents;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Business.DocumentStateEventManagers
{
  [Export(typeof(IDocumentStateEventManager))]
  public class CollectionManifestVerifiedEventManager : DocumentStateEventManager<CollectionManifest>
  {
    public override string TargetStateIdentifier
    {
      get { return CollectionManifest.States.Verified; }
    }

    public override void Process(CollectionManifest doc, DocumentTypeState originalState, DocumentStateEventScope scope)
    {
      #region Set Quoted Waybills to Pending Collection

      ObjectRepository<Waybill> or = PersistanceManager.GetRepository<Waybill>();
      foreach (WaybillReference wr in doc.Waybills.Where(wr1 => wr1.WaybillState.Identifier == Waybill.States.Quoted))
      {
        ManifestStateEventScope ms = new ManifestStateEventScope(scope);
        Waybill w = or.GetInstance(wr.GlobalIdentifier);
        w = TransferIfRequired<Waybill>(w);
        DocumentHelper.SetState(w, Waybill.States.PendingCollection, ms);
      }

      #endregion
    }
  }
}

﻿using Afx.Business.Data;
using Afx.Business.Documents;
using Afx.Business.Security;
using Afx.Business.Service;
using FreightManagement.Business.Service;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Business.DocumentStateEventManagers
{
  [Export(typeof(IDocumentStateEventManager))]
  public class WaybillReceiptVerifiedEventManager : DocumentStateEventManager<WaybillReceipt>
  {
    public override string TargetStateIdentifier
    {
      get { return WaybillReceipt.States.Verified; }
    }

    public override void Process(WaybillReceipt doc, DocumentTypeState originalState, DocumentStateEventScope scope)
    {
      ObjectRepository<Waybill> or = PersistanceManager.GetRepository<Waybill>();

      foreach (var wr in doc.Waybills)
      {
        if (wr.WaybillState.Identifier != Waybill.States.OnFloor  || wr.CurrentDistributionCenter == null || !wr.CurrentDistributionCenter.OrganizationalUnit.Equals(doc.OrganizationalUnit))
        {
          Waybill w = or.GetInstance(wr.GlobalIdentifier);
          w = TransferIfRequired<Waybill>(w);
          
          var wrw = doc.GetAssociativeObject<WaybillReceiptWaybill>(wr);
          foreach (var wf in wrw.WaybillFlags)
          {
            if (!w.WaybillFlags.Contains(wf)) w.WaybillFlags.Add(wf);
          }
          foreach (var wf in w.WaybillFlags.ToArray())
          {
            if (!wrw.WaybillFlags.Contains(wf)) w.WaybillFlags.Remove(wf);
          }
          w.SetBay(wrw.Bay, wrw.Comment);

          DocumentHelper.SetState(w, Waybill.States.OnFloor, new DocumentStateEventScope(doc.OrganizationalUnit));
        }
      }
    }
  }
}

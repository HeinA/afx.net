﻿using Afx.Business.Data;
using Afx.Business.Documents;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Business.DocumentStateEventManagers
{
  [Export(typeof(IDocumentStateEventManager))]
  public class WaybillInTransitEventManager : DocumentStateEventManager<Waybill>
  {
    public override string TargetStateIdentifier
    {
      get { return Waybill.States.InTransit; }
    }

    public override void Process(Waybill doc, DocumentTypeState originalState, DocumentStateEventScope scope)
    {
      //Only a DeliveryManifest can set a Waybill to InTransit
      ManifestStateEventScope ms = scope as ManifestStateEventScope;
      if (ms == null && originalState.Identifier == Business.Waybill.States.InTransit) return;
      if (ms == null && originalState.Identifier != Business.Waybill.States.InTransit) throw new RoutingException("Only a DeliveryManifest can set a Waybill InTransit");

      //if (doc.IsPointToPoint) return;
      if (doc.IsPointToPoint)
      {
        //*****************************************************************************************************************************
        //  Transport Manifest Tracing
        //*****************************************************************************************************************************
        #region Trace departure

        doc.TraceLog.Add(new WaybillTrace(string.Format("Departed {0}({1}) to {2}({3}) at {4}", doc.Consigner, doc.CollectionCity.Name, doc.Consignee, doc.DeliveryCity.Name, DateTime.Now)));

        #endregion
      }
      else
      {
        //*****************************************************************************************************************************
        //  Standard Delivery Manifest Tracing
        //*****************************************************************************************************************************
        #region Get The Waybill's current DC

        DistributionCenter dc = null;
        if (scope.TargetOU == null)
        {
          dc = doc.CurrentDistributionCenter;
        }
        else
        {
          dc = Cache.Instance.GetObjects<DistributionCenter>(dc1 => dc1.OrganizationalUnit.Equals(scope.TargetOU)).FirstOrDefault();
        }

        if (dc == null) throw new RoutingException(string.Format("Invalid Distribution Center for Waybill {0}", doc.DocumentNumber));

        #endregion

        #region Get the Forwarding Route for the DC

        WaybillRoute wrCurrent = doc.Routes.Where(wr1 => !wr1.IsDeleted && wr1.DistributionCenter.Equals(dc)).FirstOrDefault();
        if (wrCurrent == null) throw new RoutingException(string.Format("Invalid Route for Waybill {0}", doc.DocumentNumber));

        #endregion

        #region Trace departure

        if (wrCurrent.DepartureTimestamp == null)
        {
          wrCurrent.DepartureTimestamp = DateTime.Now;
          doc.TraceLog.Add(new WaybillTrace(string.Format("Departed facility {0}", wrCurrent.DistributionCenter.Name)));
        }

        #endregion

      }
    }
  }
}

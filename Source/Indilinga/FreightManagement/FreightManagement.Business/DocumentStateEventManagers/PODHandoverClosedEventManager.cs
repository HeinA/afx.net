﻿using Afx.Business;
using Afx.Business.Data;
using Afx.Business.Documents;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Business.DocumentStateEventManagers
{
  [Export(typeof(IDocumentStateEventManager))]
  public class PODHandoverClosedEventManager : DocumentStateEventManager<PODHandover>
  {
    public override string TargetStateIdentifier
    {
      get { return PODHandover.States.Closed; }
    }

    public override void Process(PODHandover doc, DocumentTypeState originalState, DocumentStateEventScope scope)
    {
      if (string.IsNullOrWhiteSpace(doc.Signature))
      {
        throw new MessageException("Signature is mandatory for the POD Handover to be marked as Closed.");
      }
    }
  }
}

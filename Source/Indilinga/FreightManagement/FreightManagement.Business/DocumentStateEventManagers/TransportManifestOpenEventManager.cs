﻿using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Documents;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Business.DocumentStateEventManagers
{
  [Export(typeof(IDocumentStateEventManager))]
  public class TransportManifestOpenEventManager : DocumentStateEventManager<TransportManifest>
  {
    public override string TargetStateIdentifier
    {
      get { return TransportManifest.States.Open; }
    }

    public override void Process(TransportManifest doc, DocumentTypeState originalState, DocumentStateEventScope scope)
    {
      #region Trace Loading / Unloading

      BasicCollection<WaybillTrace_Internal> traces = new BasicCollection<WaybillTrace_Internal>();

      foreach (TransportManifestWaybill mw in doc.GetAssociativeObjects<TransportManifestWaybill>().Where(w => !w.IsDeleted && w.IsNew))
      {
        traces.Add(new WaybillTrace_Internal(mw.Reference, string.Format("Loaded on to Transport Manifest {0}", doc.DocumentNumber)));
      }

      foreach (TransportManifestWaybill mw in doc.GetAssociativeObjects<TransportManifestWaybill>().Where(w => w.IsDeleted && !w.IsNew))
      {
        traces.Add(new WaybillTrace_Internal(mw.Reference, string.Format("Unloaded from Transport Manifest {0}", doc.DocumentNumber)));
      }

      if (traces.Count > 0)
      {
        ObjectRepository<WaybillTrace_Internal> or1 = PersistanceManager.GetRepository<WaybillTrace_Internal>();
        or1.Persist(traces);
      }

      #endregion
    }
  }
}

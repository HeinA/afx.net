﻿using Afx.Business;
using Afx.Business.Data;
using Afx.Business.Documents;
using Afx.Business.Security;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Business.DocumentStateEventManagers
{
  [Export(typeof(IDocumentStateEventManager))]
  public class TripsheetInTransitEventManager : DocumentStateEventManager<Tripsheet>
  {
    public override string TargetStateIdentifier
    {
      get { return Tripsheet.States.InTransit; }
    }

    public override void Process(Tripsheet doc, DocumentTypeState originalState, DocumentStateEventScope scope)
    {
      #region Get Acive / Next Route & set Departure Time

      Route r = doc.CurrentRoute;
      TripsheetRoute tr = null;
      if (r == null)
      {
        r = doc.NextRoute;
        if (r != null)
        {
          // Set Departure time on next route.
          tr = doc.GetAssociativeObject<TripsheetRoute>(r);
          tr.ActualTimeOfDeparture = DateTime.Now;
        }
      }
      else
      {
        tr = doc.GetAssociativeObject<TripsheetRoute>(r);
      }

      if (r == null) throw new MessageException("Tripsheet has no next route.");

      #endregion


      ObjectRepository<Manifest> or = PersistanceManager.GetRepository<Manifest>();

      #region Set Next Route's Manifests to InTransit

      foreach (TripsheetRouteManifest rtm in tr.GetAssociativeObjects<TripsheetRouteManifest>().Where(rtm1 => !rtm1.IsDeleted && rtm1.Reference.ManifestState.Identifier == Manifest.States.Created))
      {
        //Set Manifest to InTansit
        Manifest m = or.GetInstance(rtm.Reference.GlobalIdentifier);
        m = TransferIfRequired<Manifest>(m);
        DocumentHelper.SetState(m, Manifest.States.InTransit);
      }

      #endregion


      #region Reset Removed Routes Manifests States to Created

      foreach (TripsheetRoute tr1 in doc.GetAssociativeObjects<TripsheetRoute>().Where(tr2 => tr2.IsDeleted && !tr2.IsNew))
      {
        //Route has been removed - reset all manifest states
        foreach (var mr in tr1.Manifests.Where(mr1 => mr1.ManifestState.Identifier != Manifest.States.Created))
        {
          Manifest m = or.GetInstance(mr.GlobalIdentifier);
          m = TransferIfRequired<Manifest>(m);
          DocumentHelper.SetState(m, Manifest.States.Created);
        }
      }

      #endregion

      #region Reset Removed Manifests States to Created

      foreach (TripsheetRoute tr1 in doc.GetAssociativeObjects<TripsheetRoute>().Where(tr2 => !tr2.IsDeleted))
      {
        foreach (TripsheetRouteManifest rtm in tr1.GetAssociativeObjects<TripsheetRouteManifest>())
        {
          if (rtm.IsDeleted && !rtm.IsNew && rtm.Reference.ManifestState.Identifier != Manifest.States.Created)
          {
            //Manifest has been removed - reset state
            Manifest m = or.GetInstance(rtm.Reference.GlobalIdentifier);
            m = TransferIfRequired<Manifest>(m);
            DocumentHelper.SetState(m, Manifest.States.Created);
          }
        }
      }

      #endregion
    }
  }
}

﻿using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Documents;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Business.DocumentStateEventManagers
{
  [Export(typeof(IDocumentStateEventManager))]
  public class DeliveryManifestOpenEventManager : DocumentStateEventManager<DeliveryManifest>
  {
    public override string TargetStateIdentifier
    {
      get { return DeliveryManifest.States.Open; }
    }

    public override void Process(DeliveryManifest doc, DocumentTypeState originalState, DocumentStateEventScope scope)
    {
      #region Trace Loading / Unloading

      BasicCollection<WaybillTrace_Internal> traces = new BasicCollection<WaybillTrace_Internal>();

      foreach (DeliveryManifestWaybill mw in doc.GetAssociativeObjects<DeliveryManifestWaybill>().Where(w => !w.IsDeleted && w.IsNew))
      {
        traces.Add(new WaybillTrace_Internal(mw.Reference, string.Format("Loaded on to Delivery Manifest {0}", doc.DocumentNumber)));
      }

      foreach (DeliveryManifestWaybill mw in doc.GetAssociativeObjects<DeliveryManifestWaybill>().Where(w => w.IsDeleted && !w.IsNew))
      {
        traces.Add(new WaybillTrace_Internal(mw.Reference, string.Format("Unloaded from Delivery Manifest {0}", doc.DocumentNumber)));
      }

      if (traces.Count > 0)
      {
        ObjectRepository<WaybillTrace_Internal> or1 = PersistanceManager.GetRepository<WaybillTrace_Internal>();
        or1.Persist(traces);
      }

      #endregion

      //#region Return InTransit Waybills to floor

      //ObjectRepository<Waybill> or = PersistanceManager.GetRepository<Waybill>();
      //foreach (WaybillReference wr in doc.Waybills.Where(wr1 => wr1.WaybillState.Identifier == Waybill.States.InTransit))
      //{
      //  ManifestStateEventScope ms = new ManifestStateEventScope(scope, doc);
      //  ms.ReturnToFloor = true;
      //  Waybill w = or.GetInstance(wr.GlobalIdentifier);
      //  DocumentHelper.SetState(w, Waybill.States.OnFloor, ms);
      //}

      //#endregion
    }
  }
}

using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Documents;
using Afx.Business.Security;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Business.DocumentStateEventManagers
{
  [Export(typeof(IDocumentStateEventManager))]
  public class TransferManifestVerifiedEventManager : DocumentStateEventManager<TransferManifest>
  {
    public override string TargetStateIdentifier
    {
      get { return TransferManifest.States.Verified; }
    }

    public override void Process(TransferManifest doc, DocumentTypeState originalState, DocumentStateEventScope scope)
    {
      if (doc.IsNew)
      {
        #region Trace Loading / Unloading

        BasicCollection<WaybillTrace_Internal> traces = new BasicCollection<WaybillTrace_Internal>();

        foreach (TransferManifestWaybill mw in doc.GetAssociativeObjects<TransferManifestWaybill>().Where(w => !w.IsDeleted && w.IsNew))
        {
          traces.Add(new WaybillTrace_Internal(mw.Reference, string.Format("Loaded on to Transfer Manifest {0}", doc.DocumentNumber)));
        }

        if (traces.Count > 0)
        {
          ObjectRepository<WaybillTrace_Internal> or1 = PersistanceManager.GetRepository<WaybillTrace_Internal>();
          or1.Persist(traces);
        }

        #endregion
      }

      #region Set OnFloor/Created Waybills to InTransit

      ObjectRepository<Waybill> or = PersistanceManager.GetRepository<Waybill>();
      foreach (WaybillReference wr in doc.Waybills.Where(wr1 => wr1.WaybillState.Identifier == Waybill.States.OnFloor))
      {
        ManifestStateEventScope ms = new ManifestStateEventScope(scope);
        Waybill w = or.GetInstance(wr.GlobalIdentifier);
        w = TransferIfRequired<Waybill>(w);
        //DocumentHelper.SetState(w, Waybill.States.InTransit, ms);
        DocumentHelper.SetState(w, Waybill.States.InterDepotTransfer, ms);
      }

      #endregion
    }
  }
}

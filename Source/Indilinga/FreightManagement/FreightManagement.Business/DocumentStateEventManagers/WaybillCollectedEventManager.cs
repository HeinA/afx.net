﻿using Afx.Business.Data;
using Afx.Business.Documents;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Business.DocumentStateEventManagers
{
  [Export(typeof(IDocumentStateEventManager))]
  public class WaybillCollectedEventManager : DocumentStateEventManager<Waybill>
  {
    public override string TargetStateIdentifier
    {
      get { return Waybill.States.Collected; }
    }

    public override void Process(Waybill doc, DocumentTypeState originalState, DocumentStateEventScope scope)
    {
      #region Finalize Collection Manifests

      if (originalState.Identifier == Waybill.States.PendingCollection || originalState.Identifier == Waybill.States.Collected)
      {
        using (var cmd = PersistanceManager.GetCommand())
        {
          cmd.CommandText = @"
select	D.id as CollectionManifest
,		WD.id as Waybill
,		WDS.GlobalIdentifier as [WaybillState]		
from Afx.Document D
inner join Afx.DocumentState DS on D.[State]=DS.id
inner join Afx.DocumentTypeState DTS on DS.DocumentTypeState=DTS.id
inner join FreightManagement.CollectionManifestWaybill CMW on D.id=CMW.CollectionManifest
inner join Afx.Document WD on WD.id=CMW.Waybill
inner join Afx.DocumentState WDS on WD.[State]=WDS.id
inner join Afx.DocumentTypeState WDTS on WDTS.id=WDS.DocumentTypeState
where DTS.GlobalIdentifier='{e59feb72-e9de-4459-ac51-6b560282bf2d}'";

          DataSet ds = DataHelper.ExecuteDataSet(cmd);
          foreach (var manifest in ds.Tables[0].Rows.Cast<DataRow>().Select(dr => (int)dr["CollectionManifest"]).Distinct())
          {
            bool bComplete = true;
            foreach(var dr in ds.Tables[0].Rows.Cast<DataRow>().Where(dr1 => dr1["CollectionManifest"].Equals(manifest) && !dr1["CollectionManifest"].Equals(doc.Id)))
            {
              if (dr["WaybillState"].Equals(Waybill.States.PendingCollection)) bComplete = false;
            }

            if (bComplete)
            {
              ObjectRepository<CollectionManifest> orM = PersistanceManager.GetRepository<CollectionManifest>();
              var m = orM.GetInstance(manifest);
              DocumentHelper.SetState(m, CollectionManifest.States.Closed);
            }
          }
        }
      }

      #endregion

      #region Trace Waybill Collected

      if (originalState.Identifier == Waybill.States.PendingCollection)
      {
        doc.TraceLog.Add(new WaybillTrace("Marked as Collected."));
      }

      #endregion      
    }
  }
}

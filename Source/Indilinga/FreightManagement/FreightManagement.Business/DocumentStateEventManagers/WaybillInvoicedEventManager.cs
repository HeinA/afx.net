﻿using Afx.Business;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Documents;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Business.DocumentStateEventManagers
{
  [Export(typeof(IDocumentStateEventManager))]
  public class WaybillInvoicedEventManager : DocumentStateEventManager<Waybill>
  {
    public override string TargetStateIdentifier
    {
      get { return Waybill.States.Invoiced; }
    }

    public override void Process(Waybill doc, DocumentTypeState originalState, DocumentStateEventScope scope)
    {
      #region Validate State

      if (doc.PODDate == null || doc.InvoiceDate == null || string.IsNullOrWhiteSpace(doc.PODSignature) || string.IsNullOrWhiteSpace(doc.InvoiceNumber))
      {
        throw new MessageException("POD Date, Signature, Invoice Number and Invoice Date is mandatory for the Waybill to be marked as delivered.");
      }

      #endregion

      #region Trace Waybill POD

      if (originalState.Identifier == Waybill.States.InTransit || originalState.Identifier == Waybill.States.OnDelivery)
      {
        doc.TraceLog.Add(new WaybillTrace("Marked as Delivered."));
        doc.TraceLog.Add(new WaybillTrace(string.Format("Delivery received by {0} on {1:D}", doc.PODSignature, doc.PODDate)));
      }

      if (originalState.Identifier == Waybill.States.Delivered)
      {
        doc.TraceLog.Add(new WaybillTrace(string.Format("Delivery received by {0} on {1:D}", doc.PODSignature, doc.PODDate)));
      }

      doc.TraceLog.Add(new WaybillTrace(string.Format("Invoiced on invoice {0} on {1:D}", doc.InvoiceNumber, doc.InvoiceDate)));

      #endregion    

      #region Complete all routes

      DateTime dt = DateTime.Now;
      foreach (var r in doc.Routes)
      {
        if (r.ArrivalTimestamp == null) r.ArrivalTimestamp = dt;
        if (r.DepartureTimestamp == null) r.DepartureTimestamp = dt;
      }

      #endregion

      //#region Finalize Transfer Manifests

      //if (originalState.Identifier == Waybill.States.InTransit || originalState.Identifier == Waybill.States.OnDelivery || originalState.Identifier == Waybill.States.Delivered)
      //{
      //  DataFilter<TransferManifestWaybill_Internal> df = new DataFilter<TransferManifestWaybill_Internal>(TransferManifestWaybill_Internal.WaybillIdentifierProperty, FilterType.Equals, doc.GlobalIdentifier);
      //  Collection<IDataFilter> filters = new Collection<IDataFilter>();
      //  filters.Add(df);
      //  ObjectRepository<TransferManifestWaybill_Internal> orMW = PersistanceManager.GetRepository<TransferManifestWaybill_Internal>();
      //  BasicCollection<TransferManifestWaybill_Internal> col = orMW.GetInstances(filters);

      //  ObjectRepository<TransferManifest> orM = PersistanceManager.GetRepository<TransferManifest>();
      //  foreach (var mw in col)
      //  {
      //    TransferManifest m = orM.GetInstance(mw.GlobalIdentifier);
      //    //Get number of waybill on this manifest thas has not been POD'ed / Readied for invoicing  or invoiced
      //    int i = m.Waybills.Where(w => !w.GlobalIdentifier.Equals(doc.GlobalIdentifier) && w.HasBeenReceivedByDistributionCenter(m.DestinationDC)).Count();

      //    if (i == 0) // Transfer Manifest is complete
      //    {
      //      m = TransferIfRequired<TransferManifest>(m);
      //      DocumentHelper.SetState(m, TransferManifest.States.Closed);
      //    }
      //  }
      //}

      //#endregion

      //#region Finalize Delivery Manifests

      //if (originalState.Identifier == Waybill.States.InTransit || originalState.Identifier == Waybill.States.OnDelivery || originalState.Identifier == Waybill.States.Delivered)
      //{
      //  DataFilter<DeliveryManifestWaybill_Internal> df = new DataFilter<DeliveryManifestWaybill_Internal>(DeliveryManifestWaybill_Internal.WaybillIdentifierProperty, FilterType.Equals, doc.GlobalIdentifier);
      //  Collection<IDataFilter> filters = new Collection<IDataFilter>();
      //  filters.Add(df);
      //  ObjectRepository<DeliveryManifestWaybill_Internal> orMW = PersistanceManager.GetRepository<DeliveryManifestWaybill_Internal>();
      //  BasicCollection<DeliveryManifestWaybill_Internal> col = orMW.GetInstances(filters);

      //  ObjectRepository<DeliveryManifest> orM = PersistanceManager.GetRepository<DeliveryManifest>();
      //  foreach (var mw in col)
      //  {
      //    DeliveryManifest m = orM.GetInstance(mw.GlobalIdentifier);
      //    //Get number of waybill on this manifest thas has not been POD'ed / Readied for invoicing  or invoiced
      //    int i = m.Waybills.Where(w => !w.GlobalIdentifier.Equals(doc.GlobalIdentifier)
      //      && w.WaybillState.Identifier != Waybill.States.Delivered
      //      && w.WaybillState.Identifier != Waybill.States.PODCaptured
      //      && w.WaybillState.Identifier != Waybill.States.PODTransfer
      //      && w.WaybillState.Identifier != Waybill.States.PODReceived
      //      && w.WaybillState.Identifier != Waybill.States.PendingInvoice
      //      && w.WaybillState.Identifier != Waybill.States.Invoiced).Count();

      //    if (i == 0) // DeliveryManifest is complete
      //    {
      //      m = TransferIfRequired<DeliveryManifest>(m);
      //      DocumentHelper.SetState(m, DeliveryManifest.States.Closed);
      //    }
      //  }
      //}

      //#endregion
    }
  }
}

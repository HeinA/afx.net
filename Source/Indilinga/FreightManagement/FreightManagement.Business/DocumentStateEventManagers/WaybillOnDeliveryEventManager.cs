﻿using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Documents;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Business.DocumentStateEventManagers
{
  [Export(typeof(IDocumentStateEventManager))]
  public class WaybillOnDeliveryEventManager : DocumentStateEventManager<Waybill>
  {
    public override string TargetStateIdentifier
    {
      get { return Waybill.States.OnDelivery; }
    }

    public override void Process(Waybill doc, DocumentTypeState originalState, DocumentStateEventScope scope)
    {
      #region Finalize Transfer Manifests

      //if (originalState.Identifier == Waybill.States.InTransit || originalState.Identifier == Waybill.States.OnDelivery || originalState.Identifier == Waybill.States.Delivered)
      {
        DataFilter<TransferManifestWaybill_Internal> df = new DataFilter<TransferManifestWaybill_Internal>(TransferManifestWaybill_Internal.WaybillIdentifierProperty, FilterType.Equals, doc.GlobalIdentifier);
        Collection<IDataFilter> filters = new Collection<IDataFilter>();
        filters.Add(df);
        ObjectRepository<TransferManifestWaybill_Internal> orMW = PersistanceManager.GetRepository<TransferManifestWaybill_Internal>();
        BasicCollection<TransferManifestWaybill_Internal> col = orMW.GetInstances(filters);

        ObjectRepository<TransferManifest> orM = PersistanceManager.GetRepository<TransferManifest>();
        foreach (var mw in col)
        {
          TransferManifest m = orM.GetInstance(mw.GlobalIdentifier);
          if (m.State.DocumentTypeState.Identifier == TransferManifest.States.Verified)
          {
            int i = m.Waybills.Where(w => !w.GlobalIdentifier.Equals(doc.GlobalIdentifier) && !w.HasBeenReceivedByDistributionCenter(m.DestinationDC)).Count();

            if (i == 0) // Transfer Manifest is complete
            {
              m = TransferIfRequired<TransferManifest>(m);
              DocumentHelper.SetState(m, TransferManifest.States.Closed);
            }
          }
        }
      }

      #endregion

      ManifestStateEventScope ms = scope as ManifestStateEventScope;
      if (ms == null && originalState.Identifier == Business.Waybill.States.OnDelivery) return;
      if (ms == null && originalState.Identifier != Business.Waybill.States.OnDelivery) throw new RoutingException("Only a Delivery Manifest can set a Waybill to OnDelivery");

      #region Get The Waybill's current DC

      DistributionCenter dc = null;
      if (scope.TargetOU == null)
      {
        dc = doc.CurrentDistributionCenter;
      }
      else
      {
        dc = Cache.Instance.GetObjects<DistributionCenter>(dc1 => dc1.OrganizationalUnit.Equals(scope.TargetOU)).FirstOrDefault();
      }

      if (dc == null) throw new RoutingException(string.Format("Invalid Distribution Center for Waybill {0}", doc.DocumentNumber));

      #endregion

      #region Get the Forwarding Route for the DC

      WaybillRoute wrCurrent = doc.Routes.Where(wr1 => !wr1.IsDeleted && wr1.DistributionCenter.Equals(dc)).FirstOrDefault();
      if (wrCurrent == null) throw new RoutingException(string.Format("Invalid Route for Waybill {0}", doc.DocumentNumber));

      #endregion

      #region Trace departure

      if (wrCurrent.DepartureTimestamp == null)
      {
        wrCurrent.DepartureTimestamp = DateTime.Now;
        doc.TraceLog.Add(new WaybillTrace(string.Format("Departed facility {0}", wrCurrent.DistributionCenter.Name)));
      }

      #endregion
    }
  }
}

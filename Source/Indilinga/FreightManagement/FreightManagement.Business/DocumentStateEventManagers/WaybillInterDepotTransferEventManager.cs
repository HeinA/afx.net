﻿using Afx.Business.Data;
using Afx.Business.Documents;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Business.DocumentStateEventManagers
{
  [Export(typeof(IDocumentStateEventManager))]
  public class WaybillInterDepotTransferEventManager : DocumentStateEventManager<Waybill>
  {
    public override string TargetStateIdentifier
    {
      get { return Waybill.States.InterDepotTransfer; }
    }

    public override void Process(Waybill doc, DocumentTypeState originalState, DocumentStateEventScope scope)
    {
      ManifestStateEventScope ms = scope as ManifestStateEventScope;
      if (ms == null && originalState.Identifier == Business.Waybill.States.InterDepotTransfer) return;
      if (ms == null && originalState.Identifier != Business.Waybill.States.InterDepotTransfer) throw new RoutingException("Only a Transfer Manifest can set a Waybill to InterDepotTransfer");

      #region Get The Waybill's current DC

      DistributionCenter dc = null;
      if (scope.TargetOU == null)
      {
        dc = doc.CurrentDistributionCenter;
      }
      else
      {
        dc = Cache.Instance.GetObjects<DistributionCenter>(dc1 => dc1.OrganizationalUnit.Equals(scope.TargetOU)).FirstOrDefault();
      }

      if (dc == null) throw new RoutingException(string.Format("Invalid Distribution Center for Waybill {0}", doc.DocumentNumber));

      #endregion

      #region Get the Forwarding Route for the DC

      WaybillRoute wrCurrent = doc.Routes.Where(wr1 => !wr1.IsDeleted && wr1.DistributionCenter.Equals(dc)).FirstOrDefault();
      if (wrCurrent == null) throw new RoutingException(string.Format("Invalid Route for Waybill {0}", doc.DocumentNumber));

      #endregion

      #region Trace departure

      if (wrCurrent.DepartureTimestamp == null)
      {
        wrCurrent.DepartureTimestamp = DateTime.Now;
        doc.TraceLog.Add(new WaybillTrace(string.Format("Departed facility {0}", wrCurrent.DistributionCenter.Name)));
      }

      #endregion
    }
  }
}

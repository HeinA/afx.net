﻿using Afx.Business.Documents;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Business.DocumentStateEventManagers
{
  public class ManifestStateEventScope : DocumentStateEventScope
  {
    public ManifestStateEventScope(DocumentStateEventScope scope) //, DeliveryManifest manifest)
    {
      TargetOU = scope.TargetOU;
      //Manifest = manifest;
    }

    //#region DeliveryManifest DeliveryManifest

    //public const string ManifestProperty = "DeliveryManifest";
    //DeliveryManifest mManifest;
    //public DeliveryManifest Manifest
    //{
    //  get { return mManifest; }
    //  protected set { mManifest = value; }
    //}

    //#endregion

    #region bool ReturnToFloor

    public const string ReturnToFloorProperty = "ReturnToFloor";
    bool mReturnToFloor;
    public bool ReturnToFloor
    {
      get { return mReturnToFloor; }
      set { mReturnToFloor = value; }
    }

    #endregion
  }
}

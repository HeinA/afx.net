﻿using Afx.Business.Data;
using Afx.Business.Documents;
using Afx.Business.Security;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Business.DocumentStateEventManagers
{
  [Export(typeof(IDocumentStateEventManager))]
  public class TripsheetArrivedEventManager : DocumentStateEventManager<Tripsheet>
  {
    public override string TargetStateIdentifier
    {
      get { return Tripsheet.States.Arrived; }
    }

    public override void Process(Tripsheet doc, DocumentTypeState originalState, DocumentStateEventScope scope)
    {
      #region Set Current Route Arrival Date

      Route r = doc.CurrentRoute;
      TripsheetRoute tr = null;
      if (r == null)
      {
        r = doc.LastRoute;
      }

      tr = doc.GetAssociativeObject<TripsheetRoute>(r);
      if (tr == null) throw new RoutingException("Tripsheet does not have an applicable route.");

      if (tr.ActualTimeOfArrival == null) tr.ActualTimeOfArrival = DateTime.Now;
      
      #endregion

      ObjectRepository<Manifest> or = PersistanceManager.GetRepository<Manifest>();

      #region Set Current Route's Manifests to Delivered

      foreach (TripsheetRouteManifest rtm in tr.GetAssociativeObjects<TripsheetRouteManifest>().Where(rtm1 => !rtm1.IsDeleted && rtm1.Reference.ManifestState.Identifier == Manifest.States.InTransit))
      {
        //Set Manifest to Delivered
        Manifest m = or.GetInstance(rtm.Reference.GlobalIdentifier);
        m = TransferIfRequired<Manifest>(m);
        DocumentHelper.SetState(m, Manifest.States.Delivered);
      }

 #endregion
    }
  }
}

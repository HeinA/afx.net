﻿using Afx.Business;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Business
{
  [Export(typeof(IObjectFlags))]
  public class WaybillLevyTypeFlags : IObjectFlags
  {
    public const string FuelLevy = "FreightManagement.FuelLevy";

    public Type ObjectType
    {
      get { return typeof(WaybillLevyType); }
    }

    public IEnumerable<string> Flags
    {
      get { return new string[] { FuelLevy }; }
    }
  }
}

﻿using Afx.Business;
using Afx.Business.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Business
{
  [PersistantObject(Schema = "FreightManagement")]
  [Export(typeof(Setting))]
  [DataContract(IsReference = true, Namespace = FreightManagement.Business.Namespace.FreightManagement)]
  public class DefaultBaySetting : Setting
  {
    #region Constructors

    public DefaultBaySetting()
    {
    }

    #endregion

    #region string Name

    public override string Name
    {
      get { return "Default Bay"; }
    }

    #endregion

    #region string SettingGroupIdentifier

    protected override string SettingGroupIdentifier
    {
      get { return "46f1a0e8-072e-4eda-9217-56dbc928e9d5"; }
    }

    #endregion

    #region Bay Bay

    public const string BayProperty = "Bay";
    [PersistantProperty(IsCached = true)]
    public Bay Bay
    {
      get { return GetCachedObject<Bay>(); }
      set { SetCachedObject<Bay>(value); }
    }

    #endregion

    #region Bay FloorStatusBay

    public const string FloorStatusBayProperty = "FloorStatusBay";
    [PersistantProperty(IsCached = true)]
    public Bay FloorStatusBay
    {
      get { return GetCachedObject<Bay>(); }
      set { SetCachedObject<Bay>(value); }
    }

    #endregion

    #region Bay ReceiptingBay

    public const string ReceiptingBayProperty = "ReceiptingBay";
    [PersistantProperty(IsCached = true)]
    public Bay ReceiptingBay
    {
      get { return GetCachedObject<Bay>(); }
      set { SetCachedObject<Bay>(value); }
    }

    #endregion

  }
}

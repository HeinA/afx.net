﻿using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Documents;
using Afx.Business.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Business
{
  [PersistantObject(Schema = "FreightManagement")]
  public partial class RatesConfiguration : Document
  {
    public RatesConfiguration()
      : base(Cache.Instance.GetObject<DocumentType>(DocumentTypeIdentifier))
    {
    }

    public RatesConfiguration(RatesConfiguration config)
      : base(Cache.Instance.GetObject<DocumentType>(DocumentTypeIdentifier))
    {
      Revisions.Add(config.GetRevision(DocumentDate).CreateRevision(DocumentDate));
    }

    #region string Name

    public const string NameProperty = "Name";
    [DataMember(Name = NameProperty, EmitDefaultValue = false)]
    string mName;
    [PersistantProperty]
    public string Name
    {
      get { return mName; }
      set { SetProperty<string>(ref mName, value); }
    }

    #endregion

    #region bool IsStandardConfiguration

    public const string IsStandardConfigurationProperty = "IsStandardConfiguration";
    [DataMember(Name = IsStandardConfigurationProperty, EmitDefaultValue = false)]
    bool mIsStandardConfiguration;
    [PersistantProperty]
    public bool IsStandardConfiguration
    {
      get { return mIsStandardConfiguration; }
      set { SetProperty<bool>(ref mIsStandardConfiguration, value); }
    }

    #endregion

    #region BusinessObjectCollection<RatesConfigurationRevision> Revisions

    public const string RevisionsProperty = "Revisions";
    BusinessObjectCollection<RatesConfigurationRevision> mRevisions;
    [PersistantCollection]
    [DataMember]
    [Mandatory("Rates Configuration requires at least one revision.")]
    public BusinessObjectCollection<RatesConfigurationRevision> Revisions
    {
      get { return mRevisions ?? (mRevisions = new BusinessObjectCollection<RatesConfigurationRevision>(this)); }
    }

    #endregion

    public RatesConfigurationRevision GetRevision(DateTime date)
    {
      return Revisions.Where(r => r.EffectiveDate.Date <= date).OrderByDescending(r => r.EffectiveDate).FirstOrDefault();
    }
  }
}

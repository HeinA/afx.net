﻿using Afx.Business.Data;
using Afx.Business.Tabular;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Business
{
  [PersistantObject(Schema = "FreightManagement")]
  [DataContract(IsReference = true, Namespace = FreightManagement.Business.Namespace.FreightManagement)]
  public class DistanceRateTable : TabularBusinessObject<DistanceRateTableDistanceCategory, DistanceRateTableColumn, DistanceRateTableRate> 
  {
    #region Constructors

    public DistanceRateTable()
    {
    }

    public DistanceRateTable(DistanceRateTable table)
      : this(table, null)
    {
    }

    internal DistanceRateTable(DistanceRateTable table, Dictionary<WaybillChargeType, decimal> cargoTypeIncreases)
    {
      BillingUnit = table.BillingUnit;
      UnitOfMeasure = table.UnitOfMeasure;
      MinimumCharge = table.MinimumCharge;

      foreach (var d in table.Rows)
      {
        AddRow(new DistanceRateTableDistanceCategory(d.Distance));
      }

      foreach (var c in table.Columns)
      {
        DistanceRateTableColumnFee fee = c as DistanceRateTableColumnFee;
        if (fee != null)
        {
          AddColumn(new DistanceRateTableColumnFee(fee.ChargeType));
        }

        DistanceRateTableColumnCategory cat = c as DistanceRateTableColumnCategory;
        if (cat != null)
        {
          AddColumn(new DistanceRateTableColumnCategory(cat, false));
        }
      }

      for (int iRow = 0; iRow < RowCollection.Count; iRow++)
      {
        for (int iColumn = 0; iColumn < ColumnCollection.Count; iColumn++)
        {
          var rtr = table[iRow, iColumn];
          decimal increase = 0;
          DistanceRateTableColumnCategory rtc = rtr.Column as DistanceRateTableColumnCategory;
          if (rtc != null)
          {
            increase = GetIncrease(rtc.CostComponentType, cargoTypeIncreases);
          }
          DistanceRateTableColumnFee rtf = rtr.Column as DistanceRateTableColumnFee;
          if (rtf != null)
          {
            increase = GetIncrease(rtf.ChargeType, cargoTypeIncreases);
          }
          this[iRow, iColumn].Rate = rtr.Rate + (rtr.Rate * increase / 100m);
        }
      }
    }

    #endregion

    decimal GetIncrease(WaybillChargeType ct, Dictionary<WaybillChargeType, decimal> cargoTypeIncreases)
    {
      if (cargoTypeIncreases == null) return 0;
      if (!cargoTypeIncreases.ContainsKey(ct)) return 0;
      return cargoTypeIncreases[ct];
    }

    #region BillingUnit BillingUnit

    public const string BillingUnitProperty = "BillingUnit";
    [DataMember(Name = BillingUnitProperty, EmitDefaultValue = false)]
    BillingUnit mBillingUnit;
    [PersistantProperty]
    public BillingUnit BillingUnit
    {
      get { return mBillingUnit; }
      set { SetProperty<BillingUnit>(ref mBillingUnit, value); }
    }

    #endregion

    #region string UnitOfMeasure

    public const string UnitOfMeasureProperty = "UnitOfMeasure";
    [DataMember(Name = UnitOfMeasureProperty, EmitDefaultValue = false)]
    string mUnitOfMeasure;
    [PersistantProperty]
    public string UnitOfMeasure
    {
      get { return mUnitOfMeasure; }
      set { SetProperty<string>(ref mUnitOfMeasure, value); }
    }

    #endregion

    #region decimal MinimumCharge

    public const string MinimumChargeProperty = "MinimumCharge";
    [DataMember(Name = MinimumChargeProperty, EmitDefaultValue = false)]
    decimal mMinimumCharge;
    [PersistantProperty]
    public decimal MinimumCharge
    {
      get { return mMinimumCharge; }
      set { SetProperty<decimal>(ref mMinimumCharge, value); }
    }

    #endregion

    #region WaybillFlag RequiredFlag

    public const string RequiredFlagProperty = "RequiredFlag";
    [PersistantProperty(IsCached = true)]
    public WaybillFlag RequiredFlag
    {
      get { return GetCachedObject<WaybillFlag>(); }
      set { SetCachedObject<WaybillFlag>(value); }
    }

    #endregion

  }
}

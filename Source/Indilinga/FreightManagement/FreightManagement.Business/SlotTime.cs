﻿using Afx.Business;
using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Validation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace FreightManagement.Business
{
  [PersistantObject(Schema = "FreightManagement")]
  [Cache]
  public partial class SlotTime : BusinessObject, IRevisable 
  {
    #region Constructors

    public SlotTime()
    {
    }

    public SlotTime(bool isNull)
      : base(isNull)
    {
    }

    #endregion

    #region int Revision

    public const string RevisionProperty = "Revision";
    [DataMember(Name = RevisionProperty, EmitDefaultValue = false)]
    int mRevision;
    [PersistantProperty]
    public int Revision
    {
      get { return mRevision; }
      set { SetProperty<int>(ref mRevision, value); }
    }

    #endregion

    #region int HH

    public const string HHProperty = "HH";
    [DataMember(Name = HHProperty, EmitDefaultValue = false)]
    int mHH;
    [PersistantProperty]
    public int HH
    {
      get { return mHH; }
      set
      {
        if (SetProperty<int>(ref mHH, value))
        {
          OnPropertyChanged(TextProperty);
        }
      }
    }

    #endregion

    #region int MM

    public const string MMProperty = "MM";
    [DataMember(Name = MMProperty, EmitDefaultValue = false)]
    int mMM;
    [PersistantProperty]
    public int MM
    {
      get { return mMM; }
      set
      {
        if (SetProperty<int>(ref mMM, value))
        {
          OnPropertyChanged(TextProperty);
        }
      }
    }

    #endregion

    #region string Text

    public const string TextProperty = "Text";
    public string Text
    {
      get
      {
        if (GlobalIdentifier == Guid.Empty) return string.Empty;
        return string.Format("{0:00}:{1:00}", HH, MM);
      }
    }

    #endregion
  }
}

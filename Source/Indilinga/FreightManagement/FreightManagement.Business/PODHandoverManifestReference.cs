﻿using Afx.Business;
using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Documents;
using Afx.Business.Validation;
using FleetManagement.Business;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace FreightManagement.Business
{
  [PersistantObject(Schema = "FreightManagement", TableName = "vPODHandoverManifestReference", IsReadOnly = true)]
  public partial class PODHandoverManifestReference : BusinessObject 
  {
    #region Constructors

    public PODHandoverManifestReference()
    {
    }

    #endregion



    #region string ManifestNumber

    public const string ManifestNumberProperty = "ManifestNumber";
    [DataMember(Name = ManifestNumberProperty, EmitDefaultValue = false)]
    string mManifestNumber;
    [PersistantProperty]
    public string ManifestNumber
    {
      get { return mManifestNumber; }
      private set { SetProperty<string>(ref mManifestNumber, value); }
    }

    #endregion

    #region DateTime ManifestDate

    public const string ManifestDateProperty = "ManifestDate";
    [DataMember(Name = ManifestDateProperty, EmitDefaultValue = false)]
    DateTime mManifestDate;
    [PersistantProperty]
    public DateTime ManifestDate
    {
      get { return mManifestDate; }
      private set { SetProperty<DateTime>(ref mManifestDate, value); }
    }

    #endregion

    #region DocumentTypeState ManifestState

    public const string ManifestStateProperty = "ManifestState";
    [PersistantProperty(IsCached = true)]
    public DocumentTypeState ManifestState
    {
      get { return GetCachedObject<DocumentTypeState>(); }
      set { SetCachedObject<DocumentTypeState>(value); }
    }

    #endregion

    #region Route Route

    public const string RouteProperty = "Route";
    [PersistantProperty(IsCached = true)]
    public Route Route
    {
      get { return GetCachedObject<Route>(); }
      set { SetCachedObject<Route>(value); }
    }

    #endregion

    #region int PODHandoverCount

    public const string PODHandoverCountProperty = "PODHandoverCount";
    [DataMember(Name = PODHandoverCountProperty, EmitDefaultValue = false)]
    int mPODHandoverCount;
    [PersistantProperty]
    public int PODHandoverCount
    {
      get { return mPODHandoverCount; }
      private set { SetProperty<int>(ref mPODHandoverCount, value); }
    }

    #endregion

    #region Vehicles

    #region Vehicle VehicleHorse

    public const string VehicleHorseProperty = "VehicleHorse";
    [DataMember(Name = VehicleHorseProperty, EmitDefaultValue = false)]
    Vehicle mVehicleHorse;
    [PersistantProperty]
    public Vehicle VehicleHorse
    {
      get { return mVehicleHorse; }
      set { SetProperty<Vehicle>(ref mVehicleHorse, value); }
    }

    #endregion


    #region Vehicle VehicleTrailer1

    public const string VehicleTrailer1Property = "VehicleTrailer1";
    [DataMember(Name = VehicleTrailer1Property, EmitDefaultValue = false)]
    Vehicle mVehicleTrailer1;
    [PersistantProperty]
    public Vehicle VehicleTrailer1
    {
      get { return mVehicleTrailer1; }
      set { SetProperty<Vehicle>(ref mVehicleTrailer1, value); }
    }

    #endregion

    #region Vehicle VehicleTrailer2

    public const string VehicleTrailer2Property = "VehicleTrailer2";
    [DataMember(Name = VehicleTrailer2Property, EmitDefaultValue = false)]
    Vehicle mVehicleTrailer2;
    [PersistantProperty]
    public Vehicle VehicleTrailer2
    {
      get { return mVehicleTrailer2; }
      set { SetProperty<Vehicle>(ref mVehicleTrailer2, value); }
    }

    #endregion

    #region Vehicle VehicleTrailer3

    public const string VehicleTrailer3Property = "VehicleTrailer3";
    [DataMember(Name = VehicleTrailer3Property, EmitDefaultValue = false)]
    Vehicle mVehicleTrailer3;
    [PersistantProperty]
    public Vehicle VehicleTrailer3
    {
      get { return mVehicleTrailer3; }
      set { SetProperty<Vehicle>(ref mVehicleTrailer3, value); }
    }

    #endregion

    #region Vehicle VehicleTrailer4

    public const string VehicleTrailer4Property = "VehicleTrailer4";
    [DataMember(Name = VehicleTrailer4Property, EmitDefaultValue = false)]
    Vehicle mVehicleTrailer4;
    [PersistantProperty]
    public Vehicle VehicleTrailer4
    {
      get { return mVehicleTrailer4; }
      set { SetProperty<Vehicle>(ref mVehicleTrailer4, value); }
    }

    #endregion

    #endregion

  }
}

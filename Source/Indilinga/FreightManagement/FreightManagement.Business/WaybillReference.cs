﻿using AccountManagement.Business;
using Afx.Business;
using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Documents;
using Afx.Business.Validation;
using Geographics.Business;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using ClosedXML.Excel;
using Afx.Business.Security;

namespace FreightManagement.Business
{
  [PersistantObject(Schema = "FreightManagement", TableName = "vWaybillReference", IsReadOnly = true)]
  public partial class WaybillReference : BusinessObject, IExcelRow
  {
    public static class ExportKeys
    {
      public const string OutstandingPods = "OutstandingPods";
    }

    #region Constructors

    public WaybillReference()
    {
    }

    #endregion


    #region bool IsChecked

    public const string IsCheckedProperty = "IsChecked";
    bool mIsChecked = false;
    public bool IsChecked
    {
      get { return mIsChecked; }
      set { SetProperty<bool>(ref mIsChecked, value); }
    }

    #endregion

    #region string WaybillNumber

    public const string WaybillNumberProperty = "WaybillNumber";
    [DataMember(Name = WaybillNumberProperty, EmitDefaultValue = false)]
    string mWaybillNumber;
    [PersistantProperty]
    public string WaybillNumber
    {
      get { return mWaybillNumber; }
      set { SetProperty<string>(ref mWaybillNumber, value); }
    }

    #endregion

    #region DateTime WaybillDate

    public const string WaybillDateProperty = "WaybillDate";
    [DataMember(Name = WaybillDateProperty, EmitDefaultValue = false)]
    DateTime mWaybillDate;
    [PersistantProperty]
    public DateTime WaybillDate
    {
      get { return mWaybillDate; }
      set { SetProperty<DateTime>(ref mWaybillDate, value); }
    }

    #endregion

    #region DateTime DeliveryDate

    public const string DeliveryDateProperty = "DeliveryDate";
    [DataMember(Name = DeliveryDateProperty, EmitDefaultValue = false)]
    DateTime mDeliveryDate;
    [PersistantProperty]
    public DateTime DeliveryDate
    {
      get { return mDeliveryDate; }
      set { SetProperty<DateTime>(ref mDeliveryDate, value); }
    }

    #endregion

    #region DocumentTypeState WaybillState

    public const string WaybillStateProperty = "WaybillState";
    [PersistantProperty(IsCached = true)]
    public DocumentTypeState WaybillState
    {
      get { return GetCachedObject<DocumentTypeState>(); }
      set { SetCachedObject<DocumentTypeState>(value); }
    }

    #endregion

    #region DateTime WaybillStateTimestamp

    public const string WaybillStateTimestampProperty = "WaybillStateTimestamp";
    [DataMember(Name = WaybillStateTimestampProperty, EmitDefaultValue = false)]
    DateTime mWaybillStateTimestamp;
    [PersistantProperty]
    public DateTime WaybillStateTimestamp
    {
      get { return mWaybillStateTimestamp; }
      set { SetProperty<DateTime>(ref mWaybillStateTimestamp, value); }
    }

    #endregion

    #region AccountReference Account

    public const string AccountProperty = "Account";
    [PersistantProperty(IsCached = true)]
    public AccountReference Account
    {
      get { return GetCachedObject<AccountReference>(); }
      set { SetCachedObject<AccountReference>(value); }
    }

    #endregion

    #region string Consigner

    public const string ConsignerProperty = "Consigner";
    [DataMember(Name = ConsignerProperty, EmitDefaultValue = false)]
    string mConsigner;
    [PersistantProperty]
    public string Consigner
    {
      get { return mConsigner; }
      set { SetProperty<string>(ref mConsigner, value); }
    }

    #endregion

    #region string ConsignerImportReference

    public const string ConsignerImportReferenceProperty = "ConsignerImportReference";
    [DataMember(Name = ConsignerImportReferenceProperty, EmitDefaultValue = false)]
    string mConsignerImportReference;
    [PersistantProperty]
    public string ConsignerImportReference
    {
      get { return mConsignerImportReference; }
      set { SetProperty<string>(ref mConsignerImportReference, value); }
    }

    #endregion

    #region string ConsignerExportReference

    public const string ConsignerExportReferenceProperty = "ConsignerExportReference";
    [DataMember(Name = ConsignerExportReferenceProperty, EmitDefaultValue = false)]
    string mConsignerExportReference;
    [PersistantProperty]
    public string ConsignerExportReference
    {
      get { return mConsignerExportReference; }
      set { SetProperty<string>(ref mConsignerExportReference, value); }
    }

    #endregion

    #region string Consignee

    public const string ConsigneeProperty = "Consignee";
    [DataMember(Name = ConsigneeProperty, EmitDefaultValue = false)]
    string mConsignee;
    [PersistantProperty]
    public string Consignee
    {
      get { return mConsignee; }
      set { SetProperty<string>(ref mConsignee, value); }
    }

    #endregion

    #region string ConsigneeImportReference

    public const string ConsigneeImportReferenceProperty = "ConsigneeImportReference";
    [DataMember(Name = ConsigneeImportReferenceProperty, EmitDefaultValue = false)]
    string mConsigneeImportReference;
    [PersistantProperty]
    public string ConsigneeImportReference
    {
      get { return mConsigneeImportReference; }
      set { SetProperty<string>(ref mConsigneeImportReference, value); }
    }

    #endregion

    #region string ConsigneeExportReference

    public const string ConsigneeExportReferenceProperty = "ConsigneeExportReference";
    [DataMember(Name = ConsigneeExportReferenceProperty, EmitDefaultValue = false)]
    string mConsigneeExportReference;
    [PersistantProperty]
    public string ConsigneeExportReference
    {
      get { return mConsigneeExportReference; }
      set { SetProperty<string>(ref mConsigneeExportReference, value); }
    }

    #endregion

    #region Location FullCollectionLocation

    public const string FullCollectionLocationProperty = "FullCollectionLocation";
    [PersistantProperty(IsCached = true)]
    public Location FullCollectionLocation
    {
      get { return GetCachedObject<Location>(); }
      set { SetCachedObject<Location>(value); }
    }

    #endregion

    #region Location CollectionCity

    public const string CollectionCityProperty = "CollectionCity";
    public Location CollectionCity
    {
      get
      {
        if (BusinessObject.IsNull(FullCollectionLocation)) return null;
        return FullCollectionLocation.City;
      }
    }

    #endregion

    #region Location CollectionSuburb

    public const string CollectionSuburbProperty = "CollectionSuburb";
    public Location CollectionSuburb
    {
      get
      {
        if (BusinessObject.IsNull(FullCollectionLocation)) return null;
        return FullCollectionLocation.Suburb;
      }
    }

    #endregion

    #region Location FullDeliveryLocation

    public const string FullDeliveryLocationProperty = "FullDeliveryLocation";
    [PersistantProperty(IsCached = true)]
    public Location FullDeliveryLocation
    {
      get { return GetCachedObject<Location>(); }
      set { SetCachedObject<Location>(value); }
    }

    #endregion

    #region Location DeliveryCity

    public const string DeliveryCityProperty = "DeliveryCity";
    public Location DeliveryCity
    {
      get
      {
        if (BusinessObject.IsNull(FullDeliveryLocation)) return null;
        return FullDeliveryLocation.City;
      }
    }

    #endregion

    #region Location DeliverySuburb

    public const string DeliverySuburbProperty = "DeliverySuburb";
    public Location DeliverySuburb
    {
      get
      {
        if (BusinessObject.IsNull(FullDeliveryLocation)) return null;
        return FullDeliveryLocation.Suburb;
      }
    }

    #endregion

    #region string CollectionAddress

    public const string CollectionAddressProperty = "CollectionAddress";
    [DataMember(Name = CollectionAddressProperty, EmitDefaultValue = false)]
    string mCollectionAddress;
    [PersistantProperty]
    public string CollectionAddress
    {
      get { return mCollectionAddress; }
      set { SetProperty<string>(ref mCollectionAddress, value); }
    }

    #endregion

    #region string DeliveryAddress

    public const string DeliveryAddressProperty = "DeliveryAddress";
    [DataMember(Name = DeliveryAddressProperty, EmitDefaultValue = false)]
    string mDeliveryAddress;
    [PersistantProperty]
    public string DeliveryAddress
    {
      get { return mDeliveryAddress; }
      set { SetProperty<string>(ref mDeliveryAddress, value); }
    }

    #endregion

    #region decimal Charge

    public const string ChargeProperty = "Charge";
    [DataMember(Name = ChargeProperty, EmitDefaultValue = false)]
    decimal mCharge;
    [PersistantProperty]
    public decimal Charge
    {
      get { return mCharge; }
      set { SetProperty<decimal>(ref mCharge, value); }
    }

    #endregion

    #region int Items

    public const string ItemsProperty = "Items";
    [DataMember(Name = ItemsProperty, EmitDefaultValue = false)]
    int mItems;
    [PersistantProperty]
    public int Items
    {
      get { return mItems; }
      set { SetProperty<int>(ref mItems, value); }
    }

    #endregion

    #region decimal PhysicalWeight

    public const string PhysicalWeightProperty = "PhysicalWeight";
    [DataMember(Name = PhysicalWeightProperty, EmitDefaultValue = false)]
    decimal mPhysicalWeight;
    [PersistantProperty]
    public decimal PhysicalWeight
    {
      get { return mPhysicalWeight; }
      set { SetProperty<decimal>(ref mPhysicalWeight, value); }
    }

    #endregion

    #region decimal Volume

    public const string VolumeProperty = "Volume";
    [DataMember(Name = VolumeProperty, EmitDefaultValue = false)]
    decimal mVolume;
    [PersistantProperty]
    public decimal Volume
    {
      get { return mVolume; }
      set { SetProperty<decimal>(ref mVolume, value); }
    }

    #endregion

    #region bool IsPointToPoint

    public const string IsPointToPointProperty = "IsPointToPoint";
    [DataMember(Name = IsPointToPointProperty, EmitDefaultValue = false)]
    bool mIsPointToPoint;
    [PersistantProperty]
    public bool IsPointToPoint
    {
      get { return mIsPointToPoint; }
      set { SetProperty<bool>(ref mIsPointToPoint, value); }
    }

    #endregion

    #region string ClientReferenceNumber

    public const string ClientReferenceNumberProperty = "ClientReferenceNumber";
    [DataMember(Name = ClientReferenceNumberProperty, EmitDefaultValue = false)]
    string mClientReferenceNumber;
    [PersistantProperty]
    public string ClientReferenceNumber
    {
      get { return mClientReferenceNumber; }
      set { SetProperty<string>(ref mClientReferenceNumber, value); }
    }

    #endregion

    #region Bay Bay

    public const string BayProperty = "Bay";
    [PersistantProperty(IsCached = true)]
    public Bay Bay
    {
      get { return BusinessObject.IsNull(GetCachedObject<Bay>()) ? Setting.GetSetting<DefaultBaySetting>().Bay : GetCachedObject<Bay>(); }
      set { SetCachedObject<Bay>(value); }
    }

    #endregion

    #region Associative BusinessObjectCollection<WaybillFlag> WaybillFlags

    public const string WaybillFlagsProperty = "WaybillFlags";
    AssociativeObjectCollection<WaybillReferenceWaybillFlag, WaybillFlag> mWaybillFlags;
    [PersistantCollection(AssociativeType = typeof(WaybillReferenceWaybillFlag))]
    public BusinessObjectCollection<WaybillFlag> WaybillFlags
    {
      get
      {
        if (mWaybillFlags == null) using (new EventStateSuppressor(this)) { mWaybillFlags = new AssociativeObjectCollection<WaybillReferenceWaybillFlag, WaybillFlag>(this); }
        return mWaybillFlags;
      }
    }

    #endregion

    #region DistributionCenter DistributionCenter

    public const string DistributionCenterProperty = "DistributionCenter";
    [PersistantProperty(IsCached = true)]
    public DistributionCenter DistributionCenter
    {
      get { return GetCachedObject<DistributionCenter>(); }
      set { SetCachedObject<DistributionCenter>(value); }
    }

    #endregion

    #region Route Route

    public const string RouteProperty = "Route";
    [PersistantProperty(IsCached = true)]
    public Route Route
    {
      get { return GetCachedObject<Route>(); }
      set { SetCachedObject<Route>(value); }
    }

    #endregion

    #region string InvoiceNumber

    public const string InvoiceNumberProperty = "InvoiceNumber";
    [DataMember(Name = InvoiceNumberProperty, EmitDefaultValue = false)]
    string mInvoiceNumber;
    [PersistantProperty]
    public string InvoiceNumber
    {
      get { return mInvoiceNumber; }
      set { SetProperty<string>(ref mInvoiceNumber, value); }
    }

    #endregion

    #region string AccountingOrderNumber

    public const string AccountingOrderNumberProperty = "AccountingOrderNumber";
    [DataMember(Name = AccountingOrderNumberProperty, EmitDefaultValue = false)]
    string mAccountingOrderNumber;
    [PersistantProperty]
    public string AccountingOrderNumber
    {
      get { return mAccountingOrderNumber; }
      set { SetProperty<string>(ref mAccountingOrderNumber, value); }
    }

    #endregion

    #region string InvoiceTo

    public const string InvoiceToProperty = "InvoiceTo";
    [DataMember(Name = InvoiceToProperty, EmitDefaultValue = false)]
    string mInvoiceTo;
    [PersistantProperty]
    public string InvoiceTo
    {
      get { return mInvoiceTo; }
      set { SetProperty<string>(ref mInvoiceTo, value); }
    }

    #endregion

    #region DateTime? PODDate

    public const string PODDateProperty = "PODDate";
    [DataMember(Name = PODDateProperty, EmitDefaultValue = false)]
    DateTime? mPODDate;
    [PersistantProperty]
    public DateTime? PODDate
    {
      get { return mPODDate; }
      set { SetProperty<DateTime?>(ref mPODDate, value); }
    }

    #endregion

    #region string PODSignature

    public const string PODSignatureProperty = "PODSignature";
    [DataMember(Name = PODSignatureProperty, EmitDefaultValue = false)]
    string mPODSignature;
    [PersistantProperty]
    public string PODSignature
    {
      get { return mPODSignature; }
      set
      {
        if (SetProperty<string>(ref mPODSignature, value))
        {
          if (PODDate == null) PODDate = DateTime.Now;
        }
      }
    }

    #endregion


    #region int PODHandoverCount

    public const string PODHandoverCountProperty = "PODHandoverCount";
    [DataMember(Name = PODHandoverCountProperty, EmitDefaultValue = false)]
    int mPODHandoverCount;
    [PersistantProperty]
    public int PODHandoverCount
    {
      get { return mPODHandoverCount; }
      set { SetProperty<int>(ref mPODHandoverCount, value); }
    }

    #endregion

    #region BusinessObjectCollection<WaybillReferenceRoute> Routes

    public const string RoutesProperty = "Routes";
    BusinessObjectCollection<WaybillReferenceRoute> mRoutes;
    [PersistantCollection]
    [DataMember]
    public BusinessObjectCollection<WaybillReferenceRoute> Routes
    {
      get { return mRoutes ?? (mRoutes = new BusinessObjectCollection<WaybillReferenceRoute>(this)); }
    }

    #endregion

    #region Route CollectionRoute

    public const string CollectionRouteProperty = "CollectionRoute";
    [PersistantProperty(IsCached = true)]
    public Route CollectionRoute
    {
      get { return GetCachedObject<Route>(); }
      set { SetCachedObject<Route>(value); }
    }

    #endregion

    #region Distribution Centers

    public const string CurrentDistributionCenterProperty = "CurrentDistributionCenter";
    public DistributionCenter CurrentDistributionCenter
    {
      get
      {
        WaybillReferenceRoute route = Routes.Where(r => !r.IsDeleted && r.ArrivalTimestamp != null && r.DepartureTimestamp == null).FirstOrDefault();
        if (route == null) return null;
        return route.DistributionCenter;
      }
    }

    public DistributionCenter NextDistributionCenter
    {
      get
      {
        WaybillReferenceRoute route = Routes.Where(r => !r.IsDeleted && r.ArrivalTimestamp == null && r.DepartureTimestamp == null).FirstOrDefault();
        if (route == null) return null;
        return route.DistributionCenter;
      }
    }

    public DistributionCenter LastDistributionCenter
    {
      get
      {
        WaybillReferenceRoute route = Routes.Where(r => !r.IsDeleted && r.ArrivalTimestamp != null && r.DepartureTimestamp != null).LastOrDefault();
        if (route == null) return null;
        return route.DistributionCenter;
      }
    }

    public DateTime? LastDepartureDate
    {
      get
      {
        WaybillReferenceRoute route = Routes.Where(r => !r.IsDeleted && r.ArrivalTimestamp != null && r.DepartureTimestamp != null).LastOrDefault();
        if (route == null) return null;
        return route.DepartureTimestamp;
      }
    }

    public bool HasBeenReceivedByDistributionCenter(DistributionCenter dc)
    {
      foreach (var r in this.Routes)
      {
        if (r.DistributionCenter.Equals(dc) && r.ArrivalTimestamp != null) return true;
      }
      return false;
    }

    #endregion


    public int DeliveryDuration
    {
      get
      {
        var route = Routes.FirstOrDefault();
        var inductionDate = route == null ? null : route.ArrivalTimestamp;
        if (inductionDate == null) return 0;
        if (PODDate != null) return (int)PODDate.Value.Subtract(inductionDate.Value).TotalHours;
        var duration = (int)DateTime.Now.Subtract(inductionDate.Value).TotalHours;
        if (Routes.Count(r => r.ArrivalTimestamp == null) == Routes.Count && !BusinessObject.IsNull(CollectionRoute))
        {
          duration += CollectionRoute.GetExtensionObject<RouteExtension>().ExpectedServiceDuration;
        }
        if (duration < 0)
        {
        }
        return duration;
      }
    }

    public int EstimatedDeliveryDuration
    {
      get
      {
        return DeliveryDuration + RemainingDuration;
      }
    }

    public int MinimumServiceDuration
    {
      get
      {
        var wf = this.WaybillFlags.FirstOrDefault(wf1 => wf1.Owner.IsFlagged(WaybillFlagGroupFlags.CostingFlagGroup));
        if (wf == null) return 0;
        return wf.GetExtensionObject<WaybillFlagServiceDuration>().MinimumServiceDuration;
      }
    }

    public int MaximumServiceDuration
    {
      get
      {
        var wf = this.WaybillFlags.FirstOrDefault(wf1 => wf1.Owner.IsFlagged(WaybillFlagGroupFlags.CostingFlagGroup));
        if (wf == null) return 0;
        return wf.GetExtensionObject<WaybillFlagServiceDuration>().MaximumServiceDuration;
      }
    }

    public int RemainingDuration
    {
      get
      {
        int remainder = 0;
        foreach (var route in Routes.Where(r => r.DepartureTimestamp == null))
        {
          remainder += route.Route.GetExtensionObject<RouteExtension>().ExpectedServiceDuration;
        }
        if (Routes.Count(r => r.ArrivalTimestamp == null) == Routes.Count && !BusinessObject.IsNull(CollectionRoute))
        {
          remainder += CollectionRoute.GetExtensionObject<RouteExtension>().ExpectedServiceDuration;
        }
        return remainder;
      }
    }

    public decimal DeliveryPriority
    {
      get
      {
        decimal msd = MaximumServiceDuration;
        decimal edd = EstimatedDeliveryDuration;
        if (msd == 0 || edd == 0) return 0;
        return edd / msd;
      }
    }

    public string CostingFlagsText
    {
      get { return string.Join(", ", WaybillFlags.Where(f => f.Owner.IsFlagged(WaybillFlagGroupFlags.CostingFlagGroup)).Select(f => f.Text)); }
    }

    public string ProblemFlagsText
    {
      get { return string.Join(", ", WaybillFlags.Where(f => f.Owner.IsFlagged(WaybillFlagGroupFlags.ProblemFlagGroup)).Select(f => f.Text)); }
    }

    #region decimal ChargeDocumentation

    public const string ChargeDocumentationProperty = "ChargeDocumentation";
    [DataMember(Name = ChargeDocumentationProperty, EmitDefaultValue = false)]
    decimal mChargeDocumentation;
    [PersistantProperty]
    public decimal ChargeDocumentation
    {
      get { return mChargeDocumentation; }
      set { SetProperty<decimal>(ref mChargeDocumentation, value); }
    }

    #endregion

    #region decimal ChargeTransport

    public const string ChargeTransportProperty = "ChargeTransport";
    [DataMember(Name = ChargeTransportProperty, EmitDefaultValue = false)]
    decimal mChargeTransport;
    [PersistantProperty]
    public decimal ChargeTransport
    {
      get { return mChargeTransport; }
      set { SetProperty<decimal>(ref mChargeTransport, value); }
    }

    #endregion

    #region decimal ChargeCollectionDelivery

    public const string ChargeCollectionDeliveryProperty = "ChargeCollectionDelivery";
    [DataMember(Name = ChargeCollectionDeliveryProperty, EmitDefaultValue = false)]
    decimal mChargeCollectionDelivery;
    [PersistantProperty]
    public decimal ChargeCollectionDelivery
    {
      get { return mChargeCollectionDelivery; }
      set { SetProperty<decimal>(ref mChargeCollectionDelivery, value); }
    }

    #endregion

    #region decimal ChargeVat

    public const string ChargeVatProperty = "ChargeVat";
    [DataMember(Name = ChargeVatProperty, EmitDefaultValue = false)]
    decimal mChargeVat;
    [PersistantProperty]
    public decimal ChargeVat
    {
      get { return mChargeVat; }
      set { SetProperty<decimal>(ref mChargeVat, value); }
    }

    #endregion

    #region decimal ChargeOther

    public const string ChargeOtherProperty = "ChargeOther";
    [DataMember(Name = ChargeOtherProperty, EmitDefaultValue = false)]
    decimal mChargeOther;
    [PersistantProperty]
    public decimal ChargeOther
    {
      get { return mChargeOther; }
      set { SetProperty<decimal>(ref mChargeOther, value); }
    }

    #endregion

    #region decimal ChargeTotal

    public const string ChargeTotalProperty = "ChargeTotal";
    decimal mChargeTotal;
    public decimal ChargeTotal
    {
      get { return ChargeDocumentation + ChargeTransport + ChargeCollectionDelivery + ChargeVat + ChargeOther; }
    }

    #endregion

    #region decimal DeclaredValue

    public const string DeclaredValueProperty = "DeclaredValue";
    [DataMember(Name = DeclaredValueProperty, EmitDefaultValue = false)]
    decimal mDeclaredValue;
    [PersistantProperty]
    public decimal DeclaredValue
    {
      get { return mDeclaredValue; }
      set { SetProperty<decimal>(ref mDeclaredValue, value); }
    }

    #endregion

    #region decimal ChargeImportVAT

    public const string ChargeImportVATProperty = "ChargeImportVAT";
    [DataMember(Name = ChargeImportVATProperty, EmitDefaultValue = false)]
    decimal mChargeImportVAT;
    [PersistantProperty]
    public decimal ChargeImportVAT
    {
      get { return mChargeImportVAT; }
      set { SetProperty<decimal>(ref mChargeImportVAT, value); }
    }

    #endregion

    #region decimal ChargeFuelLevy

    public const string ChargeFuelLevyProperty = "ChargeFuelLevy";
    [DataMember(Name = ChargeFuelLevyProperty, EmitDefaultValue = false)]
    decimal mChargeFuelLevy;
    [PersistantProperty]
    public decimal ChargeFuelLevy
    {
      get { return mChargeFuelLevy; }
      set { SetProperty<decimal>(ref mChargeFuelLevy, value); }
    }

    #endregion


    #region BusinessObjectCollection<WaybillReferenceCargoItem> CargoItems

    public const string CargoItemsProperty = "CargoItems";
    BusinessObjectCollection<WaybillReferenceCargoItem> mCargoItems;
    [PersistantCollection]
    [DataMember]
    public BusinessObjectCollection<WaybillReferenceCargoItem> CargoItems
    {
      get { return mCargoItems ?? (mCargoItems = new BusinessObjectCollection<WaybillReferenceCargoItem>(this)); }
    }

    #endregion

    #region BusinessObjectCollection<WaybillReferenceParcel> Parcels

    public const string ParcelsProperty = "Parcels";
    BusinessObjectCollection<WaybillReferenceParcel> mParcels;
    [PersistantCollection]
    [DataMember]
    public BusinessObjectCollection<WaybillReferenceParcel> Parcels
    {
      get { return mParcels ?? (mParcels = new BusinessObjectCollection<WaybillReferenceParcel>(this)); }
    }

    #endregion

    #region BusinessObjectCollection<WaybillReferenceDocumentReference> References

    public const string ReferencesProperty = "References";
    BusinessObjectCollection<WaybillReferenceDocumentReference> mReferences;
    [PersistantCollection]
    [DataMember]
    public BusinessObjectCollection<WaybillReferenceDocumentReference> References
    {
      get { return mReferences ?? (mReferences = new BusinessObjectCollection<WaybillReferenceDocumentReference>(this)); }
    }

    #endregion

    #region IExcelRow

    public void ExportHeader(IXLWorksheet sheet, int row, string exportKey)
    {
      int column = 1;
      sheet.Cell(row, column++).SetValue("Waybill Number").Style.Font.SetBold();
      sheet.Cell(row, column++).SetValue("Waybill State").Style.Font.SetBold();
      sheet.Cell(row, column++).SetValue("Client Reference").Style.Font.SetBold();
      sheet.Cell(row, column++).SetValue("Waybill Date").Style.Font.SetBold();
      sheet.Cell(row, column++).SetValue("Delivery Date").Style.Font.SetBold();
      sheet.Cell(row, column++).SetValue("Account").Style.Font.SetBold();
      sheet.Cell(row, column++).SetValue("Consigner").Style.Font.SetBold();
      sheet.Cell(row, column++).SetValue("Consignee").Style.Font.SetBold();
      sheet.Cell(row, column++).SetValue("Collect From").Style.Font.SetBold();
      sheet.Cell(row, column++).SetValue("Deliver To").Style.Font.SetBold();
      sheet.Cell(row, column++).SetValue("Items").Style.Font.SetBold();
      sheet.Cell(row, column++).SetValue("Weight").Style.Font.SetBold();
      sheet.Cell(row, column++).SetValue("Declared Value").Style.Font.SetBold();
      sheet.Cell(row, column++).SetValue("POD Date/Time").Style.Font.SetBold();
      sheet.Cell(row, column++).SetValue("Received By").Style.Font.SetBold();
      if (SecurityContext.User.HasRole(Roles.Accounts, Roles.AccountsSupervisor, Roles.Administrator))
      {
        sheet.Cell(row, column++).SetValue("Collection / Delivery").Style.Font.SetBold();
        sheet.Cell(row, column++).SetValue("Documentation").Style.Font.SetBold();
        sheet.Cell(row, column++).SetValue("Transport").Style.Font.SetBold();
        sheet.Cell(row, column++).SetValue("VAT").Style.Font.SetBold();
        sheet.Cell(row, column++).SetValue("Fuel Levy").Style.Font.SetBold();
        sheet.Cell(row, column++).SetValue("Other").Style.Font.SetBold();
        sheet.Cell(row, column++).SetValue("Total").Style.Font.SetBold();
      }
    }

    public void ExportRow(IXLWorksheet sheet, int row, string exportKey)
    {
      int column = 1;
      sheet.Cell(row, column++).SetValue(WaybillNumber);
      sheet.Cell(row, column++).SetValue(WaybillState.Name);
      sheet.Cell(row, column++).SetValue(ClientReferenceNumber);
      sheet.Cell(row, column++).SetValue(WaybillDate).Style.DateFormat.Format = "d MMM yyyy";
      sheet.Cell(row, column++).SetValue(DeliveryDate).Style.DateFormat.Format = "d MMM yyyy";
      sheet.Cell(row, column++).SetValue(Account.Name);
      sheet.Cell(row, column++).SetValue(Consigner);
      sheet.Cell(row, column++).SetValue(Consignee);
      sheet.Cell(row, column++).SetValue(FullCollectionLocation.FullNameWithoutZone);
      sheet.Cell(row, column++).SetValue(FullDeliveryLocation.FullNameWithoutZone);
      sheet.Cell(row, column++).SetValue(Items).Style.NumberFormat.Format = "#,##0";
      sheet.Cell(row, column++).SetValue(PhysicalWeight).Style.NumberFormat.Format = "#,##0.00";
      sheet.Cell(row, column++).SetValue(DeclaredValue).Style.NumberFormat.Format = "#,##0.00";
      sheet.Cell(row, column++).SetValue(PODDate).Style.DateFormat.Format = "d MMM yyyy HH:mm";
      sheet.Cell(row, column++).SetValue(PODSignature);
      if (SecurityContext.User.HasRole(Roles.Accounts, Roles.AccountsSupervisor, Roles.Administrator))
      {
        sheet.Cell(row, column++).SetValue(ChargeCollectionDelivery).Style.NumberFormat.Format = "#,##0.00";
        sheet.Cell(row, column++).SetValue(ChargeDocumentation).Style.NumberFormat.Format = "#,##0.00";
        sheet.Cell(row, column++).SetValue(ChargeTransport).Style.NumberFormat.Format = "#,##0.00";
        sheet.Cell(row, column++).SetValue(ChargeVat).Style.NumberFormat.Format = "#,##0.00";
        sheet.Cell(row, column++).SetValue(ChargeFuelLevy).Style.NumberFormat.Format = "#,##0.00";
        sheet.Cell(row, column++).SetValue(ChargeOther).Style.NumberFormat.Format = "#,##0.00";
        sheet.Cell(row, column++).SetFormulaA1(string.Format("=SUM(N{0}:S{0})", row)).Style.NumberFormat.Format = "#,##0.00";
      }
    }

    #endregion
  }
}

﻿using Afx.Business;
using Afx.Business.Data;
using Afx.Business.Documents;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Business
{
  [PersistantObject(Schema = "Afx", OwnerColumn = "Document", TableName = "DocumentReference", IsReadOnly = true)]
  [DataContract(IsReference = true, Namespace = FreightManagement.Business.Namespace.FreightManagement)]
  public class WaybillReferenceDocumentReference : BusinessObject<WaybillReference>
  {
    #region Constructors

    public WaybillReferenceDocumentReference()
    {
    }

    public WaybillReferenceDocumentReference(bool isNull)
      : base(isNull)
    {
    }

    #endregion

    #region DocumentTypeReference DocumentTypeReference

    public const string DocumentTypeReferenceProperty = "DocumentTypeReference";
    [PersistantProperty]
    public DocumentTypeReference DocumentTypeReference
    {
      get { return GetCachedObject<DocumentTypeReference>(); }
      set { SetCachedObject<DocumentTypeReference>(value); }
    }

    #endregion

    #region string Reference

    public const string ReferenceProperty = "Reference";
    [DataMember(Name = ReferenceProperty, EmitDefaultValue = false)]
    string mReference;
    [PersistantProperty]
    public string Reference
    {
      get { return mReference; }
      set { SetProperty<string>(ref mReference, value); }
    }

    #endregion
  }
}

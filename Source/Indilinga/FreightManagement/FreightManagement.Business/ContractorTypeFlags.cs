﻿using Afx.Business;
using ContactManagement.Business;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Business
{
  [Export(typeof(IObjectFlags))]
  public class ContractorTypeFlags : IObjectFlags
  {
    public const string Is3rdPartyCourier = "FreightManagement.3rdPartyCourier";
    public const string IsClearingAgent = "FreightManagement.ClearingAgent";

    public Type ObjectType
    {
      get { return typeof(ContractorType); }
    }

    public IEnumerable<string> Flags
    {
      get { return new string[] { Is3rdPartyCourier, IsClearingAgent }; }
    }
  }
}

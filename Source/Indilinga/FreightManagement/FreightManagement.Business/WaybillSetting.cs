﻿using Afx.Business;
using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Validation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.Runtime.Serialization;

namespace FreightManagement.Business
{
  [PersistantObject(Schema = "FreightManagement")]
  [Export(typeof(Setting))]
  [DataContract(IsReference = true, Namespace = FreightManagement.Business.Namespace.FreightManagement)]
  public partial class WaybillSetting : Setting 
  {
    #region Constructors

    public WaybillSetting()
    {
    }

    #endregion

    #region string Name

    public override string Name
    {
      get { return "Waybill Report"; }
    }

    #endregion

    #region string SettingGroupIdentifier

    protected override string SettingGroupIdentifier
    {
      get { return "46F1A0E8-072E-4EDA-9217-56DBC928E9D5"; }
    }

    #endregion

    
    #region string WaybillTerms
    
    public const string WaybillTermsProperty = "WaybillTerms";
    [DataMember(Name = WaybillTermsProperty, EmitDefaultValue=false)]
    string mWaybillTerms;
    [PersistantProperty]
    public string WaybillTerms
    {
      get { return mWaybillTerms; }
      set { SetProperty<string>(ref mWaybillTerms, value); }
    }
    
    #endregion

    #region string WaybillNotes

    public const string WaybillNotesProperty = "WaybillNotes";
    [DataMember(Name = WaybillNotesProperty, EmitDefaultValue = false)]
    string mWaybillNotes;
    [PersistantProperty]
    public string WaybillNotes
    {
      get { return mWaybillNotes; }
      set { SetProperty<string>(ref mWaybillNotes, value); }
    }

    #endregion
  }
}

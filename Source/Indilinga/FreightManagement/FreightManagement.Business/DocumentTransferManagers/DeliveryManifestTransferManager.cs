﻿using Afx.Business.Data;
using Afx.Business.Documents;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Business.DocumentTransferManagers
{
  [Export(typeof(IDocumentTransferManager))]
  class DeliveryManifestTransferManager : DocumentTransferManager<DeliveryManifest>
  {
    public override void AppendRelatedDocuments(DeliveryManifest document, DocumentTransferCollection documents)
    {
      ObjectRepository<Waybill> or = PersistanceManager.GetRepository<Waybill>();

      foreach (var wr in document.Waybills)
      {
        Waybill w = or.GetInstance(wr.GlobalIdentifier);
        documents.Add(w);
      }
    }
  }
}

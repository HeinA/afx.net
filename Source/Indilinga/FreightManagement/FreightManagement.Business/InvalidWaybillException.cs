﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Business
{
  [Serializable]
  public class InvalidWaybillException : Exception
  {
    protected InvalidWaybillException(SerializationInfo info, StreamingContext context)
      :base(info, context)
    {
    }

    public InvalidWaybillException(string message)
      : base(message)
    {
    }

    public InvalidWaybillException(string message, Exception innerException)
      : base(message, innerException)
    {
    }    
  }
}

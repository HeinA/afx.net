﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Business
{
  public enum EnumTransportManifestDateFields
  {
    [StringValueAttribute("Off-Loading Completed")]
    ActualOffloadingCompleted = 0,
    [StringValueAttribute("Off-Loading")]
    ActualOffloadingTime = 1,
    [StringValueAttribute("Arrival at Consignee")]
    ArrivalTime = 2,
    [StringValueAttribute("Departed Consigner")]
    DepartureTime = 3,
    [StringValueAttribute("Loading Completed")]
    ActualLoadingCompleted = 4,
    [StringValueAttribute("Loading")]
    ActualLoadingTime = 5
  }
}

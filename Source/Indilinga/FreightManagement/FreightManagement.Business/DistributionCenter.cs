﻿using Afx.Business;
using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Business.Validation;
using Geographics.Business;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace FreightManagement.Business
{
  [PersistantObject(Schema = "FreightManagement")]
  [Cache]
  public partial class DistributionCenter : BusinessObject, IRevisable 
  {
    #region Constructors

    public DistributionCenter()
    {
    }

    public DistributionCenter(bool isNull)
      : base(isNull)
    {
    }

    #endregion

    #region int Revision

    public const string RevisionProperty = "Revision";
    [DataMember(Name = RevisionProperty, EmitDefaultValue = false)]
    int mRevision;
    [PersistantProperty]
    public int Revision
    {
      get { return mRevision; }
      set { SetProperty<int>(ref mRevision, value); }
    }

    #endregion

    #region string Name

    public const string NameProperty = "Name";
    [DataMember(Name = NameProperty, EmitDefaultValue = false)]
    string mName;
    [Mandatory("Name is mandatory.")]
    [PersistantProperty]
    public string Name
    {
      get { return mName; }
      set { SetProperty<string>(ref mName, value); }
    }

    #endregion

    #region Location Location

    public const string LocationProperty = "Location";
    [PersistantProperty]
    [Mandatory("Location is mandatory.")]
    public Location Location
    {
      get { return GetCachedObject<Location>(); }
      set { SetCachedObject<Location>(value); }
    }

    #endregion

    #region OrganizationalUnit OrganizationalUnit

    public const string OrganizationalUnitProperty = "OrganizationalUnit";
    [PersistantProperty(IsCached = true)]
    [Mandatory("Organizational Unit is mandatory.")]
    public OrganizationalUnit OrganizationalUnit
    {
      get { return GetCachedObject<OrganizationalUnit>(); }
      set { SetCachedObject<OrganizationalUnit>(value); }
    }

    #endregion

    #region BusinessObjectCollection<Route> Routes

    public const string RoutesProperty = "Routes";
    BusinessObjectCollection<Route> mRoutes;
    [PersistantCollection]
    [DataMember]
    public BusinessObjectCollection<Route> Routes
    {
      get { return mRoutes ?? (mRoutes = new BusinessObjectCollection<Route>(this)); }
    }

    #endregion
  }
}

﻿using Afx.Business;
using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Validation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace FreightManagement.Business
{
  [PersistantObject(Schema = "FreightManagement")]
  public partial class WaybillChargeTypeReference : BusinessObject<WaybillChargeType>, IExternalReference
  {
    #region Constructors

    public WaybillChargeTypeReference()
    {
    }

    #endregion

    #region ExternalSystem ExternalSystem

    public const string ExternalSystemProperty = "ExternalSystem";
    [PersistantProperty]
    [Mandatory("External System is a mandatory field.")]
    public ExternalSystem ExternalSystem
    {
      get { return GetCachedObject<ExternalSystem>(); }
      set { SetCachedObject<ExternalSystem>(value); }
    }

    #endregion

    #region string Reference

    public const string ReferenceProperty = "Reference";
    [DataMember(Name = ReferenceProperty, EmitDefaultValue = false)]
    string mReference;
    [PersistantProperty]
    [Mandatory("Reference is a mandatory field.")]
    public string Reference
    {
      get { return mReference; }
      set { SetProperty<string>(ref mReference, value); }
    }

    #endregion  
  }
}

﻿using Afx.Business;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Documents;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Business
{
  [PersistantObject(Schema = "FreightManagement")]
  public partial class WaybillReceipt : Document
  {
    public WaybillReceipt()
      : base(Cache.Instance.GetObject<DocumentType>(DocumentTypeIdentifier))
    {
    }

    #region void OnCompositionChanged(...)

    protected override void OnCompositionChanged(CompositionChangedType compositionChangedType, string propertyName, object originalSource, BusinessObject item)
    {
      OnPropertyChanged(WaybillCountProperty);
      OnPropertyChanged(ItemsProperty);
      OnPropertyChanged(WeightProperty);

      base.OnCompositionChanged(compositionChangedType, propertyName, originalSource, item);
    }

    #endregion

    #region int WaybillCount

    public const string WaybillCountProperty = "WaybillCount";
    public int WaybillCount
    {
      get { return Waybills.Count; }
    }

    #endregion

    #region int Items

    public const string ItemsProperty = "Items";
    public int Items
    {
      get { return Waybills.Sum(w => w.Items); }
    }

    #endregion

    #region decimal Weight

    public const string WeightProperty = "Weight";
    public decimal Weight
    {
      get { return Waybills.Sum(w => w.Charge); }
    }

    #endregion

    #region Associative BusinessObjectCollection<WaybillReference> Waybills

    public const string WaybillsProperty = "Waybills";
    AssociativeObjectCollection<WaybillReceiptWaybill, WaybillReference> mWaybills;
    [PersistantCollection(AssociativeType = typeof(WaybillReceiptWaybill))]
    public BusinessObjectCollection<WaybillReference> Waybills
    {
      get
      {
        if (mWaybills == null) using (new EventStateSuppressor(this)) { mWaybills = new AssociativeObjectCollection<WaybillReceiptWaybill, WaybillReference>(this); }
        return mWaybills;
      }
    }

    #endregion

    #region Associative BusinessObjectCollection<ManifestReference> Manifests

    public const string ManifestsProperty = "Manifests";
    AssociativeObjectCollection<WaybillReceiptWaybill, ManifestReference> mManifests;
    [PersistantCollection(AssociativeType = typeof(WaybillReceiptWaybill))]
    public BusinessObjectCollection<ManifestReference> Manifests
    {
      get
      {
        if (mManifests == null) using (new EventStateSuppressor(this)) { mManifests = new AssociativeObjectCollection<WaybillReceiptWaybill, ManifestReference>(this); }
        return mManifests;
      }
    }

    #endregion
  }
}

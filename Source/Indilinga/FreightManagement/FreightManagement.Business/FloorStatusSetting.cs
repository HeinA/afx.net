﻿using Afx.Business;
using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Validation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.Runtime.Serialization;

namespace FreightManagement.Business
{
  [PersistantObject(Schema = "FreightManagement")]
  [Export(typeof(Setting))]
  [DataContract(IsReference = true, Namespace = FreightManagement.Business.Namespace.FreightManagement)]
  public partial class FloorStatusSetting : Setting 
  {
    #region Constructors

    public FloorStatusSetting()
    {
    }

    #endregion

    #region string Name

    public override string Name
    {
      get { return "Floor Status"; }
    }

    #endregion

    #region string SettingGroupIdentifier

    protected override string SettingGroupIdentifier
    {
      get { return "46F1A0E8-072E-4EDA-9217-56DBC928E9D5"; }
    }

    #endregion

    #region bool ShowDetailedIncome

    public const string ShowDetailedIncomeProperty = "ShowDetailedIncome";
    [DataMember(Name = ShowDetailedIncomeProperty, EmitDefaultValue = false)]
    bool mShowDetailedIncome;
    [PersistantProperty]
    public bool ShowDetailedIncome
    {
      get { return mShowDetailedIncome; }
      set { SetProperty<bool>(ref mShowDetailedIncome, value); }
    }

    #endregion
  }
}

﻿using Afx.Business;
using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Tabular;
using Afx.Business.Validation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Runtime.Serialization;

namespace FreightManagement.Business
{
  [PersistantObject(Schema = "FreightManagement")]
  public partial class RateTableCategory : RateTableColumn, IRateTableCategory //TableColumn<RateTable> 
  {
    #region Constructors

    public RateTableCategory()
    {
    }

    public RateTableCategory(IRateTableCategory category) 
      : this(category, true)
    {
    }

    public RateTableCategory(IRateTableCategory category, bool clone)
    {
      if (clone) GlobalIdentifier = category.GlobalIdentifier;
      StartUnit = category.StartUnit;
      IsFixedRate = category.IsFixedRate;
      IsPercentage = category.IsPercentage;
      CostComponentType = category.CostComponentType;
      ThereAfter = category.ThereAfter;
    }

    #endregion

    #region string Text

    public override string Text
    {
      get
      {
        string unit = Owner.UnitOfMeasure;
        if (IsMinimumCharge) return string.Format("Minimum\r\nCharge");
        if (IsPercentage) return string.Format("\u2265 {0}\r\n% of {1}", StartUnit, unit, CultureInfo.CurrentCulture.NumberFormat.CurrencySymbol);  //unit = string.Format("% of {0}", Owner.BillingUnit.ToString());
        if (IsFixedRate)
        {
          return string.Format("\u2265 {0} {1}\r\n{2} Fixed", StartUnit, unit, CultureInfo.CurrentCulture.NumberFormat.CurrencySymbol);
        }
        else
        {
          return string.Format("\u2265 {0} {1}\r\n{2}/{1}{3}", StartUnit, unit, CultureInfo.CurrentCulture.NumberFormat.CurrencySymbol, ThereAfter ? "\r\nThere After" : string.Empty);
        }
      }
    }

    #endregion

    #region int StartUnit

    public const string StartUnitProperty = "StartUnit";
    [DataMember(Name = StartUnitProperty, EmitDefaultValue = false)]
    int mStartUnit;
    [PersistantProperty]
    public int StartUnit
    {
      get { return mStartUnit; }
      set
      {
        if (IsMinimumCharge) return;
        SetProperty<int>(ref mStartUnit, value);
      }
    }

    #endregion

    #region bool IsFixedRate

    public const string IsFixedRateProperty = "IsFixedRate";
    [DataMember(Name = IsFixedRateProperty, EmitDefaultValue = false)]
    bool mIsFixedRate;
    [PersistantProperty]
    public bool IsFixedRate
    {
      get { return mIsFixedRate; }
      set
      {
        if (IsMinimumCharge) return;
        SetProperty<bool>(ref mIsFixedRate, value);
      }
    }

    #endregion

    #region bool IsPercentage

    public const string IsPercentageProperty = "IsPercentage";
    [DataMember(Name = IsPercentageProperty, EmitDefaultValue = false)]
    bool mIsPercentage;
    [PersistantProperty]
    public bool IsPercentage
    {
      get { return mIsPercentage; }
      set
      {
        if (IsMinimumCharge) return;
        SetProperty<bool>(ref mIsPercentage, value);
      }
    }

    #endregion

    #region bool IsMinimumCharge

    public const string IsMinimumChargeProperty = "IsMinimumCharge";
    [DataMember(Name = IsMinimumChargeProperty, EmitDefaultValue = false)]
    bool mIsMinimumCharge;
    [PersistantProperty]
    public bool IsMinimumCharge
    {
      get { return mIsMinimumCharge; }
      set
      {
        if (SetProperty<bool>(ref mIsMinimumCharge, value))
        {
          IsFixedRate = true;
          IsPercentage = false;
          StartUnit = 0;
        } 
      }
    }

    #endregion

    #region bool ThereAfter

    public const string ThereAfterProperty = "ThereAfter";
    [DataMember(Name = ThereAfterProperty, EmitDefaultValue = false)]
    bool mThereAfter;
    [PersistantProperty]
    public bool ThereAfter
    {
      get { return mThereAfter; }
      set { SetProperty<bool>(ref mThereAfter, value); }
    }

    #endregion

    #region WaybillCostComponentType CostComponentType

    public const string CostComponentTypeProperty = "CostComponentType";
    [PersistantProperty(IsCached = true)]
    public WaybillCostComponentType CostComponentType
    {
      get { return GetCachedObject<WaybillCostComponentType>(); }
      set { SetCachedObject<WaybillCostComponentType>(value); }
    }

    #endregion

    public override object Index
    {
      get
      {
        if (IsMinimumCharge) return "ZZZZZZ";
        return string.Format("ZZZZZZ - {0:000000}", StartUnit);
      }
    }
  }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Business
{
  public class Roles : Afx.Business.Security.Roles
  {
    public const string Accounts = "{51BA4D34-BAF4-4A62-A138-5C921CCE64A6}";
    public const string AccountsSupervisor = "{FA4F0D26-9897-4EF8-9EFB-7483D20691BA}";

    public const string Operations = "{AB874A6E-27CB-4356-AA26-F1FE05207FA3}";
    public const string OperationsSupervisor = "{A904EFC3-01B6-417E-86FC-ADE8AEA328C8}";

    public const string Dispatch = "{131835EF-A5CC-41AA-957B-FD29EC3EC94F}";
    public const string DispatchSupervisor = "{51B5957C-BDE1-4B8F-8A5F-4E049D263B97}";
  }
}

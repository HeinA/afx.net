﻿using Afx.Business;
using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Validation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace FreightManagement.Business
{
  [PersistantObject(Schema = "FreightManagement")]
  [Cache]
  public partial class Bay : BusinessObject, IRevisable 
  {
    #region Constructors

    public Bay()
    {
    }

    public Bay(bool isNull)
      : base(isNull)
    {
    }

    #endregion

    #region int Revision

    public const string RevisionProperty = "Revision";
    [DataMember(Name = RevisionProperty, EmitDefaultValue = false)]
    int mRevision;
    [PersistantProperty]
    public int Revision
    {
      get { return mRevision; }
      set { SetProperty<int>(ref mRevision, value); }
    }

    #endregion
    
    #region string Text

    public const string TextProperty = "Text";
    [DataMember(Name = TextProperty, EmitDefaultValue = false)]
    string mText;
    [PersistantProperty]
    [Mandatory("Text is mandatory.")]
    public string Text
    {
      get { return mText; }
      set { SetProperty<string>(ref mText, value); }
    }

    #endregion

    #region bool AllowDelivery

    public const string AllowDeliveryProperty = "AllowDelivery";
    [DataMember(Name = AllowDeliveryProperty, EmitDefaultValue = false)]
    bool mAllowDelivery;
    [PersistantProperty]
    public bool AllowDelivery
    {
      get { return mAllowDelivery; }
      set { SetProperty<bool>(ref mAllowDelivery, value); }
    }

    #endregion

    #region bool AllowTransfer

    public const string AllowTransferProperty = "AllowTransfer";
    [DataMember(Name = AllowTransferProperty, EmitDefaultValue = false)]
    bool mAllowTransfer;
    [PersistantProperty]
    public bool AllowTransfer
    {
      get { return mAllowTransfer; }
      set { SetProperty<bool>(ref mAllowTransfer, value); }
    }

    #endregion
  }
}

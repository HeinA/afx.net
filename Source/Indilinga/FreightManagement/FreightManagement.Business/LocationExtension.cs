﻿using Afx.Business;
using Afx.Business.Data;
using Geographics.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Business
{
  [PersistantObject(Schema = "FreightManagement")]
  [DataContract(IsReference = true, Namespace = FreightManagement.Business.Namespace.FreightManagement)]
  public class LocationExtension : ExtensionObject<Location>
  {
    #region decimal ImportVATRate

    public const string ImportVATRateProperty = "ImportVATRate";
    [DataMember(Name = ImportVATRateProperty, EmitDefaultValue = false)]
    decimal mImportVATRate;
    [PersistantProperty]
    public decimal ImportVATRate
    {
      get { return mImportVATRate; }
      set { SetProperty<decimal>(ref mImportVATRate, value); }
    }

    #endregion

    #region decimal StandardVATRate

    public const string StandardVATRateProperty = "StandardVATRate";
    [DataMember(Name = StandardVATRateProperty, EmitDefaultValue = false)]
    decimal mStandardVATRate;
    [PersistantProperty]
    public decimal StandardVATRate
    {
      get { return mStandardVATRate; }
      set { SetProperty<decimal>(ref mStandardVATRate, value); }
    }

    #endregion
  }
}

﻿using AccountManagement.Business;
using Afx.Business;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Documents;
using Afx.Business.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Business
{
  [PersistantObject(Schema = "FreightManagement")]
  public partial class PODHandover : Document
  {
    public PODHandover()
      : base(Cache.Instance.GetObject<DocumentType>(DocumentTypeIdentifier))
    {
    }

    #region Associative BusinessObjectCollection<WaybillReference> Waybills

    public const string WaybillsProperty = "Waybills";
    AssociativeObjectCollection<PODHandoverWaybill, WaybillReference> mWaybills;
    [PersistantCollection(AssociativeType = typeof(PODHandoverWaybill))]
    public BusinessObjectCollection<WaybillReference> Waybills
    {
      get
      {
        if (mWaybills == null) using (new EventStateSuppressor(this)) { mWaybills = new AssociativeObjectCollection<PODHandoverWaybill, WaybillReference>(this); }
        return mWaybills;
      }
    }

    #endregion

    protected override void OnCompositionChanged(CompositionChangedType compositionChangedType, string propertyName, object originalSource, BusinessObject item)
    {
      if ((compositionChangedType == CompositionChangedType.ChildAdded || compositionChangedType == CompositionChangedType.ChildRemoved) && propertyName == WaybillsProperty)
      {
        //RecalculateItems();
        //RecalculateWeight();
        if (Waybills.Count == 0)
        {
          Account = null;
        }
      }

      OnPropertyChanged(WaybillCountProperty);

      base.OnCompositionChanged(compositionChangedType, propertyName, originalSource, item);
    }

    #region int WaybillCount

    public const string WaybillCountProperty = "WaybillCount";
    [PersistantProperty(IgnoreConcurrency = true)]
    public int WaybillCount
    {
      get { return Waybills.Count; }
    }

    #endregion

    #region AccountReference Account

    public const string AccountProperty = "Account";
    [PersistantProperty(IsCached = true)]
    [Mandatory("Account is mandatory")]
    public AccountReference Account
    {
      get { return GetCachedObject<AccountReference>(); }
      set { SetCachedObject<AccountReference>(value); }
    }

    #endregion

    #region string Signature

    public const string SignatureProperty = "Signature";
    [DataMember(Name = SignatureProperty, EmitDefaultValue = false)]
    string mSignature;
    [PersistantProperty]
    public string Signature
    {
      get { return mSignature; }
      set { SetProperty<string>(ref mSignature, value); }
    }

    #endregion
  }
}

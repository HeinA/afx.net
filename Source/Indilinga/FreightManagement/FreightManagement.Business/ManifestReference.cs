﻿using Afx.Business;
using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Documents;
using Afx.Business.Security;
using Afx.Business.Validation;
using ContactManagement.Business;
using FleetManagement.Business;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.Serialization;

namespace FreightManagement.Business
{
  [PersistantObject(Schema = "FreightManagement", TableName = "vManifestReference", IsReadOnly = true)]
  public partial class ManifestReference : BusinessObject, IExcelRow
  {
    public static class ExportKeys
    {
      public const string DriverTrip = "DriverTrip";
    }

    #region Constructors

    public ManifestReference()
    {
    }

    #endregion




    #region bool IsChecked

    public const string IsCheckedProperty = "IsChecked";
    bool mIsChecked = false;
    public bool IsChecked
    {
      get { return mIsChecked; }
      set { SetProperty<bool>(ref mIsChecked, value); }
    }

    #endregion

    #region string ManifestNumber

    public const string ManifestNumberProperty = "ManifestNumber";
    [DataMember(Name = ManifestNumberProperty, EmitDefaultValue = false)]
    string mManifestNumber;
    [PersistantProperty]
    public string ManifestNumber
    {
      get { return mManifestNumber; }
      private set { SetProperty<string>(ref mManifestNumber, value); }
    }

    #endregion

    #region DateTime ManifestDate

    public const string ManifestDateProperty = "ManifestDate";
    [DataMember(Name = ManifestDateProperty, EmitDefaultValue = false)]
    DateTime mManifestDate;
    [PersistantProperty]
    public DateTime ManifestDate
    {
      get { return mManifestDate; }
      private set { SetProperty<DateTime>(ref mManifestDate, value); }
    }

    #endregion

    #region DocumentTypeState ManifestState

    public const string ManifestStateProperty = "ManifestState";
    [PersistantProperty(IsCached = true)]
    public DocumentTypeState ManifestState
    {
      get { return GetCachedObject<DocumentTypeState>(); }
      set { SetCachedObject<DocumentTypeState>(value); }
    }

    #endregion

    #region DocumentType ManifestType

    public const string ManifestTypeProperty = "ManifestType";
    [PersistantProperty(IsCached = true)]
    public DocumentType ManifestType
    {
      get { return GetCachedObject<DocumentType>(); }
      set { SetCachedObject<DocumentType>(value); }
    }

    #endregion

    #region Route Route

    public const string RouteProperty = "Route";
    [PersistantProperty(IsCached = true)]
    public Route Route
    {
      get { return GetCachedObject<Route>(); }
      set { SetCachedObject<Route>(value); }
    }

    #endregion

    #region OrganizationalUnit OrganizationalUnit

    public const string OrganizationalUnitProperty = "OrganizationalUnit";
    [DataMember(Name = OrganizationalUnitProperty, EmitDefaultValue = false)]
    OrganizationalUnit mOrganizationalUnit;
    [PersistantProperty]
    public OrganizationalUnit OrganizationalUnit
    {
      get { return mOrganizationalUnit; }
      set { SetProperty<OrganizationalUnit>(ref mOrganizationalUnit, value); }
    }

    #endregion

    #region DistributionCenter OriginDC

    public const string OriginDCProperty = "OriginDC";
    [DataMember(Name = OriginDCProperty, EmitDefaultValue = false)]
    DistributionCenter mOriginDC;
    [PersistantProperty]
    public DistributionCenter OriginDC
    {
      get { return mOriginDC; }
      set { SetProperty<DistributionCenter>(ref mOriginDC, value); }
    }

    #endregion

    #region DistributionCenter DestinationDC

    public const string DestinationDCProperty = "DestinationDC";
    [DataMember(Name = DestinationDCProperty, EmitDefaultValue = false)]
    DistributionCenter mDestinationDC;
    [PersistantProperty]
    public DistributionCenter DestinationDC
    {
      get { return mDestinationDC; }
      set { SetProperty<DistributionCenter>(ref mDestinationDC, value); }
    }

    #endregion

    #region string FullRoute

    public string FullRoute
    {
      get 
      { 
        if (Route != null)
        {
          return Route.Name;
        } 
        else
        {
          return String.Format("{0} -> {1}", OriginDC.Name, DestinationDC.Name);
        }
      }
    }

    #endregion

    #region Contact Driver

    public const string DriverProperty = "Driver";
    [DataMember(Name = DriverProperty, EmitDefaultValue = false)]
    Contact mDriver;
    [PersistantProperty]
    public Contact Driver
    {
      get { return mDriver; }
      set { SetProperty<Contact>(ref mDriver, value); }
    }

    #endregion

    #region Contact CoDriver

    public const string CoDriverProperty = "CoDriver";
    [DataMember(Name = CoDriverProperty, EmitDefaultValue = false)]
    Contact mCoDriver;
    [PersistantProperty]
    public Contact CoDriver
    {
      get { return mCoDriver; }
      set { SetProperty<Contact>(ref mCoDriver, value); }
    }

    #endregion

    #region Contact BackupDriver

    public const string BackupDriverProperty = "BackupDriver";
    [DataMember(Name = BackupDriverProperty, EmitDefaultValue = false)]
    Contact mBackupDriver;
    [PersistantProperty]
    public Contact BackupDriver
    {
      get { return mBackupDriver; }
      set { SetProperty<Contact>(ref mBackupDriver, value); }
    }

    #endregion

    #region Contractor Courier

    public const string CourierProperty = "Courier";
    [DataMember(Name = CourierProperty, EmitDefaultValue = false)]
    Contractor mCourier;
    [PersistantProperty]
    public Contractor Courier
    {
      get { return mCourier; }
      set { SetProperty<Contractor>(ref mCourier, value); }
    }

    #endregion

    #region int WaybillCount

    public const string WaybillCountProperty = "WaybillCount";
    [DataMember(Name = WaybillCountProperty, EmitDefaultValue = false)]
    int mWaybillCount;
    [PersistantProperty]
    public int WaybillCount
    {
      get { return mWaybillCount; }
      private set { SetProperty<int>(ref mWaybillCount, value); }
    }

    #endregion

    #region int Items

    public const string ItemsProperty = "Items";
    [DataMember(Name = ItemsProperty, EmitDefaultValue = false)]
    int mItems;
    [PersistantProperty]
    public int Items
    {
      get { return mItems; }
      private set { SetProperty<int>(ref mItems, value); }
    }

    #endregion

    #region decimal Weight

    public const string WeightProperty = "Weight";
    [DataMember(Name = WeightProperty, EmitDefaultValue = false)]
    decimal mWeight;
    [PersistantProperty]
    public decimal Weight
    {
      get { return mWeight; }
      private set { SetProperty<decimal>(ref mWeight, value); }
    }

    #endregion

    #region int TripsheetCount

    public const string TripsheetCountProperty = "TripsheetCount";
    [DataMember(Name = TripsheetCountProperty, EmitDefaultValue = false)]
    int mTripsheetCount;
    [PersistantProperty]
    public int TripsheetCount
    {
      get { return mTripsheetCount; }
      private set { SetProperty<int>(ref mTripsheetCount, value); }
    }

    #endregion

    #region Associative BusinessObjectCollection<Vehicle> Vehicles

    public const string VehiclesProperty = "Vehicles";
    AssociativeObjectCollection<DeliveryManifestVehicle, Vehicle> mVehicles;
    [PersistantCollection(AssociativeType = typeof(DeliveryManifestVehicle))]
    [Mandatory("Vehicles are mandatory")]
    public BusinessObjectCollection<Vehicle> Vehicles
    {
      get
      {
        if (mVehicles == null) using (new EventStateSuppressor(this)) { mVehicles = new AssociativeObjectCollection<DeliveryManifestVehicle, Vehicle>(this); }
        return mVehicles;
      }
    }

    #endregion


    #region Vehicle Horse

    public const string HorseProperty = "Horse";
    Vehicle mHorse;
    public Vehicle Horse
    {
      get
      {
        return Vehicles.Where(v => v.IsTruck.Equals(true)).FirstOrDefault();
      }
    }

    #endregion

    #region Vehicle Trailer1

    public const string Trailer1Property = "Trailer1";
    Vehicle mTrailer1;
    public Vehicle Trailer1
    {
      get
      {
        return Vehicles.Where(v => v.IsTrailer.Equals(true) && (Trailer2 != null && v != Trailer2)).FirstOrDefault();
      }
    }

    #endregion

    #region Vehicle Trailer2

    public const string Trailer2Property = "Trailer2";
    Vehicle mTrailer2;
    public Vehicle Trailer2
    {
      get
      {
        return Vehicles.Where(v => v.IsTruck.Equals(true) && (Trailer1 != null && v != Trailer1)).FirstOrDefault();
      }
    }

    #endregion



    #region decimal ChargeDocumentation

    public const string ChargeDocumentationProperty = "ChargeDocumentation";
    [DataMember(Name = ChargeDocumentationProperty, EmitDefaultValue = false)]
    decimal mChargeDocumentation;
    [PersistantProperty]
    public decimal ChargeDocumentation
    {
      get { return mChargeDocumentation; }
      set { SetProperty<decimal>(ref mChargeDocumentation, value); }
    }

    #endregion

		#region decimal ChargeTransport
		
		public const string ChargeTransportProperty = "ChargeTransport";
		[DataMember(Name = ChargeTransportProperty, EmitDefaultValue=false)]
		decimal mChargeTransport;
		[PersistantProperty]
		public decimal ChargeTransport
		{
		  get { return mChargeTransport; }
		  set { SetProperty<decimal>(ref mChargeTransport, value); }
		}
		
		#endregion

		#region decimal ChargeCollectionDelivery
		
		public const string ChargeCollectionDeliveryProperty = "ChargeCollectionDelivery";
		[DataMember(Name = ChargeCollectionDeliveryProperty, EmitDefaultValue=false)]
		decimal mChargeCollectionDelivery;
		[PersistantProperty]
		public decimal ChargeCollectionDelivery
		{
		  get { return mChargeCollectionDelivery; }
		  set { SetProperty<decimal>(ref mChargeCollectionDelivery, value); }
		}
		
		#endregion

		#region decimal ChargeFuelLevy
		
		public const string ChargeFuelLevyProperty = "ChargeFuelLevy";
		[DataMember(Name = ChargeFuelLevyProperty, EmitDefaultValue=false)]
		decimal mChargeFuelLevy;
		[PersistantProperty]
		public decimal ChargeFuelLevy
		{
		  get { return mChargeFuelLevy; }
		  set { SetProperty<decimal>(ref mChargeFuelLevy, value); }
		}
		
		#endregion

		#region decimal ChargeOther
		
		public const string ChargeOtherProperty = "ChargeOther";
		[DataMember(Name = ChargeOtherProperty, EmitDefaultValue=false)]
		decimal mChargeOther;
		[PersistantProperty]
		public decimal ChargeOther
		{
		  get { return mChargeOther; }
		  set { SetProperty<decimal>(ref mChargeOther, value); }
		}
		
		#endregion

		#region decimal ChargeDisbursementFee
		
		public const string ChargeDisbursementFeeProperty = "ChargeDisbursementFee";
		[DataMember(Name = ChargeDisbursementFeeProperty, EmitDefaultValue=false)]
		decimal mChargeDisbursementFee;
		[PersistantProperty]
		public decimal ChargeDisbursementFee
		{
		  get { return mChargeDisbursementFee; }
		  set { SetProperty<decimal>(ref mChargeDisbursementFee, value); }
		}
		
		#endregion

    #region decimal ChargeTotal

    public const string ChargeTotalProperty = "ChargeTotal";
    decimal mChargeTotal;
    public decimal ChargeTotal
    {
      get { return ChargeDocumentation + ChargeTransport + ChargeCollectionDelivery + ChargeFuelLevy + ChargeOther + ChargeDisbursementFee; }
    }

    #endregion

    #region decimal DeclaredValue

    public const string DeclaredValueProperty = "DeclaredValue";
    [DataMember(Name = DeclaredValueProperty, EmitDefaultValue = false)]
    decimal mDeclaredValue;
    [PersistantProperty]
    public decimal DeclaredValue
    {
      get { return mDeclaredValue; }
      set { SetProperty<decimal>(ref mDeclaredValue, value); }
    }

    #endregion

    public void ExportHeader(ClosedXML.Excel.IXLWorksheet sheet, int row, string exportKey)
    {
      int column = 1;
      sheet.Cell(row, column++).SetValue("Manifest Number").Style.Font.SetBold();
      sheet.Cell(row, column++).SetValue("Manifest State").Style.Font.SetBold();
      sheet.Cell(row, column++).SetValue("Manifest Type").Style.Font.SetBold();
      sheet.Cell(row, column++).SetValue("Waybill Date").Style.Font.SetBold();
      sheet.Cell(row, column++).SetValue("Delivery Date").Style.Font.SetBold();
      sheet.Cell(row, column++).SetValue("Account").Style.Font.SetBold();
      sheet.Cell(row, column++).SetValue("Consigner").Style.Font.SetBold();
      sheet.Cell(row, column++).SetValue("Consignee").Style.Font.SetBold();
      sheet.Cell(row, column++).SetValue("Collect From").Style.Font.SetBold();
      sheet.Cell(row, column++).SetValue("Deliver To").Style.Font.SetBold();
      sheet.Cell(row, column++).SetValue("Items").Style.Font.SetBold();
      sheet.Cell(row, column++).SetValue("Weight").Style.Font.SetBold();
      sheet.Cell(row, column++).SetValue("Declared Value").Style.Font.SetBold();
      if (SecurityContext.User.HasRole(Roles.Accounts, Roles.AccountsSupervisor, Roles.Administrator))
      {
        sheet.Cell(row, column++).SetValue("Documentation").Style.Font.SetBold();
        sheet.Cell(row, column++).SetValue("Transport").Style.Font.SetBold();
        sheet.Cell(row, column++).SetValue("VAT").Style.Font.SetBold();
        sheet.Cell(row, column++).SetValue("Other").Style.Font.SetBold();
        sheet.Cell(row, column++).SetValue("Total").Style.Font.SetBold();
      }
    }

    public void ExportRow(ClosedXML.Excel.IXLWorksheet sheet, int row, string exportKey)
    {
    }
  }
}

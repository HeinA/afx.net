﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Business
{
  public interface IRateTableCategory
  {
    Guid GlobalIdentifier { get; set; }
    //string Text { get; }
    int StartUnit { get; set; }
    bool IsFixedRate { get; set; }
    bool IsPercentage { get;set; }
    bool IsMinimumCharge { get; set; }
    WaybillCostComponentType CostComponentType { get; set; }
    bool ThereAfter { get; set; }
    bool IsDeleted { get; }
  }
}

﻿using Afx.Business;
using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Validation;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace FreightManagement.Business
{
  [PersistantObject(Schema = "FreightManagement", TableName = "vRatesConfigurationReference", IsReadOnly = true)]
  [Cache(Replicate = false)]
  public partial class RatesConfigurationReference : BusinessObject 
  {
    #region Constructors

    public RatesConfigurationReference()
    {
    }

    #endregion

    #region string Name

    public const string NameProperty = "Name";
    [DataMember(Name = NameProperty, EmitDefaultValue = false)]
    string mName;
    [PersistantProperty]
    public string Name
    {
      get { return mName; }
      set { SetProperty<string>(ref mName, value); }
    }

    #endregion

    #region bool IsStandardConfiguration

    public const string IsStandardConfigurationProperty = "IsStandardConfiguration";
    [DataMember(Name = IsStandardConfigurationProperty, EmitDefaultValue = false)]
    bool mIsStandardConfiguration;
    [PersistantProperty]
    public bool IsStandardConfiguration
    {
      get { return mIsStandardConfiguration; }
      set { SetProperty<bool>(ref mIsStandardConfiguration, value); }
    }

    #endregion

    #region BusinessObjectCollection<RatesConfigurationRevisionReference> Revisions

    public const string RevisionsProperty = "Revisions";
    BusinessObjectCollection<RatesConfigurationRevisionReference> mRevisions;
    [PersistantCollection]
    [DataMember]
    public BusinessObjectCollection<RatesConfigurationRevisionReference> Revisions
    {
      get { return mRevisions ?? (mRevisions = new BusinessObjectCollection<RatesConfigurationRevisionReference>(this)); }
    }

    #endregion

    public RatesConfigurationRevisionReference GetRevision(DateTime date)
    {
      return Revisions.Where(r => r.EffectiveDate.Date <= date).OrderByDescending(r => r.EffectiveDate).FirstOrDefault();
    }

    public int GetVolumetricFactor(Waybill waybill)
    {
      var revision = GetRevision(waybill.DocumentDate);
      if (revision == null) throw new InvalidOperationException("The Selected Rates Configuration does not have a revision for this date.");
      var cf = waybill.WaybillFlags.FirstOrDefault(cf1 => cf1.Owner.IsFlagged(WaybillFlagGroupFlags.CostingFlagGroup)); //.IsCostingFlag
      if (cf != null)
      {
        var fp = revision.GetAssociativeObject<RatesConfigurationRevisionReferenceCostingFlag>(cf);
        if (fp != null)
        {
          return fp.VolumetricFactor;
        }
      }
      return revision.VolumetricFactor;
    }
  }
}

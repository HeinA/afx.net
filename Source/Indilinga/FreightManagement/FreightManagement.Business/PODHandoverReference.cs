﻿using AccountManagement.Business;
using Afx.Business;
using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Documents;
using Afx.Business.Validation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;



namespace FreightManagement.Business
{
  [PersistantObject(Schema = "FreightManagement", TableName = "vPODHandoverReference", IsReadOnly = true)]
  public class PODHandoverReference : BusinessObject 
  {
    #region Constructors

    public PODHandoverReference()
    {
    }

    #endregion

    #region string PODHandoverNumber
    
    public const string PODHandoverNumberProperty = "PODHandoverNumber";
    [DataMember(Name = PODHandoverNumberProperty, EmitDefaultValue=false)]
    string mPODHandoverNumber;
    [PersistantProperty]
    public string PODHandoverNumber
    {
      get { return mPODHandoverNumber; }
      set { SetProperty<string>(ref mPODHandoverNumber, value); }
    }
    
    #endregion

    #region DateTime PODHandoverDate

    public const string PODHandoverDateProperty = "PODHandoverDate";
    [DataMember(Name = PODHandoverDateProperty, EmitDefaultValue = false)]
    DateTime mPODHandoverDate;
    [PersistantProperty]
    public DateTime PODHandoverDate
    {
      get { return mPODHandoverDate; }
      set { SetProperty<DateTime>(ref mPODHandoverDate, value); }
    }

    #endregion

    #region DocumentTypeState PODHandoverState

    public const string PODHandoverStateProperty = "PODHandoverState";
    [PersistantProperty(IsCached = true)]
    public DocumentTypeState PODHandoverState
    {
      get { return GetCachedObject<DocumentTypeState>(); }
      set { SetCachedObject<DocumentTypeState>(value); }
    }

    #endregion


    #region int WaybillCount

    public const string WaybillCountProperty = "WaybillCount";
    [DataMember(Name = WaybillCountProperty, EmitDefaultValue = false)]
    int mWaybillCount;
    [PersistantProperty]
    public int WaybillCount
    {
      get { return mWaybillCount; }
      private set { SetProperty<int>(ref mWaybillCount, value); }
    }

    #endregion

    #region AccountReference Account

    public const string AccountProperty = "Account";
    [PersistantProperty(IsCached = true)]
    public AccountReference Account
    {
      get { return GetCachedObject<AccountReference>(); }
      set { SetCachedObject<AccountReference>(value); }
    }

    #endregion

    //#region int PODHandoverCount

    //public const string PODHandoverCountProperty = "PODHandoverCount";
    //[DataMember(Name = PODHandoverCountProperty, EmitDefaultValue = false)]
    //int mPODHandoverCount;
    //[PersistantProperty]
    //public int PODHandoverCount
    //{
    //  get { return mPODHandoverCount; }
    //  set { SetProperty<int>(ref mPODHandoverCount, value); }
    //}

    //#endregion
  }
}

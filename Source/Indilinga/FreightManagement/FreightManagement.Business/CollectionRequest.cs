﻿using AccountManagement.Business;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Documents;
using Afx.Business.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Business
{
  [PersistantObject(Schema = "FreightManagement")]
  public partial class CollectionRequest : Document
  {
    public CollectionRequest()
      : base(Cache.Instance.GetObject<DocumentType>(DocumentTypeIdentifier))
    {
    }

    #region AccountReference Account

    public const string AccountProperty = "Account";
    [PersistantProperty(IsCached = true)]
    public AccountReference Account
    {
      get { return GetCachedObject<AccountReference>(); }
      set { SetCachedObject<AccountReference>(value); }
    }

    #endregion

    #region string InvoiceTo

    public const string InvoiceToProperty = "InvoiceTo";
    [DataMember(Name = InvoiceToProperty, EmitDefaultValue = false)]
    string mInvoiceTo;
    [Mandatory("Invoice To is mandatory")]
    [PersistantProperty]
    public string InvoiceTo
    {
      get { return mInvoiceTo; }
      set { SetProperty<string>(ref mInvoiceTo, value); }
    }

    #endregion

    #region string Consigner

    public const string ConsignerProperty = "Consigner";
    [DataMember(Name = ConsignerProperty, EmitDefaultValue = false)]
    string mConsigner;
    [Mandatory("Consigner is mandatory")]
    [PersistantProperty]
    public string Consigner
    {
      get { return mConsigner; }
      set { SetProperty<string>(ref mConsigner, value); }
    }

    #endregion

    #region string ConsignerAddress

    public const string ConsignerAddressProperty = "ConsignerAddress";
    [DataMember(Name = ConsignerAddressProperty, EmitDefaultValue = false)]
    string mConsignerAddress;
    [Mandatory("Consigner Address is mandatory")]
    [PersistantProperty]
    public string ConsignerAddress
    {
      get { return mConsignerAddress; }
      set { SetProperty<string>(ref mConsignerAddress, value); }
    }

    #endregion

    #region string Consignee

    public const string ConsigneeProperty = "Consignee";
    [DataMember(Name = ConsigneeProperty, EmitDefaultValue = false)]
    string mConsignee;
    [PersistantProperty]
    [Mandatory("Consignee is mandatory")]
    public string Consignee
    {
      get { return mConsignee; }
      set { SetProperty<string>(ref mConsignee, value); }
    }

    #endregion

    #region string ConsigneeAddress

    public const string ConsigneeAddressProperty = "ConsigneeAddress";
    [DataMember(Name = ConsigneeAddressProperty, EmitDefaultValue = false)]
    string mConsigneeAddress;
    [PersistantProperty]
    [Mandatory("Consignee Address is mandatory.")]
    public string ConsigneeAddress
    {
      get { return mConsigneeAddress; }
      set { SetProperty<string>(ref mConsigneeAddress, value); }
    }

    #endregion

    #region bool IsInsured

    public const string IsInsuredProperty = "IsInsured";
    [DataMember(Name = IsInsuredProperty, EmitDefaultValue = false)]
    bool mIsInsured;
    [PersistantProperty]
    public bool IsInsured
    {
      get { return mIsInsured; }
      set { SetProperty<bool>(ref mIsInsured, value); }
    }

    #endregion

    #region BusinessObjectCollection<CollectionRequestItem> Items

    public const string ItemsProperty = "Items";
    BusinessObjectCollection<CollectionRequestItem> mItems;
    [PersistantCollection]
    [DataMember]
    public BusinessObjectCollection<CollectionRequestItem> Items
    {
      get { return mItems ?? (mItems = new BusinessObjectCollection<CollectionRequestItem>(this)); }
    }

    #endregion
  }
}

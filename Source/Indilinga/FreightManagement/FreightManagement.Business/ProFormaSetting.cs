﻿using Afx.Business;
using Afx.Business.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Business
{
  [PersistantObject(Schema = "FreightManagement")]
  [Export(typeof(Setting))]
  [DataContract(IsReference = true, Namespace = FreightManagement.Business.Namespace.FreightManagement)]
  public class ProFormaSetting : Setting
  {
    #region string Name

    public override string Name
    {
      get { return "Pro Forma Invoice"; }
    }

    #endregion

    #region string SettingGroupIdentifier

    protected override string SettingGroupIdentifier
    {
      get { return "46F1A0E8-072E-4EDA-9217-56DBC928E9D5"; }
    }

    #endregion

    #region string HeaderText

    public const string HeaderTextProperty = "HeaderText";
    [DataMember(Name = HeaderTextProperty, EmitDefaultValue = false)]
    string mHeaderText;
    [PersistantProperty]
    public string HeaderText
    {
      get { return mHeaderText; }
      set { SetProperty<string>(ref mHeaderText, value); }
    }

    #endregion

    #region string FooterText

    public const string FooterTextProperty = "FooterText";
    [DataMember(Name = FooterTextProperty, EmitDefaultValue = false)]
    string mFooterText;
    [PersistantProperty]
    public string FooterText
    {
      get { return mFooterText; }
      set { SetProperty<string>(ref mFooterText, value); }
    }

    #endregion
  }
}

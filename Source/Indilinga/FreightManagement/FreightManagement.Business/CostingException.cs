﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Business
{
  [Serializable]
  public class CostingException : Exception
  {
    protected CostingException(SerializationInfo info, StreamingContext context)
      :base(info, context)
    {
    }

    public CostingException(string message)
      : base(message)
    {
    }

    public CostingException(string message, Exception innerException)
      : base(message, innerException)
    {
    }    
  }
}

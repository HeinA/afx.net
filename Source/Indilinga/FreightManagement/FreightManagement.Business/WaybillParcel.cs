﻿using Afx.Business;
using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Validation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace FreightManagement.Business
{
  [PersistantObject(Schema = "FreightManagement")]
  public partial class WaybillParcel : BusinessObject<Waybill> 
  {
    #region Constructors

    public WaybillParcel()
    {
    }

    #endregion

    #region string Barcode

    public const string BarcodeProperty = "Barcode";
    [DataMember(Name = BarcodeProperty, EmitDefaultValue = false)]
    string mBarcode;
    [PersistantProperty]
    public string Barcode
    {
      get { return mBarcode; }
      set { SetProperty<string>(ref mBarcode, value); }
    }

    #endregion

    #region bool IsIgnored

    public const string IsIgnoredProperty = "IsIgnored";
    [DataMember(Name = IsIgnoredProperty, EmitDefaultValue = false)]
    bool mIsIgnored;
    [PersistantProperty]
    public bool IsIgnored
    {
      get { return mIsIgnored; }
      set { SetProperty<bool>(ref mIsIgnored, value); }
    }

    #endregion
  }
}

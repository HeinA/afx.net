﻿using Afx.Prism;
using Afx.Prism.RibbonMdi;
using ContactManagement.Business;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContactManagement.Prism
{
  [Export(typeof(IModuleInfo))]
  public class ModuleInfo : IModuleInfo
  {
    public string ModuleNamespace
    {
      get { return Namespace.ContactManagement; }
    }

    public string[] ModuleDependencies
    {
      get { return new string[] { Afx.Business.Namespace.Afx }; }
    }

    public Type ModuleController
    {
      get { return typeof(ContactManagementModuleController); }
    }
  }
}
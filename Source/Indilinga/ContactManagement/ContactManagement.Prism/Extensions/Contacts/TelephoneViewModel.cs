﻿using Afx.Business.Data;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Documents;
using ContactManagement.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContactManagement.Prism.Extensions.Contacts
{
  public class TelephoneViewModel : ExtensionViewModel<ITelephoneCollection>
  {
    #region Constructors

    [InjectionConstructor]
    public TelephoneViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    public string Title
    {
      get { return "Telephone Numbers"; }
    }

    IEnumerable<TelephoneType> mTelephoneTypes;
    public IEnumerable<TelephoneType> TelephoneTypes
    {
      get { return mTelephoneTypes ?? (mTelephoneTypes = Cache.Instance.GetObjects<TelephoneType>(true, t => t.Text, true)); }
      set { SetProperty<IEnumerable<TelephoneType>>(ref mTelephoneTypes, value); }
    }
  }
}

﻿using Afx.Business.Data;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Documents;
using ContactManagement.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContactManagement.Prism.Extensions.Contacts
{
  public class AddressViewModel : ExtensionViewModel<IAddressCollection>
  {
    #region Constructors

    [InjectionConstructor]
    public AddressViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    public string Title
    {
      get { return "Addresses"; }
    }

    IEnumerable<AddressType> mAddressTypes;
    public IEnumerable<AddressType> AddressTypes
    {
      get { return mAddressTypes ?? (mAddressTypes = Cache.Instance.GetObjects<AddressType>(true, t => t.Text, true)); }
      set { SetProperty<IEnumerable<AddressType>>(ref mAddressTypes, value); }
    }
  }
}

﻿using Afx.Business.Data;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Documents;
using ContactManagement.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContactManagement.Prism.Extensions.Contacts
{
  public class SimpleContactViewModel : ExtensionViewModel<ISimpleContactCollection>
  {
    #region Constructors

    [InjectionConstructor]
    public SimpleContactViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    public string Title
    {
      get { return "Contacts"; }
    }

    IEnumerable<ContactType> mContactTypes;
    public IEnumerable<ContactType> ContactTypes
    {
      get { return mContactTypes ?? (mContactTypes = Cache.Instance.GetObjects<ContactType>(true, t => t.Name, true)); }
      set { SetProperty<IEnumerable<ContactType>>(ref mContactTypes, value); }
    }  
  }
}

﻿using Afx.Business.Security;
using Afx.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Activities;
using ContactManagement.Business;
using Microsoft.Practices.Prism.Commands;
using Afx.Prism.RibbonMdi.Dialogs.EditFlags;

namespace ContactManagement.Prism.Activities.AddressTypeManagement
{
  public partial class AddressTypeManagementViewModel : MdiSimpleActivityViewModel<AddressTypeManagement>
  {
    #region Constructors

    [InjectionConstructor]
    public AddressTypeManagementViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    #region DelegateCommand<AddressType> EditFlagsCommand

    DelegateCommand<AddressType> mEditFlagsCommand;
    public DelegateCommand<AddressType> EditFlagsCommand
    {
      get { return mEditFlagsCommand ?? (mEditFlagsCommand = new DelegateCommand<AddressType>(ExecuteEditFlags, CanExecuteEditFlags)); }
    }

    bool CanExecuteEditFlags(AddressType args)
    {
      return true;
    }

    void ExecuteEditFlags(AddressType args)
    {
      EditFlagsDialogController efc = Controller.CreateChildController<EditFlagsDialogController>();
      efc.DataContext = new EditFlagsDialogContext() { TargetObject = args };
      efc.Run();
    }

    #endregion
  }
}

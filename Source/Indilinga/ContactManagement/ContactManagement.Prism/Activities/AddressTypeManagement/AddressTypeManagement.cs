﻿using Afx.Business.Activities;
using ContactManagement.Business;
using ContactManagement.Business.Service;
using Afx.Prism.RibbonMdi.Activities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContactManagement.Prism.Activities.AddressTypeManagement
{
  public partial class AddressTypeManagement : SimpleActivityContext<AddressType>
  {
    public AddressTypeManagement()
    {
    }

    public AddressTypeManagement(ContextualActivity activity)
      : base(activity)
    {
    }
  }
}

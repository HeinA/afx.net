﻿using ContactManagement.Business;
using ContactManagement.Business.Service;
using Afx.Business.Service;
using Afx.Prism.RibbonMdi.Activities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.Composition;
using Afx.Business.Security;

namespace ContactManagement.Prism.Activities.AddressTypeManagement
{
  //**************************************************************************************************************
  //**************************************************************************************************************
  //*                                                                                                            *
  //*     This code is auto-generated.  Do not place any cutom code here, it will be lost on re-generation.      *
  //*                                                                                                            *
  //**************************************************************************************************************
  //**************************************************************************************************************
  [Export("Context:" + AddressTypeManagement.Key)]
  public partial class AddressTypeManagement
  {
    public const string Key = "{c011a137-14af-4c6d-a542-c74b9ca11b5f}";

    public override void LoadData()
    {
      using (var svc = ProxyFactory.GetService<IContactManagementService>(SecurityContext.MasterServer.Name))
      {
        Data = svc.LoadAddressTypeCollection();
      }
      base.LoadData();
    }

    public override void SaveData()
    {
      using (var svc = ProxyFactory.GetService<IContactManagementService>(SecurityContext.MasterServer.Name))
      {
        Data = svc.SaveAddressTypeCollection(Data);
      }
      base.SaveData();
    }
  }
}

﻿using Afx.Business.Security;
using Afx.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Activities;
using ContactManagement.Business;

namespace ContactManagement.Prism.Activities.TelephoneTypeManagement
{
  public partial class TelephoneTypeManagementViewModel : MdiSimpleActivityViewModel<TelephoneTypeManagement>
  {
    #region Constructors

    [InjectionConstructor]
    public TelephoneTypeManagementViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion
  }
}

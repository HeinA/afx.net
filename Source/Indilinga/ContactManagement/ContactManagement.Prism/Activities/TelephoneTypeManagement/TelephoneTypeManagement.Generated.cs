﻿using ContactManagement.Business;
using ContactManagement.Business.Service;
using Afx.Business.Service;
using Afx.Prism.RibbonMdi.Activities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.Composition;
using Afx.Business.Security;

namespace ContactManagement.Prism.Activities.TelephoneTypeManagement
{
  //**************************************************************************************************************
  //**************************************************************************************************************
  //*                                                                                                            *
  //*     This code is auto-generated.  Do not place any cutom code here, it will be lost on re-generation.      *
  //*                                                                                                            *
  //**************************************************************************************************************
  //**************************************************************************************************************
  [Export("Context:" + TelephoneTypeManagement.Key)]
  public partial class TelephoneTypeManagement
  {
    public const string Key = "{1a49ca69-918b-4df6-b40f-21cc822da5dc}";

    public override void LoadData()
    {
      using (var svc = ProxyFactory.GetService<IContactManagementService>(SecurityContext.MasterServer.Name))
      {
        Data = svc.LoadTelephoneTypeCollection();
      }
      base.LoadData();
    }

    public override void SaveData()
    {
      using (var svc = ProxyFactory.GetService<IContactManagementService>(SecurityContext.MasterServer.Name))
      {
        Data = svc.SaveTelephoneTypeCollection(Data);
      }
      base.SaveData();
    }
  }
}

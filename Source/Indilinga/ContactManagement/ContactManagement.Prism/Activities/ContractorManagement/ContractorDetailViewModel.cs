﻿using Afx.Business;
using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Activities;
using ContactManagement.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContactManagement.Prism.Activities.ContractorManagement
{
  public partial class ContractorDetailViewModel : ActivityDetailViewModel<Contractor>
  {
    #region Constructors

    [InjectionConstructor]
    public ContractorDetailViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    #region IEnumerable<ContractorType> ContractorTypes

    static Collection<ContractorType> mContractorTypes;
    public IEnumerable<ContractorType> ContractorTypes
    {
      get
      {
        return mContractorTypes ?? (mContractorTypes = Cache.Instance.GetObjects<ContractorType>(true, ct => ct.Name, true));
      }
    }

    #endregion
  }
}

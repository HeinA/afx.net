﻿using Afx.Business;
using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using ContactManagement.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContactManagement.Prism.Activities.ContractorManagement
{
  public partial class ContractorItemViewModel : MdiNavigationListItemViewModel<Contractor>
  {
    #region Constructors

    [InjectionConstructor]
    public ContractorItemViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    #region string CompanyName

    public string CompanyName
    {
      get
      {
        if (Model == null) return string.Empty;
        if (string.IsNullOrWhiteSpace(Model.CompanyName)) return "*** Unnamed ***";
        return Model.CompanyName;
      }
    }

    #endregion
  }
}

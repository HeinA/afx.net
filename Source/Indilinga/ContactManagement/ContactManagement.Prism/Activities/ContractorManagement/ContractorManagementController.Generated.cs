﻿using ContactManagement.Business;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.Composition;
using Microsoft.Practices.Prism;

namespace ContactManagement.Prism.Activities.ContractorManagement
{
  //**************************************************************************************************************
  //**************************************************************************************************************
  //*                                                                                                            *
  //*     This code is auto-generated.  Do not place any cutom code here, it will be lost on re-generation.      *
  //*                                                                                                            *
  //**************************************************************************************************************
  //**************************************************************************************************************
  [Export("Controller:" + global::ContactManagement.Prism.Activities.ContractorManagement.ContractorManagement.Key)]
  public partial class ContractorManagementController
  {
    void CreateRootItem()
    {
      Contractor o = new Contractor();
      DataContext.Data.Add(o);
      SelectContextViewModel(o);
      FocusViewModel<ContractorDetailViewModel>(o);
    }

    public void AddDetailsViewModel(object viewModel)
    {
      RegionManager.Regions[ContractorManagement.DetailsRegion].Add(viewModel);
    }

    public void AddTabViewModel(object viewModel)
    {
      IRegion tabRegion = RegionManager.Regions[ContractorManagement.TabRegion];
      tabRegion.Add(viewModel);
      IActiveAware aw = viewModel as IActiveAware;
      if (aw != null && aw.IsActive) tabRegion.Activate(viewModel);
    }
  }
}

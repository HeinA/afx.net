﻿using Afx.Business.Activities;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Business.Service;
using ContactManagement.Business;
using ContactManagement.Business.Service;
using Afx.Prism.RibbonMdi.Activities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContactManagement.Prism.Activities.ContractorManagement
{
  public partial class ContractorManagement : ContextualActivityContext<Contractor>
  {
    public const string DetailsRegion = "DetailsRegion";
    public const string TabRegion = "TabRegion";


    public ContractorManagement()
    {
    }

    public ContractorManagement(ContextualActivity activity)
      : base(activity)
    {
    }
  }
}

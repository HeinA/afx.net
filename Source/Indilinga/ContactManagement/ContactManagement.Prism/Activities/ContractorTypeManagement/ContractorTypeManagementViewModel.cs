﻿using Afx.Business.Security;
using Afx.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Activities;
using ContactManagement.Business;
using Microsoft.Practices.Prism.Commands;
using Afx.Prism.RibbonMdi.Dialogs.EditFlags;

namespace ContactManagement.Prism.Activities.ContractorTypeManagement
{
  public partial class ContractorTypeManagementViewModel : MdiSimpleActivityViewModel<ContractorTypeManagement>
  {
    #region Constructors

    [InjectionConstructor]
    public ContractorTypeManagementViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    #region DelegateCommand<ContractorType> EditFlagsCommand

    DelegateCommand<ContractorType> mEditFlagsCommand;
    public DelegateCommand<ContractorType> EditFlagsCommand
    {
      get { return mEditFlagsCommand ?? (mEditFlagsCommand = new DelegateCommand<ContractorType>(ExecuteEditFlags, CanExecuteEditFlags)); }
    }

    bool CanExecuteEditFlags(ContractorType args)
    {
      return true;
    }

    void ExecuteEditFlags(ContractorType args)
    {
      EditFlagsDialogController efc = Controller.CreateChildController<EditFlagsDialogController>();
      efc.DataContext = new EditFlagsDialogContext() { TargetObject = args };
      efc.Run();
    }

    #endregion
  }
}

﻿using ContactManagement.Business;
using ContactManagement.Business.Service;
using Afx.Business.Service;
using Afx.Prism.RibbonMdi.Activities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.Composition;
using Afx.Business.Security;

namespace ContactManagement.Prism.Activities.ContractorTypeManagement
{
  //**************************************************************************************************************
  //**************************************************************************************************************
  //*                                                                                                            *
  //*     This code is auto-generated.  Do not place any cutom code here, it will be lost on re-generation.      *
  //*                                                                                                            *
  //**************************************************************************************************************
  //**************************************************************************************************************
  [Export("Context:" + ContractorTypeManagement.Key)]
  public partial class ContractorTypeManagement
  {
    public const string Key = "{38d9b172-6e7b-4c8e-a926-e433b93b2e8d}";

    public override void LoadData()
    {
      using (var svc = ProxyFactory.GetService<IContactManagementService>(SecurityContext.MasterServer.Name))
      {
        Data = svc.LoadContractorTypeCollection();
      }
      base.LoadData();
    }

    public override void SaveData()
    {
      using (var svc = ProxyFactory.GetService<IContactManagementService>(SecurityContext.MasterServer.Name))
      {
        Data = svc.SaveContractorTypeCollection(Data);
      }
      base.SaveData();
    }
  }
}

﻿using Afx.Business.Activities;
using ContactManagement.Business;
using ContactManagement.Business.Service;
using Afx.Prism.RibbonMdi.Activities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContactManagement.Prism.Activities.ContractorTypeManagement
{
  public partial class ContractorTypeManagement : SimpleActivityContext<ContractorType>
  {
    public ContractorTypeManagement()
    {
    }

    public ContractorTypeManagement(Activity activity)
      : base(activity)
    {
    }
  }
}

﻿using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Afx.Prism.RibbonMdi.Activities;
using ContactManagement.Business;

namespace ContactManagement.Prism.Activities.ContactTypeManagement
{
  //**************************************************************************************************************
  //**************************************************************************************************************
  //*                                                                                                            *
  //*     This code is auto-generated.  Do not place any cutom code here, it will be lost on re-generation.      *
  //*                                                                                                            *
  //**************************************************************************************************************
  //**************************************************************************************************************
  public partial class ContactTypeManagementViewModel
  {
    public IEnumerable<ContactType> Items
    {
      get
      {
        if (Model == null) return null;
        return Model.Data;
      }
    }
  }
}

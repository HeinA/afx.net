﻿using Afx.Business.Activities;
using ContactManagement.Business;
using ContactManagement.Business.Service;
using Afx.Prism.RibbonMdi.Activities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContactManagement.Prism.Activities.ContactTypeManagement
{
  public partial class ContactTypeManagement : SimpleActivityContext<ContactType>
  {
    public ContactTypeManagement()
    {
    }

    public ContactTypeManagement(ContextualActivity activity)
      : base(activity)
    {
    }
  }
}

﻿using Afx.Business.Security;
using Afx.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Activities;
using ContactManagement.Business;
using Microsoft.Practices.Prism.Commands;
using Afx.Prism.RibbonMdi.Dialogs.EditFlags;

namespace ContactManagement.Prism.Activities.ContactTypeManagement
{
  public partial class ContactTypeManagementViewModel : MdiSimpleActivityViewModel<ContactTypeManagement>
  {
    #region Constructors

    [InjectionConstructor]
    public ContactTypeManagementViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    #region DelegateCommand<ContactType> EditFlagsCommand

    DelegateCommand<ContactType> mEditFlagsCommand;
    public DelegateCommand<ContactType> EditFlagsCommand
    {
      get { return mEditFlagsCommand ?? (mEditFlagsCommand = new DelegateCommand<ContactType>(ExecuteEditFlags, CanExecuteEditFlags)); }
    }

    bool CanExecuteEditFlags(ContactType args)
    {
      return true;
    }

    void ExecuteEditFlags(ContactType args)
    {
      EditFlagsDialogController efc = Controller.CreateChildController<EditFlagsDialogController>();
      efc.DataContext = new EditFlagsDialogContext() { TargetObject = args };
      efc.Run();
    }

    #endregion

  }
}

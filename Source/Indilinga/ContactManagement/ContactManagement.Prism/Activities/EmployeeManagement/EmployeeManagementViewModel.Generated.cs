﻿using Afx.Business.Security;
using Afx.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Afx.Prism.RibbonMdi.Activities;
using ContactManagement.Business;
using Microsoft.Practices.Prism;

namespace ContactManagement.Prism.Activities.EmployeeManagement
{
  //**************************************************************************************************************
  //**************************************************************************************************************
  //*                                                                                                            *
  //*     This code is auto-generated.  Do not place any cutom code here, it will be lost on re-generation.      *
  //*                                                                                                            *
  //**************************************************************************************************************
  //**************************************************************************************************************
  public partial class EmployeeManagementViewModel
  {
    public IEnumerable<EmployeeItemViewModel> Items
    {
      get
      {
        if (Model == null) return null;
        return this.ResolveViewModels<EmployeeItemViewModel, Employee>(Model.Data);
      }
    }
  }
}

﻿using Afx.Business;
using Afx.Business.Activities;
using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Activities;
using ContactManagement.Business;
using ContactManagement.Prism.Extensions.Contacts;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DocumentManagement.Business;
using DocumentManagement.Prism.Extensions.DocumentRepository;
using Microsoft.Win32;
using Afx.Business.Service;
using ContactManagement.Business.Service;
using System.IO;

namespace ContactManagement.Prism.Activities.EmployeeManagement
{
  public partial class EmployeeManagementController : MdiContextualActivityController<EmployeeManagement, EmployeeManagementViewModel>
  {
    #region Constructors

    [InjectionConstructor]
    public EmployeeManagementController(IController controller)
      : base(controller)
    {
    }

    #endregion

    #region OnLoaded
    
    protected override void OnLoaded()
    {
      if (DataContext.Data.Count > 0)
      {
        //Focus the first element 
        EmployeeItemViewModel ivm = GetCreateViewModel<EmployeeItemViewModel>(DataContext.Data[0], ViewModel);
        ivm.IsFocused = true;
      }

      RegisterContextRegion(RegionManager.Regions[EmployeeManagement.DetailsRegion]);
      RegisterContextRegion(RegionManager.Regions[EmployeeManagement.TabRegion]);

      base.OnLoaded();
    }

    #endregion
    
    #region OnContextViewModelChanged
    
    protected override void OnContextViewModelChanged(ISelectableViewModel context)
    {
      if (context.Model is Employee)
      {
        EmployeeDetailViewModel vm = GetCreateViewModel<EmployeeDetailViewModel>(context.Model, ViewModel);
        AddDetailsViewModel(vm);

        AddressViewModel avm = GetCreateViewModel<AddressViewModel>(context.Model, ViewModel);
        AddTabViewModel(avm);

        TelephoneViewModel tvm = GetCreateViewModel<TelephoneViewModel>(context.Model, ViewModel);
        AddTabViewModel(tvm);
      }

      base.OnContextViewModelChanged(context);
    }

    #endregion
    
    #region ExecuteOperation
    
    protected override void ExecuteOperation(Operation op, BusinessObject argument)
    {
      switch (op.Identifier)
      {
        case Operations.AddEmployee:
          CreateRootItem();
          break;

        case Operations.ImportEmployees:
          OpenFileDialog ofd = new OpenFileDialog();
          ofd.Filter = "CSV Files|*.csv";
          if (ofd.ShowDialog() != true) return;
          using (var svc = ProxyFactory.GetService<IContactManagementService>(SecurityContext.ServerName))
          {
            DataContext.Data = svc.ImportEmployees(File.ReadAllBytes(ofd.FileName));
            DataContext.IsDirty = false;
            DataContextUpdated();
          }
          break;
      }

      base.ExecuteOperation(op, argument);
    }

    #endregion
  }
}

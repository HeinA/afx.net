﻿using Afx.Business.Activities;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Business.Service;
using ContactManagement.Business;
using ContactManagement.Business.Service;
using Afx.Prism.RibbonMdi.Activities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.Composition;
  
namespace ContactManagement.Prism.Activities.EmployeeManagement
{
  //**************************************************************************************************************
  //**************************************************************************************************************
  //*                                                                                                            *
  //*     This code is auto-generated.  Do not place any cutom code here, it will be lost on re-generation.      *
  //*                                                                                                            *
  //**************************************************************************************************************
  //**************************************************************************************************************
  [Export("Context:" + EmployeeManagement.Key)]
  public partial class EmployeeManagement
  {
    public const string Key = "{bd3b1156-8864-415f-8bc2-f7fa06a48a23}";

    public override void LoadData()
    {
      using (var svc = ProxyFactory.GetService<IContactManagementService>(SecurityContext.MasterServer.Name))
      {
        Data = svc.LoadEmployeeCollection();
      }
      base.LoadData();
    }

    public override void SaveData()
    {
      using (var svc = ProxyFactory.GetService<IContactManagementService>(SecurityContext.MasterServer.Name))
      {
        Data = svc.SaveEmployeeCollection(Data);
      }
      base.SaveData();
    }
  }
}

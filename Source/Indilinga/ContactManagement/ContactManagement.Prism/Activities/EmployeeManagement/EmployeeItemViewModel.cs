﻿using Afx.Business;
using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using ContactManagement.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContactManagement.Prism.Activities.EmployeeManagement
{
  public partial class EmployeeItemViewModel : MdiNavigationListItemViewModel<Employee>
  {
    #region Constructors

    [InjectionConstructor]
    public EmployeeItemViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    #region string Lastname

    public string Lastname
    {
      get
      {
        if (Model == null) return string.Empty;
        if (string.IsNullOrWhiteSpace(Model.Lastname)) return "*** Unnamed ***";
        return Model.Lastname;
      }
    }

    #endregion
  }
}

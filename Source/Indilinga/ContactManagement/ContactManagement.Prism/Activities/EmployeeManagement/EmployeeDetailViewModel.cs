﻿using Afx.Business;
using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Activities;
using ContactManagement.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContactManagement.Prism.Activities.EmployeeManagement
{
  public partial class EmployeeDetailViewModel : ActivityDetailViewModel<Employee>
  {
    #region Constructors

    [InjectionConstructor]
    public EmployeeDetailViewModel(IController controller)
      : base(controller)
    {
    }
    
    #endregion

    #region IEnumerable<ContactType> ContactTypes

    static Collection<ContactType> mContactTypes;
    public IEnumerable<ContactType> ContactTypes
    {
      get
      {
        return mContactTypes ?? (mContactTypes = Cache.Instance.GetObjects<ContactType>(true, ct => ct.IsEmployee, ct => ct.Name, true));
      }
    }

    #endregion
  }
}

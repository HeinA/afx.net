﻿using Afx.Business;
using Afx.Business.Activities;
using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Activities;
using ContactManagement.Business;
using Microsoft.Practices.Prism;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContactManagement.Prism.Activities.ContactManagement
{
  public partial class ContactManagementController : MdiContextualActivityController<ContactManagement, ContactManagementViewModel>
  {
    #region Constructors

    [InjectionConstructor]
    public ContactManagementController(IController controller)
      : base(controller)
    {
    }

    #endregion

    #region OnLoaded
    
    protected override void OnLoaded()
    {
      if (DataContext.Data.Count > 0)
      {
        //Focus the first element 
        ContactItemViewModel ivm = GetCreateViewModel<ContactItemViewModel>(DataContext.Data[0], ViewModel);
        ivm.IsFocused = true;
      }

      RegisterContextRegion(RegionManager.Regions[ContactManagement.DetailsRegion]);
      RegisterContextRegion(RegionManager.Regions[ContactManagement.TabRegion]);

      base.OnLoaded();
    }

    #endregion
    
    #region OnContextViewModelChanged
    
    protected override void OnContextViewModelChanged(ISelectableViewModel context)
    {
      if (context.Model is Contact)
      {
        ContactDetailViewModel vm = GetCreateViewModel<ContactDetailViewModel>(context.Model, ViewModel);
        AddDetailsViewModel(vm);

        ContactManagementAddressTabViewModel vm1 = GetCreateViewModel<ContactManagementAddressTabViewModel>(context.Model, ViewModel);
        AddTabViewModel(vm1);

        ContactManagementTelephoneTabViewModel vm2 = GetCreateViewModel<ContactManagementTelephoneTabViewModel>(context.Model, ViewModel);
        AddTabViewModel(vm2);
      }

      base.OnContextViewModelChanged(context);
    }

    #endregion
    
    #region ExecuteOperation
    
    protected override void ExecuteOperation(Operation op, BusinessObject argument)
    {
      switch (op.Identifier)
      {
        case Operations.AddContact:
          CreateRootItem();
          break;
        default:
          break;
      }
      base.ExecuteOperation(op, argument);
    }
    
    #endregion
  }
}

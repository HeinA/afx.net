﻿using Afx.Business.Security;
using Afx.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Activities;
using ContactManagement.Business;

namespace ContactManagement.Prism.Activities.ContactManagement
{
  public partial class ContactManagementViewModel : MdiContextualActivityViewModel<ContactManagement>
  {
    #region Constructors

    [InjectionConstructor]
    public ContactManagementViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion
  }
}

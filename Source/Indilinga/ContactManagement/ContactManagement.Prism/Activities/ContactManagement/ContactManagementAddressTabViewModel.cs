﻿using Afx.Prism;
using ContactManagement.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContactManagement.Prism.Activities.ContactManagement
{
  public class ContactManagementAddressTabViewModel : TabViewModel<Contact>
  {
    #region Constructors

    [InjectionConstructor]
    public ContactManagementAddressTabViewModel(IController controller)
      : base(controller)
    {
      Title = "Addresses";
    }

    #endregion
  }
}

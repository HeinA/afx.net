﻿using Afx.Business;
using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using ContactManagement.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContactManagement.Prism.Activities.ContactManagement
{
  public partial class ContactItemViewModel : MdiNavigationListItemViewModel<Contact>
  {
    #region Constructors

    [InjectionConstructor]
    public ContactItemViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion
  }
}

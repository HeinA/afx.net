﻿using ContactManagement.Business;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.Composition;
using Microsoft.Practices.Prism;

namespace ContactManagement.Prism.Activities.ContactManagement
{
  //**************************************************************************************************************
  //**************************************************************************************************************
  //*                                                                                                            *
  //*     This code is auto-generated.  Do not place any cutom code here, it will be lost on re-generation.      *
  //*                                                                                                            *
  //**************************************************************************************************************
  //**************************************************************************************************************
  [Export("Controller:" + global::ContactManagement.Prism.Activities.ContactManagement.ContactManagement.Key)]
  public partial class ContactManagementController
  {
    void CreateRootItem()
    {
      Contact o = new Contact();
      DataContext.Data.Add(o);
      SelectContextViewModel(o);
      FocusViewModel<ContactDetailViewModel>(o);
    }

    void AddDetailsViewModel(object viewModel)
    {
      RegionManager.Regions[ContactManagement.DetailsRegion].Add(viewModel);
    }

    void AddTabViewModel(object viewModel)
    {
      IRegion tabRegion = RegionManager.Regions[ContactManagement.TabRegion];
      tabRegion.Add(viewModel);
      IActiveAware aw = viewModel as IActiveAware;
      if (aw != null && aw.IsActive) tabRegion.Activate(viewModel);
    }
  }
}

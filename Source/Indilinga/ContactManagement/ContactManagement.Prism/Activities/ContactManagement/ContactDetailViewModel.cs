﻿using Afx.Business;
using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Activities;
using ContactManagement.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContactManagement.Prism.Activities.ContactManagement
{
  public partial class ContactDetailViewModel : ActivityDetailViewModel<Contact>
  {
    #region Constructors

    [InjectionConstructor]
    public ContactDetailViewModel(IController controller)
      : base(controller)
    {
    }
    
    #endregion
    
    #region IEnumerable<ContactType> ContactTypes

    static Collection<ContactType> mContactTypes;
    public IEnumerable<ContactType> ContactTypes
    {
      get
      {
        return mContactTypes ?? (mContactTypes = Cache.Instance.GetObjects<ContactType>(true, (si) => si.Name, true));
      }
    }

    #endregion


  }
}

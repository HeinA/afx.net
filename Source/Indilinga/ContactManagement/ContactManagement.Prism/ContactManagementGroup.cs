﻿using Afx.Prism.RibbonMdi.Ribbon;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContactManagement.Prism
{
  [Export(typeof(IRibbonGroup))]
  public class ContactManagementGroup : RibbonGroup
  {
    public const string GroupName = "Contact Management";

    public override string TabName
    {
      get { return AdministrationTab.TabName; }
    }

    public override string Name
    {
      get { return GroupName; }
    }
  }
}

﻿using Afx.Business.Collections;
using Afx.Business.Service;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.WCF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace ContactManagement.Business.Service
{
  public partial class ContactManagementService
  {
    public BasicCollection<ContactManagement.Business.ContactType> LoadContactTypeCollection()
    {
      try
      {
        return GetAllInstances<ContactManagement.Business.ContactType>();
      }
      catch
      {
        throw;
      }
    }

    public BasicCollection<ContactManagement.Business.ContactType> SaveContactTypeCollection(BasicCollection<ContactManagement.Business.ContactType> col)
    {
      try
      {
        return Persist<ContactManagement.Business.ContactType>(col);
      }
      catch
      {
        throw;
      }
    }
  }
}
﻿using Afx.Business.Collections;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace ContactManagement.Business.Service
{
  public partial interface IContactManagementService
  {
    [OperationContract]
    BasicCollection<ContactManagement.Business.Contractor> LoadContractorCollection();

    [OperationContract]
    BasicCollection<ContactManagement.Business.Contractor> SaveContractorCollection(BasicCollection<ContactManagement.Business.Contractor> col);
  }
}


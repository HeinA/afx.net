﻿using Afx.Business;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Service;
using FileHelpers;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.WCF;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data;
using System.Data.Odbc;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace ContactManagement.Business.Service
{
  [ServiceBehavior(Namespace = Namespace.ContactManagement, ConcurrencyMode = ConcurrencyMode.Multiple)]
  [ExceptionShielding]
  [Export(typeof(IService))]
  public partial class ContactManagementService : ServiceBase, IContactManagementService
  {
    public BasicCollection<ContactManagement.Business.Employee> ImportEmployees(byte[] csvfile)
    {
      try
      {
        BasicCollection<Employee> employees = new BasicCollection<Employee>();

        var engine = new FileHelperEngine<EmployeeCsvRecord>();
        using (var ms = new MemoryStream(csvfile))
        {
          using (var sr = new StreamReader(ms))
          {
            var records = engine.ReadStream(sr);

            foreach (var r in records)
            {
              var e = new Employee() { Firstname = r.FirstName, Lastname = r.LastName, ContactType = Cache.Instance.GetObjects<ContactType>().First(ct => ct.Name.Equals(r.EmployeeType)), IDNumber = r.IdNumber, PassportNumber = r.PassportNumber, PassportExpiryDate = r.PassportExpiry };
              foreach (var a in r.Addresses.Split(new char[] {';'}, StringSplitOptions.RemoveEmptyEntries))
              {
                throw new NotImplementedException();
              }

              foreach (var pn in r.PhoneNumbers.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries))
              {
                e.TelephoneNumbers.Add(new Telephone() { TelephoneType = Cache.Instance.GetObjects<TelephoneType>().First(), TelephoneNumber = pn });
              }

              employees.Add(e);
            }

            return Persist<ContactManagement.Business.Employee>(employees);
          }
        }
      }
      catch
      {
        throw;
      }
    }
  }
}

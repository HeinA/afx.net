﻿using Afx.Business.Collections;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace ContactManagement.Business.Service
{
  public partial interface IContactManagementService
  {
    [OperationContract]
    BasicCollection<ContactManagement.Business.ContactType> LoadContactTypeCollection();

    [OperationContract]
    BasicCollection<ContactManagement.Business.ContactType> SaveContactTypeCollection(BasicCollection<ContactManagement.Business.ContactType> col);
  }
}


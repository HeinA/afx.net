﻿using Afx.Business.Collections;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace ContactManagement.Business.Service
{
  public partial interface IContactManagementService
  {
    [OperationContract]
    BasicCollection<ContactManagement.Business.ContractorType> LoadContractorTypeCollection();

    [OperationContract]
    BasicCollection<ContactManagement.Business.ContractorType> SaveContractorTypeCollection(BasicCollection<ContactManagement.Business.ContractorType> col);
  }
}


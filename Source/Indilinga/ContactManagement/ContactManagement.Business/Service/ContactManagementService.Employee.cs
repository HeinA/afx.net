﻿using Afx.Business.Collections;
using Afx.Business.Service;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.WCF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace ContactManagement.Business.Service
{
  public partial class ContactManagementService
  {
    public BasicCollection<ContactManagement.Business.Employee> LoadEmployeeCollection()
    {
      try
      {
        return GetAllInstances<ContactManagement.Business.Employee>();
      }
      catch
      {
        throw;
      }
    }

    public BasicCollection<ContactManagement.Business.Employee> SaveEmployeeCollection(BasicCollection<ContactManagement.Business.Employee> col)
    {
      try
      {
        return Persist<ContactManagement.Business.Employee>(col);
      }
      catch
      {
        throw;
      }
    }
  }
}
﻿using Afx.Business.Collections;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace ContactManagement.Business.Service
{
  public partial interface IContactManagementService
  {
    [OperationContract]
    BasicCollection<ContactManagement.Business.AddressType> LoadAddressTypeCollection();

    [OperationContract]
    BasicCollection<ContactManagement.Business.AddressType> SaveAddressTypeCollection(BasicCollection<ContactManagement.Business.AddressType> col);
  }
}


﻿using Afx.Business.Collections;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace ContactManagement.Business.Service
{
  public partial interface IContactManagementService
  {
    [OperationContract]
    BasicCollection<ContactManagement.Business.Employee> LoadEmployeeCollection();

    [OperationContract]
    BasicCollection<ContactManagement.Business.Employee> SaveEmployeeCollection(BasicCollection<ContactManagement.Business.Employee> col);
  }
}


﻿using Afx.Business;
using Afx.Business.Collections;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace ContactManagement.Business.Service
{
  [ServiceContract(Namespace = Namespace.ContactManagement)]
  public partial interface IContactManagementService : IDisposable
  {
    [OperationContract]
    BasicCollection<ContactManagement.Business.Employee> ImportEmployees(byte[] csvfile);
  }
}

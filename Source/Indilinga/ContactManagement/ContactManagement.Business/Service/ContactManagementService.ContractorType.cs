﻿using Afx.Business.Collections;
using Afx.Business.Service;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.WCF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace ContactManagement.Business.Service
{
  public partial class ContactManagementService
  {
    public BasicCollection<ContactManagement.Business.ContractorType> LoadContractorTypeCollection()
    {
      try
      {
        return GetAllInstances<ContactManagement.Business.ContractorType>();
      }
      catch
      {
        throw;
      }
    }

    public BasicCollection<ContactManagement.Business.ContractorType> SaveContractorTypeCollection(BasicCollection<ContactManagement.Business.ContractorType> col)
    {
      try
      {
        return Persist<ContactManagement.Business.ContractorType>(col);
      }
      catch
      {
        throw;
      }
    }
  }
}
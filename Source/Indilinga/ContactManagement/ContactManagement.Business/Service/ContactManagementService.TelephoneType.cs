﻿using Afx.Business.Collections;
using Afx.Business.Service;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.WCF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace ContactManagement.Business.Service
{
  public partial class ContactManagementService
  {
    public BasicCollection<ContactManagement.Business.TelephoneType> LoadTelephoneTypeCollection()
    {
      try
      {
        return GetAllInstances<ContactManagement.Business.TelephoneType>();
      }
      catch
      {
        throw;
      }
    }

    public BasicCollection<ContactManagement.Business.TelephoneType> SaveTelephoneTypeCollection(BasicCollection<ContactManagement.Business.TelephoneType> col)
    {
      try
      {
        return Persist<ContactManagement.Business.TelephoneType>(col);
      }
      catch
      {
        throw;
      }
    }
  }
}
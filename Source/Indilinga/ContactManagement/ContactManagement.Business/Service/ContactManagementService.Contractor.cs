﻿using Afx.Business.Collections;
using Afx.Business.Service;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.WCF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace ContactManagement.Business.Service
{
  public partial class ContactManagementService
  {
    public BasicCollection<ContactManagement.Business.Contractor> LoadContractorCollection()
    {
      try
      {
        return GetAllInstances<ContactManagement.Business.Contractor>();
      }
      catch
      {
        throw;
      }
    }

    public BasicCollection<ContactManagement.Business.Contractor> SaveContractorCollection(BasicCollection<ContactManagement.Business.Contractor> col)
    {
      try
      {
        return Persist<ContactManagement.Business.Contractor>(col);
      }
      catch
      {
        throw;
      }
    }
  }
}
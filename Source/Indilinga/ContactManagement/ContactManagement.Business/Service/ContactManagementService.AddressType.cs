﻿using Afx.Business.Collections;
using Afx.Business.Service;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.WCF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace ContactManagement.Business.Service
{
  public partial class ContactManagementService
  {
    public BasicCollection<ContactManagement.Business.AddressType> LoadAddressTypeCollection()
    {
      try
      {
        return GetAllInstances<ContactManagement.Business.AddressType>();
      }
      catch
      {
        throw;
      }
    }

    public BasicCollection<ContactManagement.Business.AddressType> SaveAddressTypeCollection(BasicCollection<ContactManagement.Business.AddressType> col)
    {
      try
      {
        return Persist<ContactManagement.Business.AddressType>(col);
      }
      catch
      {
        throw;
      }
    }
  }
}
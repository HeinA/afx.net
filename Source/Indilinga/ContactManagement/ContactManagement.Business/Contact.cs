﻿using Afx.Business;
using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Validation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace ContactManagement.Business
{
  [PersistantObject(Schema = "ContactManagement")]
  public abstract partial class Contact : BusinessObject
  {
    #region Constructors

    protected Contact()
    {
    }

    protected Contact(bool isNull)
      : base(isNull)
    {
    }

    #endregion

    #region ContactType ContactType

    public const string ContactTypeProperty = "ContactType";
    [PersistantProperty]
    [Mandatory("Contact type is mandatory / required.")]
    public ContactType ContactType
    {
      get { return GetCachedObject<ContactType>(); }
      set { SetCachedObject<ContactType>(value); }
    }

    #endregion

    #region string Lastname

    public const string LastnameProperty = "Lastname";
    [DataMember(Name = LastnameProperty, EmitDefaultValue = false)]
    string mLastname;
    [Mandatory("Lastname is mandatory / required.")]
    [PersistantProperty]
    public string Lastname
    {
      get { return mLastname; }
      set { SetProperty<string>(ref mLastname, value); }
    }

    #endregion

    #region string Firstname

    public const string FirstnameProperty = "Firstname";
    [DataMember(Name = FirstnameProperty, EmitDefaultValue = false)]
    string mFirstname;
    [PersistantProperty]
    public string Firstname
    {
      get { return mFirstname; }
      set { SetProperty<string>(ref mFirstname, value); }
    }

    #endregion

    #region string Middlename

    public const string MiddlenameProperty = "Middlename";
    [DataMember(Name = MiddlenameProperty, EmitDefaultValue = false)]
    string mMiddlename;
    [PersistantProperty]
    public string Middlename
    {
      get { return mMiddlename; }
      set { SetProperty<string>(ref mMiddlename, value); }
    }

    #endregion

    #region String Initials

    public const string InitialsProperty = "Initials";
    [DataMember(Name = InitialsProperty, EmitDefaultValue = false)]
    String mInitials;
    [PersistantProperty]
    public String Initials
    {
      get { return mInitials; }
      set { SetProperty<String>(ref mInitials, value); }
    }

    #endregion

    #region String Nickname

    public const string NicknameProperty = "Nickname";
    [DataMember(Name = NicknameProperty, EmitDefaultValue = false)]
    String mNickname;
    [PersistantProperty]
    public String Nickname
    {
      get { return mNickname; }
      set { SetProperty<String>(ref mNickname, value); }
    }

    #endregion

    #region string EMail

    public const string EMailProperty = "EMail";
    [DataMember(Name = EMailProperty, EmitDefaultValue = false)]
    string mEMail;
    [PersistantProperty]
    public string EMail
    {
      get { return mEMail; }
      set { SetProperty<string>(ref mEMail, value); }
    }

    #endregion

    #region string IDNumber

    public const string IDNumberProperty = "IDNumber";
    [DataMember(Name = IDNumberProperty, EmitDefaultValue = false)]
    string mIDNumber;
    [PersistantProperty]
    public string IDNumber
    {
      get { return mIDNumber; }
      set { SetProperty<string>(ref mIDNumber, value); }
    }

    #endregion

    #region string PassportNumber

    public const string PassportNumberProperty = "PassportNumber";
    [DataMember(Name = PassportNumberProperty, EmitDefaultValue = false)]
    string mPassportNumber;
    [PersistantProperty]
    public string PassportNumber
    {
      get { return mPassportNumber; }
      set { SetProperty<string>(ref mPassportNumber, value); }
    }

    #endregion

    #region DateTime? PassportExpiryDate

    public const string PassportExpiryDateProperty = "PassportExpiryDate";
    [DataMember(Name = PassportExpiryDateProperty, EmitDefaultValue = false)]
    DateTime? mPassportExpiryDate;
    [PersistantProperty]
    public DateTime? PassportExpiryDate
    {
      get { return mPassportExpiryDate; }
      set { SetProperty<DateTime?>(ref mPassportExpiryDate, value); }
    }

    #endregion

    #region DateTime DateofBirth

    public const string DateofBirthProperty = "DateofBirth";
    [DataMember(Name = DateofBirthProperty, EmitDefaultValue = false)]
    DateTime? mDateofBirth;
    [PersistantProperty]
    public DateTime? DateofBirth
    {
      get { return mDateofBirth; }
      set { SetProperty<DateTime?>(ref mDateofBirth, value); }
    }

    #endregion



    public virtual string Fullname
    {
      get
      {
        if (string.IsNullOrWhiteSpace(Lastname)) return Firstname;
        if (string.IsNullOrWhiteSpace(Firstname)) return Lastname;
        return String.Format("{1} {0}", Lastname, Firstname);
      }
    }

    public IContactOwner Owner
    {
      get { return ((IBusinessObject)this).Owner as IContactOwner; }
    }

    //public string OwnerText
    //{
    //  get
    //  {
    //    IContactOwner o = ((IBusinessObject)this).Owner as IContactOwner;
    //    if (o != null) return o.ContactOwnerText;
    //    return null;
    //  }
    //}
  }
}

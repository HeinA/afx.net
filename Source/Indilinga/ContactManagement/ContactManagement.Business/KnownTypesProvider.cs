﻿using Afx.Business.ServiceModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContactManagement.Business
{
  [Export(typeof(IKnownTypesProvider))]
  public class KnownTypesProvider : IKnownTypesProvider
  {
    public IEnumerable<Type> GetKnowTypes()
    {
      Collection<Type> types = new Collection<Type>();
      types.Add(typeof(SystemTelephoneType));
      return types;
    }
  }
}

﻿using Afx.Business;
using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Validation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace ContactManagement.Business
{
  [PersistantObject(Schema = "ContactManagement")]
  public partial class SimpleContact : Contact 
  {
    #region Constructors

    public SimpleContact()
    {
    }

    public SimpleContact(bool isNull)
      : base(isNull)
    {
    }

    #endregion

    #region string TelephoneNumber

    public const string TelephoneNumberProperty = "TelephoneNumber";
    [DataMember(Name = TelephoneNumberProperty, EmitDefaultValue = false)]
    string mTelephoneNumber;
    [PersistantProperty]
    public string TelephoneNumber
    {
      get { return mTelephoneNumber; }
      set { SetProperty<string>(ref mTelephoneNumber, value); }
    }

    #endregion

    #region string FaxNumber

    public const string FaxNumberProperty = "FaxNumber";
    [DataMember(Name = FaxNumberProperty, EmitDefaultValue = false)]
    string mFaxNumber;
    [PersistantProperty]
    public string FaxNumber
    {
      get { return mFaxNumber; }
      set { SetProperty<string>(ref mFaxNumber, value); }
    }

    #endregion
  }
}

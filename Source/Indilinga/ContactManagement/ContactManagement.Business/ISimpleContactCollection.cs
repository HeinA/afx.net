﻿using Afx.Business;
using Afx.Business.Collections;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContactManagement.Business
{
  public interface ISimpleContactCollection : IBusinessObject
  {
    BusinessObjectCollection<SimpleContact> Contacts { get; }
  }
}

﻿using Afx.Business;
using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Validation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace ContactManagement.Business
{
  [PersistantObject(Schema = "ContactManagement")]
  public partial class Address : BusinessObject 
  {
    #region Constructors

    public Address()
    {
    }

    #endregion

    #region AddressType AddressType

    public const string AddressTypeProperty = "AddressType";
    [PersistantProperty]
    public AddressType AddressType
    {
      get { return GetCachedObject<AddressType>(); }
      set { SetCachedObject<AddressType>(value); }
    }

    #endregion

    #region string Name

    public const string NameProperty = "Name";
    [DataMember(Name = NameProperty, EmitDefaultValue = false)]
    string mName;
    [PersistantProperty]
    public string Name
    {
      get { return mName; }
      set { SetProperty<string>(ref mName, value); }
    }

    #endregion

    #region string Contact

    public const string ContactProperty = "Contact";
    [DataMember(Name = ContactProperty, EmitDefaultValue = false)]
    string mContact;
    [PersistantProperty]
    public string Contact
    {
      get { return mContact; }
      set { SetProperty<string>(ref mContact, value); }
    }

    #endregion

    #region string ContactNumber

    public const string ContactNumberProperty = "ContactNumber";
    [DataMember(Name = ContactNumberProperty, EmitDefaultValue = false)]
    string mContactNumber;
    [PersistantProperty]
    public string ContactNumber
    {
      get { return mContactNumber; }
      set { SetProperty<string>(ref mContactNumber, value); }
    }

    #endregion

    #region string ImportReference

    public const string ImportReferenceProperty = "ImportReference";
    [DataMember(Name = ImportReferenceProperty, EmitDefaultValue = false)]
    string mImportReference;
    [PersistantProperty]
    public string ImportReference
    {
      get { return mImportReference; }
      set { SetProperty<string>(ref mImportReference, value); }
    }

    #endregion

    #region string ExportReference

    public const string ExportReferenceProperty = "ExportReference";
    [DataMember(Name = ExportReferenceProperty, EmitDefaultValue = false)]
    string mExportReference;
    [PersistantProperty]
    public string ExportReference
    {
      get { return mExportReference; }
      set { SetProperty<string>(ref mExportReference, value); }
    }

    #endregion

    #region string AddressText

    public const string AddressTextProperty = "AddressText";
    [DataMember(Name = AddressTextProperty, EmitDefaultValue = false)]
    string mAddressText;
    [PersistantProperty]
    public string AddressText
    {
      get { return mAddressText; }
      set { SetProperty<string>(ref mAddressText, value); }
    }

    #endregion

    public string AddressTextSingleLine
    {
      get { return AddressText == null ? null : AddressText.Replace("\r\n", ", ").Replace("\n", ", "); }
    }

    public string AddressTextWithNameSingleLine
    {
      get { return string.Format("{0}, {1}", Name, AddressText == null ? null : AddressText.Replace("\r\n", ", ").Replace("\n", ", ")); }
    }

    public string AddressTextWithContact
    {
      get { return string.Format("{1} ({2})\r\n{3}", Name, Contact, ContactNumber, AddressText); }
    }
  }
}

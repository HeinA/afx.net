﻿using Afx.Business;
using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Validation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace ContactManagement.Business
{
  [PersistantObject(Schema = "ContactManagement")]
  public abstract partial class ComplexContact : Contact, IAddressCollection, ITelephoneCollection
  {
    #region Constructors

    protected ComplexContact()
    {
    }

    protected ComplexContact(bool isNull)
      : base(isNull)
    {
    }

    #endregion

    #region Associative BusinessObjectCollection<Address> Addresses

    public const string AddressesProperty = "Addresses";
    AssociativeObjectCollection<ContactAddress, Address> mAddresses;
    [PersistantCollection(AssociativeType = typeof(ContactAddress))]
    public BusinessObjectCollection<Address> Addresses
    {
      get
      {
        if (mAddresses == null) using (new EventStateSuppressor(this)) { mAddresses = new AssociativeObjectCollection<ContactAddress, Address>(this); }
        return mAddresses;
      }
    }

    #endregion

    #region Associative BusinessObjectCollection<Telephone> TelephoneNumbers

    public const string TelephoneNumbersProperty = "TelephoneNumbers";
    AssociativeObjectCollection<ContactTelephone, Telephone> mTelephoneNumbers;
    [PersistantCollection(AssociativeType = typeof(ContactTelephone))]
    public BusinessObjectCollection<Telephone> TelephoneNumbers
    {
      get
      {
        if (mTelephoneNumbers == null) using (new EventStateSuppressor(this)) { mTelephoneNumbers = new AssociativeObjectCollection<ContactTelephone, Telephone>(this); }
        return mTelephoneNumbers;
      }
    }

    #endregion
  }
}

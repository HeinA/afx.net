﻿using Afx.Business;
using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Validation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace ContactManagement.Business
{
  [PersistantObject(Schema = "ContactManagement")]
  public partial class Telephone : BusinessObject 
  {
    #region Constructors

    public Telephone()
    {
    }

    #endregion

    #region TelephoneType TelephoneType

    public const string TelephoneTypeProperty = "TelephoneType";
    [PersistantProperty]
    public TelephoneType TelephoneType
    {
      get { return GetCachedObject<TelephoneType>(); }
      set { SetCachedObject<TelephoneType>(value); }
    }

    #endregion

    #region string TelephoneNumber

    public const string TelephoneNumberProperty = "TelephoneNumber";
    [DataMember(Name = TelephoneNumberProperty, EmitDefaultValue = false)]
    string mTelephoneNumber;
    [PersistantProperty]
    public string TelephoneNumber
    {
      get { return mTelephoneNumber; }
      set { SetProperty<string>(ref mTelephoneNumber, value); }
    }

    #endregion

  }
}

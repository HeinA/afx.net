﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContactManagement.Business
{
  public enum SystemTelephoneType
  {
    Unspecified
      ,
    Mobile
      ,
    Foreign
      ,
    Landline
      ,
    Fax
      ,
    SATPhone
      ,
    Other
  }
}

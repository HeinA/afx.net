﻿using Afx.Business;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContactManagement.Business
{
  [Export(typeof(INamespace))]
  public class Namespace : INamespace
  {
    public const string ContactManagement = "http://indilinga.com/ContactManagement/Business";

    public string GetUri()
    {
      return ContactManagement;
    }
  }
}

﻿using Afx.Business;
using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace ContactManagement.Business
{
  [PersistantObject(Schema = "ContactManagement")]
  public partial class ContactAddress : AssociativeObject<ComplexContact, Address>
  {
    protected override bool ReferenceIsOwned
    {
      get { return true; }
    }
  }
}

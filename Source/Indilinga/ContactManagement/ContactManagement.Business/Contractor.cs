﻿using Afx.Business;
using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Validation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace ContactManagement.Business
{
  [PersistantObject(Schema = "ContactManagement")]
  [Cache]
  public partial class Contractor : BusinessObject, IRevisable, ISimpleContactCollection, IContactOwner
  {
    #region Constructors

    public Contractor()
    {
    }

    public Contractor(bool isNull)
      : base(isNull)
    {
    }

    #endregion

    #region int Revision

    public const string RevisionProperty = "Revision";
    [DataMember(Name = RevisionProperty, EmitDefaultValue = false)]
    int mRevision;
    [PersistantProperty]
    public int Revision
    {
      get { return mRevision; }
      set { SetProperty<int>(ref mRevision, value); }
    }

    #endregion

    #region string CompanyName

    public const string CompanyNameProperty = "CompanyName";
    [DataMember(Name = CompanyNameProperty, EmitDefaultValue = false)]
    string mCompanyName;
    [PersistantProperty]
    [Mandatory("Company Name is mandatory")]
    public string CompanyName
    {
      get { return mCompanyName; }
      set { SetProperty<string>(ref mCompanyName, value); }
    }

    #endregion

    #region string CompanyNumber

    public const string CompanyNumberProperty = "CompanyNumber";
    [DataMember(Name = CompanyNumberProperty, EmitDefaultValue = false)]
    string mCompanyNumber;
    [PersistantProperty]
    public string CompanyNumber
    {
      get { return mCompanyNumber; }
      set { SetProperty<string>(ref mCompanyNumber, value); }
    }

    #endregion

    #region string VATNumber

    public const string VATNumberProperty = "VATNumber";
    [DataMember(Name = VATNumberProperty, EmitDefaultValue = false)]
    string mVATNumber;
    [PersistantProperty]
    public string VATNumber
    {
      get { return mVATNumber; }
      set { SetProperty<string>(ref mVATNumber, value); }
    }

    #endregion

    #region int Rating

    public const string RatingProperty = "Rating";
    [DataMember(Name = RatingProperty, EmitDefaultValue = false)]
    int mRating;
    [PersistantProperty]
    public int Rating
    {
      get { return mRating; }
      set { SetProperty<int>(ref mRating, value); }
    }

    #endregion

    #region Associative BusinessObjectCollection<SimpleContact> Contacts

    public const string ContactsProperty = "Contacts";
    AssociativeObjectCollection<ContractorContact, SimpleContact> mContacts;
    [PersistantCollection(AssociativeType = typeof(ContractorContact))]
    public BusinessObjectCollection<SimpleContact> Contacts
    {
      get
      {
        if (mContacts == null) using (new EventStateSuppressor(this)) { mContacts = new AssociativeObjectCollection<ContractorContact, SimpleContact>(this); }
        return mContacts;
      }
    }

    #endregion

    public string ContactOwnerText
    {
      get { return CompanyName; }
    }

    #region ContractorType ContractorType

    public const string ContractorTypeProperty = "ContractorType";
    [PersistantProperty]
    public ContractorType ContractorType
    {
      get { return GetCachedObject<ContractorType>(); }
      set { SetCachedObject<ContractorType>(value); }
    }

    #endregion

  }
}

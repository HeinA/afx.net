﻿using Afx.Business;
using ContactManagement.Business;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContactManagement.Business
{
  [Export(typeof(IObjectFlags))]
  public class AddressTypeFlags : IObjectFlags
  {
    public const string PhysicalAddress = "ContactManagement.PhysicalAddress";
    public const string PostalAddress = "ContactManagement.PostalAddress";

    public Type ObjectType
    {
      get { return typeof(AddressType); }
    }

    public IEnumerable<string> Flags
    {
      get { return new string[] { PhysicalAddress, PostalAddress }; }
    }
  }
}

﻿using Afx.Business;
using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace ContactManagement.Business
{
  [PersistantObject(Schema = "ContactManagement", HasFlags = true)]
  [Cache(Priority = 0)]
  public partial class ContactType : BusinessObject, IRevisable 
  {
    #region Constructors

    public ContactType()
    {
    }

    public ContactType(bool isNull)
      : base(isNull)
    {
    }

    #endregion

    #region int Revision

    public const string RevisionProperty = "Revision";
    [DataMember(Name = RevisionProperty, EmitDefaultValue = false)]
    int mRevision;
    [PersistantProperty]
    public int Revision
    {
      get { return mRevision; }
      set { SetProperty<int>(ref mRevision, value); }
    }

    #endregion

    #region String Name

    public const string NameProperty = "Name";
    [DataMember(Name = NameProperty, EmitDefaultValue = false)]
    String mName;
    [PersistantProperty]
    public String Name
    {
      get { return mName; }
      set { SetProperty<String>(ref mName, value); }
    }

    #endregion

    #region bool IsEmployee

    public const string IsEmployeeProperty = "IsEmployee";
    [DataMember(Name = IsEmployeeProperty, EmitDefaultValue = false)]
    bool mIsEmployee;
    [PersistantProperty]
    public bool IsEmployee
    {
      get { return mIsEmployee; }
      set { SetProperty<bool>(ref mIsEmployee, value); }
    }

    #endregion

    #region bool IsContractor

    public const string IsContractorProperty = "IsContractor";
    [DataMember(Name = IsContractorProperty, EmitDefaultValue = false)]
    bool mIsContractor;
    [PersistantProperty]
    public bool IsContractor
    {
      get { return mIsContractor; }
      set { SetProperty<bool>(ref mIsContractor, value); }
    }

    #endregion
  }
}

﻿using Afx.Business;
using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Validation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace ContactManagement.Business
{
  [PersistantObject(Schema = "ContactManagement")]
  [Cache]
  public partial class TelephoneType : BusinessObject, IRevisable 
  {
    #region Constructors

    public TelephoneType()
    {
    }

    public TelephoneType(bool isNull)
      : base(isNull)
    {
    }

    #endregion

    #region int Revision

    public const string RevisionProperty = "Revision";
    [DataMember(Name = RevisionProperty, EmitDefaultValue = false)]
    int mRevision;
    [PersistantProperty]
    public int Revision
    {
      get { return mRevision; }
      set { SetProperty<int>(ref mRevision, value); }
    }

    #endregion

    #region string Text

    public const string TextProperty = "Text";
    [DataMember(Name = TextProperty, EmitDefaultValue = false)]
    string mText;
    [PersistantProperty]
    public string Text
    {
      get { return mText; }
      set { SetProperty<string>(ref mText, value); }
    }

    #endregion
    
    #region SystemTelephoneType SystemTelephoneType

    public const string SystemTelephoneTypeProperty = "SystemTelephoneType";
    [DataMember(Name = SystemTelephoneTypeProperty, EmitDefaultValue = false)]
    SystemTelephoneType mSystemTelephoneType;
    [PersistantProperty]
    [Mandatory("System Telephone Type is mandatory.")]
    public SystemTelephoneType SystemTelephoneType
    {
      get { return mSystemTelephoneType; }
      set { SetProperty<SystemTelephoneType>(ref mSystemTelephoneType, value); }
    }

    #endregion
  }
}

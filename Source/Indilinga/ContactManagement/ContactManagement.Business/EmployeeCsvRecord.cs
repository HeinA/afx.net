﻿using FileHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContactManagement.Business
{
  [DelimitedRecord(",")]
  [IgnoreFirst]
  class EmployeeCsvRecord
  {
    public string FirstName;
    public string LastName;
    public string EmployeeType;

    [FieldConverter(ConverterKind.Date, "yyyy/MM/dd")]
    public DateTime? DOB;

    public string IdNumber;
    public string PassportNumber;

    [FieldConverter(ConverterKind.Date, "yyyy/MM/dd")]
    public DateTime? PassportExpiry;

    public string Addresses;
    public string PhoneNumbers;
  }
}

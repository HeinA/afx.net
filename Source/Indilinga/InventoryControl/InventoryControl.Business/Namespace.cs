﻿using Afx.Business;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryControl.Business
{
  [Export(typeof(INamespace))]
  public class Namespace : INamespace
  {
    public const string InventoryControl = "http://indilinga.com/InventoryControl/Business";

    public string GetUri()
    {
      return InventoryControl;
    }
  }
}

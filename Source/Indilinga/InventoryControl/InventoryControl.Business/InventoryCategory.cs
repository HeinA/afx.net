﻿using Afx.Business;
using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Validation;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace InventoryControl.Business
{
  [PersistantObject(Schema = "IC")]
  [Cache(Priority = 1)]
  public partial class InventoryCategory : BusinessObject, IExternalReferenceCollection, IRevisable
  {
    #region Constructors

    public InventoryCategory()
    {
    }

    public InventoryCategory(bool isNull)
      : base(isNull)
    {
    }

    #endregion

    #region int Revision

    public const string RevisionProperty = "Revision";
    [DataMember(Name = RevisionProperty, EmitDefaultValue = false)]
    int mRevision = 1;
    [PersistantProperty]
    public int Revision
    {
      get { return mRevision; }
      set { SetProperty<int>(ref mRevision, value); }
    }

    #endregion

    #region string Description

    public const string DescriptionProperty = "Description";
    [DataMember(Name = DescriptionProperty, EmitDefaultValue = false)]
    string mDescription;
    [PersistantProperty]
    [Mandatory("Description is a mandatory field.")]
    public string Description
    {
      get { return mDescription; }
      set { SetProperty<string>(ref mDescription, value); }
    }

    #endregion

    #region BusinessObjectCollection<InventoryCategoryReference> ExternalReferences

    public const string ExternalReferencesProperty = "ExternalReferences";
    BusinessObjectCollection<InventoryCategoryReference> mExternalReferences;
    [PersistantCollection]
    [DataMember]
    public BusinessObjectCollection<InventoryCategoryReference> ExternalReferences
    {
      get { return mExternalReferences ?? (mExternalReferences = new BusinessObjectCollection<InventoryCategoryReference>(this)); }
    }

    #endregion

    #region void OnCompositionChanged(...)

    protected override void OnCompositionChanged(CompositionChangedType compositionChangedType, string propertyName, object originalSource, BusinessObject item)
    {
      if ((compositionChangedType == CompositionChangedType.ChildAdded || compositionChangedType == CompositionChangedType.ChildRemoved) && propertyName == ExternalReferencesProperty)
      {
        OnPropertyChanged("ExternalReferenceText");
      }
      base.OnCompositionChanged(compositionChangedType, propertyName, originalSource, item);
    }

    #endregion

    #region IExternalReferenceCollection

    public string ExternalReferenceText
    {
      get { return ExternalReferenceHelper.GetReferenceText(this); }
    }

    IList IExternalReferenceCollection.ExternalReferences
    {
      get { return ExternalReferences; }
    }

    IExternalReference IExternalReferenceCollection.CreateReference(ExternalSystem es, string reference)
    {
      return new InventoryCategoryReference() { ExternalSystem = es, Reference = reference };
    }

    #endregion
  }
}
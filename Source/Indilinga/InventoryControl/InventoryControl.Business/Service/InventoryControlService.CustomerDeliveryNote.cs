﻿using Afx.Business.Service;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.WCF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace InventoryControl.Business.Service
{
  public partial class InventoryControlService
  {
    public InventoryControl.Business.CustomerDeliveryNote LoadCustomerDeliveryNote(Guid globalIdentifier)
    {
      try
      {
        return GetInstance<InventoryControl.Business.CustomerDeliveryNote>(globalIdentifier);
      }
      catch
      {
        throw;
      }
    }

    public InventoryControl.Business.CustomerDeliveryNote SaveCustomerDeliveryNote(InventoryControl.Business.CustomerDeliveryNote obj)
    {
      try
      {
        return Persist<InventoryControl.Business.CustomerDeliveryNote>(obj);
      }
      catch
      {
        throw;
      }
    }
  }
}
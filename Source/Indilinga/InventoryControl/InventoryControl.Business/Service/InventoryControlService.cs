﻿using Afx.Business;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Service;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.WCF;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data;
using System.Data.Odbc;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace InventoryControl.Business.Service
{
  [ServiceBehavior(Namespace = Namespace.InventoryControl, ConcurrencyMode = ConcurrencyMode.Multiple)]
  [ExceptionShielding]
  [Export(typeof(IService))]
  public partial class InventoryControlService : ServiceBase, IInventoryControlService
  {
    #region Import

    public BasicCollection<InventoryCategory> ImportInventoryCategories(ExternalSystem es)
    {
      try
      {
        DataSet ds = null;
        using (OdbcConnection con = new OdbcConnection(es.ConnectionString))
        {
          con.Open();
          using (OdbcCommand cmd = new OdbcCommand("SELECT * FROM InventoryCategory", con))
          {
            ds = DataHelper.ExecuteDataSet(cmd);
          }
        }

        BasicCollection<InventoryCategory> col = GetAllInstances<InventoryCategory>();

        foreach (DataRow dr in ds.Tables[0].Rows)
        {
          InventoryCategory ic = col.FirstOrDefault(ic1 => ic1.ExternalReferences.Any(er => er.ExternalSystem.Equals(es) && er.Reference == ((string)dr["ICCode"]).Trim()));
          if (ic == null)
          {
            ic = new InventoryCategory();
            ic.Description = ((string)dr["ICDesc"]).Trim();
            ic.ExternalReferences.Add(new InventoryCategoryReference() { ExternalSystem = es, Reference = ((string)dr["ICCode"]).Trim() });
            col.Add(ic);
          }
        }

        return Persist<InventoryCategory>(col);
      }
      catch(Exception ex)
      {
        throw new ImportException("An error occured during the import.", ex);
      }
    }

    public BasicCollection<InventoryItem> ImportInventoryItems(ExternalSystem es)
    {
      try
      {
        DataSet ds = null;
        using (OdbcConnection con = new OdbcConnection(es.ConnectionString))
        {
          con.Open();
          using (OdbcCommand cmd = new OdbcCommand("SELECT * FROM Inventory", con))
          {
            ds = DataHelper.ExecuteDataSet(cmd);
          }
        }

        BasicCollection<InventoryItem> col = GetAllInstances<InventoryItem>();

        foreach (DataRow dr in ds.Tables[0].Rows)
        {
          InventoryItem ii = col.FirstOrDefault(ic1 => ic1.ExternalReferences.Any(er => er.ExternalSystem.Equals(es) && er.Reference == ((string)dr["ItemCode"]).Trim()));
          InventoryCategory ic = Cache.Instance.GetObjects<InventoryCategory>().FirstOrDefault(ic1 => ic1.ExternalReferences.Any(er => er.ExternalSystem.Equals(es) && er.Reference == ((string)dr["Category"]).Trim()));
          if (ic == null) 
            throw new InvalidOperationException(string.Format("Could not find Category.  ({0}={1})", es.Name, dr["Category"]));

          if (ii == null)
          {
            ii = new InventoryItem();
            ii.Description = ((string)dr["Description"]).Trim();
            ii.Category = ic;
            ii.ExternalReferences.Add(new InventoryItemReference() { ExternalSystem = es, Reference = ((string)dr["ItemCode"]).Trim() });
            col.Add(ii);
          }
        }

        return Persist<InventoryItem>(col);
      }
      catch (Exception ex)
      {
        throw new ImportException("An error occured during the import.", ex);
      }
    }

    #endregion
  }
}

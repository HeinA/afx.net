﻿using Afx.Business.Collections;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace InventoryControl.Business.Service
{
  public partial interface IInventoryControlService
  {
    [OperationContract]
    BasicCollection<InventoryControl.Business.InventoryCategory> LoadInventoryCategoryCollection();

    [OperationContract]
    BasicCollection<InventoryControl.Business.InventoryCategory> SaveInventoryCategoryCollection(BasicCollection<InventoryControl.Business.InventoryCategory> col);
  }
}


﻿using Afx.Business.Collections;
using Afx.Business.Service;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.WCF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace InventoryControl.Business.Service
{
  public partial class InventoryControlService
  {
    public BasicCollection<InventoryControl.Business.InventoryItem> LoadInventoryItemCollection()
    {
      try
      {
        return GetAllInstances<InventoryControl.Business.InventoryItem>();
      }
      catch
      {
        throw;
      }
    }

    public BasicCollection<InventoryControl.Business.InventoryItem> SaveInventoryItemCollection(BasicCollection<InventoryControl.Business.InventoryItem> col)
    {
      try
      {
        return Persist<InventoryControl.Business.InventoryItem>(col);
      }
      catch
      {
        throw;
      }
    }
  }
}
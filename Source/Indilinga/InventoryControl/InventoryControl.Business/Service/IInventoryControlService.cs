﻿using Afx.Business;
using Afx.Business.Collections;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace InventoryControl.Business.Service
{
  [ServiceContract(Namespace = Namespace.InventoryControl)]
  public partial interface IInventoryControlService : IDisposable
  {
    #region Import

    [OperationContract]
    BasicCollection<InventoryCategory> ImportInventoryCategories(ExternalSystem es);

    [OperationContract]
    BasicCollection<InventoryItem> ImportInventoryItems(ExternalSystem es);

    #endregion
  }
}

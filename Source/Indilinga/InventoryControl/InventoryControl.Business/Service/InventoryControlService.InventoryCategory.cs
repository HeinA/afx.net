﻿using Afx.Business.Collections;
using Afx.Business.Service;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.WCF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace InventoryControl.Business.Service
{
  public partial class InventoryControlService
  {
    public BasicCollection<InventoryControl.Business.InventoryCategory> LoadInventoryCategoryCollection()
    {
      try
      {
        return GetAllInstances<InventoryControl.Business.InventoryCategory>();
      }
      catch
      {
        throw;
      }
    }

    public BasicCollection<InventoryControl.Business.InventoryCategory> SaveInventoryCategoryCollection(BasicCollection<InventoryControl.Business.InventoryCategory> col)
    {
      try
      {
        return Persist<InventoryControl.Business.InventoryCategory>(col);
      }
      catch
      {
        throw;
      }
    }
  }
}
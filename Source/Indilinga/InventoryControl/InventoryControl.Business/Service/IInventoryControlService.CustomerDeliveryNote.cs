﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace InventoryControl.Business.Service
{
  public partial interface IInventoryControlService
  {
    [OperationContract]
    InventoryControl.Business.CustomerDeliveryNote LoadCustomerDeliveryNote(Guid globalIdentifier);

    [OperationContract]
    InventoryControl.Business.CustomerDeliveryNote SaveCustomerDeliveryNote(InventoryControl.Business.CustomerDeliveryNote obj);
  }
}

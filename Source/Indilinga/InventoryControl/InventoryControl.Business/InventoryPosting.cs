﻿using Afx.Business;
using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Documents;
using Afx.Business.Security;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace InventoryControl.Business
{
  [PersistantObject(Schema = "IC")]
  public partial class InventoryPosting : BusinessObject 
  {
    #region Constructors

    public InventoryPosting()
    {
    }

    #endregion

    #region DateTime Timestamp

    public const string TimestampProperty = "Timestamp";
    [DataMember(Name = TimestampProperty, EmitDefaultValue = false)]
    DateTime mTimestamp;
    [PersistantProperty]
    public DateTime Timestamp
    {
      get { return mTimestamp; }
      set
      {
        if (SetProperty<DateTime>(ref mTimestamp, value))
        {
          Year = Timestamp.Year;
          Period = Timestamp.Month;
        }
      }
    }

    #endregion

    #region int Year

    public const string YearProperty = "Year";
    [DataMember(Name = YearProperty, EmitDefaultValue = false)]
    int mYear;
    [PersistantProperty]
    public int Year
    {
      get { return mYear; }
      set { SetProperty<int>(ref mYear, value); }
    }

    #endregion

    #region int Period

    public const string PeriodProperty = "Period";
    [DataMember(Name = PeriodProperty, EmitDefaultValue = false)]
    int mPeriod;
    [PersistantProperty]
    public int Period
    {
      get { return mPeriod; }
      set { SetProperty<int>(ref mPeriod, value); }
    }

    #endregion

    #region OrganizationalUnit OrganizationalUnit

    public const string OrganizationalUnitProperty = "OrganizationalUnit";
    [PersistantProperty]
    public OrganizationalUnit OrganizationalUnit
    {
      get { return GetCachedObject<OrganizationalUnit>(); }
      set { SetCachedObject<OrganizationalUnit>(value); }
    }

    #endregion

    #region InventoryItem InventoryItem

    public const string InventoryItemProperty = "InventoryItem";
    [PersistantProperty]
    public InventoryItem InventoryItem
    {
      get { return GetCachedObject<InventoryItem>(); }
      set { SetCachedObject<InventoryItem>(value); }
    }

    #endregion

    #region Document Document

    public const string DocumentProperty = "Document";
    [DataMember(Name = DocumentProperty, EmitDefaultValue = false)]
    Document mDocument;
    [PersistantProperty]
    public Document Document
    {
      get { return mDocument; }
      set { SetProperty<Document>(ref mDocument, value); }
    }

    #endregion

    #region InventoryPostingType InventoryPostingType

    public const string InventoryPostingTypeProperty = "InventoryPostingType";
    [DataMember(Name = InventoryPostingTypeProperty, EmitDefaultValue = false)]
    InventoryPostingType mInventoryPostingType;
    [PersistantProperty]
    public InventoryPostingType InventoryPostingType
    {
      get { return mInventoryPostingType; }
      set { SetProperty<InventoryPostingType>(ref mInventoryPostingType, value); }
    }

    #endregion

    #region decimal Quantity

    public const string QuantityProperty = "Quantity";
    [DataMember(Name = QuantityProperty, EmitDefaultValue = false)]
    decimal mQuantity;
    [PersistantProperty]
    public decimal Quantity
    {
      get { return mQuantity; }
      set { SetProperty<decimal>(ref mQuantity, value); }
    }

    #endregion

    #region InventoryItemQUOM QUOM

    public const string QUOMProperty = "QUOM";
    [DataMember(Name = QUOMProperty, EmitDefaultValue = false)]
    InventoryItemQUOM mQUOM;
    [PersistantProperty]
    public InventoryItemQUOM QUOM
    {
      get { return mQUOM; }
      set { SetProperty<InventoryItemQUOM>(ref mQUOM, value); }
    }

    #endregion

    #region string Remark

    public const string RemarkProperty = "Remark";
    [DataMember(Name = RemarkProperty, EmitDefaultValue = false)]
    string mRemark;
    [PersistantProperty]
    public string Remark
    {
      get { return mRemark; }
      set { SetProperty<string>(ref mRemark, value); }
    }

    #endregion
  }
}

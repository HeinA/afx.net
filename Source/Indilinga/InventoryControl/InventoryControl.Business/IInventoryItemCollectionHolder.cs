﻿using Afx.Business.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryControl.Business
{
  public interface IInventoryItemCollectionHolder
  {
    void CalculateItems();
    OrganizationalUnit OrganizationalUnit { get; }
    IEnumerable<InventoryItem> Items { get; }
    void AddItem(InventoryItem item);
    void RemoveItem(InventoryItem item);
  }
}

﻿using Afx.Business.Data;
using Afx.Business.Documents;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace InventoryControl.Business
{
  [DataContract(IsReference = true, Namespace = Namespace.InventoryControl)]
  public partial class CustomerDeliveryNote
  {
    public const string DocumentTypeIdentifier = "{a8f7003e-8118-436b-8841-3e0cc5c8452f}";
    public class States
    {
      public const string Created = "{c330a201-0396-44f5-9de1-29ed0b5afa02}";
      public const string Posted = "{134357e5-1cca-48ef-b487-95d5941a5cc6}";
    }

    protected CustomerDeliveryNote(DocumentType documentType)
      : base(documentType)
    {
    }
  }
}

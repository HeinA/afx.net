﻿using Afx.Business;
using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Security;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace InventoryControl.Business
{
  [PersistantObject(Schema = "IC")]
  public partial class InventoryItemOrganizationalUnit : AssociativeObject<InventoryItem, OrganizationalUnit>
  {
  }
}

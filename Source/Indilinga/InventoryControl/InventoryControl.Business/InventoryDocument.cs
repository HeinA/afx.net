﻿using AccountManagement.Business;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Documents;
using Afx.Business.Validation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace InventoryControl.Business
{
  [PersistantObject(Schema = "Ic")]
  [DataContract(IsReference = true, Namespace = Namespace.InventoryControl)]
  public abstract class InventoryDocument : Document, IInventoryItemCollectionHolder
  {
    #region Constructors

    protected InventoryDocument(DocumentType documentType)
      : base(documentType)
    {
    }

    #endregion

    #region BusinessObjectCollection<InventoryDocumentItem> Items

    public const string ItemsProperty = "Items";
    BusinessObjectCollection<InventoryDocumentItem> mItems;
    [PersistantCollection]
    [DataMember]
    public BusinessObjectCollection<InventoryDocumentItem> Items
    {
      get { return mItems ?? (mItems = new BusinessObjectCollection<InventoryDocumentItem>(this)); }
    }

    #endregion

    #region IInventoryItemCollectionHolder

    Collection<InventoryItem> mInventoryItems;
    void IInventoryItemCollectionHolder.CalculateItems()
    {
      mInventoryItems = new Collection<InventoryItem>();
      foreach (var di in Items)
      {
        mInventoryItems.Add(di.Item);
      }
    }

    IEnumerable<InventoryItem> IInventoryItemCollectionHolder.Items
    {
      get { return mInventoryItems; }
    }

    InventoryDocumentItem GetItem(InventoryItem item)
    {
      Debug.WriteLine("GetItem()");
      return Items.FirstOrDefault(di => di.Item.Equals(item));
    }

    void IInventoryItemCollectionHolder.AddItem(InventoryItem item)
    {
      if (GetItem(item) != null) return;
      Items.Add(new InventoryDocumentItem() { Item = item });
    }

    void IInventoryItemCollectionHolder.RemoveItem(InventoryItem item)
    {
      InventoryDocumentItem di = GetItem(item);
      if (di == null) return;
      di.IsDeleted = true;
    }

    #endregion
  }
}

﻿using Afx.Business;
using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Validation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace InventoryControl.Business
{
  [PersistantObject(Schema = "IC")]
  public partial class InventoryItemQUOM : BusinessObject<InventoryItem> 
  {
    #region Constructors

    public InventoryItemQUOM()
    {
    }

    public InventoryItemQUOM(bool isNull)
      : base(isNull)
    {
    }

    #endregion

    #region string Text

    public const string TextProperty = "Text";
    [DataMember(Name = TextProperty, EmitDefaultValue = false)]
    string mText;
    [PersistantProperty]
    [Mandatory("Text is a mandatory field.")]
    public string Text
    {
      get { return mText; }
      set { SetProperty<string>(ref mText, value); }
    }

    #endregion

    #region decimal Multiplier

    public const string MultiplierProperty = "Multiplier";
    [DataMember(Name = MultiplierProperty, EmitDefaultValue = false)]
    decimal mMultiplier;
    [PersistantProperty]
    [Mandatory("Multiplier is a mandatory field.")]
    public decimal Multiplier
    {
      get { return mMultiplier; }
      set { SetProperty<decimal>(ref mMultiplier, value); }
    }

    #endregion
  }
}

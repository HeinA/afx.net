﻿using Afx.Business;
using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Security;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace InventoryControl.Business
{
  [PersistantObject(Schema = "IC")]
  [Cache]
  public partial class InventoryItem : BusinessObject, IExternalReferenceCollection, IRevisable
  {
    #region int Revision

    public const string RevisionProperty = "Revision";
    [DataMember(Name = RevisionProperty, EmitDefaultValue = false)]
    int mRevision = 1;
    [PersistantProperty]
    public int Revision
    {
      get { return mRevision; }
      set { SetProperty<int>(ref mRevision, value); }
    }

    #endregion

    #region Constructors

    public InventoryItem()
    {
    }

    public InventoryItem(bool isNull)
      : base(isNull)
    {
    }

    #endregion

    #region string Description

    public const string DescriptionProperty = "Description";
    [DataMember(Name = DescriptionProperty, EmitDefaultValue = false)]
    string mDescription;
    [PersistantProperty]
    public string Description
    {
      get { return mDescription; }
      set { SetProperty<string>(ref mDescription, value); }
    }

    #endregion

    #region InventoryCategory Category

    public const string CategoryProperty = "Category";
    [PersistantProperty]
    public InventoryCategory Category
    {
      get { return GetCachedObject<InventoryCategory>(); }
      set { SetCachedObject<InventoryCategory>(value); }
    }

    #endregion

    #region BusinessObjectCollection<InventoryItemReference> ExternalReferences

    public const string ExternalReferencesProperty = "ExternalReferences";
    BusinessObjectCollection<InventoryItemReference> mExternalReferences;
    [PersistantCollection]
    [DataMember]
    public BusinessObjectCollection<InventoryItemReference> ExternalReferences
    {
      get { return mExternalReferences ?? (mExternalReferences = new BusinessObjectCollection<InventoryItemReference>(this)); }
    }

    #endregion

    #region Associative BusinessObjectCollection<OrganizationalUnit> ApplicableOrganizationalUnits

    public const string ApplicableOrganizationalUnitsProperty = "ApplicableOrganizationalUnits";
    AssociativeObjectCollection<InventoryItemOrganizationalUnit, OrganizationalUnit> mApplicableOrganizationalUnits;
    [PersistantCollection(AssociativeType = typeof(InventoryItemOrganizationalUnit))]
    public BusinessObjectCollection<OrganizationalUnit> ApplicableOrganizationalUnits
    {
      get
      {
        if (mApplicableOrganizationalUnits == null) using (new EventStateSuppressor(this)) { mApplicableOrganizationalUnits = new AssociativeObjectCollection<InventoryItemOrganizationalUnit, OrganizationalUnit>(this); }
        return mApplicableOrganizationalUnits;
      }
    }

    #endregion

    #region BusinessObjectCollection<InventoryItemQUOM> mQuantityUOM

    public const string UnitsProperty = "QuantityUOM";
    BusinessObjectCollection<InventoryItemQUOM> mQuantityUOM;
    [PersistantCollection]
    [DataMember]
    public BusinessObjectCollection<InventoryItemQUOM> QuantityUOM
    {
      get { return mQuantityUOM ?? (mQuantityUOM = new BusinessObjectCollection<InventoryItemQUOM>(this)); }
    }

    #endregion

    #region InventoryItemQUOM StockQUOM

    public const string StockUOMProperty = "StockQUOM";
    [PersistantProperty(PostProcess = true)]
    public InventoryItemQUOM StockQUOM
    {
      get { return GetChildObject<InventoryItemQUOM>(QuantityUOM); }
      set { SetChildObject<InventoryItemQUOM>(value); }
    }

    #endregion

    #region InventoryItemQUOM SaleQUOM

    public const string SaleUOMProperty = "SaleQUOM";
    [PersistantProperty(PostProcess = true)]
    public InventoryItemQUOM SaleQUOM
    {
      get { return GetChildObject<InventoryItemQUOM>(QuantityUOM); }
      set { SetChildObject<InventoryItemQUOM>(value); }
    }

    #endregion

    #region void OnCompositionChanged(...)

    protected override void OnCompositionChanged(CompositionChangedType compositionChangedType, string propertyName, object originalSource, BusinessObject item)
    {
      if ((compositionChangedType == CompositionChangedType.ChildAdded || compositionChangedType == CompositionChangedType.ChildRemoved) && propertyName == ExternalReferencesProperty)
      {
        OnPropertyChanged("ExternalReferenceText");
      }
      base.OnCompositionChanged(compositionChangedType, propertyName, originalSource, item);
    }

    #endregion

    #region IExternalReferenceCollection

    public string ExternalReferenceText
    {
      get { return ExternalReferenceHelper.GetReferenceText(this); }
    }

    IList IExternalReferenceCollection.ExternalReferences
    {
      get { return ExternalReferences; }
    }

    IExternalReference IExternalReferenceCollection.CreateReference(ExternalSystem es, string reference)
    {
      return new InventoryItemReference() { ExternalSystem = es, Reference = reference };
    }

    #endregion
  }
}
﻿using Afx.Business;
using Afx.Business.Data;
using Afx.Business.Security;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace InventoryControl.Business
{
  [DataContract(IsReference = true, Namespace = InventoryControl.Business.Namespace.InventoryControl)]
  [PersistantObject(Schema = "IC")]
  [Export(typeof(Setting))]
  public class GoodsInTransitSettings : Setting
  {
    #region Constructors

    public GoodsInTransitSettings()
    {
    }

    #endregion

    #region string Name

    public override string Name
    {
      get { return "Goods in Transit"; }
    }

    #endregion

    #region string SettingGroupIdentifier

    protected override string SettingGroupIdentifier
    {
      get { return "f6867ad4-210c-4196-ba2a-3ea5946d7ac4"; }
    }

    #endregion

    #region OrganizationalUnit GoodsInTransitLocation

    public const string GoodsInTransitLocationProperty = "GoodsInTransitLocation";
    [PersistantProperty]
    public OrganizationalUnit GoodsInTransitLocation
    {
      get { return GetCachedObject<OrganizationalUnit>(); }
      set { SetCachedObject<OrganizationalUnit>(value); }
    }

    #endregion

  }
}

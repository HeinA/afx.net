﻿using AccountManagement.Business;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Documents;
using Afx.Business.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace InventoryControl.Business
{
  [PersistantObject(Schema = "IC")]
  public partial class CustomerDeliveryNote : InventoryDocument
  {
    #region Constructors

    public CustomerDeliveryNote()
      : base(Cache.Instance.GetObject<DocumentType>(DocumentTypeIdentifier))
    {
    }

    #endregion

    #region Account Account

    public const string AccountProperty = "Account";
    [DataMember(Name = AccountProperty, EmitDefaultValue = false)]
    Account mAccount;
    [PersistantProperty]
    [Mandatory("Account is mandatory.")]
    public Account Account
    {
      get { return mAccount; }
      set { SetProperty<Account>(ref mAccount, value); }
    }

    #endregion
  }
}

﻿using Afx.Business;
using Afx.Business.Activities;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Documents;
using InventoryControl.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace InventoryControl.Prism.Documents.CustomerDeliveryNote
{
  public class CustomerDeliveryNoteHeaderViewModel : ExtensionViewModel<InventoryControl.Business.CustomerDeliveryNote>
  {
    #region Constructors

    [InjectionConstructor]
    public CustomerDeliveryNoteHeaderViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion
  }
}


﻿using Afx.Business;
using Afx.Business.Activities;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Documents;
using InventoryControl.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace InventoryControl.Prism.Documents.CustomerDeliveryNote
{
  public partial class CustomerDeliveryNoteViewModel : MdiDocumentViewModel<CustomerDeliveryNoteContext>
  {
    [InjectionConstructor]
    public CustomerDeliveryNoteViewModel(IController controller)
      : base(controller)
    {
    }

    #region Items

    public IEnumerable<InventoryDocumentItem> Items
    {
      get { return Model.Document.Items; }
    }

    #endregion
  }
}


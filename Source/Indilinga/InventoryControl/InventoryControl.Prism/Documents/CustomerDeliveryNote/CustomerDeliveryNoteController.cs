﻿using AccountManagement.Prism.Extensions.Account;
using Afx.Business;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Documents;
using InventoryControl.Business;
using InventoryControl.Prism.Dialogs.EditInventoryDialog;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryControl.Prism.Documents.CustomerDeliveryNote
{
  public partial class CustomerDeliveryNoteController : MdiDocumentController<CustomerDeliveryNoteContext, CustomerDeliveryNoteViewModel>
  {
    [InjectionConstructor]
    public CustomerDeliveryNoteController(IController controller)
      : base(controller)
    {
    }

    protected override void ExtendDocument()
    {
      DocumentAccountSelectionViewModel vm = ExtendDocument<DocumentAccountSelectionViewModel>(Afx.Prism.RibbonMdi.Documents.Regions.HeaderRegion);
      vm.Bind(DataContext.Document, InventoryControl.Business.CustomerDeliveryNote.AccountProperty);

      base.ExtendDocument();
    }

    protected override void ExecuteOperation(Operation op, BusinessObject argument)
    {
      switch (op.Identifier)
      {
        case Operations.EditInventory:
          {
            EditInventoryDialogController c = CreateChildController<EditInventoryDialogController>();
            c.ItemCollectionHolder = DataContext.Document;
            c.Run();
          }
          break;
      }
      base.ExecuteOperation(op, argument);
    }
  }
}


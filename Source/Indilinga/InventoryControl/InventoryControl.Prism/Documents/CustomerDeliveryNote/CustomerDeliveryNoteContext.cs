﻿using Afx.Business.Security;
using Afx.Business.Service;
using Afx.Prism.RibbonMdi.Documents;
using InventoryControl.Business;
using InventoryControl.Business.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryControl.Prism.Documents.CustomerDeliveryNote
{
  public partial class CustomerDeliveryNoteContext : DocumentContext<InventoryControl.Business.CustomerDeliveryNote>
  {
    public CustomerDeliveryNoteContext()
      : base(InventoryControl.Business.CustomerDeliveryNote.DocumentTypeIdentifier)
    {
    }

    public CustomerDeliveryNoteContext(Guid documentIdentifier)
      : base(InventoryControl.Business.CustomerDeliveryNote.DocumentTypeIdentifier, documentIdentifier)
    {
    }

    public override void SaveData()
    {
      using (var svc = ProxyFactory.GetService<IInventoryControlService>(SecurityContext.ServerName))
      {
        Document = svc.SaveCustomerDeliveryNote(Document);
      }
      base.SaveData();
    }

    public override void LoadData()
    {
      using (var svc = ProxyFactory.GetService<IInventoryControlService>(SecurityContext.ServerName))
      {
        Document = svc.LoadCustomerDeliveryNote(DocumentIdentifier);
      }
      base.LoadData();
    }
  }
}


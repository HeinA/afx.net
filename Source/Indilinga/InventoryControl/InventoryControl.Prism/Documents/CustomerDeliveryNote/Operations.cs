﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryControl.Prism.Documents.CustomerDeliveryNote
{
  public class Operations : Afx.Prism.RibbonMdi.StandardOperations
  {
      public const string EditInventory = "{f4c84e44-747d-408e-b5cf-dab3fc5d79ea}";
      public const string SetVehicle = "{d03a113b-607e-4b03-b616-9ce1b951a891}";
      public const string SetDefaultWeight = "{9e958a90-97f0-4c7f-8bd6-65d9b8aa7574}";
      public const string AddWeight = "{f94a82e0-0907-4c82-a575-87bb12679eba}";
  }
}
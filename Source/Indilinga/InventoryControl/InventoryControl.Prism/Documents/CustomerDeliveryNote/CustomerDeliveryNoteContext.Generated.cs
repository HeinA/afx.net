﻿using Afx.Business.Security;
using Afx.Business.Service;
using Afx.Prism.RibbonMdi.Documents;
using InventoryControl.Business;
using InventoryControl.Business.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.Composition;

namespace InventoryControl.Prism.Documents.CustomerDeliveryNote
{
  [Export("Context:" + InventoryControl.Business.CustomerDeliveryNote.DocumentTypeIdentifier)]
  public partial class CustomerDeliveryNoteContext
  {
  }
}


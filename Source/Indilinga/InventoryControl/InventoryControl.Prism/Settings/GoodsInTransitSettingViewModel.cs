﻿using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using InventoryControl.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryControl.Prism.Settings
{
  [Export(typeof(MdiSettingViewModel<GoodsInTransitSettings>))]
  public class GoodsInTransitSettingViewModel : MdiSettingViewModel<GoodsInTransitSettings>
  {
    #region Constructors

    [InjectionConstructor]
    public GoodsInTransitSettingViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    #region OrganizationalUnit GoodsInTransitLocation

    IEnumerable<OrganizationalUnit> mOrganizationalUnits;
    public IEnumerable<OrganizationalUnit> OrganizationalUnits
    {
      get { return mOrganizationalUnits ?? (mOrganizationalUnits = Cache.Instance.GetObjects<OrganizationalUnit>(true, ou => ou.Name, true)); }
    }

    public OrganizationalUnit GoodsInTransitLocation
    {
      get { return Model == null ? GetDefaultValue<OrganizationalUnit>() : Model.GoodsInTransitLocation; }
      set { Model.GoodsInTransitLocation = value; }
    }

    #endregion

  }
}

﻿using Afx.Business.Documents;
using Afx.Prism;
using Afx.Prism.Documents;
using Afx.Prism.RibbonMdi.Documents;
using InventoryControl.Business;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryControl.Prism.Reports.CustomerDeliveryNote
{
  [Export(InventoryControl.Business.CustomerDeliveryNote.DocumentTypeIdentifier, typeof(IDocumentPrintInfo))]
  public class CustomerDeliveryNoteInfo : IDocumentPrintInfo
  {
    public int Priority
    {
      get { return 100; }
    }

    public Stream ReportStream(object argument)
    {
      return this.GetType().Assembly.GetManifestResourceStream("InventoryControl.Prism.Reports.CustomerDeliveryNote.CustomerDeliveryNote.rdlc"); 
    }

    public object GetScope(IController controller)
    {
      return null;
    }

    public void RefreshReportData(LocalReport report, Document document, object scope, object argument)
    {
    }

    public short Copies
    {
      get { return 1; }
    }
  }
}

﻿using Afx.Prism;
using Afx.Prism.RibbonMdi;
using Afx.Prism.RibbonMdi.Documents;
using InventoryControl.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace InventoryControl.Prism.Dialogs.EditInventoryDialog
{
  public class EditInventoryDialogController : MdiDialogController<EditInventoryDialogContext, EditInventoryDialogViewModel>
  {
    public const string EditInventoryDialogControllerKey = "InventoryControl.Prism.Dialogs.EditInventoryDialog.EditInventoryDialogController";

    public EditInventoryDialogController(IController controller)
      : base(EditInventoryDialogControllerKey, controller)
    {
    }

    Collection<InventoryItem> mItems = new Collection<InventoryItem>();
    public Collection<InventoryItem> Items
    {
      get { return mItems; }
      set { mItems = value; }
    }

    #region IInventoryItemCollectionHolder SourceItems

    IInventoryItemCollectionHolder mItemCollectionHolder;
    public IInventoryItemCollectionHolder ItemCollectionHolder
    {
      get { return mItemCollectionHolder; }
      set
      {
        mItemCollectionHolder = value;
        ItemCollectionHolder.CalculateItems();
        foreach (var i in ItemCollectionHolder.Items)
        {
          if (!i.IsDeleted) Items.Add(i);
        }
      }
    }

    #endregion

    //#region IMdiDocumentController DocumentController

    //IMdiDocumentController mInventoryDocumentController;
    //[Dependency]
    //public IMdiDocumentController InventoryDocumentController
    //{
    //  get { return mInventoryDocumentController; }
    //  set
    //  {
    //    mInventoryDocumentController = value;
    //    Items = new Collection<InventoryItem>();
    //    InventoryDocument doc = null;
    //    try
    //    {
    //      doc = (InventoryDocument)mInventoryDocumentController.DataContext.Document;
    //    }
    //    catch
    //    {
    //      throw new InvalidOperationException("Calling controller's DataContext must contain a document of type InventoryDocument");
    //    }
    //    foreach (var i in doc.Items)
    //    {
    //      if (!i.IsDeleted) Items.Add(i.Item);
    //    }
    //  }
    //}

    //#endregion

    protected override bool OnRun()
    {
      if (DataContext == null) DataContext = new EditInventoryDialogContext();
      ViewModel.Caption = "Edit Inventory";
      return base.OnRun();
    }

    protected override void ApplyChanges()
    {
      //InventoryDocument doc = null;
      //try
      //{
      //  doc = (InventoryDocument)mInventoryDocumentController.DataContext.Document;
      //}
      //catch
      //{
      //  throw new InvalidOperationException("Calling controller's DataContext must contain a document of type InventoryDocument");
      //}

      foreach (var i in Items)
      {
        ItemCollectionHolder.AddItem(i);
        //InventoryDocumentItem ii = ItemCollectionHolder.Items.FirstOrDefault(ii1 => ii1.Item.Equals(i));
        //if (ii == null)
        //{
        //  ii = new InventoryDocumentItem() { Item = i };
        //  ItemCollectionHolder.Items.Add(ii);
        //}
        //else
        //{
        //  ii.IsDeleted = false;
        //}
      }

      Queue<InventoryItem> removed = new Queue<InventoryItem>();

      foreach (var ii in ItemCollectionHolder.Items)
      {
        if (!Items.Contains(ii))
        {
          removed.Enqueue(ii);
          //ItemCollectionHolder.RemoveItem(ii);
          //ii.IsDeleted = true;
        }
      }

      while (removed.Count > 0)
      {
        ItemCollectionHolder.RemoveItem(removed.Dequeue());
      }
      //IRegion tabRegion = WeighbridgeTicketController.RegionManager.Regions[Afx.Prism.RibbonMdi.Documents.Regions.TabRegion];
      //foreach (object o in tabRegion.Views)
      //{
      //  if (o is WeighbridgeTicketInventoryViewModel)
      //  {
      //    tabRegion.Activate(o);
      //  }
      //}
      base.ApplyChanges();
    }
  }
}

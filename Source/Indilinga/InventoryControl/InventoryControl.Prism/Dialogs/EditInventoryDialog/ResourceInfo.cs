﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryControl.Prism.Dialogs.EditInventoryDialog
{
  [Export(typeof(Afx.Prism.ResourceInfo))]
  public class ResourceInfo : Afx.Prism.ResourceInfo
  {
    public override Uri GetUri()
    {
      return new Uri("pack://application:,,,/InventoryControl.Prism;component/Dialogs/EditInventoryDialog/Resources.xaml", UriKind.Absolute);
    }

    public override int Priority
    {
      get { return 90; }
    }
  }
}
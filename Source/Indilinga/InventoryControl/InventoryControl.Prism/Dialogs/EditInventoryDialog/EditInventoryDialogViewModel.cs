﻿using Afx.Business.Data;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using InventoryControl.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryControl.Prism.Dialogs.EditInventoryDialog
{
  public class EditInventoryDialogViewModel : MdiDialogViewModel<EditInventoryDialogContext>
  {
    [InjectionConstructor]
    public EditInventoryDialogViewModel(IController controller)
      : base(controller)
    {
    }

    #region EditInventoryDialogController EditInventoryDialogController

    EditInventoryDialogController mEditInventoryDialogController;
    [Dependency]
    public EditInventoryDialogController EditInventoryDialogController
    {
      get { return mEditInventoryDialogController; }
      set { mEditInventoryDialogController = value; }
    }

    #endregion

    IList mItems;
    IList Items
    {
      get { return mItems ?? (mItems = Cache.Instance.GetObjects<InventoryItem>(ii => ii.ApplicableOrganizationalUnits.Contains(EditInventoryDialogController.ItemCollectionHolder.OrganizationalUnit), ii => ii.Description, true)); }
    }

    public IEnumerable<ItemViewModel> ItemViewModels
    {
      get { return ResolveViewModels<ItemViewModel, InventoryItem>(Items); }
    }
  }
}

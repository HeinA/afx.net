﻿using Afx.Prism;
using InventoryControl.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryControl.Prism.Dialogs.EditInventoryDialog
{
  public class ItemViewModel : SelectableViewModel<InventoryItem>
  {
    #region Constructors

    [InjectionConstructor]
    public ItemViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    #region EditInventoryDialogController EditInventoryDialogController

    EditInventoryDialogController mEditInventoryDialogController;
    [Dependency]
    public EditInventoryDialogController EditInventoryDialogController
    {
      get { return mEditInventoryDialogController; }
      set { mEditInventoryDialogController = value; }
    }

    #endregion

    #region string Description

    public string Description
    {
      get { return Model == null ? GetDefaultValue<string>() : Model.Description; }
      set { Model.Description = value; }
    }

    #endregion

    #region bool IsChecked

    public bool IsChecked
    {
      get { return EditInventoryDialogController.Items.Contains(Model); }
      set
      {
        if (value)
        {
          if (!EditInventoryDialogController.Items.Contains(Model)) EditInventoryDialogController.Items.Add(Model);
        }
        else
        {
          if (EditInventoryDialogController.Items.Contains(Model)) EditInventoryDialogController.Items.Remove(Model);
        }
      }
    }

    #endregion
  }
}

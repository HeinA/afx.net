﻿using Afx.Business.Activities;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Business.Service;
using InventoryControl.Business;
using InventoryControl.Business.Service;
using Afx.Prism.RibbonMdi.Activities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.Composition;

namespace InventoryControl.Prism.Activities.InventoryCategoryManagement
{
  public partial class InventoryCategoryManagement : SimpleActivityContext<InventoryCategory>
  {
    public InventoryCategoryManagement()
    {
    }

    public InventoryCategoryManagement(ContextualActivity activity)
      : base(activity)
    {
    }
  }
}

﻿using InventoryControl.Business;
using InventoryControl.Business.Service;
using Afx.Business.Service;
using Afx.Prism.RibbonMdi.Activities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.Composition;
using Afx.Business.Security;

namespace InventoryControl.Prism.Activities.InventoryCategoryManagement
{
  //**************************************************************************************************************
  //**************************************************************************************************************
  //*                                                                                                            *
  //*     This code is auto-generated.  Do not place any cutom code here, it will be lost on re-generation.      *
  //*                                                                                                            *
  //**************************************************************************************************************
  //**************************************************************************************************************
  [Export("Context:" + InventoryCategoryManagement.Key)]
  public partial class InventoryCategoryManagement
  {
    public const string Key = "{dfb43e55-d6cc-46eb-ae3c-65bafa14df6d}";

    public override void LoadData()
    {
      using (var svc = ProxyFactory.GetService<IInventoryControlService>(SecurityContext.MasterServer.Name))
      {
        Data = svc.LoadInventoryCategoryCollection();
      }
      base.LoadData();
    }

    public override void SaveData()
    {
      using (var svc = ProxyFactory.GetService<IInventoryControlService>(SecurityContext.MasterServer.Name))
      {
        Data = svc.SaveInventoryCategoryCollection(Data);
      }
      base.SaveData();
    }
  }
}

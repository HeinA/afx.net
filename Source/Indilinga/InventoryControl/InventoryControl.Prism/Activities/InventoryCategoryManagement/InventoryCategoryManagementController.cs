﻿using Afx.Business;
using Afx.Business.Activities;
using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Business.Service;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using Afx.Prism.RibbonMdi.Activities;
using Afx.Prism.RibbonMdi.Dialogs.EditReferences;
using Afx.Prism.RibbonMdi.Dialogs.Import;
using InventoryControl.Business;
using InventoryControl.Business.Service;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryControl.Prism.Activities.InventoryCategoryManagement
{
  public partial class InventoryCategoryManagementController : MdiSimpleActivityController<InventoryCategoryManagement, InventoryCategoryManagementViewModel>, IImportController
  {
    #region Constructors

    [InjectionConstructor]
    public InventoryCategoryManagementController(IController controller)
      : base(controller)
    {
    }

    #endregion

    #region OnLoaded
    
    protected override void OnLoaded()
    {
      ViewModel.IsFocused = true;
      base.OnLoaded();
    }

    #endregion
    
    #region ExecuteOperation
    
    protected override void ExecuteOperation(Operation op, BusinessObject argument)
    {
      switch (op.Identifier)
      {
        case Operations.Import:
          ImportDialogController ic = CreateChildController<ImportDialogController>();
          ic.ImportController = this;
          ic.Run();
          break;
      }

      base.ExecuteOperation(op, argument);
    }
    
    #endregion

    public void DoImport(ExternalSystem system, string identifier)
    {
      try
      {
        using (var svc = ProxyFactory.GetService<IInventoryControlService>(SecurityContext.MasterServer.Name))
        {
          DataContext.Data = svc.ImportInventoryCategories(system);
          DataContextUpdated();
          DataContext.IsDirty = false;
        }
      }
      catch (Exception ex)
      {
        if (ExceptionHelper.HandleException(ex)) throw;
      }
    }

    public void ExecuteEditReferences(InventoryCategory ic)
    {
      EditReferencesDialogController rc = CreateChildController<EditReferencesDialogController>();
      rc.DataContext = new EditReferencesDialogContext() { ExternalReferenceCollection = ic };
      rc.Run();
    }
  }
}
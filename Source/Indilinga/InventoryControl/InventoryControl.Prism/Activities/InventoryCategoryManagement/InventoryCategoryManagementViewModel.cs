﻿using Afx.Business.Security;
using Afx.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Activities;
using InventoryControl.Business;
using Microsoft.Practices.Prism.Commands;

namespace InventoryControl.Prism.Activities.InventoryCategoryManagement
{
  public partial class InventoryCategoryManagementViewModel : MdiSimpleActivityViewModel<InventoryCategoryManagement>
  {
    #region Constructors

    [InjectionConstructor]
    public InventoryCategoryManagementViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    [Dependency]
    public new InventoryCategoryManagementController Controller { get; set; }

    #region DelegateCommand<InventoryCategory> EditReferencesCommand

    DelegateCommand<InventoryCategory> mEditReferencesCommand;
    public DelegateCommand<InventoryCategory> EditReferencesCommand
    {
      get { return mEditReferencesCommand ?? (mEditReferencesCommand = new DelegateCommand<InventoryCategory>(ExecuteEditReferences, CanExecuteEditReferences)); }
    }

    bool CanExecuteEditReferences(InventoryCategory args)
    {
      return true;
    }

    void ExecuteEditReferences(InventoryCategory args)
    {
      Controller.ExecuteEditReferences(args);
    }

    #endregion
  }
}

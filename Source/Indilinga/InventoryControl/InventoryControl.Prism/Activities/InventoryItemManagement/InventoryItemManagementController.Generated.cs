﻿using InventoryControl.Business;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.Composition;
using Microsoft.Practices.Prism;

namespace InventoryControl.Prism.Activities.InventoryItemManagement
{
  //**************************************************************************************************************
  //**************************************************************************************************************
  //*                                                                                                            *
  //*     This code is auto-generated.  Do not place any cutom code here, it will be lost on re-generation.      *
  //*                                                                                                            *
  //**************************************************************************************************************
  //**************************************************************************************************************
  [Export("Controller:" + global::InventoryControl.Prism.Activities.InventoryItemManagement.InventoryItemManagement.Key)]
  public partial class InventoryItemManagementController
  {
    void CreateRootItem()
    {
      InventoryItem o = new InventoryItem();
      DataContext.Data.Add(o);
      SelectContextViewModel(o);
      FocusViewModel<InventoryItemDetailViewModel>(o);
    }

    void AddDetailsViewModel(object viewModel)
    {
      RegionManager.Regions[InventoryItemManagement.DetailsRegion].Add(viewModel);
    }

    void AddTabViewModel(object viewModel)
    {
      IRegion tabRegion = RegionManager.Regions[InventoryItemManagement.TabRegion];
      tabRegion.Add(viewModel);
      IActiveAware aw = viewModel as IActiveAware;
      if (aw != null && aw.IsActive) tabRegion.Activate(viewModel);
    }
  }
}

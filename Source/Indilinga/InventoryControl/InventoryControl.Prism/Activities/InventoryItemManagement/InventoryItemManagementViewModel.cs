﻿using Afx.Business.Security;
using Afx.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Activities;
using InventoryControl.Business;

namespace InventoryControl.Prism.Activities.InventoryItemManagement
{
  public partial class InventoryItemManagementViewModel : MdiContextualActivityViewModel<InventoryItemManagement>
  {
    #region Constructors

    [InjectionConstructor]
    public InventoryItemManagementViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion
  }
}

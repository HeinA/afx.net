﻿using Afx.Business.Activities;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Business.Service;
using InventoryControl.Business;
using InventoryControl.Business.Service;
using Afx.Prism.RibbonMdi.Activities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.Composition;
  
namespace InventoryControl.Prism.Activities.InventoryItemManagement
{
  //**************************************************************************************************************
  //**************************************************************************************************************
  //*                                                                                                            *
  //*     This code is auto-generated.  Do not place any cutom code here, it will be lost on re-generation.      *
  //*                                                                                                            *
  //**************************************************************************************************************
  //**************************************************************************************************************
  [Export("Context:" + InventoryItemManagement.Key)]
  public partial class InventoryItemManagement
  {
    public const string Key = "{c9a38679-baa3-49c3-9758-0ea36c379dcf}";

    public override void LoadData()
    {
      using (var svc = ProxyFactory.GetService<IInventoryControlService>(SecurityContext.MasterServer.Name))
      {
        Data = svc.LoadInventoryItemCollection();
      }
      base.LoadData();
    }

    public override void SaveData()
    {
      using (var svc = ProxyFactory.GetService<IInventoryControlService>(SecurityContext.MasterServer.Name))
      {
        Data = svc.SaveInventoryItemCollection(Data);
      }
      base.SaveData();
    }
  }
}

﻿using Afx.Business;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Activities;
using InventoryControl.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryControl.Prism.Activities.InventoryItemManagement
{
  public class InventoryItemReferencesViewModel : ActivityTabDetailViewModel<InventoryItem>
  {
    #region Constructors

    [InjectionConstructor]
    public InventoryItemReferencesViewModel(IController controller)
      : base(controller)
    {
      Title = "_References";
    }

    #endregion

    BasicCollection<ExternalSystem> mExternalSystems;
    public IEnumerable<ExternalSystem> ExternalSystems
    {
      get { return mExternalSystems ?? (mExternalSystems = Cache.Instance.GetObjects<ExternalSystem>(true, es => es.Name, true)); }
    }

    #region IEnumerable<InventoryItemReference> ExternalReferences

    public IEnumerable<InventoryItemReference> ExternalReferences
    {
      get
      {
        if (Model == null) return null;
        return Model.ExternalReferences;
      }
    }

    #endregion
  }
}

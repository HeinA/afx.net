﻿using Afx.Business.Activities;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Business.Service;
using InventoryControl.Business;
using InventoryControl.Business.Service;
using Afx.Prism.RibbonMdi.Activities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryControl.Prism.Activities.InventoryItemManagement
{
  public partial class InventoryItemManagement : ContextualActivityContext<InventoryItem>
  {
    public const string TabRegion = "TabRegion";
    public const string DetailsRegion = "DetailsRegion";

    public InventoryItemManagement()
    {
    }

    public InventoryItemManagement(ContextualActivity activity)
      : base(activity)
    {
    }
  }
}

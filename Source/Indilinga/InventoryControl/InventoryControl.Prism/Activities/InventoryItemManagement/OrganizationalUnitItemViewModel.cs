﻿using Afx.Business.Security;
using Afx.Prism;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryControl.Prism.Activities.InventoryItemManagement
{
  public class OrganizationalUnitItemViewModel : SelectableViewModel<OrganizationalUnit>
  {
    #region Constructors

    [InjectionConstructor]
    public OrganizationalUnitItemViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    #region OrganizationalUnitsViewModel Parent

    public new OrganizationalUnitsViewModel Parent
    {
      get { return (OrganizationalUnitsViewModel)base.Parent; }
    }

    #endregion

    #region string Name

    public string Name
    {
      get
      {
        if (Model == null) return default(string);
        return Model.Name;
      }
      set { Model.Name = value; }
    }

    #endregion

    #region bool IsSelected

    public override bool IsSelected
    {
      get
      {
        if (base.IsSelected)
        {
          Parent.SelectedOrganizationalUnitItemViewModel = this;
        }
        return base.IsSelected;
      }
      set
      {
        base.IsSelected = value;
        if (base.IsSelected)
        {
          Parent.SelectedOrganizationalUnitItemViewModel = this;
        }
      }
    }

    #endregion

    #region bool HasOrganizationalUnit

    public bool HasOrganizationalUnit
    {
      get { return Parent.Model.ApplicableOrganizationalUnits.Contains(Model); }
      set
      {
        if (value) Parent.Model.ApplicableOrganizationalUnits.Add(Model);
        else Parent.Model.ApplicableOrganizationalUnits.Remove(Model);
        OnPropertyChanged("HasOrganizationalUnit");
      }
    }

    #endregion
  }
}

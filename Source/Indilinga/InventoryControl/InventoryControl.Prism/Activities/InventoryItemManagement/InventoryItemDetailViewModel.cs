﻿using Afx.Business;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Activities;
using InventoryControl.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryControl.Prism.Activities.InventoryItemManagement
{
  public partial class InventoryItemDetailViewModel : ActivityTabDetailViewModel<InventoryItem>
  {
    #region Constructors

    [InjectionConstructor]
    public InventoryItemDetailViewModel(IController controller)
      : base(controller)
    {
    }
    
    #endregion

    #region string Description

    public string Description
    {
      get { return Model == null ? GetDefaultValue<string>() : Model.Description; }
      set { Model.Description = value; }
    }

    #endregion

    #region InventoryCategory Category

    IEnumerable<InventoryCategory> mCategories;
    public IEnumerable<InventoryCategory> Categories
    {
      get { return mCategories ?? (mCategories = Cache.Instance.GetObjects<InventoryCategory>(ic => ic.Description, true)); }
    }

    public InventoryCategory Category
    {
      get { return Model == null ? GetDefaultValue<InventoryCategory>() : Model.Category; }
      set { Model.Category = value; }
    }

    #endregion

    #region BusinessObjectCollection<InventoryItemQUOM> QuantityUOM

    public BusinessObjectCollection<InventoryItemQUOM> QuantityUOM
    {
      get { return Model == null ? null : Model.QuantityUOM; }
    }

    #endregion

    protected override void OnModelCompositionChanged(CompositionChangedEventArgs e)
    {
      if (e.OriginalSource is InventoryItemQUOM || e.PropertyName == InventoryItem.UnitsProperty)
      {
        this.OnPropertyChanged("QuantityUOMList");
      }
      base.OnModelCompositionChanged(e);
    }

    #region InventoryItemQUOM StockQUOM

    public InventoryItemQUOM StockQUOM
    {
      get { return Model == null ? GetDefaultValue<InventoryItemQUOM>() : Model.StockQUOM; }
      set { Model.StockQUOM = value; }
    }

    #endregion

    #region InventoryItemQUOM SaleQUOM

    public InventoryItemQUOM SaleQUOM
    {
      get { return Model == null ? GetDefaultValue<InventoryItemQUOM>() : Model.SaleQUOM; }
      set { Model.SaleQUOM = value; }
    }

    #endregion

    public IEnumerable<InventoryItemQUOM> QuantityUOMList
    {
      get
      {
        BasicCollection<InventoryItemQUOM> items = new BasicCollection<InventoryItemQUOM>();
        items.Add(new InventoryItemQUOM(true));
        foreach (InventoryItemQUOM i in QuantityUOM.OrderBy(i1 => i1.Text))
        {
          if (!i.IsDeleted) items.Add(i);
        }
        return items;
      }
    }
  }
}

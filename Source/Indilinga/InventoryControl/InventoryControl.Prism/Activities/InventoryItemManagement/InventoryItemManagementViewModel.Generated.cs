﻿using Afx.Business.Security;
using Afx.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Afx.Prism.RibbonMdi.Activities;
using InventoryControl.Business;
using Microsoft.Practices.Prism;

namespace InventoryControl.Prism.Activities.InventoryItemManagement
{
  //**************************************************************************************************************
  //**************************************************************************************************************
  //*                                                                                                            *
  //*     This code is auto-generated.  Do not place any cutom code here, it will be lost on re-generation.      *
  //*                                                                                                            *
  //**************************************************************************************************************
  //**************************************************************************************************************
  public partial class InventoryItemManagementViewModel
  {
    public IEnumerable<InventoryItemItemViewModel> Items
    {
      get
      {
        if (Model == null) return null;
        return this.ResolveViewModels<InventoryItemItemViewModel, InventoryItem>(Model.Data);
      }
    }
  }
}

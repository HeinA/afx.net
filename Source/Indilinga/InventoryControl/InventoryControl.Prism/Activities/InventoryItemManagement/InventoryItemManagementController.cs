﻿using Afx.Business;
using Afx.Business.Activities;
using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Business.Service;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using Afx.Prism.RibbonMdi.Activities;
using Afx.Prism.RibbonMdi.Dialogs.Import;
using InventoryControl.Business;
using InventoryControl.Business.Service;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryControl.Prism.Activities.InventoryItemManagement
{
  public partial class InventoryItemManagementController : MdiContextualActivityController<InventoryItemManagement, InventoryItemManagementViewModel>, IImportController
  {
    #region Constructors

    [InjectionConstructor]
    public InventoryItemManagementController(IController controller)
      : base(controller)
    {
    }

    #endregion

    #region OnLoaded
    
    protected override void OnLoaded()
    {
      if (DataContext.Data.Count > 0)
      {
        //Focus the first element 
        InventoryItemItemViewModel ivm = GetCreateViewModel<InventoryItemItemViewModel>(DataContext.Data[0], ViewModel);
        ivm.IsFocused = true;
      }

      RegisterContextRegion(RegionManager.Regions[InventoryItemManagement.TabRegion]);
      RegisterContextRegion(RegionManager.Regions[InventoryItemManagement.DetailsRegion]);

      base.OnLoaded();
    }

    #endregion
    
    #region OnContextViewModelChanged
    
    protected override void OnContextViewModelChanged(ISelectableViewModel context)
    {
      if (context.Model is InventoryItem)
      {
        InventoryItemDetailViewModel vm = GetCreateViewModel<InventoryItemDetailViewModel>(context.Model, ViewModel);
        AddDetailsViewModel(vm);

        InventoryItemReferencesViewModel rvm = GetCreateViewModel<InventoryItemReferencesViewModel>(context.Model, ViewModel);
        AddTabViewModel(rvm);

        OrganizationalUnitsViewModel ouvm = GetCreateViewModel<OrganizationalUnitsViewModel>(context.Model, ViewModel);
        AddTabViewModel(ouvm);
      }

      base.OnContextViewModelChanged(context);
    }

    #endregion
    
    #region ExecuteOperation
    
    protected override void ExecuteOperation(Operation op, BusinessObject argument)
    {
      switch (op.Identifier)
      {
        case Operations.AddItem:
          CreateRootItem();
          break;

        case Operations.Import:
          ImportDialogController ic = CreateChildController<ImportDialogController>();
          ic.ImportController = this;
          ic.Run();
          break;
      }

      base.ExecuteOperation(op, argument);
    }
    
    #endregion

    public void DoImport(ExternalSystem system, string identifier)
    {
      try
      {
        using (var svc = ProxyFactory.GetService<IInventoryControlService>(SecurityContext.MasterServer.Name))
        {
          DataContext.Data = svc.ImportInventoryItems(system);
          DataContextUpdated();
          DataContext.IsDirty = false;
        }
      }
      catch (Exception ex)
      {
        if (ExceptionHelper.HandleException(ex)) throw;
      }
    }
  }
}

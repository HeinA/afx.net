﻿using Afx.Business;
using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using InventoryControl.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryControl.Prism.Activities.InventoryItemManagement
{
  public partial class InventoryItemItemViewModel : MdiNavigationListItemViewModel<InventoryItem>
  {
    #region Constructors

    [InjectionConstructor]
    public InventoryItemItemViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    #region string Category

    public string Category
    {
      get { return Model == null ? GetDefaultValue<string>() : Model.Category.Description; }
    }

    #endregion

    #region string Description

    public string Description
    {
      get { return Model == null ? GetDefaultValue<string>() : Model.Description; }
    }

    #endregion

  }
}

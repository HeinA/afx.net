﻿using FreightManagement.Business;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParcelLabel_100x50
{
  [Export(typeof(IParcelLabel))]
  public class Label : PrintDocument, IParcelLabel
  {
    public Label()
    {
      StartIndex = 1;
    }

    public string WaybillNumber { get; set; }
    public int StartIndex { get; set; }
    public int EndIndex { get; set; }
    public int TotalLabels { get; set; }
    public DateTime Date { get; set; }
    public string Consigner { get; set; }
    public string Consignee { get; set; }
    public string DeliveryCity { get; set; }
    public string OrganizationalUnit { get; set; }
    public PointF Offset { get; set; }

    float MMtoDPI(float mm, float dpi)
    {
      return mm * dpi / 2.54f / 10f;
    }

    protected override void OnBeginPrint(PrintEventArgs e)
    {
      this.OriginAtMargins = false;
      base.OnBeginPrint(e);
    }

    protected override void OnPrintPage(PrintPageEventArgs e)
    {
      Graphics g = e.Graphics;
      g.PageUnit = GraphicsUnit.Pixel;

      g.TranslateTransform(MMtoDPI(Offset.X, g.DpiX), MMtoDPI(Offset.Y, g.DpiY));

      Font fBarcode = new Font("Code 128", 30f);
      Font fTahomaBig = new Font("Tahoma", 14f);
      Font fTahomaBigBold = new Font("Tahoma", 16f, FontStyle.Bold);
      Font fTahomaSmall = new Font("Tahoma", 10f);
      Font fTahomaSmallBold = new Font("Tahoma", 10f, FontStyle.Bold);
      Font fTahomaSmallSmall = new Font("Tahoma", 8f);
      Font fTahomaVerySmall = new Font("Tahoma", 7f);

      StringFormat formatMC = new StringFormat();
      formatMC.LineAlignment = StringAlignment.Center;
      formatMC.Alignment = StringAlignment.Center;

      StringFormat formatLC = new StringFormat();
      formatLC.LineAlignment = StringAlignment.Center;
      formatLC.Alignment = StringAlignment.Near;

      StringFormat formatRC = new StringFormat();
      formatRC.LineAlignment = StringAlignment.Center;
      formatRC.Alignment = StringAlignment.Far;

      string pn = string.Format("{0}{1:000}{2:000}", WaybillNumber, TotalLabels, StartIndex);
      string bc = BarCode.BarcodeConverter128.StringToBarcode(pn);
      g.RotateTransform(-90f);

      SizeF ipn = g.MeasureString(pn, fTahomaBig);
      SizeF ibc = g.MeasureString(bc, fBarcode);

      g.DrawString(bc, fBarcode, Brushes.Black, (MMtoDPI(50, g.DpiX) - ((MMtoDPI(50, g.DpiX) - ibc.Width) / 2)) * -1, MMtoDPI(2, g.DpiY));
      g.DrawString(pn, fTahomaBig, Brushes.Black, (MMtoDPI(50, g.DpiX) - ((MMtoDPI(50, g.DpiX) - ipn.Width) / 2)) * -1, MMtoDPI(13, g.DpiY));

      g.RotateTransform(90f);

      Pen pen = new Pen(Brushes.Black, 3);

      g.DrawLine(pen, new PointF(MMtoDPI(20, g.DpiX), MMtoDPI(0, g.DpiY)), new PointF(MMtoDPI(20, g.DpiX), MMtoDPI(50, g.DpiY)));

      RectangleF rect1 = new RectangleF(MMtoDPI(20, g.DpiX), MMtoDPI(1, g.DpiY), MMtoDPI(80, g.DpiX), MMtoDPI(6, g.DpiY));
      g.DrawString(string.Format("Date: {0:dd/MM/yyyy}", Date), fTahomaSmall, Brushes.Black, rect1, formatRC);
      g.DrawString(string.Format("Waybill: {0}", WaybillNumber), fTahomaSmall, Brushes.Black, rect1, formatLC);
      g.DrawLine(pen, new PointF(MMtoDPI(20, g.DpiX), MMtoDPI(7, g.DpiY)), new PointF(MMtoDPI(100, g.DpiX), MMtoDPI(7, g.DpiY)));

      RectangleF rect2 = new RectangleF(MMtoDPI(20, g.DpiX), MMtoDPI(7, g.DpiY), MMtoDPI(80, g.DpiX), MMtoDPI(10, g.DpiY));
      g.DrawString(string.Format("To: {0}", Consignee), fTahomaSmallBold, Brushes.Black, rect2, formatLC);
      RectangleF rect3 = new RectangleF(MMtoDPI(20, g.DpiX), MMtoDPI(17, g.DpiY), MMtoDPI(80, g.DpiX), MMtoDPI(6, g.DpiY));
      g.DrawString(string.Format("{0}", DeliveryCity), fTahomaBigBold, Brushes.Black, rect3, formatRC);
      g.DrawLine(pen, new PointF(MMtoDPI(20, g.DpiX), MMtoDPI(23, g.DpiY)), new PointF(MMtoDPI(100, g.DpiX), MMtoDPI(23, g.DpiY)));

      RectangleF rect4 = new RectangleF(MMtoDPI(20, g.DpiX), MMtoDPI(23, g.DpiY), MMtoDPI(80, g.DpiX), MMtoDPI(5, g.DpiY));
      g.DrawString(string.Format("From: {0}", Consigner), fTahomaSmall, Brushes.Black, rect4, formatLC);
      RectangleF rect5 = new RectangleF(MMtoDPI(20, g.DpiX), MMtoDPI(28, g.DpiY), MMtoDPI(80, g.DpiX), MMtoDPI(4, g.DpiY));
      //g.DrawString(string.Format("{0}", Waybill.Reference), fTahomaSmall, Brushes.Black, rect5, formatLC);
      //g.DrawLine(pen, new PointF(MMtoDPI(20, g.DpiX), MMtoDPI(32, g.DpiY)), new PointF(MMtoDPI(100, g.DpiX), MMtoDPI(32, g.DpiY)));

      RectangleF rect6 = new RectangleF(MMtoDPI(68, g.DpiX), MMtoDPI(34, g.DpiY), MMtoDPI(32, g.DpiX), MMtoDPI(12, g.DpiY));
      g.DrawString(string.Format("Package\r\n{0} of {1}", StartIndex, TotalLabels), fTahomaBig, Brushes.Black, rect6, formatRC);

      //RectangleF rect9 = new RectangleF(MMtoDPI(20.5f, g.DpiX), MMtoDPI(33, g.DpiY), MMtoDPI(48, g.DpiX), MMtoDPI(3, g.DpiY));
      //g.DrawString("Courier:", fTahomaVerySmall, Brushes.Black, rect9, formatLC);

      RectangleF rect7 = new RectangleF(MMtoDPI(20, g.DpiX), MMtoDPI(34, g.DpiY), MMtoDPI(48, g.DpiX), MMtoDPI(12, g.DpiY));
      g.DrawString(OrganizationalUnit, fTahomaBig, Brushes.Black, rect7, formatLC);

      RectangleF rect8 = new RectangleF(MMtoDPI(20, g.DpiX), MMtoDPI(47, g.DpiY), MMtoDPI(80, g.DpiX), MMtoDPI(5, g.DpiY));
      g.DrawString("Indilinga Systems Design & Logistics", fTahomaVerySmall, Brushes.Black, rect8, formatRC);
      g.DrawLine(pen, new PointF(MMtoDPI(20, g.DpiX), MMtoDPI(47, g.DpiY)), new PointF(MMtoDPI(100, g.DpiX), MMtoDPI(47, g.DpiY)));

      if (++StartIndex <= EndIndex)
      {
        e.HasMorePages = true;
      }

      base.OnPrintPage(e);
    }

    public void Print(string printer)
    {
      //Index = 0;
      if (!string.IsNullOrWhiteSpace(printer)) this.PrinterSettings.PrinterName = printer;
      this.Print();
    }
  }
}

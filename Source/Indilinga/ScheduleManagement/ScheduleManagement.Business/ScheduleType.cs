﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScheduleManagement.Business
{
  public enum ScheduleType
  {
    Delivery = 1,
    Collection = 2,
    Transport = 3,
    Transfer = 4,
    PODHandover = 5,
    LoadAllocation = 6,
    Maintenance = 7
  }
}

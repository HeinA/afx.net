﻿using Afx.Business;
using Afx.Business.Data;
using System.ComponentModel.Composition;
using System.Runtime.Serialization;

namespace ScheduleManagement.Business
{
  [PersistantObject(Schema = "ScheduleManagement")]
  [Export(typeof(Setting))]
  [DataContract(IsReference = true, Namespace = ScheduleManagement.Business.Namespace.ScheduleManagement)]
  public partial class ScheduleSetting : Setting 
  {
    #region Constructors

    public ScheduleSetting()
    {
    }

    #endregion

    #region string Name

    public override string Name
    {
      get { return "Schedule Planner"; }
    }

    #endregion

    #region string SettingGroupIdentifier

    /// <summary>
    /// GENERAL
    /// </summary>
    protected override string SettingGroupIdentifier
    {
      get { return "46F1A0E8-072E-4EDA-9217-56DBC928E9D5"; }
    }

    #endregion

    

    #region Timescale DefaultTimescale
    
    public const string DefaultTimescaleProperty = "DefaultTimescale";
    [DataMember(Name = DefaultTimescaleProperty, EmitDefaultValue=false)]
    Timescale mDefaultTimescale;
    [PersistantProperty]
    public Timescale DefaultTimescale
    {
      get { return mDefaultTimescale; }
      set { SetProperty<Timescale>(ref mDefaultTimescale, value); }
    }
    
    #endregion

    #region bool RefreshOnSave

    public const string RefreshOnSaveProperty = "RefreshOnSave";
    [DataMember(Name = RefreshOnSaveProperty, EmitDefaultValue = false)]
    bool mRefreshOnSave = false;
    [PersistantProperty]
    public bool RefreshOnSave
    {
      get { return mRefreshOnSave; }
      set { SetProperty<bool>(ref mRefreshOnSave, value); }
    }

    #endregion

    #region bool AutoRefreshTimerEnabled

    public const string AutoRefreshTimerEnabledProperty = "AutoRefreshTimerEnabled";
    [DataMember(Name = AutoRefreshTimerEnabledProperty, EmitDefaultValue = false)]
    bool mAutoRefreshTimerEnabled = false;
    [PersistantProperty]
    public bool AutoRefreshTimerEnabled
    {
      get { return mAutoRefreshTimerEnabled; }
      set { SetProperty<bool>(ref mAutoRefreshTimerEnabled, value); }
    }

    #endregion

    #region int AutoRefreshIntervalMinutes
    
    public const string AutoRefreshIntervalMinutesProperty = "AutoRefreshIntervalMinutes";
    [DataMember(Name = AutoRefreshIntervalMinutesProperty, EmitDefaultValue=false)]
    int mAutoRefreshIntervalMinutes = 15;
    [PersistantProperty]
    public int AutoRefreshIntervalMinutes
    {
      get { return mAutoRefreshIntervalMinutes; }
      set { SetProperty<int>(ref mAutoRefreshIntervalMinutes, value); }
    }
    
    #endregion
  }
}

﻿using Afx.Business.Data;
using Afx.Business.Documents;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ScheduleManagement.Business
{
  [DataContract(IsReference = true, Namespace = Namespace.ScheduleManagement)]
  public partial class MaintenanceBooking
  {
    public const string DocumentTypeIdentifier = "{ee5298bd-b9dd-480a-ba28-8175e1e1e7e3}";
    public class States
    {
      public const string Booked = "{a3aab150-5a28-4d73-8b10-41b8163b3e7d}";
      public const string Cancelled = "{4ab428b1-2ae4-47b7-b91d-8611b6a93fe4}";
      public const string Released = "{40629d42-110f-4f4b-bd2c-a695a4f30961}";
      public const string Testing = "{bf8ab743-b5d7-4105-8319-db874bf60eb9}";
      public const string Workshop = "{c4e432ee-6ad3-4665-9ef5-cc1647e5b09a}";
    }

    protected MaintenanceBooking(DocumentType documentType)
      : base(documentType)
    {
    }
  }
}

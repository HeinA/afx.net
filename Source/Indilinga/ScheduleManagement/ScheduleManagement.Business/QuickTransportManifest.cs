﻿using AccountManagement.Business;
using Afx.Business;
using Afx.Business.Data;
using Afx.Business.Validation;
using ContactManagement.Business;
using FleetManagement.Business;
using FreightManagement.Business;
using Geographics.Business;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ScheduleManagement.Business
{
  public class QuickTransportManifest : BusinessObject
  {
    public QuickTransportManifest() { }

    #region Manifest Information

    #region DateTime? ScheduledOffLoadingDate

    public const string ScheduledOffLoadingDateProperty = "ScheduledOffLoadingDate";
    [DataMember(Name = ScheduledOffLoadingDateProperty, EmitDefaultValue = false)]
    DateTime? mScheduledOffLoadingDate;
    public DateTime? ScheduledOffLoadingDate
    {
      get { return mScheduledOffLoadingDate; }
      set { SetProperty<DateTime?>(ref mScheduledOffLoadingDate, value); }
    }

    #endregion

    #region SlotTime ScheduledLoadingSlot

    public const string ScheduledLoadingSlotProperty = "ScheduledLoadingSlot";
    [DataMember(Name = ScheduledLoadingSlotProperty, EmitDefaultValue = false)]
    SlotTime mScheduledLoadingSlot;
    public SlotTime ScheduledLoadingSlot
    {
      get { return mScheduledLoadingSlot; }
      set { SetProperty<SlotTime>(ref mScheduledLoadingSlot, value); }
    }

    #endregion

    #region SlotTime ScheduledOffLoadingSlot
    
    public const string ScheduledOffLoadingSlotProperty = "ScheduledOffLoadingSlot";
    [DataMember(Name = ScheduledOffLoadingSlotProperty, EmitDefaultValue=false)]
    SlotTime mScheduledOffLoadingSlot;
    public SlotTime ScheduledOffLoadingSlot
    {
      get { return mScheduledOffLoadingSlot; }
      set { SetProperty<SlotTime>(ref mScheduledOffLoadingSlot, value); }
    }
    
    #endregion

    #region DateTime? ScheduledLoadingDate

    public const string ScheduledLoadingDateProperty = "ScheduledLoadingDate";
    [DataMember(Name = ScheduledLoadingDateProperty, EmitDefaultValue = false)]
    DateTime? mScheduledLoadingDate;
    public DateTime? ScheduledLoadingDate
    {
      get { return mScheduledLoadingDate; }
      set { mScheduledLoadingDate = value; }
    }

    #endregion

    #region Vehicle SelectedVehicle

    public const string SelectedVehicleProperty = "SelectedVehicle";
    [DataMember(Name = SelectedVehicleProperty, EmitDefaultValue = false)]
    public Vehicle SelectedVehicle
    {
      get { return GetCachedObject<Vehicle>(); }
      set { SetCachedObject<Vehicle>(value); }
    }

    #endregion
    

    #region decimal FeesNightOut

    public const string FeesNightOutProperty = "FeesNightOut";
    [DataMember(Name = FeesNightOutProperty, EmitDefaultValue = false)]
    decimal mFeesNightOut = 0;
    public decimal FeesNightOut
    {
      get { return mFeesNightOut; }
      set { SetProperty<decimal>(ref mFeesNightOut, value); }
    }

    #endregion

    #region decimal FeesTarpaullin
    
    public const string FeesTarpaullinProperty = "FeesTarpaullin";
    [DataMember(Name = FeesTarpaullinProperty, EmitDefaultValue=false)]
    decimal mFeesTarpaullin = 0;
    public decimal FeesTarpaullin
    {
      get { return mFeesTarpaullin; }
      set { SetProperty<decimal>(ref mFeesTarpaullin, value); }
    }
    
    #endregion

    #region decimal FeesToll
    
    public const string FeesTollProperty = "FeesToll";
    [DataMember(Name = FeesTollProperty, EmitDefaultValue=false)]
    decimal mFeesToll = 0;
    public decimal FeesToll
    {
      get { return mFeesToll; }
      set { SetProperty<decimal>(ref mFeesToll, value); }
    }
    
    #endregion

    #region Drivers

    #region Employee Driver

    public const string DriverProperty = "Driver";
    [DataMember(Name = DriverProperty, EmitDefaultValue = false)]
    Employee mDriver;
    public Employee Driver
    {
      get { return mDriver; }
      set { SetProperty<Employee>(ref mDriver, value); }
    }

    #endregion

    #region Employee CoDriver
    
    public const string CoDriverProperty = "CoDriver";
    [DataMember(Name = CoDriverProperty, EmitDefaultValue=false)]
    Employee mCoDriver;
    public Employee CoDriver
    {
      get { return mCoDriver; }
      set { SetProperty<Employee>(ref mCoDriver, value); }
    }
    
    #endregion

    #region Employee BackupDriver
    
    public const string BackupDriverProperty = "BackupDriver";
    [DataMember(Name = BackupDriverProperty, EmitDefaultValue=false)]
    Employee mBackupDriver;
    public Employee BackupDriver
    {
      get { return mBackupDriver; }
      set { SetProperty<Employee>(ref mBackupDriver, value); }
    }
    
    #endregion

    #endregion

    //Afx.dm decimal FeesForeignInsurance
    //Afx.dm decimal FeesForeignDisks
    //Afx.dm decimal FeesFuel

    #endregion

    #region BorderPosts BorderPost

    public const string BorderPostProperty = "BorderPost";
    [DataMember(Name = BorderPostProperty, EmitDefaultValue = false)]
    BorderPosts mBorderPost;
    public BorderPosts BorderPost
    {
      get { return mBorderPost; }
      set { SetProperty<BorderPosts>(ref mBorderPost, value); }
    }

    #endregion

    #region Waybill Information

    #region AccountReference Account

    public const string AccountProperty = "Account";
    [DataMember(Name = AccountProperty, EmitDefaultValue = false)]
    public AccountReference Account
    {
      get { return GetCachedObject<AccountReference>(); }
      set { SetCachedObject<AccountReference>(value); }
    }

    #endregion

    #region string ClientReferenceNumber

    public const string ClientReferenceNumberProperty = "ClientReferenceNumber";
    [DataMember(Name = ClientReferenceNumberProperty, EmitDefaultValue = false)]
    string mClientReferenceNumber;
    public string ClientReferenceNumber
    {
      get { return mClientReferenceNumber; }
      set { SetProperty<string>(ref mClientReferenceNumber, value); }
    }

    #endregion

    #region string Consigner

    public const string ConsignerProperty = "Consigner";
    [DataMember(Name = ConsignerProperty, EmitDefaultValue = false)]
    string mConsigner;
    public string Consigner
    {
      get { return mConsigner; }
      set { SetProperty<string>(ref mConsigner, value); }
    }

    #endregion

    #region string Consignee

    public const string ConsigneeProperty = "Consignee";
    [DataMember(Name = ConsigneeProperty, EmitDefaultValue = false)]
    string mConsignee;
    public string Consignee
    {
      get { return mConsignee; }
      set { SetProperty<string>(ref mConsignee, value); }
    }

    #endregion

    #region string OriginAddressText

    public const string OriginAddressTextProperty = "OriginAddressText";
    [DataMember(Name = OriginAddressTextProperty, EmitDefaultValue = false)]
    string mOriginAddressText;
    public string OriginAddressText
    {
      get { return mOriginAddressText; }
      set { SetProperty<string>(ref mOriginAddressText, value); }
    }

    #endregion

    #region string DestinationAddressText

    public const string DestinationAddressTextProperty = "DestinationAddressText";
    [DataMember(Name = DestinationAddressTextProperty, EmitDefaultValue = false)]
    string mDestinationAddressText;
    public string DestinationAddressText
    {
      get { return mDestinationAddressText; }
      set { SetProperty<string>(ref mDestinationAddressText, value); }
    }

    #endregion

    #region bool IncludeLocalCollection

    public const string IncludeLocalCollectionProperty = "IncludeLocalCollection";
    [DataMember(Name = IncludeLocalCollectionProperty, EmitDefaultValue = false)]
    bool mIncludeLocalCollection = true;
    public bool IncludeLocalCollection
    {
      get { return mIncludeLocalCollection; }
      set { SetProperty<bool>(ref mIncludeLocalCollection, value); }
    }

    #endregion

    #region bool IncludeLocalDelivery

    public const string IncludeLocalDeliveryProperty = "IncludeLocalDelivery";
    [DataMember(Name = IncludeLocalDeliveryProperty, EmitDefaultValue = false)]
    bool mIncludeLocalDelivery = true;
    public bool IncludeLocalDelivery
    {
      get { return mIncludeLocalDelivery; }
      set { SetProperty<bool>(ref mIncludeLocalDelivery, value); }
    }

    #endregion

    #region Waybill Cargo Item Information

    #region CargoType CargoType

    public const string CargoTypeProperty = "CargoType";
    [DataMember(Name = CargoTypeProperty, EmitDefaultValue = false)]
    public CargoType CargoType
    {
      get { return GetCachedObject<CargoType>(); }
      set { SetCachedObject<CargoType>(value); }
    }

    #endregion


    #region decimal Price

    public const string PriceProperty = "Price";
    [DataMember(Name = PriceProperty, EmitDefaultValue = false)]
    decimal mPrice;
    public decimal Price
    {
      get { return mPrice; }
      set { SetProperty<decimal>(ref mPrice, value); }
    }

    #endregion

    #region int Weight

    public const string WeightProperty = "Weight";
    [DataMember(Name = WeightProperty, EmitDefaultValue = false)]
    int mWeight;
    public int Weight
    {
      get { return mWeight; }
      set { SetProperty<int>(ref mWeight, value); }
    }

    #endregion

    #region string Description

    public const string DescriptionProperty = "Description";
    [DataMember(Name = DescriptionProperty, EmitDefaultValue = false)]
    string mDescription;
    public string Description
    {
      get { return mDescription; }
      set { SetProperty<string>(ref mDescription, value); }
    }

    #endregion

    #region int Items

    public const string ItemsProperty = "Items";
    [DataMember(Name = ItemsProperty, EmitDefaultValue = false)]
    int mItems;
    public int Items
    {
      get { return mItems; }
      set { SetProperty<int>(ref mItems, value); }
    }

    #endregion

    #region decimal DeclaredValue

    public const string DeclaredValueProperty = "DeclaredValue";
    [DataMember(Name = DeclaredValueProperty, EmitDefaultValue = false)]
    decimal mDeclaredValue;
    public decimal DeclaredValue
    {
      get { return mDeclaredValue; }
      set { SetProperty<decimal>(ref mDeclaredValue, value); }
    }

    #endregion

    #endregion

    #endregion

    #region bool CreateTripsheet

    public const string CreateTripsheetProperty = "CreateTripsheet";
    [DataMember(Name = CreateTripsheetProperty, EmitDefaultValue = false)]
    bool mCreateTripsheet = false;
    public bool CreateTripsheet
    {
      get { return mCreateTripsheet; }
      set { SetProperty<bool>(ref mCreateTripsheet, value); }
    }

    #endregion

    #region bool HasTripsheet

    public const string HasTripsheetProperty = "HasTripsheet";
    [DataMember(Name = HasTripsheetProperty, EmitDefaultValue = false)]
    bool mHasTripsheet;
    [PersistantProperty]
    public bool HasTripsheet
    {
      get { return mHasTripsheet; }
      set { SetProperty<bool>(ref mHasTripsheet, value); }
    }

    #endregion

    protected override void GetErrors(Collection<string> errors, string propertyName)
    {
      if (propertyName == null || propertyName == AccountProperty)
      {
        if (BusinessObject.IsNull(Account)) errors.Add("Account is a mandatory field.");
      }

      if (propertyName == null || propertyName == SelectedVehicleProperty)
      {
        if (BusinessObject.IsNull(SelectedVehicle)) errors.Add("Vehicle is a mandatory field.");
      }

      if (propertyName == null || propertyName == CargoTypeProperty)
      {
        if (BusinessObject.IsNull(CargoType)) errors.Add("Cargo Type is a mandatory field.");
      }


      if (propertyName == null || propertyName == ConsignerProperty)
      {
        if (String.IsNullOrWhiteSpace(Consigner)) errors.Add("Consigner is a mandatory field.");
      }

      if (propertyName == null || propertyName == ConsigneeProperty)
      {
        if (String.IsNullOrWhiteSpace(Consignee)) errors.Add("Consignee is a mandatory field.");
      }

      if (propertyName == null || propertyName == OriginAddressTextProperty)
      {
        if (String.IsNullOrWhiteSpace(OriginAddressText)) errors.Add("Collection Address is a mandatory field.");
      }

      if (propertyName == null || propertyName == DestinationAddressTextProperty)
      {
        if (String.IsNullOrWhiteSpace(DestinationAddressText)) errors.Add("Delivary Address is a mandatory field.");
      }




      if (propertyName == null || propertyName == ScheduledLoadingDateProperty)
      {
        if (ScheduledLoadingDate == null) errors.Add("Schedule Loading is a mandatory field.");
        if (((ScheduledOffLoadingDate < (DateTime?)ScheduledLoadingDate) || (ScheduledOffLoadingDate == (DateTime?)ScheduledLoadingDate)) && BusinessObject.IsNull(ScheduledLoadingSlot)) errors.Add("Scheduled Loading Date is greater than Scheduled Off Loading Date.");
      }


      if (propertyName == null || propertyName == ScheduledOffLoadingDateProperty)
      {
        if (ScheduledOffLoadingDate == null) errors.Add("Scheduled Off Loading Date is a mandatory field.");
        if (((ScheduledOffLoadingDate < (DateTime?)ScheduledLoadingDate) || (ScheduledOffLoadingDate == (DateTime?)ScheduledLoadingDate)) && BusinessObject.IsNull(ScheduledOffLoadingSlot)) errors.Add("Scheduled Off Loading Date is earlier than Scheduled Loading Date.");
      }


      if (propertyName == null || (propertyName == ScheduledLoadingSlotProperty || propertyName == ScheduledOffLoadingSlotProperty))
      {
        if (BusinessObject.IsNull(ScheduledLoadingSlot) && ((DateTime?)ScheduledLoadingDate == (DateTime?)ScheduledOffLoadingDate)) errors.Add("Scheduled Loading Slot has to be specified.");
        if (BusinessObject.IsNull(ScheduledOffLoadingSlot) && ((DateTime?)ScheduledLoadingDate == (DateTime?)ScheduledOffLoadingDate)) errors.Add("Scheduled Off Loading Slot has to be specified.");

        if (!BusinessObject.IsNull(ScheduledOffLoadingSlot) && !BusinessObject.IsNull(ScheduledLoadingSlot))
        {
          if (((DateTime)ScheduledOffLoadingDate).AddHours(ScheduledOffLoadingSlot.HH) < ((DateTime)ScheduledLoadingDate).AddHours(ScheduledLoadingSlot.HH)) errors.Add("Scheduled Off Loading slot is earlier than Scheduled Loading slot.");
        }
      }

      base.GetErrors(errors, propertyName);
    }
  }
}

﻿using Afx.Business.Data;
using Afx.Business.Documents;
using FreightManagement.Business;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScheduleManagement.Business.DocumentTransferManagers
{
  [Export(typeof(IDocumentTransferManager))]
  public class TripsheetTransferManager : DocumentTransferManager<Tripsheet>
  {
    public override void AppendRelatedDocuments(Tripsheet document, DocumentTransferCollection documents)
    {
      ObjectRepository<DeliveryManifest> or = PersistanceManager.GetRepository<DeliveryManifest>();

      //foreach (var tr in document.GetAssociativeObjects<TripsheetRoute>())
      //{
        foreach (var mr in document.Manifests)
        {
          DeliveryManifest m = or.GetInstance(mr.GlobalIdentifier);
          documents.Add(m);
        }
      //}
    }
  }
}

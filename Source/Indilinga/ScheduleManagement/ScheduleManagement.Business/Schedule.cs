﻿using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Documents;
using Afx.Business.Validation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace AccountManagement.Business
{
  [PersistantObject(Schema = "Accounts")]
  public partial class Account
  {
    #region Constructors

    public Account()
      : base(Cache.Instance.GetObject<DocumentType>(DocumentTypeIdentifier))
    {
    }

    #endregion

    #region string Name

    public const string NameProperty = "Name";
    [DataMember(Name = NameProperty, EmitDefaultValue = false)]
    string mName;
    [PersistantProperty]
    [Mandatory("Account Name is mandatory.")]
    public string Name
    {
      get { return mName; }
      set { SetProperty<string>(ref mName, value); }
    }

    #endregion

    #region string Alert

    public const string AlertProperty = "Alert";
    [DataMember(Name = AlertProperty, EmitDefaultValue = false)]
    string mAlert;
    [PersistantProperty]
    public string Alert
    {
      get { return mAlert; }
      set { SetProperty<string>(ref mAlert, value); }
    }

    #endregion

    #region void GetErrors(...)

    protected override void GetErrors(Collection<string> errors, string propertyName)
    {
      if (IsPropertyApplicable(propertyName, Document.DocumentNumberProperty))
      {
        if (string.IsNullOrWhiteSpace(DocumentNumber)) errors.Add("Account Number is mandatory.");
      }

      base.GetErrors(errors, propertyName);
    }

    #endregion
  }
}

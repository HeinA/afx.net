﻿using Afx.Business;
using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace AccountManagement.Business
{
  [PersistantObject(Schema = "Accounts", TableName = "vAccountReference")]
  [Cache(Replicate = false)]
  public partial class AccountReference : BusinessObject
  {
    #region Constructors

    public AccountReference()
    {
    }

    public AccountReference(bool isNull)
      : base(isNull)
    {
    }

    #endregion

    #region string AccountNumber

    public const string AccountNumberProperty = "AccountNumber";
    [DataMember(Name = AccountNumberProperty, EmitDefaultValue = false)]
    string mAccountNumber;
    [PersistantProperty]
    public string AccountNumber
    {
      get { return mAccountNumber; }
      private set { SetProperty<string>(ref mAccountNumber, value); }
    }

    #endregion

    #region string Name

    public const string NameProperty = "Name";
    [DataMember(Name = NameProperty, EmitDefaultValue = false)]
    string mName;
    [PersistantProperty]
    public string Name
    {
      get { return mName; }
      private set { SetProperty<string>(ref mName, value); }
    }

    #endregion

    #region string Alert

    public const string AlertProperty = "Alert";
    [DataMember(Name = AlertProperty, EmitDefaultValue = false)]
    string mAlert;
    [PersistantProperty]
    public string Alert
    {
      get { return mAlert; }
      set { SetProperty<string>(ref mAlert, value); }
    }

    #endregion
  }
}

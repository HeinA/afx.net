﻿using Afx.Business.Data;
using Afx.Business.Documents;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ScheduleManagement.Business
{
  [DataContract(IsReference = true, Namespace = Namespace.ScheduleManagement)]
  public partial class LoadAllocation
  {
    public const string DocumentTypeIdentifier = "{92d2eec9-822e-419e-9e7f-8cb08905fcf0}";
    public class States
    {
      public const string Cancelled = "{1d5d34a1-943d-447b-94c7-2c1abc00104d}";
      public const string Complete = "{79ca0f88-5e96-4a8e-bc7b-d5bd96fe2a36}";
      public const string Scheduled = "{7b72f718-2d0b-4247-8251-e483cad1c931}";
    }

    protected LoadAllocation(DocumentType documentType)
      : base(documentType)
    {
    }
  }
}

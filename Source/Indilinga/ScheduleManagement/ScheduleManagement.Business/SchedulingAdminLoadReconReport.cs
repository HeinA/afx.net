﻿using Afx.Business;
using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Validation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Dialogs.PrintPreviewDialog
{
  [PersistantObject(Schema = "ScheduleManagement")]
  public partial class SchedulingAdminLoadReconReport : BusinessObject 
  {
    #region Constructors

    public SchedulingAdminLoadReconReport()
    {
    }

    #endregion

    
    #region string Text

    public const string TextProperty = "Text";
    [DataMember(Name = TextProperty, EmitDefaultValue = false)]
    string mText;
    [PersistantProperty]
    [Mandatory("Text is mandatory.")]
    public string Text
  {
      get { return mText; }
      set { SetProperty<string>(ref mText, value); }
    }

    #endregion

  }
}

﻿using Afx.Business;
using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Documents;
using Afx.Business.Validation;
using FreightManagement.Business;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace ScheduleManagement.Business
{
  [PersistantObject(Schema = "ScheduleManagement", TableName = "vScheduledAction", IsReadOnly = true)]
  public partial class ScheduledAction : BusinessObject 
  {
    #region Constructors

    public ScheduledAction()
    {
    }

    #endregion

    #region DocumentType DocumentType

    public const string DocumentTypeProperty = "DocumentType";
    [PersistantProperty(IsCached = true)]
    public DocumentType DocumentType
    {
      get { return GetCachedObject<DocumentType>(); }
      set { SetCachedObject<DocumentType>(value); }
    }

    #endregion

    #region Guid DocumentIdentifier

    public const string DocumentIdentifierProperty = "DocumentIdentifier";
    [DataMember(Name = DocumentIdentifierProperty, EmitDefaultValue = false)]
    Guid mDocumentIdentifier;
    [PersistantProperty]
    public Guid DocumentIdentifier
    {
      get { return mDocumentIdentifier; }
      set { SetProperty<Guid>(ref mDocumentIdentifier, value); }
    }

    #endregion

    #region string DocumentNumber

    public const string DocumentNumberProperty = "DocumentNumber";
    [DataMember(Name = DocumentNumberProperty, EmitDefaultValue = false)]
    string mDocumentNumber;
    [PersistantProperty]
    public string DocumentNumber
    {
      get { return mDocumentNumber; }
      set { SetProperty<string>(ref mDocumentNumber, value); }
    }

    #endregion

    #region string Action

    public const string ActionProperty = "Action";
    [DataMember(Name = ActionProperty, EmitDefaultValue = false)]
    string mAction;
    [PersistantProperty]
    public string Action
    {
      get { return mAction; }
      set { SetProperty<string>(ref mAction, value); }
    }

    #endregion

    #region string Route

    public const string RouteProperty = "Route";
    [DataMember(Name = RouteProperty, EmitDefaultValue = false)]
    string mRoute;
    [PersistantProperty]
    public string Route
    {
      get { return mRoute; }
      set { SetProperty<string>(ref mRoute, value); }
    }

    #endregion

    #region DateTime ActionDate

    public const string ActionDateProperty = "ActionDate";
    [DataMember(Name = ActionDateProperty, EmitDefaultValue = false)]
    DateTime mActionDate;
    [PersistantProperty]
    public DateTime ActionDate
    {
      get { return mActionDate; }
      set { SetProperty<DateTime>(ref mActionDate, value); }
    }

    #endregion
  }
}

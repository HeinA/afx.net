﻿using Afx.Business;
using Afx.Business.Data;
using FreightManagement.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ScheduleManagement.Business
{
  [PersistantObject(Schema = "ScheduleManagement")]
  [DataContract(IsReference = true, Namespace = ScheduleManagement.Business.Namespace.ScheduleManagement)]
  public class TripsheetFee : BusinessObject<Tripsheet>
  {
    #region FeeType FeeType

    public const string FeeTypeProperty = "FeeType";
    [PersistantProperty(IsCached = true)]
    public FeeType FeeType
    {
      get { return GetCachedObject<FeeType>(); }
      set { SetCachedObject<FeeType>(value); }
    }

    #endregion

    #region decimal Fee

    public const string FeeProperty = "Fee";
    [DataMember(Name = FeeProperty, EmitDefaultValue = false)]
    decimal mFee;
    [PersistantProperty]
    public decimal Fee
    {
      get { return mFee; }
      set { SetProperty<decimal>(ref mFee, value); }
    }

    #endregion
  }
}

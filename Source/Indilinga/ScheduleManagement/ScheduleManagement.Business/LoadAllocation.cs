﻿using Afx.Business;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Documents;
using Afx.Business.Validation;
using ContactManagement.Business;
using FleetManagement.Business;
using FreightManagement.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ScheduleManagement.Business
{
  [PersistantObject(Schema = "ScheduleManagement")]
  public partial class LoadAllocation : Document, IVehicleCollection
  {
    public LoadAllocation()
      : base(Cache.Instance.GetObject<DocumentType>(DocumentTypeIdentifier))
    {
    }

    #region DateTime LoadingDate

    public const string LoadingDateProperty = "LoadingDate";
    [DataMember(Name = LoadingDateProperty, EmitDefaultValue = false)]
    DateTime mLoadingDate = DateTime.Now.Date;
    [PersistantProperty]
    [Mandatory("Loading Date is mandatory.")]
    public DateTime LoadingDate
    {
      get { return mLoadingDate; }
      set { SetProperty<DateTime>(ref mLoadingDate, value.Date); }
    }

    #endregion

    #region SlotTime LoadingSlot

    public const string LoadingSlotProperty = "LoadingSlot";
    [PersistantProperty(IsCached = true)]
    public SlotTime LoadingSlot
    {
      get { return GetCachedObject<SlotTime>(); }
      set { SetCachedObject<SlotTime>(value); }
    }

    #endregion

    #region DateTime OffloadingDate

    public const string OffloadingDateProperty = "OffloadingDate";
    [DataMember(Name = OffloadingDateProperty, EmitDefaultValue = false)]
    DateTime mOffloadingDate = DateTime.Now.Date.AddDays(3);
    [Mandatory("Offloading Date is mandatory.")]
    [PersistantProperty]
    public DateTime OffloadingDate
    {
      get { return mOffloadingDate; }
      set { SetProperty<DateTime>(ref mOffloadingDate, value.Date); }
    }

    #endregion

    #region SlotTime OffloadingSlot

    public const string OffloadingSlotProperty = "OffloadingSlot";
    [PersistantProperty(IsCached = true)]
    public SlotTime OffloadingSlot
    {
      get { return GetCachedObject<SlotTime>(); }
      set { SetCachedObject<SlotTime>(value); }
    }

    #endregion

    #region Route Route

    public const string RouteProperty = "Route";
    [PersistantProperty(IsCached = true)]
    [Mandatory("Route is mandatory.")]
    public Route Route
    {
      get { return GetCachedObject<Route>(); }
      set { SetCachedObject<Route>(value); }
    }

    #endregion

    #region Associative BusinessObjectCollection<Vehicle> Vehicles

    public const string VehiclesProperty = "Vehicles";
    AssociativeObjectCollection<LoadAllocationVehicle, Vehicle> mVehicles;
    [PersistantCollection(AssociativeType = typeof(LoadAllocationVehicle))]
    public BusinessObjectCollection<Vehicle> Vehicles
    {
      get
      {
        if (mVehicles == null) using (new EventStateSuppressor(this)) { mVehicles = new AssociativeObjectCollection<LoadAllocationVehicle, Vehicle>(this); }
        return mVehicles;
      }
    }

    #endregion

    #region Associative BusinessObjectCollection<WaybillReference> Waybills

    public const string WaybillsProperty = "Waybills";
    AssociativeObjectCollection<LoadAllocationWaybill, WaybillReference> mWaybills;
    [PersistantCollection(AssociativeType = typeof(LoadAllocationWaybill))]
    public BusinessObjectCollection<WaybillReference> Waybills
    {
      get
      {
        if (mWaybills == null) using (new EventStateSuppressor(this)) { mWaybills = new AssociativeObjectCollection<LoadAllocationWaybill, WaybillReference>(this); }
        return mWaybills;
      }
    }

    #endregion

    #region DistributionCenter DistributionCenter

    public const string DistributionCenterProperty = "DistributionCenter";
    [PersistantProperty]
    public DistributionCenter DistributionCenter
    {
      get { return Route.Owner; }
    }

    #endregion

    #region Guid AssociatedTripsheet

    public const string AssociatedTripsheetProperty = "AssociatedTripsheet";
    [DataMember(Name = AssociatedTripsheetProperty, EmitDefaultValue = false)]
    Guid mAssociatedTripsheet;
    [PersistantProperty(IgnoreConcurrency = true)]
    public Guid AssociatedTripsheet
    {
      get { return mAssociatedTripsheet; }
      set { SetProperty<Guid>(ref mAssociatedTripsheet, value); }
    }

    #endregion


    /// <summary>
    /// Lead driver for trip.
    /// </summary>
    #region Contact Driver

    public const string DriverProperty = "Driver";
    [PersistantProperty(IsCached = true)]
    //[Mandatory("Driver is mandatory")]
    public Contact Driver
    {
      get { return GetCachedObject<Contact>(); }
      set { SetCachedObject<Contact>(value); }
    }

    #region string DriverMobilePhone

    public const string DriverMobilePhoneProperty = "DriverMobilePhone";
    public string DriverMobilePhone
    {
      get
      {
        ComplexContact cc = Driver as ComplexContact;
        if (cc != null)
        {
          Telephone t = cc.TelephoneNumbers.Where(x => x.TelephoneType.SystemTelephoneType == SystemTelephoneType.Mobile).FirstOrDefault();
          return t == null ? string.Empty : t.TelephoneNumber;
        }
        return string.Empty;
      }
    }

    #endregion

    #region string DriverLandlinePhone

    public const string DriverLandlinePhoneProperty = "DriverLandlinePhone";
    public string DriverLandlinePhone
    {
      get
      {
        ComplexContact cc = Driver as ComplexContact;
        if (cc != null)
        {
          Telephone t = cc.TelephoneNumbers.Where(x => x.TelephoneType.SystemTelephoneType == SystemTelephoneType.Landline).FirstOrDefault();
          return t == null ? string.Empty : t.TelephoneNumber;
        }
        return string.Empty;
      }
    }

    #endregion

    #region string DriverForeignMobilePhone

    public const string DriverForeignMobilePhoneProperty = "DriverForeignMobilePhone";
    public string DriverForeignMobilePhone
    {
      get
      {
        ComplexContact cc = Driver as ComplexContact;
        if (cc != null)
        {
          return cc.TelephoneNumbers.Where(x => x.TelephoneType.SystemTelephoneType == SystemTelephoneType.Foreign).FirstOrDefault().TelephoneNumber;
        }
        return "-";
      }
    }

    #endregion

    #endregion

    /// <summary>
    /// Co-driver for trip.
    /// </summary>
    #region Contact CoDriver

    public const string CoDriverProperty = "CoDriver";
    [PersistantProperty(IsCached = true)]
    public Contact CoDriver
    {
      get { return GetCachedObject<Contact>(); }
      set { SetCachedObject<Contact>(value); }
    }

    #endregion

    /// <summary>
    /// Backup driver for trip.
    /// </summary>
    #region Contact BackupDriver

    public const string BackupDriverProperty = "BackupDriver";
    [PersistantProperty(IsCached = true)]
    public Contact BackupDriver
    {
      get { return GetCachedObject<Contact>(); }
      set { SetCachedObject<Contact>(value); }
    }

    #endregion


    protected override void GetErrors(System.Collections.ObjectModel.Collection<string> errors, string propertyName)
    {
      if (string.IsNullOrWhiteSpace(propertyName) || propertyName == WaybillsProperty)
      {
        if (!GetIsWaybillsValid()) errors.Add("Document contains invalid waybills for the specified route.");
      }

      base.GetErrors(errors, propertyName);
    }

    bool GetIsWaybillsValid()
    {
      foreach(var w in Waybills)
      {
        if (!GetIsWaybillValid(w))
        {
          return false;
        }
      }
      return true;
    }

    bool GetIsWaybillValid(WaybillReference w)
    {
      bool valid = false;
      foreach (var r in w.Routes)
      {
        if (r.Route.Equals(Route)) valid = true;
      }
      return valid;
    }
  }
}

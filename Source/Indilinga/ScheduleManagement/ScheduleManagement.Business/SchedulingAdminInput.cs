﻿using AccountManagement.Business;
using Afx.Business;
using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Documents;
using Afx.Business.Validation;
using ClosedXML.Excel;
using ContactManagement.Business;
using FleetManagement.Business;
using Geographics.Business;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace ScheduleManagement.Business
{
  [PersistantObject(Schema = "ScheduleManagement", TableName = "vwSchedulingAdminInput")]
  public partial class SchedulingAdminInput : BusinessObject, IExcelRow 
  {
    #region Constructors

    public SchedulingAdminInput()
    {
    }

    #endregion


    #region string WaybillNumber

    public const string WaybillNumberProperty = "WaybillNumber";
    [DataMember(Name = WaybillNumberProperty, EmitDefaultValue = false)]
    string mWaybillNumber;
    [PersistantProperty]
    public string WaybillNumber
    {
      get { return mWaybillNumber; }
      set { SetProperty<string>(ref mWaybillNumber, value); }
    }

    #endregion

    #region DateTime WaybillDate

    public const string WaybillDateProperty = "WaybillDate";
    [DataMember(Name = WaybillDateProperty, EmitDefaultValue = false)]
    DateTime mWaybillDate;
    [PersistantProperty]
    public DateTime WaybillDate
    {
      get { return mWaybillDate; }
      set { SetProperty<DateTime>(ref mWaybillDate, value); }
    }

    #endregion

    #region DocumentTypeState WaybillState

    public const string WaybillStateProperty = "WaybillState";
    [PersistantProperty]
    public DocumentTypeState WaybillState
    {
      get { return GetCachedObject<DocumentTypeState>(); }
      set { SetCachedObject<DocumentTypeState>(value); }
    }

    #endregion


    #region int DocId
    
    public const string DocIdProperty = "DocId";
    [DataMember(Name = DocIdProperty, EmitDefaultValue=false)]
    int mDocId;
    [PersistantProperty]
    public int DocId
    {
      get { return mDocId; }
      set { SetProperty<int>(ref mDocId, value); }
    }
    
    #endregion

    #region Guid DocGlobalIdentifier
    
    public const string DocGlobalIdentifierProperty = "DocGlobalIdentifier";
    [DataMember(Name = DocGlobalIdentifierProperty, EmitDefaultValue=false)]
    Guid mDocGlobalIdentifier;
    [PersistantProperty]
    public Guid DocGlobalIdentifier
    {
      get { return mDocGlobalIdentifier; }
      set { SetProperty<Guid>(ref mDocGlobalIdentifier, value); }
    }
    
    #endregion

    #region string TransportManifestNumber

    public const string TransportManifestNumberProperty = "TransportManifestNumber";
    [DataMember(Name = TransportManifestNumberProperty, EmitDefaultValue = false)]
    string mTransportManifestNumber;
    [PersistantProperty]
    public string TransportManifestNumber
    {
      get { return mTransportManifestNumber; }
      set { SetProperty<string>(ref mTransportManifestNumber, value); }
    }

    #endregion

    #region DateTime TransportManifestDate

    public const string TransportManifestDateProperty = "TransportManifestDate";
    [DataMember(Name = TransportManifestDateProperty, EmitDefaultValue = false)]
    DateTime mTransportManifestDate;
    [PersistantProperty]
    public DateTime TransportManifestDate
    {
      get { return mTransportManifestDate; }
      set { SetProperty<DateTime>(ref mTransportManifestDate, value); }
    }

    #endregion

    #region DateTime? ScheduledLoadingDate
    
    public const string ScheduledLoadingDateProperty = "ScheduledLoadingDate";
    [DataMember(Name = ScheduledLoadingDateProperty, EmitDefaultValue=false)]
    DateTime? mScheduledLoadingDate;
    [PersistantProperty]
    public DateTime? ScheduledLoadingDate
    {
      get { return mScheduledLoadingDate; }
      set { SetProperty<DateTime?>(ref mScheduledLoadingDate, value); }
    }
    

    #endregion

    #region AccountReference Account
    
    public const string AccountProperty = "Account";
    [PersistantProperty]
    public AccountReference Account
    {
      get { return GetCachedObject<AccountReference>(); }
      set { SetCachedObject<AccountReference>(value); }
    }
    
    #endregion

    #region string ClientOrderNumber
    
    public const string ClientOrderNumberProperty = "ClientOrderNumber";
    [DataMember(Name = ClientOrderNumberProperty, EmitDefaultValue=false)]
    string mClientOrderNumber;
    [PersistantProperty]
    public string ClientOrderNumber
    {
      get { return mClientOrderNumber; }
      set { SetProperty<string>(ref mClientOrderNumber, value); }
    }
    
    #endregion

    #region string Client
    
    public const string ClientProperty = "Client";
    [DataMember(Name = ClientProperty, EmitDefaultValue=false)]
    string mClient;
    [PersistantProperty]
    public string Client
    {
      get { return mClient; }
      set { SetProperty<string>(ref mClient, value); }
    }
    
    #endregion

    #region Location LoadingPoint
    
    public const string LoadingPointProperty = "LoadingPoint";
    [PersistantProperty]
    public Location LoadingPoint
    {
      get { return GetCachedObject<Location>(); }
      set { SetCachedObject<Location>(value); }
    }
    
    #endregion

    #region string DeliverTo
    
    public const string DeliverToProperty = "DeliverTo";
    [DataMember(Name = DeliverToProperty, EmitDefaultValue=false)]
    string mDeliverTo;
    [PersistantProperty]
    public string DeliverTo
    {
      get { return mDeliverTo; }
      set { SetProperty<string>(ref mDeliverTo, value); }
    }
    
    #endregion

    #region Location Destination
    
    public const string DestinationProperty = "Destination";
    [PersistantProperty]
    public Location Destination
    {
      get { return GetCachedObject<Location>(); }
      set { SetCachedObject<Location>(value); }
    }
    
    #endregion

    #region string Corrections
    
    public const string CorrectionsProperty = "Corrections";
    [DataMember(Name = CorrectionsProperty, EmitDefaultValue=false)]
    string mCorrections;
    [PersistantProperty]
    public string Corrections
    {
      get { return mCorrections; }
      set { SetProperty<string>(ref mCorrections, value); }
    }
    
    #endregion

    #region decimal Rate
    
    public const string RateProperty = "Rate";
    [DataMember(Name = RateProperty, EmitDefaultValue=false)]
    decimal mRate;
    [PersistantProperty]
    public decimal Rate
    {
      get { return mRate; }
      set { SetProperty<decimal>(ref mRate, value); }
    }
    
    #endregion

    #region string Order
    
    public const string OrderProperty = "Order";
    [DataMember(Name = OrderProperty, EmitDefaultValue=false)]
    string mOrder;
    [PersistantProperty]
    public string Order
    {
      get { return mOrder; }
      set { SetProperty<string>(ref mOrder, value); }
    }
    
    #endregion

    #region string Reference
    
    public const string ReferenceProperty = "Reference";
    [DataMember(Name = ReferenceProperty, EmitDefaultValue=false)]
    string mReference;
    [PersistantProperty]
    public string Reference
    {
      get { return mReference; }
      set { SetProperty<string>(ref mReference, value); }
    }
    
    #endregion

    #region string DeliveryNotes
    
    public const string DeliveryNotesProperty = "DeliveryNotes";
    [DataMember(Name = DeliveryNotesProperty, EmitDefaultValue=false)]
    string mDeliveryNotes;
    [PersistantProperty]
    public string DeliveryNotes
    {
      get { return mDeliveryNotes; }
      set { SetProperty<string>(ref mDeliveryNotes, value); }
    }
    
    #endregion

    #region string InvoiceNumber
    
    public const string InvoiceNumberProperty = "InvoiceNumber";
    [DataMember(Name = InvoiceNumberProperty, EmitDefaultValue=false)]
    string mInvoiceNumber;
    [PersistantProperty]
    public string InvoiceNumber
    {
      get { return mInvoiceNumber; }
      set { SetProperty<string>(ref mInvoiceNumber, value); }
    }
    
    #endregion

    #region string Trailers
    
    public const string TrailersProperty = "Trailers";
    [DataMember(Name = TrailersProperty, EmitDefaultValue=false)]
    string mTrailers;
    [PersistantProperty]
    public string Trailers
    {
      get { return mTrailers; }
      set { SetProperty<string>(ref mTrailers, value); }
    }
    
    #endregion

    #region Vehicle Horse

    public const string HorseProperty = "Horse";
    [PersistantProperty]
    public Vehicle Horse
    {
      get { return GetCachedObject<Vehicle>(); }
      set { SetCachedObject<Vehicle>(value); }
    }

    #endregion

    #region Employee Driver

    public const string DriverProperty = "Driver";
    [PersistantProperty]
    public Employee Driver
    {
      get { return GetCachedObject<Employee>(); }
      set { SetCachedObject<Employee>(value); }
    }

    #endregion

    #region Employee CoDriver
    
    public const string CoDriverProperty = "CoDriver";
    [PersistantProperty]
    public Employee CoDriver
    {
      get { return GetCachedObject<Employee>(); }
      set { SetCachedObject<Employee>(value); }
    }
    
    #endregion

    #region Employee BackupDriver
    
    public const string BackupDriverProperty = "BackupDriver";
    [PersistantProperty]
    public Employee BackupDriver
    {
      get { return GetCachedObject<Employee>(); }
      set { SetCachedObject<Employee>(value); }
    }
    
    #endregion



    public string TruckInfo
    {
      get 
      {
        return Horse.FleetNumber + " - " + Horse.RegistrationNumber + " - " + (string.IsNullOrWhiteSpace(Trailers) ? "No Trailer" : Trailers) + " - " + Driver.Fullname;
      }
    }

    #region IExcelRow

    public void ExportHeader(IXLWorksheet sheet, int row, string exportKey)
    {
      int column = 1;
      sheet.Cell(row, column++).SetValue("Date").Style.Font.SetBold();
      sheet.Cell(row, column++).SetValue("Manifest").Style.Font.SetBold();
      sheet.Cell(row, column++).SetValue("Waybill").Style.Font.SetBold();
      sheet.Cell(row, column++).SetValue("Account").Style.Font.SetBold();
      sheet.Cell(row, column++).SetValue("Client Order No").Style.Font.SetBold();
      sheet.Cell(row, column++).SetValue("Client").Style.Font.SetBold();
      sheet.Cell(row, column++).SetValue("Loading Point").Style.Font.SetBold();
      sheet.Cell(row, column++).SetValue("Delivery To").Style.Font.SetBold();
      sheet.Cell(row, column++).SetValue("Destination").Style.Font.SetBold();
      sheet.Cell(row, column++).SetValue("Rate").Style.Font.SetBold();
      sheet.Cell(row, column++).SetValue("Internal Order").Style.Font.SetBold();
      sheet.Cell(row, column++).SetValue("Invoice No").Style.Font.SetBold();
      sheet.Cell(row, column++).SetValue("Delivery Notes").Style.Font.SetBold();
      sheet.Cell(row, column++).SetValue("Truck Info").Style.Font.SetBold();
    }

    public void ExportRow(IXLWorksheet sheet, int row, string exportKey)
    {
      int column = 1;
      sheet.Cell(row, column++).SetValue(WaybillDate).Style.DateFormat.Format = "d MMM yyyy";
      sheet.Cell(row, column++).SetValue(TransportManifestNumber);
      sheet.Cell(row, column++).SetValue(WaybillNumber);
      sheet.Cell(row, column++).SetValue(Account.Name);
      sheet.Cell(row, column++).SetValue(ClientOrderNumber);
      sheet.Cell(row, column++).SetValue(Client);
      sheet.Cell(row, column++).SetValue(LoadingPoint.Name);
      sheet.Cell(row, column++).SetValue(DeliverTo);
      sheet.Cell(row, column++).SetValue(Destination.City.Name);
      sheet.Cell(row, column++).SetValue(Rate).Style.NumberFormat.Format = "#,##0.00";
      sheet.Cell(row, column++).SetValue(Order);
      sheet.Cell(row, column++).SetValue(InvoiceNumber);
      sheet.Cell(row, column++).SetValue(DeliveryNotes);
      sheet.Cell(row, column++).SetValue(TruckInfo);
    }

    #endregion
  }
}

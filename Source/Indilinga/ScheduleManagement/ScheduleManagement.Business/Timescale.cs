﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScheduleManagement.Business
{
  public enum Timescale
  {
    Day = 0
    , Month = 1
    , Week = 2
    , TwoWeeks = 3
  }
}

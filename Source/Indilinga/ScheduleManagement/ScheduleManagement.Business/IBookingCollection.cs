﻿using Afx.Business.Collections;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScheduleManagement.Business
{
  public interface IBookingCollection
  {
    BusinessObjectCollection<VehicleSchedule> Bookings
    {
      get;
    }
  }
}

﻿using Afx.Business;
using Afx.Business.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScheduleManagement.Business
{
  [PersistantObject(Schema = "ScheduleManagement")]
  public class QuickTransportManifestLinkage : BusinessObject
  {
    #region Guid Waybill

    public const string WaybillProperty = "Waybill";
    Guid mWaybill;
    [PersistantProperty]
    public Guid Waybill
    {
      get { return mWaybill; }
      set { SetProperty<Guid>(ref mWaybill, value); }
    }

    #endregion

    #region Guid? Tripsheet

    public const string TripsheetProperty = "Tripsheet";
    Guid? mTripsheet;
    [PersistantProperty]
    public Guid? Tripsheet
    {
      get { return mTripsheet; }
      set { SetProperty<Guid?>(ref mTripsheet, value); }
    }

    #endregion
  }
}

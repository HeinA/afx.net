﻿using AccountManagement.Business;
using Afx.Business;
using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Documents;
using Afx.Business.Validation;
using ContactManagement.Business;
using FleetManagement.Business;
using FreightManagement.Business;
using Geographics.Business;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace ScheduleManagement.Business
{
  [PersistantObject(Schema = "ScheduleManagement", TableName = "vVehicleSchedule")]
  public partial class VehicleSchedule : BusinessObject 
  {
    #region Constructors

    public VehicleSchedule()
    {
    }

    #endregion


    #region bool IsChecked

    public const string IsCheckedProperty = "IsChecked";
    bool mIsChecked;
    public bool IsChecked
    {
      get { return mIsChecked; }
      set { SetProperty<bool>(ref mIsChecked, value); }
    }

    #endregion



    #region ScheduleType ScheduleType

    public const string ScheduleTypeProperty = "ScheduleType";
    [DataMember(Name = ScheduleTypeProperty, EmitDefaultValue = false)]
    ScheduleType mScheduleType = ScheduleType.Delivery;
    [PersistantProperty]
    public ScheduleType ScheduleType
    {
      get { return mScheduleType; }
      set { SetProperty<ScheduleType>(ref mScheduleType, value); }
    }

    #endregion


    #region "Drivers (Contractor, Main Driver, Co-Driver & Backup Driver)"

    #region Contact Driver

    public const string DriverProperty = "Driver";
    [PersistantProperty(IsCached = true)]
    public Contact Driver
    {
      get { return GetCachedObject<Contact>(); }
      set { SetCachedObject<Contact>(value); }
    }

    #endregion

    #region Contact BackupDriver

    public const string BackupDriverProperty = "BackupDriver";
    [PersistantProperty(IsCached = true)]
    public Contact BackupDriver
    {
      get { return GetCachedObject<Contact>(); }
      set { SetCachedObject<Contact>(value); }
    }

    #endregion

    #region Contact CoDriver

    public const string CoDriverProperty = "CoDriver";
    [PersistantProperty(IsCached = true)]
    public Contact CoDriver
    {
      get { return GetCachedObject<Contact>(); }
      set { SetCachedObject<Contact>(value); }
    }

    #endregion

    #region Contractor Contractor

    public const string ContractorProperty = "Contractor";
    [DataMember(Name = ContractorProperty, EmitDefaultValue = false)]
    Contractor mContractor;
    [PersistantProperty]
    public Contractor Contractor
    {
      get { return mContractor; }
      set { SetProperty<Contractor>(ref mContractor, value); }
    }

    #endregion

    #region Contractor Courier

    public const string CourierProperty = "Courier";
    public Contractor Courier
    {
      get { return Contractor; }
    }

    #endregion

    #endregion


    #region "Document Information (Document #, Document Identifier, Document State, Document State GUID)"

    #region string DocumentNumber

    public const string DocumentNumberProperty = "DocumentNumber";
    [DataMember(Name = DocumentNumberProperty, EmitDefaultValue = false)]
    string mDocumentNumber;
    [PersistantProperty]
    public string DocumentNumber
    {
      get { return mDocumentNumber; }
      set { SetProperty<string>(ref mDocumentNumber, value); }
    }

    #endregion

    #region Guid DocumentIdentifier

    public const string DocumentIdentifierProperty = "DocumentIdentifier";
    [DataMember(Name = DocumentIdentifierProperty, EmitDefaultValue = false)]
    Guid mDocumentIdentifier;
    [PersistantProperty]
    public Guid DocumentIdentifier
    {
      get { return mDocumentIdentifier; }
      set { SetProperty<Guid>(ref mDocumentIdentifier, value); }
    }

    #endregion

    #region DateTime? DocumentDate

    public const string DocumentDateProperty = "DocumentDate";
    [DataMember(Name = DocumentDateProperty, EmitDefaultValue = false)]
    DateTime? mDocumentDate;
    [PersistantProperty]
    public DateTime? DocumentDate
    {
      get { return mDocumentDate; }
      set { SetProperty<DateTime?>(ref mDocumentDate, value); }
    }

    #endregion


    #region DocumentType DocumentType

    public const string DocumentTypeProperty = "DocumentType";
    [PersistantProperty]
    public DocumentType DocumentType
    {
      get { return GetCachedObject<DocumentType>(); }
      set { SetCachedObject<DocumentType>(value); }
    }

    #endregion


    #region string DocumentState

    public const string DocumentStateProperty = "DocumentState";
    [DataMember(Name = DocumentStateProperty, EmitDefaultValue = false)]
    string mDocumentState;
    [PersistantProperty]
    public string DocumentState
    {
      get { return mDocumentState; }
      set { SetProperty<string>(ref mDocumentState, value); }
    }

    #endregion

    #region Guid DocumentStateGuid

    public const string DocumentStateGuidProperty = "DocumentStateGuid";
    [DataMember(Name = DocumentStateGuidProperty, EmitDefaultValue = false)]
    Guid mDocumentStateGuid;
    [PersistantProperty]
    public Guid DocumentStateGuid
    {
      get { return mDocumentStateGuid; }
      set { SetProperty<Guid>(ref mDocumentStateGuid, value); }
    }

    #endregion

    #endregion


    #region "Vehicle Information"

    #region Vehicle Vehicle

    public const string VehicleProperty = "Vehicle";
    [PersistantProperty(IsCached = true)]
    public Vehicle Vehicle
    {
      get { return GetCachedObject<Vehicle>(); }
      set { SetCachedObject<Vehicle>(value); }
    }

    #endregion

    #region bool IsOwned

    public const string IsOwnedProperty = "IsOwned";
    [DataMember(Name = IsOwnedProperty, EmitDefaultValue = false)]
    bool mIsOwned;
    [PersistantProperty]
    public bool IsOwned
    {
      get { return mIsOwned; }
      set { SetProperty<bool>(ref mIsOwned, value); }
    }

    #endregion

    #region Vehicle Trailer

    public const string TrailerProperty = "Trailer";
    [PersistantProperty(IsCached = true)]
    public Vehicle Trailer
    {
      get { return GetCachedObject<Vehicle>(); }
      set { SetCachedObject<Vehicle>(value); }
    }

    #endregion

    #endregion


    #region "Route Information (Route, Origin, Destination)"

    #region Location Origin

    public const string OriginProperty = "Origin";
    [PersistantProperty(IsCached = true)]
    public Location Origin
    {
      get { return GetCachedObject<Location>(); }
      set { SetCachedObject<Location>(value); }
    }

    #endregion

    #region Location Destination

    public const string DestinationProperty = "Destination";
    [PersistantProperty(IsCached = true)]
    public Location Destination
    {
      get { return GetCachedObject<Location>(); }
      set { SetCachedObject<Location>(value); }
    }

    #endregion
    
    #region Route Route

    public const string RouteProperty = "Route";
    Route mRoute;
    public Route Route
    {
      get { return mRoute; }
      set { SetProperty<Route>(ref mRoute, value); }
    }

    #endregion

    #endregion


    #region "Date & Time Information (ScheduledDateOfDeparture, ScheduledDateOfArrival, ActualDateOfDeparture, ActualDateOfArrival)"

    #region DateTime? ScheduledLoadingDate
    
    public const string ScheduledLoadingDateProperty = "ScheduledLoadingDate";
    [DataMember(Name = ScheduledLoadingDateProperty, EmitDefaultValue=false)]
    DateTime? mScheduledLoadingDate;
    [PersistantProperty]
    public DateTime? ScheduledLoadingDate
    {
      get { return mScheduledLoadingDate; }
      set { SetProperty<DateTime?>(ref mScheduledLoadingDate, value); }
    }
    
    #endregion

    #region DateTime? ScheduledOffloadingDate
    
    public const string ScheduledOffloadingDateProperty = "ScheduledOffloadingDate";
    [DataMember(Name = ScheduledOffloadingDateProperty, EmitDefaultValue=false)]
    DateTime? mScheduledOffloadingDate;
    [PersistantProperty]
    public DateTime? ScheduledOffloadingDate
    {
      get { return mScheduledOffloadingDate; }
      set { SetProperty<DateTime?>(ref mScheduledOffloadingDate, value); }
    }
    
    #endregion

    #region DateTime? ActualLoadingTime

    public const string ActualLoadingTimeProperty = "ActualLoadingTime";
    [DataMember(Name = ActualLoadingTimeProperty, EmitDefaultValue = false)]
    DateTime? mActualLoadingTime;
    [PersistantProperty]
    public DateTime? ActualLoadingTime
    {
      get { return mActualLoadingTime; }
      set { SetProperty<DateTime?>(ref mActualLoadingTime, value); }
    }

    #endregion

    #region DateTime? ActualOffloadingTime

    public const string ActualOffloadingTimeProperty = "ActualOffloadingTime";
    [DataMember(Name = ActualOffloadingTimeProperty, EmitDefaultValue = false)]
    DateTime? mActualOffloadingTime;
    [PersistantProperty]
    public DateTime? ActualOffloadingTime
    {
      get { return mActualOffloadingTime; }
      set { SetProperty<DateTime?>(ref mActualOffloadingTime, value); }
    }

    #endregion

    #region DateTime? RenderLoadingDateTime

    public const string RenderLoadingDateTimeProperty = "RenderLoadingDateTime";
    [DataMember(Name = RenderLoadingDateTimeProperty, EmitDefaultValue = false)]
    DateTime? mRenderLoadingDateTime;
    [PersistantProperty]
    public DateTime? RenderLoadingDateTime
    {
      get { return mRenderLoadingDateTime; }
      set { SetProperty<DateTime?>(ref mRenderLoadingDateTime, value); }
    }

    #endregion

    #region DateTime? RenderOffloadingDateTime

    public const string RenderOffloadingDateTimeProperty = "RenderOffloadingDateTime";
    [DataMember(Name = RenderOffloadingDateTimeProperty, EmitDefaultValue = false)]
    DateTime? mRenderOffloadingDateTime;
    [PersistantProperty]
    public DateTime? RenderOffloadingDateTime
    {
      get { return mRenderOffloadingDateTime; }
      set { SetProperty<DateTime?>(ref mRenderOffloadingDateTime, value); }
    }

    #endregion


    #endregion


    #region "Fees"

    #region decimal TarpaullinFees

    public const string TarpaullinFeesProperty = "TarpaullinFees";
    [DataMember(Name = TarpaullinFeesProperty, EmitDefaultValue = false)]
    decimal mTarpaullinFees;
    [PersistantProperty]
    public decimal TarpaullinFees
    {
      get { return mTarpaullinFees; }
      set { SetProperty<decimal>(ref mTarpaullinFees, value); }
    }

    #endregion

    #region decimal TollFees

    public const string TollFeesProperty = "TollFees";
    [DataMember(Name = TollFeesProperty, EmitDefaultValue = false)]
    decimal mTollFees;
    [PersistantProperty]
    public decimal TollFees
    {
      get { return mTollFees; }
      set { SetProperty<decimal>(ref mTollFees, value); }
    }

    #endregion

    #region decimal NightOutFees

    public const string NightOutFeesProperty = "NightOutFees";
    [DataMember(Name = NightOutFeesProperty, EmitDefaultValue = false)]
    decimal mNightOutFees;
    [PersistantProperty]
    public decimal NightOutFees
    {
      get { return mNightOutFees; }
      set { SetProperty<decimal>(ref mNightOutFees, value); }
    }

    #endregion

    #region decimal FuelLiters

    public const string FuelLitersProperty = "FuelLiters";
    [DataMember(Name = FuelLitersProperty, EmitDefaultValue = false)]
    decimal mFuelLiters;
    [PersistantProperty]
    public decimal FuelLiters
    {
      get { return mFuelLiters; }
      set { SetProperty<decimal>(ref mFuelLiters, value); }
    }

    #endregion

    #region decimal Rates

    public const string RatesProperty = "Rates";
    [DataMember(Name = RatesProperty, EmitDefaultValue = false)]
    decimal mRates;
    [PersistantProperty]
    public decimal Rates
    {
      get { return mRates; }
      set { SetProperty<decimal>(ref mRates, value); }
    }

    #endregion

    #endregion

    #region string Clients

    public const string ClientsProperty = "Clients";
    [DataMember(Name = ClientsProperty, EmitDefaultValue = false)]
    string mClients = "NO CLIENT SPECIFIED";
    [PersistantProperty]
    public string Clients
    {
      get { return mClients; }
      set {
        if (SetProperty<string>(ref mClients, value))
        {
          if (mAccounts == null) mAccounts = new BusinessObjectCollection<AccountReference>();
          mAccounts.Clear();
        }
      }
    }

    #endregion

    #region IEnumerable<AccountReference> Accounts

    public const string AccountsProperty = "Accounts";
    BusinessObjectCollection<AccountReference> mAccounts;
    public IEnumerable<AccountReference> Accounts
    {
      get 
      { 
        if (mAccounts == null) mAccounts = new BusinessObjectCollection<AccountReference>();
        //return Cache.Instance.GetObjects<AccountReference>(x => x.GlobalIdentifier); 
        if (mClients != "NO CLIENT SPECIFIED")
        {
          foreach (var guid in Clients.Split(",".ToCharArray()[0]))
          {
            mAccounts.Add(Cache.Instance.GetObject<AccountReference>(guid));
          }
        }
        return mAccounts;
      }
    }

    #endregion

    #region string Orders

    public const string OrdersProperty = "Orders";
    [DataMember(Name = OrdersProperty, EmitDefaultValue = false)]
    string mOrders;
    [PersistantProperty]
    public string Orders
    {
      get { return mOrders; }
      set { SetProperty<string>(ref mOrders, value); }
    }

    #endregion

    #region int WaybillCount

    public const string WaybillCountProperty = "WaybillCount";
    [DataMember(Name = WaybillCountProperty, EmitDefaultValue = false)]
    int mWaybillCount;
    [PersistantProperty]
    public int WaybillCount
    {
      get { return mWaybillCount; }
      set { SetProperty<int>(ref mWaybillCount, value); }
    }

    #endregion

    #region decimal WaybillCharge
    
    public const string WaybillChargeProperty = "WaybillCharge";
    [DataMember(Name = WaybillChargeProperty, EmitDefaultValue=false)]
    decimal mWaybillCharge;
    [PersistantProperty]
    public decimal WaybillCharge
    {
      get { return mWaybillCharge; }
      set { SetProperty<decimal>(ref mWaybillCharge, value); }
    }
    
    #endregion

    #region decimal WaybillWeight

    public const string WaybillWeightProperty = "WaybillWeight";
    [DataMember(Name = WaybillWeightProperty, EmitDefaultValue = false)]
    decimal mWaybillWeight;
    [PersistantProperty]
    public decimal WaybillWeight
    {
      get { return mWaybillWeight; }
      set { SetProperty<decimal>(ref mWaybillWeight, value); }
    }

    #endregion



    #region bool AccountingOrderNumberProvided

    public const string AccountingOrderNumberProvidedProperty = "AccountingOrderNumberProvided";
    [DataMember(Name = AccountingOrderNumberProvidedProperty, EmitDefaultValue = false)]
    bool mAccountingOrderNumberProvided;
    [PersistantProperty]
    public bool AccountingOrderNumberProvided
    {
      get { return mAccountingOrderNumberProvided; }
      set { SetProperty<bool>(ref mAccountingOrderNumberProvided, value); }
    }

    #endregion
    #region string AccountingOrderNumber
    
    public const string AccountingOrderNumberProperty = "AccountingOrderNumber";
    [DataMember(Name = AccountingOrderNumberProperty, EmitDefaultValue=false)]
    string mAccountingOrderNumber;
    [PersistantProperty]
    public string AccountingOrderNumber
    {
      get { return mAccountingOrderNumber; }
      set { SetProperty<string>(ref mAccountingOrderNumber, value); }
    }
    
    #endregion
      
    #region bool ClientReferenceNumberProvided
    
    public const string ClientReferenceNumberProvidedProperty = "ClientReferenceNumberProvided";
    [DataMember(Name = ClientReferenceNumberProvidedProperty, EmitDefaultValue=false)]
    bool mClientReferenceNumberProvided;
    [PersistantProperty]
    public bool ClientReferenceNumberProvided
    {
      get { return mClientReferenceNumberProvided; }
      set { SetProperty<bool>(ref mClientReferenceNumberProvided, value); }
    }
    
    #endregion
    #region string ClientReferenceNumber
    
    public const string ClientReferenceNumberProperty = "ClientReferenceNumber";
    [DataMember(Name = ClientReferenceNumberProperty, EmitDefaultValue=false)]
    string mClientReferenceNumber;
    [PersistantProperty]
    public string ClientReferenceNumber
    {
      get { return mClientReferenceNumber; }
      set { SetProperty<string>(ref mClientReferenceNumber, value); }
    }
    
    #endregion
      
    #region bool InvoiceNumberProvided
    
    public const string InvoiceNumberProvidedProperty = "InvoiceNumberProvided";
    [DataMember(Name = InvoiceNumberProvidedProperty, EmitDefaultValue=false)]
    bool mInvoiceNumberProvided;
    [PersistantProperty]
    public bool InvoiceNumberProvided
    {
      get { return mInvoiceNumberProvided; }
      set { SetProperty<bool>(ref mInvoiceNumberProvided, value); }
    }
    
    #endregion
    #region string InvoiceNumber
    
    public const string InvoiceNumberProperty = "InvoiceNumber";
    [DataMember(Name = InvoiceNumberProperty, EmitDefaultValue=false)]
    string mInvoiceNumber;
    [PersistantProperty]
    public string InvoiceNumber
    {
      get { return mInvoiceNumber; }
      set { SetProperty<string>(ref mInvoiceNumber, value); }
    }
    
    #endregion

    #region string Notes

    public const string NotesProperty = "Notes";
    [DataMember(Name = NotesProperty, EmitDefaultValue = false)]
    string mNotes;
    [PersistantProperty]
    public string Notes
    {
      get { return mNotes; }
      set { SetProperty<string>(ref mNotes, value); }
    }

    #endregion

    #region string Reference

    public const string ReferenceProperty = "Reference";
    [DataMember(Name = ReferenceProperty, EmitDefaultValue = false)]
    string mReference;
    [PersistantProperty]
    public string Reference
    {
      get { return mReference; }
      set { SetProperty<string>(ref mReference, value); }
    }

    #endregion
  }
}

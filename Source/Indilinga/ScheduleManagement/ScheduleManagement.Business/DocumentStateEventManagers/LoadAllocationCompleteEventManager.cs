﻿using Afx.Business;
using Afx.Business.Data;
using Afx.Business.Documents;
using Afx.Business.Security;
using FreightManagement.Business;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScheduleManagement.Business.DocumentStateEventManagers
{
  [Export(typeof(IDocumentStateEventManager))]
  public class LoadAllocationCompleteEventManager : DocumentStateEventManager<LoadAllocation>
  {
    public override string TargetStateIdentifier
    {
      get { return LoadAllocation.States.Complete; }
    }

    public override void Process(LoadAllocation doc, DocumentTypeState originalState, DocumentStateEventScope scope)
    {
      if (originalState.Identifier != LoadAllocation.States.Scheduled) return;

      Validate(doc);

      LoadAllocationStateEventScope s = scope as LoadAllocationStateEventScope;
      if (s == null) CreateTripsheet(doc);
      else AttachToTripsheet(s.Tripsheet, doc);
    }

    private void Validate(LoadAllocation doc)
    {
      DistributionCenter originDC = doc.Route.Owner;
      //foreach (var w in doc.Waybills)
      //{
      //  if (!originDC.Equals(w.DistributionCenter)) throw new MessageException("Not all waybills are currently on the origin DC's floor.");
      //}
      if (doc.Vehicles.Count == 0) throw new MessageException("Load Allocation must have at least one Vehicle assigned.");
    }

    void CreateTripsheet(LoadAllocation doc)
    {
      ObjectRepository<Tripsheet> or = PersistanceManager.GetRepository<Tripsheet>();
      Tripsheet ts = new Tripsheet();
      ts.Driver = doc.Driver;
      ts.CoDriver = doc.CoDriver;
      ts.BackupDriver = doc.BackupDriver;

      AttachVehicles(ts, doc);
      //AttachNewDeliveryManifest(ts, doc);
      or.Persist(ts);
      doc.AssociatedTripsheet = ts.GlobalIdentifier;
    }

    void AttachToTripsheet(Guid guid, LoadAllocation doc)
    {
      ObjectRepository<Tripsheet> or = PersistanceManager.GetRepository<Tripsheet>();
      Tripsheet ts = or.GetInstance(guid);

      AttachVehicles(ts, doc);
      //AttachNewDeliveryManifest(ts, doc);
      or.Persist(ts);
      doc.AssociatedTripsheet = ts.GlobalIdentifier;
    }

    void AttachVehicles(Tripsheet ts, LoadAllocation doc)
    {
      foreach (var v in doc.Vehicles)
      {
        if (!ts.Vehicles.Contains(v)) ts.Vehicles.Add(v);
      }
    }

    /// <summary>
    /// Attach new Delivery Manifest to tripsheet.
    /// </summary>
    /// <param name="ts">Tripsheet</param>
    /// <param name="doc">Load Allocation</param>
    void AttachNewDeliveryManifest(Tripsheet ts, LoadAllocation doc)
    {
      DeliveryManifest m = new DeliveryManifest();
      foreach (var w in doc.Waybills)
      {
        m.Waybills.Add(w);
      }
      ObjectRepository<DeliveryManifest> orM = PersistanceManager.GetRepository<DeliveryManifest>();
      if (doc.Waybills.Count != 0)
      {
        m.State = new DocumentState(SecurityContext.User, Cache.Instance.GetObject<DocumentTypeState>(DeliveryManifest.States.Verified));
      }
      else
      {
        m.State = new DocumentState(SecurityContext.User, Cache.Instance.GetObject<DocumentTypeState>(DeliveryManifest.States.Open));
      }
      orM.Persist(m);


      ObjectRepository<ManifestReference> orMR = PersistanceManager.GetRepository<ManifestReference>();
      ManifestReference mr = orMR.GetInstance(m.GlobalIdentifier);
      ts.Manifests.Add(mr);
    }

    /// <summary>
    /// Attach new Transfer Manifest to tripsheet.
    /// </summary>
    /// <param name="ts">Tripsheet</param>
    /// <param name="doc">Load Allocation</param>
    void AttachNewTransferManifest(Tripsheet ts, LoadAllocation doc)
    {
      TransferManifest m = new TransferManifest();
      foreach (var w in doc.Waybills)
      {
        m.Waybills.Add(w);
      }
      ObjectRepository<TransferManifest> orM = PersistanceManager.GetRepository<TransferManifest>();
      if (doc.Waybills.Count != 0)
      {
        m.State = new DocumentState(SecurityContext.User, Cache.Instance.GetObject<DocumentTypeState>(TransferManifest.States.Verified));
      }
      else
      {
        m.State = new DocumentState(SecurityContext.User, Cache.Instance.GetObject<DocumentTypeState>(TransferManifest.States.Open));
      }
      orM.Persist(m);


      ObjectRepository<ManifestReference> orMR = PersistanceManager.GetRepository<ManifestReference>();
      ManifestReference mr = orMR.GetInstance(m.GlobalIdentifier);
      ts.Manifests.Add(mr);
    }

    /// <summary>
    /// Attach new Collection Manifest to tripsheet.
    /// </summary>
    /// <param name="ts">Tripsheet</param>
    /// <param name="doc">Load Allocation</param>
    void AttachNewCollectionManifest(Tripsheet ts, LoadAllocation doc)
    {
      CollectionManifest m = new CollectionManifest();
      foreach (var w in doc.Waybills)
      {
        m.Waybills.Add(w);
      }
      ObjectRepository<CollectionManifest> orM = PersistanceManager.GetRepository<CollectionManifest>();
      if (doc.Waybills.Count != 0)
      {
        m.State = new DocumentState(SecurityContext.User, Cache.Instance.GetObject<DocumentTypeState>(CollectionManifest.States.Verified));
      }
      else
      {
        m.State = new DocumentState(SecurityContext.User, Cache.Instance.GetObject<DocumentTypeState>(CollectionManifest.States.Open));
      }
      orM.Persist(m);


      ObjectRepository<ManifestReference> orMR = PersistanceManager.GetRepository<ManifestReference>();
      ManifestReference mr = orMR.GetInstance(m.GlobalIdentifier);
      ts.Manifests.Add(mr);
    }

    //TripsheetRoute GetTripsheetRoute(Tripsheet ts, LoadAllocation doc)
    //{
    //  Route r = ts.Routes.LastOrDefault();
    //  TripsheetRoute tsr = null;
    //  if (r == null)
    //  {
    //    ts.Routes.Add(doc.Route);
    //    tsr = ts.GetAssociativeObject<TripsheetRoute>(doc.Route);
    //    tsr.ScheduledLoadingDate = doc.LoadingDate;
    //    tsr.ScheduledLoadingSlot = doc.LoadingSlot;
    //    tsr.ScheduledOffloadingDate = doc.OffloadingDate;
    //    tsr.ScheduledOffloadingSlot = doc.OffloadingSlot;
    //  }
    //  else
    //  {
    //    if (r.Equals(doc.Route))
    //    {
    //      tsr = ts.GetAssociativeObject<TripsheetRoute>(doc.Route);
    //    }
    //    else
    //    {
    //      if (r.Destination.Equals(doc.Route.Origin))
    //      {
    //        ts.Routes.Add(doc.Route);
    //        tsr = ts.GetAssociativeObject<TripsheetRoute>(doc.Route);
    //        tsr.ScheduledLoadingDate = doc.LoadingDate;
    //        tsr.ScheduledLoadingSlot = doc.LoadingSlot;
    //        tsr.ScheduledOffloadingDate = doc.OffloadingDate;
    //        tsr.ScheduledOffloadingSlot = doc.OffloadingSlot;
    //      }
    //      else
    //      {
    //        throw new MessageException("Load Allocation Route is not a valid next route for the selected Tripsheet");
    //      }
    //    }
    //  }
    //  return tsr;
    //}
  }
}

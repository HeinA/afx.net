﻿using Afx.Business.Documents;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScheduleManagement.Business.DocumentStateEventManagers
{
  public class LoadAllocationStateEventScope : DocumentStateEventScope
  {
    #region Guid Tripsheet

    public const string TripsheetProperty = "Tripsheet";
    Guid mTripsheet;
    public Guid Tripsheet
    {
      get { return mTripsheet; }
      set { mTripsheet = value; }
    }

    #endregion
  }
}

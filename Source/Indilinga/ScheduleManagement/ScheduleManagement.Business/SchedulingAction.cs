﻿using Afx.Business;
using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Validation;
using ContactManagement.Business;
using FleetManagement.Business;
using FreightManagement.Business;
using Geographics.Business;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace ScheduleManagement.Business
{
  [PersistantObject(Schema = "ScheduleManagement", TableName = "vScheduling_ActionView")]
  public partial class SchedulingAction : BusinessObject 
  {
    #region Constructors

    public SchedulingAction()
    {
    }

    #endregion

    #region string FleetNumber

    public const string FleetNumberProperty = "FleetNumber";
    [DataMember(Name = FleetNumberProperty, EmitDefaultValue = false)]
    string mFleetNumber;
    [PersistantProperty]
    public string FleetNumber
    {
      get { return mFleetNumber; }
      set { SetProperty<string>(ref mFleetNumber, value); }
    }

    #endregion

    #region string RegistrationNumber

    public const string RegistrationNumberProperty = "RegistrationNumber";
    [DataMember(Name = RegistrationNumberProperty, EmitDefaultValue = false)]
    string mRegistrationNumber;
    [PersistantProperty]
    public string RegistrationNumber
    {
      get { return mRegistrationNumber; }
      set { SetProperty<string>(ref mRegistrationNumber, value); }
    }

    #endregion

    #region string VehicleNotes

    public const string VehicleNotesProperty = "VehicleNotes";
    [DataMember(Name = VehicleNotesProperty, EmitDefaultValue = false)]
    string mVehicleNotes;
    [PersistantProperty]
    public string VehicleNotes
    {
      get { return mVehicleNotes; }
      set { SetProperty<string>(ref mVehicleNotes, value); }
    }

    #endregion

    #region Vehicle Horse

    public const string HorseProperty = "Horse";
    [PersistantProperty]
    public Vehicle Horse
    {
      get { return GetCachedObject<Vehicle>(); }
      set { SetCachedObject<Vehicle>(value); }
    }

    #endregion

    #region Vehicle Trailer1

    public const string Trailer1Property = "Trailer1";
    [PersistantProperty]
    public Vehicle Trailer1
    {
      get { return GetCachedObject<Vehicle>(); }
      set { SetCachedObject<Vehicle>(value); }
    }

    #endregion

    #region Vehicle Trailer2

    public const string Trailer2Property = "Trailer2";
    [PersistantProperty]
    public Vehicle Trailer2
    {
      get { return GetCachedObject<Vehicle>(); }
      set { SetCachedObject<Vehicle>(value); }
    }

    #endregion

    #region Employee Driver

    public const string DriverProperty = "Driver";
    [PersistantProperty]
    public Employee Driver
    {
      get { return GetCachedObject<Employee>(); }
      set { SetCachedObject<Employee>(value); }
    }

    #endregion


    #region string Action

    public const string ActionProperty = "Action";
    [DataMember(Name = ActionProperty, EmitDefaultValue = false)]
    string mAction;
    [PersistantProperty]
    public string Action
    {
      get { return mAction; }
      set { SetProperty<string>(ref mAction, value); }
    }

    #endregion

    #region string StatusToday

    public const string StatusTodayProperty = "StatusToday";
    [DataMember(Name = StatusTodayProperty, EmitDefaultValue = false)]
    string mStatusToday;
    [PersistantProperty]
    public string StatusToday
    {
      get { return mStatusToday; }
      set { SetProperty<string>(ref mStatusToday, value); }
    }

    #endregion

    #region string StatusTomorrow

    public const string StatusTomorrowProperty = "StatusTomorrow";
    [DataMember(Name = StatusTomorrowProperty, EmitDefaultValue = false)]
    string mStatusTomorrow;
    [PersistantProperty]
    public string StatusTomorrow
    {
      get { return mStatusTomorrow; }
      set { SetProperty<string>(ref mStatusTomorrow, value); }
    }

    #endregion

    #region string StatusCurrent

    public const string StatusCurrentProperty = "StatusCurrent";
    [DataMember(Name = StatusCurrentProperty, EmitDefaultValue = false)]
    string mStatusCurrent;
    [PersistantProperty]
    public string StatusCurrent
    {
      get { return mStatusCurrent; }
      set { SetProperty<string>(ref mStatusCurrent, value); }
    }

    #endregion

    #region string Notes

    public const string NotesProperty = "Notes";
    [DataMember(Name = NotesProperty, EmitDefaultValue = false)]
    string mNotes;
    [PersistantProperty]
    public string Notes
    {
      get { return mNotes; }
      set { SetProperty<string>(ref mNotes, value); }
    }

    #endregion

    #region DateTime? ActionDate

    public const string ActionDateProperty = "ActionDate";
    [DataMember(Name = ActionDateProperty, EmitDefaultValue = false)]
    DateTime? mActionDate;
    [PersistantProperty]
    public DateTime? ActionDate
    {
      get { return mActionDate; }
      set { SetProperty<DateTime?>(ref mActionDate, value); }
    }

    #endregion


    #region Location Origin

    public const string OriginProperty = "Origin";
    [PersistantProperty]
    public Location Origin
    {
      get { return GetCachedObject<Location>(); }
      set { SetCachedObject<Location>(value); }
    }

    #endregion

    #region Location Destination

    public const string DestinationProperty = "Destination";
    [PersistantProperty]
    public Location Destination
    {
      get { return GetCachedObject<Location>(); }
      set { SetCachedObject<Location>(value); }
    }

    #endregion



    #region Location LastPosition

    public const string LastPositionProperty = "LastPosition";
    [PersistantProperty]
    public Location LastPosition
    {
      get { return GetCachedObject<Location>(); }
      set { SetCachedObject<Location>(value); }
    }

    #endregion

    #region DateTime? Today

    public const string TodayProperty = "Today";
    [DataMember(Name = TodayProperty, EmitDefaultValue = false)]
    DateTime? mToday;
    [PersistantProperty]
    public DateTime? Today
    {
      get { return mToday; }
      set { SetProperty<DateTime?>(ref mToday, value); }
    }

    #endregion


    #region TransportManifest TransportManifest
    
    public const string TransportManifestProperty = "TransportManifest";
    [DataMember(Name = TransportManifestProperty, EmitDefaultValue = false)]
    TransportManifest mTransportManifest;
    [PersistantProperty(Name="DocumentID")]
    public TransportManifest TransportManifest
    {
      get { return mTransportManifest; }
      set { SetProperty<TransportManifest>(ref mTransportManifest, value); }
    }
    
    #endregion


    #region string Consigners

    public const string ConsignersProperty = "Consigners";
    [DataMember(Name = ConsignersProperty, EmitDefaultValue = false)]
    string mConsigners;
    [PersistantProperty]
    public string Consigners
    {
      get { return mConsigners; }
      set { SetProperty<string>(ref mConsigners, value); }
    }

    #endregion

    #region string Consignees

    public const string ConsigneesProperty = "Consignees";
    [DataMember(Name = ConsigneesProperty, EmitDefaultValue = false)]
    string mConsignees;
    [PersistantProperty]
    public string Consignees
    {
      get { return mConsignees; }
      set { SetProperty<string>(ref mConsignees, value); }
    }

    #endregion

    #region string StatusTodayLocation

    public const string StatusTodayLocationProperty = "StatusTodayLocation";
    public string StatusTodayLocation
    {
      get { return StatusToday == "In-Transit" || StatusTomorrow == "Available" ? StatusToday : String.Format("{0} ({1})", StatusToday, Origin.City.Name); }
    }

    #endregion

    #region string StatusTomorrowLocation

    public const string StatusTomorrowLocationProperty = "StatusTomorrowLocation";
    public string StatusTomorrowLocation
    {
      get { return StatusTomorrow == "In-Transit" || StatusTomorrow == "Available" ? StatusTomorrow : String.Format("{0} ({1})", StatusTomorrow, Destination.City.Name); }
    }

    #endregion

    #region string Trailers

    public const string TrailersProperty = "Trailers";
    public string Trailers
    {
      get { return String.Format((BusinessObject.IsNull(Trailer2) ? "{0}" : "{0}, {1}"), Trailer1.FleetNumber, Trailer2.FleetNumber); }
    }

    #endregion
  }
}

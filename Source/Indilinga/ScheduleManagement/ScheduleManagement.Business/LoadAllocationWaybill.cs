﻿using Afx.Business;
using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using FreightManagement.Business;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace ScheduleManagement.Business
{
  [PersistantObject(Schema = "ScheduleManagement", ReferenceColumn = "Waybill")]
  public partial class LoadAllocationWaybill : AssociativeObject<LoadAllocation, WaybillReference>
  {
  }
}

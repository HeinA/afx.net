﻿using Afx.Business;
using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Documents;
using Afx.Business.Validation;
using ContactManagement.Business;
using FleetManagement.Business;
using Geographics.Business;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace ScheduleManagement.Business
{
  [PersistantObject(Schema = "ScheduleManagement", TableName = "vScheduledPayments_Columns")]
  public partial class SchedulingDailyPayments : BusinessObject 
  {
    public static class ExportKeys
    {
      public const string SchedulingDailyPayments = "SchedulingDailyPayments";
    }

    #region Constructors

    public SchedulingDailyPayments()
    {
    }

    #endregion


    #region bool IsChecked

    public const string IsCheckedProperty = "IsChecked";
    bool mIsChecked;
    public bool IsChecked
    {
      get { return mIsChecked; }
      set { SetProperty<bool>(ref mIsChecked, value); }
    }

    #endregion

    #region DocumentType DocumentType

    public const string DocumentTypeProperty = "DocumentType";
    [PersistantProperty]
    public DocumentType DocumentType
    {
      get { return GetCachedObject<DocumentType>(); }
      set { SetCachedObject<DocumentType>(value); }
    }

    #endregion

    #region strign DocumentNumber
    
    public const string DocumentNumberProperty = "DocumentNumber";
    [DataMember(Name = DocumentNumberProperty, EmitDefaultValue=false)]
    string mDocumentNumber;
    [PersistantProperty]
    public string DocumentNumber
    {
      get { return mDocumentNumber; }
      set { SetProperty<string>(ref mDocumentNumber, value); }
    }
    
    #endregion

    #region DateTime DocumentDate
    
    public const string DocumentDateProperty = "DocumentDate";
    [DataMember(Name = DocumentDateProperty, EmitDefaultValue=false)]
    DateTime mDocumentDate;
    [PersistantProperty]
    public DateTime DocumentDate
    {
      get { return mDocumentDate; }
      set { SetProperty<DateTime>(ref mDocumentDate, value); }
    }
    
    #endregion

    #region Vehicle Vehicle

    public const string VehicleProperty = "Vehicle";
    [PersistantProperty]
    public Vehicle Vehicle
    {
      get { return GetCachedObject<Vehicle>(); }
      set { SetCachedObject<Vehicle>(value); }
    }

    #endregion

    #region string Clients

    public const string ClientsProperty = "Clients";
    [DataMember(Name = ClientsProperty, EmitDefaultValue = false)]
    string mClients;
    [PersistantProperty]
    public string Clients
    {
      get { return mClients; }
      set { SetProperty<string>(ref mClients, value); }
    }

    #endregion


    #region Location Origin
    
    public const string OriginProperty = "Origin";
    [PersistantProperty]
    public Location Origin
    {
      get { return GetCachedObject<Location>(); }
      set { SetCachedObject<Location>(value); }
    }
    
    #endregion

    #region Location Destination
    
    public const string DestinationProperty = "Destination";
    [PersistantProperty]
    public Location Destination
    {
      get { return GetCachedObject<Location>(); }
      set { SetCachedObject<Location>(value); }
    }
    
    #endregion

    #region Employee Driver
    
    public const string DriverProperty = "Driver";
    [PersistantProperty]
    public Employee Driver
    {
      get { return GetCachedObject<Employee>(); }
      set { SetCachedObject<Employee>(value); }
    }
    
    #endregion

    #region Employee CoDriver
    
    public const string CoDriverProperty = "CoDriver";
    [PersistantProperty]
    public Employee CoDriver
    {
      get { return GetCachedObject<Employee>(); }
      set { SetCachedObject<Employee>(value); }
    }
    
    #endregion

    #region Employee BackupDriver
    
    public const string BackupDriverProperty = "BackupDriver";
    [PersistantProperty]
    public Employee BackupDriver
    {
      get { return GetCachedObject<Employee>(); }
      set { SetCachedObject<Employee>(value); }
    }
    
    #endregion


    #region DateTime? ScheduledLoadingDate
    
    public const string ScheduledLoadingDateProperty = "ScheduledLoadingDate";
    [DataMember(Name = ScheduledLoadingDateProperty, EmitDefaultValue=false)]
    DateTime? mScheduledLoadingDate;
    [PersistantProperty]
    public DateTime? ScheduledLoadingDate
    {
      get { return mScheduledLoadingDate; }
      set { SetProperty<DateTime?>(ref mScheduledLoadingDate, value); }
    }
    
    #endregion

    #region DateTime? ScheduledOffloadingDate
    
    public const string ScheduledOffloadingDateProperty = "ScheduledOffloadingDate";
    [DataMember(Name = ScheduledOffloadingDateProperty, EmitDefaultValue=false)]
    DateTime? mScheduledOffloadingDate;
    [PersistantProperty]
    public DateTime? ScheduledOffloadingDate
    {
      get { return mScheduledOffloadingDate; }
      set { SetProperty<DateTime?>(ref mScheduledOffloadingDate, value); }
    }
    
    #endregion

    #region string TrailerTypes

    public const string TrailerTypesProperty = "TrailerTypes";
    [DataMember(Name = TrailerTypesProperty, EmitDefaultValue = false)]
    string mTrailerTypes;
    [PersistantProperty]
    public string TrailerTypes
    {
      get { return mTrailerTypes; }
      set { SetProperty<string>(ref mTrailerTypes, value); }
    }

    #endregion

    #region string Trailers
    
    public const string TrailersProperty = "Trailers";
    [DataMember(Name = TrailersProperty, EmitDefaultValue=false)]
    string mTrailers;
    [PersistantProperty]
    public string Trailers
    {
      get { return mTrailers; }
      set { SetProperty<string>(ref mTrailers, value); }
    }
    
    #endregion


    #region decimal Rate

    public const string RateProperty = "Rate";
    [DataMember(Name = RateProperty, EmitDefaultValue = false)]
    decimal mRate;
    [PersistantProperty]
    public decimal Rate
    {
      get { return mRate; }
      set { SetProperty<decimal>(ref mRate, value); }
    }

    #endregion

    #region string SoftState

    public const string SoftStateProperty = "SoftState";
    [DataMember(Name = SoftStateProperty, EmitDefaultValue = false)]
    string mSoftState;
    [PersistantProperty]
    public string SoftState
    {
      get { return mSoftState; }
      set { SetProperty<string>(ref mSoftState, value); }
    }

    #endregion


    #region decimal NightOutFee
    
    public const string NightOutFeeProperty = "NightOutFee";
    [DataMember(Name = NightOutFeeProperty, EmitDefaultValue=false)]
    decimal mNightOutFee;
    [PersistantProperty]
    public decimal NightOutFee
    {
      get { return mNightOutFee; }
      set { SetProperty<decimal>(ref mNightOutFee, value); }
    }
    
    #endregion

    #region decimal TollFee
    
    public const string TollFeeProperty = "TollFee";
    [DataMember(Name = TollFeeProperty, EmitDefaultValue=false)]
    decimal mTollFee;
    [PersistantProperty]
    public decimal TollFee
    {
      get { return mTollFee; }
      set { SetProperty<decimal>(ref mTollFee, value); }
    }
    
    #endregion

    #region decimal TarpaullinFee
    
    public const string TarpaullinFeeProperty = "TarpaullinFee";
    [DataMember(Name = TarpaullinFeeProperty, EmitDefaultValue=false)]
    decimal mTarpaullinFee;
    [PersistantProperty]
    public decimal TarpaullinFee
    {
      get { return mTarpaullinFee; }
      set { SetProperty<decimal>(ref mTarpaullinFee, value); }
    }
    
    #endregion

    #region decimal CasualFee

    public const string CasualFeeProperty = "CasualFee";
    [DataMember(Name = CasualFeeProperty, EmitDefaultValue = false)]
    decimal mCasualFee;
    [PersistantProperty]
    public decimal CasualFee
    {
      get { return mCasualFee; }
      set { SetProperty<decimal>(ref mCasualFee, value); }
    }

    #endregion

    #region decimal OtherFee

    public const string OtherFeeProperty = "OtherFee";
    [DataMember(Name = OtherFeeProperty, EmitDefaultValue = false)]
    decimal mOtherFee;
    [PersistantProperty]
    public decimal OtherFee
    {
      get { return mOtherFee; }
      set { SetProperty<decimal>(ref mOtherFee, value); }
    }

    #endregion

    #region decimal TotalFees

    public decimal TotalFees
    {
      get { return NightOutFee + TollFee + TarpaullinFee + CasualFee + OtherFee; }
    }

    #endregion

  }
}

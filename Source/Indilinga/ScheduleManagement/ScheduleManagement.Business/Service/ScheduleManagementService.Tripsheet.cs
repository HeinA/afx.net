﻿using Afx.Business.Service;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.WCF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace ScheduleManagement.Business.Service
{
  public partial class ScheduleManagementService
  {
    public ScheduleManagement.Business.Tripsheet LoadTripsheet(Guid globalIdentifier)
    {
      try
      {
        return GetInstance<ScheduleManagement.Business.Tripsheet>(globalIdentifier);
      }
      catch
      {
        throw;
      }
    }

    public ScheduleManagement.Business.Tripsheet SaveTripsheet(ScheduleManagement.Business.Tripsheet obj)
    {
      try
      {
        return Persist<ScheduleManagement.Business.Tripsheet>(obj);
      }
      catch
      {
        throw;
      }
    }
  }
}
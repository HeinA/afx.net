﻿using Afx.Business;
using Afx.Business.Collections;
using Afx.Business.Documents;
using FleetManagement.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace ScheduleManagement.Business.Service
{
  [ServiceContract(Namespace = Namespace.ScheduleManagement)]
  public partial interface IScheduleManagementService : IDisposable
  {

    #region Vehicle Schedules

    [OperationContract]
    BasicCollection<ScheduleManagement.Business.VehicleSchedule> LoadVehicleSchedules(DateTime startDate, DateTime endDate);

    #endregion

    [OperationContract]
    BasicCollection<ScheduleManagement.Business.VehicleSchedule> LoadReconVehicleSchedules(DateTime startDate, DateTime endDate, int driver, int vehicle, string client);

    #region Load Allocations

    [OperationContract]
    BasicCollection<ScheduleManagement.Business.LoadAllocation> LoadScheduledAllocations(int distributionCenter, int route);

    [OperationContract]
    BasicCollection<ScheduleManagement.Business.ScheduledAction> LoadScheduledActions();

    #endregion

    #region Reschedule Vehicle bookings and manifests to new vehicle

    [OperationContract]
    void RescheduleVehicles(Vehicle fromVehicle, Vehicle toVehicle, BasicCollection<VehicleSchedule> bookings);

    #endregion

    #region Quick Transport Manifest Wizard

    [OperationContract]
    ScheduleManagement.Business.QuickTransportManifest LoadQuickTransportManifest(Guid globalIdentifier);

    [OperationContract]
    ScheduleManagement.Business.QuickTransportManifest SaveQuickTransportManifest(ScheduleManagement.Business.QuickTransportManifest obj);

    #endregion


    [OperationContract]
    BasicCollection<SchedulingAction> LoadSchedulingActions(DateTime startDate);

    #region Daily Payment Schedule

    [OperationContract]
    BasicCollection<ScheduledPayment> LoadReconDailyPayments(DateTime startDate, DateTime endDate);

    #endregion

    #region Admin Input

    [OperationContract]
      BasicCollection<SchedulingAdminInput> LoadAdminInput(DateTime startDate, DateTime endDate, string AccountingOrderNumber = "", bool? IsNotEntered = false, IEnumerable<DocumentTypeState> states = null);

    [OperationContract]
    BasicCollection<SchedulingDailyPayments> LoadDailyPayments(DateTime startDate, DateTime endDate);

    #endregion
  }
}

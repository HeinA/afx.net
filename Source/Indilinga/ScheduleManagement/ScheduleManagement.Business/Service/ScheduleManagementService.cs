﻿using Afx.Business;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Documents;
using Afx.Business.Service;
using FleetManagement.Business;
using FreightManagement.Business;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.WCF;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.Data;
using System.Data.Odbc;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace ScheduleManagement.Business.Service
{
  [ServiceBehavior(Namespace = Namespace.ScheduleManagement, ConcurrencyMode = ConcurrencyMode.Multiple)]
  [ExceptionShielding]
  [Export(typeof(IService))]
    public partial class ScheduleManagementService : ServiceBase, IScheduleManagementService
  {

    /// <summary>
    /// Load vehicle schedules between two dates.
    /// </summary>
    /// <param name="startDate">The date to start looking at vehicle schedules.</param>
    /// <param name="endDate">The date at which to stop looking at vehicle schedules.</param>
    /// <returns>A collection of Mixed schedules.</returns>
    public BasicCollection<VehicleSchedule> LoadVehicleSchedules(DateTime startDate, DateTime endDate)
    {
      try
      {
        Collection<IDataFilter> filters = new Collection<IDataFilter>();
        filters.Add(new DataFilter<VehicleSchedule>(VehicleSchedule.RenderOffloadingDateTimeProperty, FilterType.GreaterThanEqual, startDate.Date));
        filters.Add(new DataFilter<VehicleSchedule>(VehicleSchedule.RenderLoadingDateTimeProperty, FilterType.LessThan, endDate.Date.AddDays(1)));

        BasicCollection<VehicleSchedule> col = GetInstances<VehicleSchedule>(filters);


        Collection<IDataFilter> filtersToday = new Collection<IDataFilter>();
        filtersToday.Add(new DataFilter<VehicleSchedule>(VehicleSchedule.RenderOffloadingDateTimeProperty, FilterType.GreaterThanEqual, DateTime.Today.Date));
        filtersToday.Add(new DataFilter<VehicleSchedule>(VehicleSchedule.RenderLoadingDateTimeProperty, FilterType.LessThan, DateTime.Today.Date.AddDays(1)));

        BasicCollection<VehicleSchedule> colToday = GetInstances<VehicleSchedule>(filtersToday);
        
        return new BasicCollection<VehicleSchedule>(col.Union(colToday));
      }
      catch
      {
        throw;
      }
    }

    /// <summary>
    /// Load vehicle schedules between two dates.
    /// </summary>
    /// <param name="startDate">The date to start looking at vehicle schedules.</param>
    /// <param name="endDate">The date at which to stop looking at vehicle schedules.</param>
    /// <returns>A collection of Mixed schedules.</returns>
    public BasicCollection<VehicleSchedule> LoadReconVehicleSchedules(DateTime startDate, DateTime endDate, int driver, int vehicle, string client)
    {
      try
      {
        Collection<IDataFilter> filters = new Collection<IDataFilter>();
        filters.Add(new DataFilter<VehicleSchedule>(VehicleSchedule.RenderOffloadingDateTimeProperty, FilterType.GreaterThanEqual, startDate.Date));
        filters.Add(new DataFilter<VehicleSchedule>(VehicleSchedule.RenderLoadingDateTimeProperty, FilterType.LessThan, endDate.Date.AddDays(1)));
        if (driver != 0) filters.Add(new DataFilter<VehicleSchedule>(VehicleSchedule.DriverProperty, FilterType.Equals, driver));
        if (vehicle != 0) filters.Add(new DataFilter<VehicleSchedule>(VehicleSchedule.VehicleProperty, FilterType.Equals, vehicle));
        if (!String.IsNullOrWhiteSpace(client)) filters.Add(new DataFilter<VehicleSchedule>(VehicleSchedule.ClientsProperty, FilterType.Like, client));

        BasicCollection<VehicleSchedule> col = GetInstances<VehicleSchedule>(filters);


        Collection<IDataFilter> filtersToday = new Collection<IDataFilter>();
        filtersToday.Add(new DataFilter<VehicleSchedule>(VehicleSchedule.RenderOffloadingDateTimeProperty, FilterType.GreaterThanEqual, DateTime.Today.Date));
        filtersToday.Add(new DataFilter<VehicleSchedule>(VehicleSchedule.RenderLoadingDateTimeProperty, FilterType.LessThan, DateTime.Today.Date.AddDays(1)));
        if (driver != 0) filters.Add(new DataFilter<VehicleSchedule>(VehicleSchedule.DriverProperty, FilterType.Equals, driver));
        if (vehicle != 0) filters.Add(new DataFilter<VehicleSchedule>(VehicleSchedule.VehicleProperty, FilterType.Equals, vehicle));
        if (!String.IsNullOrWhiteSpace(client)) filters.Add(new DataFilter<VehicleSchedule>(VehicleSchedule.ClientsProperty, FilterType.Like, client));

        BasicCollection<VehicleSchedule> colToday = GetInstances<VehicleSchedule>(filtersToday);

        return new BasicCollection<VehicleSchedule>(col.Union(colToday));
      }
      catch
      {
        throw;
      }
    }



    #region Load Allocations

    /// <summary>
    /// Loads scheduled allocations between two dates and for a specific DC and route.
    /// </summary>
    /// <param name="startDate">The date to start looking at load allocations.</param>
    /// <param name="endDate">The date at which to stop looking at load allocations.</param>
    /// <param name="distributionCenter">The distribution center to filter on.</param>
    /// <param name="route">The route to filter on.</param>
    /// <returns>A collection Load Allocations.</returns>
    public BasicCollection<ScheduleManagement.Business.LoadAllocation> LoadScheduledAllocations(int distributionCenter, int route)
    {
      try
      {
        using (new ConnectionScope())
        using (new SqlScope("LoadScheduledAllocations"))
        {
          SearchDatagraph sg = new SearchDatagraph(typeof(ScheduleManagement.Business.LoadAllocation))
            .Select(LoadAllocation.DistributionCenterProperty)
            .Select(LoadAllocation.RouteProperty)
            .Join(Document.StateProperty)
              .Join(DocumentState.DocumentTypeStateProperty)
                .Select(DocumentTypeState.GlobalIdentifierProperty)
                .Where(DocumentTypeState.GlobalIdentifierProperty, FilterType.Equals, LoadAllocation.States.Scheduled)
                .EndJoin()
              .EndJoin()
            .Where(LoadAllocation.DistributionCenterProperty, FilterType.Equals, distributionCenter, distributionCenter != 0)
            .Where(LoadAllocation.RouteProperty, FilterType.Equals, route, route != 0);

          SearchResults sr = ExecuteSearch(sg);
          return GetInstances<LoadAllocation>(GetIdentifierCollection(sr));
        }
      }
      catch
      {
        throw;
      }
    }

    /// <summary>
    /// Loads scheduled actions.
    /// </summary>
    /// <returns>A collection of Scheduled Actions.</returns>
    public BasicCollection<ScheduleManagement.Business.ScheduledAction> LoadScheduledActions()
    {
      try
      {
        return GetAllInstances<ScheduleManagement.Business.ScheduledAction>();
      }
      catch
      {
        throw;
      }
    }

    #endregion


    /// <summary>
    /// Reschedule mixed schedules from one vehicle to another vehicle. This function only applies to trucks.
    /// </summary>
    /// <param name="fromVehicle">The vehicle to assign from.</param>
    /// <param name="toVehicle">The vehicle to assign to.</param>
    /// <param name="bookings">The list of bookings to re-assign.</param>
    public void RescheduleVehicles(Vehicle fromVehicle, Vehicle toVehicle, BasicCollection<VehicleSchedule> bookings)
    { 
      try
      {
        using (new ConnectionScope())
        using (new SqlScope("RescheduleVehicles"))
        {
          ObjectRepository<LoadAllocation> orLA = PersistanceManager.GetRepository<LoadAllocation>();
          ObjectRepository<DeliveryManifest> orDelM = PersistanceManager.GetRepository<DeliveryManifest>();
          ObjectRepository<CollectionManifest> orColM = PersistanceManager.GetRepository<CollectionManifest>();
          ObjectRepository<TransportManifest> orTrpM = PersistanceManager.GetRepository<TransportManifest>();
          ObjectRepository<TransferManifest> orTrfM = PersistanceManager.GetRepository<TransferManifest>();
          ObjectRepository<PODHandoverManifest> orPODM = PersistanceManager.GetRepository<PODHandoverManifest>();


          foreach (VehicleSchedule vs in bookings)
          {
            switch (vs.ScheduleType)
            {
              case ScheduleType.LoadAllocation:
                LoadAllocation l = orLA.GetInstance(vs.DocumentIdentifier);
                l.Vehicles.Remove(fromVehicle);
                l.Vehicles.Add(toVehicle);
                orLA.Persist(l);
                break;
              case ScheduleType.Delivery:
                DeliveryManifest d = orDelM.GetInstance(vs.DocumentIdentifier);
                d.Vehicles.Remove(fromVehicle);
                d.Vehicles.Add(toVehicle);
                orDelM.Persist(d);

                break;
              case ScheduleType.Collection:
                CollectionManifest c = orColM.GetInstance(vs.DocumentIdentifier);
                c.Vehicles.Remove(fromVehicle);
                c.Vehicles.Add(toVehicle);
                orColM.Persist(c);

                break;
              case ScheduleType.Transport:
                TransportManifest tp = orTrpM.GetInstance(vs.DocumentIdentifier);
                tp.Vehicles.Remove(fromVehicle);
                tp.Vehicles.Add(toVehicle);
                orTrpM.Persist(tp);

                break;
              case ScheduleType.Transfer:
                TransferManifest tf = orTrfM.GetInstance(vs.DocumentIdentifier);
                tf.Vehicles.Remove(fromVehicle);
                tf.Vehicles.Add(toVehicle);
                orTrfM.Persist(tf);

                break;
              case ScheduleType.PODHandover:
                PODHandoverManifest p = orPODM.GetInstance(vs.DocumentIdentifier);
                p.Vehicles.Remove(fromVehicle);
                p.Vehicles.Add(toVehicle);
                orPODM.Persist(p);

                break;
            }
          }


        }
      }
      catch
      {
        throw;
      }
    }
    

    #region Scheduling Actions

    public BasicCollection<SchedulingAction> LoadSchedulingActions(DateTime startDate)
    {
      try
      {
        Collection<IDataFilter> filters = new Collection<IDataFilter>();
        filters.Add(new DataFilter<SchedulingAction>(SchedulingAction.TodayProperty, FilterType.GreaterThanEqual, startDate.Date));

        BasicCollection<SchedulingAction> col = GetInstances<SchedulingAction>(filters);


        Collection<IDataFilter> filtersToday = new Collection<IDataFilter>();
        filtersToday.Add(new DataFilter<SchedulingAction>(SchedulingAction.TodayProperty, FilterType.GreaterThanEqual, DateTime.Today.Date));

        BasicCollection<SchedulingAction> colToday = GetInstances<SchedulingAction>(filtersToday);

        return new BasicCollection<SchedulingAction>(col.Union(colToday));
      }
      catch
      {
        throw;
      }
    }

    #endregion


    #region Quick Transport Manifest Wizard

    public ScheduleManagement.Business.QuickTransportManifest LoadQuickTransportManifest(Guid globalIdentifier)
    {
      try
      {
        ObjectRepository<ScheduleManagement.Business.QuickTransportManifestLinkage> orQTML = PersistanceManager.GetRepository<ScheduleManagement.Business.QuickTransportManifestLinkage>();
        ObjectRepository<FreightManagement.Business.TransportManifest> orTransportManifest = PersistanceManager.GetRepository<FreightManagement.Business.TransportManifest>();
        ObjectRepository<FreightManagement.Business.Waybill> orWaybill = PersistanceManager.GetRepository<FreightManagement.Business.Waybill>();

        ScheduleManagement.Business.QuickTransportManifestLinkage qtml = orQTML.GetInstance(globalIdentifier);
        if (qtml == null) throw new MessageException("This booking was not made through the wizard.");

        ScheduleManagement.Business.QuickTransportManifest qtm = new QuickTransportManifest();
        using (new EventStateSuppressor(qtm))
        {
          FreightManagement.Business.TransportManifest manifest = orTransportManifest.GetInstance(globalIdentifier);
          qtm.GlobalIdentifier = manifest.GlobalIdentifier;
          qtm.ScheduledLoadingDate = manifest.ScheduledLoadingDate;
          qtm.ScheduledOffLoadingDate = manifest.ScheduledOffloadingDate;
          qtm.SelectedVehicle = manifest.Vehicles.FirstOrDefault();

          FreightManagement.Business.Waybill waybill = orWaybill.GetInstance(qtml.Waybill);

          qtm.Account = waybill.Account;
          qtm.Consigner = waybill.Consigner;
          qtm.Consignee = waybill.Consignee;
          qtm.OriginAddressText = waybill.CollectionAddress;
          qtm.DestinationAddressText = waybill.CollectionAddress;
          qtm.ClientReferenceNumber = waybill.ClientReferenceNumber;
          qtm.IncludeLocalCollection = waybill.IncludeLocalCollection;
          qtm.IncludeLocalDelivery = waybill.IncludeLocalDelivery;
          qtm.Weight = (int)waybill.PhysicalWeight;
          qtm.Items = (int)waybill.TotalItems;
          qtm.Weight = (int)waybill.PhysicalWeight;
          qtm.DeclaredValue = (int)waybill.DeclaredValue;

          #region Fees / Expenses

          TransportManifestFee fee;

          var ft = Cache.Instance.GetObjects<FeeType>(false, ft1 => ft1.IsFlagged(FeeTypeFlags.NightOutFee)).First();
          fee = manifest.Fees.FirstOrDefault(f => f.FeeType.Equals(ft));
          if (fee != null) qtm.FeesNightOut = fee.Amount;

          ft = Cache.Instance.GetObjects<FeeType>(false, ft1 => ft1.IsFlagged(FeeTypeFlags.TollFee)).First();
          fee = manifest.Fees.FirstOrDefault(f => f.FeeType.Equals(ft));
          if (fee != null) qtm.FeesToll = fee.Amount;

          ft = Cache.Instance.GetObjects<FeeType>(false, ft1 => ft1.IsFlagged(FeeTypeFlags.TarpaullingFee)).First();
          fee = manifest.Fees.FirstOrDefault(f => f.FeeType.Equals(ft));
          if (fee != null) qtm.FeesTarpaullin = fee.Amount;

          #endregion

          WaybillChargeType ct = Cache.Instance.GetObjects<WaybillCostComponentType>(false, wct => wct.IsFlagged(WaybillCostComponentTypeFlags.TransportCharge), true).FirstOrDefault();
          qtm.Price = (int)waybill.Charges.Where(c => c.ChargeType.Equals(ct)).Sum(c => c.Charge);
          qtm.HasTripsheet = qtml.Tripsheet.HasValue;
        }

        return qtm;
      }
      catch
      {
        throw;
      }
    }

    public ScheduleManagement.Business.QuickTransportManifest SaveQuickTransportManifest(ScheduleManagement.Business.QuickTransportManifest obj)
    {
      try
      {
        using (new SqlScope("Persist"))
        {
          using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
          using (new ConnectionScope())
          {
            ObjectRepository<ScheduleManagement.Business.QuickTransportManifestLinkage> orQTML = PersistanceManager.GetRepository<ScheduleManagement.Business.QuickTransportManifestLinkage>();
            ObjectRepository<FreightManagement.Business.TransportManifest> orTransportManifest = PersistanceManager.GetRepository<FreightManagement.Business.TransportManifest>();
            ObjectRepository<FreightManagement.Business.ManifestReference> orTransportManifestReference = PersistanceManager.GetRepository<FreightManagement.Business.ManifestReference>();
            ObjectRepository<FreightManagement.Business.Waybill> orWaybill = PersistanceManager.GetRepository<FreightManagement.Business.Waybill>();
            ObjectRepository<FreightManagement.Business.WaybillReference> orWaybillReference = PersistanceManager.GetRepository<FreightManagement.Business.WaybillReference>();
            ObjectRepository<ScheduleManagement.Business.Tripsheet> orTripsheet = PersistanceManager.GetRepository<ScheduleManagement.Business.Tripsheet>();

            Waybill waybill = null;
            TransportManifest manifest = null;
            Tripsheet tripsheet = null;

            ScheduleManagement.Business.QuickTransportManifestLinkage qtml = orQTML.GetInstance(obj.GlobalIdentifier);
            if (qtml != null)
            {
              waybill = orWaybill.GetInstance(qtml.Waybill);
              manifest = orTransportManifest.GetInstance(qtml.GlobalIdentifier);
              if (qtml.Tripsheet.HasValue) tripsheet = orTripsheet.GetInstance(qtml.Tripsheet.Value);
            }
            else
            {
              qtml = new QuickTransportManifestLinkage();
              qtml.GlobalIdentifier = obj.GlobalIdentifier;

              waybill = new Waybill();
              qtml.Waybill = waybill.GlobalIdentifier;

              manifest = new TransportManifest();
              manifest.GlobalIdentifier = qtml.GlobalIdentifier;
            }

            #region Waybill

            waybill.Account = obj.Account;
            waybill.Consigner = obj.Consigner;
            waybill.Consignee = obj.Consignee;
            waybill.CollectionAddress = obj.OriginAddressText;
            waybill.DeliveryAddress = obj.DestinationAddressText;
            waybill.ClientReferenceNumber = obj.ClientReferenceNumber;
            waybill.IncludeLocalCollection = obj.IncludeLocalCollection;
            waybill.IncludeLocalDelivery = obj.IncludeLocalDelivery;
            waybill.IsPointToPoint = true;

            #region Waybill Cargo

            WaybillCargoItem newCargoItem = new WaybillCargoItem();
            foreach (var item in waybill.Items)
            {
              item.IsDeleted = true;
            }
            waybill.Items.Add(newCargoItem);
            newCargoItem.CargoType = obj.CargoType;
            newCargoItem.DeclaredValue = 0;
            newCargoItem.Description = obj.Description;
            newCargoItem.Items = obj.Items;
            newCargoItem.PhysicalWeight = obj.Weight;

            #endregion

            #region Waybill Charges

            WaybillChargeType ct = Cache.Instance.GetObjects<WaybillCostComponentType>(false, wct => wct.IsFlagged(WaybillCostComponentTypeFlags.TransportCharge), true).FirstOrDefault();
            foreach (var c in waybill.Charges.Where(c1 => c1.ChargeType.Equals(ct) && c1.SystemGenerated))
            {
              c.IsDeleted = true;
            }
            WaybillCharge newWaybillCharge = new WaybillCharge();
            newWaybillCharge.Charge = obj.Price;
            newWaybillCharge.ChargeType = ct;
            newWaybillCharge.Description = string.Empty;
            newWaybillCharge.SystemGenerated = true;
            waybill.Charges.Add(newWaybillCharge);

            #endregion

            orWaybill.Persist(waybill);
            waybill = orWaybill.GetInstance(waybill.GlobalIdentifier);
            FreightManagement.Business.WaybillReference wr = orWaybillReference.GetInstance(waybill.GlobalIdentifier); 

            #endregion

            #region Manifest

            manifest.ScheduledLoadingDate = obj.ScheduledLoadingDate;
            manifest.ScheduledOffloadingDate = obj.ScheduledOffLoadingDate;
            manifest.ScheduledLoadingSlot = obj.ScheduledLoadingSlot;
            manifest.ScheduledOffloadingSlot = obj.ScheduledOffLoadingSlot;

            manifest.FeesNightout = obj.FeesNightOut;
            manifest.FeesTarpaullin = obj.FeesTarpaullin;
            manifest.FeesToll = obj.FeesToll;

            manifest.BorderPost = obj.BorderPost;

            manifest.Driver = obj.Driver;
            manifest.CoDriver = obj.CoDriver;
            manifest.BackupDriver = obj.BackupDriver;

            if (!manifest.Vehicles.Contains(obj.SelectedVehicle)) manifest.Vehicles.Add(obj.SelectedVehicle);
            if (!manifest.Waybills.Contains(wr)) manifest.Waybills.Add(wr);

            #region Fees / Expenses 

            TransportManifestFee fee;

            var ft = Cache.Instance.GetObjects<FeeType>(false, ft1 => ft1.IsFlagged(FeeTypeFlags.NightOutFee)).First();
            fee = manifest.Fees.FirstOrDefault(f => f.FeeType.Equals(ft));
            if (fee == null)
            {
              fee = new TransportManifestFee();
              manifest.Fees.Add(fee);
              fee.FeeType = ft;
            }
            fee.PaymentDate = (DateTime)obj.ScheduledLoadingDate;
            fee.Amount = obj.FeesNightOut;

            ft = Cache.Instance.GetObjects<FeeType>(false, ft1 => ft1.IsFlagged(FeeTypeFlags.TollFee)).First();
            fee = manifest.Fees.FirstOrDefault(f => f.FeeType.Equals(ft));
            if (fee == null)
            {
              fee = new TransportManifestFee();
              manifest.Fees.Add(fee);
              fee.FeeType = ft;
            }
            fee.PaymentDate = (DateTime)obj.ScheduledLoadingDate;
            fee.Amount = obj.FeesToll;

            ft = Cache.Instance.GetObjects<FeeType>(false, ft1 => ft1.IsFlagged(FeeTypeFlags.TarpaullingFee)).First();
            fee = manifest.Fees.FirstOrDefault(f => f.FeeType.Equals(ft));
            if (fee == null)
            {
              fee = new TransportManifestFee();
              manifest.Fees.Add(fee);
              fee.FeeType = ft;
            }
            fee.PaymentDate = (DateTime)obj.ScheduledLoadingDate;
            fee.Amount = obj.FeesTarpaullin;

            #endregion

            orTransportManifest.Persist(manifest);
            manifest = orTransportManifest.GetInstance(manifest.GlobalIdentifier);

            #endregion

            #region Tripsheet 

            if (obj.CreateTripsheet || obj.HasTripsheet)
            {
              if (tripsheet == null)
              {
                tripsheet = new Tripsheet();
                qtml.Tripsheet = tripsheet.GlobalIdentifier;
              }

              tripsheet.Driver = manifest.Driver;
              if (!tripsheet.Vehicles.Contains(obj.SelectedVehicle)) tripsheet.Vehicles.Add(obj.SelectedVehicle);

              FreightManagement.Business.ManifestReference mr = orTransportManifestReference.GetInstance(manifest.GlobalIdentifier); 
              if (!tripsheet.Manifests.Contains(mr)) tripsheet.Manifests.Add(mr);

              orTripsheet.Persist(tripsheet);
            }

            #endregion

            orQTML.Persist(qtml);
            obj =  LoadQuickTransportManifest(qtml.GlobalIdentifier);
            scope.Complete();
            return obj;
          }
        }
      }
      catch
      {
        throw;
      }
    }

    #endregion

    #region Admin Input

    public BasicCollection<SchedulingAdminInput> LoadAdminInput(DateTime startDate, DateTime endDate, string AccountingOrderNumber = "", bool? IsNotEntered = false, IEnumerable<DocumentTypeState> states = null)
    {
      try
      {
        Collection<IDataFilter> filters = new Collection<IDataFilter>();
        filters.Add(new DataFilter<SchedulingAdminInput>(SchedulingAdminInput.ScheduledLoadingDateProperty, FilterType.GreaterThanEqual, startDate.Date));
        filters.Add(new DataFilter<SchedulingAdminInput>(SchedulingAdminInput.ScheduledLoadingDateProperty, FilterType.LessThanEqual, endDate.Date.AddDays(1)));
        //if (!String.IsNullOrWhiteSpace(AccountingOrderNumber) && !IsNotEntered) filters.Add(new DataFilter<SchedulingAdminInput>(SchedulingAdminInput.OrderProperty, FilterType.Like, "%" + AccountingOrderNumber + "%"));
        //if (IsNotEntered) filters.Add(new DataFilter<SchedulingAdminInput>(SchedulingAdminInput.OrderProperty, FilterType.Equals, ""));
        if (states != null) filters.Add(new DataFilter<SchedulingAdminInput>(SchedulingAdminInput.WaybillStateProperty, FilterType.In, states));

        BasicCollection<SchedulingAdminInput> col = GetInstances<SchedulingAdminInput>(filters);


        Collection<IDataFilter> filtersToday = new Collection<IDataFilter>();
        filtersToday.Add(new DataFilter<SchedulingAdminInput>(SchedulingAdminInput.ScheduledLoadingDateProperty, FilterType.GreaterThanEqual, DateTime.Today.Date));
        filtersToday.Add(new DataFilter<SchedulingAdminInput>(SchedulingAdminInput.ScheduledLoadingDateProperty, FilterType.LessThanEqual, DateTime.Today.Date.AddDays(1)));
        //if (!String.IsNullOrWhiteSpace(AccountingOrderNumber) && !IsNotEntered) filters.Add(new DataFilter<SchedulingAdminInput>(SchedulingAdminInput.OrderProperty, FilterType.Like, "%" + AccountingOrderNumber + "%"));
        //if (IsNotEntered) filters.Add(new DataFilter<SchedulingAdminInput>(SchedulingAdminInput.OrderProperty, FilterType.Equals, ""));
        if (states != null) filters.Add(new DataFilter<SchedulingAdminInput>(SchedulingAdminInput.WaybillStateProperty, FilterType.In, states));

        BasicCollection<SchedulingAdminInput> colToday = GetInstances<SchedulingAdminInput>(filtersToday);

        return new BasicCollection<SchedulingAdminInput>(col.Union(colToday));
      }
      catch
      {
        throw;
      }
    }

    #endregion

    #region Daily Payment Schedule

    public BasicCollection<ScheduledPayment> LoadReconDailyPayments(DateTime startDate, DateTime endDate)
    {
      try
      {
        Collection<IDataFilter> filters = new Collection<IDataFilter>();
        filters.Add(new DataFilter<ScheduledPayment>(ScheduledPayment.PaymentDateProperty, FilterType.GreaterThanEqual, startDate.Date));
        filters.Add(new DataFilter<ScheduledPayment>(ScheduledPayment.PaymentDateProperty, FilterType.LessThanEqual, endDate.Date.AddDays(1)));

        BasicCollection<ScheduledPayment> col = GetInstances<ScheduledPayment>(filters);


        Collection<IDataFilter> filtersToday = new Collection<IDataFilter>();
        filtersToday.Add(new DataFilter<ScheduledPayment>(ScheduledPayment.PaymentDateProperty, FilterType.GreaterThanEqual, DateTime.Today.Date));
        filtersToday.Add(new DataFilter<ScheduledPayment>(ScheduledPayment.PaymentDateProperty, FilterType.LessThanEqual, DateTime.Today.Date.AddDays(1)));

        BasicCollection<ScheduledPayment> colToday = GetInstances<ScheduledPayment>(filtersToday);

        return new BasicCollection<ScheduledPayment>(col.Union(colToday));
      }
      catch
      {
        throw;
      }
    }

    public BasicCollection<SchedulingDailyPayments> LoadDailyPayments(DateTime startDate, DateTime endDate)
    {
      try
      {
        Collection<IDataFilter> filters = new Collection<IDataFilter>();
        filters.Add(new DataFilter<SchedulingDailyPayments>(SchedulingDailyPayments.ScheduledLoadingDateProperty, FilterType.GreaterThanEqual, startDate.Date));
        filters.Add(new DataFilter<SchedulingDailyPayments>(SchedulingDailyPayments.ScheduledLoadingDateProperty, FilterType.LessThanEqual, endDate.Date.AddDays(1)));

        BasicCollection<SchedulingDailyPayments> col = GetInstances<SchedulingDailyPayments>(filters);


        Collection<IDataFilter> filtersToday = new Collection<IDataFilter>();
        filtersToday.Add(new DataFilter<SchedulingDailyPayments>(SchedulingDailyPayments.ScheduledLoadingDateProperty, FilterType.GreaterThanEqual, DateTime.Today.Date));
        filtersToday.Add(new DataFilter<SchedulingDailyPayments>(SchedulingDailyPayments.ScheduledLoadingDateProperty, FilterType.LessThanEqual, DateTime.Today.Date.AddDays(1)));

        BasicCollection<SchedulingDailyPayments> colToday = GetInstances<SchedulingDailyPayments>(filtersToday);

        return new BasicCollection<SchedulingDailyPayments>(col.Union(colToday));
      }
      catch
      {
        throw;
      }
    }

    #endregion
  }
}

﻿using Afx.Business.Collections;
using Afx.Business.Service;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.WCF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace ScheduleManagement.Business.Service
{
  public partial class ScheduleManagementService
  {
    public BasicCollection<ScheduleManagement.Business.MaintenanceType> LoadMaintenanceTypeCollection()
    {
      try
      {
        return GetAllInstances<ScheduleManagement.Business.MaintenanceType>();
      }
      catch
      {
        throw;
      }
    }

    public BasicCollection<ScheduleManagement.Business.MaintenanceType> SaveMaintenanceTypeCollection(BasicCollection<ScheduleManagement.Business.MaintenanceType> col)
    {
      try
      {
        return Persist<ScheduleManagement.Business.MaintenanceType>(col);
      }
      catch
      {
        throw;
      }
    }
  }
}
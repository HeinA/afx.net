﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace ScheduleManagement.Business.Service
{
  public partial interface IScheduleManagementService
  {
    [OperationContract]
    ScheduleManagement.Business.MaintenanceBooking LoadMaintenanceBooking(Guid globalIdentifier);

    [OperationContract]
    ScheduleManagement.Business.MaintenanceBooking SaveMaintenanceBooking(ScheduleManagement.Business.MaintenanceBooking obj);
  }
}

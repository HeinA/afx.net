﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace ScheduleManagement.Business.Service
{
  public partial interface IScheduleManagementService
  {
    [OperationContract]
    ScheduleManagement.Business.LoadAllocation LoadLoadAllocation(Guid globalIdentifier);

    [OperationContract]
    ScheduleManagement.Business.LoadAllocation SaveLoadAllocation(ScheduleManagement.Business.LoadAllocation obj);
  }
}

﻿using Afx.Business.Collections;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace ScheduleManagement.Business.Service
{
  public partial interface IScheduleManagementService
  {
    [OperationContract]
    BasicCollection<ScheduleManagement.Business.MaintenanceType> LoadMaintenanceTypeCollection();

    [OperationContract]
    BasicCollection<ScheduleManagement.Business.MaintenanceType> SaveMaintenanceTypeCollection(BasicCollection<ScheduleManagement.Business.MaintenanceType> col);
  }
}


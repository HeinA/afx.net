﻿using Afx.Business.Service;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.WCF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace ScheduleManagement.Business.Service
{
  public partial class ScheduleManagementService
  {
    public ScheduleManagement.Business.LoadAllocation LoadLoadAllocation(Guid globalIdentifier)
    {
      try
      {
        return GetInstance<ScheduleManagement.Business.LoadAllocation>(globalIdentifier);
      }
      catch
      {
        throw;
      }
    }

    public ScheduleManagement.Business.LoadAllocation SaveLoadAllocation(ScheduleManagement.Business.LoadAllocation obj)
    {
      try
      {
        return Persist<ScheduleManagement.Business.LoadAllocation>(obj);
      }
      catch
      {
        throw;
      }
    }
  }
}
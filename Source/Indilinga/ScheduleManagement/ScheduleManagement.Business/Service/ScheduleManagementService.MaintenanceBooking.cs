﻿using Afx.Business.Service;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.WCF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace ScheduleManagement.Business.Service
{
  public partial class ScheduleManagementService
  {
    public ScheduleManagement.Business.MaintenanceBooking LoadMaintenanceBooking(Guid globalIdentifier)
    {
      try
      {
        return GetInstance<ScheduleManagement.Business.MaintenanceBooking>(globalIdentifier);
      }
      catch
      {
        throw;
      }
    }

    public ScheduleManagement.Business.MaintenanceBooking SaveMaintenanceBooking(ScheduleManagement.Business.MaintenanceBooking obj)
    {
      try
      {
        return Persist<ScheduleManagement.Business.MaintenanceBooking>(obj);
      }
      catch
      {
        throw;
      }
    }
  }
}
﻿using AccountManagement.Business;
using Afx.Business;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Documents;
using Afx.Business.Validation;
using FleetManagement.Business;
using Geographics.Business;
using System;
using System.Runtime.Serialization;

namespace ScheduleManagement.Business
{
  [PersistantObject(Schema = "ScheduleManagement")]
  public partial class MaintenanceBooking : Document, IVehicleCollection
  {
    public MaintenanceBooking()
      : base(Cache.Instance.GetObject<DocumentType>(DocumentTypeIdentifier))
    {
    }


    #region AccountReference Account

    public const string AccountProperty = "Account";
    [DataMember(Name = AccountProperty, EmitDefaultValue = false)]
    AccountReference mAccount;
    [PersistantProperty]
    //[Mandatory("Account is mandatory")]
    public AccountReference Account
    {
      get { return mAccount; }
      set { SetProperty<AccountReference>(ref mAccount, value); }
    }

    #endregion

    #region DateTime STD

    public const string STDProperty = "STD";
    [DataMember(Name = STDProperty, EmitDefaultValue = false)]
    DateTime mSTD = DateTime.Now;
    [PersistantProperty]
    public DateTime STD
    {
      get { return mSTD; }
      set { SetProperty<DateTime>(ref mSTD, value); }
    }

    #endregion

    #region DateTime STA

    public const string STAProperty = "STA";
    [DataMember(Name = STAProperty, EmitDefaultValue = false)]
    DateTime mSTA = DateTime.Now;
    [PersistantProperty]
    public DateTime STA
    {
      get { return mSTA; }
      set { SetProperty<DateTime>(ref mSTA, value); }
    }

    #endregion

    #region Location Origin

    public const string OriginProperty = "Origin";
    [PersistantProperty]
    public Location Origin
    {
      get { return GetCachedObject<Location>(); }
      set { SetCachedObject<Location>(value); }
    }

    #endregion

    //#region Vehicle Vehicle

    //public const string VehicleProperty = "Vehicle";
    //[PersistantProperty]
    //[Mandatory("Vehicle is mandatory")]
    //public Vehicle Vehicle
    //{
    //  get { return GetCachedObject<Vehicle>(); }
    //  set { SetCachedObject<Vehicle>(value); }
    //}

    //#endregion

    #region MaintenanceType MaintenanceType

    public const string MaintenanceTypeProperty = "MaintenanceType";
    [PersistantProperty]
    public MaintenanceType MaintenanceType
    {
      get { return GetCachedObject<MaintenanceType>(); }
      set { SetCachedObject<MaintenanceType>(value); }
    }

    #endregion

    #region string Comments

    public const string CommentsProperty = "Comments";
    [DataMember(Name = CommentsProperty, EmitDefaultValue = false)]
    string mComments;
    [PersistantProperty]
    public string Comments
    {
      get { return mComments; }
      set { SetProperty<string>(ref mComments, value); }
    }

    #endregion

    #region Associative BusinessObjectCollection<Vehicle> Vehicles

    public const string VehiclesProperty = "Vehicles";
    AssociativeObjectCollection<MaintenanceBookingVehicle, Vehicle> mVehicles;
    [PersistantCollection(AssociativeType = typeof(MaintenanceBookingVehicle))]
    [Mandatory("Vehicles are mandatory")]
    public BusinessObjectCollection<Vehicle> Vehicles
    {
      get
      {
        if (mVehicles == null) using (new EventStateSuppressor(this)) { mVehicles = new AssociativeObjectCollection<MaintenanceBookingVehicle, Vehicle>(this); }
        return mVehicles;
      }
    }

    #endregion
  }
}

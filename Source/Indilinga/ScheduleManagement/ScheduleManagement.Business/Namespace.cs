﻿using Afx.Business;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScheduleManagement.Business
{
  [Export(typeof(INamespace))]
  public class Namespace : INamespace
  {
    public const string ScheduleManagement = "http://indilinga.com/ScheduleManagement/Business";

    public string GetUri()
    {
      return ScheduleManagement;
    }
  }
}

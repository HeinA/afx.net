﻿using Afx.Business;
using Afx.Business.Data;
using Afx.Business.Documents;
using ContactManagement.Business;
using FreightManagement.Business;
using Geographics.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ScheduleManagement.Business
{
  [PersistantObject(Schema = "ScheduleManagement", TableName = "vScheduledPayments")]
  public partial class ScheduledPayment : BusinessObject
  {
    public static class ExportKeys
    {
      public const string ScheduledPayments = "ScheduledPayments";
    }

    #region Constructors

    public ScheduledPayment()
    {
    }

    #endregion


    #region bool IsChecked

    public const string IsCheckedProperty = "IsChecked";
    bool mIsChecked;
    public bool IsChecked
    {
      get { return mIsChecked; }
      set { SetProperty<bool>(ref mIsChecked, value); }
    }

    #endregion


    #region int DocId
    
    public const string DocIdProperty = "DocId";
    [DataMember(Name = DocIdProperty, EmitDefaultValue=false)]
    int mDocId;
    [PersistantProperty]
    public int DocId
    {
      get { return mDocId; }
      set { SetProperty<int>(ref mDocId, value); }
    }
    
    #endregion

    #region Guid DocGlobalIdentifier
    
    public const string DocGlobalIdentifierProperty = "DocGlobalIdentifier";
    [DataMember(Name = DocGlobalIdentifierProperty, EmitDefaultValue=false)]
    Guid mDocGlobalIdentifier;
    [PersistantProperty]
    public Guid DocGlobalIdentifier
    {
      get { return mDocGlobalIdentifier; }
      set { SetProperty<Guid>(ref mDocGlobalIdentifier, value); }
    }
    
    #endregion	

    #region string DocumentNumber
    
    public const string DocumentNumberProperty = "DocumentNumber";
    [DataMember(Name = DocumentNumberProperty, EmitDefaultValue=false)]
    string mDocumentNumber;
    [PersistantProperty]
    public string DocumentNumber
    {
      get { return mDocumentNumber; }
      set { SetProperty<string>(ref mDocumentNumber, value); }
    }
    
    #endregion

    #region DateTime DocumentDate
    
    public const string DocumentDateProperty = "DocumentDate";
    [DataMember(Name = DocumentDateProperty, EmitDefaultValue=false)]
    DateTime mDocumentDate;
    [PersistantProperty]
    public DateTime DocumentDate
    {
      get { return mDocumentDate; }
      set { SetProperty<DateTime>(ref mDocumentDate, value); }
    }
    
    #endregion

    #region Location Origin

    public const string OriginProperty = "Origin";
    [PersistantProperty]
    public Location Origin
    {
      get { return GetCachedObject<Location>(); }
      set { SetCachedObject<Location>(value); }
    }

    #endregion

    #region Location Destination

    public const string DestinationProperty = "Destination";
    [PersistantProperty]
    public Location Destination
    {
      get { return GetCachedObject<Location>(); }
      set { SetCachedObject<Location>(value); }
    }

    #endregion


    #region Employee Driver
    
    public const string DriverProperty = "Driver";
    [PersistantProperty]
    public Employee Driver
    {
      get { return GetCachedObject<Employee>(); }
      set { SetCachedObject<Employee>(value); }
    }
    
    #endregion

    #region Employee CoDriver
    
    public const string CoDriverProperty = "CoDriver";
    [PersistantProperty]
    public Employee CoDriver
    {
      get { return GetCachedObject<Employee>(); }
      set { SetCachedObject<Employee>(value); }
    }
    
    #endregion

    #region Employee BackupDriver
    
    public const string BackupDriverProperty = "BackupDriver";
    [PersistantProperty]
    public Employee BackupDriver
    {
      get { return GetCachedObject<Employee>(); }
      set { SetCachedObject<Employee>(value); }
    }
    
    #endregion

    #region DateTime? ScheduledLoadingDate
    
    public const string ScheduledLoadingDateProperty = "ScheduledLoadingDate";
    [DataMember(Name = ScheduledLoadingDateProperty, EmitDefaultValue=false)]
    DateTime? mScheduledLoadingDate;
    [PersistantProperty]
    public DateTime? ScheduledLoadingDate
    {
      get { return mScheduledLoadingDate; }
      set { SetProperty<DateTime?>(ref mScheduledLoadingDate, value); }
    }
    
    #endregion

    #region DateTime? ScheduledOffloadingDate
    
    public const string ScheduledOffloadingDateProperty = "ScheduledOffloadingDate";
    [DataMember(Name = ScheduledOffloadingDateProperty, EmitDefaultValue=false)]
    DateTime? mScheduledOffloadingDate;
    [PersistantProperty]
    public DateTime? ScheduledOffloadingDate
    {
      get { return mScheduledOffloadingDate; }
      set { SetProperty<DateTime?>(ref mScheduledOffloadingDate, value); }
    }
    
    #endregion

    #region FeeType FeeType
    
    public const string FeeTypeProperty = "FeeType";
    [PersistantProperty]
    public FeeType FeeType
    {
      get { return GetCachedObject<FeeType>(); }
      set { SetCachedObject<FeeType>(value); }
    }
    
    #endregion

    #region string FeeTypeName
    
    public const string FeeTypeNameProperty = "FeeTypeName";
    [DataMember(Name = FeeTypeNameProperty, EmitDefaultValue=false)]
    string mFeeTypeName;
    [PersistantProperty]
    public string FeeTypeName
    {
      get { return mFeeTypeName; }
      set { SetProperty<string>(ref mFeeTypeName, value); }
    }
    
    #endregion

    #region DateTime PaymentDate
    
    public const string PaymentDateProperty = "PaymentDate";
    [DataMember(Name = PaymentDateProperty, EmitDefaultValue=false)]
    DateTime mPaymentDate;
    [PersistantProperty]
    public DateTime PaymentDate
    {
      get { return mPaymentDate; }
      set { SetProperty<DateTime>(ref mPaymentDate, value); }
    }
    
    #endregion

    #region decimal Amount
    
    public const string AmountProperty = "Amount";
    [DataMember(Name = AmountProperty, EmitDefaultValue=false)]
    decimal mAmount;
    [PersistantProperty]
    public decimal Amount
    {
      get { return mAmount; }
      set { SetProperty<decimal>(ref mAmount, value); }
    }
    
    #endregion

    #region bool Paid
    
    public const string PaidProperty = "Paid";
    [DataMember(Name = PaidProperty, EmitDefaultValue=false)]
    bool mPaid;
    [PersistantProperty]
    public bool Paid
    {
      get { return mPaid; }
      set { SetProperty<bool>(ref mPaid, value); }
    }
    
    #endregion

    #region bool ReceiptReceived
    
    public const string ReceiptReceivedProperty = "ReceiptReceived";
    [DataMember(Name = ReceiptReceivedProperty, EmitDefaultValue=false)]
    bool mReceiptReceived;
    [PersistantProperty]
    public bool ReceiptReceived
    {
      get { return mReceiptReceived; }
      set { SetProperty<bool>(ref mReceiptReceived, value); }
    }
    
    #endregion

    #region string ReceiptNumber
    
    public const string ReceiptNumberProperty = "ReceiptNumber";
    [DataMember(Name = ReceiptNumberProperty, EmitDefaultValue=false)]
    string mReceiptNumber;
    [PersistantProperty]
    public string ReceiptNumber
    {
      get { return mReceiptNumber; }
      set { SetProperty<string>(ref mReceiptNumber, value); }
    }
    
    #endregion
  }
}

﻿using Afx.Business.Data;
using Afx.Business.Documents;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ScheduleManagement.Business
{
  [DataContract(IsReference = true, Namespace = Namespace.ScheduleManagement)]
  public partial class Tripsheet
  {
    public const string DocumentTypeIdentifier = "{fbe4e5b9-d6c7-4d8b-b5e2-141d3a0cc4bc}";
    public class States
    {
      public const string Cancelled = "{b3357dce-e7e9-476b-83c0-604f4a2a66ec}";
      public const string Closed = "{756368c9-f811-49be-89ed-a2d2915a5482}";
      public const string Open = "{ba2b4a5a-7ca6-4b91-ad0d-44a1fdae6468}";
    }

    protected Tripsheet(DocumentType documentType)
      : base(documentType)
    {
    }
  }
}

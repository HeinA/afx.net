﻿using Afx.Business;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Documents;
using Afx.Business.Validation;
using ContactManagement.Business;
using DocumentManagement.Business;
using FleetManagement.Business;
using FreightManagement.Business;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace ScheduleManagement.Business
{
  [PersistantObject(Schema = "ScheduleManagement")]
  public partial class Tripsheet : Document, IVehicleCollection
  {
    public Tripsheet()
      : base(Cache.Instance.GetObject<DocumentType>(DocumentTypeIdentifier))
    {
    }

    /// <summary>
    /// Contractor for trip.
    /// </summary>
    #region Contractor Contractor

    public const string ContractorProperty = "Contractor";
    [PersistantProperty(IsCached = true)]
    public Contractor Contractor
    {
      get { return GetCachedObject<Contractor>(); }
      set { SetCachedObject<Contractor>(value); }
    }

    #endregion

    /// <summary>
    /// Lead driver for trip.
    /// </summary>
    #region Contact Driver

    public const string DriverProperty = "Driver";
    [PersistantProperty(IsCached = true)]
    //[Mandatory("Driver is mandatory")]
    public Contact Driver
    {
      get { return GetCachedObject<Contact>(); }
      set { SetCachedObject<Contact>(value); }
    }

    #region string DriverMobilePhone

    public const string DriverMobilePhoneProperty = "DriverMobilePhone";
    public string DriverMobilePhone
    {
      get
      {
        ComplexContact cc = Driver as ComplexContact;
        if (cc != null)
        {
          return cc.TelephoneNumbers.Where(x => x.TelephoneType.SystemTelephoneType == SystemTelephoneType.Mobile).FirstOrDefault().TelephoneNumber;
        }
        return "-";
      }
    }

    #endregion

    #region string DriverLandlinePhone

    public const string DriverLandlinePhoneProperty = "DriverLandlinePhone";
    public string DriverLandlinePhone
    {
      get
      {
        ComplexContact cc = Driver as ComplexContact;
        if (cc != null)
        {
          return cc.TelephoneNumbers.Where(x => x.TelephoneType.SystemTelephoneType == SystemTelephoneType.Landline).FirstOrDefault().TelephoneNumber;
        }
        return "-";
      }
    }

    #endregion

    #endregion

    /// <summary>
    /// Co-driver for trip.
    /// </summary>
    #region Contact CoDriver

    public const string CoDriverProperty = "CoDriver";
    [PersistantProperty(IsCached = true)]
    public Contact CoDriver
    {
      get { return GetCachedObject<Contact>(); }
      set { SetCachedObject<Contact>(value); }
    }

    #endregion

    /// <summary>
    /// Backup driver for trip.
    /// </summary>
    #region Contact BackupDriver

    public const string BackupDriverProperty = "BackupDriver";
    [PersistantProperty(IsCached = true)]
    public Contact BackupDriver
    {
      get { return GetCachedObject<Contact>(); }
      set { SetCachedObject<Contact>(value); }
    }

    #endregion

    #region int OdometerStart

    public const string OdometerStartProperty = "OdometerStart";
    [DataMember(Name = OdometerStartProperty, EmitDefaultValue = false)]
    int mOdometerStart;
    [PersistantProperty]
    public int OdometerStart
    {
      get { return mOdometerStart; }
      set { SetProperty<int>(ref mOdometerStart, value); }
    }

    #endregion

    #region int OdometerEnd

    public const string OdometerEndProperty = "OdometerEnd";
    [DataMember(Name = OdometerEndProperty, EmitDefaultValue = false)]
    int mOdometerEnd;
    [PersistantProperty]
    public int OdometerEnd
    {
      get { return mOdometerEnd; }
      set { SetProperty<int>(ref mOdometerEnd, value); }
    }

    #endregion

    #region decimal FuelLiters

    public const string FuelLitersProperty = "FuelLiters";
    [DataMember(Name = FuelLitersProperty, EmitDefaultValue = false)]
    decimal mFuelLiters;
    [PersistantProperty]
    public decimal FuelLiters
    {
      get { return mFuelLiters; }
      set { SetProperty<decimal>(ref mFuelLiters, value); }
    }

    #endregion

    #region DateTime? StartDate

    public const string StartDateProperty = "StartDate";
    [DataMember(Name = StartDateProperty, EmitDefaultValue = false)]
    DateTime? mStartDate;
    [PersistantProperty]
    public DateTime? StartDate
    {
      get { return mStartDate; }
      set { SetProperty<DateTime?>(ref mStartDate, value); }
    }

    #endregion

    #region DateTime? EndDate

    public const string EndDateProperty = "EndDate";
    [DataMember(Name = EndDateProperty, EmitDefaultValue = false)]
    DateTime? mEndDate;
    [PersistantProperty]
    public DateTime? EndDate
    {
      get { return mEndDate; }
      set { SetProperty<DateTime?>(ref mEndDate, value); }
    }

    #endregion

    #region Associative BusinessObjectCollection<Vehicle> Vehicles

    public const string VehiclesProperty = "Vehicles";
    AssociativeObjectCollection<TripsheetVehicle, Vehicle> mVehicles;
    [PersistantCollection(AssociativeType = typeof(TripsheetVehicle))]
    //[DataMember]  //**** Uncomment if the collection type is not cached ****//
    public BusinessObjectCollection<Vehicle> Vehicles
    {
      get
      {
        if (mVehicles == null) using (new EventStateSuppressor(this))
          {
            mVehicles = new AssociativeObjectCollection<TripsheetVehicle, Vehicle>(this);
          }
        return mVehicles;
      }
    }

    #endregion
   
    #region Associative BusinessObjectCollection<ManifestReference> Manifests

    public const string ManifestsProperty = "Manifests";
    AssociativeObjectCollection<TripsheetManifest, ManifestReference> mManifests;
    [PersistantCollection(AssociativeType = typeof(TripsheetManifest))]
    //[DataMember]  //**** Uncomment if the collection type is not cached ****//
    public BusinessObjectCollection<ManifestReference> Manifests
    {
      get
      {
        if (mManifests == null) using (new EventStateSuppressor(this)) { mManifests = new AssociativeObjectCollection<TripsheetManifest, ManifestReference>(this); }
        return mManifests;
      }
    }

    #endregion

    #region BusinessObjectCollection<TripsheetFee> Fees

    public const string FeesProperty = "Fees";
    BusinessObjectCollection<TripsheetFee> mFees;
    [PersistantCollection]
    [DataMember]
    public BusinessObjectCollection<TripsheetFee> Fees
    {
      get { return mFees ?? (mFees = new BusinessObjectCollection<TripsheetFee>(this)); }
    }

    #endregion

    #region decimal TotalVehicleLoadWeight

    public const string TotalVehicleLoadWeightProperty = "TotalVehicleLoadWeight";
    decimal mTotalVehicleLoadWeight;
    public decimal TotalVehicleLoadWeight
    {
      get
      {
        CalculateVehicleWeights();
        return mTotalVehicleLoadWeight;
      }

      private set { SetProperty<decimal>(ref mTotalVehicleLoadWeight, value); }
    }

    #endregion

    #region decimal TotalManifestWeights

    public const string TotalManifestWeightsProperty = "TotalManifestWeights";
    decimal mTotalManifestWeights;
    public decimal TotalManifestWeights
    {
      get
      {
        CalculateManifestWeights();
        return mTotalManifestWeights;
      }
      private set { SetProperty<decimal>(ref mTotalManifestWeights, value); }
    }

    #endregion

    private void CalculateVehicleWeights()
    {
      using (new EventStateSuppressor(this))
      {
        decimal temp = 0;
        foreach (Vehicle v in Vehicles)
        {
          temp += v.MaxLoadWeight;
        }
        TotalVehicleLoadWeight = temp;
      }
    }

    private void CalculateManifestWeights()
    {
      using (new EventStateSuppressor(this))
      {
        decimal temp = 0;
        foreach (ManifestReference m in Manifests)
        {
          temp += m.Weight;
        }
        TotalManifestWeights = temp;
      }
    }

    protected override void OnCompositionChanged(CompositionChangedType compositionChangedType, string propertyName, object originalSource, BusinessObject item)
    {

      base.OnCompositionChanged(compositionChangedType, propertyName, originalSource, item);

      if (OperationContext.Current != null) return;

      if ((compositionChangedType == CompositionChangedType.ChildAdded || compositionChangedType == CompositionChangedType.ChildRemoved))
      {
        CalculateVehicleWeights();
        CalculateManifestWeights();
      }
    }

    protected override void GetErrors(Collection<string> errors, string propertyName)
    {
      if (propertyName == TotalManifestWeightsProperty || String.IsNullOrWhiteSpace(propertyName))
      {
        if (TotalVehicleLoadWeight > 0)
        {
          if (TotalManifestWeights > TotalVehicleLoadWeight)
          {
            errors.Add("DeliveryManifest weight exceeded Vehicle load weight limits.");
          }
        }
      }

      base.GetErrors(errors, propertyName);
    }
  }
}

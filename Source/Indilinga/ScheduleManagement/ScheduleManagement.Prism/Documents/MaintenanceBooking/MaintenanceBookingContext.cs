﻿using Afx.Business.Security;
using Afx.Business.Service;
using Afx.Prism.RibbonMdi.Documents;
using ScheduleManagement.Business;
using ScheduleManagement.Business.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScheduleManagement.Prism.Documents.MaintenanceBooking
{
  public partial class MaintenanceBookingContext : DocumentContext<ScheduleManagement.Business.MaintenanceBooking>
  {
    public MaintenanceBookingContext()
      : base(ScheduleManagement.Business.MaintenanceBooking.DocumentTypeIdentifier)
    {
    }

    public MaintenanceBookingContext(Guid documentIdentifier)
      : base(ScheduleManagement.Business.MaintenanceBooking.DocumentTypeIdentifier, documentIdentifier)
    {
    }

    public override void SaveData()
    {
      using (var svc = ProxyFactory.GetService<IScheduleManagementService>(SecurityContext.ServerName))
      {
        Document = svc.SaveMaintenanceBooking(Document);
      }
      base.SaveData();
    }

    public override void LoadData()
    {
      using (var svc = ProxyFactory.GetService<IScheduleManagementService>(SecurityContext.ServerName))
      {
        Document = svc.LoadMaintenanceBooking(DocumentIdentifier);
      }
      base.LoadData();
    }
  }
}


﻿using Afx.Prism;
using Afx.Prism.RibbonMdi.Documents;
using ScheduleManagement.Business;
using Microsoft.Practices.Unity;
using System.Collections.Generic;
using Afx.Business.Data;

namespace ScheduleManagement.Prism.Documents.MaintenanceBooking
{
  public class MaintenanceBookingHeaderViewModel : DocumentHeaderViewModel<ScheduleManagement.Business.MaintenanceBooking>
  {
    #region Constructors

    [InjectionConstructor]
    public MaintenanceBookingHeaderViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    public IEnumerable<MaintenanceType> MaintenanceTypes
    {
      get { return Cache.Instance.GetObjects<MaintenanceType>(true, mt => mt.Text, true); }
    }
  }

}


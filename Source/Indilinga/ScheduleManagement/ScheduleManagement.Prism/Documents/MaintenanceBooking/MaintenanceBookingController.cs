﻿using Afx.Business;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Documents;
using ScheduleManagement.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FleetManagement.Prism.Dialogs.EditVehiclesDialog;
using ScheduleManagement.Prism.Events;

namespace ScheduleManagement.Prism.Documents.MaintenanceBooking
{
  public partial class MaintenanceBookingController : MdiDocumentController<MaintenanceBookingContext, MaintenanceBookingViewModel>
  {
    [InjectionConstructor]
    public MaintenanceBookingController(IController controller)
      : base(controller)
    {
    }

    protected override void ExecuteOperation(Operation op, BusinessObject argument)
    {
      switch (op.Identifier)
      {
        case Operations.EditVehicles:
          EditVehiclesDialogController c = CreateChildController<EditVehiclesDialogController>();
          c.VehicleCollection = DataContext.Document;
          c.Run();
          break;
        case Operations.Test:
          SetDocumentState(Business.MaintenanceBooking.States.Testing);
          break;
        case Operations.Cancel:
          SetDocumentState(Business.MaintenanceBooking.States.Cancelled);
          break;
        case Operations.Workshop:
          SetDocumentState(Business.MaintenanceBooking.States.Workshop);
          break;
        case Operations.Release:
          SetDocumentState(Business.MaintenanceBooking.States.Released);
          break;
      }

      base.ExecuteOperation(op, argument);
    }

    protected override void ExtendDocument()
    {
      ExtendDocument<MaintenanceBookingHeaderViewModel>(HeaderRegion);

      base.ExtendDocument();
    }

    protected override void SaveDataContext()
    {
      base.SaveDataContext();

      ApplicationController.EventAggregator.GetEvent<UpdateVehicleScheduleEvent>().Publish(EventArgs.Empty);
    }
  }
}


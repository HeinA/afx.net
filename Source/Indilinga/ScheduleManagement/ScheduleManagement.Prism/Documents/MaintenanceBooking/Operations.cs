﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScheduleManagement.Prism.Documents.MaintenanceBooking
{
  public class Operations : Afx.Prism.RibbonMdi.StandardOperations
  {
      public const string Cancel = "{7b85fd4d-9369-4e89-b3dc-cc26f8fec47a}";
      public const string EditVehicles = "{168e9619-d884-4205-8c1c-d7d437cab59d}";
      public const string Release = "{d9f4f353-88f5-4307-93d5-d38ad840a7af}";
      public const string Test = "{ce81a7eb-543b-4cfe-8577-028329017e42}";
      public const string Workshop = "{de1500b2-0d83-4bb1-aff0-14dcd89a739e}";
  }
}
﻿using Afx.Prism;
using Afx.Prism.RibbonMdi.Documents;
using Microsoft.Practices.Unity;

namespace ScheduleManagement.Prism.Documents.MaintenanceBooking
{
  public partial class MaintenanceBookingViewModel : MdiDocumentViewModel<MaintenanceBookingContext>
  {
    [InjectionConstructor]
    public MaintenanceBookingViewModel(IController controller)
      : base(controller)
    {
    }

  }
}


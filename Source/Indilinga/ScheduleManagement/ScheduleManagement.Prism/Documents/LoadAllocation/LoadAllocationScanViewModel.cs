﻿using Afx.Business;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Documents;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace ScheduleManagement.Prism.Documents.LoadAllocation
{
  public class LoadAllocationScanViewModel : ExtensionViewModel<ScheduleManagement.Business.LoadAllocation>
  {
    #region Constructors

    [InjectionConstructor]
    public LoadAllocationScanViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    [Dependency]
    public LoadAllocationController LoadAllocationController { get; set; }

    #region string WaybillNumber

    public const string WaybillNumberProperty = "WaybillNumber";
    string mWaybillNumber;
    public string WaybillNumber
    {
      get { return mWaybillNumber; }
      set { SetProperty<string>(ref mWaybillNumber, value); }
    }

    #endregion

    #region DelegateCommand KeyDownCommand

    DelegateCommand<KeyEventArgs> mKeyDownCommand;
    public DelegateCommand<KeyEventArgs> KeyDownCommand
    {
      get { return mKeyDownCommand ?? (mKeyDownCommand = new DelegateCommand<KeyEventArgs>(ExecuteKeyDown)); }
    }

    void ExecuteKeyDown(KeyEventArgs e)
    {
      if (e.Key != Key.Enter) return;

      try
      {
        LoadAllocationController.AttachWaybill(WaybillNumber, true);
        WaybillNumber = string.Empty;
      }
      catch (Exception ex)
      {
        if (ExceptionHelper.HandleException(ex)) throw ex;
      }

      e.Handled = true;
    }

    #endregion
  }
}

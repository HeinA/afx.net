﻿using Afx.Prism;
using Afx.Prism.RibbonMdi;
using Afx.Prism.RibbonMdi.Documents;
using FreightManagement.Business;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScheduleManagement.Prism.Documents.LoadAllocation
{
  public class LoadAllocationWaybillsViewModel : ExtensionViewModel<ScheduleManagement.Business.LoadAllocation>, IDropTarget
  {
    #region Constructors

    [InjectionConstructor]
    public LoadAllocationWaybillsViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    [Dependency]
    public LoadAllocationController LoadAllocationController { get; set; }

    #region WaybillReference SelectedWaybill

    public const string SelectedWaybillProperty = "SelectedWaybill";
    WaybillReference mSelectedWaybill;
    public WaybillReference SelectedWaybill
    {
      get { return mSelectedWaybill; }
      set { SetProperty<WaybillReference>(ref mSelectedWaybill, value); }
    }

    #endregion

    #region DelegateCommand<WaybillReference> OpenWaybillCommand

    DelegateCommand<WaybillReference> mOpenWaybillCommand;
    public DelegateCommand<WaybillReference> OpenWaybillCommand
    {
      get { return mOpenWaybillCommand ?? (mOpenWaybillCommand = new DelegateCommand<WaybillReference>(ExecuteOpenWaybill, CanExecuteOpenWaybill)); }
    }

    bool CanExecuteOpenWaybill(WaybillReference args)
    {
      return true;
    }

    void ExecuteOpenWaybill(WaybillReference args)
    {
      MdiApplicationController.Current.EditDocument(FreightManagement.Business.Waybill.DocumentTypeIdentifier, args.GlobalIdentifier);
    }
    
    #endregion

    #region IEnumerable<WaybillItemViewModel> Waybills

    public IEnumerable<WaybillItemViewModel> Waybills
    {
      get { return ResolveViewModels<WaybillItemViewModel, WaybillReference>(Model.Waybills); }
    }

    #endregion

    protected override void OnModelPropertyChanged(string propertyName)
    {
      if (propertyName == Business.LoadAllocation.RouteProperty)
      {
        ValidateWaybills();
      }
      base.OnModelPropertyChanged(propertyName);
    }

    public void ValidateWaybills()
    {
      foreach (var vm in Waybills)
      {
        vm.ValidateRoute();
      }
    }

    public bool CanDrop(object data)
    {
      Collection<DraggedDocumentReference> docs = data as Collection<DraggedDocumentReference>;
      if (docs != null)
      {
        bool allow = true;
        foreach (var dr in docs)
        {
          if (dr.DocumentType.Identifier != Waybill.DocumentTypeIdentifier) allow = false;
        }
        return allow;
      }
      return false;
    }

    public void DoDrop(object data)
    {
      Collection<DraggedDocumentReference> docs = data as Collection<DraggedDocumentReference>;
      if (docs != null)
      {
        foreach (var dr in docs)
        {
          LoadAllocationController.AttachWaybill(dr.DocumentGuid, false);
        }
      }
    }
  }
}

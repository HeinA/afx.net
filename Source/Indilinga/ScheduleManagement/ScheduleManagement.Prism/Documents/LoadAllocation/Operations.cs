﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScheduleManagement.Prism.Documents.LoadAllocation
{
  public class Operations : Afx.Prism.RibbonMdi.StandardOperations
  {
      public const string AttachtoTripsheet = "{0315f4ef-f856-4225-978f-7f64bb7df377}";
      public const string Cancel = "{9539feca-9edc-47e5-b131-a7a0647bac6e}";
      public const string CreateTripsheet = "{254d59bd-ab43-4970-9918-393e99d76b29}";
      public const string EditVehicles = "{0778f0a3-d384-42fb-9f30-16e6dc69d200}";
      public const string NewWaybill = "{bda589aa-4aa8-4039-9fd7-8c2b9272cb74}";
      public const string OpenTripsheet = "{e763027c-4924-4cd6-8c66-d3ca8d91abe0}";
  }
}
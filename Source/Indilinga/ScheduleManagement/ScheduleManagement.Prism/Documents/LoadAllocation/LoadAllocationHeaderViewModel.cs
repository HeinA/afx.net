﻿using Afx.Business;
using Afx.Business.Activities;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Documents;
using ScheduleManagement.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using FreightManagement.Business;
using Afx.Business.Data;
using Afx.Business.Documents;

namespace ScheduleManagement.Prism.Documents.LoadAllocation
{
  public class LoadAllocationHeaderViewModel : DocumentHeaderViewModel<ScheduleManagement.Business.LoadAllocation>
  {
    #region Constructors

    [InjectionConstructor]
    public LoadAllocationHeaderViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    #region void OnModelPropertyChanged(...)

    protected override void OnModelPropertyChanged(string propertyName)
    {
      switch (propertyName)
      {
        case Document.OrganizationalUnitProperty:
          OnPropertyChanged("Routes");
          break;
      }

      base.OnModelPropertyChanged(propertyName);
    }

    #endregion

    #region IEnumerable<Route> Routes

    public IEnumerable<Route> Routes
    {
      get { return Cache.Instance.GetObjects<Route>(true, r => r.Owner.OrganizationalUnit.Equals(Model.OrganizationalUnit), r => r.Name, true); }
    }

    #endregion

    #region IEnumerable<SlotTime> SlotTimes

    public IEnumerable<SlotTime> SlotTimes
    {
      get { return Cache.Instance.GetObjects<SlotTime>(true, st => st.Text, true); }
    }

    #endregion
  }
}


﻿using Afx.Business.Security;
using Afx.Business.Service;
using Afx.Prism.RibbonMdi.Documents;
using ScheduleManagement.Business;
using ScheduleManagement.Business.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScheduleManagement.Prism.Documents.LoadAllocation
{
  public partial class LoadAllocationContext : DocumentContext<ScheduleManagement.Business.LoadAllocation>
  {
    public LoadAllocationContext()
      : base(ScheduleManagement.Business.LoadAllocation.DocumentTypeIdentifier)
    {
    }

    public LoadAllocationContext(Guid documentIdentifier)
      : base(ScheduleManagement.Business.LoadAllocation.DocumentTypeIdentifier, documentIdentifier)
    {
    }

    public override void SaveData()
    {
      using (var svc = ProxyFactory.GetService<IScheduleManagementService>(SecurityContext.ServerName))
      {
        Document = svc.SaveLoadAllocation(Document);
      }
      base.SaveData();
    }

    public override void LoadData()
    {
      using (var svc = ProxyFactory.GetService<IScheduleManagementService>(SecurityContext.ServerName))
      {
        Document = svc.LoadLoadAllocation(DocumentIdentifier);
      }
      base.LoadData();
    }
  }
}


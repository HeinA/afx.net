﻿using Afx.Business.Security;
using Afx.Business.Service;
using Afx.Prism.RibbonMdi.Documents;
using ScheduleManagement.Business;
using ScheduleManagement.Business.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.Composition;

namespace ScheduleManagement.Prism.Documents.LoadAllocation
{
  [Export("Context:" + ScheduleManagement.Business.LoadAllocation.DocumentTypeIdentifier)]
  public partial class LoadAllocationContext
  {
  }
}


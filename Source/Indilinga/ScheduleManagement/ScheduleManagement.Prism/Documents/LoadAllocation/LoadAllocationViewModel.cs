﻿using Afx.Business;
using Afx.Business.Activities;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Documents;
using ScheduleManagement.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace ScheduleManagement.Prism.Documents.LoadAllocation
{
  public partial class LoadAllocationViewModel : MdiDocumentViewModel<LoadAllocationContext>
  {
    [InjectionConstructor]
    public LoadAllocationViewModel(IController controller)
      : base(controller)
    {
    }
  }
}


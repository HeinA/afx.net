﻿using Afx.Prism;
using FreightManagement.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace ScheduleManagement.Prism.Documents.LoadAllocation
{
  public class WaybillItemViewModel : SelectableViewModel<WaybillReference>
  {
    #region Constructors

    [InjectionConstructor]
    public WaybillItemViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    new LoadAllocationWaybillsViewModel Parent
    {
      get { return (LoadAllocationWaybillsViewModel)base.Parent; }
    }

    #region bool HasValidRoute

    public const string HasValidRouteProperty = "HasValidRoute";
    public bool HasValidRoute
    {
      get
      {
        bool valid = false;
        foreach (var r in Model.Routes)
        {
          if (r.Route.Equals(Parent.Model.Route)) valid = true;
        }
        return valid;
      }
    }

    #endregion

    public bool HasError
    {
      get { return !HasValidRoute; }
    }

    public new string Error
    {
      get
      {
        if (HasError) return "Waybill does not contain a valid route.";
        return null;
      }
    }

    public void ValidateRoute()
    {
      OnPropertyChanged("HasError");
      OnPropertyChanged("Error");
    }
  }
}

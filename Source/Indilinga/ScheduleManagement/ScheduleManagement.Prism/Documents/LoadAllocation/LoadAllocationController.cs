﻿using Afx.Business;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Documents;
using ScheduleManagement.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FreightManagement.Business;
using Afx.Prism.RibbonMdi;
using FreightManagement.Prism.Documents.Waybill;
using Afx.Business.Service;
using FreightManagement.Business.Service;
using Afx.Business.Security;
using FleetManagement.Prism.Dialogs.EditVehiclesDialog;

namespace ScheduleManagement.Prism.Documents.LoadAllocation
{
  public partial class LoadAllocationController : MdiDocumentController<LoadAllocationContext, LoadAllocationViewModel>
  {
    [InjectionConstructor]
    public LoadAllocationController(IController controller)
      : base(controller)
    {
    }

    protected override void ExtendDocument()
    {
      ExtendDocument<LoadAllocationHeaderViewModel>(HeaderRegion);
      ExtendDocument<LoadAllocationScanViewModel>(HeaderRegion);

      ExtendDocument<LoadAllocationWaybillsViewModel>(DockRegion);

      ExtendDocument<LoadAllocationNotesViewModel>(TabRegion);

      base.ExtendDocument();
    }

    protected override void ExecuteOperation(Operation op, BusinessObject argument)
    {
      switch (op.Identifier)
      {
        case Operations.NewWaybill:
          WaybillController wc = (WaybillController)MdiApplicationController.Current.EditDocument(Waybill.DocumentTypeIdentifier);
          Waybill wb = wc.DataContext.Document;
          wb.CollectionDate = DataContext.Document.LoadingDate;
          wb.DeliveryDate = DataContext.Document.OffloadingDate;
          wb.IsPointToPoint = true;
          wc.Saved += Waybill_Saved;
          wc.Disposing += Waybill_Disposing;
          break;

        case Operations.EditVehicles:
          EditVehiclesDialogController c = CreateChildController<EditVehiclesDialogController>();
          c.VehicleCollection = DataContext.Document;
          c.Run();
          break;

        case Operations.CreateTripsheet:
          SetDocumentState(Business.LoadAllocation.States.Complete);
          break;

        case Operations.OpenTripsheet:
          MdiApplicationController.Current.EditDocument(ScheduleManagement.Business.Tripsheet.DocumentTypeIdentifier, DataContext.Document.AssociatedTripsheet);
          break;

        case Operations.Cancel:
          SetDocumentState(Business.LoadAllocation.States.Cancelled);
          break;
      }
      base.ExecuteOperation(op, argument);
    }

    void Waybill_Disposing(object sender, EventArgs e)
    {
      WaybillController wc = (WaybillController)sender;
      wc.Saved -= Waybill_Saved;
      wc.Disposing -= Waybill_Disposing;
    }

    void Waybill_Saved(object sender, EventArgs e)
    {
      WaybillController wc = (WaybillController)sender;
      AttachWaybill(wc.DataContext.Document.DocumentNumber, false);
    }

    public void AttachWaybill(Guid documentGuid, bool delete)
    {
      using (var svc = ProxyFactory.GetService<IFreightManagementService>(SecurityContext.ServerName))
      {
        AttachWaybill(svc.LoadWaybillReference(documentGuid), delete);
      }
    }

    public void AttachWaybill(string documentNumber, bool delete)
    {
      using (var svc = ProxyFactory.GetService<IFreightManagementService>(SecurityContext.ServerName))
      {
        AttachWaybill(svc.LoadWaybillReferenceByDocumentNumber(documentNumber), delete);
      }
    }

    public void AttachWaybill(WaybillReference waybill, bool delete)
    {
      if (waybill == null)
      {
        System.Media.SystemSounds.Exclamation.Play();
        return;
      }

      if (delete && DataContext.Document.Waybills.Contains(waybill))
      {
        DataContext.Document.Waybills.Remove(waybill);
        return;
      }

      if (!DataContext.Document.Waybills.Contains(waybill)) DataContext.Document.Waybills.Add(waybill);
    }
  }
}


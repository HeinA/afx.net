﻿using Afx.Business.Documents;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Documents;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScheduleManagement.Prism.Documents.LoadAllocation
{
  public class LoadAllocationNotesViewModel : ExtensionViewModel<ScheduleManagement.Business.LoadAllocation>
  {
    #region Constructors

    [InjectionConstructor]
    public LoadAllocationNotesViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    #region string Title

    public const string TitleProperty = "Title";
    public string Title
    {
      get { return "_Notes"; }
    }

    #endregion


    #region void OnModelPropertyChanged(...)

    protected override void OnModelPropertyChanged(string propertyName)
    {
      switch (propertyName)
      {
        case Document.NotesProperty:
          OnPropertyChanged("Notes");
          break;
      }

      base.OnModelPropertyChanged(propertyName);
    }

    #endregion
  }
}

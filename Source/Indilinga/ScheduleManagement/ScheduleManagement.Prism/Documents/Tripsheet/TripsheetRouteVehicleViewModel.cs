﻿using Afx.Prism;
using Afx.Prism.RibbonMdi.Documents;
using FleetManagement.Business;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace FreightManagement.Prism.Documents.Tripsheet
{
  public class TripsheetRouteVehicleViewModel : ExtensionViewModel<Vehicle>
  {
    #region Constructors

    [InjectionConstructor]
    public TripsheetRouteVehicleViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    [Dependency]
    public TripsheetController TripsheetRouteController { get; set; }

    #region string TitleText

    public string TitleText
    {
      get { return "Route Vehicles"; }
    }

    #endregion


    #region DelegateCommand ContextMenuOpeningCommand

    public DelegateCommand<ContextMenuEventArgs> mContextMenuOpeningCommand;
    public DelegateCommand<ContextMenuEventArgs> ContextMenuOpeningCommand
    {
      get { return mContextMenuOpeningCommand ?? (mContextMenuOpeningCommand = new DelegateCommand<ContextMenuEventArgs>(OnContextMenuOpening)); }
    }

    void OnContextMenuOpening(ContextMenuEventArgs e)
    {
      e.Handled = true;
    }

    #endregion
  }
}

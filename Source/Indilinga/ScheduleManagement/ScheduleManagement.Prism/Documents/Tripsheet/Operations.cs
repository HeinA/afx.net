﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScheduleManagement.Prism.Documents.Tripsheet
{
  public class Operations : Afx.Prism.RibbonMdi.StandardOperations
  {
      public const string EditVehicles = "{13e62781-4b07-4a57-815b-f9d10edcfdc9}";
      public const string Finalize = "{27516a99-7ac3-4bfc-80b7-dbb80d24d7be}";
      public const string Reopen = "{b7f1034c-e7e5-473a-8cce-2270f847965d}";
  }
}
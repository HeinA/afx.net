﻿using Afx.Prism;
using FleetManagement.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScheduleManagement.Prism.Documents.Tripsheet
{
  public class VehicleItemViewModel : SelectableViewModel<Vehicle>
  {
    [InjectionConstructor]
    public VehicleItemViewModel(IController controller)
      : base(controller)
    {
    }

    public new TripsheetVehiclesViewModel Parent
    {
      get { return (TripsheetVehiclesViewModel)base.Parent; }
    }

    public override bool IsSelected
    {
      get
      {
        if (base.IsSelected) Parent.SelectedVehicle = this;
        return base.IsSelected;
      }
      set
      {
        base.IsSelected = value;
        if (base.IsSelected) Parent.SelectedVehicle = this;
      }
    }
  }
}

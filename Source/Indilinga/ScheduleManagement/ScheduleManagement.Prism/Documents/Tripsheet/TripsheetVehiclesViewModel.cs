﻿using Afx.Prism;
using Afx.Prism.RibbonMdi.Documents;
using ScheduleManagement.Business;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using FreightManagement.Business;
using FleetManagement.Business;

namespace ScheduleManagement.Prism.Documents.Tripsheet
{
  public class TripsheetVehiclesViewModel : ExtensionViewModel<ScheduleManagement.Business.Tripsheet>
  {
    #region Constructors

    [InjectionConstructor]
    public TripsheetVehiclesViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    [Dependency]
    public TripsheetController TripsheetController { get; set; }

    #region string TitleText

    public string TitleText
    {
      get { return "Vehicles"; }
    }

    #endregion

    #region DelegateCommand ContextMenuOpeningCommand

    public DelegateCommand<ContextMenuEventArgs> mContextMenuOpeningCommand;
    public DelegateCommand<ContextMenuEventArgs> ContextMenuOpeningCommand
    {
      get { return mContextMenuOpeningCommand ?? (mContextMenuOpeningCommand = new DelegateCommand<ContextMenuEventArgs>(OnContextMenuOpening)); }
    }

    void OnContextMenuOpening(ContextMenuEventArgs e)
    {
      e.Handled = true;
    }

    #endregion

    public IEnumerable<VehicleItemViewModel> Vehicles
    {
      get { return ResolveViewModels<VehicleItemViewModel, Vehicle>(Model.Vehicles); }
    }

    public void Refresh()
    {
      OnPropertyChanged(null);
    }
  }
}

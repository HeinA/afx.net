﻿using Afx.Business;
using Afx.Business.Data;
using Afx.Business.Documents;
using Afx.Business.Security;
using Afx.Business.Service;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Tools;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScheduleManagement.Prism.Documents.Tripsheet
{
  [Export(typeof(SearchItemViewModel))]
  public partial class TripsheetSearchViewModel : SearchItemViewModel
  {
    #region Constructors

    [InjectionConstructor]
    public TripsheetSearchViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    #region DocumentType DocumentType

    public override DocumentType DocumentType
    {
      get { return Cache.Instance.GetObject<DocumentType>(ScheduleManagement.Business.Tripsheet.DocumentTypeIdentifier); }
    }

    #endregion

    #region string DocumentNumber

    string mDocumentNumber;
    public string DocumentNumber
    {
      get { return mDocumentNumber; }
      set { SetProperty<string>(ref mDocumentNumber, value); }
    }

    #endregion

    #region DocumentTypeState DocumentTypeState

    public IEnumerable<DocumentTypeState> DocumentTypeStates
    {
      get { return new List<DocumentTypeState>(new DocumentTypeState[] { new DocumentTypeState(true) }).Union(Cache.Instance.GetObject<DocumentType>(ScheduleManagement.Business.Tripsheet.DocumentTypeIdentifier).HeirarchyStates.OrderBy(dts => dts.Name)); }
    }

    DocumentTypeState mDocumentTypeState = new DocumentTypeState(true);
    public DocumentTypeState DocumentTypeState
    {
      get { return mDocumentTypeState; }
      set { SetProperty<DocumentTypeState>(ref mDocumentTypeState, value); }
    }

    #endregion

    #region bool UseSingleDate

    bool mUseSingleDate = true;
    public bool UseSingleDate
    {
      get { return mUseSingleDate; }
      set { SetProperty<bool>(ref mUseSingleDate, value); }
    }

    #endregion

    #region bool UseDateRange

    bool mUseDateRange = false;
    public bool UseDateRange
    {
      get { return mUseDateRange; }
      set { SetProperty<bool>(ref mUseDateRange, value); }
    }

    #endregion

    #region Nullable<DateTime> Date1

    Nullable<DateTime> mDate1 = null;
    public Nullable<DateTime> Date1
    {
      get { return mDate1; }
      set { SetProperty<Nullable<DateTime>>(ref mDate1, value); }
    }

    #endregion

    #region Nullable<DateTime> Date2

    Nullable<DateTime> mDate2;
    public Nullable<DateTime> Date2
    {
      get { return mDate2; }
      set { SetProperty<Nullable<DateTime>>(ref mDate2, value); }
    }

    #endregion

    #region OrganizationalUnit OrganizationalUnit

    public IEnumerable<OrganizationalUnit> OrganizationalUnits
    {
      get { return Cache.Instance.GetObjects<OrganizationalUnit>(true, ou => ou.UserAssignable, ou => ou.Name, true); }
    }

    OrganizationalUnit mOrganizationalUnit = new OrganizationalUnit(true);
    public OrganizationalUnit OrganizationalUnit
    {
      get { return mOrganizationalUnit; }
      set { SetProperty<OrganizationalUnit>(ref mOrganizationalUnit, value); }
    }

    #endregion

    #region string FleetNumber

    public const string FleetNumberProperty = "FleetNumber";
    string mFleetNumber;
    public string FleetNumber
    {
      get { return mFleetNumber; }
      set { SetProperty<string>(ref mFleetNumber, value); }
    }

    #endregion

    #region string RegistrationNumber

    public const string RegistrationNumberProperty = "RegistrationNumber";
    string mRegistrationNumber;
    public string RegistrationNumber
    {
      get { return mRegistrationNumber; }
      set { SetProperty<string>(ref mRegistrationNumber, value); }
    }

    #endregion

    #region string DriverFirstName

    public const string DriverFirstNameProperty = "DriverFirstName";
    string mDriverFirstName;
    public string DriverFirstName
    {
      get { return mDriverFirstName; }
      set { SetProperty<string>(ref mDriverFirstName, value); }
    }

    #endregion

    #region string DriverLastName

    public const string DriverLastNameProperty = "DriverLastName";
    string mDriverLastName;
    public string DriverLastName
    {
      get { return mDriverLastName; }
      set { SetProperty<string>(ref mDriverLastName, value); }
    }

    #endregion

    #region string ManifestNumber

    public const string ManifestNumberProperty = "ManifestNumber";
    string mManifestNumber;
    public string ManifestNumber
    {
      get { return mManifestNumber; }
      set { SetProperty<string>(ref mManifestNumber, value); }
    }

    #endregion

    #region SearchResults ExecuteSearch()

    protected override SearchResults ExecuteSearch()
    {
      using (var svc = ServiceFactory.GetService<IAfxService>(SecurityContext.ServerName))
      {
        SearchDatagraph sg = new SearchDatagraph(typeof(ScheduleManagement.Business.Tripsheet))
          .Select(Document.DocumentDateProperty, "Date", "dd MMM yyyy")
          .Select(Document.DocumentNumberProperty, "Document")
          .Join(Document.StateProperty)
            .Join(DocumentState.DocumentTypeStateProperty)
              .Select(DocumentTypeState.NameProperty, "State")
              .Where(DocumentTypeState.NameProperty, FilterType.Equals, DocumentTypeState.Name, !string.IsNullOrWhiteSpace(DocumentTypeState.Name))
            .EndJoin()
          .EndJoin()
          .Join(Business.Tripsheet.VehiclesProperty)
            .Select(FleetManagement.Business.Vehicle.FleetNumberProperty, "Fleet No")
            .Select(FleetManagement.Business.Vehicle.RegistrationNumberProperty, "Registration No")
            .Where(FleetManagement.Business.Vehicle.FleetNumberProperty, FilterType.Like, string.Format("%{0}%", FleetNumber), !string.IsNullOrWhiteSpace(FleetNumber))
            .Where(FleetManagement.Business.Vehicle.RegistrationNumberProperty, FilterType.Like, string.Format("%{0}%", RegistrationNumber), !string.IsNullOrWhiteSpace(RegistrationNumber))
          .EndJoin()
          .Join(Business.Tripsheet.DriverProperty)
            .Select(ContactManagement.Business.Contact.FirstnameProperty, "Driver Name")
            .Select(ContactManagement.Business.Contact.LastnameProperty, "Driver Lastname")
            .Where(ContactManagement.Business.Contact.FirstnameProperty, FilterType.Like, string.Format("%{0}%", DriverFirstName), !string.IsNullOrWhiteSpace(DriverFirstName))
            .Where(ContactManagement.Business.Contact.LastnameProperty, FilterType.Like, string.Format("%{0}%", DriverLastName), !string.IsNullOrWhiteSpace(DriverLastName))
          .EndJoin()
          .Join(Business.Tripsheet.ManifestsProperty)
            .Where(FreightManagement.Business.ManifestReference.ManifestNumberProperty, FilterType.Like, string.Format("%{0}%", ManifestNumber), !string.IsNullOrWhiteSpace(ManifestNumber))
          .EndJoin()
          .Join(Document.OrganizationalUnitProperty)
            .Select(OrganizationalUnit.NameProperty, "Organizational Unit")
            .Where(OrganizationalUnit.NameProperty, FilterType.Equals, OrganizationalUnit.Name, !string.IsNullOrWhiteSpace(OrganizationalUnit.Name))
          .EndJoin()
          .Where(Document.DocumentNumberProperty, FilterType.Like, string.Format("%{0}%", DocumentNumber), !string.IsNullOrWhiteSpace(DocumentNumber))
          .WhereBetween(Document.DocumentDateProperty, Date1, Date2, Date1 != null && UseDateRange, Date2 != null && UseDateRange)
          .Where(Document.DocumentDateProperty, FilterType.Equals, Date1, Date1 != null && UseSingleDate);

        return svc.Instance.Search(sg);
      }
    }

    #endregion
  }
}

﻿using Afx.Business;
using Afx.Business.Activities;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Documents;
using ScheduleManagement.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using Microsoft.Practices.Prism.Commands;
using FleetManagement.Prism.Dialogs.EditVehiclesDialog;
using FreightManagement.Business;
using Afx.Business.Data;

namespace ScheduleManagement.Prism.Documents.Tripsheet
{
  public class TripsheetHeaderViewModel : DocumentHeaderViewModel<ScheduleManagement.Business.Tripsheet>
  {
    #region Constructors

    [InjectionConstructor]
    public TripsheetHeaderViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    public IEnumerable<FeeType> FeeTypes
    {
      get { return Cache.Instance.GetObjects<FeeType>(true, x => x.Text, true); }
    }
  }
}


﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScheduleManagement.Prism.Documents.Tripsheet
{
  [Export(typeof(Afx.Prism.ResourceInfo))]
  public class ResourceInfo : Afx.Prism.ResourceInfo
  {
    public override Uri GetUri()
    {
      return new Uri("pack://application:,,,/ScheduleManagement.Prism;component/Documents/Tripsheet/Resources.xaml", UriKind.Absolute);
    }

    public override int Priority
    {
      get { return 100; }
    }
  }
}
﻿using Afx.Prism;
using ScheduleManagement.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FreightManagement.Business;

namespace ScheduleManagement.Prism.Documents.Tripsheet
{
  public class ManifestItemViewModel : SelectableViewModel<ManifestReference>
  {
    [InjectionConstructor]
    public ManifestItemViewModel(IController controller)
      : base(controller)
    {
    }

    public new TripsheetManifestsViewModel Parent
    {
      get { return (TripsheetManifestsViewModel)base.Parent; }
    }

    public override bool IsSelected
    {
      get
      {
        if (base.IsSelected) Parent.SelectedManifest = this.Model;
        return base.IsSelected;
      }
      set
      {
        base.IsSelected = value;
        if (base.IsSelected) Parent.SelectedManifest = this.Model;
      }
    }
  }
}

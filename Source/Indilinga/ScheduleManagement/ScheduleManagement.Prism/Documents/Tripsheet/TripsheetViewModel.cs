﻿using Afx.Business;
using Afx.Business.Activities;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Documents;
using ScheduleManagement.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using Afx.Prism.RibbonMdi;
using Afx.Business.Service;
using FreightManagement.Business.Service;
using Afx.Business.Security;

namespace ScheduleManagement.Prism.Documents.Tripsheet
{
  public partial class TripsheetViewModel : MdiDocumentViewModel<TripsheetContext>, IDropTarget
  {
    [InjectionConstructor]
    public TripsheetViewModel(IController controller)
      : base(controller)
    {
    }

    [Dependency]
    public TripsheetController TripsheetController { get; set; }

    public bool CanDrop(object data)
    {
      Collection<DraggedDocumentReference> refs = data as Collection<DraggedDocumentReference>;
      if (refs == null) return false;

      foreach (var r in refs)
      {
        if (r.DocumentType.Identifier == FreightManagement.Business.DeliveryManifest.DocumentTypeIdentifier) return true;
        if (r.DocumentType.Identifier == FreightManagement.Business.TransferManifest.DocumentTypeIdentifier) return true;
        if (r.DocumentType.Identifier == FreightManagement.Business.TransportManifest.DocumentTypeIdentifier) return true;
      }

      return false;
    }

    public void DoDrop(object data)
    {
      try
      {
        Collection<DraggedDocumentReference> refs = data as Collection<DraggedDocumentReference>;
        if (refs == null) return;

        using (var svc = ServiceFactory.GetService<IFreightManagementService>(SecurityContext.ServerName))
        {
          foreach (var r in refs)
          {
            if (r.DocumentType.Identifier == FreightManagement.Business.DeliveryManifest.DocumentTypeIdentifier || r.DocumentType.Identifier == FreightManagement.Business.TransferManifest.DocumentTypeIdentifier || r.DocumentType.Identifier == FreightManagement.Business.TransportManifest.DocumentTypeIdentifier)
            {
              TripsheetController.AttachManifest(svc.Instance.LoadManifestReference(r.DocumentGuid));
            }
          }
        }
      }
      catch (Exception ex)
      {
        if (ExceptionHelper.HandleException(ex)) throw ex;
      }
    }
  }
}


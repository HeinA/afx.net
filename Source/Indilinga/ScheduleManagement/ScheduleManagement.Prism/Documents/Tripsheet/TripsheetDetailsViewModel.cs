﻿using Afx.Business.Data;
using Afx.Prism;
using Afx.Prism.Controls;
using Afx.Prism.RibbonMdi;
using FleetManagement.Business;
using FleetManagement.Prism.Dialogs.EditVehiclesDialog;
using ScheduleManagement.Business;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FreightManagement.Business;

namespace ScheduleManagement.Prism.Documents.Tripsheet
{
  public class TripsheetDetailsViewModel : ViewModel<ScheduleManagement.Business.Tripsheet>
  {
    #region Constructors

    [InjectionConstructor]
    public TripsheetDetailsViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    public override bool IsReadOnly
    {
      get
      {
        return Model.IsReadOnly;
      }
    }

    #region DelegateCommand<ItemDeletingEventArgs> ItemDeletingCommand

    DelegateCommand<ItemDeletingEventArgs> mItemDeletingCommand;
    public DelegateCommand<ItemDeletingEventArgs> ItemDeletingCommand
    {
      get { return mItemDeletingCommand ?? (mItemDeletingCommand = new DelegateCommand<ItemDeletingEventArgs>(ExecuteItemDeleting, CanExecuteItemDeleting)); }
    }

    bool CanExecuteItemDeleting(ItemDeletingEventArgs args)
    {
      return true;
    }

    void ExecuteItemDeleting(ItemDeletingEventArgs args)
    {
      ScheduleManagement.Business.Tripsheet mr = args.Item as ScheduleManagement.Business.Tripsheet;
      if (mr == null) return;
    }

    #endregion

  }
}

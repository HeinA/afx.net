﻿using Afx.Business.Security;
using Afx.Business.Service;
using Afx.Prism.RibbonMdi.Documents;
using ScheduleManagement.Business;
using ScheduleManagement.Business.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScheduleManagement.Prism.Documents.Tripsheet
{
  public partial class TripsheetContext : DocumentContext<ScheduleManagement.Business.Tripsheet>
  {
    public TripsheetContext()
      : base(ScheduleManagement.Business.Tripsheet.DocumentTypeIdentifier)
    {
    }

    public TripsheetContext(Guid documentIdentifier)
      : base(ScheduleManagement.Business.Tripsheet.DocumentTypeIdentifier, documentIdentifier)
    {
    }

    public override bool IsDirty
    {
      get
      {
        return base.IsDirty;
      }
      set
      {
        base.IsDirty = value;
      }
    }

    public override void SaveData()
    {
      using (var svc = ProxyFactory.GetService<IScheduleManagementService>(SecurityContext.ServerName))
      {
        Document = svc.SaveTripsheet(Document);
      }
      base.SaveData();
    }

    public override void LoadData()
    {
      using (var svc = ProxyFactory.GetService<IScheduleManagementService>(SecurityContext.ServerName))
      {
        Document = svc.LoadTripsheet(DocumentIdentifier);
      }
      base.LoadData();
    }
  }
}


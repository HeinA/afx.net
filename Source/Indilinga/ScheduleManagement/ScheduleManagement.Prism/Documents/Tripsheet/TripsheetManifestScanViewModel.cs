﻿using Afx.Business;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Documents;
using Microsoft.Practices.Unity;
using Syncfusion.Windows.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace ScheduleManagement.Prism.Documents.Tripsheet
{
  public class TripsheetManifestScanViewModel : ExtensionViewModel<ScheduleManagement.Business.Tripsheet>
  {
    #region Constructors

    [InjectionConstructor]
    public TripsheetManifestScanViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    [Dependency]
    public TripsheetController TripsheetController { get; set; }

    #region string ManifestNumber

    public const string ManifestNumberProperty = "ManifestNumber";
    string mManifestNumber;
    public string ManifestNumber
    {
      get { return mManifestNumber; }
      set { SetProperty<string>(ref mManifestNumber, value); }
    }

    #endregion

    #region string ErrorMessage

    public const string ErrorMessageProperty = "ErrorMessage";
    string mErrorMessage;
    public string ErrorMessage
    {
      get { return mErrorMessage; }
      set
      {
        if (SetProperty<string>(ref mErrorMessage, value))
        {
          OnPropertyChanged(VisibilityProperty);
        }
      }
    }

    #endregion

    #region Visibility Visibility

    public const string VisibilityProperty = "Visibility";
    public Visibility Visibility
    {
      get { return string.IsNullOrWhiteSpace(ErrorMessage) ? Visibility.Collapsed : Visibility.Visible; }
    }

    #endregion

    #region DelegateCommand KeyDownCommand

    DelegateCommand<KeyEventArgs> mKeyDownCommand;
    public DelegateCommand<KeyEventArgs> KeyDownCommand
    {
      get { return mKeyDownCommand ?? (mKeyDownCommand = new DelegateCommand<KeyEventArgs>(ExecuteKeyDown)); }
    }

    void ExecuteKeyDown(KeyEventArgs e)
    {
      if (e.Key != Key.Enter) return;

      try
      {
        TripsheetController.AttachManifest(ManifestNumber);
        ManifestNumber = string.Empty;
      }
      catch (Exception ex)
      {
        if (ExceptionHelper.HandleException(ex)) throw ex;
      }

      e.Handled = true;
    }

    #endregion
  }
}

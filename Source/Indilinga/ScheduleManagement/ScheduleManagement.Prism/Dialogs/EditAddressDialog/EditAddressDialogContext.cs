﻿using Afx.Business;
using Afx.Business.Validation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScheduleManagement.Prism.Dialogs.EditAddressDialog
{
  public class EditAddressDialogContext : BusinessObject
  {
    #region string Name

    public const string NameProperty = "Name";
    string mName;
    [Mandatory("Name is mandatory.")]
    public string Name
    {
      get { return mName; }
      set { SetProperty<string>(ref mName, value); }
    }

    #endregion

    #region string AddressText

    public const string AddressTextProperty = "AddressText";
    string mAddressText;
    [Mandatory("Address is mandatory.")]
    [MinimumStringLength("Address length must be a minimum of 3 characters.")]
    public string AddressText
    {
      get { return mAddressText; }
      set { SetProperty<string>(ref mAddressText, value); }
    }

    #endregion
  }
}
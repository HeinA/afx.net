﻿using AccountManagement.Business.Service;
using Afx.Business;
using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Business.Service;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using ContactManagement.Business;
using Microsoft.Practices.Unity;
using ScheduleManagement.Prism.Dialogs.ScheduleWizardDialog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace ScheduleManagement.Prism.Dialogs.EditAddressDialog
{
  public class EditAddressDialogController : MdiDialogController<EditAddressDialogContext, EditAddressDialogViewModel>
  {
    public const string EditAddressDialogControllerKey = "ScheduleManagement.Prism.Dialogs.EditAddressDialog.EditAddressDialogController";

    public EditAddressDialogController(IController controller)
      : base(EditAddressDialogControllerKey, controller)
    {
    }

    #region AddressType AddressType

    public const string AddressTypeProperty = "AddressType";
    AddressType mAddressType;
    public AddressType AddressType
    {
      get { return mAddressType; }
      set { mAddressType = value; }
    }

    #endregion

    #region ScheduleWizardDialogViewModel ScheduleWizardDialogViewModel

    public const string ScheduleWizardDialogViewModelProperty = "ScheduleWizardDialogViewModel";
    ScheduleWizardDialogViewModel mScheduleWizardDialogViewModel;
    public ScheduleWizardDialogViewModel ScheduleWizardDialogViewModel
    {
      get { return mScheduleWizardDialogViewModel; }
      set { mScheduleWizardDialogViewModel = value; }
    }

    #endregion

    protected override bool OnRun()
    {
      if (DataContext == null) DataContext = new EditAddressDialogContext();
      if (AddressType == EditAddressDialog.AddressType.ShipFrom)
      {
        ViewModel.Caption = "Ship From Address";
      }
      if (AddressType == EditAddressDialog.AddressType.ShipTo)
      {
        ViewModel.Caption = "Ship To Address";
      }

      DataContext.AddressText = Address.AddressText;
      DataContext.Name = Address.Name;

      return base.OnRun();
    }

    public override bool Validate()
    {
      return base.Validate();
    }

    #region Address Address

    public const string AddressProperty = "Address";
    Address mAddress;
    public Address Address
    {
      get { return mAddress ?? (mAddress = new Address()); }
      set { mAddress = value; }
    }

    #endregion

    protected override void ApplyChanges()
    {
      try
      {
        Address.AddressText = DataContext.AddressText;
        Address.Name = DataContext.Name;

        using (var svc = ProxyFactory.GetService<IAccountManagementService>(SecurityContext.ServerName))
        {
          svc.EditAccountAddresses(ScheduleWizardDialogViewModel.Model.QuickTransportManifest.Account.GlobalIdentifier, Address);
        }

        if (AddressType == EditAddressDialog.AddressType.ShipFrom)
        {
          Address.AddressType = Cache.Instance.GetObjects<ContactManagement.Business.AddressType>(at => at.IsFlagged(FreightManagement.Business.AddressTypeFlags.ShipFromAddress)).FirstOrDefault();

          ScheduleWizardDialogViewModel.ShipFromAddresses = ScheduleWizardDialogViewModel.ShipFromAddresses.Union(new Address[] { Address }).OrderBy(a1 => a1.AddressTextSingleLine);
          ScheduleWizardDialogViewModel.SelectedShipFromAddress = Address;
        }

        if (AddressType == EditAddressDialog.AddressType.ShipTo)
        {
          Address.AddressType = Cache.Instance.GetObjects<ContactManagement.Business.AddressType>(at => at.IsFlagged(FreightManagement.Business.AddressTypeFlags.ShipToAddress)).FirstOrDefault();

          ScheduleWizardDialogViewModel.ShipToAddresses = ScheduleWizardDialogViewModel.ShipToAddresses.Union(new Address[] { Address }).OrderBy(a1 => a1.AddressTextSingleLine);
          ScheduleWizardDialogViewModel.SelectedShipToAddress = Address;
        }
      }
      catch (Exception ex)
      {
        if (ExceptionHelper.HandleException(ex)) throw ex;
      }

      base.ApplyChanges();
    }
  }
}

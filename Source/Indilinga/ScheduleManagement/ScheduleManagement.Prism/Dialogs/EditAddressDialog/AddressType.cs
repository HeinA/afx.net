﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScheduleManagement.Prism.Dialogs.EditAddressDialog
{
  public enum AddressType
  {
    ShipFrom
    , ShipTo
  }
}

﻿using Afx.Business;
using Afx.Business.Security;
using Afx.Business.Service;
using Afx.Prism.RibbonMdi.Documents;
using FreightManagement.Business;
using ScheduleManagement.Business.Service;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScheduleManagement.Prism.Dialogs.ScheduleWizardDialog
{
  public class ScheduleWizardDialogContext : BusinessObject
  {
    #region ScheduleManagement.Business.QuickTransportManifest QuickTransportManifest

    public const string QuickTransportManifestProperty = "QuickTransportManifest";
    ScheduleManagement.Business.QuickTransportManifest mQuickTransportManifest;
    public ScheduleManagement.Business.QuickTransportManifest QuickTransportManifest
    {
      get { return mQuickTransportManifest; }
      set
      {
        if (mQuickTransportManifest != null) mQuickTransportManifest.CompositionChanged -= mQuickTransportManifest_CompositionChanged;
        if (SetProperty<ScheduleManagement.Business.QuickTransportManifest>(ref mQuickTransportManifest, value))
        {
          if (mQuickTransportManifest != null) mQuickTransportManifest.CompositionChanged += mQuickTransportManifest_CompositionChanged;
        }
      }
    }

    void mQuickTransportManifest_CompositionChanged(object sender, CompositionChangedEventArgs e)
    {
      Revalidate = true;
      QuickTransportManifest.Validate();
    }

    #endregion

    #region RatesConfigurationReference RatesConfiguration

    public const string RatesConfigurationProperty = "RatesConfiguration";
    public RatesConfigurationReference RatesConfiguration
    {
      get { return GetCachedObject<RatesConfigurationReference>(); }
      set
      {
        if (SetCachedObject<RatesConfigurationReference>(value))
        {
          if (value != null)
          {
            var rcr = RatesConfiguration.GetRevision((DateTime)QuickTransportManifest.ScheduledLoadingDate);
            if (rcr == null) throw new InvalidOperationException("The Selected Rates Configuration does not have a revision for this date.");
            //VolumetricFactor = rcr.VolumetricFactor;
          }
        }
      }
    }

    #endregion


    protected override void GetErrors(Collection<string> errors, string propertyName)
    {
     // errors.Clear();
      try
      {
        QuickTransportManifest.Validate();
        IDataErrorInfo ei = QuickTransportManifest as IDataErrorInfo;
        if (!string.IsNullOrWhiteSpace(ei[propertyName]))
        {
          if (QuickTransportManifest.Error == null) return;
          foreach (string s in QuickTransportManifest.Error.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries))
          {
            errors.Add(s);
          }
        }
      }
      catch (Exception ex)
      {
        throw ex;
      }

      base.GetErrors(errors, propertyName);
    }

    //protected override void OnCompositionChanged(CompositionChangedType compositionChangedType, string propertyName, object originalSource, BusinessObject item)
    //{
    //  base.OnCompositionChanged(compositionChangedType, propertyName, originalSource, item);
    //  Revalidate = true;
    //  QuickTransportManifest.Validate();
    //}
  }
}
﻿using AccountManagement.Business;
using AccountManagement.Business.Service;
using Afx.Business;
using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Business.Service;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using ContactManagement.Business;
using FleetManagement.Business;
using FreightManagement.Business;
using FreightManagement.Business.Service;
using ScheduleManagement.Prism.Dialogs.EditAddressDialog;
using Geographics.Business;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using ScheduleManagement.Business;
using Afx.Business.Validation;
using System.Collections.ObjectModel;

namespace ScheduleManagement.Prism.Dialogs.ScheduleWizardDialog
{
  public class ScheduleWizardDialogViewModel : MdiDialogViewModel<ScheduleWizardDialogContext>
  {
    #region Constructors

    [InjectionConstructor]
    public ScheduleWizardDialogViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    #region Dependency Parent Controller

    [Dependency]
    public ScheduleWizardDialogController ScheduleWizardDialogController { get; set; }

    #endregion


    #region IEnumerable<AccountReference> Accounts

    public const string AccountsProperty = "Accounts";
    public IEnumerable<AccountReference> Accounts
    {
      get { return Cache.Instance.GetObjects<AccountReference>(true, a => a.Name, true); }
    }

    #endregion

    #region List<CargoType> CargoTypes

    List<CargoType> CargoTypes
    {
      get
      {
        return ScheduleWizardDialogController.StandardRatesConfiguration.GetRevision((DateTime)ScheduleWizardDialogController.ScheduledLoadingDate).CargoTypes
          .Distinct()
          .Where(ct => ct.IsPointToPointType == true)
          .OrderBy(i => i.Text)
          .ToList();
      }
    }

    #endregion

    public IEnumerable<CargoTypeButtonViewModel> CargoTypeButtonViewModels
    {
      get { return ResolveViewModels<CargoTypeButtonViewModel, CargoType>(CargoTypes); }
    }

    #region IEnumerable<Address> ShipFromAddresses

    public const string ShipFromAddressesProperty = "ShipFromAddresses";
    IEnumerable<Address> mShipFromAddresses;
    public IEnumerable<Address> ShipFromAddresses
    {
      get { return mShipFromAddresses; }
      set { SetProperty<IEnumerable<Address>>(ref mShipFromAddresses, value); }
    }

    #endregion

    #region IEnumerable<Address> ShipToAddresses

    public const string ShipToAddressesProperty = "ShipToAddresses";
    IEnumerable<Address> mShipToAddresses;
    public IEnumerable<Address> ShipToAddresses
    {
      get { return mShipToAddresses; }
      set { SetProperty<IEnumerable<Address>>(ref mShipToAddresses, value); }
    }

    #endregion

    public IEnumerable<SlotTime> SlotTimes
    {
      get { return Cache.Instance.GetObjects<SlotTime>(true, st => st.Text, true); }
    }

    #region IEnumerable<Employee> Drivers

    public const string DriversProperty = "Drivers";
    IEnumerable<Employee> mDrivers;
    public IEnumerable<Employee> Drivers
    {
      get { return Cache.Instance.GetObjects<Employee>(true, e => e.Fullname, true); }
    }

    #endregion


    #region CargoTypeButtonViewModel CargoType

    public const string CargoTypeProperty = "CargoType";
    CargoTypeButtonViewModel mCargoType;
    [Mandatory("Cargo type is Mandatory.")]
    public CargoTypeButtonViewModel CargoType
    {
      get { return mCargoType; }
      set
      {
        if (SetProperty<CargoTypeButtonViewModel>(ref mCargoType, value))
        {
          Model.QuickTransportManifest.CargoType = value.Model;
        }
      }
    }

    #endregion

    protected override void OnModelCompositionChanged(CompositionChangedEventArgs e)
    {
      try
      {
        switch (e.PropertyName)
        {
          case ScheduleWizardDialogContext.QuickTransportManifestProperty:
            this.Model.QuickTransportManifest.CompositionChanged += QuickTransportManifest_CompositionChanged;
            break;
        }
      }
      catch (Exception ex)
      {
        if (ExceptionHelper.HandleException(ex)) throw ex;
      }
      base.OnModelCompositionChanged(e);
    }

    void QuickTransportManifest_CompositionChanged(object sender, CompositionChangedEventArgs e)
    {
      try
      {
        switch (e.PropertyName)
        {
          case QuickTransportManifest.AccountProperty:
            OnPropertyChanged(AccountAddressVisibilityProperty);
            UpdateAddresses();
            break;
        }
      }
      catch (Exception ex)
      {
        if (ExceptionHelper.HandleException(ex)) throw ex;
      }
    }

    private void UpdateAddresses()
    {
      if (BusinessObject.IsNull(Model.QuickTransportManifest)) return;
      if (BusinessObject.IsNull(Model.QuickTransportManifest.Account)) return;

      Task.Run(() =>
      {
        try
        {
          using (var svc = ProxyFactory.GetService<IAccountManagementService>(SecurityContext.ServerName))
          {
            var add = svc.LoadAccountAddresses(Model.QuickTransportManifest.Account.GlobalIdentifier);

            MdiApplicationController.Current.Dispatcher.BeginInvoke(new Action(() =>
            {
              try
              {
                ShipFromAddresses = add.Where(a => a.AddressType.IsFlagged(FreightManagement.Business.AddressTypeFlags.ShipFromAddress)).OrderBy(a => a.AddressTextWithNameSingleLine);
                ShipToAddresses = add.Where(a => a.AddressType.IsFlagged(FreightManagement.Business.AddressTypeFlags.ShipToAddress)).OrderBy(a => a.AddressTextWithNameSingleLine);

                if (string.IsNullOrWhiteSpace(Model.QuickTransportManifest.OriginAddressText))
                {
                  SelectedShipFromAddress = ShipFromAddresses.Where(a => a.Id == Model.QuickTransportManifest.Account.GetExtensionObject<AccountReferenceExtension>().DefaultShipFromAddress).FirstOrDefault();
                }
              }
              catch (Exception ex)
              {
                if (ExceptionHelper.HandleException(ex)) throw;
              }
            }), System.Windows.Threading.DispatcherPriority.Normal);
          }

        }
        catch (Exception ex)
        {
          if (ExceptionHelper.HandleException(ex)) throw;
        }
      });
    }

    #region Address SelectedShipFromAddress

    public const string SelectedShipFromAddressProperty = "SelectedShipFromAddress";
    Address mSelectedShipFromAddress;
    public Address SelectedShipFromAddress
    {
      get { return mSelectedShipFromAddress; }
      set
      {
        SetProperty<Address>(ref mSelectedShipFromAddress, value);
        if (value != null)
        {
          Model.QuickTransportManifest.OriginAddressText = value.AddressText;
          Model.QuickTransportManifest.Consigner = value.Name;
        }
      }
    }

    #endregion

    #region Address SelectedShipToAddress

    public const string SelectedShipToAddressProperty = "SelectedShipToAddress";
    Address mSelectedShipToAddress;
    public Address SelectedShipToAddress
    {
      get { return mSelectedShipToAddress; }
      set
      {
        SetProperty<Address>(ref mSelectedShipToAddress, value);
        if (value != null)
        {
          Model.QuickTransportManifest.DestinationAddressText = value.AddressText;
          Model.QuickTransportManifest.Consignee = value.Name;
        }
      }
    }

    #endregion


    #region Commands

    #region DelegateCommand AddShipFromAddressCommand

    DelegateCommand mAddShipFromAddressCommand;
    public DelegateCommand AddShipFromAddressCommand
    {
      get { return mAddShipFromAddressCommand ?? (mAddShipFromAddressCommand = new DelegateCommand(ExecuteAddShipFromAddress, CanExecuteAddShipFromAddress)); }
    }

    bool CanExecuteAddShipFromAddress()
    {
      return true;
    }

    void ExecuteAddShipFromAddress()
    {
      EditAddressDialogController ac = Controller.CreateChildController<EditAddressDialogController>();
      ac.AddressType = ScheduleManagement.Prism.Dialogs.EditAddressDialog.AddressType.ShipFrom;
      ac.ScheduleWizardDialogViewModel = this;
      ac.Run();
    }

    #endregion

    #region DelegateCommand EditShipFromAddressCommand

    DelegateCommand mEditShipFromAddressCommand;
    public DelegateCommand EditShipFromAddressCommand
    {
      get { return mEditShipFromAddressCommand ?? (mEditShipFromAddressCommand = new DelegateCommand(ExecuteEditShipFromAddress, CanExecuteEditShipFromAddress)); }
    }

    bool CanExecuteEditShipFromAddress()
    {
      return true;
    }

    void ExecuteEditShipFromAddress()
    {
      try
      {
        if (BusinessObject.IsNull(SelectedShipFromAddress)) throw new MessageException("No Ship From address is selected");
        EditAddressDialogController ac = Controller.CreateChildController<EditAddressDialogController>();
        ac.AddressType = ScheduleManagement.Prism.Dialogs.EditAddressDialog.AddressType.ShipFrom;
        ac.Address = SelectedShipFromAddress;
        ac.ScheduleWizardDialogViewModel = this;
        ac.Run();
      }
      catch (Exception ex)
      {
        if (ExceptionHelper.HandleException(ex)) throw ex;
      }
    }

    #endregion

    #region DelegateCommand AddShipToAddressCommand

    DelegateCommand mAddShipToAddressCommand;
    public DelegateCommand AddShipToAddressCommand
    {
      get { return mAddShipToAddressCommand ?? (mAddShipToAddressCommand = new DelegateCommand(ExecuteAddShipToAddress, CanExecuteAddShipToAddress)); }
    }

    bool CanExecuteAddShipToAddress()
    {
      return true;
    }

    void ExecuteAddShipToAddress()
    {
      EditAddressDialogController ac = Controller.CreateChildController<EditAddressDialogController>();
      ac.AddressType = ScheduleManagement.Prism.Dialogs.EditAddressDialog.AddressType.ShipTo;
      ac.ScheduleWizardDialogViewModel = this;
      ac.Run();
    }

    #endregion

    #region DelegateCommand EditShipToAddressCommand

    DelegateCommand mEditShipToAddressCommand;
    public DelegateCommand EditShipToAddressCommand
    {
      get { return mEditShipToAddressCommand ?? (mEditShipToAddressCommand = new DelegateCommand(ExecuteEditShipToAddress, CanExecuteEditShipToAddress)); }
    }

    bool CanExecuteEditShipToAddress()
    {
      return true;
    }

    void ExecuteEditShipToAddress()
    {
      try
      {
        if (BusinessObject.IsNull(SelectedShipToAddress)) throw new MessageException("No Ship To address is selected");
        EditAddressDialogController ac = Controller.CreateChildController<EditAddressDialogController>();
        ac.AddressType = ScheduleManagement.Prism.Dialogs.EditAddressDialog.AddressType.ShipTo;
        ac.Address = SelectedShipToAddress;
        ac.ScheduleWizardDialogViewModel = this;
        ac.Run();
      }
      catch (Exception ex)
      {
        if (ExceptionHelper.HandleException(ex)) throw ex;
      }
    }

    #endregion

    #endregion


    #region Visibility AccountAddressVisibility

    const string AccountAddressVisibilityProperty = "AccountAddressVisibility";
    public Visibility AccountAddressVisibility
    {
      get
      {
        if (BusinessObject.IsNull(Model.QuickTransportManifest.Account)) return Visibility.Collapsed;
        return Visibility.Visible;
      }
    }

    #endregion

  }
}

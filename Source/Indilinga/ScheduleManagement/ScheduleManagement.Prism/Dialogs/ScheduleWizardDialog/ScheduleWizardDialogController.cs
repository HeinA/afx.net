﻿using Afx.Business;
using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Business.Service;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using FleetManagement.Business;
using FreightManagement.Business;
using Microsoft.Practices.Unity;
using ScheduleManagement.Business;
using ScheduleManagement.Business.Service;
using ScheduleManagement.Prism.Activities.VehicleScheduleManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;

namespace ScheduleManagement.Prism.Dialogs.ScheduleWizardDialog
{
  public class ScheduleWizardDialogController : MdiDialogController<ScheduleWizardDialogContext, ScheduleWizardDialogViewModel>
  {
    public const string ScheduleWizardDialogControllerKey = "ScheduleManagement.Prism.Dialogs.ScheduleWizardDialog.ScheduleWizardDialogController";

    #region Constructors

    public ScheduleWizardDialogController(IController controller)
      : base(ScheduleWizardDialogControllerKey, controller)
    {
    }

    #endregion

    #region Dependency Parent Controller

    [Dependency]
    public VehicleScheduleManagementController VehicleScheduleManagementController { get; set; }

    #endregion

    #region ScheduleWizardDialogViewModel ScheduleWizardViewModel

    public const string ScheduleWizardViewModelProperty = "ScheduleWizardViewModel";
    ScheduleWizardDialogViewModel mScheduleWizardViewModel;
    public ScheduleWizardDialogViewModel ScheduleWizardViewModel
    {
      get { return mScheduleWizardViewModel; }
      set { mScheduleWizardViewModel = value; }
    }

    #endregion

    #region When Dialog is run

    protected override bool OnRun()
    {
      if (DataContext == null) DataContext = new ScheduleWizardDialogContext();
      this.DataContext.QuickTransportManifest = new QuickTransportManifest();
      this.DataContext.QuickTransportManifest.ScheduledLoadingDate = ScheduledLoadingDate;
      this.DataContext.QuickTransportManifest.SelectedVehicle = SelectedVehicle;
      ViewModel.Caption = "Quick Schedule Dialog";
      return base.OnRun();
    }

    #endregion

    protected override void ApplyChanges()
    {
      try
      {
        using (var svc = ProxyFactory.GetService<IScheduleManagementService>(SecurityContext.MasterServer.Name))
        {
          svc.SaveQuickTransportManifest(this.DataContext.QuickTransportManifest);
        }

        if (Setting.GetSetting<ScheduleSetting>().RefreshOnSave) VehicleScheduleManagementController.Refresh();
      }
      catch (Exception ex)
      {
        base.CanClose = false;
        System.Media.SystemSounds.Exclamation.Play();
        if (ExceptionHelper.HandleException(ex)) throw ex;
      }

      //string exportBooking = String.Empty;
      //exportBooking = "Horse No:\t{0}\r\n";
      //exportBooking += "Client:\t{1}\r\n";
      //exportBooking += "Loading:\t{2}\r\n";
      //exportBooking += "Loading Date:\t{3}\r\n";
      //exportBooking += "Off-Loading:\t{4}\r\n";
      //exportBooking += "Off-Loading Date:\t{5}\r\n";
      //exportBooking += "Transporter:\t{6}\r\n";
      //exportBooking += "Horse:\t{7}\r\n";
      //exportBooking += "Trailer 1:\t{8} - {9}\r\n";
      //exportBooking += "Trailer 2:\t{10} - {11}\r\n";
      //exportBooking += "Driver:\t{12}\r\n";
      //exportBooking += "Initials, Surname:\t{13}, {14}\r\n";
      //exportBooking += "Passport:\t{15}\r\n";
      //exportBooking += "Exp Date:\t{16}\r\n";
      //exportBooking += "ID:\t{17}\r\n";
      //exportBooking += "Nam Cell:\t{18}\r\n";
      //exportBooking += "RSA Cell:\t{19}\r\n";
      //exportBooking += "Rate:\t{20}\r\n";
      //exportBooking += "Invoice To:\t{21}\r\n";
      //exportBooking += "Comments:\t{22}";



      //exportBooking = "{\\rtf1\\ansi \\ansicpg1252";
      //exportBooking += "{\\fonttbl{\\f0\\fnil Calibri;}{\\f1\\fnil Calibri;}{\\f2\\fnil Calibri;}{\\f3\\fnil Calibri;}{\\f4\\fnil Calibri;}{\\f5\\fnil Calibri;}{\\f6\\fnil Calibri Light;}{\\f7\\fnil Calibri;}{\\f8\\fnil Calibri;}{\\f9\\fnil Calibri;}{\\f10\\fnil Calibri;}{\\f11\\fnil Calibri;}{\\f12\\fnil Calibri;}{\\f13\\fnil Calibri;}{\\f14\\fnil Calibri;}{\\f15\\fnil Calibri;}{\\f16\\fnil Calibri;}{\\f17\\fnil Calibri;}{\\f18\\fnil Calibri;}{\\f19\\fnil Calibri;}{\\f20\\fnil Calibri;}{\\f21\\fnil Calibri;}{\\f22\\fnil Segoe UI;}{\\f23\\fnil Calibri;}{\\f24\\fnil Calibri;}}";
      //exportBooking += "{\\info{\\id220}}\\plain {\\colortbl\\red0\\green0\\blue0;\\red255\\green255\\blue255;\\red255\\green0\\blue0;\\red0\\green255\\blue0;\\red0\\green0\\blue255;\\red255\\green255\\blue0;\\red255\\green0\\blue255;\\red0\\green255\\blue255;\\red0\\green0\\blue0;\\red255\\green255\\blue255;\\red255\\green0\\blue0;\\red0\\green255\\blue0;\\red0\\green0\\blue255;\\red255\\green255\\blue0;\\red255\\green0\\blue255;\\red0\\green255\\blue255;\\red128\\green0\\blue0;\\red0\\green128\\blue0;\\red0\\green0\\blue128;\\red128\\green128\\blue0;\\red128\\green0\\blue128;\\red0\\green128\\blue128;\\red192\\green192\\blue192;\\red128\\green128\\blue128;\\red153\\green153\\blue255;\\red153\\green51\\blue102;\\red255\\green255\\blue204;\\red204\\green255\\blue255;\\red102\\green0\\blue102;\\red255\\green128\\blue128;\\red0\\green102\\blue204;\\red204\\green204\\blue255;\\red0\\green0\\blue128;\\red255\\green0\\blue255;\\red255\\green255\\blue0;\\red0\\green255\\blue255;\\red128\\green0\\blue128;\\red128\\green0\\blue0;\\red0\\green128\\blue128;\\red0\\green0\\blue255;\\red0\\green204\\blue255;\\red204\\green255\\blue255;\\red204\\green255\\blue204;\\red255\\green255\\blue153;\\red153\\green204\\blue255;\\red255\\green153\\blue204;\\red204\\green153\\blue255;\\red255\\green204\\blue153;\\red51\\green102\\blue255;\\red51\\green204\\blue204;\\red153\\green204\\blue0;\\red255\\green204\\blue0;\\red255\\green153\\blue0;\\red255\\green102\\blue0;\\red102\\green102\\blue153;\\red150\\green150\\blue150;\\red0\\green51\\blue102;\\red51\\green153\\blue102;\\red0\\green51\\blue0;\\red51\\green51\\blue0;\\red153\\green51\\blue0;\\red153\\green51\\blue102;\\red51\\green51\\blue153;\\red51\\green51\\blue51;;\\red255\\green255\\blue255;\\red100\\green100\\blue100;\\red240\\green240\\blue240;\\red0\\green0\\blue0;\\red255\\green255\\blue255;\\red160\\green160\\blue160;\\red51\\green153\\blue255;\\red0\\green0\\blue0;\\red200\\green200\\blue200;\\red55\\green55\\blue55;\\red255\\green255\\blue255;\\red100\\green100\\blue100;\\red0\\green0\\blue0;\\red255\\green255\\blue255;\\red0\\green0\\blue0;\\red255\\green255\\blue225;\\red0\\green0\\blue0;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;\\red192\\green192\\blue192;\\red150\\green150\\blue150;\\red128\\green128\\blue128;\\red102\\green102\\blue102;\\red51\\green51\\blue51;\\red50\\green106\\blue199;\\red192\\green53\\blue62;\\red129\\green87\\blue183;\\red0\\green124\\blue32;\\red176\\green62\\blue132;\\red182\\green73\\blue0;\\red38\\green115\\blue146;\\red51\\green102\\blue153;\\red128\\green0\\blue0;\\red0\\green128\\blue0;\\red0\\green0\\blue128;\\red128\\green128\\blue0;\\red128\\green0\\blue128;\\red0\\green128\\blue128;\\red0\\green0\\blue208;\\red212\\green212\\blue212;}";
      //exportBooking += "\\trowd \\trgaph30\\trleft-30\\trrh290\\cellx1856\\cellx7367\\pard \\intbl \\ql \\f20\\fs22 \\b \\cf8 Horse No:\\cell \\ql \\f0\\fs22 \\b0 {0}\\cell ";
      //exportBooking += "\\pard \\intbl \\row\\trowd \\trgaph30\\trleft-30\\trrh290\\cellx1856\\clcfpat22\\clcbpat64\\clshdng10000\\cellx7367\\pard \\intbl \\ql \\f20\\fs22 \\b Client:\\cell \\ql \\f0\\fs22 \\b0 {1}\\cell ";
      //exportBooking += "\\pard \\intbl \\row\\trowd \\trgaph30\\trleft-30\\trrh290\\cellx1856\\clcfpat22\\clcbpat64\\clshdng10000\\cellx7367\\pard \\intbl \\ql \\f20\\fs22 \\b Loading:\\cell \\ql \\f0\\fs22 \\b0 {2} \\cell ";
      //exportBooking += "\\pard \\intbl \\row\\trowd \\trgaph30\\trleft-30\\trrh290\\cellx1856\\cellx7367\\pard \\intbl \\ql \\f20\\fs22 \\b Loading Date:\\cell \\qr \\f0\\fs22 \\b0 {3}\\cell ";
      //exportBooking += "\\pard \\intbl \\row\\trowd \\trgaph30\\trleft-30\\trrh290\\cellx1856\\clcfpat22\\clcbpat64\\clshdng10000\\cellx7367\\pard \\intbl \\ql \\f20\\fs22 \\b Off-Loading:\\cell \\ql \\f0\\fs22 \\b0 {4}\\cell ";
      //exportBooking += "\\pard \\intbl \\row\\trowd \\trgaph30\\trleft-30\\trrh290\\cellx1856\\cellx7367\\pard \\intbl \\ql \\f20\\fs22 \\b Off-Loading Date:\\cell \\qr \\f0\\fs22 \\b0 {5}\\cell ";
      //exportBooking += "\\pard \\intbl \\row\\trowd \\trgaph30\\trleft-30\\trrh290\\cellx1856\\cellx7367\\pard \\intbl \\ql \\f20\\fs22 \\b Transporter:\\cell \\ql \\f0\\fs22 \\b0 {6}\\cell ";
      //exportBooking += "\\pard \\intbl \\row\\trowd \\trgaph30\\trleft-30\\trrh290\\cellx1856\\clcfpat22\\clcbpat64\\clshdng10000\\cellx7367\\pard \\intbl \\ql \\f20\\fs22 \\b Horse:\\cell \\ql \\f0\\fs22 \\b0 {7}\\cell ";
      //exportBooking += "\\pard \\intbl \\row\\trowd \\trgaph30\\trleft-30\\trrh290\\cellx1856\\clcfpat22\\clcbpat64\\clshdng10000\\cellx7367\\pard \\intbl \\ql \\f20\\fs22 \\b Trailer 1:\\cell \\ql \\f0\\fs22 \\b0 {8} - {9}\\cell ";
      //exportBooking += "\\pard \\intbl \\row\\trowd \\trgaph30\\trleft-30\\trrh290\\cellx1856\\clcfpat22\\clcbpat64\\clshdng10000\\cellx7367\\pard \\intbl \\ql \\f20\\fs22 \\b Trailer 2:\\cell \\ql \\f0\\fs22 \\b0 {10} - {11}\\cell ";
      //exportBooking += "\\pard \\intbl \\row\\trowd \\trgaph30\\trleft-30\\trrh290\\cellx1856\\clcfpat22\\clcbpat64\\clshdng10000\\cellx7367\\pard \\intbl \\ql \\f20\\fs22 \\b Driver:\\cell \\ql \\f0\\fs22 \\b0 {12}\\cell ";
      //exportBooking += "\\pard \\intbl \\row\\trowd \\trgaph30\\trleft-30\\trrh290\\cellx1856\\clcfpat22\\clcbpat64\\clshdng10000\\cellx7367\\pard \\intbl \\ql \\f20\\fs22 \\b Initials, Surname:\\cell \\ql \\f0\\fs22 \\b0 {13}, {14}\\cell ";
      //exportBooking += "\\pard \\intbl \\row\\trowd \\trgaph30\\trleft-30\\trrh290\\cellx1856\\clcfpat22\\clcbpat64\\clshdng10000\\cellx7367\\pard \\intbl \\ql \\f20\\fs22 \\b Passport:\\cell \\ql \\f0\\fs22 \\b0 {15}\\cell ";
      //exportBooking += "\\pard \\intbl \\row\\trowd \\trgaph30\\trleft-30\\trrh290\\cellx1856\\clcfpat22\\clcbpat64\\clshdng10000\\cellx7367\\pard \\intbl \\ql \\f20\\fs22 \\b Exp Date:\\cell \\qr \\f0\\fs22 \\b0 {16}\\cell ";
      //exportBooking += "\\pard \\intbl \\row\\trowd \\trgaph30\\trleft-30\\trrh290\\cellx1856\\clcfpat22\\clcbpat64\\clshdng10000\\cellx7367\\pard \\intbl \\ql \\f20\\fs22 \\b ID:\\cell \\qr \\f0\\fs22 \\b0 {17}\\cell ";
      //exportBooking += "\\pard \\intbl \\row\\trowd \\trgaph30\\trleft-30\\trrh290\\cellx1856\\clcfpat22\\clcbpat64\\clshdng10000\\cellx7367\\pard \\intbl \\ql \\f20\\fs22 \\b Nam Cell:\\cell \\ql \\f0\\fs22 \\b0 {18}\\cell ";
      //exportBooking += "\\pard \\intbl \\row\\trowd \\trgaph30\\trleft-30\\trrh290\\cellx1856\\clcfpat22\\clcbpat64\\clshdng10000\\cellx7367\\pard \\intbl \\ql \\f20\\fs22 \\b RSA Cell:\\cell \\ql \\f0\\fs22 \\b0 {19}\\cell ";
      //exportBooking += "\\pard \\intbl \\row\\trowd \\trgaph30\\trleft-30\\trrh290\\cellx1856\\cellx7367\\pard \\intbl \\ql \\f20\\fs22 \\b Border Post\\cell \\qr \\f0\\fs22 \\b0 {20}\\cell ";
      //exportBooking += "\\pard \\intbl \\row\\trowd \\trgaph30\\trleft-30\\trrh290\\cellx1856\\clcfpat22\\clcbpat64\\clshdng10000\\cellx7367\\pard \\intbl \\ql \\f20\\fs22 \\b Rate:\\cell \\qr \\f0\\fs22 \\b0 {21}\\cell ";
      //exportBooking += "\\pard \\intbl \\row\\trowd \\trgaph30\\trleft-30\\trrh290\\cellx1856\\cellx7367\\pard \\intbl \\ql \\f20\\fs22 \\b Invoice To:\\cell \\ql \\f0\\fs22 \\b0 {22}\\cell ";
      //exportBooking += "\\pard \\intbl \\row\\trowd \\trgaph30\\trleft-30\\trrh290\\cellx1856\\cellx7367\\pard \\intbl \\ql \\f20\\fs22 \\b Comments:\\cell \\qr \\f0\\fs22 \\b0 {23}\\cell \\pard \\intbl \\row}";






      //exportBooking = String.Format(exportBooking, 
      //  this.DataContext.QuickTransportManifest.SelectedVehicle.FleetNumber,
      //  this.DataContext.QuickTransportManifest.Account.Name,
      //  this.DataContext.QuickTransportManifest.OriginAddressText,
      //  this.DataContext.QuickTransportManifest.ScheduledLoadingDate,
      //  this.DataContext.QuickTransportManifest.DestinationAddressText,
      //  this.DataContext.QuickTransportManifest.ScheduledOffLoadingDate,
      //  SecurityContext.OrganizationalUnit.Owner.Name,
      //  this.DataContext.QuickTransportManifest.SelectedVehicle.RegistrationNumber,
      //  "Unspecified Trailer 1 Reg",
      //  "Unspecified Trailer 1 Fleet",
      //  "Unspecified Trailer 2 Reg",
      //  "Unspecified Trailer 2 Fleet",
      //  BusinessObject.IsNull(this.DataContext.QuickTransportManifest.Driver) ? "N/A" : this.DataContext.QuickTransportManifest.Driver.Nickname,
      //  BusinessObject.IsNull(this.DataContext.QuickTransportManifest.Driver) ? "N/A" : this.DataContext.QuickTransportManifest.Driver.Initials,
      //  BusinessObject.IsNull(this.DataContext.QuickTransportManifest.Driver) ? "N/A" : this.DataContext.QuickTransportManifest.Driver.Lastname,
      //  BusinessObject.IsNull(this.DataContext.QuickTransportManifest.Driver) ? "N/A" : this.DataContext.QuickTransportManifest.Driver.PassportNumber,
      //  BusinessObject.IsNull(this.DataContext.QuickTransportManifest.Driver) ? "N/A" : (this.DataContext.QuickTransportManifest.Driver.PassportExpiryDate == null ? "" : ((DateTime)this.DataContext.QuickTransportManifest.Driver.PassportExpiryDate).ToString("d")),
      //  BusinessObject.IsNull(this.DataContext.QuickTransportManifest.Driver) ? "N/A" : this.DataContext.QuickTransportManifest.Driver.IDNumber,
      //  BusinessObject.IsNull(this.DataContext.QuickTransportManifest.Driver.TelephoneNumbers.Where(t => t.TelephoneType.SystemTelephoneType == ContactManagement.Business.SystemTelephoneType.Mobile).FirstOrDefault()) ? "N/A" : this.DataContext.QuickTransportManifest.Driver.TelephoneNumbers.Where(t => t.TelephoneType.SystemTelephoneType == ContactManagement.Business.SystemTelephoneType.Mobile).FirstOrDefault().TelephoneNumber,
      //  BusinessObject.IsNull(this.DataContext.QuickTransportManifest.Driver.TelephoneNumbers.Where(t => t.TelephoneType.SystemTelephoneType == ContactManagement.Business.SystemTelephoneType.Foreign).FirstOrDefault()) ? "N/A" : this.DataContext.QuickTransportManifest.Driver.TelephoneNumbers.Where(t => t.TelephoneType.SystemTelephoneType == ContactManagement.Business.SystemTelephoneType.Foreign).FirstOrDefault().TelephoneNumber,
      //  BusinessObject.IsNull(this.DataContext.QuickTransportManifest.BorderPost) ? "" : this.DataContext.QuickTransportManifest.BorderPost.Text,
      //  this.DataContext.QuickTransportManifest.Price,
      //  this.DataContext.QuickTransportManifest.Consignee,
      //  "");
      //Clipboard.SetText(exportBooking);
      //ExceptionHelper.HandleException(new MessageException("BOOKING\r\n\r\nCOPIED TO CLIPBOARD"));






      base.ApplyChanges();
    }

    #region Rates Configuration to use for costing waybill

    void SetRatesConfigurations()
    {
      RatesConfigurationReference rc = Cache.Instance.GetObjects<RatesConfigurationReference>(rcr1 => rcr1.IsStandardConfiguration).FirstOrDefault();
      if (rc == null) throw new RatesConfigurationException("No standard Rates have been configured");
      StandardRatesConfiguration = rc;

      if (DataContext.RatesConfiguration != null) DataContext.RatesConfiguration = Cache.Instance.GetObject<RatesConfigurationReference>(DataContext.RatesConfiguration.GlobalIdentifier);
    }

    #region RatesConfigurationReference StandardRatesConfiguration

    RatesConfigurationReference mStandardRatesConfiguration;
    public RatesConfigurationReference StandardRatesConfiguration
    {
      get
      {
        if (mStandardRatesConfiguration == null)
        {
          SetRatesConfigurations();
        }
        return mStandardRatesConfiguration;
      }
      set
      {
        mStandardRatesConfiguration = value;

        if (DataContext.RatesConfiguration == null)
        {
          if (StandardRatesConfiguration == null) throw new RatesConfigurationException("No standard Rates have been configured");
          var rcr = StandardRatesConfiguration.GetRevision((DateTime)ScheduledLoadingDate);
          if (rcr == null) throw new RatesConfigurationException("The Standard Rates Configuration does not have a revision for this date.");
          //DataContext.Document.VolumetricFactor = rcr.VolumetricFactor;
        }
      }
    }

    #endregion

    #endregion

    #region Incoming values from parent

    #region Vehicle SelectedVehicle

    public const string SelectedVehicleProperty = "SelectedVehicle";
    Vehicle mSelectedVehicle;
    public Vehicle SelectedVehicle
    {
      get { return mSelectedVehicle; }
      set { mSelectedVehicle = value; }
    }

    #endregion

    #region DateTime? ScheduledLoadingDate

    public const string ScheduledLoadingDateProperty = "ScheduledLoadingDate";
    DateTime? mScheduledLoadingDate;
    public DateTime? ScheduledLoadingDate
    {
      get { return mScheduledLoadingDate; }
      set 
      { 
        mScheduledLoadingDate = value; 
      }
    }

    #endregion

    #endregion
  }
}

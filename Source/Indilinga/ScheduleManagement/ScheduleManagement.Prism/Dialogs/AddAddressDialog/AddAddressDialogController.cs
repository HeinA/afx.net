﻿using Afx.Prism;
using Afx.Prism.RibbonMdi;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace ScheduleManagement.Prism.Dialogs.AddAddressDialog
{
  public class AddAddressDialogController : MdiDialogController<AddAddressDialogContext, AddAddressDialogViewModel>
  {
    public const string AddAddressDialogControllerKey = "ScheduleManagement.Prism.Dialogs.AddAddressDialog.AddAddressDialogController";

    public AddAddressDialogController(IController controller)
      : base(AddAddressDialogControllerKey, controller)
    {
    }

    protected override bool OnRun()
    {
      if (DataContext == null) DataContext = new AddAddressDialogContext();
      ViewModel.Caption = "AddAddressDialog";
      return base.OnRun();
    }

    protected override void ApplyChanges()
    {
      base.ApplyChanges();
    }
  }
}

﻿using Afx.Business.Collections;
using Afx.Business.Security;
using Afx.Business.Service;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using Afx.Prism.RibbonMdi.Documents;
using FleetManagement.Business;
using FreightManagement.Business;
using Microsoft.Practices.Unity;
using ScheduleManagement.Business;
using ScheduleManagement.Business.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace ScheduleManagement.Prism.Dialogs.ReAssignBookingsDialog
{
  public class ReAssignBookingsDialogController : MdiDialogController<ReAssignBookingsDialogContext, ReAssignBookingsDialogViewModel>
  {
    public const string ReAssignBookingsDialogControllerKey = "ScheduleManagement.Prism.Dialogs.ReAssignBookingsDialog.ReAssignBookingsDialogController";

    public ReAssignBookingsDialogController(IController controller)
      : base(ReAssignBookingsDialogControllerKey, controller)
    {
    }

    protected override bool OnRun()
    {
      if (DataContext == null) DataContext = new ReAssignBookingsDialogContext();
      ViewModel.Caption = "Re-assign Bookings Dialog";
      return base.OnRun();
    }

    protected override void ApplyChanges()
    {
      try
      {

        Bookings.Clear();
        foreach (BookingItemViewModel b in this.ViewModel.Bookings)
        {
          if (!Bookings.Contains(b.Model)) Bookings.Add(b.Model);
        }

        if (Vehicles.Count == 1 && this.ViewModel.Bookings.Count() > 0)
        {
          using (var svc = ProxyFactory.GetService<IScheduleManagementService>(SecurityContext.ServerName))
          {
            Vehicle newVehicle = Vehicles.FirstOrDefault<Vehicle>();
            svc.RescheduleVehicles(SelectedVehicle, newVehicle, Bookings);
          }

          foreach (VehicleSchedule vs in Bookings)
          {

            IMdiDocumentController dc;
            switch (vs.ScheduleType)
            {
              case ScheduleType.LoadAllocation:
                dc = (IMdiDocumentController)MdiApplicationController.Current.EditDocument(LoadAllocation.DocumentTypeIdentifier, vs.DocumentIdentifier);
                dc.Print(false);
                //dc.Terminate();
                break;
              //case ScheduleType.Delivery:
              //  dc = (IMdiDocumentController)MdiApplicationController.Current.EditDocument(DeliveryManifest.DocumentTypeIdentifier, vs.DocumentIdentifier);
              //  dc.Print(false);
              //  //dc.Terminate();
              //  break;
              //case ScheduleType.Collection:
              //  dc = (IMdiDocumentController)MdiApplicationController.Current.EditDocument(CollectionManifest.DocumentTypeIdentifier, vs.DocumentIdentifier);
              //  dc.Print(false);
              //  //dc.Terminate();
              //  break;
              case ScheduleType.Transport:
                dc = (IMdiDocumentController)MdiApplicationController.Current.EditDocument(TransportManifest.DocumentTypeIdentifier, vs.DocumentIdentifier);
                dc.Print(false);
                //dc.Terminate();
                break;
              //case ScheduleType.Transfer:
              //  dc = (IMdiDocumentController)MdiApplicationController.Current.EditDocument(TransferManifest.DocumentTypeIdentifier, vs.DocumentIdentifier);
              //  dc.Print(false);
              //  //dc.Terminate();
              //  break;
              //case ScheduleType.PODHandover:
              //  dc = (IMdiDocumentController)MdiApplicationController.Current.EditDocument(PODHandoverManifest.DocumentTypeIdentifier, vs.DocumentIdentifier);
              //  dc.Print(false);
              //  //dc.Terminate();
              //  break;
            }
          }
        }
      }
      catch
      {
        throw;
      }


      base.ApplyChanges();
    }


    #region "Colllections"

    BasicCollection<Vehicle> mVehicles = new BasicCollection<Vehicle>();
    public BasicCollection<Vehicle> Vehicles
    {
      get { return mVehicles; }
    }


    #region BasicCollection<VehicleSchedule> Bookings

    BasicCollection<VehicleSchedule> mBookings = new BasicCollection<VehicleSchedule>();
    public BasicCollection<VehicleSchedule> Bookings
    {
      get { return mBookings; }
      set { mBookings = value; }
    }

    #endregion

    #region IVehicleCollection VehicleCollection

    public const string VehicleCollectionProperty = "VehicleCollection";
    IVehicleCollection mVehicleCollection;
    public IVehicleCollection VehicleCollection
    {
      get { return mVehicleCollection; }
      set
      {
        mVehicleCollection = value;
        foreach (var v in mVehicleCollection.Vehicles)
        {
          Vehicles.Add(v);
        }
      }
    }

    #endregion

    #endregion


    #region Vehicle SelectedVehicle

    public const string SelectedVehicleProperty = "SelectedVehicle";
    Vehicle mSelectedVehicle;
    public Vehicle SelectedVehicle
    {
      get { return mSelectedVehicle; }
      set { mSelectedVehicle = value; }
    }

    #endregion
    
    #region DateTime FilterDateStartOnController
    
    public const string FilterDateStartOnControllerProperty = "FilterDateStartOnController";
    DateTime mFilterDateStartOnController = new DateTime();
    public DateTime FilterDateStartOnController
    {
      get { return mFilterDateStartOnController; }
      set { mFilterDateStartOnController = value; }
    }
    
    #endregion

    #region DateTime FilterDateEndOnController

    public const string FilterDateEndOnControllerProperty = "FilterDateEndOnController";
    DateTime mFilterDateEndOnController = new DateTime();
    public DateTime FilterDateEndOnController
    {
      get { return mFilterDateEndOnController; }
      set { mFilterDateEndOnController = value; }
    }

    #endregion
  }
}

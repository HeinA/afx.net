﻿using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Business.Service;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using FleetManagement.Business;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Unity;
using ScheduleManagement.Business;
using ScheduleManagement.Business.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace ScheduleManagement.Prism.Dialogs.ReAssignBookingsDialog
{
  public class ReAssignBookingsDialogViewModel : MdiDialogViewModel<ReAssignBookingsDialogContext>
  {
    [InjectionConstructor]
    public ReAssignBookingsDialogViewModel(IController controller)
      : base(controller)
    {
    }

    [Dependency]
    public ReAssignBookingsDialogController ReAssignBookingsDialogController { get; set; }

    #region "Collections"

    public IEnumerable<VehicleItemViewModel> Vehicles
    {
      get
      {
        if (!String.IsNullOrWhiteSpace(FilterText))
        {
          return ResolveViewModels<VehicleItemViewModel, Vehicle>(Cache.Instance.GetObjects<Vehicle>(v => v.MakeModel.ToUpperInvariant().Contains(FilterText.ToUpperInvariant()) || v.RegistrationNumber.ToUpperInvariant().Contains(FilterText.ToUpperInvariant()) || v.FleetNumber.ToUpperInvariant().Contains(FilterText.ToUpperInvariant()), v => v.FleetNumber, true));
        }
        else
        {
          return ResolveViewModels<VehicleItemViewModel, Vehicle>(Cache.Instance.GetObjects<Vehicle>(v => v.FleetNumber, true));
        }
      }
    }

    public IEnumerable<VehicleUnit> VehicleUnits
    {
      get { return Cache.Instance.GetObjects<VehicleUnit>(v => v.FleetUnitNumber.ToUpperInvariant().Contains(FilterText.ToUpperInvariant()), v => v.FleetUnitNumber, true); }
    }

    private IEnumerable<BookingItemViewModel> mBookings;
    public IEnumerable<BookingItemViewModel> Bookings
    {
      get
      {
        return mBookings;
      }
    }

    private void UpdateBookings()
    {
      if (FilterDateStart == null || FilterDateEnd == null) return;
      using (var svc = ProxyFactory.GetService<IScheduleManagementService>(SecurityContext.ServerName))
      {
        ReAssignBookingsDialogController.Bookings = svc.LoadVehicleSchedules((DateTime)FilterDateStart, (DateTime)FilterDateEnd);
      }
      mBookings = ResolveViewModels<BookingItemViewModel, VehicleSchedule>(ReAssignBookingsDialogController.Bookings.Where<VehicleSchedule>(vs => vs.Vehicle.Equals(SelectedVehicle) && (IsDateWithin(FilterDateStart, FilterDateEnd, vs.ScheduledLoadingDate) || IsDateWithin(FilterDateStart, FilterDateEnd, vs.ScheduledOffloadingDate))).ToList());// 
      OnPropertyChanged("Bookings");
    }

    #endregion

    #region "Filters"

    private bool IsDateWithin(DateTime? startOfRangeDate, DateTime? endOfRangeDate, DateTime? testDate)
    {
      if (testDate >= startOfRangeDate && testDate <= endOfRangeDate) return true;
      return false;
    }

    public Vehicle SelectedVehicle
    {
      get { return ReAssignBookingsDialogController.SelectedVehicle; }
    }

    #region DateTime FilterDateStart

    public const string FilterDateStartProperty = "FilterDateStart";
    DateTime? mFilterDateStart;
    public DateTime? FilterDateStart
    {
      get { return mFilterDateStart; }
      set
      {
        if (SetProperty<DateTime?>(ref mFilterDateStart, value))
        {
          UpdateBookings();
        }
      }
    }

    #endregion

    #region DateTime FilterDateEnd

    public const string FilterDateEndProperty = "FilterDateEnd";
    DateTime? mFilterDateEnd;
    public DateTime? FilterDateEnd
    {
      get { return mFilterDateEnd; }
      set
      {
        if (SetProperty<DateTime?>(ref mFilterDateEnd, value))
        {
          UpdateBookings();
        }
      }
    }

    #endregion


    #region string FilterText

    public const string FilterTextProperty = "FilterText";
    string mFilterText;
    public string FilterText
    {
      get { return mFilterText; }
      set
      {
        if (SetProperty<string>(ref mFilterText, value))
        {
          OnPropertyChanged("Vehicles");
        }
      }
    }

    #endregion

    #endregion

    

    #region VehicleItemViewModel SelectedItemViewModel

    public const string SelectedItemViewModelProperty = "SelectedItemViewModel";
    VehicleItemViewModel mSelectedItemViewModel;
    public VehicleItemViewModel SelectedItemViewModel
    {
      get { return mSelectedItemViewModel; }
      set { SetProperty<VehicleItemViewModel>(ref mSelectedItemViewModel, value); }
    }

    #endregion

    #region DelegateCommand KeyDownCommand

    DelegateCommand<KeyEventArgs> mKeyDownCommand;
    public DelegateCommand<KeyEventArgs> KeyDownCommand
    {
      get { return mKeyDownCommand ?? (mKeyDownCommand = new DelegateCommand<KeyEventArgs>(ExecuteKeyDown)); }
    }

    void ExecuteKeyDown(KeyEventArgs e)
    {
      if (e.Key != Key.Space) return;
      if (SelectedItemViewModel != null) SelectedItemViewModel.IsChecked = !SelectedItemViewModel.IsChecked;
      e.Handled = true;
    }

    #endregion
  }
}

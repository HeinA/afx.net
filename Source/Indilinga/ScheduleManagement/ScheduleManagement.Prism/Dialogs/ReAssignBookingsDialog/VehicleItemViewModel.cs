﻿using Afx.Prism;
using FleetManagement.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScheduleManagement.Prism.Dialogs.ReAssignBookingsDialog
{
  public class VehicleItemViewModel : SelectableViewModel<Vehicle>
  {
    [InjectionConstructor]
    public VehicleItemViewModel(IController controller)
      : base(controller)
    {
    }

    [Dependency]
    public ReAssignBookingsDialogController ReAssignBookingsDialogController { get; set; }

    new ReAssignBookingsDialogViewModel Parent
    {
      get { return (ReAssignBookingsDialogViewModel)base.Parent; }
    }


    public override bool IsSelected
    {
      get
      {
        if (base.IsSelected) Parent.SelectedItemViewModel = this;
        return base.IsSelected;
      }
      set
      {
        base.IsSelected = value;
        if (base.IsSelected) Parent.SelectedItemViewModel = this;
      }
    }

    #region bool IsChecked

    public const string IsCheckedProperty = "IsChecked";
    public bool IsChecked
    {
      get { return ReAssignBookingsDialogController.Vehicles.Contains(Model); }
      set
      {
        if (value)
        {
          if (!ReAssignBookingsDialogController.Vehicles.Contains(Model))
          {
            ReAssignBookingsDialogController.Vehicles.Add(Model);
            OnPropertyChanged(IsCheckedProperty);
          }
        }
        else
        {
          if (ReAssignBookingsDialogController.Vehicles.Contains(Model))
          {
            ReAssignBookingsDialogController.Vehicles.Remove(Model);
            OnPropertyChanged(IsCheckedProperty);
          }
        }
      }
    }

    #endregion
  }
}

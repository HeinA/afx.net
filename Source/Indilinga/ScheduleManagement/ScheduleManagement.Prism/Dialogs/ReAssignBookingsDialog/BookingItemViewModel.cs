﻿using Afx.Prism;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScheduleManagement.Prism.Dialogs.ReAssignBookingsDialog
{
  public class BookingItemViewModel : ViewModel<ScheduleManagement.Business.VehicleSchedule>
  {
    [InjectionConstructor]
    public BookingItemViewModel(IController controller)
      : base(controller)
    {
    }

    [Dependency]
    public ReAssignBookingsDialogController ReAssignBookingsDialogController { get; set; }

    new ReAssignBookingsDialogViewModel Parent
    {
      get { return (ReAssignBookingsDialogViewModel)base.Parent; }
    }
  }
}

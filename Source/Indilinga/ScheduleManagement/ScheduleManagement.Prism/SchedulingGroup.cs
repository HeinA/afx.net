﻿using Afx.Prism.RibbonMdi.Ribbon;
using FreightManagement.Prism;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScheduleManagement.Prism
{
  [Export(typeof(IRibbonGroup))]
  public class SchedulingGroup : RibbonGroup
  {
    public const string GroupName = "Scheduling";

    public override string TabName
    {
      get { return FreightManagementTab.TabName; }
    }

    public override string Name
    {
      get { return GroupName; }
    }
  }
}

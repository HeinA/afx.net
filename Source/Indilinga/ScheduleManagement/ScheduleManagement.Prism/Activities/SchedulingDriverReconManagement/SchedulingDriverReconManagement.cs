﻿using Afx.Business.Activities;
using ScheduleManagement.Business;
using ScheduleManagement.Business.Service;
using Afx.Prism.RibbonMdi.Activities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScheduleManagement.Prism.Activities.SchedulingDriverReconManagement
{
  public partial class SchedulingDriverReconManagement : CustomActivityContext
  {
    public SchedulingDriverReconManagement()
    {
    }

    public SchedulingDriverReconManagement(Activity activity)
      : base(activity)
    {
    }
  }
}

﻿using Afx.Business;
using Afx.Business.Activities;
using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Activities;
using ScheduleManagement.Business;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Afx.Business.Service;
using ScheduleManagement.Business.Service;

namespace ScheduleManagement.Prism.Activities.LoadAllocationsManagement
{
  public partial class LoadAllocationsManagementController : MdiCustomActivityController<LoadAllocationsManagement, LoadAllocationsManagementViewModel>
  {
    #region Constructors

    [InjectionConstructor]
    public LoadAllocationsManagementController(IController controller)
      : base(controller)
    {
    }

    #endregion

    #region OnLoaded
    
    protected override void OnLoaded()
    {
      ViewModel.IsFocused = true;
      base.OnLoaded();
    }

    #endregion
    
    #region ExecuteOperation
    
    protected override void ExecuteOperation(Operation op, BusinessObject argument)
    {

      base.ExecuteOperation(op, argument);
    }
    
    #endregion

    protected override void LoadDataContext()
    {
      try
      {
        using (new WaitCursor())
        using (var svc = ProxyFactory.GetService<IScheduleManagementService>(SecurityContext.ServerName))
        {
          DataContext.Allocations = svc.LoadScheduledAllocations((ViewModel.SelectedDistributionCenter == null ? 0 : ViewModel.SelectedDistributionCenter.Id), (ViewModel.SelectedRoute == null ? 0 : ViewModel.SelectedRoute.Id));
        }
      }
      catch (Exception ex)
      {
        if (ExceptionHelper.HandleException(ex)) throw ex;
      }

      base.LoadDataContext();
    }
  }
}
﻿using Afx.Business.Activities;
using Afx.Business.Data;
using Afx.Prism.RibbonMdi;
using Afx.Prism.RibbonMdi.Ribbon;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScheduleManagement.Prism.Activities.LoadAllocationsManagement
{
  [Export(typeof(IRibbonItem))]
  public class LoadAllocationsManagementButton : RibbonButtonViewModel
  {
    public override string TabName
    {
      get { return ScheduleManagement.Prism.ScheduleManagementTab.TabName; }
    }

    public override string GroupName
    {
      get { return FreightSchedulingGroup.GroupName; }
    }

    public override string ItemIdentifier
    {
      get { return "LoadAllocationsManagement"; }
    }

    public override int Index
    {
      get { return 1; }
    }

    #region Activity Activity

    public Activity Activity
    {
      get { return Cache.Instance.GetObject<Activity>(LoadAllocationsManagement.Key); }
    }

    #endregion

    public bool IsEnabled
    {
      get
      {
        try
        {
          if (Activity == null) return false;
          return Activity.CanView;
        }
        catch
        {
          return false;
        }
      }
    }

    protected override void OnExecute()
    {
      MdiApplicationController.Current.ExecuteActivity(Activity);
    }
  }
}

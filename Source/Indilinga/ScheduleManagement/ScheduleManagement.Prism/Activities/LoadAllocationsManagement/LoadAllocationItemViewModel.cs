﻿using Afx.Prism;
using Microsoft.Practices.Unity;
using ScheduleManagement.Business;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScheduleManagement.Prism.Activities.LoadAllocationsManagement
{
  public class LoadAllocationItemViewModel : SelectableViewModel<LoadAllocation>
  {
    #region Constructors

    [InjectionConstructor]
    public LoadAllocationItemViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    new LoadAllocationsManagementViewModel Parent
    {
      get { return (LoadAllocationsManagementViewModel)base.Parent; }
    }

    public override bool IsSelected
    {
      get
      {
        if (base.IsSelected) Parent.SelectedLoadAllocationItemViewModel = this;
        return base.IsSelected;
      }
      set
      {
        base.IsSelected = value;
        if (base.IsSelected) Parent.SelectedLoadAllocationItemViewModel = this;
      }
    }

    #region string Vehicles

    public const string VehiclesProperty = "Vehicles";
    public string Vehicles
    {
      get
      {
        Collection<string> vehicles = new Collection<string>();
        foreach (var v in Model.Vehicles)
        {
          vehicles.Add(v.Text);
        }
        return string.Join(", ", vehicles);
      }
    }

    #endregion
  }
}

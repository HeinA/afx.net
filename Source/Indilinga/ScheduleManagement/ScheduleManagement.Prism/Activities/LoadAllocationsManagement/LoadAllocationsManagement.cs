﻿using Afx.Business.Activities;
using ScheduleManagement.Business;
using ScheduleManagement.Business.Service;
using Afx.Prism.RibbonMdi.Activities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Afx.Business.Collections;

namespace ScheduleManagement.Prism.Activities.LoadAllocationsManagement
{
  public partial class LoadAllocationsManagement : CustomActivityContext
  {
    public LoadAllocationsManagement()
    {
    }

    public LoadAllocationsManagement(Activity activity)
      : base(activity)
    {
    }

    #region BasicCollection<LoadAllocation> Allocations

    public const string AllocationsProperty = "Allocations";
    BasicCollection<LoadAllocation> mAllocations;
    public BasicCollection<LoadAllocation> Allocations
    {
      get { return mAllocations; }
      set { SetProperty<BasicCollection<LoadAllocation>>(ref mAllocations, value); }
    }

    #endregion
  }
}

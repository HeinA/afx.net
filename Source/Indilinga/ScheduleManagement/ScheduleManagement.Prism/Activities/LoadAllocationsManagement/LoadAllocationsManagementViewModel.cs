﻿using Afx.Business.Security;
using Afx.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Activities;
using ScheduleManagement.Business;
using FreightManagement.Business;
using Afx.Business.Data;
using Microsoft.Practices.Prism.Commands;
using Afx.Business.Service;
using FreightManagement.Business.Service;
using ScheduleManagement.Business.Service;
using Afx.Prism.RibbonMdi;

namespace ScheduleManagement.Prism.Activities.LoadAllocationsManagement
{
  public partial class LoadAllocationsManagementViewModel : MdiCustomActivityViewModel<LoadAllocationsManagement>
  {
    #region Constructors

    [InjectionConstructor]
    public LoadAllocationsManagementViewModel(IController controller)
      : base(controller)
    {
      SelectedDistributionCenter = DistributionCenters.Where(dc => dc.OrganizationalUnit.Equals(SecurityContext.OrganizationalUnit)).FirstOrDefault();
    }

    #endregion

    [Dependency]
    public LoadAllocationsManagementController LoadAllocationsManagementController { get; set; }

    #region DistributionCenter SelectedDistributionCenter

    public const string SelectedDistributionCenterProperty = "SelectedDistributionCenter";
    DistributionCenter mSelectedDistributionCenter;
    public DistributionCenter SelectedDistributionCenter
    {
      get { return mSelectedDistributionCenter; }
      set
      {
        if (SetProperty<DistributionCenter>(ref mSelectedDistributionCenter, value))
        {
          OnPropertyChanged("Routes");
        }
      }
    }

    #endregion

    #region IEnumerable<DistributionCenter> DistributionCenters

    public IEnumerable<DistributionCenter> DistributionCenters
    {
      get { return Cache.Instance.GetObjects<DistributionCenter>(true, r => r.Name, true); }
    }

    #endregion

    #region IEnumerable<Route> Routes

    public IEnumerable<Route> Routes
    {
      get
      {
        if (BusinessObject.IsNull(SelectedDistributionCenter)) return Cache.Instance.GetObjects<Route>(true, r => r.Name, true);
        return Cache.Instance.GetObjects<Route>(true, r => r.Origin.Equals(SelectedDistributionCenter.Location), r => r.Name, true);
      }
    }

    #endregion

    #region Route SelectedRoute

    public const string SelectedRouteProperty = "SelectedRoute";
    Route mSelectedRoute;
    public Route SelectedRoute
    {
      get { return mSelectedRoute; }
      set { SetProperty<Route>(ref mSelectedRoute, value); }
    }

    #endregion

    #region DelegateCommand FilterCommand

    DelegateCommand mFilterCommand;
    public DelegateCommand FilterCommand
    {
      get { return mFilterCommand ?? (mFilterCommand = new DelegateCommand(ExecuteFilter, CanExecuteFilter)); }
    }

    bool CanExecuteFilter()
    {
      return true;
    }

    void ExecuteFilter()
    {
      LoadAllocationsManagementController.Refresh(true);
    }

    #endregion

    #region LoadAllocationItemViewModel SelectedLoadAllocationItemViewModel

    public const string SelectedLoadAllocationItemViewModelProperty = "SelectedLoadAllocationItemViewModel";
    LoadAllocationItemViewModel mSelectedLoadAllocationItemViewModel;
    public LoadAllocationItemViewModel SelectedLoadAllocationItemViewModel
    {
      get { return mSelectedLoadAllocationItemViewModel; }
      set { SetProperty<LoadAllocationItemViewModel>(ref mSelectedLoadAllocationItemViewModel, value); }
    }

    #endregion

    #region DelegateCommand ItemActivatedCommand

    DelegateCommand mItemActivatedCommand;
    public DelegateCommand ItemActivatedCommand
    {
      get { return mItemActivatedCommand ?? (mItemActivatedCommand = new DelegateCommand(ExecuteItemActivated)); }
    }

    void ExecuteItemActivated()
    {
      if (SelectedLoadAllocationItemViewModel != null && SelectedLoadAllocationItemViewModel.Model != null) MdiApplicationController.Current.EditDocument(SelectedLoadAllocationItemViewModel.Model);
    }

    #endregion

    #region IEnumerable<LoadAllocationItemViewModel> LoadAllocations

    public const string LoadAllocationsProperty = "LoadAllocations";
    public IEnumerable<LoadAllocationItemViewModel> LoadAllocations
    {
      get { return ResolveViewModels<LoadAllocationItemViewModel, LoadAllocation>(Model.Allocations); }
    }

    #endregion

    protected override void OnModelPropertyChanged(string propertyName)
    {
      if (propertyName == LoadAllocationsManagement.AllocationsProperty)
      {
        OnPropertyChanged(LoadAllocationsProperty);
      }

      base.OnModelPropertyChanged(propertyName);
    }
  }
}

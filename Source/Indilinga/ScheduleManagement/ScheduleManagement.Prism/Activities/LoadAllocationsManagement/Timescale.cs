﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScheduleManagement.Prism.Activities.LoadAllocationsManagement
{
  public enum Timescale
  {
    Day = 0
    , Month = 1
    , Week = 2
  }
}

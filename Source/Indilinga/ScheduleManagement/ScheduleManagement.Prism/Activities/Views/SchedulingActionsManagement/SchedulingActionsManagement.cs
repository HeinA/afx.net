﻿using Afx.Business.Activities;
using ScheduleManagement.Business;
using ScheduleManagement.Business.Service;
using Afx.Prism.RibbonMdi.Activities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using Afx.Business.Service;
using Afx.Business.Security;

namespace ScheduleManagement.Prism.Activities.Views.SchedulingActionsManagement
{
  public partial class SchedulingActionsManagement : CustomActivityContext
  {
    public SchedulingActionsManagement()
    {
    }

    public SchedulingActionsManagement(Activity activity)
      : base(activity)
    {
    }

    public override bool IsDirty
    {
      get { return false; }
      set { }
    }

    #region Collections

    #region ObservableCollection<SchedulingAction> Items

    public const string ItemsProperty = "Items";
    ObservableCollection<SchedulingAction> mItems;
    public ObservableCollection<SchedulingAction> Items
    {
      get { return mItems; }
      set { SetProperty<ObservableCollection<SchedulingAction>>(ref mItems, value); }
    }

    #endregion

    #endregion


    #region "Filters"
    
    #region string FilterBy

    public const string FilterByProperty = "FilterBy";
    string mFilterBy = "Dashboard";
    public string FilterBy
    {
      get { return mFilterBy; }
      set
      {
        SetProperty(ref mFilterBy, value);
      }
    }

    #endregion
    
    #region DateTime fromDateFilter

    public const string fromDateFilterProperty = "fromDateFilter";
    DateTime mfromDateFilter = DateTime.Today.AddMonths(-1);
    public DateTime fromDateFilter
    {
      get { return mfromDateFilter; }
      set
      {
        if (SetProperty<DateTime>(ref mfromDateFilter, value))
        {
          LoadItems();
        }
      }
    }

    #endregion

    #region string TrailerType

    public const string TrailerTypeProperty = "TrailerType";
    string mTrailerType = "Truck";
    public string TrailerType
    {
      get { return mTrailerType; }
      set {
        if (SetProperty<string>(ref mTrailerType, value))
        {
          OnPropertyChanged("ActionItems"); 
          OnCompositionChanged(Afx.Business.CompositionChangedType.PropertyChanged, "Items", "TrailerType", this);
        }
      }
    }

    #endregion

    #region string FleetNumber

    public const string FleetNumberProperty = "FleetNumber";
    string mFleetNumber = "";
    public string FleetNumber
    {
      get { return mFleetNumber; }
      set 
      { 
        if (SetProperty<string>(ref mFleetNumber, value))
        {
          OnPropertyChanged("ActionItems"); 
          OnCompositionChanged(Afx.Business.CompositionChangedType.PropertyChanged, "Items", "FleetNumber", this);
        }
      }
    }

    #endregion

    #region string Status

    public const string StatusProperty = "Status";
    string mStatus = "";
    public string Status
    {
      get { return mStatus; }
      set
      {
        if (SetProperty<string>(ref mStatus, value))
        {
          OnPropertyChanged("ActionItems");
          OnCompositionChanged(Afx.Business.CompositionChangedType.PropertyChanged, "Items", "Status", this);
        }
      }
    }

    #endregion

    #endregion

    public override void LoadData()
    {
      LoadItems();
      base.LoadData();
    }

    void LoadItems()
    {
      using (var svc = ProxyFactory.GetService<IScheduleManagementService>(SecurityContext.ServerName))
      {
        Items = svc.LoadSchedulingActions(fromDateFilter);
      }
    }
  }
}

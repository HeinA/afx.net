﻿using Afx.Business.Security;
using Afx.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Activities;
using ScheduleManagement.Business;
using System.Collections.ObjectModel;
using Afx.Business.Collections;
using Afx.Business.Data;
using FleetManagement.Business;
using Afx.Business.Service;
using FreightManagement.Business.Service;

namespace ScheduleManagement.Prism.Activities.Views.SchedulingActionsManagement
{
  public partial class SchedulingActionsManagementViewModel : MdiCustomActivityViewModel<SchedulingActionsManagement>
  {
    #region Constructors

    [InjectionConstructor]
    public SchedulingActionsManagementViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    protected override void OnModelCompositionChanged(CompositionChangedEventArgs e)
    {
      switch (e.PropertyName)
      {
        case "Items":
          mActionItems = null;
          OnPropertyChanged("ActionItems");
          break;
        case "TrailerType":
          mActionItems = null;
          OnPropertyChanged("ActionItems");
          break;
        case "FleetNumber":
          mActionItems = null;
          OnPropertyChanged("ActionItems");
          break;
      }
      base.OnModelCompositionChanged(e);
    }

    IEnumerable<SchedulingActionViewModel> mActionItems;
    public IEnumerable<SchedulingActionViewModel> ActionItems
    {
      get
      {
        if (Model == null) return null;
        if (String.IsNullOrWhiteSpace(Model.TrailerType))
        {
          return mActionItems ?? (mActionItems = ResolveViewModels<SchedulingActionViewModel, SchedulingAction>(Model.Items.Where(v => v.FleetNumber.Contains(Model.FleetNumber)).ToList()));
        }
        else
        {
          return mActionItems ?? (mActionItems = ResolveViewModels<SchedulingActionViewModel, SchedulingAction>(Model.Items.Where(v => v.VehicleNotes != null && v.VehicleNotes.Contains(Model.TrailerType)).Where(v => v.FleetNumber.Contains(Model.FleetNumber)).ToList()));
        }
      }
    }

    #region IEnumerable<string> TrailerTypes

    public const string TrailerTypesProperty = "TrailerTypes";
    public IEnumerable<string> TrailerTypes
    {
      get { return Cache.Instance.GetObjects<Vehicle>(true, v => v.IsTrailer = true, true).OrderBy(v => v.Notes).Select(v => v.Notes).Distinct(); }
    }

    #endregion
  }
}

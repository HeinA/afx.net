﻿using Afx.Business;
using Afx.Business.Data;
using Afx.Business.Documents;
using Afx.Business.Security;
using Afx.Business.Service;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using Afx.Prism.RibbonMdi.Events;
using FreightManagement.Business;
using FreightManagement.Business.Service;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Unity;
using ScheduleManagement.Business;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScheduleManagement.Prism.Activities.Views.SchedulingActionsManagement
{
  public class SchedulingActionViewModel : ViewModel<SchedulingAction>
  {
    #region Constructors

    [InjectionConstructor]
    public SchedulingActionViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    public bool IsDeleted
    {
      get { return Model.IsDeleted; }
    }

    public bool HasError
    {
      get { return Model.HasError; }
    }

    public string HorseFleetNumber
    {
      get { return Model.Horse.FleetNumber; }
    }

    #region string NewSoftState

    public const string NewSoftStateProperty = "NewSoftState";
    string mNewSoftState;
    public string NewSoftState
    {
      get 
      {
        string result = "";
        if (ActualOffloadingCompleted != null) result = "Off-Loading Completed";
        else if (ActualOffloadingTime != null) result = "Off-Loading";
        else if (ArrivalTime != null) result = "Arrived Consignee";
        else if (DepartureTime != null) result = "Departed Consigner";
        else if (ActualLoadingCompleted != null) result = "Loading Completed";
        else if (ActualLoadingTime != null) result = "Loading";
        return result; 
      }
      set { SetProperty<string>(ref mNewSoftState, value); }
    }

    #endregion

    #region string NextSoftState

    public const string NextSoftStateProperty = "NextSoftState";
    string mNextSoftState;
    public string NextSoftState
    {
      get
      {
        string result = "Available";
        if (ActualOffloadingCompleted != null) result = "Available";
        else if (ActualOffloadingTime != null) result = "Off-Loading Completed";
        else if (ArrivalTime != null) result = "Off-Loading";
        else if (DepartureTime != null) result = "Arrival at Consignee";
        else if (ActualLoadingCompleted != null) result = "Departed Consigner";
        else if (ActualLoadingTime != null) result = "Loading Completed";
        else if (ActualLoadingTime == null) result = "Loading";
        return result;
      }
    }

    #endregion


    #region DateTime? ActualLoadingTime

    public const string ActualLoadingTimeProperty = "ActualLoadingTime";
    DateTime? mActualLoadingTime;
    public DateTime? ActualLoadingTime
    {
      get { return BusinessObject.IsNull(Model.TransportManifest) ? null : Model.TransportManifest.ActualLoadingTime; }
      set
      {
        if (SetProperty<DateTime?>(ref mActualLoadingTime, value))
        {
          Model.TransportManifest.ActualLoadingTime = value;
          UpdateManifest(EnumTransportManifestDateFields.ActualLoadingTime, value);
        }
      }
    }

    #endregion

    #region DateTime? ActualOffloadingTime

    public const string ActualOffloadingTimeProperty = "ActualOffloadingTime";
    DateTime? mActualOffloadingTime;
    public DateTime? ActualOffloadingTime
    {
      get { return BusinessObject.IsNull(Model.TransportManifest) ? null : Model.TransportManifest.ActualOffloadingTime; }
      set
      {
        if (SetProperty<DateTime?>(ref mActualOffloadingTime, value))
        {
          Model.TransportManifest.ActualOffloadingTime = value;
          UpdateManifest(EnumTransportManifestDateFields.ActualOffloadingTime, value);
        }
      }
    }

    #endregion

    #region DateTime? DepartureTime

    public const string DepartureTimeProperty = "DepartureTime";
    DateTime? mDepartureTime;
    public DateTime? DepartureTime
    {
      get { return BusinessObject.IsNull(Model.TransportManifest) ? null : Model.TransportManifest.DepartureTime; }
      set
      {
        if (SetProperty<DateTime?>(ref mDepartureTime, value))
        {
          Model.TransportManifest.DepartureTime = value;
          UpdateManifest(EnumTransportManifestDateFields.DepartureTime, value);
        }
      }
    }

    #endregion

    #region DateTime? ArrivalTime

    public const string ArrivalTimeProperty = "ArrivalTime";
    DateTime? mArrivalTime;
    public DateTime? ArrivalTime
    {
      get { return BusinessObject.IsNull(Model.TransportManifest) ? null : Model.TransportManifest.ArrivalTime; }
      set
      {
        if (SetProperty<DateTime?>(ref mArrivalTime, value))
        {
          Model.TransportManifest.ArrivalTime = value;
          UpdateManifest(EnumTransportManifestDateFields.ArrivalTime, value);
        }
      }
    }

    #endregion

    #region DateTime? ActualLoadingCompleted

    public const string ActualLoadingCompletedProperty = "ActualLoadingCompleted";
    DateTime? mActualLoadingCompleted;
    public DateTime? ActualLoadingCompleted
    {
      get { return BusinessObject.IsNull(Model.TransportManifest) ? null : Model.TransportManifest.ActualLoadingCompleted; }
      set
      {
        if (SetProperty<DateTime?>(ref mActualLoadingCompleted, value))
        {
          Model.TransportManifest.ActualLoadingCompleted = value;
          UpdateManifest(EnumTransportManifestDateFields.ActualLoadingCompleted, value);
        }
      }
    }

    #endregion

    #region DateTime? ActualOffloadingCompleted

    public const string ActualOffloadingCompletedProperty = "ActualOffloadingCompleted";
    DateTime? mActualOffloadingCompleted;
    public DateTime? ActualOffloadingCompleted
    {
      get { return BusinessObject.IsNull(Model.TransportManifest) ? null : Model.TransportManifest.ActualOffloadingCompleted; }
      set
      {
        if (SetProperty<DateTime?>(ref mActualOffloadingCompleted, value))
        {
          Model.TransportManifest.ActualOffloadingCompleted = value;
          UpdateManifest(EnumTransportManifestDateFields.ActualOffloadingCompleted, value);
        }
      }
    }

    #endregion


    #region DelegateCommand SetDateCommand

    DelegateCommand<SchedulingActionViewModel> mSetDateCommand;
    public DelegateCommand<SchedulingActionViewModel> SetDateCommand
    {
      get { return mSetDateCommand ?? (mSetDateCommand = new DelegateCommand<SchedulingActionViewModel>(ExecuteSetDate, CanExecuteSetDate)); }
    }

    bool CanExecuteSetDate(SchedulingActionViewModel vm)
    {
      return !BusinessObject.IsNull(Model.TransportManifest);
    }

    void ExecuteSetDate(SchedulingActionViewModel vm)
    {
      switch (vm.NextSoftState)
      {
        case "Available":
          break;
        case "Off-Loading Completed":
          ActualOffloadingCompleted = DateTime.Now;
          break;
        case "Off-Loading":
          ActualOffloadingTime = DateTime.Now;
          break;
        case "Arrival at Consignee":
          ArrivalTime = DateTime.Now;
          break;
        case "Departed Consigner":
          DepartureTime = DateTime.Now;
          break;
        case "Loading Completed":
          ActualLoadingCompleted = DateTime.Now;
          break;
        case "Loading":
          ActualLoadingTime = DateTime.Now;
          break;
      }
      OnPropertyChanged("NewSoftState");
      OnPropertyChanged("NextSoftState");

    }

    #endregion

    #region DelegateCommand OpenManifestCommand

    DelegateCommand<SchedulingActionViewModel> mOpenManifestCommand;
    public DelegateCommand<SchedulingActionViewModel> OpenManifestCommand
    {
      get { return mOpenManifestCommand ?? (mOpenManifestCommand = new DelegateCommand<SchedulingActionViewModel>(ExecuteOpenManifest, CanExecuteOpenManifest)); }
    }

    bool CanExecuteOpenManifest(SchedulingActionViewModel args)
    {
      return true;
    }

    void ExecuteOpenManifest(SchedulingActionViewModel args)
    {
      MdiApplicationController.Current.EventAggregator.GetEvent<EditDocumentEvent>().Publish(new EditDocumentEventArgs(Cache.Instance.GetObject<DocumentType>(TransportManifest.DocumentTypeIdentifier), args.Model.TransportManifest.GlobalIdentifier));
    }

    #endregion



    private async void UpdateManifest(EnumTransportManifestDateFields whichDate, DateTime? date)
    {
      try
      {
        NewSoftState = await UpdateManifestAsync(whichDate, date);
        OnPropertyChanged(NewSoftStateProperty);
      }
      catch (Exception ex)
      {
        ExceptionHelper.HandleException(ex);
      }
    }

    async Task<string> UpdateManifestAsync(EnumTransportManifestDateFields whichDate, DateTime? date)
    {
      await Task.Run(() =>
      {
        using (var svc = ProxyFactory.GetService<IFreightManagementService>(SecurityContext.ServerName))
        {
          NewSoftState = svc.SaveTransportManifestDates(Model.TransportManifest.GlobalIdentifier, whichDate, date).ToString();
        }
      });

      return NewSoftState;
    }
  }
}

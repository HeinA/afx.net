﻿using ScheduleManagement.Business;
using ScheduleManagement.Business.Service;
using Afx.Business.Service;
using Afx.Prism.RibbonMdi.Activities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.Composition;
using Afx.Business.Security;

namespace ScheduleManagement.Prism.Activities.Views.SchedulingActionsManagement
{
  //**************************************************************************************************************
  //**************************************************************************************************************
  //*                                                                                                            *
  //*     This code is auto-generated.  Do not place any cutom code here, it will be lost on re-generation.      *
  //*                                                                                                            *
  //**************************************************************************************************************
  //**************************************************************************************************************
  [Export("Context:" + global::ScheduleManagement.Prism.Activities.Views.SchedulingActionsManagement.SchedulingActionsManagement.Key)]
  public partial class SchedulingActionsManagement
  {
    public const string Key = "{c78b96c4-2821-45db-964d-677620958b78}";
  }
}

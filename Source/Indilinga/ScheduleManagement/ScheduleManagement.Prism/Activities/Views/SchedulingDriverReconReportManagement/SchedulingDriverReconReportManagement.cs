﻿using Afx.Business.Activities;
using ScheduleManagement.Business;
using ScheduleManagement.Business.Service;
using Afx.Prism.RibbonMdi.Activities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using ContactManagement.Business;
using Afx.Business.Collections;
using Afx.Business;
using Afx.Business.Service;
using Afx.Business.Security;
using AccountManagement.Business;
using FreightManagement.Business;
using Afx.Business.Data;
using FleetManagement.Business;
using System.ComponentModel;

namespace ScheduleManagement.Prism.Activities.Views.SchedulingDriverReconReportManagement
{
  public partial class SchedulingDriverReconReportManagement : CustomActivityContext
  {

    #region Constructors

    public SchedulingDriverReconReportManagement()
    {
    }

    public SchedulingDriverReconReportManagement(Activity activity)
      : base(activity)
    {
    }

    #endregion

    public override bool IsDirty
    {
      get { return false; }
      set { }
    }

    #region Containers

    #region Collection<ManifestReference> Schedules

    public const string SchedulesProperty = "Schedules";
    BasicCollection<VehicleSchedule> mSchedules;
    public BasicCollection<VehicleSchedule> Schedules
    {
      get { return mSchedules; }
      set
      {
        if (SetProperty<BasicCollection<VehicleSchedule>>(ref mSchedules, value))
        {
          LoadPalettes();
          IEnumerable<Vehicle> UniqueVehicles = mSchedules.Select(s => s.Vehicle).Distinct();
          IEnumerable<Contact> UniqueDrivers = mSchedules.Select(s => s.Driver).Distinct();
          IEnumerable<DateTime> UniqueTrips = mSchedules.Select(s => (DateTime)s.DocumentDate).Distinct();
          Series.Clear();
          switch (FilterBy)
          { 
            case "By Driver":
              Collection<DriverContainer> drivers = new Collection<DriverContainer>();
              IEnumerable<VehicleSchedule> tempSchedules;

              foreach (var driver in UniqueDrivers)
              {
                if (BusinessObject.IsNull(driver))
                {
                  tempSchedules = mSchedules.Where(mw => mw.Driver == null && (mw.Vehicle != null && mw.Vehicle.IsTruck));
                  if (tempSchedules.Count() != 0) drivers.Add(new DriverContainer(new SimpleContact(true), tempSchedules, FilterBy));
                }
                else
                {
                  tempSchedules = mSchedules.Where(c => !BusinessObject.IsNull(c.Driver) && c.Driver.Equals(driver));
                  drivers.Add(new DriverContainer(driver, mSchedules.Where(c => !BusinessObject.IsNull(c.Driver) && c.Driver.Equals(driver)), FilterBy));
                }
              }




              //if (mSchedules.Where(mw => mw.Driver == null).DefaultIfEmpty() != null)
              //{
              //}

              //var varDriverResults = mSchedules.Where(mw => mw.Driver != null && (mw.Vehicle != null && mw.Vehicle.IsTruck)).Select(c => c.Driver).Distinct().OrderBy(r => r.Fullname);
              //if (varDriverResults != null && varDriverResults.DefaultIfEmpty() != null)
              //{
              //  foreach (var driver in varDriverResults)
              //  {
              //    if (!BusinessObject.IsNull(driver))
              //    {
              //      drivers.Add(new DriverContainer(driver, mSchedules.Where(c => !BusinessObject.IsNull(c.Driver) && c.Driver.Equals(driver)), FilterBy));
              //    }
              //    else
              //    {
              //      drivers.Add(new DriverContainer(driver, mSchedules.Where(mw => BusinessObject.IsNull(mw.Driver)), FilterBy));
              //    }
              //  }
              //}
              Drivers = drivers;

              //ChartTitle = "Fees per Driver";
              //ChartSubTitle = "";


              //ObservableCollection<SeriesData> tempSeries2 = new ObservableCollection<SeriesData>();
              //ObservableCollection<SeriesDataKeyValuePair> newSeriesItem2 = new ObservableCollection<SeriesDataKeyValuePair>();
              
              //SeriesSource.Clear();
              //tempSeries2.Clear();
              //newSeriesItem2 = new ObservableCollection<SeriesDataKeyValuePair>();
              //foreach (var group in mSchedules.Select(s => s.Driver).Distinct())
              //{
              //  if (group == null)
              //  {
              //    newSeriesItem2.Add(new SeriesDataKeyValuePair() { Category = "Unassigned driver", Number = mSchedules.Where(v => BusinessObject.IsNull(v.Driver)).Sum(s => s.WaybillCharge) });
              //  }
              //  else
              //  {
              //    newSeriesItem2.Add(new SeriesDataKeyValuePair() { Category = (String.IsNullOrEmpty(group.Fullname) ? "Unassigned driver" : group.Fullname), Number = mSchedules.Where(v => v.Driver != null && v.Driver.Equals(group)).Sum(s => s.WaybillCharge) });
              //  }
              //}
              //SeriesData newSeries2 = new SeriesData() { SeriesDisplayName="Income", Items = newSeriesItem2 };
              //tempSeries2.Add(newSeries2);
              
              //newSeriesItem2 = new ObservableCollection<SeriesDataKeyValuePair>();
              //foreach (var group in mSchedules.Select(s => s.Driver).Distinct())
              //{
              //  if (group == null)
              //  {
              //    newSeriesItem2.Add(new SeriesDataKeyValuePair() { Category = "Unassigned driver", Number = mSchedules.Where(v => BusinessObject.IsNull(v.Driver)).Count() });
              //  }
              //  else
              //  {
              //    newSeriesItem2.Add(new SeriesDataKeyValuePair() { Category = (String.IsNullOrEmpty(group.Fullname) ? "Unassigned driver" : group.Fullname), Number = mSchedules.Where(v => v.Driver != null && v.Driver.Equals(group)).Count() });
              //  }
              //}
              //newSeries2 = new SeriesData() { SeriesDisplayName="Trips", Items = newSeriesItem2 };
              //tempSeries2.Add(newSeries2);
              
              //newSeriesItem2 = new ObservableCollection<SeriesDataKeyValuePair>();
              //foreach (var group in mSchedules.Select(s => s.Driver).Distinct())
              //{
              //  if (group == null)
              //  {
              //    newSeriesItem2.Add(new SeriesDataKeyValuePair() { Category = "Unassigned driver", Number = mSchedules.Where(v => BusinessObject.IsNull(v.Driver)).Average(s => (decimal)((TimeSpan)((DateTime)s.RenderOffloadingDateTime - (DateTime)s.RenderLoadingDateTime)).TotalHours) });
              //  }
              //  else
              //  {
              //    newSeriesItem2.Add(new SeriesDataKeyValuePair() { Category = (String.IsNullOrEmpty(group.Fullname) ? "Unassigned driver" : group.Fullname), Number = mSchedules.Where(v => v.Driver != null && v.Driver.Equals(group)).Average(s => (decimal)((TimeSpan)((DateTime)s.RenderOffloadingDateTime - (DateTime)s.RenderLoadingDateTime)).TotalHours) });
              //  }
              //}
              //newSeries2 = new SeriesData() { SeriesDisplayName="Average Trip Hours", Items = newSeriesItem2 };
              //tempSeries2.Add(newSeries2);

              //Series = tempSeries2;
              break;
            case "By Horse":
              Collection<VehicleContainer> vehicles = new Collection<VehicleContainer>();
              if (mSchedules.Where(mw => mw.Vehicle == null).DefaultIfEmpty() != null)
              {
                vehicles.Add(new VehicleContainer(new Vehicle(true), mSchedules.Where(mw => mw.Vehicle == null && (mw.Vehicle != null && mw.Vehicle.IsTruck)), FilterBy));
              }

              var varVehicleResults = mSchedules.Where(mw => mw.Vehicle != null && (mw.Vehicle != null && mw.Vehicle.IsTruck)).Select(c => c.Vehicle).Distinct().OrderBy(r => r.Text);
              if (varVehicleResults != null && varVehicleResults.DefaultIfEmpty() != null)
              {
                foreach (var vehicle in varVehicleResults)
                {
                  if (!BusinessObject.IsNull(vehicle))
                  {
                    vehicles.Add(new VehicleContainer(vehicle, mSchedules.Where(c => !BusinessObject.IsNull(c.Vehicle) && c.Vehicle.Equals(vehicle)), FilterBy));
                  }
                  else
                  {
                    vehicles.Add(new VehicleContainer(vehicle, mSchedules.Where(mw => BusinessObject.IsNull(mw.Vehicle)), FilterBy));
                  }
                }
              }
              Vehicles = vehicles;

              //ChartTitle = "Fees per Vehicle";
              //ChartSubTitle = "";


              
              //ObservableCollection<SeriesData> tempSeries3 = new ObservableCollection<SeriesData>();
              //ObservableCollection<SeriesDataKeyValuePair> newSeriesItem3 = new ObservableCollection<SeriesDataKeyValuePair>();
              //SeriesSource.Clear();
              //tempSeries3.Clear();
              //newSeriesItem3 = new ObservableCollection<SeriesDataKeyValuePair>();
              //foreach (var group in mSchedules.Select(s => s.Vehicle).Distinct())
              //{
              //  newSeriesItem3.Add(new SeriesDataKeyValuePair() { Category = (String.IsNullOrEmpty(group.Text) ? "Unassigned vehicle" : group.Text), Number = mSchedules.Where(v => v.Vehicle.Equals(group)).Sum(s => s.WaybillCharge) });
              //}
              //SeriesData newSeries3 = new SeriesData() { SeriesDisplayName="Income", Items = newSeriesItem3 };
              //tempSeries3.Add(newSeries3);
              
              //newSeriesItem3 = new ObservableCollection<SeriesDataKeyValuePair>();
              //foreach (var group in mSchedules.Select(s => s.Vehicle).Distinct())
              //{
              //  newSeriesItem3.Add(new SeriesDataKeyValuePair() { Category = (String.IsNullOrEmpty(group.Text) ? "Unassigned vehicle" : group.Text), Number = mSchedules.Where(v => v.Vehicle.Equals(group)).Sum(s => s.WaybillWeight) });
              //}
              //newSeries3 = new SeriesData() { SeriesDisplayName="Weight", Items = newSeriesItem3 };
              //tempSeries3.Add(newSeries3);

              //Series = tempSeries3;
              break;
            case "By Trip":
              Collection<TripContainer> trips = new Collection<TripContainer>();
              foreach (var date in UniqueTrips)
              {
                tempSchedules = mSchedules.Where(c => c.ScheduledLoadingDate.Equals(date));
                trips.Add(new TripContainer(date, mSchedules.Where(c => c.ScheduledLoadingDate.Equals(date)), FilterBy));
              }
              break;
            case "By Client":
              //Collection<AccountContainer> accounts = new Collection<AccountContainer>();
              //if (mSchedules.Where(mw => mw.Accounts == null).DefaultIfEmpty() != null)
              //{
              //  accounts.Add(new AccountContainer(new AccountReference(true), mSchedules.Where(mw => mw.Accounts == null)));
              //}

              //var varAccountResults = mSchedules.Where(mw => mw.Accounts != null).Select(c => c.Accounts);

              //if (varAccountResults != null && varAccountResults.DefaultIfEmpty() != null)
              //{
              //  foreach (var account in varAccountResults)
              //  {
              //    foreach (var subAccount in account)
              //    {
              //      if (!BusinessObject.IsNull(subAccount))
              //      {
              //        accounts.Add(new AccountContainer(subAccount, mSchedules.Where(c => c.Accounts != null && c.Accounts.Contains(subAccount))));
              //      }
              //      else
              //      {
              //        accounts.Add(new AccountContainer(subAccount, mSchedules.Where(mw => mw.Accounts == null)));
              //      }
              //    }
              //  }
              //}
              //Accounts = accounts;
              break;
            case "By Dashboard":


              //ObservableCollection<SeriesData> tempSeries = new ObservableCollection<SeriesData>();
              //SeriesSource.Clear();
              //tempSeries.Clear();
              //foreach (var group in mSchedules.Select(s => s.Vehicle).Distinct())
              //{

              //  ObservableCollection<SeriesDataKeyValuePair> newSeriesItem = new ObservableCollection<SeriesDataKeyValuePair>();
              //  newSeriesItem.Add(new SeriesDataKeyValuePair() { Category = "Income", Number = mSchedules.Where(v => v.Vehicle.Equals(group)).Sum(s => s.WaybillCharge) });
              //  newSeriesItem.Add(new SeriesDataKeyValuePair() { Category = "Weight", Number = mSchedules.Where(v => v.Vehicle.Equals(group)).Sum(s => s.WaybillWeight) });
              //  //newSeries.Items.Add(new SeriesDataKeyValuePair() { Category = "Tarpaullin Fees", Number = mSchedules.Where(v => v.Vehicle.Equals(group)).Sum(s => s.TarpaullinFees) });
              //  //newSeries.Items.Add(new SeriesDataKeyValuePair() { Category = "Night Out Fees", Number=mSchedules.Where(v => v.Vehicle.Equals(group)).Sum(s => s.NightOutFees) });
              //  //newSeries.Items.Add(new SeriesDataKeyValuePair() { Category = "Toll Fees", Number=mSchedules.Where(v => v.Vehicle.Equals(group)).Sum(s => s.TollFees) });


              //  SeriesData newSeries = new SeriesData() { SeriesDisplayName=(String.IsNullOrEmpty(group.Text) ? "Blank" : group.Text), Items = newSeriesItem };
              //  tempSeries.Add(newSeries);
              //}

              //Series = tempSeries;

              break;
          }
        }
      }
    }

    #endregion

    #region Collection<DriverContainer> Drivers

    Collection<DriverContainer> mDrivers;
    public Collection<DriverContainer> Drivers
    {
      get { return mDrivers; }
      set { SetProperty<Collection<DriverContainer>>(ref mDrivers, value); }
    }

    #endregion

    #region Collection<VehicleContainer> Vehicles

    Collection<VehicleContainer> mVehicles;
    public Collection<VehicleContainer> Vehicles
    {
      get { return mVehicles; }
      set { SetProperty<Collection<VehicleContainer>>(ref mVehicles, value); }
    }

    #endregion

    #region Collection<VehicleContainer> Accounts

    Collection<AccountContainer> mAccounts;
    public Collection<AccountContainer> Accounts
    {
      get { return mAccounts; }
      set { SetProperty<Collection<AccountContainer>>(ref mAccounts, value); }
    }

    #endregion

    #region Collection<VehicleContainer> Trips

    Collection<TripContainer> mTrips;
    public Collection<TripContainer> Trips
    {
      get { return mTrips; }
      set { SetProperty<Collection<TripContainer>>(ref mTrips, value); }
    }

    #endregion


    //*******************************************************************************************************************************************
    //*******************************************************************************************************************************************
    //**  CHART
    //*******************************************************************************************************************************************
    //*******************************************************************************************************************************************
    #region Chart

    #region Chart Source Data Collections

    #region IEnumerable<SeriesDataKeyValuePair> SeriesSource

    public const string SeriesSourceProperty = "SeriesSource";
    BasicCollection<SeriesDataKeyValuePair> mSeriesSource = new BasicCollection<SeriesDataKeyValuePair>();
    public BasicCollection<SeriesDataKeyValuePair> SeriesSource
    {
      get { return mSeriesSource; }
      set { SetProperty<BasicCollection<SeriesDataKeyValuePair>>(ref mSeriesSource, value); }
    }

    #endregion

    #region BasicCollection<SeriesData> Series

    public const string SeriesProperty = "Series";
    ObservableCollection<SeriesData> mSeries = new ObservableCollection<SeriesData>();
    public ObservableCollection<SeriesData> Series
    {
      get { return mSeries; }
      set { SetProperty<ObservableCollection<SeriesData>>(ref mSeries, value); }
    }

    #endregion

    #endregion

    #region Properties

    #region string ChartTitle

    public const string ChartTitleProperty = "ChartTitle";
    string mChartTitle;
    public string ChartTitle
    {
      get { return mChartTitle; }
      set { SetProperty<string>(ref mChartTitle, value); }
    }

    #endregion

    #region string ChartSubTitle

    public const string ChartSubTitleProperty = "ChartSubTitle";
    string mChartSubTitle;
    public string ChartSubTitle
    {
      get { return mChartSubTitle; }
      set { SetProperty<string>(ref mChartSubTitle, value); }
    }

    #endregion

    #region object SelectedItem

    public const string SelectedItemProperty = "SelectedItem";
    object mSelectedItem;
    public object SelectedItem
    {
      get { return mSelectedItem; }
      set { SetProperty<object>(ref mSelectedItem, value); }
    }

    #endregion

    #region object SelectedPalette

    public const string SelectedPaletteProperty = "SelectedPalette";
    object mSelectedPalette;
    public object SelectedPalette
    {
      get { return mSelectedPalette; }
      set { SetProperty<object>(ref mSelectedPalette, value); }
    }

    #endregion

    #endregion

    #region Palettes

    public Dictionary<string, Afx.Prism.Controls.Charts.ResourceDictionaryCollection> Palettes { get; set; }

    private void LoadPalettes()
    {
      Palettes = new Dictionary<string, Afx.Prism.Controls.Charts.ResourceDictionaryCollection>();
      Palettes.Add("Default", null);

      SelectedPalette = Palettes.FirstOrDefault();
    }

    #endregion

    #endregion

    #endregion



    #region "Filters"

    #region DistributionCenter DistributionCenter

    public const string DistributionCenterProperty = "DistributionCenter";
    DistributionCenter mDistributionCenter = Cache.Instance.GetObjects<DistributionCenter>(dc => dc.OrganizationalUnit.Equals(SecurityContext.OrganizationalUnit)).FirstOrDefault();
    public DistributionCenter DistributionCenter
    {
      get { return mDistributionCenter; }
      set
      {
        if (SetProperty<DistributionCenter>(ref mDistributionCenter, value))
        {
          LoadSchedules();
        }
      }
    }

    #endregion

    #region Route Route

    public const string RouteProperty = "Route";
    Route mRoute;
    public Route Route
    {
      get { return mRoute; }
      set
      {
        if (SetProperty<Route>(ref mRoute, value))
        {
          LoadSchedules();
        }
      }
    }

    #endregion

    #region AccountReference Account

    public const string AccountProperty = "Account";
    AccountReference mAccount;
    /// <summary>
    /// Account reference object property.
    /// </summary>
    public AccountReference Account
    {
      get { return mAccount; }
      set
      {
        if (SetProperty<AccountReference>(ref mAccount, value))
        {
          LoadSchedules();
        }
      }
    }

    #endregion

    #region Employee Driver

    public const string DriverProperty = "Driver";
    Employee mDriver;
    public Employee Driver
    {
      get { return mDriver; }
      set
      {
        if (SetProperty<Employee>(ref mDriver, value))
        {
          LoadSchedules();
        }
      }
    }

    #endregion

    #region Vehicle Horse

    public const string HorseProperty = "Horse";
    Vehicle mHorse;
    public Vehicle Horse
    {
      get { return mHorse; }
      set
      {
        if (SetProperty<Vehicle>(ref mHorse, value))
        {
          LoadSchedules();
        }
      }
    }

    #endregion


    #region string FilterBy

    public const string FilterByProperty = "FilterBy";
    string mFilterBy = "Dashboard";
    public string FilterBy
    {
      get { return mFilterBy; }
      set
      {
        if (SetProperty(ref mFilterBy, value))
        {
          //FloorStatusManagementController.Refresh();
        }
      }
    }

    #endregion


    #region DateTime fromDateFilter

    public const string fromDateFilterProperty = "fromDateFilter";
    DateTime mfromDateFilter = DateTime.Today.AddMonths(-1);
    public DateTime fromDateFilter
    {
      get { return mfromDateFilter; }
      set
      {
        if (SetProperty<DateTime>(ref mfromDateFilter, value))
        {
          LoadSchedules();
        }
      }
    }

    #endregion

    #region DateTime toDateFilter

    public const string toDateFilterProperty = "toDateFilter";
    DateTime mtoDateFilter = DateTime.Today.AddDays(1);
    public DateTime toDateFilter
    {
      get { return mtoDateFilter; }
      set
      {
        if (SetProperty<DateTime>(ref mtoDateFilter, value))
        {
          LoadSchedules();
        }
      }
    }

    #endregion

    #endregion

    public override void LoadData()
    {
      LoadSchedules();
      base.LoadData();
    }

    void LoadSchedules()
    {
      using (var svc = ProxyFactory.GetService<IScheduleManagementService>(SecurityContext.ServerName))
      {
        Schedules = svc.LoadReconVehicleSchedules(fromDateFilter, toDateFilter, BusinessObject.IsNull(Driver) && Driver == null ? 0 : Driver.Id, BusinessObject.IsNull(Horse) && Horse == null ? 0 : Horse.Id, BusinessObject.IsNull(Account) && Account == null ? "" : Account.Name);
      }
    }

    protected override void OnPropertyChanged(string propertyName)
    {
      switch (propertyName)
      {
        case VehicleSchedule.IsCheckedProperty:
          OnPropertyChanged("IsChecked");
          break;
      }
      base.OnPropertyChanged(propertyName);
    }
  }

  /// <summary>
  /// Sub Class - Series
  /// </summary>
  public class SeriesData
  {
    public string SeriesDisplayName { get; set; }

    public string SeriesDescription { get; set; }

    public ObservableCollection<SeriesDataKeyValuePair> Items { get; set; }
  }

  /// <summary>
  /// Sub Class - Series Data Key Value Pair
  /// </summary>
  public class SeriesDataKeyValuePair : INotifyPropertyChanged
  {
    public string Category { get; set; }

    private decimal _number = 0;
    public decimal Number
    {
      get
      {
        return _number;
      }
      set
      {
        _number = value;
        if (PropertyChanged != null)
        {
          PropertyChanged(this, new PropertyChangedEventArgs("Number"));
        }
      }

    }

    public event PropertyChangedEventHandler PropertyChanged;
  }
}

﻿using ScheduleManagement.Business;
using ScheduleManagement.Business.Service;
using Afx.Business.Service;
using Afx.Prism.RibbonMdi.Activities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.Composition;
using Afx.Business.Security;

namespace ScheduleManagement.Prism.Activities.Views.SchedulingDriverReconReportManagement
{
  //**************************************************************************************************************
  //**************************************************************************************************************
  //*                                                                                                            *
  //*     This code is auto-generated.  Do not place any cutom code here, it will be lost on re-generation.      *
  //*                                                                                                            *
  //**************************************************************************************************************
  //**************************************************************************************************************
  [Export("Context:" + global::ScheduleManagement.Prism.Activities.Views.SchedulingDriverReconReportManagement.SchedulingDriverReconReportManagement.Key)]
  public partial class SchedulingDriverReconReportManagement
  {
    public const string Key = "{e1753a08-caf4-4830-b120-6fa9ba0b0634}";
  }
}

﻿using AccountManagement.Business;
using Afx.Business;
using Afx.Business.Collections;
using ScheduleManagement.Business;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScheduleManagement.Prism.Activities.Views.SchedulingDriverReconReportManagement
{
  public class AccountContainer : BusinessObject
  {

    #region Constructors

    public AccountContainer(AccountReference account, IEnumerable<VehicleSchedule> collection)
    {
      Account = account;
      Schedules = collection;
    }

    #endregion

    #region AccountReference Account

    public const string AccountProperty = "Account";
    AccountReference mAccount;
    public AccountReference Account
    {
      get { return mAccount; }
      set { SetProperty<AccountReference>(ref mAccount, value); }
    }

    #endregion

    #region string FilterBy

    public const string FilterByProperty = "FilterBy";
    string mFilterBy = "";
    public string FilterBy
    {
      get { return mFilterBy; }
      private set { SetProperty<string>(ref mFilterBy, value); }
    }

    #endregion

    //#region Account Account

    //public const string AccountProperty = "Account";
    //AccountReference mAccount;
    //public AccountReference Account
    //{
    //  get { return mAccount; }
    //  set { SetProperty<AccountReference>(ref mAccount, value); }
    //}

    //#endregion

    //#region AccountReference Account

    //public const string AccountProperty = "Account";
    //public AccountReference Account
    //{
    //  get { return GetCachedObject<AccountReference>(); }
    //  set { SetCachedObject<AccountReference>(value); }
    //}

    //#endregion


    #region IEnumerable<VehicleSchedule> Schedules

    public const string SchedulesProperty = "Schedules";
    IEnumerable<VehicleSchedule> mSchedules;
    public IEnumerable<VehicleSchedule> Schedules
    {
      get { return mSchedules; }
      private set
      {
        mSchedules = value;

        try
        {
          switch (FilterBy)
          {
            case "By Client":
              Collection<DriverContainer> drivers = new Collection<DriverContainer>();

              var varResults = (BusinessObject.IsNull(Account) ? mSchedules.Where(mw => mw.Accounts == null).Select(c => c.Driver).Distinct().OrderBy(r => r.Fullname) : mSchedules.Where(mw => mw.Accounts.Contains(Account)).Select(c => c.Driver).Distinct().OrderBy(r => r.Fullname));
              if (varResults != null && varResults.DefaultIfEmpty() != null)
              {
                foreach (var driver in varResults)
                {
                  if (!BusinessObject.IsNull(driver))
                  {
                    drivers.Add(new DriverContainer(driver, mSchedules.Where(mw => mw.Driver.Equals(driver))));
                  }
                  else
                  {
                    drivers.Add(new DriverContainer(driver, mSchedules));
                  }
                }
              }
              Drivers = drivers;
              break;
            case "By Horse":
              break;
            case "By Trip":
              break;
          }
        }
        catch
        {
          throw;
        }
      }
    }

    #endregion

    #region IEnumerable<DriverContainer> Drivers

    public const string DriversProperty = "Drivers";
    IEnumerable<DriverContainer> mDrivers;
    public IEnumerable<DriverContainer> Drivers
    {
      get { return mDrivers; }
      set { SetProperty<IEnumerable<DriverContainer>>(ref mDrivers, value); }
    }

    #endregion

    #region decimal TotalIncome

    public const string TotalIncomeProperty = "TotalIncome";
    decimal mTotalIncome;
    public decimal TotalIncome
    {
      get
      {

        return Schedules.Where(mw => (mw.Vehicle != null && mw.Vehicle.IsTruck)).Sum(s => s.WaybillCharge);
      }
    }

    #endregion

    #region decimal TotalWaybills

    public const string TotalWaybillsProperty = "TotalWaybills";
    decimal mTotalWaybills;
    public decimal TotalWaybills
    {
      get { return Schedules.Where(mw => (mw.Vehicle != null && mw.Vehicle.IsTruck)).Sum(s => s.WaybillCount); }
    }

    #endregion

    #region decimal TotalWaybillWeight

    public const string TotalWaybillWeightProperty = "TotalWaybillWeight";
    decimal mTotalWaybillWeight;
    public decimal TotalWaybillWeight
    {
      get { return Schedules.Where(mw => (mw.Vehicle != null && mw.Vehicle.IsTruck)).Sum(s => s.WaybillWeight); }
    }

    #endregion

  }
}

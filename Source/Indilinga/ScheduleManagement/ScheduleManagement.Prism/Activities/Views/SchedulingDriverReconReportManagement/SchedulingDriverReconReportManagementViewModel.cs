﻿using Afx.Business.Security;
using Afx.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Activities;
using ScheduleManagement.Business;
using FleetManagement.Business;
using Afx.Business.Data;
using FreightManagement.Business;
using AccountManagement.Business;
using ContactManagement.Business;

namespace ScheduleManagement.Prism.Activities.Views.SchedulingDriverReconReportManagement
{
  public partial class SchedulingDriverReconReportManagementViewModel : MdiCustomActivityViewModel<SchedulingDriverReconReportManagement>
  {
    #region Constructors

    [InjectionConstructor]
    public SchedulingDriverReconReportManagementViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion


    #region IEnumerable<DistributionCenter> DistributionCenters

    public IEnumerable<DistributionCenter> DistributionCenters
    {
      get { return Cache.Instance.GetObjects<DistributionCenter>(true, dc => dc.Name, true); }
    }

    #endregion

    #region IEnumerable<Route> Routes

    public IEnumerable<Route> Routes
    {
      get { return Cache.Instance.GetObjects<Route>(true, r => r.Owner.Equals(this.Model.DistributionCenter), r => r.Name, true); }
    }

    #endregion

    #region IEnumerable<AccountReference> Accounts

    public IEnumerable<AccountReference> Accounts
    {
      get
      {
        ///, a => a.OrganizationalUnit.Equals(SecurityContext.OrganizationalUnit)
        return Cache.Instance.GetObjects<AccountReference>(true, a => a.Name, true).ToList();
      }
    }

    #endregion

    #region IEnumerable<Vehicle> Vehicles

    public IEnumerable<Vehicle> Vehicles
    {
      get { return Cache.Instance.GetObjects<Vehicle>(true, v => v.GetExtensionObject<VehicleFreightDetails>().ShowInFloorStatusLoadList, v => v.Text, true); }
    }

    #endregion

    #region IEnumerable<Employee> Drivers

    public IEnumerable<Employee> Drivers
    {
      get { return Cache.Instance.GetObjects<Employee>(true, v => v.Fullname, true); }
    }

    #endregion

    public void RefreshRoutes()
    {
      OnPropertyChanged("Routes");
    }

    protected override void OnModelPropertyChanged(string propertyName)
    {
      switch (propertyName)
      {
        case "DistributionCenter":
          OnPropertyChanged("Routes");
          break;
        case "Account":
          OnPropertyChanged("Accounts");
          break;
        case "Vehicle":
          OnPropertyChanged("Vehicles");
          break;
        case "Driver":
          OnPropertyChanged("Drivers");
          break;
        //case "VehicleSummary":
        //  OnPropertyChanged("Vehicles");
        //  break;
      }

      base.OnModelPropertyChanged(propertyName);
    }
  }
}

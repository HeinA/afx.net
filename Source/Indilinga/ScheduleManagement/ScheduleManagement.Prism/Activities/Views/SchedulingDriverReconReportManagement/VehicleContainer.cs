﻿using Afx.Business;
using Afx.Business.Data;
using Afx.Business.Documents;
using Afx.Prism.RibbonMdi;
using Afx.Prism.RibbonMdi.Events;
using FleetManagement.Business;
using FreightManagement.Business;
using Microsoft.Practices.Prism.Commands;
using ScheduleManagement.Business;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScheduleManagement.Prism.Activities.Views.SchedulingDriverReconReportManagement
{
  public class VehicleContainer : BusinessObject
  {

    #region Constructors

    public VehicleContainer(Vehicle vehicle, IEnumerable<VehicleSchedule> collection, string filterby = "")
    {
      Vehicle = vehicle;
      Schedules = collection;
      FilterBy = filterby;
    }

    #endregion

    #region string FilterBy

    public const string FilterByProperty = "FilterBy";
    string mFilterBy = "";
    public string FilterBy
    {
      get { return mFilterBy; }
      private set { SetProperty<string>(ref mFilterBy, value); }
    }

    #endregion

    #region Vehicle Vehicle

    public const string VehicleProperty = "Vehicle";
    Vehicle mVehicle;
    public Vehicle Vehicle
    {
      get { return mVehicle; }
      set { SetProperty<Vehicle>(ref mVehicle, value); }
    }

    #endregion

    #region IEnumerable<VehicleSchedule> Schedules

    public const string SchedulesProperty = "Schedules";
    IEnumerable<VehicleSchedule> mSchedules;
    public IEnumerable<VehicleSchedule> Schedules
    {
      get { return mSchedules; }
      set { SetProperty<IEnumerable<VehicleSchedule>>(ref mSchedules, value); }
    }

    #endregion

    #region decimal TotalIncome

    public const string TotalIncomeProperty = "TotalIncome";
    decimal mTotalIncome;
    public decimal TotalIncome
    {
      get
      {

        return Schedules.Sum(s => s.WaybillCharge);
      }
    }

    #endregion

    #region decimal TotalWaybills

    public const string TotalWaybillsProperty = "TotalWaybills";
    decimal mTotalWaybills;
    public decimal TotalWaybills
    {
      get { return Schedules.Sum(s => s.WaybillCount); }
    }

    #endregion

    #region decimal TotalWaybillWeight

    public const string TotalWaybillWeightProperty = "TotalWaybillWeight";
    decimal mTotalWaybillWeight;
    public decimal TotalWaybillWeight
    {
      get { return Schedules.Where(mw => (mw.Vehicle != null && mw.Vehicle.IsTruck)).Sum(s => s.WaybillWeight); }
    }

    #endregion

    #region DelegateCommand<WaybillFloorStatus> OpenDocumentCommand

    DelegateCommand<VehicleSchedule> mOpenDocumentCommand;
    public DelegateCommand<VehicleSchedule> OpenDocumentCommand
    {
      get { return mOpenDocumentCommand ?? (mOpenDocumentCommand = new DelegateCommand<VehicleSchedule>(ExecuteOpenWaybill, CanExecuteOpenWaybill)); }
    }

    bool CanExecuteOpenWaybill(VehicleSchedule args)
    {
      return true;
    }

    void ExecuteOpenWaybill(VehicleSchedule args)
    {
      MdiApplicationController.Current.EventAggregator.GetEvent<EditDocumentEvent>().Publish(new EditDocumentEventArgs(Cache.Instance.GetObject<DocumentType>(TransportManifest.DocumentTypeIdentifier), args.DocumentIdentifier));
    }

    #endregion
  }
}

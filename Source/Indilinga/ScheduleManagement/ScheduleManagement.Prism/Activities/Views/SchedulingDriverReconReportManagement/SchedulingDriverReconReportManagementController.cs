﻿using Afx.Business;
using Afx.Business.Activities;
using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Activities;
using ScheduleManagement.Business;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScheduleManagement.Prism.Activities.Views.SchedulingDriverReconReportManagement
{
  public partial class SchedulingDriverReconReportManagementController : MdiCustomActivityController<SchedulingDriverReconReportManagement, SchedulingDriverReconReportManagementViewModel>
  {
    #region Constructors

    [InjectionConstructor]
    public SchedulingDriverReconReportManagementController(IController controller)
      : base(controller)
    {
    }

    #endregion

    #region OnLoaded
    
    protected override void OnLoaded()
    {
      ViewModel.IsFocused = true;
      base.OnLoaded();
    }

    #endregion
    
    #region ExecuteOperation
    
    protected override void ExecuteOperation(Operation op, BusinessObject argument)
    {

      base.ExecuteOperation(op, argument);
    }

    #endregion

    protected override void OnDataContextPropertyChanged(string propertyName)
    {
      switch (propertyName)
      {
        case SchedulingDriverReconReportManagement.FilterByProperty:
          LoadDataContext();
          break;
      }
      base.OnDataContextPropertyChanged(propertyName);
    }

    #region "Hooks"

    protected override void LoadDataContext()
    {
      UnhookSchedules();
      base.LoadDataContext();
      HookSchedules();
    }

    private void HookSchedules()
    {
      if (DataContext.Schedules == null) return;

      foreach (var w in DataContext.Schedules)
      {
        w.CompositionChanged += w_CompositionChanged;
      }
    }

    private void UnhookSchedules()
    {
      if (DataContext.Schedules == null) return;

      foreach (var w in DataContext.Schedules)
      {
        w.CompositionChanged -= w_CompositionChanged;
      }
    }

    void w_CompositionChanged(object sender, CompositionChangedEventArgs e)
    {
      //Collection<VehicleSummary> vsCol = new Collection<VehicleSummary>();

      //switch (e.PropertyName)
      //{
      //  case "Vehicle":
      //    foreach (var w in DataContext.Schedules)
      //    {
      //      //if (BusinessObject.IsNull(w.Vehicle)) continue;

      //      if (!BusinessObject.IsNull(w.Vehicle) && w.Vehicle != null)
      //      {
      //        VehicleSummary vs = vsCol.Where(vs1 => vs1.Vehicle.Equals(w.Vehicle)).FirstOrDefault();
      //        if (vs == null)
      //        {
      //          vs = new VehicleSummary() { Vehicle = w.Vehicle };
      //          vsCol.Add(vs);
      //        }

      //        //vs.WaybillCount++;
      //        //vs.PhysicalWeight += w.OnFloor;
      //        //vs.Volume += w.Volume;
      //        if (vs.Waybills == null) vs.Waybills = new BusinessObjectCollection<WaybillFloorStatus>();
      //        vs.Waybills.Add(w);
      //      }
      //    }
      //    break;
      //  default:
      //    foreach (var w in DataContext.Schedules)
      //    {
      //      if (BusinessObject.IsNull(w.Vehicle)) continue;

      //      VehicleSummary vs = vsCol.Where(vs1 => vs1.Vehicle.Equals(w.Vehicle)).FirstOrDefault();
      //      if (vs == null)
      //      {
      //        vs = new VehicleSummary() { Vehicle = w.Vehicle };
      //        vsCol.Add(vs);
      //      }
      //      //vs.WaybillCount++;
      //      //vs.PhysicalWeight += w.OnFloor;
      //      //vs.Volume += w.Volume;
      //      if (vs.Waybills == null) vs.Waybills = new BusinessObjectCollection<WaybillFloorStatus>();
      //      vs.Waybills.Add(w);
      //    }
      //    break;
      //}

      //DataContext.VehicleSummary = vsCol;
    }

    #endregion
  }
}
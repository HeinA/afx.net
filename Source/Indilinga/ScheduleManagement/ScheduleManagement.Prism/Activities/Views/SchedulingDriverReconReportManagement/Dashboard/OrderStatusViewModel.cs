﻿using Afx.Business;
using Afx.Prism;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScheduleManagement.Prism.Activities.Views.SchedulingDriverReconReportManagement.Dashboard
{
  public partial class OrderStatusViewModel : ViewModel<OrderStatus>
  {
    #region Constructors

    [InjectionConstructor]
    public OrderStatusViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion
  }

  public class OrderStatus : BusinessObject
  {
    public OrderStatus() { }

    #region string Text

    public const string TextProperty = "Text";
    string mText;
    public string Text
    {
      get { return mText; }
      set { SetProperty<string>(ref mText, value); }
    }

    #endregion

    #region int Today

    public const string TodayProperty = "Today";
    int mToday;
    public int Today
    {
      get { return mToday; }
      set { SetProperty<int>(ref mToday, value); }
    }

    #endregion

    #region int Week

    public const string WeekProperty = "Week";
    int mWeek;
    public int Week
    {
      get { return mWeek; }
      set { SetProperty<int>(ref mWeek, value); }
    }

    #endregion

    #region int Month

    public const string MonthProperty = "Month";
    int mMonth;
    public int Month
    {
      get { return mMonth; }
      set { SetProperty<int>(ref mMonth, value); }
    }

    #endregion
  }
}

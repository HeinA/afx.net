﻿using Afx.Business.Documents;
using Afx.Prism;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScheduleManagement.Prism.Activities.Views.SchedulingDriverReconReportManagement
{
  public class DashboardTruckStatusReportViewModel : ViewModel<DocumentTypeState>
  {
    [InjectionConstructor]
    public DashboardTruckStatusReportViewModel(IController controller)
      : base(controller) 
    { }
  }
}

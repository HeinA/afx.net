﻿using Afx.Business.Documents;
using Afx.Prism;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScheduleManagement.Prism.Activities.Views.SchedulingDriverReconReportManagement
{
  public partial class DocumentTypeStateViewModel : ViewModel<DocumentTypeState>
  {
    #region Constructors

    [InjectionConstructor]
    public DocumentTypeStateViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion
  }
}

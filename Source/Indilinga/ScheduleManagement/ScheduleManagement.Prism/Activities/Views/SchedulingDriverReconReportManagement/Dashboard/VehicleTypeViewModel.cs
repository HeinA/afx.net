﻿using Afx.Business.Documents;
using Afx.Prism;
using FleetManagement.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScheduleManagement.Prism.Activities.Views.SchedulingDriverReconReportManagement
{
  public partial class VehicleTypeViewModel : ViewModel<Vehicle>
  {
    #region Constructors

    [InjectionConstructor]
    public VehicleTypeViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion
  }
}

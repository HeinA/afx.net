﻿using Afx.Business;
using ContactManagement.Business;
using FleetManagement.Business;
using FreightManagement.Business;
using ScheduleManagement.Business;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScheduleManagement.Prism.Activities.Views.SchedulingDriverReconReportManagement
{
  public class DriverContainer : BusinessObject
  {
    #region Constructors

    public DriverContainer(Contact driver, IEnumerable<VehicleSchedule> collection, string filterby = "")
    {
      Driver = driver;
      FilterBy = filterby;
      Schedules = collection;
    }

    #endregion

    #region string FilterBy

    public const string FilterByProperty = "FilterBy";
    string mFilterBy = "";
    public string FilterBy
    {
      get { return mFilterBy; }
      private set { SetProperty<string>(ref mFilterBy, value); }
    }

    #endregion

    #region Contact Driver

    public const string DriverProperty = "Driver";
    Contact mDriver;
    public Contact Driver
    {
      get { return mDriver; }
      set { SetProperty<Contact>(ref mDriver, value); }
    }

    #endregion

    #region IEnumerable<VehicleSchedule> Schedules

    public const string SchedulesProperty = "Schedules";
    IEnumerable<VehicleSchedule> mSchedules;
    public IEnumerable<VehicleSchedule> Schedules
    {
      get { return mSchedules; }
      private set 
      {
        mSchedules = value;

        try
        {
          switch (FilterBy)
          { 
            case "By Driver":
              Collection<VehicleContainer> vehicles = new Collection<VehicleContainer>();

              var varResults = (BusinessObject.IsNull(Driver) ? mSchedules.Where(mw => mw.Driver == null && (mw.Vehicle != null && mw.Vehicle.IsTruck)).Select(c => c.Vehicle).Distinct().OrderBy(r => r.Text) : mSchedules.Where(mw => mw.Driver.Equals(Driver) && (mw.Vehicle != null && mw.Vehicle.IsTruck)).Select(c => c.Vehicle).Distinct().OrderBy(r => r.Text));
              if (varResults != null && varResults.DefaultIfEmpty() != null)
              {
                foreach (var vehicle in varResults)
                {
                  if (!BusinessObject.IsNull(vehicle))
                  {
                    vehicles.Add(new VehicleContainer(vehicle, mSchedules.Where(mw => mw.Vehicle.Equals(vehicle))));
                  }
                  else
                  {
                    vehicles.Add(new VehicleContainer(vehicle, mSchedules));
                  }
                }
              }
              Vehicles = vehicles;
              break;
            case "By Horse":
              break;
            case "By Trip":
              break;
          }
        }
        catch
        {
          throw;
        }
      }
    }

    #endregion

    #region IEnumerable<VehicleContainer> Vehicles

    public const string VehiclesProperty = "Vehicles";
    IEnumerable<VehicleContainer> mVehicles;
    public IEnumerable<VehicleContainer> Vehicles
    {
      get { return mVehicles; }
      set { SetProperty<IEnumerable<VehicleContainer>>(ref mVehicles, value); }
    }

    #endregion

    #region decimal TotalIncome

    public const string TotalIncomeProperty = "TotalIncome";
    decimal mTotalIncome;
    public decimal TotalIncome
    {
      get 
      {

        return Schedules.Where(mw => (mw.Vehicle != null && mw.Vehicle.IsTruck)).Sum(s => s.WaybillCharge); 
      }
    }

    #endregion

    #region decimal TotalWaybills

    public const string TotalWaybillsProperty = "TotalWaybills";
    decimal mTotalWaybills;
    public decimal TotalWaybills
    {
      get { return Schedules.Where(mw => (mw.Vehicle != null && mw.Vehicle.IsTruck)).Sum(s => s.WaybillCount); }
    }

    #endregion

    #region decimal TotalWaybillWeight

    public const string TotalWaybillWeightProperty = "TotalWaybillWeight";
    decimal mTotalWaybillWeight;
    public decimal TotalWaybillWeight
    {
      get { return Schedules.Where(mw => (mw.Vehicle != null && mw.Vehicle.IsTruck)).Sum(s => s.WaybillWeight); }
    }

    #endregion
  }
}

﻿using Afx.Business;
using ScheduleManagement.Business;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScheduleManagement.Prism.Activities.Views.SchedulingDriverReconReportManagement
{
  public class TripContainer : BusinessObject
  {
    public TripContainer(DateTime trip, IEnumerable<VehicleSchedule> collection, string filterby = "")
    {
      Trip = trip;
      FilterBy = filterby;
      Schedules = collection;
    }

    #region DateTime Trip

    public const string TripProperty = "Trip";
    DateTime mTrip;
    public DateTime Trip
    {
      get { return mTrip; }
      set { SetProperty<DateTime>(ref mTrip, value); }
    }

    #endregion

    #region string FilterBy

    public const string FilterByProperty = "FilterBy";
    string mFilterBy = "";
    public string FilterBy
    {
      get { return mFilterBy; }
      private set { SetProperty<string>(ref mFilterBy, value); }
    }

    #endregion

    #region ObservableCollection<VehicleSchedule> Schedules

    public const string SchedulesProperty = "Schedules";
    IEnumerable<VehicleSchedule> mSchedules;
    public IEnumerable<VehicleSchedule> Schedules
    {
      get { return mSchedules; }
      private set
      {
        mSchedules = value;

        try
        {
          switch (FilterBy)
          {
            case "By Trip":
              Collection<VehicleContainer> vehicles = new Collection<VehicleContainer>();

              var varResults = mSchedules.Where(mw => mw.ScheduledLoadingDate.Equals(Trip) && (mw.Vehicle != null && mw.Vehicle.IsTruck)).Select(c => c.Vehicle).Distinct().OrderBy(r => r.Text);
              if (varResults != null && varResults.DefaultIfEmpty() != null)
              {
                foreach (var vehicle in varResults)
                {
                  if (!BusinessObject.IsNull(vehicle))
                  {
                    vehicles.Add(new VehicleContainer(vehicle, mSchedules.Where(mw => mw.Vehicle.Equals(vehicle))));
                  }
                  else
                  {
                    vehicles.Add(new VehicleContainer(vehicle, mSchedules));
                  }
                }
              }
              Vehicles = vehicles;
              break;
            case "By Horse":
              break;
          }
        }
        catch
        {
          throw;
        }
      }
    }

    #endregion

    #region IEnumerable<VehicleContainer> Vehicles

    public const string VehiclesProperty = "Vehicles";
    IEnumerable<VehicleContainer> mVehicles;
    public IEnumerable<VehicleContainer> Vehicles
    {
      get { return mVehicles; }
      set { SetProperty<IEnumerable<VehicleContainer>>(ref mVehicles, value); }
    }

    #endregion


    #region decimal TotalIncome

    public const string TotalIncomeProperty = "TotalIncome";
    decimal mTotalIncome;
    public decimal TotalIncome
    {
      get
      {

        return Schedules.Where(mw => (mw.Vehicle != null && mw.Vehicle.IsTruck)).Sum(s => s.WaybillCharge);
      }
    }

    #endregion

    #region decimal TotalWaybills

    public const string TotalWaybillsProperty = "TotalWaybills";
    decimal mTotalWaybills;
    public decimal TotalWaybills
    {
      get { return Schedules.Where(mw => (mw.Vehicle != null && mw.Vehicle.IsTruck)).Sum(s => s.WaybillCount); }
    }

    #endregion

    #region decimal TotalWaybillWeight

    public const string TotalWaybillWeightProperty = "TotalWaybillWeight";
    decimal mTotalWaybillWeight;
    public decimal TotalWaybillWeight
    {
      get { return Schedules.Where(mw => (mw.Vehicle != null && mw.Vehicle.IsTruck)).Sum(s => s.WaybillWeight); }
    }

    #endregion
  }
}

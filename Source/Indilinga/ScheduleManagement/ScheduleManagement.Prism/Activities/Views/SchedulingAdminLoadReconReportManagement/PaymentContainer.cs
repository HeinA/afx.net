﻿using Afx.Business;
using ScheduleManagement.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScheduleManagement.Prism.Activities.Views.SchedulingAdminLoadReconReportManagement
{
  public class PaymentContainer : BusinessObject
  {
    #region Constructors

    public PaymentContainer(int week, IEnumerable<ScheduledPayment> collection, string filterby = "")
    {
      Week = week;
      FilterBy = filterby;
      Payments = collection;
    }

    #endregion

    #region string FilterBy

    public const string FilterByProperty = "FilterBy";
    string mFilterBy = "";
    public string FilterBy
    {
      get { return mFilterBy; }
      private set { SetProperty<string>(ref mFilterBy, value); }
    }

    #endregion

    #region int Week

    public const string WeekProperty = "Week";
    int mWeek;
    public int Week
    {
      get { return mWeek; }
      set { SetProperty<int>(ref mWeek, value); }
    }

    #endregion

    #region IEnumerable<ScheduledPayment> Payments

    public const string PaymentsProperty = "Payments";
    IEnumerable<ScheduledPayment> mPayments;
    public IEnumerable<ScheduledPayment> Payments
    {
      get { return mPayments; }
      set { SetProperty<IEnumerable<ScheduledPayment>>(ref mPayments, value); }
    }

    #endregion

    public decimal TotalPayments
    {
      get
      {
        return Payments == null ? 0 : Payments.Sum(x => x.Amount);
      }
    }

    public decimal CountPayments
    {
      get
      {
        return Payments == null ? 0 : Payments.Count();
      }
    }
  }
}

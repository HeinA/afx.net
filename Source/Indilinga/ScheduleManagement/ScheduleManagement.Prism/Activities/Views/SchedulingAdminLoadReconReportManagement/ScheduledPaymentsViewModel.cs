﻿using Afx.Business.Data;
using Afx.Business.Documents;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using Afx.Prism.RibbonMdi.Documents;
using Afx.Prism.RibbonMdi.Events;
using FreightManagement.Business;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Unity;
using ScheduleManagement.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScheduleManagement.Prism.Activities.Views.SchedulingAdminLoadReconReportManagement
{
  public partial class ScheduledPaymentsViewModel : ViewModel<ScheduledPayment>
  {
    #region Constructors

    [InjectionConstructor]
    public ScheduledPaymentsViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    public bool IsDeleted
    {
      get { return Model.IsDeleted; }
    }

    public bool HasError
    {
      get { return Model.HasError; }
    }

    [Dependency]
    public SchedulingAdminLoadReconReportManagementController SchedulingAdminLoadReconReportManagementController { get; set; }

    #region DelegateCommand<ScheduledPayment> OpenDocumentCommand

    DelegateCommand<ScheduledPaymentsViewModel> mOpenDocumentCommand;
    public DelegateCommand<ScheduledPaymentsViewModel> OpenDocumentCommand
    {
      get { return mOpenDocumentCommand ?? (mOpenDocumentCommand = new DelegateCommand<ScheduledPaymentsViewModel>(ExecuteOpenWaybill, CanExecuteOpenWaybill)); }
    }

    bool CanExecuteOpenWaybill(ScheduledPaymentsViewModel args)
    {
      return true;
    }

    void ExecuteOpenWaybill(ScheduledPaymentsViewModel args)
    {
      MdiApplicationController.Current.EventAggregator.GetEvent<EditDocumentEvent>().Publish(new EditDocumentEventArgs(Cache.Instance.GetObject<DocumentType>(TransportManifest.DocumentTypeIdentifier), args.Model.DocGlobalIdentifier));
    }

    #endregion

  }
}

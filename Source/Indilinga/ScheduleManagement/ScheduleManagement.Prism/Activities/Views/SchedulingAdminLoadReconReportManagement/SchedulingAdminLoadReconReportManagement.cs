﻿using Afx.Business.Activities;
using ScheduleManagement.Business;
using ScheduleManagement.Business.Service;
using Afx.Prism.RibbonMdi.Activities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Afx.Business.Service;
using Afx.Business.Security;
using Afx.Business;
using System.Collections.ObjectModel;
using Afx.Business.Collections;
using System.Windows.Data;
using Afx.Business.Documents;
using Afx.Business.Data;
using Afx.Prism.RibbonMdi.Events;
using Afx.Prism.RibbonMdi;
using FreightManagement.Business;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Unity;

namespace ScheduleManagement.Prism.Activities.Views.SchedulingAdminLoadReconReportManagement
{
  public partial class SchedulingAdminLoadReconReportManagement : CustomActivityContext
  {
    public SchedulingAdminLoadReconReportManagement()
    {
    }

    public SchedulingAdminLoadReconReportManagement(Activity activity)
      : base(activity)
    {
    }

    public override bool IsDirty
    {
      get { return false; }
      set { }
    }

    #region Collections


    #region ObservableCollection<ScheduledPayment> ScheduledPayments

    public const string ScheduledPaymentsProperty = "ScheduledPayments";
    ObservableCollection<ScheduledPayment> mScheduledPayments;
    public ObservableCollection<ScheduledPayment> ScheduledPayments
    {
      get { return mScheduledPayments; }
      set { SetProperty<ObservableCollection<ScheduledPayment>>(ref mScheduledPayments, value); }
    }

    #endregion


    #region Collection<DriverContainer> Payments

    Collection<PaymentContainer> mPayments;
    public Collection<PaymentContainer> Payments
    {
      get { return mPayments; }
      set { SetProperty<Collection<PaymentContainer>>(ref mPayments, value); }
    }

    #endregion

    #endregion


    #region "Filters"


    #region string FilterBy

    public const string FilterByProperty = "FilterBy";
    string mFilterBy = "Dashboard";
    public string FilterBy
    {
      get { return mFilterBy; }
      set
      {
        if (SetProperty(ref mFilterBy, value))
        {
          //FloorStatusManagementController.Refresh();
        }
      }
    }

    #endregion


    #region DateTime fromDateFilter

    public const string fromDateFilterProperty = "fromDateFilter";
    DateTime mfromDateFilter = DateTime.Today.AddMonths(-1);
    public DateTime fromDateFilter
    {
      get { return mfromDateFilter; }
      set
      {
        if (SetProperty<DateTime>(ref mfromDateFilter, value))
        {
          LoadPayments();
        }
      }
    }

    #endregion

    #region DateTime toDateFilter

    public const string toDateFilterProperty = "toDateFilter";
    DateTime mtoDateFilter = DateTime.Today.AddDays(1);
    public DateTime toDateFilter
    {
      get { return mtoDateFilter; }
      set
      {
        if (SetProperty<DateTime>(ref mtoDateFilter, value))
        {
          LoadPayments();
        }
      }
    }

    #endregion

    #endregion

    public override void LoadData()
    {
      LoadPayments();
      base.LoadData();
    }

    void LoadPayments()
    {
      using (var svc = ProxyFactory.GetService<IScheduleManagementService>(SecurityContext.ServerName))
      {
        ScheduledPayments = svc.LoadReconDailyPayments(fromDateFilter, toDateFilter);
      }
    }

    protected override void OnPropertyChanged(string propertyName)
    {
      switch (propertyName)
      {
        case VehicleSchedule.IsCheckedProperty:
          OnPropertyChanged("IsChecked");
          break;
      }
      base.OnPropertyChanged(propertyName);
    }
  }
}

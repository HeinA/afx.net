﻿using Afx.Business.Security;
using Afx.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Activities;
using ScheduleManagement.Business;
using Microsoft.Practices.Prism.Commands;
using Afx.Prism.RibbonMdi.Events;
using Afx.Prism.RibbonMdi;
using Afx.Business.Data;
using Afx.Business.Documents;
using FreightManagement.Business;
using System.Collections.ObjectModel;

namespace ScheduleManagement.Prism.Activities.Views.SchedulingAdminLoadReconReportManagement
{
  public partial class SchedulingAdminLoadReconReportManagementViewModel : MdiCustomActivityViewModel<SchedulingAdminLoadReconReportManagement>
  {
    #region Constructors

    [InjectionConstructor]
    public SchedulingAdminLoadReconReportManagementViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion


    [Dependency]
    public SchedulingAdminLoadReconReportManagementController SchedulingAdminLoadReconReportManagementController { get; set; }

    protected override void OnPropertyChanged(string propertyName)
    {
      switch (propertyName)
      {
        case "ScheduledPayments":
          OnPropertyChanged("PaymentItems");
          break;
      }
      base.OnPropertyChanged(propertyName);
    }

    protected override void OnModelCompositionChanged(CompositionChangedEventArgs e)
    {
      switch (e.PropertyName)
      {
        case "ScheduledPayments":
          OnPropertyChanged("PaymentItems");
          break;
      }
      base.OnModelCompositionChanged(e);
    }

    IEnumerable<ScheduledPaymentsViewModel> mPaymentItems;
    public IEnumerable<ScheduledPaymentsViewModel> PaymentItems
    {
      get
      {
        if (Model == null) return null;
        return mPaymentItems ?? (mPaymentItems = ResolveViewModels<ScheduledPaymentsViewModel, ScheduledPayment>(Model.ScheduledPayments));
      }
    }
  }
}

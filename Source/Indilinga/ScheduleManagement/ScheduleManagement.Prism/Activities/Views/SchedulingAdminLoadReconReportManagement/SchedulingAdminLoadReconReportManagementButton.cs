﻿using Afx.Business.Activities;
using Afx.Business.Data;
using Afx.Prism.RibbonMdi;
using Afx.Prism.RibbonMdi.Ribbon;
using FreightManagement.Prism;
using ReportingModule.Prism;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScheduleManagement.Prism.Activities.Views.SchedulingAdminLoadReconReportManagement
{
  [Export(typeof(IRibbonItem))]
  public class SchedulingAdminLoadReconReportManagementButton : RibbonButtonViewModel
  {
    public override string TabName
    {
      get { return FreightManagementTab.TabName; }
    }

    public override string GroupName
    {
      get { return ScheduleManagement.Prism.SchedulingGroup.GroupName; }
    }

    public override string ItemIdentifier
    {
      get { return "SchedulingAdminLoadReconReportManagement"; }
    }

    public override int Index
    {
      get { return 0; }
    }

    #region Activity Activity

    public Activity Activity
    {
      get { return Cache.Instance.GetObject<Activity>(SchedulingAdminLoadReconReportManagement.Key); }
    }

    #endregion

    public bool IsEnabled
    {
      get
      {
        try
        {
          if (Activity == null) return false;
          return Activity.CanView;
        }
        catch
        {
          return false;
        }
      }
    }

    protected override void OnExecute()
    {
      MdiApplicationController.Current.ExecuteActivity(Activity);
    }
  }
}

﻿using Afx.Prism;
using Microsoft.Practices.Unity;
using ScheduleManagement.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScheduleManagement.Prism.Activities.Views.SchedulingDailyPaymentsManagement
{
  public partial class SchedulingPaymentViewModel : ViewModel<SchedulingDailyPayments>
  {
    #region Constructors

    [InjectionConstructor]
    public SchedulingPaymentViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    public bool IsDeleted
    {
      get { return Model.IsDeleted; }
    }

    public bool HasError
    {
      get { return Model.HasError; }
    }

    [Dependency]
    public SchedulingDailyPaymentsManagementController SchedulingDailyPaymentsManagementController { get; set; }

    #region string FleetNumber

    public string FleetNumber
    {
      get { return Model.Vehicle.FleetNumber; }
    }

    #endregion
  }
}

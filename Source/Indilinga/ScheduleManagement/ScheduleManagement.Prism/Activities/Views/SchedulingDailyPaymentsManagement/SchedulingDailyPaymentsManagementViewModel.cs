﻿using Afx.Business.Security;
using Afx.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Activities;
using ScheduleManagement.Business;

namespace ScheduleManagement.Prism.Activities.Views.SchedulingDailyPaymentsManagement
{
  public partial class SchedulingDailyPaymentsManagementViewModel : MdiCustomActivityViewModel<SchedulingDailyPaymentsManagement>
  {
    #region Constructors

    [InjectionConstructor]
    public SchedulingDailyPaymentsManagementViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    protected override void OnModelCompositionChanged(CompositionChangedEventArgs e)
    {
      switch (e.PropertyName)
      {
        case "Items":
          mAdminInputItems = null;
          OnPropertyChanged("AdminInputItems");
          break;
      }
      base.OnModelCompositionChanged(e);
    }

    protected override void OnPropertyChanged(string propertyName)
    {
      switch (propertyName)
      {
        case "Items":
          mAdminInputItems = null;
          OnPropertyChanged("AdminInputItems");
          break;
      }
      base.OnPropertyChanged(propertyName);
    }

    IEnumerable<SchedulingPaymentViewModel> mAdminInputItems;
    public IEnumerable<SchedulingPaymentViewModel> AdminInputItems
    {
      get
      {
        if (Model == null) return null;
        return mAdminInputItems ?? (mAdminInputItems = ResolveViewModels<SchedulingPaymentViewModel, SchedulingDailyPayments>(Model.Items));
      }
    }
  }
}

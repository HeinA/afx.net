﻿using ScheduleManagement.Business;
using ScheduleManagement.Business.Service;
using Afx.Business.Service;
using Afx.Prism.RibbonMdi.Activities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.Composition;
using Afx.Business.Security;

namespace ScheduleManagement.Prism.Activities.Views.SchedulingDailyPaymentsManagement
{
  //**************************************************************************************************************
  //**************************************************************************************************************
  //*                                                                                                            *
  //*     This code is auto-generated.  Do not place any cutom code here, it will be lost on re-generation.      *
  //*                                                                                                            *
  //**************************************************************************************************************
  //**************************************************************************************************************
  [Export("Context:" + global::ScheduleManagement.Prism.Activities.Views.SchedulingDailyPaymentsManagement.SchedulingDailyPaymentsManagement.Key)]
  public partial class SchedulingDailyPaymentsManagement
  {
    public const string Key = "{a6064e10-ead1-48d5-8c0a-89bf24ce29bf}";
  }
}

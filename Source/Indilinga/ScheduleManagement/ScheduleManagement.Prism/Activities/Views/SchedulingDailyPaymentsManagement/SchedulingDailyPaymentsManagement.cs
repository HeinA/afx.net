﻿using Afx.Business.Activities;
using ScheduleManagement.Business;
using ScheduleManagement.Business.Service;
using Afx.Prism.RibbonMdi.Activities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Afx.Business.Service;
using Afx.Business.Security;
using System.Collections.ObjectModel;

namespace ScheduleManagement.Prism.Activities.Views.SchedulingDailyPaymentsManagement
{
  public partial class SchedulingDailyPaymentsManagement : CustomActivityContext
  {
    public SchedulingDailyPaymentsManagement()
    {
    }

    public SchedulingDailyPaymentsManagement(Activity activity)
      : base(activity)
    {
    }

    public override bool IsDirty
    {
      get { return false; }
      set { }
    }

    #region Collections


    #region ObservableCollection<SchedulingDailyPayments> Items

    public const string ItemsProperty = "Items";
    ObservableCollection<SchedulingDailyPayments> mItems;
    public ObservableCollection<SchedulingDailyPayments> Items
    {
      get { return mItems; }
      set { SetProperty<ObservableCollection<SchedulingDailyPayments>>(ref mItems, value); }
    }

    #endregion

    #endregion


    #region "Filters"


    #region string FilterBy

    public const string FilterByProperty = "FilterBy";
    string mFilterBy = "Dashboard";
    public string FilterBy
    {
      get { return mFilterBy; }
      set
      {
        if (SetProperty(ref mFilterBy, value))
        {
          //FloorStatusManagementController.Refresh();
        }
      }
    }

    #endregion


    #region DateTime fromDateFilter

    public const string fromDateFilterProperty = "fromDateFilter";
    DateTime mfromDateFilter = DateTime.Today;
    public DateTime fromDateFilter
    {
      get { return mfromDateFilter; }
      set
      {
        if (SetProperty<DateTime>(ref mfromDateFilter, value))
        {
          LoadItems();
        }
      }
    }

    #endregion

    #region DateTime toDateFilter

    public const string toDateFilterProperty = "toDateFilter";
    DateTime mtoDateFilter = DateTime.Today.AddDays(1).AddSeconds(-1);
    public DateTime toDateFilter
    {
      get { return mtoDateFilter; }
      set
      {
        if (SetProperty<DateTime>(ref mtoDateFilter, value))
        {
          LoadItems();
        }
      }
    }

    #endregion

    #endregion

    public override void LoadData()
    {
      LoadItems();
      base.LoadData();
    }

    void LoadItems()
    {
      using (var svc = ProxyFactory.GetService<IScheduleManagementService>(SecurityContext.ServerName))
      {
        Items = svc.LoadDailyPayments(fromDateFilter, toDateFilter);
      }
    }

    protected override void OnPropertyChanged(string propertyName)
    {
      switch (propertyName)
      {
        case VehicleSchedule.IsCheckedProperty:
          OnPropertyChanged("IsChecked");
          break;
      }
      base.OnPropertyChanged(propertyName);
    }
  }
}

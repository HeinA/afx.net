﻿using Afx.Business.Activities;
using ScheduleManagement.Business;
using ScheduleManagement.Business.Service;
using Afx.Prism.RibbonMdi.Activities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using Afx.Business.Service;
using Afx.Business.Security;
using Afx.Business.Documents;
using Afx.Business.Data;
using FreightManagement.Business;
using Microsoft.Practices.Prism.Commands;
using Afx.Business.Collections;

namespace ScheduleManagement.Prism.Activities.Views.SchedulingAdminInputManagement
{
  public partial class SchedulingAdminInputManagement : CustomActivityContext
  {
    public SchedulingAdminInputManagement()
    {
    }

    public SchedulingAdminInputManagement(Activity activity)
      : base(activity)
    {
    }

    public override bool IsDirty
    {
      get { return false; }
      set { }
    }

    #region Collections
    
    #region ObservableCollection<SchedulingAdminInput> Items

    public const string ItemsProperty = "Items";
    ObservableCollection<SchedulingAdminInput> mItems;
    public ObservableCollection<SchedulingAdminInput> Items
    {
      get { return mItems; }
      set { SetProperty<ObservableCollection<SchedulingAdminInput>>(ref mItems, value); }
    }

    #endregion

    //#region ObservableCollection<DocumentTypeState> WaybillStates

    //public ObservableCollection<DocumentTypeState> WaybillStates
    //{
    //  get { return Cache.Instance.GetObjects<DocumentTypeState>(true, dts => dts.Owner.Identifier.Equals(Waybill.DocumentTypeIdentifier), dts => dts.Name, true); }
    //}

    //#endregion

    #endregion


    #region "Filters"


    #region string FilterBy

    public const string FilterByProperty = "FilterBy";
    string mFilterBy = "Dashboard";
    public string FilterBy
    {
      get { return mFilterBy; }
      set
      {
        if (SetProperty(ref mFilterBy, value))
        {
          //FloorStatusManagementController.Refresh();
        }
      }
    }

    #endregion


    #region DateTime fromDateFilter

    public const string fromDateFilterProperty = "fromDateFilter";
    DateTime mfromDateFilter = DateTime.Today.AddMonths(-1);
    public DateTime fromDateFilter
    {
      get { return mfromDateFilter; }
      set
      {
        if (SetProperty<DateTime>(ref mfromDateFilter, value))
        {
          LoadItems();
        }
      }
    }

    #endregion

    #region DateTime toDateFilter

    public const string toDateFilterProperty = "toDateFilter";
    DateTime mtoDateFilter = DateTime.Today.AddDays(1);
    public DateTime toDateFilter
    {
      get { return mtoDateFilter; }
      set
      {
        if (SetProperty<DateTime>(ref mtoDateFilter, value))
        {
          LoadItems();
        }
      }
    }

    #endregion

    #region string AccountingOrderNumber

    public const string AccountingOrderNumberProperty = "AccountingOrderNumber";
    string mAccountingOrderNumber = "";
    public string AccountingOrderNumber
    {
      get { return mAccountingOrderNumber; }
      set
      {
        if (SetProperty<string>(ref mAccountingOrderNumber, value))
        {
          LoadItems();
        }
      }
    }

    #endregion

    #region string InvoiceNumber

    public const string InvoiceNumberProperty = "InvoiceNumber";
    string mInvoiceNumber = string.Empty;
    public string InvoiceNumber
    {
      get { return mInvoiceNumber; }
      set { SetProperty<string>(ref mInvoiceNumber, value); }
    }

    #endregion

    #region bool? IsNotEntered

    public const string IsNotEnteredProperty = "IsNotEntered";
    bool? mIsNotEntered = true;
    public bool? IsNotEntered
    {
      get { return mIsNotEntered; }
      set {
        if (SetProperty<bool?>(ref mIsNotEntered, value))
        {
          LoadItems();
        }
      }
    }

    #endregion

    #endregion

    public override void LoadData()
    {
      LoadItems();
      base.LoadData();
    }

    void LoadItems()
    {
      using (var svc = ProxyFactory.GetService<IScheduleManagementService>(SecurityContext.ServerName))
      {
        Items = svc.LoadAdminInput(fromDateFilter, toDateFilter, AccountingOrderNumber, IsNotEntered);
      }
    }
  }
}

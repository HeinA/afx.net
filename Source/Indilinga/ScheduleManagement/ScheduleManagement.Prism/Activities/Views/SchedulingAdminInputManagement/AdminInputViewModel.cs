﻿using Afx.Business.Data;
using Afx.Business.Documents;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using Afx.Prism.RibbonMdi.Events;
using FreightManagement.Business;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Unity;
using ScheduleManagement.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScheduleManagement.Prism.Activities.Views.SchedulingAdminInputManagement
{
  public partial class AdminInputViewModel : ViewModel<SchedulingAdminInput>
  {
    #region Constructors

    [InjectionConstructor]
    public AdminInputViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    public bool IsDeleted
    {
      get { return Model.IsDeleted; }
    }

    public bool HasError
    {
      get { return Model.HasError; }
    }

    [Dependency]
    public SchedulingAdminInputManagementController SchedulingAdminInputManagementController { get; set; }

    #region DelegateCommand<SchedulingAdminInput> OpenTransportManifestCommand

    DelegateCommand<AdminInputViewModel> mOpenTransportManifestCommand;
    public DelegateCommand<AdminInputViewModel> OpenTransportManifestCommand
    {
      get { return mOpenTransportManifestCommand ?? (mOpenTransportManifestCommand = new DelegateCommand<AdminInputViewModel>(ExecuteOpenTransportManifest, CanExecuteOpenTransportManifest)); }
    }

    bool CanExecuteOpenTransportManifest(AdminInputViewModel args)
    {
      return true;
    }

    void ExecuteOpenTransportManifest(AdminInputViewModel args)
    {
      MdiApplicationController.Current.EventAggregator.GetEvent<EditDocumentEvent>().Publish(new EditDocumentEventArgs(Cache.Instance.GetObject<DocumentType>(TransportManifest.DocumentTypeIdentifier), args.Model.DocGlobalIdentifier));
    }

    #endregion

    #region DelegateCommand<SchedulingAdminInput> OpenWaybillCommand

    DelegateCommand<AdminInputViewModel> mOpenWaybillCommand;
    public DelegateCommand<AdminInputViewModel> OpenWaybillCommand
    {
      get { return mOpenWaybillCommand ?? (mOpenWaybillCommand = new DelegateCommand<AdminInputViewModel>(ExecuteOpenWaybill, CanExecuteOpenWaybill)); }
    }

    bool CanExecuteOpenWaybill(AdminInputViewModel args)
    {
      return true;
    }

    void ExecuteOpenWaybill(AdminInputViewModel args)
    {
      MdiApplicationController.Current.EventAggregator.GetEvent<EditDocumentEvent>().Publish(new EditDocumentEventArgs(Cache.Instance.GetObject<DocumentType>(Waybill.DocumentTypeIdentifier), args.Model.GlobalIdentifier));
    }

    #endregion

  }
}

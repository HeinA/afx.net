﻿using ScheduleManagement.Business;
using ScheduleManagement.Business.Service;
using Afx.Business.Service;
using Afx.Prism.RibbonMdi.Activities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.Composition;
using Afx.Business.Security;

namespace ScheduleManagement.Prism.Activities.Views.SchedulingAdminInputManagement
{
  //**************************************************************************************************************
  //**************************************************************************************************************
  //*                                                                                                            *
  //*     This code is auto-generated.  Do not place any cutom code here, it will be lost on re-generation.      *
  //*                                                                                                            *
  //**************************************************************************************************************
  //**************************************************************************************************************
  [Export("Context:" + global::ScheduleManagement.Prism.Activities.Views.SchedulingAdminInputManagement.SchedulingAdminInputManagement.Key)]
  public partial class SchedulingAdminInputManagement
  {
    public const string Key = "{defa4c11-e98d-4bc1-bfae-a5045c62134d}";
  }
}

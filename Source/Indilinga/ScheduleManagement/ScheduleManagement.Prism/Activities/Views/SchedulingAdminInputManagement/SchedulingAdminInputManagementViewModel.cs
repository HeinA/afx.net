﻿using Afx.Business.Security;
using Afx.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Activities;
using ScheduleManagement.Business;
using Microsoft.Practices.Prism.Commands;
using Afx.Business.Documents;
using System.Collections.ObjectModel;
using Afx.Business.Collections;

namespace ScheduleManagement.Prism.Activities.Views.SchedulingAdminInputManagement
{
  public partial class SchedulingAdminInputManagementViewModel : MdiCustomActivityViewModel<SchedulingAdminInputManagement>
  {
    #region Constructors

    [InjectionConstructor]
    public SchedulingAdminInputManagementViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    protected override void OnModelCompositionChanged(CompositionChangedEventArgs e)
    {
      switch (e.PropertyName)
      {
        case "Items":
          mAdminInputItems = null;
          OnPropertyChanged("AdminInputItems");
          break;
      }
      base.OnModelCompositionChanged(e);
    }

    IEnumerable<AdminInputViewModel> mAdminInputItems;
    public IEnumerable<AdminInputViewModel> AdminInputItems
    {
      get
      {
        if (Model == null) return null;
        if (Model.IsNotEntered == null)
        {
          return mAdminInputItems ?? (mAdminInputItems = ResolveViewModels<AdminInputViewModel, SchedulingAdminInput>(Model.Items.Where(sai => sai.ClientOrderNumber.Contains(Model.AccountingOrderNumber)).Where(sai => sai.InvoiceNumber.Contains(Model.InvoiceNumber)).ToList()));
        }
        else
        {
          return mAdminInputItems ?? (mAdminInputItems = ResolveViewModels<AdminInputViewModel, SchedulingAdminInput>(Model.Items.Where(sai => String.IsNullOrWhiteSpace(sai.Order) == Model.IsNotEntered).Where(sai => sai.Order.Contains(Model.AccountingOrderNumber)).Where(sai => sai.InvoiceNumber.Contains(Model.InvoiceNumber)).ToList()));
        }
      }
    }
  }
}

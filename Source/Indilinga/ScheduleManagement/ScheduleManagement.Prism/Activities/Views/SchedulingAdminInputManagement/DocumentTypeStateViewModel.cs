﻿using Afx.Business;
using Afx.Business.Collections;
using Afx.Business.Documents;
using Afx.Prism;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScheduleManagement.Prism.Activities.Views.SchedulingAdminInputManagement
{
  public class DocumentTypeStateViewModel : ViewModel<DocumentTypeState>
  {
    #region Constructors

    [InjectionConstructor]
    public DocumentTypeStateViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    public bool IsDeleted
    {
      get { return Model.IsDeleted; }
    }

    public bool HasError
    {
      get { return Model.HasError; }
    }

    [Dependency]
    public SchedulingAdminInputManagementController SchedulingAdminInputManagementController { get; set; }

    #region bool IsChecked

    public const string IsCheckedProperty = "IsChecked";
    bool mIsChecked;
    public bool IsChecked
    {
      get { return mIsChecked; }
      set { SetProperty<bool>(ref mIsChecked, value); }
    }

    #endregion

    #region DelegateCommand CheckBoxItemCheckCommand

    DelegateCommand<DocumentTypeStateViewModel> mCheckBoxItemCheckCommand;
    public DelegateCommand<DocumentTypeStateViewModel> CheckBoxItemCheckCommand
    {
      get { return mCheckBoxItemCheckCommand ?? (mCheckBoxItemCheckCommand = new DelegateCommand<DocumentTypeStateViewModel>(ExecuteCheckBoxItemCheck, CanExecuteCheckBoxItemCheck)); }
    }

    bool CanExecuteCheckBoxItemCheck(DocumentTypeStateViewModel args)
    {
      return true;
    }

    void ExecuteCheckBoxItemCheck(DocumentTypeStateViewModel args)
    {
      if (args == null) return;
      //((SchedulingAdminInputManagementViewModel)Parent).SelectedStates = ((SchedulingAdminInputManagementViewModel)Parent).SelectedStates ?? new BasicCollection<DocumentTypeState>();
      //if (((SchedulingAdminInputManagementViewModel)Parent).SelectedStates.Contains(args.Model) && !IsChecked) ((SchedulingAdminInputManagementViewModel)Parent).SelectedStates.Remove(args.Model);
      //if (!((SchedulingAdminInputManagementViewModel)Parent).SelectedStates.Contains(args.Model) && IsChecked) ((SchedulingAdminInputManagementViewModel)Parent).SelectedStates.Add(args.Model);


      ////SchedulingAdminInputManagementController.ViewModel.PropertyChanged("AdminInputItems");
    }

    #endregion
  }
}

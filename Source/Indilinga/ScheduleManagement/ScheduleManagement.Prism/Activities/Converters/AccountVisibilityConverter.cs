﻿
using System;
using System.Windows;
using System.Windows.Data;

namespace ScheduleManagement.Prism.Activities.Converters
{
  public class AccountVisibilityConverter : IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    {
      string st = ((string)value);//.Replace("System.Windows.Controls.ComboBoxItem: ", "");
      switch (st)
      {
        case "By Client":
          return Visibility.Visible;
        default:
          return Visibility.Collapsed;
      }
    }

    public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    {
      throw new NotImplementedException();
    }
  }
}

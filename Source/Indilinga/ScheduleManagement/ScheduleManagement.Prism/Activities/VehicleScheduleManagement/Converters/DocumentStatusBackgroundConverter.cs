﻿using FreightManagement.Business;
using ScheduleManagement.Business;
using System;
using System.Windows.Data;
using System.Windows.Media;

namespace ScheduleManagement.Prism.Activities.VehicleScheduleManagement.Converters
{
  public class DocumentStatusBackgroundConverter : IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    {
      if (value != null)
      {
        string st = ((Guid)value).ToString("b");
        switch (st)
        {
          #region "Delivary Manifests"
          case DeliveryManifest.States.Open:
            return new SolidColorBrush(Color.FromArgb(255, 255, 227, 192));
          case DeliveryManifest.States.Closed:
            return new SolidColorBrush(Color.FromArgb(255, 147, 206, 245));
          #endregion
          #region "Collection Manifests"
          case CollectionManifest.States.Open:
            return new SolidColorBrush(Color.FromArgb(255, 255, 227, 192));
          case CollectionManifest.States.Closed:
            return new SolidColorBrush(Color.FromArgb(255, 147, 206, 245));
          #endregion
          #region "Transfer Manifests"
          case TransferManifest.States.Open:
            return new SolidColorBrush(Color.FromArgb(255, 255, 227, 192));
          case TransferManifest.States.Closed:
            return new SolidColorBrush(Color.FromArgb(255, 147, 206, 245));
          #endregion
          #region "Transport Manifests"
          case TransportManifest.States.Open: //247, 253, 253
            return new SolidColorBrush(Color.FromArgb(255, 202, 236, 250));
          case TransportManifest.States.Closed:
            return new SolidColorBrush(Color.FromArgb(255, 154, 212, 237));
          case TransportManifest.States.Verified:
            return new SolidColorBrush(Color.FromArgb(255, 139, 204, 232));
          #endregion
          #region "POD Handover Manifests"
          case PODHandoverManifest.States.Open:
            return new SolidColorBrush(Color.FromArgb(255, 255, 227, 192));
          case PODHandoverManifest.States.Closed:
            return new SolidColorBrush(Color.FromArgb(255, 147, 206, 245));
          #endregion
          #region "Load Allocations"
          case LoadAllocation.States.Scheduled:
            return new SolidColorBrush(Color.FromArgb(255, 255, 255, 255));
          case LoadAllocation.States.Complete:
            return new SolidColorBrush(Color.FromArgb(255, 255, 255, 255));
          case LoadAllocation.States.Cancelled:
            return new SolidColorBrush(Color.FromArgb(255, 255, 255, 255));
          #endregion
          #region "Maintenance Booking"
          case MaintenanceBooking.States.Booked:
            return new SolidColorBrush(Color.FromArgb(255, 251, 224, 233));
          case MaintenanceBooking.States.Workshop:
            return new SolidColorBrush(Color.FromArgb(255, 255, 208, 204));
          case MaintenanceBooking.States.Testing:
            return new SolidColorBrush(Color.FromArgb(255, 255, 231, 204));
          case MaintenanceBooking.States.Cancelled:
            return new SolidColorBrush(Colors.LightGray);
          case MaintenanceBooking.States.Released:
            return new SolidColorBrush(Color.FromArgb(255, 201, 252, 210));
          #endregion
          default:
            return new SolidColorBrush(Colors.Gray);
        }
      }
      else
      {
        return null;
      }
    }

    public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    {
      throw new NotImplementedException();
    }
  }
}

﻿using ScheduleManagement.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media;

namespace ScheduleManagement.Prism.Activities.VehicleScheduleManagement.Converters
{
  public class ScheduleTypeBackgroundConverter : IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    {
      ScheduleType st = (ScheduleType)value;
      switch (st)
      {
        case ScheduleType.Delivery: return new SolidColorBrush(Color.FromArgb(255, 198, 234, 180));

        case ScheduleType.Collection: return new SolidColorBrush(Color.FromArgb(255, 255, 245, 196));

        case ScheduleType.Transport: return new SolidColorBrush(Color.FromArgb(255, 215, 241, 241));

        case ScheduleType.Transfer: return new SolidColorBrush(Color.FromArgb(255, 70, 108, 138));

        case ScheduleType.PODHandover: return new SolidColorBrush(Color.FromArgb(255, 120, 186, 88));

        case ScheduleType.Maintenance: return new SolidColorBrush(Color.FromArgb(255, 255, 221, 219));

        case ScheduleType.LoadAllocation: return new SolidColorBrush(Color.FromArgb(255, 255, 255, 255));

        default: throw new ArgumentException("Invalid Schedule Type");
      }
    }

    public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    {
      throw new NotImplementedException();
    }
  }
}

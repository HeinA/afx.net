﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace ScheduleManagement.Prism.Activities.VehicleScheduleManagement.Converters
{
  public class ExpandedVisibilityConverter : IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    {
      DisplayViewType st = (DisplayViewType)value;
      if (st == DisplayViewType.Expanded)
      {
        return Visibility.Visible;
      } else {
        return Visibility.Collapsed;
      }
    }

    public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    {
      throw new NotImplementedException();
    }
  }
}

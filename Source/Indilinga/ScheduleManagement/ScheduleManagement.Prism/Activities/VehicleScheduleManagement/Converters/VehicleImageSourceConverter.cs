﻿using System;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace ScheduleManagement.Prism.Activities.VehicleScheduleManagement.Converters
{
  public class VehicleImageSourceConverter : IMultiValueConverter
  {

    public object Convert(object[] value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    {
      string vt = (string)value[0];
      bool IsTruck = (bool)value[2];
      bool IsRefrigerated = (bool)value[3];
      string strUri2;
      try
      {
        if (IsTruck)
        {
          //switch (vt.ToUpperInvariant() ?? String.Empty)
          //{
          //  case "MERCEDES-BENZ":
          //  case "MERCEDEZ-BENZ":
          //  case "NISSAN":
          //  case "HYUNDAI":
          //  case "KIA":
          //  case "MAN":
          //  case "FIRMACO":
          //  case "TOYOTA":
          //  case "HINO":
          //  case "GRADER":
          //  case "CATERPILLAR":
          //  case "FORD":
          //    strUri2 = String.Format(@"pack://application:,,,/ScheduleManagement.Prism;component/Resources/Trucks/{0}.png", (string)value[0], (string)value[1]);
          //    return new BitmapImage(new Uri(strUri2));
          //  default:
          //    strUri2 = String.Format(@"pack://application:,,,/ScheduleManagement.Prism;component/Resources/Trucks/{0}", "default.png");
          //    return new BitmapImage(new Uri(strUri2));
          //}
          strUri2 = String.Format(@"pack://application:,,,/ScheduleManagement.Prism;component/Resources/Trucks/{0}", "default.png");
          return new BitmapImage(new Uri(strUri2));
        }
        else
        {
          if (IsRefrigerated)
          {
            strUri2 = String.Format(@"pack://application:,,,/ScheduleManagement.Prism;component/Resources/Trailers/{0}", "Refrigerated Trailer.png");
            return new BitmapImage(new Uri(strUri2));
          }
          else
          {
            strUri2 = String.Format(@"pack://application:,,,/ScheduleManagement.Prism;component/Resources/Trailers/{0}", "Flatbed Trailer.png");
            return new BitmapImage(new Uri(strUri2));
          }
        }
      }
      catch
      {
        strUri2 = String.Format(@"pack://application:,,,/ScheduleManagement.Prism;component/Resources/Trucks/{0}", "default.png");
        return new BitmapImage(new Uri(strUri2));
      }


      //if (targetType == typeof(ImageSource))
      //{
      //  if (value is string)
      //  {
      //    string str = (string)value[0];
      //    return new BitmapImage(new Uri(str, UriKind.RelativeOrAbsolute));
      //  }
      //  else if (value is Uri)
      //  {
      //    Uri uri = (Uri)value[0];
      //    return new BitmapImage(uri);
      //  }
      //}
      //return value;
    }

    public object[] ConvertBack(object value, Type[] targetType, object parameter, System.Globalization.CultureInfo culture)
    {
      throw new NotImplementedException();
    }
  }
}

﻿using ScheduleManagement.Business;
using System;
using System.Windows;
using System.Windows.Data;

namespace ScheduleManagement.Prism.Activities.VehicleScheduleManagement.Converters
{
  public class MaintenanceVisibilityConverter : IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    {
      ScheduleType st = (ScheduleType)value;
      switch (st)
      {
        case ScheduleType.Maintenance: return Visibility.Visible;

        default: return Visibility.Collapsed;
      }
      //if (st == ScheduleType.Trip)
      //{
      //  return Visibility.Collapsed;
      //}
      //else if (st == ScheduleType.Maintenance)
      //{
      //  return Visibility.Visible;
      //}
      //else if (st == ScheduleType.LoadAllocation)
      //{
      //  return Visibility.Collapsed;
      //}
      //else
      //{
      //  return Visibility.Collapsed;
      //}
    }

    public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    {
      throw new NotImplementedException();
    }
  }
}

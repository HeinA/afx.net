﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media;

namespace ScheduleManagement.Prism.Activities.VehicleScheduleManagement.Converters
{
  public class OwnedVehicleBackgroundConverter : IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    {
      if (value != null)
      {
        string st = ((bool)value).ToString();
        switch (st)
        {
          case "True":
            return new SolidColorBrush(Color.FromArgb(255, 254, 191, 206));
          default:
            return new SolidColorBrush(Colors.Transparent);
        }
      }
      else
      {
        return null;
      }
    }

    public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    {
      throw new NotImplementedException();
    }
  }
}

﻿using ScheduleManagement.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media;

namespace ScheduleManagement.Prism.Activities.VehicleScheduleManagement.Converters
{
  public class ScheduleTypeBorderConverter : IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    {
      ScheduleType st = (ScheduleType)value;
      switch(st)
      {
        case ScheduleType.Delivery: return new SolidColorBrush(Color.FromArgb(255, 80, 150, 47));

        case ScheduleType.Collection: return new SolidColorBrush(Color.FromArgb(255, 173, 122, 54));

        case ScheduleType.Transport: return new SolidColorBrush(Color.FromArgb(255, 157, 181, 200));

        case ScheduleType.Transfer: return new SolidColorBrush(Color.FromArgb(255, 40, 80, 111));

        case ScheduleType.PODHandover: return new SolidColorBrush(Color.FromArgb(255, 120, 120, 120));

        case ScheduleType.Maintenance: return new SolidColorBrush(Color.FromArgb(255, 242, 160, 173));

        case ScheduleType.LoadAllocation: return new SolidColorBrush(Color.FromArgb(255, 255, 255, 255));

        default: throw new ArgumentException("Invalid Schedule Type");
      }
    }

    public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    {
      throw new NotImplementedException();
    }
  }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScheduleManagement.Prism.Activities.VehicleScheduleManagement
{
  public enum DisplayViewType
  {
    Compact = 0
    , Expanded = 1
  }
}

﻿using Afx.Business;
using Afx.Business.Activities;
using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Activities;
using ScheduleManagement.Business;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Afx.Business.Service;
using Afx.Business.Collections;
using ScheduleManagement.Business.Service;
using ScheduleManagement.Prism.Events;

namespace ScheduleManagement.Prism.Activities.VehicleScheduleManagement
{
  public partial class VehicleScheduleManagementController : MdiCustomActivityController<VehicleScheduleManagement, VehicleScheduleManagementViewModel>
  {
    #region Constructors

    [InjectionConstructor]
    public VehicleScheduleManagementController(IController controller)
      : base(controller)
    {
    }

    #endregion

    protected override void OnRunning()
    {
      ApplicationController.EventAggregator.GetEvent<UpdateVehicleScheduleEvent>().Subscribe(OnUpdateVehicleSchedule);

      ViewModel.Timescale = Setting.GetSetting<ScheduleSetting>().DefaultTimescale;
      base.OnRunning();
    }

    private void OnUpdateVehicleSchedule(EventArgs obj)
    {
      Refresh(true);
    }

    #region OnLoaded
    
    protected override void OnLoaded()
    {
      ViewModel.IsFocused = true;
      base.OnLoaded();
    }

    #endregion


    protected override void LoadDataContext()
    {
      using (var svc = ProxyFactory.GetService<IScheduleManagementService>(SecurityContext.ServerName))
      {
        ViewModel.VehicleSchedules = svc.LoadVehicleSchedules(ViewModel.FilterStartDate, ViewModel.FilterEndDate);
      }

      base.LoadDataContext();
    }

    #region ExecuteOperation
    
    protected override void ExecuteOperation(Operation op, BusinessObject argument)
    {
      base.ExecuteOperation(op, argument);
    }
    
    #endregion
  }
}
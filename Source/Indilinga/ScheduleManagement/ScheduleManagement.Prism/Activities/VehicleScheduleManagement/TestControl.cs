﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace ScheduleManagement.Prism.Activities.VehicleScheduleManagement
{
  public class TestControl : Control
  {
    public TestControl()
    {
      this.Loaded += TestControl_Loaded;
    }

    void TestControl_Loaded(object sender, System.Windows.RoutedEventArgs e)
    {
    }
  }
}

﻿using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Activities;
using ScheduleManagement.Business;
using Microsoft.Practices.Prism.Commands;
using Afx.Business.Collections;
using FleetManagement.Business;
using Afx.Business.Data;
using Afx.Prism.RibbonMdi;
using Afx.Prism.RibbonMdi.Events;
using Afx.Business.Documents;
using Afx.Prism.Controls;
using System.Windows.Input;
using FreightManagement.Business;
using System.Threading;

namespace ScheduleManagement.Prism.Activities.VehicleScheduleManagement
{
  public partial class VehicleScheduleManagementViewModel : MdiCustomActivityViewModel<VehicleScheduleManagement>
  {
    #region Constructors

    [InjectionConstructor]
    public VehicleScheduleManagementViewModel(IController controller)
      : base(controller)
    {
      FilterStartDate = DateTime.Now;
    }

    #endregion

    protected override void Close()
    {
      base.Close();
    }

    [Dependency]
    public VehicleScheduleManagementController VehicleScheduleManagementController { get; set; }

    public IEnumerable<VehiclesViewModel> VehiclesViewModels
    {
      get
      {
        BasicCollection<Vehicle> vehicles = Cache.Instance.GetObjects<Vehicle>(false, v => v.FleetNumber != null && v.FleetNumber.Contains(VehicleFilter), v => v.Text, true);//v => v.IsTruck || v.IsTrailer, 
        if (ShowTrucks && ShowTrailers)
        {
          return ResolveViewModels<VehiclesViewModel, Vehicle>(vehicles);
        }
        else if (ShowTrucks)
        {
          return ResolveViewModels<VehiclesViewModel, Vehicle>(vehicles.Where(v => v.IsTruck).ToList());
        }
        else if (ShowTrailers)
        {
          return ResolveViewModels<VehiclesViewModel, Vehicle>(vehicles.Where(v => v.IsTrailer).ToList());
        } 
        else
        {
          return ResolveViewModels<VehiclesViewModel, Vehicle>(vehicles.Where(v => !v.IsTrailer && !v.IsTruck).ToList());
        }
      }
    }

    BasicCollection<VehicleSchedule> mVehicleSchedules;
    public BasicCollection<VehicleSchedule> VehicleSchedules
    {
      get { return mVehicleSchedules; }
      set
      {
        mVehicleSchedules = value;
        OnPropertyChanged("VehicleViewModels");
      }
    }

    #region Timescale Timescale

    public const string TimescaleProperty = "Timescale";
    Timescale mTimescale = Timescale.Week;
    public Timescale Timescale
    {
      get { return mTimescale; }
      set
      {
        if (SetProperty<Timescale>(ref mTimescale, value))
        {
          SetEndDate();
        }
      }
    }

    #endregion

    #region ViewDisplayType ViewDisplayType

    public const string ViewDisplayTypeProperty = "ViewDisplayType";
    DisplayViewType mViewDisplayType = DisplayViewType.Expanded;
    public DisplayViewType ViewDisplayType
    {
      get { return mViewDisplayType; }
      set
      {
        if (SetProperty<DisplayViewType>(ref mViewDisplayType, value))
        {
          SwitchScheduleDisplayType();
        }
      }
    }

    #endregion

    #region DateTime FilterStartDate

    public const string FilterStartDateProperty = "FilterStartDate";
    DateTime mFilterStartDate;
    public DateTime FilterStartDate
    {
      get { return mFilterStartDate; }
      set
      {
        if (SetProperty<DateTime>(ref mFilterStartDate, value.Date))
        {
          SetEndDate();
        }
      }
    }

    #endregion

    /// <summary>
    /// Set the end date according to filter start date and time scale.
    /// </summary>
    void SetEndDate()
    {
      switch (Timescale)
      {
        case Timescale.Day:
          FilterEndDate = FilterStartDate.AddDays(1);
          break;

        case Timescale.Week:
          FilterEndDate = FilterStartDate.AddDays(7);
          break;

        case Timescale.TwoWeeks:
          FilterEndDate = FilterStartDate.AddDays(14);
          break;

        case Timescale.Month:
          FilterEndDate = FilterStartDate.AddMonths(1);
          break;
      }

      if (VehicleScheduleManagementController != null)
      {
        using (new WaitCursor())
        {
          VehicleScheduleManagementController.Refresh(true);
          SelectedVehicleSchedule = null;
        }
      }
    }

    /// <summary>
    /// Change between compact and expanded view.
    /// </summary>
    void SwitchScheduleDisplayType()
    {
      switch (ViewDisplayType)
      {
        case Activities.VehicleScheduleManagement.DisplayViewType.Compact:
          DisplayViewHeight = 24;
          break;
        case Activities.VehicleScheduleManagement.DisplayViewType.Expanded:
          DisplayViewHeight = 50;
          break;
      }

      if (VehicleScheduleManagementController != null)
      {
        using (new WaitCursor())
        {
          VehicleScheduleManagementController.Refresh(true);
          SelectedVehicleSchedule = null;
        }
      }
    }

    #region DateTime FilterEndDate

    public const string FilterEndDateProperty = "FilterEndDate";
    DateTime mFilterEndDate;
    public DateTime FilterEndDate
    {
      get { return mFilterEndDate; }
      set { SetProperty<DateTime>(ref mFilterEndDate, value.Date); }
    }

    #endregion

    #region string VehicleFilter

    public const string VehicleFilterProperty = "VehicleFilter";
    string mVehicleFilter = String.Empty;
    public string VehicleFilter
    {
      get { return mVehicleFilter; }
      set
      {
        if (SetProperty<string>(ref mVehicleFilter, value))
        {
          OnPropertyChanged("VehiclesViewModels");
        }
      }
    }

    #endregion


    #region DateTime? ActiveDate

    public const string ActiveDateProperty = "ActiveDate";
    DateTime? mActiveDate;
    public DateTime? ActiveDate
    {
      get { return mActiveDate; }
      set 
      {
        SetProperty<DateTime?>(ref mActiveDate, value);
      }
    }

    #endregion

    #region string ActiveOwnerGuid

    public const string ActiveOwnerGuidProperty = "ActiveOwnerGuid";
    string mActiveOwnerGuid;
    public string ActiveOwnerGuid
    {
      get { return mActiveOwnerGuid; }
      set { SetProperty<string>(ref mActiveOwnerGuid, value); }
    }

    #endregion


    #region VehicleSchedule ActiveViewModel

    public const string ActiveViewModelProperty = "ActiveViewModel";
    VehicleSchedule mActiveViewModel;
    public VehicleSchedule ActiveViewModel
    {
      get { return mActiveViewModel; }
      set { SetProperty<VehicleSchedule>(ref mActiveViewModel, value); }
    }


    public void MouseOver(object sender, MouseEventArgs e)
    {
    }


    public void TextBlockMouseLeftButtonDown(object sender, MouseEventArgs e)
    {
      //var tb = sender as TextBlock;
      //if (tb != null)
      //{
      //  MyViewModel vm = tb.DataContext as MyViewModel;

      //  if (vm != null && TextBlockMouseLeftButtonDownCommand != null
      //      && TextBlockMouseLeftButtonDownCommand.CanExecute(null))
      //  {
      //      vm.TextBlockMouseLeftButtonDownCommand.Execute(null)
      //  }
      //}
    }




    #region DelegateCommand ChangeActiveViewModelCommand

    DelegateCommand mChangeActiveViewModelCommand;
    public DelegateCommand ChangeActiveViewModelCommand
    {
      get { return mChangeActiveViewModelCommand ?? (mChangeActiveViewModelCommand = new DelegateCommand(ExecuteChangeActiveViewModel, CanExecuteChangeActiveViewModel)); }
    }

    bool CanExecuteChangeActiveViewModel()
    {
      return true;
    }

    void ExecuteChangeActiveViewModel()
    {
    }

    #endregion


    #endregion

    #region int DisplayViewHeight

    public const string DisplayViewHeightProperty = "DisplayViewHeight";
    int mDisplayViewHeight;
    public int DisplayViewHeight
    {
      get { return mDisplayViewHeight; }
      set { SetProperty<int>(ref mDisplayViewHeight, value); }
    }

    #endregion

    #region bool ShowTrucks

    public const string ShowTrucksProperty = "ShowTrucks";
    bool mShowTrucks = true;
    public bool ShowTrucks
    {
      get { return mShowTrucks; }
      set
      {
        if (SetProperty<bool>(ref mShowTrucks, value))
        {
          OnPropertyChanged("VehiclesViewModels");
        }
      }
    }

    #endregion

    #region bool ShowTrailers

    public const string ShowTrailersProperty = "ShowTrailers";
    bool mShowTrailers = false;
    public bool ShowTrailers
    {
      get { return mShowTrailers; }
      set
      {
        if (SetProperty<bool>(ref mShowTrailers, value))
        {
          OnPropertyChanged("VehiclesViewModels");
        }
      }
    }

    #endregion

    #region VehicleScheduleGanttPanelViewModel SelectedVehicleSchedule

    public const string SelectedVehicleProperty = "SelectedVehicleSchedule";
    VehicleSchedule mSelectedVehicleSchedule;
    public VehicleSchedule SelectedVehicleSchedule
    {
      get { return mSelectedVehicleSchedule; }
      set { SetProperty<VehicleSchedule>(ref mSelectedVehicleSchedule, value); }
    }

    #endregion
    
  }

}

﻿using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Documents;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using Geographics.Business;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Unity;
using ScheduleManagement.Business;
using System;
using System.Linq;

namespace ScheduleManagement.Prism.Activities.VehicleScheduleManagement
{
  public class VehicleScheduleGanttPanelViewModel : ViewModel<VehicleSchedule>
  {
    #region Constructors

    [InjectionConstructor]
    public VehicleScheduleGanttPanelViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    public new VehiclesViewModel Parent
    {
      get { return (VehiclesViewModel)base.Parent; }
    }

    #region bool IsSelected

    public const string IsSelectedProperty = "IsSelected";
    bool mIsSelected;
    public bool IsSelected
    {
      get { return mIsSelected; }
      set
      {
        if (SetProperty<bool>(ref mIsSelected, value))
        {
          if (value)
          {
            Parent.Parent.SelectedVehicleSchedule = Model;
          }
          else
          {
            if (Model.Equals(Parent.Parent.SelectedVehicleSchedule))
            {
              Parent.Parent.SelectedVehicleSchedule = null;
            }
          }
        }
      }
    }

    #endregion

    #region decimal TotalToll

    public const string TotalTollProperty = "TotalToll";
    public decimal TotalToll
    {
      get 
      {
        return this.Model.TollFees;
      }
    }

    #endregion

    #region decimal TotalRate

    public const string TotalRateProperty = "TotalRate";
    public decimal TotalRate
    {
      get { return this.Model.Rates; }
    }

    #endregion

    #region decimal TotalTarpaulin

    public const string TotalTarpaulinProperty = "TotalTarpaulin";
    public decimal TotalTarpaulin
    {
      get { return this.Model.TarpaullinFees; }
    }

    #endregion

    #region decimal TotalNightOut

    public const string TotalNightOutProperty = "TotalNightOut";
    public decimal TotalNightOut
    {
      get { return this.Model.NightOutFees; }
    }

    #endregion

    #region string DocumentStatus

    public const string DocumentStatusProperty = "DocumentStatus";
    public string DocumentStatus
    {
      get 
      {

        //Document d = Cache.Instance.GetObjects<VehicleBooking>(false, vb => vb.DocumentNumber, true).FirstOrDefault() as Document;
        //if (d != null)
        //{
        //  return d.State.ToString();
        //}
        //else
        //{
        //  return "Unknown";
        //}
        return "";
      }
    }

    #endregion

    #region Location Origin

    public const string OriginProperty = "Origin";
    public Location Origin
    {
      get
      {
        if (this.Model.Route != null)
        {
          return this.Model.Route.Origin;
        }
        else
        {
          return new Location(true);
        }
      }
    }

    #endregion

    #region Location Destination

    public const string DestinationProperty = "Destination";
    public Location Destination
    {
      get 
      {
        if (this.Model.Route != null)
        {
          return this.Model.Route.Destination;
        } 
        else
        {
          return new Location(true);
        }
      }
    }

    #endregion

    private bool IsDateWithin(DateTime? startOfRangeDate, DateTime? endOfRangeDate, DateTime? testDate)
    {
      //Old Date test.
      //if (testDate >= startOfRangeDate && testDate <= endOfRangeDate) return true;
      if (testDate > startOfRangeDate && testDate < endOfRangeDate) return true;
      return false;
    }

    #region bool ScheduleConflicts

    public const string ScheduleConflictsProperty = "ScheduleConflicts";
    bool mScheduleConflicts;
    public bool ScheduleConflicts
    {
      get
      {
        mScheduleConflicts = false;

        //BasicCollection<VehicleSchedule> Schedules = Parent.VehicleSchedules;
        foreach (
          VehicleSchedule Schedule in Parent.VehicleSchedules.Where(sc => 
            (
              #region "Model.ETD GT Vehicle List of Schedules excluding Model.Document ETD & Model.ETD LT ETA"
                (
                  (
                    IsDateWithin(sc.RenderLoadingDateTime.Value.Date, sc.RenderOffloadingDateTime.Value.Date, this.Model.RenderLoadingDateTime.Value.Date)
                  )
              #endregion
                  ||
              #region "Model.ETA GT Vehicle List of Schedules excluding Model.Document ETD && Model.ETA LT ETA"
                  (
                    IsDateWithin(sc.RenderLoadingDateTime.Value.Date, sc.RenderOffloadingDateTime.Value.Date, this.Model.RenderOffloadingDateTime.Value.Date)
                  )
                )
              #endregion
            ) 
            && 
            sc.DocumentNumber != this.Model.DocumentNumber
          ))
        {
            mScheduleConflicts = true;
            break;
        }
        
        return mScheduleConflicts;
      }

    }

    #endregion



    #region DelegateCommand OpenDocumentCommand

    DelegateCommand mOpenDocumentCommand;
    public DelegateCommand OpenDocumentCommand
    {
      get { return mOpenDocumentCommand ?? (mOpenDocumentCommand = new DelegateCommand(ExecuteOpenDocument, CanExecuteOpenDocument)); }
    }

    bool CanExecuteOpenDocument()
    {
      return true;
    }

    void ExecuteOpenDocument()
    {
      //switch (this.Model.ScheduleType)
      //{
      //  case ScheduleType.Trip:
      //    //TripSheet ts = new TripSheet();
      //    break;
      //  case ScheduleType.LoadAllocation:
      //    break;
      //  case ScheduleType.Maintenance:
      //    break;
      //}
      try
      {
        MdiApplicationController.Current.EditDocument(this.Model.DocumentType, this.Model.DocumentIdentifier);
      }
      catch
      {
        throw;
      }
    }

    #endregion
  }
}

﻿using Afx.Business.Activities;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using ContactManagement.Business;
using FleetManagement.Business;
using FreightManagement.Business;
using ScheduleManagement.Business;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Unity;
using ScheduleManagement.Business;
using ScheduleManagement.Prism.Dialogs.ReAssignBookingsDialog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Controls;
using Afx.Business;
using ScheduleManagement.Prism.Dialogs.ScheduleWizardDialog;

namespace ScheduleManagement.Prism.Activities.VehicleScheduleManagement
{
  public class VehiclesViewModel : ViewModel<Vehicle>
  {
    #region Constructors

    [InjectionConstructor]
    public VehiclesViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    [Dependency]
    public VehicleScheduleManagementController VehicleScheduleManagementController { get; set; }

    public new VehicleScheduleManagementViewModel Parent
    {
      get { return (VehicleScheduleManagementViewModel)base.Parent; }
    }

    //BasicCollection<VehicleSchedule> mVehicleSchedules = new BasicCollection<VehicleSchedule>();
    public IEnumerable<VehicleSchedule> VehicleSchedules
    {
      //get { return mVehicleSchedules; }
      get { return Parent.VehicleSchedules.Where(vs1 => vs1.Vehicle.Equals(this.Model)); }
    }

    public IEnumerable<VehicleScheduleGanttPanelViewModel> VehicleScheduleViewModels
    {
      get
      {
        if (Parent.VehicleSchedules == null) return null;
        //VehicleSchedules.Clear();
        //foreach (var vs in Parent.VehicleSchedules.Where(vs1 => vs1.Vehicle.Equals(this.Model)))
        //{
        //  VehicleSchedules.Add(vs);
        //}
        IEnumerable<VehicleScheduleGanttPanelViewModel> ret = ResolveViewModels<VehicleScheduleGanttPanelViewModel, VehicleSchedule>(VehicleSchedules.ToList());
        return ret;
      }
    }

    #region Vehicle Vehicle

    public const string VehicleProperty = "Vehicle";
    Vehicle mVehicle;
    public Vehicle Vehicle
    {
      get
      {
        return this.Model;
      }
    }

    #endregion

    #region DelegateCommand ChangeActiveViewModelCommand

    DelegateCommand mChangeActiveViewModelCommand;
    public DelegateCommand ChangeActiveViewModelCommand
    {
      get { return mChangeActiveViewModelCommand ?? (mChangeActiveViewModelCommand = new DelegateCommand(ExecuteChangeActiveViewModel, CanExecuteChangeActiveViewModel)); }
    }

    bool CanExecuteChangeActiveViewModel()
    {
      return true;
    }

    void ExecuteChangeActiveViewModel()
    {
    }

    #endregion

    #region string ActiveDriverLandlinePhone

    public const string ActiveDriverLandlinePhoneProperty = "ActiveDriverLandlinePhone";
    string mActiveDriverLandlinePhone = "UNKNOWN NR";
    public string ActiveDriverLandlinePhone
    {
      get
      {
        try
        { 
          //Contact Driver = null;
          //BasicCollection<VehicleSchedule> Schedules = Parent.VehicleSchedules;
          //foreach (VehicleSchedule Schedule in Schedules.Where(sc => (sc.OriginSTD.Date <= DateTime.Today.Date && sc.OriginSTA.Date >= DateTime.Today.Date) && sc.ScheduleType == ScheduleType.Trip && sc.Vehicle.RegistrationNumber.ToUpperInvariant().Equals(this.Model.RegistrationNumber.ToUpperInvariant())))
          //{
          //  Driver = Schedule.Driver;
          //  break;
          //}
          if (mActiveDriver != null)
          {
            Employee c = mActiveDriver as Employee;
            if (mActiveDriver != null)
            {
              Telephone mPhone;
              if (typeof(ContactManagement.Business.SimpleContact).IsAssignableFrom(mActiveDriver.GetType()))
              {
                SimpleContact ct = mActiveDriver as SimpleContact;
                mPhone = new Telephone();
                mPhone.TelephoneNumber = ct.TelephoneNumber;
              } 
              else
              {
                mPhone = c.TelephoneNumbers.Where(t => t.TelephoneType.SystemTelephoneType == SystemTelephoneType.Landline).FirstOrDefault();
              }
              if (mPhone != null) mActiveDriverMobilePhone = mPhone.TelephoneNumber;
              return mActiveDriverLandlinePhone;
            }
            else
            {
              return "UNKNOWN NR";
            }
          }
          else
          {
            return "UNKNOWN NR";
          }
        }
        catch (Exception ex)
        {
          throw (ex);
        }
      }
    }

    #endregion

    #region string ActiveDriverMobilePhone

    public const string ActiveDriverMobilePhoneProperty = "ActiveDriverMobilePhone";
    string mActiveDriverMobilePhone = "UNKNOWN NR";
    public string ActiveDriverMobilePhone
    {
      get
      {
        try
        {
          //Contact Driver = null;
          //BasicCollection<VehicleSchedule> Schedules = Parent.VehicleSchedules;
          //foreach (VehicleSchedule Schedule in Schedules.Where(sc => (sc.OriginSTD.Date <= DateTime.Today.Date && sc.OriginSTA.Date >= DateTime.Today.Date) && sc.ScheduleType == ScheduleType.Trip && sc.Vehicle.RegistrationNumber.ToUpperInvariant().Equals(this.Model.RegistrationNumber.ToUpperInvariant())))
          //{
          //  Driver = Schedule.Driver;
          //  break;
          //}
          if (mActiveDriver != null)
          {
            Employee c = mActiveDriver as Employee;
            if (mActiveDriver != null)
            {
              Telephone mPhone;
              if (typeof(ContactManagement.Business.SimpleContact).IsAssignableFrom(mActiveDriver.GetType()))
              {
                SimpleContact ct = mActiveDriver as SimpleContact;
                mPhone = new Telephone();
                mPhone.TelephoneNumber = ct.TelephoneNumber;
              }
              else
              {
                mPhone = c.TelephoneNumbers.Where(t => t.TelephoneType.SystemTelephoneType == SystemTelephoneType.Mobile).FirstOrDefault();
              }
              if (mPhone != null) mActiveDriverMobilePhone = mPhone.TelephoneNumber;
              return mActiveDriverLandlinePhone;
            }
            else
            {
              return "UNKNOWN NR";
            }
          }
          else
          {
            return "UNKNOWN NR";
          }
        }
        catch (Exception ex)
        {
          throw (ex);
        }
      }
    }

    #endregion

    #region Contact ActiveDriver

    public const string ActiveDriverProperty = "ActiveDriver";
    private Contact mActiveDriver;
    public Contact ActiveDriver
    {
      get 
      {
        BasicCollection<VehicleSchedule> Schedules = Parent.VehicleSchedules;
        foreach (VehicleSchedule Schedule in Schedules.Where(sc => (!BusinessObject.IsNull(sc.Vehicle) && sc.Vehicle != null) && (sc.ScheduledLoadingDate.Value.Date <= DateTime.Today.Date && sc.ScheduledOffloadingDate.Value.Date >= DateTime.Today.Date) && sc.ScheduleType != ScheduleType.Maintenance && sc.Vehicle.RegistrationNumber.ToUpperInvariant().Equals(this.Model.RegistrationNumber.ToUpperInvariant())))
        {
          mActiveDriver = Schedule.Driver;
          return mActiveDriver;
        }
        return null;
      }
    }

    #endregion

    #region bool VehicleStatus

    public const string VehicleStatusProperty = "VehicleStatus";
    public bool VehicleStatus
    {
      get
      {
        BasicCollection<VehicleSchedule> Schedules = Parent.VehicleSchedules;
        foreach (VehicleSchedule Schedule in Schedules.Where(sc => (!BusinessObject.IsNull(sc.Vehicle) && sc.Vehicle != null) && (sc.ScheduledLoadingDate.Value.Date <= DateTime.Today.Date && sc.ScheduledOffloadingDate.Value.Date >= DateTime.Today.Date) && sc.Vehicle.RegistrationNumber.ToUpperInvariant().Equals(this.Model.RegistrationNumber.ToUpperInvariant())))
        {
          if (Schedule.ScheduleType == ScheduleType.Maintenance) return true;
        }
        return false;
      }
    }

    #endregion

    #region Vehicle ActiveTrailer

    public const string ActiveTrailerProperty = "ActiveTrailer";
    string mActiveTrailer = "NO TRAILER";
    public string ActiveTrailer
    {
      get
      {
        BasicCollection<VehicleSchedule> Schedules = Parent.VehicleSchedules;

        string ScheduleNumber = "";
        foreach (VehicleSchedule Schedule in Schedules.Where(sc => (sc.ScheduledLoadingDate.Value.Date <= DateTime.Today.Date && sc.ScheduledOffloadingDate.Value.Date >= DateTime.Today.Date) && sc.Vehicle.RegistrationNumber == this.Model.RegistrationNumber))
        {
          ScheduleNumber = Schedule.DocumentNumber;
          break;
        }
        bool first = true;
        foreach (VehicleSchedule Schedule in Schedules.Where(sc => (sc.ScheduledLoadingDate.Value.Date <= DateTime.Today.Date && sc.ScheduledOffloadingDate.Value.Date >= DateTime.Today.Date) && sc.DocumentNumber == ScheduleNumber))
        {
          if (Schedule.Vehicle.RegistrationNumber != this.Model.RegistrationNumber)
          {
            if (first)
            {
              first = false;
              mActiveTrailer = "";
            } 
            mActiveTrailer +=  Schedule.Vehicle.FleetNumber + "\t" + Schedule.Vehicle.RegistrationNumber + "\r\n";
          }
        }
        return mActiveTrailer;
      }
    }

    #endregion

    #region string ActiveTrailerType

    public const string ActiveTrailerTypeProperty = "ActiveTrailerType";
    string mActiveTrailerType = "";
    public string ActiveTrailerType
    {
      get
      {
        BasicCollection<VehicleSchedule> Schedules = Parent.VehicleSchedules;
        
        string ScheduleNumber = "";
        foreach (VehicleSchedule Schedule in Schedules.Where(sc => (sc.ScheduledLoadingDate.Value.Date <= DateTime.Today.Date && sc.ScheduledOffloadingDate.Value.Date >= DateTime.Today.Date) && sc.Vehicle.RegistrationNumber == this.Model.RegistrationNumber))
        {
          ScheduleNumber = Schedule.DocumentNumber;
        }
        bool first = true;
        foreach (VehicleSchedule Schedule in Schedules.Where(sc => (sc.ScheduledLoadingDate.Value.Date <= DateTime.Today.Date && sc.ScheduledOffloadingDate.Value.Date >= DateTime.Today.Date) && sc.DocumentNumber == ScheduleNumber))
        {
          if (Schedule.Vehicle.RegistrationNumber != this.Model.RegistrationNumber)
          {
            if (first)
            {
              first = false;
              mActiveTrailerType = "";
            }
            mActiveTrailerType += Schedule.Vehicle.MakeModel+ "\r\n";
          }
        }
        return mActiveTrailerType;
      }
    }

    #endregion



    #region Location ActiveOrigin

    public const string ActiveOriginProperty = "ActiveOrigin";
    string mActiveOrigin = "UNKNOWN";
    public string ActiveOrigin
    {
      get
      {
        BasicCollection<VehicleSchedule> Schedules = Parent.VehicleSchedules;

        string ScheduleNumber = "";
        foreach (VehicleSchedule Schedule in Schedules.Where(sc => (sc.ScheduledLoadingDate.Value.Date <= DateTime.Today.Date && sc.ScheduledOffloadingDate.Value.Date >= DateTime.Today.Date) && sc.Vehicle.RegistrationNumber == this.Model.RegistrationNumber))
        {
          ScheduleNumber = Schedule.DocumentNumber;
          break;
        }
        bool first = true;
        foreach (VehicleSchedule Schedule in Schedules.Where(sc => (sc.ScheduledLoadingDate.Value.Date <= DateTime.Today.Date && sc.ScheduledOffloadingDate.Value.Date >= DateTime.Today.Date) && sc.DocumentNumber == ScheduleNumber))
        {
          if (Schedule.Vehicle.RegistrationNumber == this.Model.RegistrationNumber)
          {
            if (first)
            {
              first = false;
              mActiveOrigin = "";
            }
            mActiveOrigin += Schedule.Origin.Name; //.Vehicle.FleetNumber + "\t" + Schedule.Vehicle.RegistrationNumber + "\r\n";
          }
        }
        return mActiveOrigin;
      }
    }

    #endregion

    #region Location ActiveDestination

    public const string ActiveDestinationProperty = "ActiveDestination";
    string mActiveDestination = "UNKNOWN";
    public string ActiveDestination
    {
      get
      {
        BasicCollection<VehicleSchedule> Schedules = Parent.VehicleSchedules;

        string ScheduleNumber = "";
        foreach (VehicleSchedule Schedule in Schedules.Where(sc => (sc.ScheduledLoadingDate.Value.Date <= DateTime.Today.Date && sc.ScheduledOffloadingDate.Value.Date >= DateTime.Today.Date) && sc.Vehicle.RegistrationNumber == this.Model.RegistrationNumber))
        {
          ScheduleNumber = Schedule.DocumentNumber;
          break;
        }
        bool first = true;
        foreach (VehicleSchedule Schedule in Schedules.Where(sc => (sc.ScheduledLoadingDate.Value.Date <= DateTime.Today.Date && sc.ScheduledOffloadingDate.Value.Date >= DateTime.Today.Date) && sc.DocumentNumber == ScheduleNumber))
        {
          if (Schedule.Vehicle.RegistrationNumber == this.Model.RegistrationNumber)
          {
            if (first)
            {
              first = false;
              mActiveDestination = "";
            }
            mActiveDestination += Schedule.Destination.Name; //.Vehicle.FleetNumber + "\t" + Schedule.Vehicle.RegistrationNumber + "\r\n";
          }
        }
        return mActiveDestination;
      }
    }

    #endregion





    #region DelegateCommand AddTransferManifest

    DelegateCommand mAddTransferManifest;
    public DelegateCommand AddTransferManifest
    {
      get { return mAddTransferManifest ?? (mAddTransferManifest = new DelegateCommand(ExecuteAddTransferManifest, CanExecuteAddTransferManifest)); }
    }

    bool CanExecuteAddTransferManifest()
    {
      return (Parent.ActiveDate != null);
    }

    void ExecuteAddTransferManifest()
    {
      if (Parent.ActiveDate == null) return;
      TransferManifest ts = new TransferManifest();
      MdiApplicationController.Current.EditDocument(ts);
    }

    #endregion



    #region DelegateCommand AddTransportManifest

    DelegateCommand mAddTransportManifest;
    public DelegateCommand AddTransportManifest
    {
      get { return mAddTransportManifest ?? (mAddTransportManifest = new DelegateCommand(ExecuteAddTransportManifest, CanExecuteAddTransportManifest)); }
    }

    bool CanExecuteAddTransportManifest()
    {
      return (Parent.ActiveDate != null);
    }

    void ExecuteAddTransportManifest()
    {
      if (Parent.ActiveDate == null) return;
      TransportManifest ts = new TransportManifest();
      ts.ScheduledLoadingDate = (DateTime)Parent.ActiveDate;
      MdiApplicationController.Current.EditDocument(ts);
    }

    #endregion



    #region DelegateCommand AddCollectionManifest

    DelegateCommand mAddCollectionManifest;
    public DelegateCommand AddCollectionManifest
    {
      get { return mAddCollectionManifest ?? (mAddCollectionManifest = new DelegateCommand(ExecuteAddCollectionManifest, CanExecuteAddCollectionManifest)); }
    }

    bool CanExecuteAddCollectionManifest()
    {
      return (Parent.ActiveDate != null);
    }

    void ExecuteAddCollectionManifest()
    {
      if (Parent.ActiveDate == null) return;
      CollectionManifest ts = new CollectionManifest();
      MdiApplicationController.Current.EditDocument(ts);
    }

    #endregion



    #region DelegateCommand AddDeliveryManifest

    DelegateCommand mAddDeliveryManifest;
    public DelegateCommand AddDeliveryManifest
    {
      get { return mAddDeliveryManifest ?? (mAddDeliveryManifest = new DelegateCommand(ExecuteAddDeliveryManifest, CanExecuteAddDeliveryManifest)); }
    }

    bool CanExecuteAddDeliveryManifest()
    {
      return (Parent.ActiveDate != null);
    }

    void ExecuteAddDeliveryManifest()
    {
      if (Parent.ActiveDate == null) return;
      DeliveryManifest ts = new DeliveryManifest();
      MdiApplicationController.Current.EditDocument(ts);
    }

    #endregion



    #region DelegateCommand AddPODHandoverManifest

    DelegateCommand mAddPODHandoverManifest;
    public DelegateCommand AddPODHandoverManifest
    {
      get { return mAddPODHandoverManifest ?? (mAddPODHandoverManifest = new DelegateCommand(ExecuteAddPODHandoverManifest, CanExecuteAddPODHandoverManifest)); }
    }

    bool CanExecuteAddPODHandoverManifest()
    {
      return (Parent.ActiveDate != null);
    }

    void ExecuteAddPODHandoverManifest()
    {
      if (Parent.ActiveDate == null) return;
      PODHandoverManifest ts = new PODHandoverManifest();
      MdiApplicationController.Current.EditDocument(ts);
    }

    #endregion



    #region DelegateCommand AddTripScheduleCommand

    DelegateCommand mAddTripScheduleCommand;
    public DelegateCommand AddTripScheduleCommand
    {
      get { return mAddTripScheduleCommand ?? (mAddTripScheduleCommand = new DelegateCommand(ExecuteAddTripSchedule, CanExecuteAddTripSchedule)); }
    }

    bool CanExecuteAddTripSchedule()
    {
      return (Parent.ActiveDate != null);
    }

    void ExecuteAddTripSchedule()
    {
      if (Parent.ActiveDate == null) return;
      Tripsheet ts = new Tripsheet();
      MdiApplicationController.Current.EditDocument(ts);
    }

    #endregion



    #region DelegateCommand AddNewMaintenanceBookingCommand

    DelegateCommand mAddNewMaintenanceBookingCommand;
    public DelegateCommand AddNewMaintenanceBookingCommand
    {
      get { return mAddNewMaintenanceBookingCommand ?? (mAddNewMaintenanceBookingCommand = new DelegateCommand(ExecuteAddNewMaintenanceBooking, CanExecuteAddNewMaintenanceBooking)); }
    }

    bool CanExecuteAddNewMaintenanceBooking()
    {
      return (Parent.ActiveDate != null);
    }

    void ExecuteAddNewMaintenanceBooking()
    {
      if (Parent.ActiveDate == null) return;
      MaintenanceBooking mb = new MaintenanceBooking();
      mb.STD = (DateTime)Parent.ActiveDate;
      if (this.Model != null) mb.Vehicles.Add(this.Model);
      MdiApplicationController.Current.EditDocument(mb);
    }

    #endregion



    #region DelegateCommand AddNewLoadAllocationCommand

    DelegateCommand mAddNewLoadAllocationCommand;
    public DelegateCommand AddNewLoadAllocationCommand
    {
      get { return mAddNewLoadAllocationCommand ?? (mAddNewLoadAllocationCommand = new DelegateCommand(ExecuteAddNewLoadAllocation, CanExecuteAddNewLoadAllocation)); }
    }

    bool CanExecuteAddNewLoadAllocation()
    {
      return (Parent.ActiveDate != null);
    }

    void ExecuteAddNewLoadAllocation()
    {
      if (Parent.ActiveDate == null) return;
      LoadAllocation la = new LoadAllocation();
      la.LoadingDate = (DateTime)Parent.ActiveDate;
      if (this.Model != null) la.Vehicles.Add(this.Model);
      MdiApplicationController.Current.EditDocument(la);
    }

    #endregion



    #region DelegateCommand ReAllocateBookingsCommand

    DelegateCommand mReAllocateBookingsCommand;
    public DelegateCommand ReAllocateBookingsCommand
    {
      get { return mReAllocateBookingsCommand ?? (mReAllocateBookingsCommand = new DelegateCommand(ExecuteReAllocateBookings, CanExecuteReAllocateBookings)); }
    }

    bool CanExecuteReAllocateBookings()
    {
      return true;
    }

    void ExecuteReAllocateBookings()
    {
      ReAssignBookingsDialogController rabdc = Controller.GetCreateChildController<ReAssignBookingsDialogController>(ReAssignBookingsDialogController.ReAssignBookingsDialogControllerKey);
      rabdc.SelectedVehicle = this.Model;
      rabdc.Run();
    }

    #endregion



    #region DelegateCommand FastScheduleCommand

    DelegateCommand mFastScheduleCommand;
    public DelegateCommand FastScheduleCommand
    {
      get { return mFastScheduleCommand ?? (mReAllocateBookingsCommand = new DelegateCommand(ExecuteFastScheduleCommand, CanExecuteFastScheduleCommand)); }
    }

    bool CanExecuteFastScheduleCommand()
    {
      return true;
    }

    void ExecuteFastScheduleCommand()
    {
      if ((DateTime?)Parent.ActiveDate == null) { System.Media.SystemSounds.Exclamation.Play(); return; }
      ScheduleWizardDialogController swdc = Controller.GetCreateChildController<ScheduleWizardDialogController>(ScheduleWizardDialogController.ScheduleWizardDialogControllerKey);
      swdc.ScheduledLoadingDate = (DateTime)Parent.ActiveDate;
      swdc.VehicleScheduleManagementController = VehicleScheduleManagementController;
      swdc.SelectedVehicle = this.Model;
      swdc.Run();
    }

    #endregion



    #region DelegateCommand EditVehicleCommand

    DelegateCommand mEditVehicleCommand;
    public DelegateCommand EditVehicleCommand
    {
      get { return mEditVehicleCommand ?? (mEditVehicleCommand = new DelegateCommand(ExecuteEditVehicle, CanExecuteEditVehicle)); }
    }

    bool CanExecuteEditVehicle()
    {
      return true;
    }

    void ExecuteEditVehicle()
    {
      MdiApplicationController.Current.ExecuteActivity(Cache.Instance.GetObject<Activity>(FleetManagement.Prism.Activities.OwnedVehicleManagement.OwnedVehicleManagement.Key), this.Model.GlobalIdentifier);
    }

    #endregion



    #region DelegateCommand AssignDefaultDriverCommand

    DelegateCommand mAssignDefaultDriverCommand;
    public DelegateCommand AssignDefaultDriverCommand
    {
      get { return mAssignDefaultDriverCommand ?? (mAssignDefaultDriverCommand = new DelegateCommand(ExecuteAssignDefaultDriver, CanExecuteAssignDefaultDriver)); }
    }

    bool CanExecuteAssignDefaultDriver()
    {
      return true;
    }

    void ExecuteAssignDefaultDriver()
    {
    }

    #endregion


    #region DelegateCommand ContextMenuOpeningCommand

    public DelegateCommand<ContextMenuEventArgs> mContextMenuOpeningCommand;
    public DelegateCommand<ContextMenuEventArgs> ContextMenuOpeningCommand
    {
      get { return mContextMenuOpeningCommand ?? (mContextMenuOpeningCommand = new DelegateCommand<ContextMenuEventArgs>(OnContextMenuOpening)); }
    }

    void OnContextMenuOpening(ContextMenuEventArgs e)
    {
      try
      {
        AddNewLoadAllocationCommand.RaiseCanExecuteChanged();
        AddNewMaintenanceBookingCommand.RaiseCanExecuteChanged();
        AddTripScheduleCommand.RaiseCanExecuteChanged();
      }
      catch
      {
        throw;
      }
    }

    #endregion

    #region DelegateCommand VehicleContextMenuOpeningCommand

    public DelegateCommand<ContextMenuEventArgs> mVehicleContextMenuOpeningCommand;
    public DelegateCommand<ContextMenuEventArgs> VehicleContextMenuOpeningCommand
    {
      get { return mVehicleContextMenuOpeningCommand ?? (mVehicleContextMenuOpeningCommand = new DelegateCommand<ContextMenuEventArgs>(OnVehicleContextMenuOpening)); }
    }

    void OnVehicleContextMenuOpening(ContextMenuEventArgs e)
    {
      try
      {
        
      }
      catch
      {
        throw;
      }
    }

    #endregion

  }
}

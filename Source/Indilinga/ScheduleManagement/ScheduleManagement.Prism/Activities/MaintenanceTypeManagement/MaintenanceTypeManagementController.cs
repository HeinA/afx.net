﻿using Afx.Business;
using Afx.Business.Activities;
using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Activities;
using ScheduleManagement.Business;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScheduleManagement.Prism.Activities.MaintenanceTypeManagement
{
  public partial class MaintenanceTypeManagementController : MdiContextualActivityController<MaintenanceTypeManagement, MaintenanceTypeManagementViewModel>
  {
    #region Constructors

    [InjectionConstructor]
    public MaintenanceTypeManagementController(IController controller)
      : base(controller)
    {
    }

    #endregion

    #region OnLoaded
    
    protected override void OnLoaded()
    {
      if (DataContext.Data.Count > 0)
      {
        //Focus the first element 
        MaintenanceTypeItemViewModel ivm = GetCreateViewModel<MaintenanceTypeItemViewModel>(DataContext.Data[0], ViewModel);
        ivm.IsFocused = true;
      }

      RegisterContextRegion(RegionManager.Regions[MaintenanceTypeManagement.DetailsRegion]);
      RegisterContextRegion(RegionManager.Regions[MaintenanceTypeManagement.TabRegion]);

      base.OnLoaded();
    }

    #endregion
    
    #region OnContextViewModelChanged
    
    protected override void OnContextViewModelChanged(ISelectableViewModel context)
    {
      if (context.Model is MaintenanceType)
      {
        MaintenanceTypeDetailViewModel vm = GetCreateViewModel<MaintenanceTypeDetailViewModel>(context.Model, ViewModel);
        AddDetailsViewModel(vm);
      }

      base.OnContextViewModelChanged(context);
    }

    #endregion
    
    #region ExecuteOperation
    
    protected override void ExecuteOperation(Operation op, BusinessObject argument)
    {
      switch (op.Identifier)
      {
        case Operations.AddMaintenanceType:
          CreateRootItem();
          break;
      }
      base.ExecuteOperation(op, argument);
    }
    
    #endregion
  }
}

﻿using Afx.Business.Security;
using Afx.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Afx.Prism.RibbonMdi.Activities;
using ScheduleManagement.Business;
using Microsoft.Practices.Prism;

namespace ScheduleManagement.Prism.Activities.MaintenanceTypeManagement
{
  //**************************************************************************************************************
  //**************************************************************************************************************
  //*                                                                                                            *
  //*     This code is auto-generated.  Do not place any cutom code here, it will be lost on re-generation.      *
  //*                                                                                                            *
  //**************************************************************************************************************
  //**************************************************************************************************************
  public partial class MaintenanceTypeManagementViewModel
  {
    public IEnumerable<MaintenanceTypeItemViewModel> Items
    {
      get
      {
        if (Model == null) return null;
        return this.ResolveViewModels<MaintenanceTypeItemViewModel, MaintenanceType>(Model.Data);
      }
    }
  }
}

﻿using Afx.Business;
using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Activities;
using ScheduleManagement.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScheduleManagement.Prism.Activities.MaintenanceTypeManagement
{
  public partial class MaintenanceTypeDetailViewModel : ActivityDetailViewModel<MaintenanceType>
  {
    #region Constructors

    [InjectionConstructor]
    public MaintenanceTypeDetailViewModel(IController controller)
      : base(controller)
    {
    }
    
    #endregion
  }
}

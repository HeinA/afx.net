﻿using Afx.Business.Security;
using Afx.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Activities;
using ScheduleManagement.Business;

namespace ScheduleManagement.Prism.Activities.MaintenanceTypeManagement
{
  public partial class MaintenanceTypeManagementViewModel : MdiContextualActivityViewModel<MaintenanceTypeManagement>
  {
    #region Constructors

    [InjectionConstructor]
    public MaintenanceTypeManagementViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion
  }
}

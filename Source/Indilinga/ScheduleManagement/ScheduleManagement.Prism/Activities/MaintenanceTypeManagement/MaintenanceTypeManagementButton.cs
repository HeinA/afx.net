﻿using Afx.Business.Activities;
using Afx.Business.Data;
using Afx.Prism.RibbonMdi;
using Afx.Prism.RibbonMdi.Ribbon;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScheduleManagement.Prism.Activities.MaintenanceTypeManagement
{
  [Export(typeof(IRibbonItem))]
  public class MaintenanceTypeManagementButton : RibbonButtonViewModel
  {
    public override string TabName
    {
      get { return FreightManagement.Prism.FreightManagementTab.TabName; }
    }

    public override string GroupName
    {
      get { return FreightManagement.Prism.ConfigurationGroup.GroupName; }
    }

    public override string ItemIdentifier
    {
      get { return "MaintenanceTypeManagement"; }
    }

    public override int Index
    {
      get { return 0; }
    }

    #region Activity Activity

    public Activity Activity
    {
      get { return Cache.Instance.GetObject<Activity>(MaintenanceTypeManagement.Key); }
    }

    #endregion

    public bool IsEnabled
    {
      get
      {
        try
        {
          if (Activity == null) return false;
          return Activity.CanView;
        }
        catch
        {
          return false;
        }
      }
    }

    protected override void OnExecute()
    {
      MdiApplicationController.Current.ExecuteActivity(Activity);
    }
  }
}

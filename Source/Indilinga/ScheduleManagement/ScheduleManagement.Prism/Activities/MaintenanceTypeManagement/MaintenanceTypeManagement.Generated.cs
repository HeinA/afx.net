﻿using Afx.Business.Activities;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Business.Service;
using ScheduleManagement.Business;
using ScheduleManagement.Business.Service;
using Afx.Prism.RibbonMdi.Activities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.Composition;
  
namespace ScheduleManagement.Prism.Activities.MaintenanceTypeManagement
{
  //**************************************************************************************************************
  //**************************************************************************************************************
  //*                                                                                                            *
  //*     This code is auto-generated.  Do not place any cutom code here, it will be lost on re-generation.      *
  //*                                                                                                            *
  //**************************************************************************************************************
  //**************************************************************************************************************
  [Export("Context:" + MaintenanceTypeManagement.Key)]
  public partial class MaintenanceTypeManagement
  {
    public const string Key = "{dbc911a9-e65f-4247-b459-9fff46341b6e}";

    public override void LoadData()
    {
      using (var svc = ProxyFactory.GetService<IScheduleManagementService>(SecurityContext.MasterServer.Name))
      {
        Data = svc.LoadMaintenanceTypeCollection();
      }
      base.LoadData();
    }

    public override void SaveData()
    {
      using (var svc = ProxyFactory.GetService<IScheduleManagementService>(SecurityContext.MasterServer.Name))
      {
        Data = svc.SaveMaintenanceTypeCollection(Data);
      }
      base.SaveData();
    }
  }
}

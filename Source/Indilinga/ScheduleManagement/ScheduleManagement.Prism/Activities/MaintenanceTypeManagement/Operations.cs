﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScheduleManagement.Prism.Activities.MaintenanceTypeManagement
{
  //**************************************************************************************************************
  //**************************************************************************************************************
  //*                                                                                                            *
  //*     This code is auto-generated.  Do not place any cutom code here, it will be lost on re-generation.      *
  //*                                                                                                            *
  //**************************************************************************************************************
  //**************************************************************************************************************
  public class Operations : Afx.Prism.RibbonMdi.StandardOperations
  {
      public const string AddMaintenanceType = "{49739022-c183-4890-8594-cc25aa993b29}";
  }
}

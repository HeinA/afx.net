﻿using Afx.Business.Security;
using Afx.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Activities;
using ScheduleManagement.Business;

namespace ScheduleManagement.Prism.Activities.SchedulingAdminLoadReconManagement
{
  public partial class SchedulingAdminLoadReconManagementViewModel : MdiCustomActivityViewModel<SchedulingAdminLoadReconManagement>
  {
    #region Constructors

    [InjectionConstructor]
    public SchedulingAdminLoadReconManagementViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion
  }
}

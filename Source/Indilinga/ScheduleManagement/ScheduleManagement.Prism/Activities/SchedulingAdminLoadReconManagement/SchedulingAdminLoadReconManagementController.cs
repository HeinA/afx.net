﻿using Afx.Business;
using Afx.Business.Activities;
using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Activities;
using ScheduleManagement.Business;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScheduleManagement.Prism.Activities.SchedulingAdminLoadReconManagement
{
  public partial class SchedulingAdminLoadReconManagementController : MdiCustomActivityController<SchedulingAdminLoadReconManagement, SchedulingAdminLoadReconManagementViewModel>
  {
    #region Constructors

    [InjectionConstructor]
    public SchedulingAdminLoadReconManagementController(IController controller)
      : base(controller)
    {
    }

    #endregion

    #region OnLoaded
    
    protected override void OnLoaded()
    {
      ViewModel.IsFocused = true;
      base.OnLoaded();
    }

    #endregion
    
    #region ExecuteOperation
    
    protected override void ExecuteOperation(Operation op, BusinessObject argument)
    {
      switch (op.Identifier)
      {
        case Operations.ExcelExport:
          break;
      }
      base.ExecuteOperation(op, argument);
    }
    
    #endregion
  }
}
﻿using ScheduleManagement.Business;
using ScheduleManagement.Business.Service;
using Afx.Business.Service;
using Afx.Prism.RibbonMdi.Activities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.Composition;
using Afx.Business.Security;

namespace ScheduleManagement.Prism.Activities.SchedulingAdminLoadReconManagement
{
  //**************************************************************************************************************
  //**************************************************************************************************************
  //*                                                                                                            *
  //*     This code is auto-generated.  Do not place any cutom code here, it will be lost on re-generation.      *
  //*                                                                                                            *
  //**************************************************************************************************************
  //**************************************************************************************************************
  [Export("Context:" + global::ScheduleManagement.Prism.Activities.SchedulingAdminLoadReconManagement.SchedulingAdminLoadReconManagement.Key)]
  public partial class SchedulingAdminLoadReconManagement
  {
    public const string Key = "{3e6a957c-e826-4078-aedc-46becf34feeb}";
  }
}

﻿using Afx.Business;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using Microsoft.Practices.Unity;
using ScheduleManagement.Business;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScheduleManagement.Prism.Settings
{
  [Export(typeof(MdiSettingViewModel<ScheduleSetting>))]
  public class ScheduleSettingViewModel : MdiSettingViewModel<ScheduleSetting>
  {
    #region Constructors

    [InjectionConstructor]
    public ScheduleSettingViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion
  }
}

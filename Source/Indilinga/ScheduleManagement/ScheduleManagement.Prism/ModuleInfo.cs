﻿using Afx.Prism;
using Afx.Prism.RibbonMdi;
using ScheduleManagement.Business;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScheduleManagement.Prism
{
  [Export(typeof(IModuleInfo))]
  public class ModuleInfo : IModuleInfo
  {
    public string ModuleNamespace
    {
      get { return Namespace.ScheduleManagement; }
    }

    public string[] ModuleDependencies
    {
      get { return new string[] { Afx.Business.Namespace.Afx, FleetManagement.Business.Namespace.FleetManagement, ContactManagement.Business.Namespace.ContactManagement, AccountManagement.Business.Namespace.AccountManagement, FreightManagement.Business.Namespace.FreightManagement }; }
    }

    public Type ModuleController
    {
      get { return typeof(ScheduleManagementModuleController); }
    }
  }
}
﻿using Afx.Business;
using Afx.Business.Activities;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Documents;
using Afx.Business.Security;
using Afx.Business.Service;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using Afx.Prism.RibbonMdi.Documents;
using Afx.Prism.RibbonMdi.Events;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Unity;
using ScheduleManagement.Business;
using ScheduleManagement.Business.Service;
using ScheduleManagement.Prism.Tools.ScheduledActions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ScheduleManagement.Prism
{
  public class ScheduleManagementModuleController : MdiModuleController
  {
    [InjectionConstructor]
    public ScheduleManagementModuleController(IController controller)
      : base("ScheduleManagementModuleController Module Controller", controller)
    {
    }

    #region ScheduledActionsController ScheduledActionsController

    public const string ScheduledActionsControllerProperty = "ScheduledActionsController";
    ScheduledActionsController mScheduledActionsController;
    public ScheduledActionsController ScheduledActionsController
    {
      get { return mScheduledActionsController; }
      set { mScheduledActionsController = value; }
    }

    #endregion

    protected override void OnRunning()
    {
      ScheduledActionsController = GetCreateChildController<ScheduledActionsController>(ScheduledActionsContext.ControllerKey);
      ScheduledActionsController.Run();

      base.OnRunning();
    }

    //protected override void OnUserAuthenticated(EventArgs obj)
    //{
    //  base.OnUserAuthenticated(obj);

    //  using (var svc = ProxyFactory.GetService<IScheduleManagementService>(SecurityContext.ServerName))
    //  {
    //    BasicCollection<VehicleSchedule> shed = svc.LoadVehicleSchedules(new DateTime(2015, 6, 23), new DateTime(2015, 6, 24));
    //  }
    //}
  }
}

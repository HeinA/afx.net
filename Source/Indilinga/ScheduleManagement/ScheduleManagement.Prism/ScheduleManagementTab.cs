﻿using Afx.Prism.RibbonMdi.Ribbon;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScheduleManagement.Prism
{
  [Export(typeof(IRibbonTab))]
  public class ScheduleManagementTab : RibbonTab
  {
    public const string TabName = "Schedule";

    public override string Name
    {
      get { return TabName; }
    }

    public override int Index
    {
      get { return 1; }
    }
  }
}

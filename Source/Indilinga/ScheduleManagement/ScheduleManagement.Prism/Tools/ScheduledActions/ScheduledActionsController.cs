﻿using Afx.Business.Security;
using Afx.Business.Service;
using Afx.Prism;
using Afx.Prism.Events;
using Afx.Prism.RibbonMdi;
using Microsoft.Practices.Unity;
using ScheduleManagement.Business.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScheduleManagement.Prism.Tools.ScheduledActions
{
  public class ScheduledActionsController : MdiDockableToolController<ScheduledActionsContext, ScheduledActionsViewModel>
  {
    [InjectionConstructor]
    public ScheduledActionsController(IController controller)
      : base(ScheduledActionsContext.ControllerKey, controller)
    {
      DataContext = new ScheduledActionsContext();
      MdiApplicationController.Current.EventAggregator.GetEvent<CacheUpdatedEvent>().Subscribe(OnCacheUpdated);
    }

    private void OnCacheUpdated(EventArgs obj)
    {
      Refresh();
    }

    public void Refresh()
    {
      if (SecurityContext.User == null) return;
      using (var svc = ProxyFactory.GetService<IScheduleManagementService>(SecurityContext.ServerName))
      {
        DataContext.ScheduledActions = svc.LoadScheduledActions();
      }
    }

    protected override void ConfigureContainer()
    {
      MdiApplicationController.Current.Container.RegisterInstance<ScheduledActionsController>(this);
      base.ConfigureContainer();
    }
  }
}

﻿using Afx.Business;
using Afx.Business.Collections;
using ScheduleManagement.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScheduleManagement.Prism.Tools.ScheduledActions
{
  public class ScheduledActionsContext : BusinessObject
  {
    public const string ControllerKey = "ScheduleManagement.Prism.Tools.ScheduledActions.ScheduledActionsContext";

    #region BasicCollection<ScheduledAction> ScheduledActions

    public const string ScheduledActionsProperty = "ScheduledActions";
    BasicCollection<ScheduledAction> mScheduledActions;
    public BasicCollection<ScheduledAction> ScheduledActions
    {
      get { return mScheduledActions; }
      set { SetProperty<BasicCollection<ScheduledAction>>(ref mScheduledActions, value); }
    }

    #endregion
  }
}

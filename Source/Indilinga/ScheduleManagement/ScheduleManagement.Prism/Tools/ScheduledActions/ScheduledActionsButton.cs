﻿using Afx.Prism.RibbonMdi.Ribbon;
using FreightManagement.Prism;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScheduleManagement.Prism.Tools.ScheduledActions
{
  [Export(typeof(IRibbonItem))]
  public class ScheduledActionsButton : RibbonToggleButtonViewModel
  {
    public override string TabName
    {
      get { return ScheduleManagementTab.TabName; }
    }

    public override string GroupName
    {
      get { return FreightSchedulingGroup.GroupName; }
    }

    public override string ItemIdentifier
    {
      get { return "ScheduledActions"; }
    }

    public override int Index
    {
      get { return 2; }
    }

    ScheduledActionsController mScheduledActionsController = null;
    [Dependency]
    public ScheduledActionsController ScheduledActionsController
    {
      get { return mScheduledActionsController; }
      set
      {
        mScheduledActionsController = value;
        mScheduledActionsController.ViewModel.PropertyChanged += ViewModel_PropertyChanged;
      }
    }

    void ViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
    {
      if (e.PropertyName == "IsVisible")
      {
        OnPropertyChanged("IsChecked");
      }
    }

    public override bool IsChecked
    {
      get { return ScheduledActionsController.ViewModel.IsVisible; }
      set { ScheduledActionsController.ViewModel.IsVisible = value; }
    }
  }
}

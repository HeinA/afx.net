﻿using Afx.Prism;
using Afx.Prism.RibbonMdi;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Unity;
using ScheduleManagement.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScheduleManagement.Prism.Tools.ScheduledActions
{
  public class ScheduledActionsViewModel : MdiDockableToolViewModel<ScheduledActionsContext>
  {
    [InjectionConstructor]
    public ScheduledActionsViewModel(IController controller)
      : base(controller)
    {
      Title = "Scheduled Actions";
    }

    [Dependency]
    public ScheduledActionsController ScheduledActionsController { get; set; }

    #region string ContentId

    public override string ContentId
    {
      get { return ScheduledActionsContext.ControllerKey; }
    }

    #endregion

    #region ScheduledAction SelectedAction

    public const string SelectedActionProperty = "SelectedAction";
    ScheduledAction mSelectedAction;
    public ScheduledAction SelectedAction
    {
      get { return mSelectedAction; }
      set { SetProperty<ScheduledAction>(ref mSelectedAction, value); }
    }

    #endregion

    #region DelegateCommand EditDocumentCommand

    DelegateCommand mEditDocumentCommand;
    public DelegateCommand EditDocumentCommand
    {
      get { return mEditDocumentCommand ?? (mEditDocumentCommand = new DelegateCommand(ExecuteEditDocument, CanExecuteEditDocument)); }
    }

    bool CanExecuteEditDocument()
    {
      return true;
    }

    void ExecuteEditDocument()
    {
      if (SelectedAction == null) return;

      MdiApplicationController.Current.EditDocument(SelectedAction.DocumentType, SelectedAction.DocumentIdentifier);
    }

    #endregion

    #region DelegateCommand RefreshCommand

    DelegateCommand mRefreshCommand;
    public DelegateCommand RefreshCommand
    {
      get { return mRefreshCommand ?? (mRefreshCommand = new DelegateCommand(ExecuteRefresh, CanExecuteRefresh)); }
    }

    bool CanExecuteRefresh()
    {
      return true;
    }

    void ExecuteRefresh()
    {
      using (new WaitCursor())
      {
        ScheduledActionsController.Refresh();
      }
    }

    #endregion

  }
}

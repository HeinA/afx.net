﻿using Afx.Business.Documents;
using Afx.Prism;
using Afx.Prism.Documents;
using Afx.Prism.RibbonMdi.Documents;
using FreightManagement.Business;
using ScheduleManagement.Prism.Documents.Tripsheet;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ScheduleManagement.Business;

namespace ScheduleManagement.Prism.Reports.Tripsheet
{
  [Export(ScheduleManagement.Business.Tripsheet.DocumentTypeIdentifier, typeof(IDocumentPrintInfo))]
  class TripsheetInfo : IDocumentPrintInfo
  {
    public int Priority
    {
      get { return 90; }
    }

    public Stream ReportStream(object argument)
    {
      return this.GetType().Assembly.GetManifestResourceStream("ScheduleManagement.Prism.Reports.Tripsheet.Tripsheet.rdlc"); 
    }

    public object GetScope(IController controller)
    {
      return null;
    }

    public void RefreshReportData(LocalReport report, Document document, object scope, object argument)
    {
      ScheduleManagement.Business.Tripsheet ts = (ScheduleManagement.Business.Tripsheet)document;

      #region Tripsheet Details

      ReportDataSource reportDataSource = new ReportDataSource();
      reportDataSource.Name = "Document";
      reportDataSource.Value = new object[] { new DocumentPrintViewModel(document) };
      report.DataSources.Add(reportDataSource);

      reportDataSource = new ReportDataSource();
      reportDataSource.Name = "TripsheetDetails";
      reportDataSource.Value = new object[] { new TripsheetPrintViewModel(ts) };
      report.DataSources.Add(reportDataSource);

      #endregion



      #region Tripsheet Vehicles

      reportDataSource = new ReportDataSource();
      reportDataSource.Name = "TripsheetVehicles";
      Collection<TripsheetVehiclePrintViewModel> routeVehicles = new Collection<TripsheetVehiclePrintViewModel>();
      if (ts.Vehicles != null)
      {
        foreach (var tsrv in ts.Vehicles)
        {
          routeVehicles.Add(new TripsheetVehiclePrintViewModel(tsrv));
        }
      }
      reportDataSource.Value = routeVehicles;
      report.DataSources.Add(reportDataSource);

      #endregion

      #region Tripsheet Manifests

      reportDataSource = new ReportDataSource();
      reportDataSource.Name = "TripsheetManifests";
      Collection<TripsheetManifestPrintViewModel> routeManifests = new Collection<TripsheetManifestPrintViewModel>();
      foreach (var tsrv in ts.Manifests)
      {
        routeManifests.Add(new TripsheetManifestPrintViewModel(tsrv));
      }
      reportDataSource.Value = routeManifests;
      report.DataSources.Add(reportDataSource);

      #endregion

      report.ShowDetailedSubreportMessages = true;
      report.LoadSubreportDefinition("TripsheetSubReport", this.GetType().Assembly.GetManifestResourceStream("ScheduleManagement.Prism.Reports.Tripsheet.TripsheetSubReport.rdlc"));
    }
    
    public short Copies
    {
      get { return 1; }
    }
  }
}

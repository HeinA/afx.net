﻿using FleetManagement.Business;
using ScheduleManagement.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScheduleManagement.Prism.Reports.Tripsheet
{
  public class TripsheetVehiclePrintViewModel
  {
    public TripsheetVehiclePrintViewModel(Vehicle details)
    {
      Details = details;
    }

    #region Vehicle Details

    Vehicle mDetails;
    Vehicle Details
    {
      get { return mDetails; }
      set { mDetails = value; }
    }

    #endregion

    public string RegistrationNumber
    {
      get { return Details.RegistrationNumber; }
    }

    public string VehicleFleetNumber
    {
      get { return Details.FleetNumber; }
    }

    public string VehicleIdentificationNumber
    {
      get { return Details.Vin; }
    }

    public string MakeModel
    {
      get { return Details.MakeModel; }
    }

  }
}

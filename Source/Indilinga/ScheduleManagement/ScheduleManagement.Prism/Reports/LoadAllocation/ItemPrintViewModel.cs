﻿#region Usings

using FreightManagement.Business;

#endregion

namespace ScheduleManagement.Prism.Reports.LoadAllocation
{
  public class ItemPrintViewModel
  {
    public ItemPrintViewModel(WaybillReference item)
    {
      Item = item;
    }

    public string WaybillNumber
    {
      get { return Item.WaybillNumber; }
    }

    #region WaybillCargoItem Item

    WaybillReference mItem;
    WaybillReference Item
    {
      get { return mItem; }
      set { mItem = value; }
    }

    #endregion

  }
}

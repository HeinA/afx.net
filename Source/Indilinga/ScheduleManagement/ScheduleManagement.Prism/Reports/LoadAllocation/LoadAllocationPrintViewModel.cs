﻿#region Usings

#endregion

using Afx.Business;
using ContactManagement.Business;
using FleetManagement.Business;
using FreightManagement.Business;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace ScheduleManagement.Prism.Reports.LoadAllocation
{
  public class LoadAllocationPrintViewModel
  {
    public LoadAllocationPrintViewModel(ScheduleManagement.Business.LoadAllocation details)
    {
      Details = details;
    }

    #region LoadAllocation Details

    ScheduleManagement.Business.LoadAllocation mDetails;
    ScheduleManagement.Business.LoadAllocation Details
    {
      get { return mDetails; }
      set { mDetails = value; }
    }

    #endregion



    public string Client
    {
      get { return (Details.Waybills == null ? string.Empty : string.Join(", ", Details.Waybills.Select(wb => wb.Account.Name))); }
    }

    public string Route
    {
      get { return (Details.Route == null ? null : Details.Route.Name); }
    }



    #region "Vehicle Information (Horse & Trailer(s))"

    public string Vehicles
    {
      get
      {
        if (Details.Vehicles == null) return null;
        return string.Join(", ", Details.Vehicles.Select(w => string.Format("{0} ({1})", w.FleetNumber, w.RegistrationNumber)));
      }
    }

    #region "Horse Information"

    public string HorseFleetNumber
    {
      get
      {
        if (Details.Vehicles == null) return null;
        if (Details.Vehicles.Select(v => v.IsTruck == true).Count() == 0) return null;
        return Details.Vehicles.Where(v => v.IsTruck == true).First<Vehicle>().FleetNumber;
      }
    }

    public string HorseRegistrationNumber
    {
      get
      {
        if (Details.Vehicles == null) return null;
        if (Details.Vehicles.Select(v => v.IsTruck == true).Count() == 0) return null;
        return Details.Vehicles.Where(v => v.IsTruck == true).First<Vehicle>().RegistrationNumber;
      }
    }

    #endregion


    #region "Trailer(s) Information"

    public string TrailerFleetNumber
    {
      get
      {
        if (Details.Vehicles == null) return null;
        if (Details.Vehicles.Select(v => v.IsTrailer == true).Count() == 0) return null;
        return string.Join(", ", Details.Vehicles.Where(v => v.IsTrailer == true).Select(v => v.FleetNumber));
      }
    }

    public string TrailerRegistrationNumber
    {
      get
      {
        if (Details.Vehicles == null) return null;
        if (Details.Vehicles.Select(v => v.IsTrailer == true).Count() == 0) return null;
        return string.Join(", ", Details.Vehicles.Where(v => v.IsTrailer == true).Select(v => v.RegistrationNumber));
      }
    }

    /// <summary>
    /// Trailer 1
    /// <para>
    ///   Returns Registration Number if available otherwise null;
    /// </para>
    /// </summary>
    public string Trailer1RegistrationNumber
    {
      get
      {
        if (Details.Vehicles == null) return null;
        if (Details.Vehicles.Select(v => v.IsTrailer == true).Count() == 0) return null;
        return Details.Vehicles.Where(v => v.IsTrailer == true).First().RegistrationNumber;
      }
    }

    /// <summary>
    /// Trailer 1
    /// <para>
    ///   Returns Fleet Number if available otherwise null;
    /// </para>
    /// </summary>
    public string Trailer1FleetNumber
    {
      get
      {
        if (Details.Vehicles == null) return null;
        if (Details.Vehicles.Select(v => v.IsTrailer == true).Count() == 0) return null;
        return Details.Vehicles.Where(v => v.IsTrailer == true).First().FleetNumber;
      }
    }

    /// <summary>
    /// Trailer 2
    /// <para>
    ///   Returns Registration Number if available otherwise null;
    /// </para>
    /// </summary>
    public string Trailer2RegistrationNumber
    {
      get
      {
        if (Details.Vehicles == null) return null;
        if (Details.Vehicles.Select(v => v.IsTrailer == true).Count() == 0) return null;
        return Details.Vehicles.Where(v => v.IsTrailer == true).Last().RegistrationNumber;
      }
    }

    /// <summary>
    /// Trailer 2
    /// <para>
    ///   Returns Fleet Number if available otherwise null;
    /// </para>
    /// </summary>
    public string Trailer2FleetNumber
    {
      get
      {
        if (Details.Vehicles == null) return null;
        if (Details.Vehicles.Select(v => v.IsTrailer == true).Count() == 0) return null;
        return Details.Vehicles.Where(v => v.IsTrailer == true).Last().FleetNumber;
      }
    }

    #endregion

    #endregion



    #region "Drivers Information"

    #region "Driver"
    public string DriverFullNames
    {
      get { return (Details.Driver == null ? string.Empty : Details.Driver.Fullname); }
    }

    public string DriverIDNumber
    {
      get { return (Details.Driver == null ? string.Empty : Details.Driver.IDNumber); }
    }

    public string DriverPassport
    {
      get { return (Details.Driver == null ? string.Empty : Details.Driver.PassportNumber); }
    }

    public DateTime? DriverPassportDate
    {
      get { return (Details.Driver == null ? null : (DateTime?)Details.Driver.PassportExpiryDate); }
    }

    public string DriverLocalMobilePhone
    {
      get
      {
        return (Details.Driver == null ? null : Details.DriverMobilePhone);
      }
    }

    public string DriverRoamingMobilePhone
    {
      get
      {
        return (Details.Driver == null ? null : Details.DriverForeignMobilePhone);
      }
    }
    #endregion

    #region "Co Driver"
    public string CoDriverFulllNames
    {
      get { return (Details.CoDriver == null ? string.Empty : Details.CoDriver.Fullname); }
    }

    public string CoDriverIDNumber
    {
      get { return (Details.CoDriver == null ? string.Empty : Details.CoDriver.IDNumber); }
    }

    public string CoDriverPassport
    {
      get { return (Details.CoDriver == null ? string.Empty : Details.CoDriver.PassportNumber); }
    }

    public DateTime? CoDriverPassportDate
    {
      get { return (Details.CoDriver == null ? null : (DateTime?)Details.CoDriver.PassportExpiryDate); }
    }
    #endregion

    #region "Backup Driver"
    public string BackupDriverFulllNames
    {
      get { return (Details.BackupDriver == null ? string.Empty : Details.BackupDriver.Fullname); }
    }

    public string BackupDriverIDNumber
    {
      get { return (Details.BackupDriver == null ? string.Empty : Details.BackupDriver.IDNumber); }
    }

    public string BackupDriverPassport
    {
      get { return (Details.BackupDriver == null ? string.Empty : Details.BackupDriver.PassportNumber); }
    }

    public DateTime? BackupDriverPassportDate
    {
      get { return (Details.BackupDriver == null ? null : (DateTime?)Details.BackupDriver.PassportExpiryDate); }
    }
    #endregion

    #endregion



    #region "Loading Information"

    public string LoadingLocation
    {
      get { return (Details.Route == null ? String.Empty : Details.Route.Origin.Name); }
    }

    public DateTime? LoadingDate
    {
      get { return (DateTime?)Details.LoadingDate.Date.AddHours(Details.LoadingSlot.HH).AddMinutes(Details.LoadingSlot.MM); }
    }

    //public DateTime? LoadingSlot
    //{
    //  get { return new DateTime?(new DateTime(Details.LoadingSlot.HH, Details.LoadingSlot.MM, 0)); }
    //}

    #endregion



    #region "Off-Loading Information"

    public string OffLoadingLocation
    {
      get { return (Details.Route == null ? String.Empty : Details.Route.Destination.Name); }
    }

    public DateTime? OffLoadingDate
    {
      get { return (DateTime?)Details.OffloadingDate.Date.AddHours(Details.OffloadingSlot.HH).AddMinutes(Details.OffloadingSlot.MM); }
    }

    //public DateTime? OffLoadingSlot
    //{
    //  get { return new DateTime?(new DateTime(Details.OffloadingSlot.HH, Details.OffloadingSlot.MM, 0)); }
    //}

    #endregion


    #region "Waybill Information"

    public IEnumerable<WaybillReference> Waybills
    {
      get { return Details.Waybills; }
    }

    public decimal Rate
    {
      get 
      {
        return Details.Waybills.Sum(wb => wb.Charge);
      }
    }

    public string InvoiceTo
    {
      get { return (Details.Waybills == null ? string.Empty : string.Join(", ", Details.Waybills.Select(wb => wb.InvoiceTo))); }
    }

    #endregion
  }
}

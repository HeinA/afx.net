﻿#region Usings

using Afx.Business.Documents;
using Afx.Prism;
using Afx.Prism.Documents;
using Microsoft.Reporting.WinForms;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.IO;

#endregion

namespace ScheduleManagement.Prism.Reports.LoadAllocation
{
  [Export(ScheduleManagement.Business.LoadAllocation.DocumentTypeIdentifier, typeof(IDocumentPrintInfo))]
  public class LoadAllocationInfo : IDocumentPrintInfo
  {
    public int Priority
    {
      get { return 90; }
    }

    public Stream ReportStream(object argument)
    {
      return this.GetType().Assembly.GetManifestResourceStream("ScheduleManagement.Prism.Reports.LoadAllocation.LoadAllocation.rdlc"); 
    }

    public object GetScope(IController controller)
    {
      return null;
    }

    public void RefreshReportData(LocalReport report, Document document, object scope, object argument)
    {
      ScheduleManagement.Business.LoadAllocation la = (ScheduleManagement.Business.LoadAllocation)document;

      #region "Load Allocation Details."

      //*********************************************************************************************************
      //*** Document Details
      //*********************************************************************************************************
      ReportDataSource reportDataSource = new ReportDataSource();
      reportDataSource.Name = "Document";
      reportDataSource.Value = new object[] { new DocumentPrintViewModel(document) };
      report.DataSources.Add(reportDataSource);

      //*********************************************************************************************************
      //*** Load Allocation Details
      //*********************************************************************************************************
      reportDataSource = new ReportDataSource();
      reportDataSource.Name = "LoadAllocationDetails";
      reportDataSource.Value = new object[] { new LoadAllocationPrintViewModel(la) };
      report.DataSources.Add(reportDataSource);

      #endregion

      #region "Waybill Items"

      reportDataSource = new ReportDataSource();
      reportDataSource.Name = "LoadAllocationWaybillItems";
      Collection<ItemPrintViewModel> items = new Collection<ItemPrintViewModel>();

      foreach (var lawb in la.Waybills)
      {
        items.Add(new ItemPrintViewModel(lawb));
      }

      reportDataSource.Value = items;
      report.DataSources.Add(reportDataSource);

      #endregion
    }

    public short Copies
    {
      get { return 1; }
    }
  }
}

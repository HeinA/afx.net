﻿using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScanStatistics.GetParametersDialog
{
  public class GetParametersDialogViewModel : MdiReportParameterViewModel<GetParametersDialogContext>
  {
    [InjectionConstructor]
    public GetParametersDialogViewModel(IController controller)
      : base(controller)
    {
    }

    public IEnumerable<UserReference> Users
    {
      get { return Cache.Instance.GetObjects<UserReference>(true, u => u.FullName, true); }
    }
  }
}

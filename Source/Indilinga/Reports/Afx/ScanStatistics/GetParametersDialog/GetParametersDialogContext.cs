﻿using Afx.Business;
using Afx.Business.Security;
using Afx.Prism.RibbonMdi;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScanStatistics.GetParametersDialog
{
  public class GetParametersDialogContext : MdiReportParameterContext
  {
    #region DateTime? StartDate

    public const string StartDateProperty = "StartDate";
    DateTime? mStartDate;
    public DateTime? StartDate
    {
      get { return mStartDate; }
      set { SetProperty<DateTime?>(ref mStartDate, value); }
    }

    #endregion

    #region DateTime? EndDate

    public const string EndDateProperty = "EndDate";
    DateTime? mEndDate;
    public DateTime? EndDate
    {
      get { return mEndDate; }
      set { SetProperty<DateTime?>(ref mEndDate, value); }
    }

    #endregion

    #region UserReference User

    public const string UserProperty = "User";
    UserReference mUser = new UserReference(true);
    public UserReference User
    {
      get { return mUser; }
      set { SetProperty<UserReference>(ref mUser, value); }
    }

    #endregion
  }
}
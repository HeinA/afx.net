﻿using Afx.Prism;
using Afx.Prism.RibbonMdi;
using CrystalDecisions.CrystalReports.Engine;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace ScanStatistics.GetParametersDialog
{
  public class GetParametersDialogController : MdiReportParameterController<GetParametersDialogContext, GetParametersDialogViewModel>
  {
    public const string GetParametersDialogControllerKey = "ScanStatistics.GetParametersDialog.GetParametersDialogController";

    public GetParametersDialogController(IController controller)
      : base(GetParametersDialogControllerKey, controller)
    {
    }

    protected override void ApplyChanges()
    {
      ReportDocument.SetParameterValue("@StartDate", DataContext.StartDate);
      ReportDocument.SetParameterValue("@EndDate", DataContext.EndDate);
      ReportDocument.SetParameterValue("@User", DataContext.User.FullName);
    }
  }
}

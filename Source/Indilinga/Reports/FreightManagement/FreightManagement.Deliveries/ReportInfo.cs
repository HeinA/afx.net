﻿using Afx.Business.Security;
using Afx.Prism.RibbonMdi;
using CrystalDecisions.CrystalReports.Engine;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Deliveries
{
  [Export(typeof(ICrystalReportInfo))]
  [Authorization(Afx.Business.Security.Roles.Administrator)]
  [Authorization(FreightManagement.Business.Roles.Accounts)]
  [Authorization(FreightManagement.Business.Roles.AccountsSupervisor)]
  [Authorization(FreightManagement.Business.Roles.Dispatch)]
  [Authorization(FreightManagement.Business.Roles.DispatchSupervisor)]
  [Authorization(FreightManagement.Business.Roles.Operations)]
  [Authorization(FreightManagement.Business.Roles.OperationsSupervisor)]
  public class ReportInfo : CrystalReportInfo<GetParametersDialog.GetParametersDialogController>
  {
    public override string GroupName { get { return "Operations"; } }
    public override string ReportName { get { return "Daily Deliveries"; } }


    public override byte[] GetReport()
    {
      return ResourceHelper.GetResource(System.Reflection.Assembly.GetExecutingAssembly(), "FreightManagement.Deliveries.FreightManagement.Deliveries.rpt");
    }
  }
}

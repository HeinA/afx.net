﻿using Afx.Business.Security;
using Afx.Prism.RibbonMdi;
using CrystalDecisions.CrystalReports.Engine;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.OutstandingDeliveries
{
  [Export(typeof(ICrystalReportInfo))]
  [Authorization(Afx.Business.Security.Roles.Administrator)]
  public class ReportInfo : CrystalReportInfo<GetParametersDialog.GetParametersDialogController>
  {
    public override string GroupName { get { return "Operations"; } }
    public override string ReportName { get { return "Outstanding Deliveries"; } }


    public override byte[] GetReport()
    {
      return ResourceHelper.GetResource(System.Reflection.Assembly.GetExecutingAssembly(), "FreightManagement.OutstandingDeliveries.FreightManagement.OutstandingDeliveries.rpt");
    }
  }
}

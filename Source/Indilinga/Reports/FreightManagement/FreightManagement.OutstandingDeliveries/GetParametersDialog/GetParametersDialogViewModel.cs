﻿using Afx.Business;
using Afx.Business.Data;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using ContactManagement.Business;
using FleetManagement.Business;
using FreightManagement.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.OutstandingDeliveries.GetParametersDialog
{
  public class GetParametersDialogViewModel : MdiReportParameterViewModel<GetParametersDialogContext>
  {
    [InjectionConstructor]
    public GetParametersDialogViewModel(IController controller)
      : base(controller)
    {
    }

    public IEnumerable<Route> Routes
    {
      get { return Cache.Instance.GetObjects<Route>(true, r => r.Name, true); }
    }

    public IEnumerable<Contractor> Couriers
    {
      get { return Cache.Instance.GetObjects<Contractor>(true, r => r.CompanyName, true); }
    }

    protected override void OnModelPropertyChanged(string propertyName)
    {
      switch (propertyName)
      {
        case GetParametersDialogContext.CourierProperty:
          Model.Driver = new SimpleContact(true);
          OnPropertyChanged("Drivers");
          break;
      }

      base.OnModelPropertyChanged(propertyName);
    }

    public IEnumerable<Contact> Drivers
    {
      get
      {
        IEnumerable<Contact> drivers = null;
        if (BusinessObject.IsNull(Model.Courier)) drivers = Cache.Instance.GetObjects<Employee>(true, e => e.ContactType.IsFlagged(ContactTypeFlags.Driver), e => e.Fullname, true);
        else drivers = new Contact[] { new SimpleContact(true) }.Union(Model.Courier.Contacts.Where(e => e.ContactType.IsFlagged(ContactTypeFlags.Driver)).OrderBy(e => e.Fullname));
        return drivers;
      }
    }
  }
}

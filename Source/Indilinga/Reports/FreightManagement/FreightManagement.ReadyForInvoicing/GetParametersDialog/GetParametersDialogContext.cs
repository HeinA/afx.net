﻿using AccountManagement.Business;
using Afx.Business;
using Afx.Prism.RibbonMdi;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.ReadyForInvoicing.GetParametersDialog
{
  public class GetParametersDialogContext : MdiReportParameterContext
  {
    #region AccountReference Account

    public const string AccountProperty = "Account";
    AccountReference mAccount = new AccountReference(true);
    public AccountReference Account
    {
      get { return mAccount; }
      set { SetProperty<AccountReference>(ref mAccount, value); }
    }

    #endregion
  }
}
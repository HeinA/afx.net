﻿using CrystalDecisions.CrystalReports.Engine;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.ReadyForInvoicing.GetParametersDialog
{
  [Export(typeof(Afx.Prism.ResourceInfo))]
  public class ResourceInfo : Afx.Prism.ResourceInfo
  {
    public override Uri GetUri()
    {
      return new Uri("pack://application:,,,/FreightManagement.ReadyForInvoicing;component/GetParametersDialog/Resources.xaml", UriKind.Absolute);
    }
  }
}
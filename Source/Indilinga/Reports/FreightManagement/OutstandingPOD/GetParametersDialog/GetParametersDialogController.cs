﻿using Afx.Prism;
using Afx.Prism.RibbonMdi;
using CrystalDecisions.CrystalReports.Engine;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace OutstandingPOD.GetParametersDialog
{
  public class GetParametersDialogController : MdiReportParameterController<GetParametersDialogContext, GetParametersDialogViewModel>
  {
    public const string GetParametersDialogControllerKey = "OutstandingPOD.GetParametersDialog.GetParametersDialogController";

    public GetParametersDialogController(IController controller)
      : base(GetParametersDialogControllerKey, controller)
    {
    }

    protected override void ApplyChanges()
    {
      ReportDocument.SetParameterValue("@StartDate", DataContext.StartDate);
      ReportDocument.SetParameterValue("@EndDate", DataContext.EndDate);
      ReportDocument.SetParameterValue("@Account", DataContext.Account.Name);
      ReportDocument.SetParameterValue("@Consignee", DataContext.Consignee);
      ReportDocument.SetParameterValue("@Manifest", DataContext.Manifest);
      ReportDocument.SetParameterValue("@Route", DataContext.Route.Name);
      ReportDocument.SetParameterValue("@Courier", DataContext.Courier.CompanyName);
      ReportDocument.SetParameterValue("@Driver", DataContext.Driver.Fullname);
    }
  }
}

﻿using AccountManagement.Business;
using Afx.Business;
using Afx.Prism.RibbonMdi;
using ContactManagement.Business;
using FreightManagement.Business;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OutstandingPOD.GetParametersDialog
{
  public class GetParametersDialogContext : MdiReportParameterContext
  {
    #region DateTime? StartDate

    public const string StartDateProperty = "StartDate";
    DateTime? mStartDate;
    public DateTime? StartDate
    {
      get { return mStartDate; }
      set { SetProperty<DateTime?>(ref mStartDate, value); }
    }

    #endregion

    #region DateTime? EndDate

    public const string EndDateProperty = "EndDate";
    DateTime? mEndDate;
    public DateTime? EndDate
    {
      get { return mEndDate; }
      set { SetProperty<DateTime?>(ref mEndDate, value); }
    }

    #endregion

    #region AccountReference Account

    public const string AccountProperty = "Account";
    AccountReference mAccount = new AccountReference(true);
    public AccountReference Account
    {
      get { return mAccount; }
      set { SetProperty<AccountReference>(ref mAccount, value); }
    }

    #endregion

    #region string Consignee

    public const string ConsigneeProperty = "Consignee";
    string mConsignee;
    public string Consignee
    {
      get { return mConsignee; }
      set { SetProperty<string>(ref mConsignee, value); }
    }

    #endregion

    #region string Manifest

    public const string ManifestProperty = "Manifest";
    string mManifest;
    public string Manifest
    {
      get { return mManifest; }
      set { SetProperty<string>(ref mManifest, value); }
    }

    #endregion

    #region Route Route

    public const string RouteProperty = "Route";
    Route mRoute = new Route(true);
    public Route Route
    {
      get { return mRoute; }
      set { SetProperty<Route>(ref mRoute, value); }
    }

    #endregion

    #region Contractor Courier

    public const string CourierProperty = "Courier";
    Contractor mCourier = new Contractor(true);
    public Contractor Courier
    {
      get { return mCourier; }
      set { SetProperty<Contractor>(ref mCourier, value); }
    }

    #endregion

    #region Contact Driver

    public const string DriverProperty = "Driver";
    Contact mDriver = new SimpleContact(true);
    public Contact Driver
    {
      get { return mDriver; }
      set { SetProperty<Contact>(ref mDriver, value); }
    }

    #endregion
  }
}
﻿using Afx.Business.Data;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using FreightManagement.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlaggedWaybills.GetParametersDialog
{
  public class GetParametersDialogViewModel : MdiReportParameterViewModel<GetParametersDialogContext>
  {
    [InjectionConstructor]
    public GetParametersDialogViewModel(IController controller)
      : base(controller)
    {
    }

    public IEnumerable<ItemViewModel> Flags
    {
      get { return ResolveViewModels<ItemViewModel, WaybillFlag>(Cache.Instance.GetObjects<WaybillFlag>((r) => r.Text, true)); }
    }
  }
}

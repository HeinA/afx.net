﻿using Afx.Prism;
using FreightManagement.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlaggedWaybills.GetParametersDialog
{
  public class ItemViewModel : SelectableViewModel<WaybillFlag>
  {
    [InjectionConstructor]
    public ItemViewModel(IController controller)
      : base(controller)
    {
    }

    [Dependency]
    public GetParametersDialogController GetParametersDialogController { get; set; }

    public bool IsChecked
    {
      get { return GetParametersDialogController.Flags.Contains(Model.Text); }
      set
      {
        if (value)
        {
          if (!GetParametersDialogController.Flags.Contains(Model.Text))
          {
            GetParametersDialogController.Flags.Add(Model.Text);
            OnPropertyChanged("IsChecked");
          }
        }
        else
        {
          if (GetParametersDialogController.Flags.Contains(Model.Text))
          {
            GetParametersDialogController.Flags.Remove(Model.Text);
            OnPropertyChanged("IsChecked");
          }
        }
      }
    }
  }
}

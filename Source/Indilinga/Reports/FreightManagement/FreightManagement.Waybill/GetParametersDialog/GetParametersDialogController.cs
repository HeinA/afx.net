﻿using Afx.Prism;
using Afx.Prism.RibbonMdi;
using CrystalDecisions.CrystalReports.Engine;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace FreightManagement.Waybill.GetParametersDialog
{
  public class GetParametersDialogController : MdiReportParameterController<GetParametersDialogContext, GetParametersDialogViewModel>
  {
    public const string GetParametersDialogControllerKey = "FreightManagement.Waybill.GetParametersDialog.GetParametersDialogController";

    public GetParametersDialogController(IController controller)
      : base(GetParametersDialogControllerKey, controller)
    {
    }

    protected override void ApplyChanges()
    {
      ReportDocument.SetParameterValue("@StartDate", DataContext.StartDate);
      ReportDocument.SetParameterValue("@EndDate", DataContext.EndDate);
      ReportDocument.SetParameterValue("@Account", DataContext.Account.Name);
      ReportDocument.SetParameterValue("@Consignee", DataContext.Consignee);
      ReportDocument.SetParameterValue("@State", DataContext.State.Name);
      ReportDocument.SetParameterValue("@Age", DataContext.Age);
    }
  }
}

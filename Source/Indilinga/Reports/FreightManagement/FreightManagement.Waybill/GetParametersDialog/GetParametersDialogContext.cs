﻿using AccountManagement.Business;
using Afx.Business;
using Afx.Business.Documents;
using Afx.Prism.RibbonMdi;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Waybill.GetParametersDialog
{
  public class GetParametersDialogContext : MdiReportParameterContext
  {
    #region DateTime? StartDate

    public const string StartDateProperty = "StartDate";
    DateTime? mStartDate;
    public DateTime? StartDate
    {
      get { return mStartDate; }
      set { SetProperty<DateTime?>(ref mStartDate, value); }
    }

    #endregion

    #region DateTime? EndDate

    public const string EndDateProperty = "EndDate";
    DateTime? mEndDate;
    public DateTime? EndDate
    {
      get { return mEndDate; }
      set { SetProperty<DateTime?>(ref mEndDate, value); }
    }

    #endregion

    #region AccountReference Account

    public const string AccountProperty = "Account";
    AccountReference mAccount = new AccountReference(true);
    public AccountReference Account
    {
      get { return mAccount; }
      set { SetProperty<AccountReference>(ref mAccount, value); }
    }

    #endregion

    #region string Consignee

    public const string ConsigneeProperty = "Consignee";
    string mConsignee;
    public string Consignee
    {
      get { return mConsignee; }
      set { SetProperty<string>(ref mConsignee, value); }
    }

    #endregion

    #region DocumentTypeState State

    public const string StateProperty = "State";
    DocumentTypeState mState = new DocumentTypeState(true);
    public DocumentTypeState State
    {
      get { return mState; }
      set { SetProperty<DocumentTypeState>(ref mState, value); }
    }

    #endregion

    #region int? Age

    public const string AgeProperty = "Age";
    int? mAge;
    public int? Age
    {
      get { return mAge; }
      set { SetProperty<int?>(ref mAge, value); }
    }

    #endregion
  }
}
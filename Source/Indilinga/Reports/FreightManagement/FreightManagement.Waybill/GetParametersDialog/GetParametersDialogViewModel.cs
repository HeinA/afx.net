﻿using Afx.Business.Data;
using Afx.Business.Documents;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreightManagement.Waybill.GetParametersDialog
{
  public class GetParametersDialogViewModel : MdiReportParameterViewModel<GetParametersDialogContext>
  {
    [InjectionConstructor]
    public GetParametersDialogViewModel(IController controller)
      : base(controller)
    {
    }

    public IEnumerable<DocumentTypeState> States
    {
      get { return Cache.Instance.GetObjects<DocumentTypeState>(true, dts=>dts.Owner.Identifier == Business.Waybill.DocumentTypeIdentifier, r => r.Name, true); }
    }
  }
}

﻿using Afx.Prism;
using Afx.Prism.RibbonMdi;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OutstandingPODManagement.GetParametersDialog
{
  public class GetParametersDialogViewModel : MdiReportParameterViewModel<GetParametersDialogContext>
  {
    [InjectionConstructor]
    public GetParametersDialogViewModel(IController controller)
      : base(controller)
    {
    }
  }
}

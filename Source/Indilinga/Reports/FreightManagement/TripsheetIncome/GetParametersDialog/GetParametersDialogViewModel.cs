﻿using Afx.Business;
using Afx.Business.Data;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using ContactManagement.Business;
using FleetManagement.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TripsheetIncome.GetParametersDialog
{
  public class GetParametersDialogViewModel : MdiReportParameterViewModel<GetParametersDialogContext>
  {
    [InjectionConstructor]
    public GetParametersDialogViewModel(IController controller)
      : base(controller)
    {
    }

    public IEnumerable<Contact> Drivers
    {
      get
      {
        return Cache.Instance.GetObjects<Employee>(true, e => e.ContactType.IsFlagged(ContactTypeFlags.Driver), e => e.Fullname, true);
        //IEnumerable<Contact> drivers = null;
        //if (BusinessObject.IsNull(Model.Courier)) drivers = Cache.Instance.GetObjects<Employee>(true, e => e.ContactType.IsFlagged(ContactTypeFlags.Driver), e => e.Fullname, true);
        //else drivers = new Contact[] { new SimpleContact(true) }.Union(Model.Courier.Contacts.Where(e => e.ContactType.IsFlagged(ContactTypeFlags.Driver)).OrderBy(e => e.Fullname));
        //return drivers;
      }
    }
  }
}

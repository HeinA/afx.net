﻿using Afx.Business;
using Afx.Prism.RibbonMdi;
using ContactManagement.Business;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TripsheetIncome.GetParametersDialog
{
  public class GetParametersDialogContext : MdiReportParameterContext
  {
    #region DateTime? StartDate

    public const string StartDateProperty = "StartDate";
    DateTime? mStartDate;
    public DateTime? StartDate
    {
      get { return mStartDate; }
      set { SetProperty<DateTime?>(ref mStartDate, value); }
    }

    #endregion

    #region DateTime? EndDate

    public const string EndDateProperty = "EndDate";
    DateTime? mEndDate;
    public DateTime? EndDate
    {
      get { return mEndDate; }
      set { SetProperty<DateTime?>(ref mEndDate, value); }
    }

    #endregion

    #region Contact Driver

    public const string DriverProperty = "Driver";
    Contact mDriver = new SimpleContact(true);
    public Contact Driver
    {
      get { return mDriver; }
      set { SetProperty<Contact>(ref mDriver, value); }
    }

    #endregion
  }
}
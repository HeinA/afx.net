﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Mvc
{
	public class DataHelper
	{
		public static DataSet ExecuteDataSet(IDbCommand cmd)
		{
			try
			{
				System.Data.DataSet ds = new System.Data.DataSet();
				ds.EnforceConstraints = false;
				ds.Locale = CultureInfo.InvariantCulture;
				using (IDataReader r = cmd.ExecuteReader())
				{
					ds.Load(r, LoadOption.OverwriteChanges, string.Empty);
					r.Close();
				}
				return ds;
			}
			catch
			{
				throw;
			}
		}
	}
}

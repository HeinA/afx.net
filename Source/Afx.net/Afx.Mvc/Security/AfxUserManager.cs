﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Mvc.Security
{
	public class AfxUserManager : UserManager<AfxUser>
	{
		public AfxUserManager(IUserStore<AfxUser> store)
						: base(store)
		{
		}


		public static AfxUserManager Create(IdentityFactoryOptions<AfxUserManager> options, IOwinContext context)
		{
			var manager = new AfxUserManager(new AfxUserStore("Local"));
			// Configure validation logic for usernames
			manager.UserValidator = new UserValidator<AfxUser>(manager)
			{
				AllowOnlyAlphanumericUserNames = false,
				RequireUniqueEmail = true
			};

			// Configure validation logic for passwords
			manager.PasswordValidator = new PasswordValidator
			{
				RequiredLength = 6,
				RequireNonLetterOrDigit = true,
				RequireDigit = true,
				RequireLowercase = true,
				RequireUppercase = true,
			};

			// Configure user lockout defaults
			manager.UserLockoutEnabledByDefault = true;
			manager.DefaultAccountLockoutTimeSpan = TimeSpan.FromMinutes(5);
			manager.MaxFailedAccessAttemptsBeforeLockout = 5;

			//// Register two factor authentication providers. This application uses Phone and Emails as a step of receiving a code for verifying the user
			//// You can write your own provider and plug it in here.
			//manager.RegisterTwoFactorProvider("Phone Code", new PhoneNumberTokenProvider<AfxUser>
			//{
			//	MessageFormat = "Your security code is {0}"
			//});
			//manager.RegisterTwoFactorProvider("Email Code", new EmailTokenProvider<AfxUser>
			//{
			//	Subject = "Security Code",
			//	BodyFormat = "Your security code is {0}"
			//});
			//manager.EmailService = new EmailService();
			//manager.SmsService = new SmsService();

			//var dataProtectionProvider = options.DataProtectionProvider;
			//if (dataProtectionProvider != null)
			//{
			//	manager.UserTokenProvider = new DataProtectorTokenProvider<AfxUser>(dataProtectionProvider.Create("ASP.NET Identity"));
			//}
			return manager;
		}

		//public override bool SupportsUserTwoFactor => false;
		public override Task<bool> GetTwoFactorEnabledAsync(string userId)
		{
			return Task.FromResult<bool>(false);
		}

		protected override Task<bool> VerifyPasswordAsync(IUserPasswordStore<AfxUser, string> store, AfxUser user, string password)
		{
			return base.VerifyPasswordAsync(store, user, password);
		}
	}
}

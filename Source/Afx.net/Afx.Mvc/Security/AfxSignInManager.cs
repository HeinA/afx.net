﻿using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Mvc.Security
{
	public class AfxSignInManager : SignInManager<AfxUser, string>
	{
		public AfxSignInManager(AfxUserManager userManager, IAuthenticationManager authenticationManager)
						: base(userManager, authenticationManager)
		{
		}

		public override Task<ClaimsIdentity> CreateUserIdentityAsync(AfxUser user)
		{
			return user.GenerateUserIdentityAsync((AfxUserManager)UserManager);
		}

		public static AfxSignInManager Create(IdentityFactoryOptions<AfxSignInManager> options, IOwinContext context)
		{
			return new AfxSignInManager(context.GetUserManager<AfxUserManager>(), context.Authentication);
		}
	}
}

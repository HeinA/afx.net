﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Mvc.Security
{
	public class AfxUser : IUser
	{
		public string Id { get; set; }

		public string UserName { get; set; }
		public string PasswordHash { get; set; }

		public string[] Roles { get; internal set; }

		public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<AfxUser> manager)
		{
			var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
			return userIdentity;
		}
	}
}

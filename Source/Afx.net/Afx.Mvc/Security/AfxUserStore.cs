﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Mvc.Security
{
	public class AfxUserStore : IUserStore<AfxUser>, IUserPasswordStore<AfxUser>, IUserLockoutStore<AfxUser, string>, IUserRoleStore<AfxUser>
	{
		string ConnectionName { get; set; }

		public AfxUserStore(string connectionName)
		{
			ConnectionName = connectionName;
		}

		#region IUserStore

		public Task<AfxUser> FindByIdAsync(string userId)
		{
			if (string.IsNullOrEmpty(userId)) return Task.FromResult<AfxUser>(null);

			DataSet ds = null;
			using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings[ConnectionName].ConnectionString))
			{
				con.Open();

				using (var cmd = new SqlCommand("select * from FreightManagement.[ExternalUser] where EMail=@gi", con))
				{
					cmd.Parameters.AddWithValue("@gi", userId);
					ds = DataHelper.ExecuteDataSet(cmd);
				}
			}
			if (ds.Tables[0].Rows.Count == 0) return Task.FromResult<AfxUser>(null);

			AfxUser u = new AfxUser { Id = (string)ds.Tables[0].Rows[0]["EMail"], UserName = (string)ds.Tables[0].Rows[0]["Username"], PasswordHash = (string)ds.Tables[0].Rows[0]["PasswordHash"] };

			return Task.FromResult<AfxUser>(u);
		}

		public Task<AfxUser> FindByNameAsync(string userName)
		{
			if (string.IsNullOrEmpty(userName)) return Task.FromResult<AfxUser>(null);

			DataSet ds = null;
			using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings[ConnectionName].ConnectionString))
			{
				con.Open();

        using (var cmd = new SqlCommand("select * from FreightManagement.[ExternalUser] where EMail=@un", con))
				{
					cmd.Parameters.AddWithValue("@un", userName);
					ds = DataHelper.ExecuteDataSet(cmd);
				}
			}
			if (ds.Tables[0].Rows.Count == 0) return Task.FromResult<AfxUser>(null);

      AfxUser u = new AfxUser { Id = (string)ds.Tables[0].Rows[0]["EMail"], UserName = (string)ds.Tables[0].Rows[0]["Username"], PasswordHash = (string)ds.Tables[0].Rows[0]["PasswordHash"] };

			return Task.FromResult<AfxUser>(u);
		}

		public Task CreateAsync(AfxUser user)
		{
			throw new NotImplementedException();
		}

		public Task DeleteAsync(AfxUser user)
		{
			throw new NotImplementedException();
		}

		public Task UpdateAsync(AfxUser user)
		{
			throw new NotImplementedException();
		}

		#endregion

		#region IUserPasswordStore

		public Task<string> GetPasswordHashAsync(AfxUser user)
		{
			return Task.FromResult<string>(user.PasswordHash);
		}

		public Task<bool> HasPasswordAsync(AfxUser user)
		{
			throw new NotImplementedException();
		}

		public Task SetPasswordHashAsync(AfxUser user, string passwordHash)
		{
			throw new NotImplementedException();
		}

		#endregion

		#region IUserLockoutStore

		public Task<int> GetAccessFailedCountAsync(AfxUser user)
		{
			return Task.FromResult<int>(0);
		}

		public Task<bool> GetLockoutEnabledAsync(AfxUser user)
		{
			return Task.FromResult<bool>(false);
		}

		public Task<DateTimeOffset> GetLockoutEndDateAsync(AfxUser user)
		{
			throw new NotImplementedException();
		}

		public Task<int> IncrementAccessFailedCountAsync(AfxUser user)
		{
			throw new NotImplementedException();
		}

		public Task ResetAccessFailedCountAsync(AfxUser user)
		{
			throw new NotImplementedException();
		}

		public Task SetLockoutEnabledAsync(AfxUser user, bool enabled)
		{
			throw new NotImplementedException();
		}

		public Task SetLockoutEndDateAsync(AfxUser user, DateTimeOffset lockoutEnd)
		{
			throw new NotImplementedException();
		}

		#endregion

		#region IUserRoleStore

		public Task AddToRoleAsync(AfxUser user, string roleName)
		{
			throw new NotImplementedException();
		}

		public Task RemoveFromRoleAsync(AfxUser user, string roleName)
		{
			throw new NotImplementedException();
		}

		public Task<IList<string>> GetRolesAsync(AfxUser user)
		{
			List<string> roles = new List<string>();
      
      DataSet ds = null;
      using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings[ConnectionName].ConnectionString))
      {
        con.Open();

        using (var cmd = new SqlCommand("select ER.Text from FreightManagement.[ExternalUser] EU inner join FreightManagement.ExternalUserRole EUR on EU.Id=EUR.ExternalUser inner join FreightManagement.ExternalRole ER on EUR.ExternalRole=ER.Id where EMail=@id", con))
        {
          cmd.Parameters.AddWithValue("@id", user.Id);
          ds = DataHelper.ExecuteDataSet(cmd);
        }
      }
      
      foreach (DataRow dr in ds.Tables[0].Rows)
      {
        roles.Add((string)dr["Text"]);
      }
      
			user.Roles = roles.ToArray();
			return Task.FromResult<IList<string>>(roles);
		}

		public Task<bool> IsInRoleAsync(AfxUser user, string roleName)
		{
			if (user.Roles.Contains(roleName)) return Task.FromResult<bool>(true);
			return Task.FromResult<bool>(false);
		}

		#endregion

		public void Dispose()
		{
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Mvc.jqGrid
{
  public class Filter
  {
    public string groupOp { get; set; }
    public Rule[] rules { get; set; }
  }
}

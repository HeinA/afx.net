﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Mvc.jqGrid
{
  public class JqGridResult
  {
    public JqGridResult(int pageCount, int pageNumber, int totalRecord, DataTable dt)
    {
      List<string[]> list = new List<string[]>();
      foreach (DataRow row in dt.Rows)
      {
        string[] rowData = row.ItemArray.Select(field => (field is DateTime ? ((DateTime)field).ToString("yyyy/MM/dd HH:mm:ss") : field.ToString()).ToString().Replace("\"", "\"\"")).ToArray();
        list.Add(rowData);
      }

      this.total = pageCount;
      this.records = totalRecord;
      rows = (
        from emp in list
        select new
        {
          id = int.Parse(emp[0]),
          cell = emp
        }).ToArray();
      this.page = pageNumber;
    }

    public int total { get; set; }
    public int page { get; set; }
    public int records { get; set; }
    public object[] rows { get; set; }
  }
}

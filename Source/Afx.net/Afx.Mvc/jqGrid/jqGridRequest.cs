﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Mvc.jqGrid
{
  public class jqGridRequest
  {
    public string sidx { get; set; }
    public string sord { get; set; }
    public int page { get; set; }
    public int rows { get; set; }
    public bool _search { get; set; }
    public string filters { get; set; }

    public Filter GetFilters()
    {
      return JsonConvert.DeserializeObject<Filter>(filters);
    }
  }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Mvc.jqGrid
{
  public static class GridHelper
  {
    static string ParseFilters(Filter filters, SqlCommand cmd)
    {
      List<string> filterList = new List<string>();
      int opCount = 1;
      foreach (var r in filters.rules)
      {
        switch (r.op.ToUpperInvariant())
        {
          case "CN":
            filterList.Add(string.Format("{0} LIKE @p_Op{1}", r.field, opCount));
            cmd.Parameters.AddWithValue(string.Format("@p_Op{0}", opCount++), string.Format("%{0}%", r.data));
            break;

          case "EQ":
            filterList.Add(string.Format("{0} = @p_Op{1}", r.field, opCount));
            cmd.Parameters.AddWithValue(string.Format("@p_Op{0}", opCount++), r.data);
            break;
        }
      }

      if (filterList.Count == 0) return null;
      return string.Format(" WHERE {0}", string.Join(string.Format(" {0} ", filters.groupOp), filterList));
    }

    public static JqGridResult GetGridPage(string query, jqGridRequest request, SqlConnection con, params object[] args)
    {
      try
      {
        int paramCount = 1;
        DataSet ds = null;
        int records = 0;
        using (var cmd = new SqlCommand(string.Empty, con))
        {
          using (var sw = new StringWriter())
          {
            sw.WriteLine(string.Format("SELECT COUNT(1) FROM ({0}) AS RESULT", query));
            if (request._search)
            {
              sw.WriteLine(ParseFilters(request.GetFilters(), cmd));
            }
            cmd.CommandText = sw.ToString();
          }
          foreach (var a in args)
          {
            cmd.Parameters.AddWithValue(string.Format("@P{0}", paramCount++), a);
          }
          records = (int)cmd.ExecuteScalar();
        }

        paramCount = 1;
        using (var cmd = new SqlCommand(string.Empty, con))
        {
          using (var sw = new StringWriter())
          {
            sw.WriteLine(string.Format("SELECT * FROM ({0}) AS RESULT", query));
            if (request._search)
            {
              sw.WriteLine(ParseFilters(request.GetFilters(), cmd));
            }
            if (!string.IsNullOrWhiteSpace(request.sidx))
            {
              sw.WriteLine(string.Format(" ORDER BY {0} {1}", request.sidx, request.sord));
            }
            sw.WriteLine("OFFSET {0} ROWS", request.rows * (request.page - 1));
            sw.WriteLine("FETCH NEXT {0} ROWS ONLY", request.rows);
            cmd.CommandText = sw.ToString();
            foreach (var a in args)
            {
              cmd.Parameters.AddWithValue(string.Format("@P{0}", paramCount++), a);
            }

            ds = DataHelper.ExecuteDataSet(cmd);
          }
        }

        var pages = (int)Math.Ceiling((decimal)records / (decimal)request.rows);
        return new JqGridResult(pages, request.page, records, ds.Tables[0]);
      }
      catch (Exception ex)
      {
        throw;
      }
    }
  }
}

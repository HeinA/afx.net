﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Mvc.jqGrid
{
  public class Rule
  {
    public string field { get; set; }
    public string op { get; set; }
    public object data { get; set; }
  }
}

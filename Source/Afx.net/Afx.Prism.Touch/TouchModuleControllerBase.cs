﻿using Afx.Business;
using Afx.Business.Security;
using Afx.Prism.Events;
using Microsoft.Practices.Prism.Modularity;
using Microsoft.Practices.Prism.PubSubEvents;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.Touch
{
  public abstract class TouchModuleControllerBase : Controller, IModule, ITouchModuleController
  {
    public const string PageRegion = TouchApplicationViewModel.PageRegion;

    protected TouchModuleControllerBase(IController controller)
      : base(controller)
    {
    }

    protected TouchModuleControllerBase(string id, IController controller)
      : base(id, controller)
    {
    }

    protected override void ConfigureContainer()
    {
      Container.RegisterInstance<ITouchModuleController>(this);
      base.ConfigureContainer();
    }

    [Dependency]
    public IEventAggregator EventAggregator { get; set; }

    public virtual void Initialize()
    {
      ConfigureContainer();
      AfterContainerConfigured();
      Run();
    }

    public TViewModel Navigate<TViewModel>()
      where TViewModel : class, IViewModelBase
    {
      IRegion region = RegionManager.Regions[PageRegion];
      foreach (var v in region.Views)
      {
        region.Remove(v);
      }

      TViewModel vm = Container.Resolve<TViewModel>();
      region.Add(vm);
      return vm;
    }

    public TViewModel Navigate<TViewModel>(IRegion region)
      where TViewModel : class, IViewModelBase
    {
      foreach (var v in region.Views)
      {
        region.Remove(v);
      }

      TViewModel vm = Container.Resolve<TViewModel>();
      region.Add(vm);
      return vm;
    }

    public TViewModel Navigate<TViewModel, TModel>(IRegion region, IBusinessObject model)
      where TViewModel : class, IViewModel<TModel>
      where TModel : class, IBusinessObject
    {
      foreach (var v in region.Views)
      {
        region.Remove(v);
      }

      TViewModel vm = Container.Resolve<TViewModel>();
      vm.Model = (TModel)model;
      region.Add(vm);
      return vm;
    }

    public TViewModel Navigate<TViewModel>(IBusinessObject model)
      where TViewModel : class, IViewModel
    {
      try
      {
        IRegion region = RegionManager.Regions[PageRegion];
        foreach (var v in region.Views)
        {
          region.Remove(v);
        }

        TViewModel vm = Container.Resolve<TViewModel>();
        vm.Model = model;
        region.Add(vm);
        return vm;
      }
      catch
      {
        throw;
      }
    }

    protected virtual void OnNavigated(NavigationResult result)
    {
    }

    protected override void AfterContainerConfigured()
    {
      ApplicationController.EventAggregator.GetEvent<UserAuthenticatedEvent>().Subscribe(OnUserAuthenticated, ThreadOption.UIThread);
      //ApplicationController.EventAggregator.GetEvent<EditDocumentEvent>().Subscribe(OnEditDocument);
      base.AfterContainerConfigured();
    }

    protected virtual void OnUserAuthenticated(EventArgs obj)
    {
      if (SecurityContext.User != null)
      {
      }
    }

    protected virtual void ConfigureRibbon()
    {
    }

    internal virtual void ConfigureRibbonEnd()
    {
    }

    ITouchApplicationController mApplicationController;
    [Dependency]
    public ITouchApplicationController ApplicationController
    {
      get { return mApplicationController; }
      set { mApplicationController = value; }
    }

    [Dependency]
    public IRegionManager RegionManager { get; set; }
  }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.Touch.Events
{
  public class ExceptionEventArgs : EventArgs
  {
    public ExceptionEventArgs(Exception ex)
    {
      Exception = ex;
    }

    #region Exception Exception

    Exception mException;
    public Exception Exception
    {
      get { return mException; }
      private set { mException = value; }
    }

    #endregion
  }
}

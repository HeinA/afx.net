﻿using Afx.Business;
using Afx.Business.Security;
using Afx.Business.Service;
using Afx.Prism.Regions;
using Microsoft.Practices.Prism.Modularity;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Prism.UnityExtensions;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using Xceed.Wpf.Toolkit.Primitives;

namespace Afx.Prism.Touch
{
  public abstract class TouchBootstrapper : UnityBootstrapper
  {
    public TouchBootstrapper()
    {
      mCurrent = this;

      foreach (var ri in ExtensibilityManager.CompositionContainer.GetExportedValues<Afx.Prism.ResourceInfo>().OrderBy(ri1 => ri1.Priority))
      {
        ResourceDictionary dict = new ResourceDictionary();
        dict.Source = ri.GetUri();
        Application.Current.Resources.MergedDictionaries.Add(dict);
      }
    }

    static TouchBootstrapper mCurrent;
    public static TouchBootstrapper Current
    {
      get { return mCurrent; }
    }

    ITouchApplicationController mApplicationController;
    public ITouchApplicationController ApplicationController
    {
      get { return mApplicationController; }
      private set { mApplicationController = value; }
    }

    TouchApplicationView mApplicationView;

    protected override DependencyObject CreateShell()
    {
      TouchApplicationViewModel vm = CreateApplicationViewModel();
      Container.RegisterInstance<ITouchApplicationViewModel>(vm);

      mApplicationView = ApplicationController.GetInstance<TouchApplicationView>();
      Container.RegisterInstance<IApplicationView>(mApplicationView);

      return mApplicationView;
    }

    protected override void InitializeShell()
    {
      base.InitializeShell();
      Application.Current.MainWindow = (Window)this.Shell;
      Application.Current.MainWindow.Show();
    }

    protected override RegionAdapterMappings ConfigureRegionAdapterMappings()
    {
      RegionAdapterMappings mappings = base.ConfigureRegionAdapterMappings();
      mappings.RegisterMapping(typeof(WindowContainer), ApplicationController.GetInstance<WindowContainerRegionAdapter>());
      mappings.RegisterMapping(typeof(DockPanel), ApplicationController.GetInstance<DockPanelRegionAdapter>());
      return mappings;
    }

    protected virtual TouchApplicationViewModel CreateApplicationViewModel()
    {
      return ApplicationController.GetInstance<TouchApplicationViewModel>();
    }

    protected override void ConfigureContainer()
    {
      base.ConfigureContainer();
      IUnityContainer uc = this.Container;
      ApplicationController = new TouchApplicationController(uc, CreateApplicationContext());
    }

    protected virtual TouchApplication CreateApplicationContext()
    {
      return new TouchApplication();
    }

    internal AuthenticationResult GetLoginDetails()
    {
      return DefaultLoginDetails();
    }

    protected virtual AuthenticationResult DefaultLoginDetails()
    {
      ICredentialProvider cp = ExtensibilityManager.CompositionContainer.GetExportedValues<ICredentialProvider>().FirstOrDefault();
      if (cp == null) return null;
      LoginOrganizationalUnit loginOU = null;
      using (var svc = ProxyFactory.GetService<IAfxService>(DefaultServer.ServerName))
      {
        loginOU = svc.LoadLoginOrganizationalUnits().Where(ou => ou.GlobalIdentifier.Equals(cp.OrganizationalUnit)).FirstOrDefault();
      }
      return SecurityContext.Authenticate(loginOU, cp.Username, cp.Password);
    }

    protected override IModuleCatalog CreateModuleCatalog()
    {
      IModuleCatalog mc = base.CreateModuleCatalog();
      mc.AddModule(new ModuleInfo(Namespace.Afx, "Afx.Prism.Touch.TouchModuleController, Afx.Prism.Touch"));

      foreach (IModuleInfo mi in ExtensibilityManager.CompositionContainer.GetExportedValues<IModuleInfo>())
      {
        mc.AddModule(new ModuleInfo(mi.ModuleNamespace, TypeHelper.GetTypeName(mi.ModuleController), mi.ModuleDependencies));
      }
      return mc;
    }

    protected override void ConfigureModuleCatalog()
    {
      base.ConfigureModuleCatalog();
    }

    protected override void InitializeModules()
    {
      ApplicationController.Run();
      base.InitializeModules();
    }

  }
}

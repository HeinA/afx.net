﻿using Afx.Prism.Touch.Events;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.Configuration;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.Touch
{
  [ConfigurationElementType(typeof(CustomHandlerData))]
  public class ExceptionHandler : IExceptionHandler
  {
    public ExceptionHandler(NameValueCollection values)
    {
    }

    #region IExceptionHandler Members

    public Exception HandleException(Exception exception, Guid handlingInstanceId)
    {
      BackgroundWorker bw = new BackgroundWorker();
      bw.RunWorkerCompleted += (s, e) =>
        {
          try
          {
            TouchApplicationController.Current.Dispatcher.BeginInvoke(new Action(() =>
              {
                TouchApplicationController.Current.EventAggregator.GetEvent<ExceptionEvent>().Publish(new ExceptionEventArgs(exception));
              }), System.Windows.Threading.DispatcherPriority.Background);
            bw.Dispose();
          }
          catch
          {
#if DEBUG
            if (Debugger.IsAttached) Debugger.Break();
#endif
            throw;
          }
        };
      bw.RunWorkerAsync();

      return null;
    }

    #endregion
  }
}

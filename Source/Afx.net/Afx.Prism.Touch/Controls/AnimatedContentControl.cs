﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Afx.Prism.Touch.Controls
{
  public class AnimatedContentControl : ContentControl
  {
    private ContentControl _old;
    private ContentControl _current;

    public AnimatedContentControl()
    {
      DefaultStyleKey = typeof(AnimatedContentControl);
    }

    public override void OnApplyTemplate()
    {
      _old = (ContentControl)GetTemplateChild("_old");
      _current = (ContentControl)GetTemplateChild("_current");
      base.OnApplyTemplate();
    }

    protected override void OnContentChanged(object oldContent, object newContent)
    {
      base.OnContentChanged(oldContent, newContent);

      if (_current != null && _old != null)
      {
        _current.Content = newContent;
        _old.Content = oldContent;

        CreateFade(1, 0, _old);
        CreateFade(0, 1, _current);
      }
    }

    private void CreateFade(double from, double to, DependencyObject element)
    {

      DoubleAnimation da = new DoubleAnimation();
      da.Duration = new Duration(TimeSpan.FromSeconds(1));
      da.From = from;
      da.To = to;
      Storyboard.SetTarget(da, element);
      Storyboard.SetTargetProperty(da, new PropertyPath("Opacity"));

      Storyboard sb = new Storyboard();
      sb.Children.Add(da);
      sb.Begin();
    }
  }
}

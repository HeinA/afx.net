﻿using Afx.Business;
using Afx.Business.Security;
using Afx.Business.Service;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;

namespace Afx.Prism.Touch.Login
{
  [ViewModelRegistration]
  public class LoginViewModel : DialogViewModel<LoginCredentials>
  {
    [InjectionConstructor]
    public LoginViewModel(LoginCredentials context, IController controller)
      : base(context, controller)
    {
    }

    //#region Model Propertry string Username

    //public string Username
    //{
    //  get
    //  {
    //    if (Model == null) return default(string);
    //    return Model.Username;
    //  }
    //  set { Model.Username = value; }
    //}

    //#endregion

    //#region Model Propertry string PasswordHash

    //public string PasswordHash
    //{
    //  get
    //  {
    //    if (Model == null) return default(string);
    //    return Model.PasswordHash;
    //  }
    //  set { Model.PasswordHash = value; }
    //}

    //#endregion

    //#region string Server

    //public string Server
    //{
    //  get
    //  {
    //    if (Model == null) return GetDefaultValue<string>();
    //    return Model.Server;
    //  }
    //  set { Model.Server = value; }
    //}

    //#endregion

    //#region IEnumerable<Login> Servers

    //public IEnumerable<LoginOrganizationalUnit> OrganizationalUnits
    //{
    //  get
    //  {
    //    using (var svc = ProxyFactory.GetService<IAfxService>(DefaultServer.ServerName))
    //    {
    //      return svc.LoadLoginOrganizationalUnits();
    //    }
    //  }
    //}

    //#endregion

    #region DelegateCommand QuitCommand

    DelegateCommand mQuitCommand;
    public DelegateCommand QuitCommand
    {
      get { return mQuitCommand ?? (mQuitCommand = new DelegateCommand(ExecuteQuit, CanExecuteQuit)); }
    }

    bool CanExecuteQuit()
    {
      return true;
    }

    void ExecuteQuit()
    {
      Application.Current.MainWindow.Close();
    }

    #endregion

    #region DelegateCommand LoginCommand

    DelegateCommand mLoginCommand;
    public DelegateCommand LoginCommand
    {
      get { return mLoginCommand ?? (mLoginCommand = new DelegateCommand(ExecuteLogin, CanExecuteLogin)); }
    }

    bool CanExecuteLogin()
    {
      return true;
    }

    [Dependency]
    public TouchModuleController ModuleController { get; set; }

    void ExecuteLogin()
    {
      if (Model.Validate())
      {
        if (Model.AuthenticationResult != null)
        {
          ApplicationController.Current.Dispatcher.BeginInvoke(new Action(() =>
          {
            ModuleController.LoginUser(Model.AuthenticationResult);
          }), DispatcherPriority.Normal);
        }
      }
    }

    #endregion

  }
}

﻿using Afx.Business;
using Afx.Business.Documents;
using Afx.Prism.Documents;
using Afx.Prism.Events;
using Microsoft.Practices.Unity;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.Touch
{
  public class TouchApplicationController : ApplicationController, ITouchApplicationController
  {
    public TouchApplicationController(IUnityContainer container, TouchApplication applicationContext)
      : base("Touch Application Controller", container)
    {
      mApplicationContext = applicationContext;
      Container.RegisterInstance<TouchApplication>(mApplicationContext);
      Container.RegisterInstance<ITouchApplicationController>(this);
      mCurrent = this;

      foreach (Type t in ViewModelCatalog.ViewModels)
      {
        Container.RegisterType(typeof(object), t, t.FullName);
      }

      this.EventAggregator.GetEvent<UserAuthenticatedEvent>().Subscribe(OnUserAuthenticated);

      mViewer = new ReportViewer();

      //BackgroundWorker bw = new BackgroundWorker();
      //bw.DoWork += (s, e) =>
      //  {
      //  };
      //bw.RunWorkerCompleted += (s, e) =>
      //  {
      //    bw.Dispose();
      //  };
      //bw.RunWorkerAsync();
    }

    static TouchApplicationController mCurrent;
    public static new TouchApplicationController Current
    {
      get { return TouchApplicationController.mCurrent; }
    }

    TouchApplication mApplicationContext;
    public TouchApplication ApplicationContext
    {
      get { return mApplicationContext; }
    }

    private void OnUserAuthenticated(EventArgs obj)
    {
    }

    ReportViewer mViewer;
    public void PrintReport(Document document, bool preview)
    {
      //BackgroundWorker bw = new BackgroundWorker();
      //bw.DoWork += (s, e) =>
      //{
        // Variables
        Warning[] warnings;
        string[] streamIds;
        string mimeType = string.Empty;
        string encoding = string.Empty;
        string extension = string.Empty;


        string contractName = document.DocumentType.GlobalIdentifier.ToString("b");
        IDocumentPrintInfo reportInfo = ExtensibilityManager.CompositionContainer.GetExportedValues<IDocumentPrintInfo>(contractName).OrderBy(ri1 => ri1.Priority).FirstOrDefault();
        if (reportInfo == null)
        {
          System.Media.SystemSounds.Hand.Play();
          return;
        }
        object scope = reportInfo.GetScope(this);

        mViewer.LocalReport.LoadReportDefinition(reportInfo.ReportStream(null));

        SetupReport(mViewer);
        RefreshReportData(reportInfo, document, mViewer, scope);

        if (preview)
        {
          byte[] bytes = mViewer.LocalReport.Render("PDF", null, out mimeType, out encoding, out extension, out streamIds, out warnings);
          string file = string.Format("{0}.pdf", Path.GetTempFileName());
          File.WriteAllBytes(file, bytes);
          Process.Start(file);
        }
        else
        {
          try
          {
            ReportPrintDocument pd = new ReportPrintDocument(mViewer.LocalReport);
            pd.PrinterSettings.Copies = reportInfo.Copies;
            pd.Print();
          }
          catch
          {
            throw;
          }
        }
      //};
      //bw.RunWorkerCompleted += (s, e) =>
      //{
      //  if (e.Error != null) if (ExceptionHelper.HandleException(e.Error)) throw e.Error;
      //  bw.Dispose();
      //};
      //bw.RunWorkerAsync();
    }

    protected void SetupReport(ReportViewer viewer)
    {
      PageSettings pageSettings = viewer.GetPageSettings();
      PaperSize psA4 = null;
      foreach (PaperSize ps in pageSettings.PrinterSettings.PaperSizes)
      {
        if (ps.Kind == PaperKind.A4)
        {
          psA4 = ps;
          break;
        }
      }
      if (psA4 != null)
      {
        pageSettings.PaperSize = psA4;
        viewer.SetPageSettings(pageSettings);
      }

      viewer.LocalReport.DataSources.Clear();
      viewer.ProcessingMode = ProcessingMode.Local;
    }

    protected void RefreshReportData(IDocumentPrintInfo reportInfo, Document document, ReportViewer rv, object scope)
    {
      reportInfo.RefreshReportData(rv.LocalReport, document, scope, null);
      rv.LocalReport.Refresh();
      rv.RefreshReport();
    }
  }
}

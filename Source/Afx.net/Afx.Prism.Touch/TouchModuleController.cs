﻿using Afx.Business;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Business.Service;
using Afx.Prism.Events;
using Afx.Prism.Touch.Login;
using Afx.Prism.Touch.WaitAnimation;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace Afx.Prism.Touch
{
  public class TouchModuleController : TouchModuleControllerBase
  {
    [InjectionConstructor]
    public TouchModuleController(IController controller)
      : base("Touch Module Controller", controller)
    {
      Container.RegisterInstance<TouchModuleController>(this);
    }

    protected override bool OnRun()
    {
      ApplicationController.Dispatcher.BeginInvoke(new Action(() =>
      {
        AuthenticationResult ar = null;
        int retries = 0;
        bool loggedin = false;
        while (!loggedin)
        {
          try
          {
            ar = TouchBootstrapper.Current.GetLoginDetails();
            if (ar == null) break;
            //DebugHelper.LogMessage("Attempting Automated Login...");
            LoginUser(ar);
            loggedin = true;
          }
          catch (Exception ex)
          {
            if (retries++ > 5)
            {
              ExceptionHelper.HandleException(ex);
              break;
            }
            Thread.Sleep(1000);
          }
        }
        if (ar == null)
        {
          Navigate<LoginViewModel>();
        }
      }), DispatcherPriority.ApplicationIdle);

      return base.OnRun();
    }

    public void LoginUser(AuthenticationResult ar)
    {
      if (ar.User != null)
      {
        //DebugHelper.LogMessage("Logging in user...");
        Navigate<WaitAnimationViewModel>();

        SecurityContext.Login(ar);


        BasicCollection<BusinessObject> cache = null;
        Stopwatch sw = new Stopwatch();
        sw.Start();

        BackgroundWorker bw = new BackgroundWorker();
        bw.DoWork += (s, e) =>
        {
          //DebugHelper.LogMessage("Loading Cache...");
          using (var svc = ProxyFactory.GetService<IAfxService>(SecurityContext.ServerName))
          {
            cache = svc.LoadCache();
          }
        };
        bw.RunWorkerCompleted += (s, e) =>
        {
          try
          {
            if (e.Error != null) throw e.Error;
            Cache.Instance.LoadCache(cache);
            //DebugHelper.LogMessage("Cache loaded.");
            sw.Stop();
            //LogHelper.LogMessage("Client Cache Load: {0}ms", sw.ElapsedMilliseconds);

            ApplicationController.EventAggregator.GetEvent<UserAuthenticatedEvent>().Publish(EventArgs.Empty);
            bw.Dispose();
          }
          finally
          {
          }
        };
        bw.RunWorkerAsync();

        //DebugHelper.LogMessage("User Logged in.");
      }
    }
  }
}

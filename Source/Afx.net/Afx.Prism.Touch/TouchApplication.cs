﻿using Afx.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.Touch
{
  public class TouchApplication : BusinessObject
  {
    #region string Title

    public const string TitleProperty = "Title";
    string mTitle = "Touch Application";
    public string Title
    {
      get { return mTitle; }
      set { SetProperty<string>(ref mTitle, value, TitleProperty); }
    }

    #endregion
  }
}

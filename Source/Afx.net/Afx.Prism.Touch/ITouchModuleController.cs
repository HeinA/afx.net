﻿using Afx.Business;using Microsoft.Practices.Prism.PubSubEvents;
using Microsoft.Practices.Prism.Regions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Afx.Prism.Touch
{
  public interface ITouchModuleController
  {
    TViewModel Navigate<TViewModel>() where TViewModel : class, IViewModelBase;
    TViewModel Navigate<TViewModel>(IRegion region) where TViewModel : class, IViewModelBase;
    TViewModel Navigate<TViewModel>(IBusinessObject model) where TViewModel : class, IViewModel;
    TViewModel Navigate<TViewModel, TModel>(IRegion region, IBusinessObject model)
      where TViewModel : class, IViewModel<TModel>
      where TModel : class, IBusinessObject;
    IEventAggregator EventAggregator { get; }
    IRegionManager RegionManager { get; }
  }
}

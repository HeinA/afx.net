﻿using Afx.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.Touch
{
  public class DialogViewModel<TModel> : ViewModel<TModel>, IViewModel<TModel>
    where TModel : class, IBusinessObject
  {
    protected DialogViewModel(IController controller)
      : base(controller)
    {
    }

    protected DialogViewModel(TModel context, IController controller)
      : this(controller)
    {
      Model = context;
    }

    protected override void OnModelCompositionChanged(CompositionChangedEventArgs e)
    {
      //base.OnModelCompositionChanged(e);
    }
  }
}

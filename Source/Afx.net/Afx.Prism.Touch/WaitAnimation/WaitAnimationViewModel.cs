﻿using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.Touch.WaitAnimation
{
  [ViewModelRegistration]
  public class WaitAnimationViewModel : ViewModel
  {
    [InjectionConstructor]
    public WaitAnimationViewModel(IController controller)
      : base(controller)
    {
    }

  }
}

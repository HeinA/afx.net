﻿using Afx.Business;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.Touch
{
  public static class ViewModelCatalog
  {
    static Collection<Type> mViewModels = new Collection<Type>();

    public static IEnumerable<Type> ViewModels
    {
      get { return ViewModelCatalog.mViewModels; }
    }

    static ViewModelCatalog()
    {
      Afx.Business.Utilities.PreLoadDeployedAssemblies();
      foreach (Assembly a in AppDomain.CurrentDomain.GetAssemblies())
      {
        try
        {
          foreach (Type t in a.GetTypes().Where(t => t.IsDefined(typeof(ViewModelRegistrationAttribute))))
          {
            mViewModels.Add(t);
          }
        }
        catch
        {
          throw;
        }
      }
    }
  }
}

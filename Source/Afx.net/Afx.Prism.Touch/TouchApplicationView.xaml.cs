﻿using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Afx.Prism.Touch
{
  /// <summary>
  /// Interaction logic for MainWindow.xaml
  /// </summary>
  public partial class TouchApplicationView : Window, IApplicationView
  {
    public TouchApplicationView()
    {
      InitializeComponent();
    }

    [Dependency]
    public ITouchApplicationViewModel ViewModel
    {
      get { return (ITouchApplicationViewModel)DataContext; }
      set { DataContext = value; }
    }
  }
}

﻿using Afx.Business.Documents;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.Touch
{
  public interface ITouchApplicationController : IApplicationController
  {
    void PrintReport(Document document, bool preview);
  }
}

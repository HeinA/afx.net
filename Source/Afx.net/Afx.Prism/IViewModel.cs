﻿using Afx.Business;
using Afx.Business.Collections;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;

namespace Afx.Prism
{
  public interface IViewModelBase : INotifyPropertyChanged //, IDisposable //, IViewModelProvider
  {
    IViewModelBase Parent { get; }
    IController Controller { get; }
    bool IsFocused { get; set; }
  }

  public interface IViewModel : IViewModelBase
  {
    IBusinessObject Model { get; set; }
    string Error { get; }
    Visibility ErrorVisibility { get; }
    bool IsReadOnly { get; }
    bool IsValid { get; }
    bool Validate();    
    bool IsViewModelOf(BusinessObject model);
    void OnEnterPressed();

    IEnumerable<TViewModel> ResolveViewModels<TViewModel, TModel>(IList models, [CallerMemberName] string propertyName = null)
      where TViewModel : class, IViewModel
      where TModel : class, IBusinessObject;

    void ResetViewModelCollections();
  }

  public interface IViewModel<TModel> : IViewModel, IDataErrorInfo
    where TModel : class, IBusinessObject
  {
    new TModel Model { get; set; }
  }
}

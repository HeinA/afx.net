﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace Afx.Prism
{
  public class CollectionCountToVisibilityConverter : IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    {
      IList list = value as IList;
      int visibleCount = 0;
      if (list != null)
      {
        if (list.Count == 0) return Visibility.Collapsed;
        foreach (object o in list)
        {
          IVisibilityAware va = o as IVisibilityAware;
          if (va == null || va != null && va.IsVisible) visibleCount++;
        }
      }
      if (visibleCount == 0) return Visibility.Collapsed;
      return Visibility.Visible;
    }

    public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    {
      throw new NotImplementedException();
    }
  }
}

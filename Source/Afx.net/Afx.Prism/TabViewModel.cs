﻿using Afx.Business;
using Microsoft.Practices.Prism;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace Afx.Prism
{
  public abstract class TabViewModel
  {
    public const string SuspendSelection = "Afx.Prism.TabViewModel.SuspendSelection";
  }
  
  public class TabViewModel<TModel> : StatefullViewModel<TModel>, IActiveAware
    where TModel : class, IBusinessObject
  {
    protected TabViewModel(IController controller)
      : base(controller)
    {
    }

    string mTitle;
    public string Title
    {
      get { return mTitle; }
      set { SetProperty<string>(ref mTitle, value); }
    }

    #region Brush Foreground

    public SolidColorBrush Foreground
    {
      get
      {
        if (Model == null) return new SolidColorBrush(Colors.Black);
        if (!string.IsNullOrWhiteSpace(Model.Error)) return new SolidColorBrush(Colors.Red);
        return new SolidColorBrush(Colors.Black);
      }
    }

    #endregion

    #region FontWeight FontWeight

    public FontWeight FontWeight
    {
      get
      {
        if (Model == null) return FontWeights.Normal;
        if (!string.IsNullOrWhiteSpace(Model.Error)) return FontWeights.Bold;
        return FontWeights.Normal;
      }
    }

    #endregion

    public bool IsActive
    {
      get { return Controller.GetState<bool>(this.GetType().FullName); }
      set
      {
        if (Controller.GetState<bool>(TabViewModel.SuspendSelection)) return;
        if (Controller.SetState<bool>(this.GetType().FullName, value))
        {
          if (IsActiveChanged != null) IsActiveChanged(this, EventArgs.Empty);
        }
      }
    }

    public event EventHandler IsActiveChanged;
  }
}

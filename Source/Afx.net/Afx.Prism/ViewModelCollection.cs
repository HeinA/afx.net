﻿using Afx.Business;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism
{
  /// <summary>
  /// http://stackoverflow.com/questions/15830008/mvvm-and-collections-of-vms
  /// </summary>
  /// <typeparam name="TViewModel"></typeparam>
  /// <typeparam name="TModel"></typeparam>
  internal class ViewModelCollection<TViewModel, TModel> : ObservableCollection<TViewModel>
    where TViewModel : class, IViewModel
    where TModel : class, IBusinessObject
  {
    private readonly IList mModels;
    private bool mSynchDisabled;
    private readonly IViewModelBase mOwner;

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="models">List of models to synch with</param>
    /// <param name="viewModelProvider"></param>
    /// <param name="context"></param>
    /// <param name="autoFetch">
    /// Determines whether the collection of ViewModels should be
    /// fetched from the model collection on construction
    /// </param>
    public ViewModelCollection(IList models, IViewModelBase owner, bool autoFetch = true)
    {
      mModels = models;
      mOwner = owner;

      // Register change handling for synchronization
      // from ViewModels to Models
      CollectionChanged += ViewModelCollectionChanged;

      // If model collection is observable register change
      // handling for synchronization from Models to ViewModels
      INotifyCollectionChanged ncc = models as INotifyCollectionChanged;
      if (ncc != null)
      {
        ncc.CollectionChanged += ModelCollectionChanged;
      }

      // Fecth ViewModels
      if (autoFetch) FetchViewModels();
    }

    /// <summary>
    /// CollectionChanged event of the ViewModelCollection
    /// </summary>
    public override sealed event NotifyCollectionChangedEventHandler CollectionChanged
    {
      add { base.CollectionChanged += value; }
      remove { base.CollectionChanged -= value; }
    }

    /// <summary>
    /// Load VM collection from model collection
    /// </summary>
    public void FetchViewModels()
    {
      // Deactivate change pushing
      mSynchDisabled = true;

      // Clear collection
      Clear();

      // Create and add new VM for each model
      foreach (var model in mModels)
        AddViewModel((TModel)model);

      // Reactivate change pushing
      mSynchDisabled = false;
    }

    private void ViewModelCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
    {
      // Return if synchronization is internally disabled
      if (mSynchDisabled) return;

      // Disable synchronization
      mSynchDisabled = true;

      switch (e.Action)
      {
        case NotifyCollectionChangedAction.Add:
          foreach (var m in e.NewItems.OfType<IViewModel>().Select(v => v.Model).OfType<TModel>())
            mModels.Add(m);
          break;

        case NotifyCollectionChangedAction.Remove:
          foreach (var m in e.OldItems.OfType<IViewModel>().Select(v => v.Model).OfType<TModel>())
            mModels.Remove(m);
          break;

        case NotifyCollectionChangedAction.Reset:
          mModels.Clear();
          foreach (var m in e.NewItems.OfType<IViewModel>().Select(v => v.Model).OfType<TModel>())
            mModels.Add(m);
          break;
      }

      //Enable synchronization
      mSynchDisabled = false;
    }

    private void ModelCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
    {
      if (mSynchDisabled) return;
      mSynchDisabled = true;

      switch (e.Action)
      {
        case NotifyCollectionChangedAction.Add:
          foreach (var m in e.NewItems.OfType<TModel>())
            this.AddIfNotNull(CreateViewModel(m));
          break;

        case NotifyCollectionChangedAction.Remove:
          foreach (var m in e.OldItems.OfType<TModel>())
            this.RemoveIfContains(GetViewModel(m));
          break;

        case NotifyCollectionChangedAction.Reset:
          Clear();
          FetchViewModels();
          break;
      }

      mSynchDisabled = false;
    }

    private void RemoveIfContains(TViewModel tViewModel)
    {
      if (this.Contains(tViewModel)) this.Remove(tViewModel);
    }

    private void AddIfNotNull(TViewModel tViewModel)
    {
      if (tViewModel != null) Add(tViewModel);
    }

    private TViewModel CreateViewModel(TModel model)
    {
      if (mOwner.Controller.State == ControllerState.Terminated) return null;
      TViewModel vm = mOwner.Controller.GetCreateViewModel<TViewModel>(model, mOwner);
      return vm;
    }

    private TViewModel GetViewModel(TModel model)
    {
      return Items.OfType<IViewModel<TModel>>().FirstOrDefault(v => v.IsViewModelOf(model as BusinessObject)) as TViewModel;
    }

    /// <summary>
    /// Adds a new ViewModel for the specified Model instance
    /// </summary>
    /// <param name="model">Model to create ViewModel for</param>
    public void AddViewModel(TModel model)
    {
      Add(CreateViewModel(model));
    }

    /// <summary>
    /// Adds a new ViewModel with a new model instance of the specified type,
    /// which is the ModelType or derived from the Model type
    /// </summary>
    /// <typeparam name="TSpecificModel">Type of Model to add ViewModel for</typeparam>
    public void AddNew<TSpecificModel>() where TSpecificModel : TModel, new()
    {
      var m = new TSpecificModel();
      Add(CreateViewModel(m));
    }
  }
}

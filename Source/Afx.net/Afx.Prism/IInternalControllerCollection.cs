﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism
{
  interface IInternalControllerCollection
  {
    void Add(IController controller);
    void Remove(IController controller);
  }
}

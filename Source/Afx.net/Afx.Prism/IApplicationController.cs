﻿using Microsoft.Practices.Prism.PubSubEvents;
using Microsoft.Practices.Prism.Regions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace Afx.Prism
{
  public interface IApplicationController : IController
  {
    IRegionManager RegionManager { get; }
    IEventAggregator EventAggregator { get; }
    Dispatcher Dispatcher { get; }
  }
}

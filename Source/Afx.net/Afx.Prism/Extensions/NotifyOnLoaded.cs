﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Interactivity;

namespace Afx.Prism.Extensions
{
  public static class NotifyOnLoaded
  {
    public static readonly DependencyProperty NotifyProperty = DependencyProperty.RegisterAttached("Notify", typeof(bool), typeof(NotifyOnLoaded), new PropertyMetadata(false, Notify));

    private static void Notify(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
      FrameworkElement fwe = d as FrameworkElement;
      if (fwe == null) return;
      if ((bool)e.NewValue)
      {
        System.Windows.Interactivity.TriggerCollection triggerCollection = Interaction.GetTriggers(fwe);
        System.Windows.Interactivity.EventTrigger trigger = new System.Windows.Interactivity.EventTrigger("Loaded");
        InteractiveCommand commandAction = new InteractiveCommand();
        BindingOperations.SetBinding(commandAction, InteractiveCommand.CommandProperty, new Binding("LoadedCommand"));
        trigger.Actions.Add(commandAction);
        triggerCollection.Add(trigger);
      }
    }

    public static bool GetNotify(DependencyObject obj)
    {
      return (bool)obj.GetValue(NotifyProperty);
    }

    public static void SetNotify(DependencyObject obj, bool value)
    {
      obj.SetValue(NotifyProperty, value);
    }
  }
}

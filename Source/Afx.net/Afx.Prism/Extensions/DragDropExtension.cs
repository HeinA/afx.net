﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;

namespace Afx.Prism.Extensions
{
  public class DragDropExtension
  {
    public static bool GetAllowDrag(DependencyObject obj)
    {
      return (bool)obj.GetValue(AllowDragProperty);
    }

    public static void SetAllowDrag(DependencyObject obj, bool value)
    {
      obj.SetValue(AllowDragProperty, value);
    }

    public static readonly DependencyProperty AllowDragProperty = DependencyProperty.RegisterAttached("AllowDrag", typeof(bool), typeof(DragDropExtension), new FrameworkPropertyMetadata(false, OnAllowDragPropertyChanged) { BindsTwoWayByDefault = true, DefaultUpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged });

    private static void OnAllowDragPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
      new DragableSource((FrameworkElement)d);
    }

    public static bool GetAllowDrop(DependencyObject obj)
    {
      return (bool)obj.GetValue(AllowDropProperty);
    }

    public static void SetAllowDrop(DependencyObject obj, bool value)
    {
      obj.SetValue(AllowDropProperty, value);
    }

    public static readonly DependencyProperty AllowDropProperty = DependencyProperty.RegisterAttached("AllowDrop", typeof(bool), typeof(DragDropExtension), new FrameworkPropertyMetadata(false, OnAllowDropPropertyChanged) { BindsTwoWayByDefault = true, DefaultUpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged });

    private static void OnAllowDropPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
      new DropTarget((FrameworkElement)d);
    }

    #region class Dragable

    class DragableSource
    {
      public DragableSource(FrameworkElement fwe)
      {
        FrameworkElement = fwe;
        if (fwe.IsLoaded)
        {
          Initialize();
        }
        else
        {
          fwe.Loaded += (s,e) =>
            {
              Initialize();
            };
        }
      }

      void Initialize()
      {
        Dragable = FrameworkElement.DataContext as IDragable;
        if (Dragable == null) throw new InvalidOperationException("ViewModel does not Implement IDragable");
        FrameworkElement.PreviewMouseLeftButtonDown -= FrameworkElement_PreviewMouseLeftButtonDown;
        FrameworkElement.PreviewMouseLeftButtonDown += FrameworkElement_PreviewMouseLeftButtonDown;
        FrameworkElement.MouseMove -= FrameworkElement_MouseMove;
        FrameworkElement.MouseMove += FrameworkElement_MouseMove;
      }

      void FrameworkElement_MouseMove(object sender, MouseEventArgs e)
      {
        Point mousePos = e.GetPosition(null);
        Vector diff = StartPoint - mousePos;

        if (e.LeftButton == MouseButtonState.Pressed && (Math.Abs(diff.X) > SystemParameters.MinimumHorizontalDragDistance || Math.Abs(diff.Y) > SystemParameters.MinimumVerticalDragDistance))
        {
          object o = Dragable.GetDragData();
          if (o != null)
          {
            DataObject dragData = new DataObject("AfxDrag", o);
            DragDrop.DoDragDrop(FrameworkElement, dragData, DragDropEffects.Link);
          }
        }
      }

      void FrameworkElement_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
      {
        StartPoint = e.GetPosition(null);
      }

      #region Point StartPoint

      public const string StartPointProperty = "StartPoint";
      Point mStartPoint;
      public Point StartPoint
      {
        get { return mStartPoint; }
        private set { mStartPoint = value; }
      }

      #endregion

      #region FrameworkElement FrameworkElement

      FrameworkElement mFrameworkElement;
      public FrameworkElement FrameworkElement
      {
        get { return mFrameworkElement; }
        set { mFrameworkElement = value; }
      }

      #endregion

      #region IDragable Dragable

      IDragable mDragable;
      public IDragable Dragable
      {
        get { return mDragable; }
        private set { mDragable = value; }
      }

      #endregion
    }

    #endregion

    class DropTarget
    {
      public DropTarget(FrameworkElement fwe)
      {
        FrameworkElement = fwe;
        if (fwe.IsLoaded)
        {
          Initialize();
        }
        else
        {
          fwe.Loaded += (s, e) =>
          {
            Initialize();
          };
        }
      }

      void Initialize()
      {
        Target = FrameworkElement.DataContext as IDropTarget;
        if (Target == null) throw new InvalidOperationException("ViewModel does not Implement IDropTarget");
        FrameworkElement.DragEnter -= FrameworkElement_DragEnter;
        FrameworkElement.DragEnter += FrameworkElement_DragEnter;
        FrameworkElement.Drop -= FrameworkElement_Drop;
        FrameworkElement.Drop += FrameworkElement_Drop;
        FrameworkElement.AllowDrop = true;
      }

      void FrameworkElement_Drop(object sender, DragEventArgs e)
      {
        object o = null;
        if (e.Data.GetDataPresent("AfxDrag"))
        {
          o = e.Data.GetData("AfxDrag");
          if (Target.CanDrop(o))
          {
            Target.DoDrop(o);
          }
        }
      }

      void FrameworkElement_DragEnter(object sender, DragEventArgs e)
      {
        object o = null;
        if (e.Data.GetDataPresent("AfxDrag"))
        {
          o = e.Data.GetData("AfxDrag");
          if (Target.CanDrop(o))
          {
            e.Effects = DragDropEffects.Link;
          }
        }
      }

      #region FrameworkElement FrameworkElement

      FrameworkElement mFrameworkElement;
      public FrameworkElement FrameworkElement
      {
        get { return mFrameworkElement; }
        set { mFrameworkElement = value; }
      }

      #endregion

      #region IDropTarget Target

      IDropTarget mDropTarget;
      public IDropTarget Target
      {
        get { return mDropTarget; }
        private set { mDropTarget = value; }
      }

      #endregion
    }
  }
}

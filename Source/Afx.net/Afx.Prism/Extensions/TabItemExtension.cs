﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace Afx.Prism.Extensions
{
  public static class TabItemExtension
  {
    public static bool GetIsSelected(DependencyObject obj)
    {
      return (bool)obj.GetValue(IsSelectedProperty);
    }

    public static void SetIsSelected(DependencyObject obj, bool value)
    {
      obj.SetValue(IsSelectedProperty, value);
    }

    public static readonly DependencyProperty IsSelectedProperty = DependencyProperty.RegisterAttached("IsSelected", typeof(bool), typeof(TabItemExtension), new FrameworkPropertyMetadata(false, OnIsSelectedPropertyChanged));

    private static void OnIsSelectedPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
      var ti = (TabItem)d;
      if ((bool)e.NewValue)
      {
        ti.IsSelected = true;
        SetIsSelected(d, false);
      }
    }
  }
}

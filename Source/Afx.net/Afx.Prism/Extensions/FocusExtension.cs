﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Threading;

namespace Afx.Prism.Extensions
{
  public static class FocusExtension
  {
    public static bool GetIsFocused(DependencyObject obj)
    {
      return (bool)obj.GetValue(IsFocusedProperty);
    }

    public static void SetIsFocused(DependencyObject obj, bool value)
    {
      obj.SetValue(IsFocusedProperty, value);
    }

    public static readonly DependencyProperty IsFocusedProperty = DependencyProperty.RegisterAttached("IsFocused", typeof(bool), typeof(FocusExtension), new FrameworkPropertyMetadata(false, OnIsFocusedPropertyChanged) { BindsTwoWayByDefault = true, DefaultUpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged });

    private static void OnIsFocusedPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
      var fwe = (FrameworkElement)d;
      if (fwe.IsLoaded)
      {
        if ((bool)e.NewValue)
        {
          fwe.Focus();
          SetIsFocused(d, false);
        }
      }
      else
      {
        fwe.Loaded += fwe_Loaded;
      }
    }

    static void fwe_Loaded(object sender, RoutedEventArgs e)
    {
      var fwe = (FrameworkElement)sender;
      fwe.Loaded -= fwe_Loaded;
      fwe.Focus();
      SetIsFocused(fwe, false);
    }

    public static bool GetIsFocusedAsync(DependencyObject obj)
    {
      return (bool)obj.GetValue(IsFocusedAsyncProperty);
    }

    public static void SetIsFocusedAsync(DependencyObject obj, bool value)
    {
      obj.SetValue(IsFocusedAsyncProperty, value);
    }

    public static readonly DependencyProperty IsFocusedAsyncProperty = DependencyProperty.RegisterAttached("IsFocusedAsync", typeof(bool), typeof(FocusExtension), new FrameworkPropertyMetadata(false, OnIsFocusedAsyncPropertyChanged) { BindsTwoWayByDefault = true, DefaultUpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged });

    private static void OnIsFocusedAsyncPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
      var fwe = (FrameworkElement)d;
      if ((bool)e.NewValue)
      {
        fwe.Dispatcher.BeginInvoke(new System.Action(() =>
        {
          try
          {
            fwe.Focus();
            SetIsFocusedAsync(d, false);
          }
          catch
          {
#if DEBUG
            if (System.Diagnostics.Debugger.IsAttached) System.Diagnostics.Debugger.Break();
#endif
          }
        }), DispatcherPriority.Background);
      }
    }
  }
}

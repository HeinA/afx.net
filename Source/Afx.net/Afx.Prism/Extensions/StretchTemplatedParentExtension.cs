﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace Afx.Prism.Extensions
{
  public static class ContentPresenterExtension
  {
    public static bool GetIsStretched(DependencyObject obj)
    {
      return (bool)obj.GetValue(IsStretchedProperty);
    }

    public static void SetIsStretched(DependencyObject obj, bool value)
    {
      obj.SetValue(IsStretchedProperty, value);
    }

    public static readonly DependencyProperty IsStretchedProperty = DependencyProperty.RegisterAttached("IsStretched", typeof(bool), typeof(ContentPresenterExtension), new FrameworkPropertyMetadata(false, OnIsStretchedPropertyChanged));

    private static void OnIsStretchedPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
      var fwe = (FrameworkElement)d;
      fwe.Loaded += fwe_Loaded;
    }

    static void fwe_Loaded(object sender, RoutedEventArgs e)
    {
      var fwe = (FrameworkElement)sender;
      ContentPresenter cp = fwe.TemplatedParent as ContentPresenter;
      if (cp != null)
      {
        cp.HorizontalAlignment = HorizontalAlignment.Stretch;
      }
    }
  }
}

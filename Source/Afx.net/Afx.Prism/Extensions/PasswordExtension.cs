﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace Afx.Prism.Extensions
{
  public static class PasswordExtension
  {
    public static readonly DependencyProperty AttachProperty = DependencyProperty.RegisterAttached("Attach", typeof(bool), typeof(PasswordExtension), new PropertyMetadata(false, Attach));

    static PasswordHasher mPasswordHasher = new PasswordHasher();
    private static void Attach(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
      PasswordBox pb = (PasswordBox)d;
      pb.PasswordChanged -= PasswordChanged;
      pb.PasswordChanged += PasswordChanged;
    }

    public static bool GetAttach(DependencyObject obj)
    {
      return (bool)obj.GetValue(AttachProperty);
    }

    public static void SetAttach(DependencyObject obj, bool value)
    {
      obj.SetValue(AttachProperty, value);
    }

    static void PasswordChanged(object sender, RoutedEventArgs e)
    {
      PasswordBox pwb = (PasswordBox)sender;
      HashAlgorithm algoritm = new SHA256Managed();
      string hash = ASCIIEncoding.ASCII.GetString(algoritm.ComputeHash(ASCIIEncoding.ASCII.GetBytes(pwb.Password)));
      pwb.SetValue(PasswordHashProperty, hash);
      //string s = mPasswordHasher.HashPassword(pwb.Password);
      pwb.SetValue(PasswordHash2Property, pwb.Password);

    }

    public static readonly DependencyProperty PasswordHashProperty = DependencyProperty.RegisterAttached("PasswordHash", typeof(string), typeof(PasswordExtension));

    public static bool GetPasswordHash(DependencyObject obj)
    {
      return (bool)obj.GetValue(PasswordHashProperty);
    }

    public static void SetPasswordHash(DependencyObject obj, bool value)
    {
      obj.SetValue(PasswordHashProperty, value);
    }

    public static readonly DependencyProperty PasswordHash2Property = DependencyProperty.RegisterAttached("PasswordHash2", typeof(string), typeof(PasswordExtension));

    public static bool GetPasswordHash2(DependencyObject obj)
    {
      return (bool)obj.GetValue(PasswordHash2Property);
    }

    public static void SetPasswordHash2(DependencyObject obj, bool value)
    {
      obj.SetValue(PasswordHash2Property, value);
    }
  }
}

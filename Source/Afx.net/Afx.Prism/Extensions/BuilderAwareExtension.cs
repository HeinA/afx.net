﻿using Microsoft.Practices.ObjectBuilder2;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.ObjectBuilder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.Extensions
{
  class BuilderAwareExtension : UnityContainerExtension
  {
    protected override void Initialize()
    {
      Context.Strategies.AddNew<BuilderAwareStrategy>(UnityBuildStage.PostInitialization);
    }
  }
}

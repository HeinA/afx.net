﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace Afx.Prism.Extensions
{
  public static class NotifyViewModelOnEnter
  {
    public static bool GetIsEnabled(DependencyObject obj)
    {
      return (bool)obj.GetValue(IsEnabledProperty);
    }

    public static void SetIsEnabled(DependencyObject obj, bool value)
    {
      obj.SetValue(IsEnabledProperty, value);
    }

    public static readonly DependencyProperty IsEnabledProperty = DependencyProperty.RegisterAttached("IsEnabled", typeof(bool), typeof(FocusExtension), new FrameworkPropertyMetadata(false, OnIsEnabledPropertyChanged) { BindsTwoWayByDefault = true, DefaultUpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged });

    private static void OnIsEnabledPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
      var fwe = (FrameworkElement)d;
      fwe.DataContextChanged += fwe_DataContextChanged;
      IViewModel vm = (IViewModel)fwe.DataContext;
      if (vm != null)
      {
        if ((bool)e.NewValue)
        {
          fwe.PreviewKeyDown += fwe_PreviewKeyDown;
        }
        else
        {
          fwe.PreviewKeyDown -= fwe_PreviewKeyDown;
        }
      }
    }

    static void fwe_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
    {
      var fwe = (FrameworkElement)sender;

      IViewModel vm = (IViewModel)e.OldValue;
      if (vm != null)
      {
        fwe.PreviewKeyDown -= fwe_PreviewKeyDown;
      }

      vm = (IViewModel)e.NewValue;
      if (vm != null)
      {
        if (GetIsEnabled(fwe))
        {
          fwe.PreviewKeyDown += fwe_PreviewKeyDown;
        }
        else
        {
          fwe.PreviewKeyDown -= fwe_PreviewKeyDown;
        }
      }
    }

    static void fwe_PreviewKeyDown(object sender, System.Windows.Input.KeyEventArgs e)
    {
      var fwe = (FrameworkElement)sender;
      IViewModel vm = (IViewModel)fwe.DataContext;
      if (vm != null && e.Key == System.Windows.Input.Key.Return)
      {
        vm.OnEnterPressed();
      }
    }
  }
}

﻿using Afx.Prism.Events;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace Afx.Prism.Extensions
{
  public static class GridExtension
  {
    public static bool GetIsFocused(DependencyObject obj)
    {
      return (bool)obj.GetValue(IsExtensibleProperty);
    }

    public static void SetIsFocused(DependencyObject obj, bool value)
    {
      obj.SetValue(IsExtensibleProperty, value);
    }

    public static readonly DependencyProperty IsExtensibleProperty = DependencyProperty.RegisterAttached("IsExtensible", typeof(bool), typeof(GridExtension), new FrameworkPropertyMetadata(false, OnIsExtensiblePropertyChanged) { BindsTwoWayByDefault = true, DefaultUpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged });

    private static void OnIsExtensiblePropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
      if (((bool)e.NewValue) == true && d is DataGrid)
      {
        DataGrid myGrid = d as DataGrid;
        var args = new GetGridButtonsEventArgs(myGrid.DataContext);
        ApplicationController.Current.EventAggregator.GetEvent<GetGridButtonsEvent>().Publish(args);
        if (args.Buttons.Count > 0)
        {
          SetGridButtons(d, args.Buttons);
          myGrid.Loaded += myGrid_Loaded;
        }
      }
    }

    static void myGrid_Loaded(object sender, RoutedEventArgs e)
    {
      DataGrid myGrid = sender as DataGrid;
      Collection<GridButtonViewModel> buttons = GetGridButtons(myGrid);

      foreach (var b in buttons)
      {
        var tc = new DataGridTemplateColumn();
        tc.Header = b.Text;

        FrameworkElementFactory fButton = new FrameworkElementFactory(typeof(Button));
        FrameworkElementFactory fTextBlock = new FrameworkElementFactory(typeof(TextBlock));
        fTextBlock.SetValue(TextBlock.TextProperty, "...");

        fButton.AddHandler(System.Windows.Controls.Primitives.ButtonBase.ClickEvent, new RoutedEventHandler(b.Stub));
        fButton.AppendChild(fTextBlock);
        DataTemplate dtButton = new DataTemplate();
        dtButton.VisualTree = fButton;

        tc.CellTemplate = dtButton;

        myGrid.Columns.Add(tc);
        myGrid.Loaded -= myGrid_Loaded;
      }
    }

    public static Collection<GridButtonViewModel> GetGridButtons(DependencyObject obj)
    {
      return (Collection<GridButtonViewModel>)obj.GetValue(GridButtonsProperty);
    }

    public static void SetGridButtons(DependencyObject obj, Collection<GridButtonViewModel> value)
    {
      obj.SetValue(GridButtonsProperty, value);
    }

    public static readonly DependencyProperty GridButtonsProperty = DependencyProperty.RegisterAttached("GridButtons", typeof(Collection<GridButtonViewModel>), typeof(GridExtension));


  }
}

﻿using Microsoft.Practices.Prism.Regions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Afx.Prism.Extensions
{
  public static class RegionManager
  {
    public static readonly DependencyProperty RegionManagerProperty = DependencyProperty.RegisterAttached("RegionManager", typeof(IRegionManager), typeof(RegionManager), new PropertyMetadata(OnRegionManagerChanged));
    public static readonly DependencyProperty RegionNameProperty = DependencyProperty.RegisterAttached("RegionName", typeof(string), typeof(RegionManager), new PropertyMetadata(OnRegionNameChanged));

    private static void OnRegionNameChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
      try
      {
        if (e.OldValue != null)
        {
          IRegionManager rm = Microsoft.Practices.Prism.Regions.RegionManager.GetRegionManager(d);
          string rn = Microsoft.Practices.Prism.Regions.RegionManager.GetRegionName(d);
          IRegion region = rm.Regions[rn];

          #region Remove Views from Old Region

          Queue<object> q = new Queue<object>();
          foreach (object o in region.Views)
          {
            q.Enqueue(o);
          }
          while (q.Count > 0)
          {
            region.Remove(q.Dequeue());
          }

          #endregion

          rm.Regions.Remove(rn);
          Microsoft.Practices.Prism.Regions.RegionManager.SetRegionManager(d, null);
        }

        if (e.NewValue != null)
        {
          Microsoft.Practices.Prism.Regions.RegionManager.SetRegionName(d, (string)e.NewValue);
        }
      }
      catch
      {
        throw;
      }
    }

    private static void OnRegionManagerChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
      try
      {
        //if (e.OldValue != null)
        //{
        //  SetRegionName(d, null);
        //  Microsoft.Practices.Prism.Regions.RegionManager.SetRegionManager(d, null);
        //}

        if (e.NewValue != null)
        {
          Microsoft.Practices.Prism.Regions.RegionManager.SetRegionManager(d, (IRegionManager)e.NewValue);
        }
      }
      catch
      {
        throw;
      }
    }

    public static bool GetRegionManager(DependencyObject obj)
    {
      return (bool)obj.GetValue(RegionManagerProperty);
    }

    public static void SetRegionManager(DependencyObject obj, IRegionManager value)
    {
      obj.SetValue(RegionManagerProperty, value);
    }

    public static bool GetRegionName(DependencyObject obj)
    {
      return (bool)obj.GetValue(RegionNameProperty);
    }

    public static void SetRegionName(DependencyObject obj, string value)
    {
      obj.SetValue(RegionNameProperty, value);
    }
  }
}

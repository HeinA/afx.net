﻿using Afx.Business.Security;
using Afx.Business.ServiceModel;
using Afx.Prism.Events;
using Microsoft.Practices.Prism.PubSubEvents;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.InterceptionExtension;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism
{
  public class TransientFaultInterceptionBehavior : IInterceptionBehavior
  {
    [Dependency]
    public IEventAggregator EventAggregator { get; set; }

    public IEnumerable<Type> GetRequiredInterfaces()
    {
      return Type.EmptyTypes;
    }

    public IMethodReturn Invoke(IMethodInvocation input, GetNextInterceptionBehaviorDelegate getNext)
    {
      bool retry = true;
      IMethodReturn msg = null;
      while (retry)
      {
        VirtualMethodInvocation i = input as VirtualMethodInvocation;
        msg = getNext()(input, getNext);
        if (msg.Exception == null) return msg;
        if (msg.Exception is FaultException<AuthorizationFault>)
        {
          RetryEventArgs args = new RetryEventArgs();
          EventAggregator.GetEvent<AuthorizationRequiredEvent>().Publish(args);
          retry = args.Retry;
        }
        else if (msg.Exception is FaultException<AuthenticationFault>)
        {
          RetryEventArgs args = new RetryEventArgs();
          EventAggregator.GetEvent<ReAuthenticateEvent>().Publish(args);
          retry = args.Retry;
        }
        else return msg;
      }

      return msg;
    }

    public bool WillExecute
    {
      get { return true; }
    }
  }
}

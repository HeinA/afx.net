﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Prism
{
  public interface IViewModelCollection
  {
    IEnumerable<ViewModelBase> BaseViewModels { get; }
  }
}

﻿using Afx.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace Afx.Prism
{
  public abstract class SelectableViewModel<TModel> : StatefullViewModel<TModel>, ISelectableViewModel
    where TModel : class, IBusinessObject
  {
    protected SelectableViewModel(IController controller)
      : base(controller)
    {
    }

    protected override void OnModelPropertyChanged(string propertyName)
    {
      switch (propertyName)
      {
        case BusinessObject.ErrorProperty:
          OnPropertyChanged("Foreground");
          OnPropertyChanged("FontWeight");
          break;

        case BusinessObject.IsDeletedProperty:
          OnPropertyChanged("TextDecoration");
          OnPropertyChanged("Foreground");
          OnPropertyChanged("FontWeight");
          break;
      }
      base.OnModelPropertyChanged(propertyName);
    }

    IBusinessObject ISelectableViewModel.Model
    {
      get { return Model; }
    }

    protected override void OnModelChanged()
    {
      base.OnModelChanged();
    }

    #region bool IsSelected

    public virtual bool IsSelected
    {
      get { return GetState<bool>(); }
      set { SetState<bool>(value); }
    }

    #endregion

    #region Brush Foreground

    public SolidColorBrush Foreground
    {
      get
      {
        if (Model == null) return new SolidColorBrush(Colors.Black);
        if (!string.IsNullOrWhiteSpace(Model.Error)) return new SolidColorBrush(Colors.Red);
        return new SolidColorBrush(Colors.Black);
      }
    }

    #endregion

    #region FontWeight FontWeight

    public FontWeight FontWeight
    {
      get
      {
        if (Model == null) return FontWeights.Normal;
        if (!string.IsNullOrWhiteSpace(Model.Error)) return FontWeights.Bold;
        return FontWeights.Normal;
      }
    }

    #endregion

    #region TextDecorationCollection TextDecoration

    public TextDecorationCollection TextDecoration
    {
      get
      {
        if (Model.IsDeleted) return TextDecorations.Strikethrough;
        else return null;
      }
    }

    #endregion

    #region bool MayDelete

    public virtual bool MayDelete
    {
      get { return true; }
    }

    #endregion
  }
}

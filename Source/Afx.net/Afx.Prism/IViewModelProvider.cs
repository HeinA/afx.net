﻿using Afx.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism
{
  public interface IViewModelProvider
  {
    TViewModel CreateViewModel<TViewModel>(IViewModelBase parent)
      where TViewModel : class, IViewModelBase;

    TViewModel GetCreateViewModel<TViewModel>(IBusinessObject model, IViewModelBase parent)
      where TViewModel : class, IViewModel;

    TViewModel GetViewModel<TViewModel>(IBusinessObject model)
      where TViewModel : class, IViewModel;

    TViewModel GetViewModel<TViewModel>()
      where TViewModel : class, IViewModel;
  }
}

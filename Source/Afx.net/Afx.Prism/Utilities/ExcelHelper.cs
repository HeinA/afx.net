﻿using Afx.Business;
using ClosedXML.Excel;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.Utilities
{
  public class ExcelHelper
  {
    public static void Export(string sheetName, IEnumerable<IExcelRow> rows, string exportKey)
    {
      SaveFileDialog sfd = new SaveFileDialog();
      sfd.Filter = "Excel File (.xlsx)|*.xlsx";
      sfd.DefaultExt = "xlsx";
      if (sfd.ShowDialog() == false) return;

      Export(sfd.FileName, sheetName, rows, exportKey);
    }

    public static void Export(string fileName, string sheetName, IEnumerable<IExcelRow> rows, string exportKey)
    {
      try
      {
        using (var wb = new XLWorkbook())
        {
          IXLWorksheet sheet = wb.Worksheets.Add(sheetName);
          int i = 1;
          foreach (var er in rows)
          {
            if (i == 1) er.ExportHeader(sheet, i++, exportKey);
            er.ExportRow(sheet, i++, exportKey);
          }

          File.Delete(fileName);
          wb.SaveAs(fileName);
        }
      }
      catch(Exception ex)
      {
        if (ExceptionHelper.HandleException(ex)) throw ex;
      }

      Process.Start(fileName);
    }

    public static string GetCellReference(string column, int row)
    {
      return string.Format("{0}{1}", column.ToUpperInvariant(), row);
    }
  }
}

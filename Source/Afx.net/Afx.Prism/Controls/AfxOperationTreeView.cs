﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Interactivity;
using System.Windows.Media;

namespace Afx.Prism.Controls
{
  public class AfxOperationTreeView : TreeView
  {
    public AfxOperationTreeView()
    {
      System.Windows.Interactivity.TriggerCollection triggerCollection = Interaction.GetTriggers(this);
      System.Windows.Interactivity.EventTrigger trigger = new System.Windows.Interactivity.EventTrigger("ContextMenuOpening");
      InteractiveCommand commandAction = new InteractiveCommand();
      BindingOperations.SetBinding(commandAction, InteractiveCommand.CommandProperty, new Binding("ContextMenuOpeningCommand"));
      trigger.Actions.Add(commandAction);
      triggerCollection.Add(trigger);
    }

    protected override void OnPreviewMouseRightButtonDown(MouseButtonEventArgs e)
    {
      TreeViewItem treeViewItem = VisualUpwardSearch(e.OriginalSource as DependencyObject);
      if (treeViewItem != null)
      {
        this.Focus();
        treeViewItem.Focus();
        e.Handled = true;
      }

      base.OnPreviewMouseRightButtonDown(e);
    }

    public static TreeViewItem VisualUpwardSearch(DependencyObject source)
    {
      while (source != null && !(source is TreeViewItem))
      {
        source = VisualTreeHelper.GetParent(source);
      }

      return source as TreeViewItem;
    }

  }
}

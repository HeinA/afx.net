﻿using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms.Integration;

namespace Afx.Prism.Controls
{
  public class ReportViewerWrapper : WindowsFormsHost
  {
    ReportViewer mChild = new ReportViewer();

    public ReportViewerWrapper()
    {
      mChild.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this.Child = mChild;
      this.Loaded += ReportViewerWrapper_Loaded;
    }

    void ReportViewerWrapper_Loaded(object sender, RoutedEventArgs e)
    {
      ReportViewer = mChild;
    }

    public static readonly DependencyProperty ReportViewerProperty = DependencyProperty.Register("ReportViewer", typeof(ReportViewer), typeof(ReportViewerWrapper));

    public ReportViewer ReportViewer
    {
      get { return (ReportViewer)GetValue(ReportViewerProperty); }
      set { SetValue(ReportViewerProperty, value); }
    }
  }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace Afx.Prism.RibbonMdi.Controls
{
  public class AfxViewTemplate : DataTemplate
  {
    public DataTemplate EditableTemplate { get; set; }
    public DataTemplate ReadOnlyTemplate { get; set; }
  }
}

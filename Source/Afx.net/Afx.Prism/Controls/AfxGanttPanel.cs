﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Afx.Prism.Controls
{
  public class ExtendedRect
  {
    public ExtendedRect() { DateRect = new Rect(); }

    public ExtendedRect (Point x, Point y, DateTime date)
    {
      DateRect = new Rect(x, y);
      Date = date;
      OwnerGuid = null;
    }

    public Rect DateRect { get; set; }
    public DateTime Date { get; set; }
    public object OwnerGuid { get; set; }
  }

  public class AfxGanttPanel : Panel
  {
    #region Start & End Date Attached Properties

    public static readonly DependencyProperty StartDateProperty =
       DependencyProperty.RegisterAttached("StartDate", typeof(DateTime), typeof(AfxGanttPanel), new FrameworkPropertyMetadata(DateTime.MinValue, FrameworkPropertyMetadataOptions.AffectsParentArrange));
    public static DateTime GetStartDate(DependencyObject obj)
    {
      return (DateTime)obj.GetValue(StartDateProperty);
    }

    public static void SetStartDate(DependencyObject obj, DateTime value)
    {
      obj.SetValue(StartDateProperty, value);
    }

    public static readonly DependencyProperty EndDateProperty =
        DependencyProperty.RegisterAttached("EndDate", typeof(DateTime), typeof(AfxGanttPanel), new FrameworkPropertyMetadata(DateTime.MaxValue, FrameworkPropertyMetadataOptions.AffectsParentArrange));

    public static DateTime GetEndDate(DependencyObject obj)
    {
      return (DateTime)obj.GetValue(EndDateProperty);
    }

    public static void SetEndDate(DependencyObject obj, DateTime value)
    {
      obj.SetValue(EndDateProperty, value);
    }

    #endregion

    #region Min & Max Date Dependency Properties

    public static readonly DependencyProperty MaxDateProperty =
       DependencyProperty.Register("MaxDate", typeof(DateTime), typeof(AfxGanttPanel), new FrameworkPropertyMetadata(DateTime.MaxValue, FrameworkPropertyMetadataOptions.AffectsMeasure | FrameworkPropertyMetadataOptions.AffectsRender));

    public DateTime MaxDate
    {
      get { return (DateTime)GetValue(MaxDateProperty); }
      set { SetValue(MaxDateProperty, value); }
    }

    public static readonly DependencyProperty MinDateProperty =
        DependencyProperty.Register("MinDate", typeof(DateTime), typeof(AfxGanttPanel), new FrameworkPropertyMetadata(DateTime.MaxValue, FrameworkPropertyMetadataOptions.AffectsMeasure | FrameworkPropertyMetadataOptions.AffectsRender));

    public DateTime MinDate
    {
      get { return (DateTime)GetValue(MinDateProperty); }
      set { SetValue(MinDateProperty, value); }
    }

    #endregion


    private static readonly DependencyProperty MouseRectProperty =
        DependencyProperty.Register("MouseRect", typeof(Rect), typeof(AfxGanttPanel), new FrameworkPropertyMetadata(new Rect(new Point(0, 0), new Point(0, 0)), FrameworkPropertyMetadataOptions.AffectsMeasure | FrameworkPropertyMetadataOptions.AffectsRender));

    private Rect MouseRect
    {
      get { return (Rect)GetValue(MouseRectProperty); }
      set { SetValue(MouseRectProperty, value); }
    }


    public static readonly DependencyProperty ActiveDateProperty =
       DependencyProperty.Register("ActiveDate", typeof(DateTime?), typeof(AfxGanttPanel), new FrameworkPropertyMetadata(DateTime.MaxValue, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

    public static readonly DependencyProperty ActiveOwnerGuidProperty =
       DependencyProperty.Register("ActiveOwnerGuid", typeof(string), typeof(AfxGanttPanel), new FrameworkPropertyMetadata(String.Empty, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

    public DateTime? ActiveDate
    {
      get { return (DateTime?)GetValue(ActiveDateProperty); }
      set { SetValue(ActiveDateProperty, value); }
    }

    public string ActiveOwnerGuid
    {
      get { return (string)GetValue(ActiveOwnerGuidProperty); }
      set { SetValue(ActiveOwnerGuidProperty, value); }
    }


    #region DateLines Dependency Properties

    public static readonly DependencyProperty DateLineThicknessProperty =
       DependencyProperty.Register("DateLineThickness", typeof(double), typeof(AfxGanttPanel), new FrameworkPropertyMetadata(1d, FrameworkPropertyMetadataOptions.AffectsMeasure | FrameworkPropertyMetadataOptions.AffectsRender));

    public double DateLineThickness
    {
      get { return (double)GetValue(DateLineThicknessProperty); }
      set { SetValue(DateLineThicknessProperty, value); }
    }

    public static readonly DependencyProperty DateLineBrushProperty =
       DependencyProperty.Register("DateLineBrush", typeof(Brush), typeof(AfxGanttPanel), new FrameworkPropertyMetadata(Brushes.LightGray, FrameworkPropertyMetadataOptions.AffectsMeasure | FrameworkPropertyMetadataOptions.AffectsRender));

    public Brush DateLineBrush
    {
      get { return (Brush)GetValue(DateLineBrushProperty); }
      set { SetValue(DateLineBrushProperty, value); }
    }

    #endregion

    #region DateDisplay Dependency Properties

    public static readonly DependencyProperty FontSizeProperty =
       DependencyProperty.Register("FontSize", typeof(double), typeof(AfxGanttPanel), new FrameworkPropertyMetadata(0d, FrameworkPropertyMetadataOptions.AffectsMeasure | FrameworkPropertyMetadataOptions.AffectsRender));

    public double FontSize
    {
      get { return (double)GetValue(FontSizeProperty); }
      set { SetValue(FontSizeProperty, value); }
    }

    public static readonly DependencyProperty FontBrushProperty =
       DependencyProperty.Register("FontBrush", typeof(Brush), typeof(AfxGanttPanel), new FrameworkPropertyMetadata(Brushes.LightGray, FrameworkPropertyMetadataOptions.AffectsMeasure | FrameworkPropertyMetadataOptions.AffectsRender));

    public Brush FontBrush
    {
      get { return (Brush)GetValue(FontBrushProperty); }
      set { SetValue(FontBrushProperty, value); }
    }

    #endregion


    public delegate void MouseOverHandler(object sender, EventArgs e);
    public event EventHandler MouseOver;

    public void AfxGanttPanel_MouseOver(object sender, RoutedEventArgs e)
    {
      //execute daddy's button click
      (((sender as AfxGanttPanel).Parent as ItemsControl).Parent as AfxGanttPanel).MouseOver(sender, e);

      e.Handled = false;

    }

    /// <summary>
    /// User for mouse tracking.
    /// </summary>
    #region List<ExtendedRect> AllRects

    public const string AllRectsProperty = "AllRects";
    List<ExtendedRect> mAllRects = new List<ExtendedRect>();
    public List<ExtendedRect> AllRects
    {
      get { return mAllRects; }
      set { mAllRects = value; }
    }

    #endregion

    protected override Size MeasureOverride(Size availableSize)
    {
      double height = 0;
      foreach (UIElement child in Children)
      {
        child.Measure(availableSize);
        if (child.DesiredSize.Height > height) height = child.DesiredSize.Height;
      }

      return new Size(0, height);
    }

    /// <summary>
    /// Clear mouse over rectangle and date.
    /// </summary>
    /// <param name="e"></param>
    protected override void OnMouseLeave(MouseEventArgs e)
    {
      //ActiveDate = null;
      MouseRect = new Rect(0, 0, 0, 0);
      base.OnMouseLeave(e);
    }

    /// <summary>
    /// Track mouse movement and set selected date and rectangle.
    /// </summary>
    /// <param name="e"></param>
    protected override void OnMouseMove(MouseEventArgs e)
    {
      if (pauseDateTracking) return;
      Point mouseXY = e.MouseDevice.GetPosition(e.MouseDevice.DirectlyOver);
      var mouseOverObject = e.Source;

      foreach (ExtendedRect r in AllRects)
      {
        if (r.DateRect != null && mouseOverObject.GetType() == typeof(Afx.Prism.Controls.AfxGanttPanel))
        {
          if (r.DateRect.IntersectsWith(new Rect(mouseXY.X, mouseXY.Y, 1, 1)))
          {
            MouseRect = r.DateRect;
            ActiveDate = r.Date;
          }
          else
          {
            ///MouseRect = new Rect(0,0,0,0);
          }
        }
        else
        {
          MouseRect = new Rect(0, 0, 0, 0);
          ActiveDate = null;
        }
      }
      
      base.OnMouseMove(e);
    }

    private bool pauseDateTracking = false;

    protected override void OnMouseUp(MouseButtonEventArgs e)
    {
      if (e.LeftButton == MouseButtonState.Pressed)
      {
        pauseDateTracking = false;
      }
      else if (e.RightButton == MouseButtonState.Released)
      {
        pauseDateTracking = true;
      }
      else if (e.RightButton == MouseButtonState.Pressed)
      {
        pauseDateTracking = false;
      }
      else if (e.RightButton == MouseButtonState.Released)
      {
        pauseDateTracking = true;
      }
      base.OnMouseUp(e);
    }

    /// <summary>
    /// Control rendering and drawing of grid and or dates.
    /// </summary>
    /// <param name="dc"></param>
    protected override void OnRender(DrawingContext dc)
    {
      if (DateLineThickness > 0 || FontSize > 0)
      {
        double range = (MaxDate - MinDate).Ticks;
        double pixelsPerTick = this.ActualWidth / range;

        DateTime date = MinDate.Date;
        while (date < MaxDate.Date)
        {
          double offset = (date - MinDate).Ticks * pixelsPerTick;
          double offset2 = (date.AddDays(1) - MinDate).Ticks * pixelsPerTick;
          
          if (DateLineThickness > 0)
          {
            AllRects.Add(new ExtendedRect(new Point(offset, 0), new Point(offset2, this.ActualHeight), date));

            Rect baseRect = new Rect(new Point(offset, 0), new Point(offset2, this.ActualHeight));
            SolidColorBrush brush = new SolidColorBrush(Colors.Transparent);
            if (date.DayOfWeek  == DayOfWeek.Saturday || date.DayOfWeek == DayOfWeek.Sunday)
            {
              brush = new SolidColorBrush(Color.FromArgb(130, 240, 240, 240));
            } 
            else 
            {
              if (date.AddTicks(-date.TimeOfDay.Ticks) == DateTime.Today)
              {
                brush = new SolidColorBrush(Color.FromArgb(50, 255, 255, 0));
              }
            }
            dc.DrawRectangle(brush, new Pen(), baseRect);
            dc.DrawLine(new Pen(DateLineBrush, DateLineThickness), new Point(offset, 0), new Point(offset, this.ActualHeight));
          }

          if (MouseRect != null)
          {
            SolidColorBrush brush = new SolidColorBrush(Colors.BurlyWood);
            dc.DrawRectangle(brush, new Pen(), MouseRect);
          }

          if (FontSize > 0)
          {
            FormattedText ft = new FormattedText(string.Format("{0:d MMM\r\nddd}", date), CultureInfo.CurrentCulture, FlowDirection.LeftToRight, new Typeface("Segoe UI"), FontSize, FontBrush);
            ft.TextAlignment = TextAlignment.Center;
            dc.DrawText(ft, new Point(offset + ((offset2 - offset)) / 2, (this.ActualHeight - ft.Height) / 2));
          }

          date = date.AddDays(1);
        }
      }

      if (DateLineThickness > 0)
      {
        dc.DrawLine(new Pen(DateLineBrush, DateLineThickness), new Point(this.ActualWidth - 1, 0), new Point(this.ActualWidth - 1, this.ActualHeight));
      }

      base.OnRender(dc);
    }

    

    protected override Size ArrangeOverride(Size finalSize)
    {
      double range = (MaxDate - MinDate).Ticks;
      double pixelsPerTick = finalSize.Width / range;

      foreach (UIElement child in Children)
      {
        double height = finalSize.Height;
        if (child.DesiredSize.Height > height) height = child.DesiredSize.Height;
        ArrangeChild(child, MinDate, pixelsPerTick, height);
      }

      return finalSize;
    }

    /// <summary>
    /// Arrange children in container.
    /// </summary>
    /// <param name="child"></param>
    /// <param name="minDate"></param>
    /// <param name="pixelsPerTick"></param>
    /// <param name="elementHeight"></param>
    private void ArrangeChild(UIElement child, DateTime minDate, double pixelsPerTick, double elementHeight)
    {
      DateTime childStartDate = GetStartDate(child);
      DateTime childEndDate = GetEndDate(child);
      if (childEndDate > MaxDate) childEndDate = MaxDate;
      if (childEndDate.Hour == 0 && childEndDate.Minute == 0 && childEndDate.Second == 0) childEndDate = childEndDate.AddDays(1).AddSeconds(-1);
      TimeSpan childDuration = childEndDate - childStartDate;

      double offset = (childStartDate - minDate).Ticks * pixelsPerTick;
      double width = childDuration.Ticks * pixelsPerTick;

      if (offset < 0)
      {
        width += offset;
        offset = 0;
      }

      if (width > 0)
      {
        child.Visibility = System.Windows.Visibility.Visible;
        child.Arrange(new Rect(offset, 0, width, elementHeight));
        ContentPresenter cp = (ContentPresenter)child;
        cp.MaxWidth = (int)width;
      }
      else
      {
        child.Visibility = System.Windows.Visibility.Collapsed;
      }
    }
  }
}

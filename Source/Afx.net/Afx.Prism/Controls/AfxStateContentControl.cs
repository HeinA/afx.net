﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace Afx.Prism.Controls
{
  public class AfxStateContentControl : ContentControl
  {
    public AfxStateContentControl()
    {
      this.DataContextChanged += AfxStateContentControl_DataContextChanged;
      Focusable = false;
    }

    void AfxStateContentControl_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
    {
      IViewModel vm = this.DataContext as IViewModel;
      if (vm != null)
      {
        WeakEventManager<INotifyPropertyChanged, PropertyChangedEventArgs>.AddHandler(vm, "PropertyChanged", OnDataContextPropertyChanged);
        UpdateState();
      }
    }

    private void UpdateState()
    {
      IViewModel vm = this.DataContext as IViewModel;
      if (vm != null)
      {
        FrameworkElement fe = null;
        if (vm.IsReadOnly && ReadOnlyContentTemplate != null) fe = (FrameworkElement)ReadOnlyContentTemplate.LoadContent();
        else if (EditableContentTemplate != null) fe = (FrameworkElement)EditableContentTemplate.LoadContent();

        if (fe != null)
        {
          fe.DataContext = this.DataContext;
        }
        this.Content = fe;
      }
    }

    private void OnDataContextPropertyChanged(object sender, PropertyChangedEventArgs e)
    {
      if (e.PropertyName == "IsReadOnly")
      {
        UpdateState();
      }
    }

    public static readonly DependencyProperty EditableContentTemplateProperty = DependencyProperty.Register("EditableContentTemplate", typeof(DataTemplate), typeof(AfxStateContentControl), new PropertyMetadata(OnEditableContentTemplateChanged));

    private static void OnEditableContentTemplateChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
      AfxStateContentControl scc = (AfxStateContentControl)d;
      scc.UpdateState();
    }

    public DataTemplate EditableContentTemplate
    {
      get { return (DataTemplate)this.GetValue(EditableContentTemplateProperty); }
      set { this.SetValue(EditableContentTemplateProperty, value); }
    }

    public static readonly DependencyProperty ReadOnlyContentTemplateProperty = DependencyProperty.Register("ReadOnlyContentTemplate", typeof(DataTemplate), typeof(AfxStateContentControl), new PropertyMetadata(OnReadOnlyContentTemplateChanged));

    private static void OnReadOnlyContentTemplateChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
      AfxStateContentControl scc = (AfxStateContentControl)d;
      scc.UpdateState();
    }

    public DataTemplate ReadOnlyContentTemplate
    {
      get { return (DataTemplate)this.GetValue(ReadOnlyContentTemplateProperty); }
      set { this.SetValue(ReadOnlyContentTemplateProperty, value); }
    }

  }
}

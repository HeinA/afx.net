﻿using Afx.Business;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace Afx.Prism.Controls
{
  public class AfxVerticalGridSplitter : GridSplitter
  {
    const string RegistryKey = @"GridSplitters\{0}\";

    public AfxVerticalGridSplitter()
    {
      if (!DesignerProperties.GetIsInDesignMode(this))
      {
        this.Unloaded += (sender, e) =>
        {
          Grid gv = (Grid)this.Parent;
          foreach (var c in gv.ColumnDefinitions)
          {
            DependencyPropertyDescriptor.FromProperty(ColumnDefinition.WidthProperty, typeof(ColumnDefinition)).RemoveValueChanged(c, OnColumnWidthChanged);
          }
        };

        this.Loaded += (sender, e) =>
        {
          if (!string.IsNullOrWhiteSpace(Identifier))
          {
            LoadWidth();
          }
        };
      }
    }

    void LoadWidth()
    {
      try
      {
        Grid gv = (Grid)this.Parent;
        foreach (var c in gv.ColumnDefinitions)
        {
          int header = gv.ColumnDefinitions.IndexOf(c);
          int width = 0;
          if (RegistryHelper.GetRegistryValue<int>(Microsoft.Win32.RegistryHive.CurrentUser, string.Format(RegistryHelper.ApplicationKey, string.Format(RegistryKey, Identifier)), header.ToString(), Microsoft.Win32.RegistryValueKind.DWord, out width))
          {
            c.Width = new GridLength(width);
          }

          DependencyPropertyDescriptor.FromProperty(ColumnDefinition.WidthProperty, typeof(ColumnDefinition)).AddValueChanged(c, OnColumnWidthChanged);
        }
      }
      catch { }
    }

    private void OnColumnWidthChanged(object sender, EventArgs e)
    {
      ColumnDefinition c = sender as ColumnDefinition;
      Grid gv = (Grid)this.Parent;
      int header = gv.ColumnDefinitions.IndexOf(c);
      if (!c.Width.IsAuto && !c.Width.IsStar)
      {
        int width = (int)c.Width.Value;
        RegistryHelper.SetRegistryValue<int>(Microsoft.Win32.RegistryHive.CurrentUser, string.Format(RegistryHelper.ApplicationKey, string.Format(RegistryKey, Identifier)), header.ToString(), Microsoft.Win32.RegistryValueKind.DWord, width);
      }
    }

    //void SaveWidth()
    //{
    //  try
    //  {
    //    Grid gv = (Grid)this.Parent;
    //    foreach (var c in gv.ColumnDefinitions)
    //    {
    //      if (!string.IsNullOrWhiteSpace(Identifier))
    //      {
    //        int header = gv.ColumnDefinitions.IndexOf(c);
    //        if (!c.Width.IsAuto && !c.Width.IsStar)
    //        {
    //          int width = (int)c.Width.Value;
    //          RegistryHelper.SetRegistryValue<int>(Microsoft.Win32.RegistryHive.CurrentUser, string.Format(RegistryHelper.ApplicationKey, string.Format(RegistryKey, Identifier)), header.ToString(), Microsoft.Win32.RegistryValueKind.DWord, width);
    //        }
    //      }
    //    }
    //  }
    //  catch { }
    //}

    public string Identifier
    {
      get { return (string)GetValue(IdentifierProperty); }
      set { SetValue(IdentifierProperty, value); }
    }
    //string mIdentifier;
    //public string Identifier
    //{
    //  get { return mIdentifier; }
    //  set
    //  {
    //    if (mIdentifier == value) return;
    //    mIdentifier = value;
    //  }
    //}

    public static readonly DependencyProperty IdentifierProperty = DependencyProperty.Register("Identifier", typeof(string), typeof(AfxVerticalGridSplitter));
    //public static readonly DependencyProperty IdentifierProperty = DependencyProperty.Register("Identifier", typeof(string), typeof(AfxVerticalGridSplitter), new PropertyMetadata(string.Empty));

    //private static void OnIdentifierChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
    //{
    //  AfxVerticalGridSplitter vgs = d as AfxVerticalGridSplitter;
    //  vgs.Identifier = (string)e.NewValue;
    //}
  }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace Afx.Prism.Controls
{
  [TemplatePart(Name = "PART_TopHeaderScrollViewer", Type = typeof(ScrollViewer))]
  [TemplatePart(Name = "PART_LeftHeaderScrollViewer", Type = typeof(ScrollViewer))]
  public class AfxHeaderedScrollViewer : ScrollViewer
  {
    static AfxHeaderedScrollViewer()
    {
      DefaultStyleKeyProperty.OverrideMetadata(typeof(AfxHeaderedScrollViewer), new FrameworkPropertyMetadata(typeof(AfxHeaderedScrollViewer)));
    }
    public object TopHeader
    {
      get { return (object)GetValue(TopHeaderProperty); }
      set { SetValue(TopHeaderProperty, value); }
    }
    public static readonly DependencyProperty TopHeaderProperty =
        DependencyProperty.Register("TopHeader", typeof(object), typeof(AfxHeaderedScrollViewer), new UIPropertyMetadata(TopHeader_PropertyChanged));
    private static void TopHeader_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
      AfxHeaderedScrollViewer instance = (AfxHeaderedScrollViewer)d;
      if (e.OldValue != null)
        instance.RemoveLogicalChild(e.OldValue);
      if (e.NewValue != null)
        instance.AddLogicalChild(e.NewValue);
    }

    public DataTemplate TopHeaderTemplate
    {
      get { return (DataTemplate)GetValue(TopHeaderTemplateProperty); }
      set { SetValue(TopHeaderTemplateProperty, value); }
    }
    public static readonly DependencyProperty TopHeaderTemplateProperty =
        DependencyProperty.Register("TopHeaderTemplate", typeof(DataTemplate), typeof(AfxHeaderedScrollViewer), new UIPropertyMetadata());

    public object LeftHeader
    {
      get { return (object)GetValue(LeftHeaderProperty); }
      set { SetValue(LeftHeaderProperty, value); }
    }
    public static readonly DependencyProperty LeftHeaderProperty =
        DependencyProperty.Register("LeftHeader", typeof(object), typeof(AfxHeaderedScrollViewer), new UIPropertyMetadata(LeftHeader_PropertyChanged));
    private static void LeftHeader_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
      AfxHeaderedScrollViewer instance = (AfxHeaderedScrollViewer)d;
      if (e.OldValue != null)
        instance.RemoveLogicalChild(e.OldValue);
      if (e.NewValue != null)
        instance.AddLogicalChild(e.NewValue);
    }

    public DataTemplate LeftHeaderTemplate
    {
      get { return (DataTemplate)GetValue(LeftHeaderTemplateProperty); }
      set { SetValue(LeftHeaderTemplateProperty, value); }
    }
    public static readonly DependencyProperty LeftHeaderTemplateProperty =
        DependencyProperty.Register("LeftHeaderTemplate", typeof(DataTemplate), typeof(AfxHeaderedScrollViewer), new UIPropertyMetadata());

    protected override System.Collections.IEnumerator LogicalChildren
    {
      get
      {
        if (TopHeader != null || LeftHeader != null)
        {

          System.Collections.ArrayList children = new System.Collections.ArrayList();
          System.Collections.IEnumerator baseEnumerator = base.LogicalChildren;
          while (baseEnumerator.MoveNext())
          {
            children.Add(baseEnumerator.Current);
          }
          if (TopHeader != null)
            children.Add(TopHeader);
          if (LeftHeader != null)
            children.Add(LeftHeader);
          return children.GetEnumerator();
        }
        return base.LogicalChildren;
      }
    }

    private ScrollViewer topHeaderScrollViewer;

    private ScrollViewer leftHeaderScrollViewer;

    public override void OnApplyTemplate()
    {
      base.OnApplyTemplate();
      topHeaderScrollViewer = base.GetTemplateChild("PART_TopHeaderScrollViewer") as ScrollViewer;
      if (topHeaderScrollViewer != null)
        topHeaderScrollViewer.ScrollChanged += topHeaderScrollViewer_ScrollChanged;
      leftHeaderScrollViewer = base.GetTemplateChild("PART_LeftHeaderScrollViewer") as ScrollViewer;
      if (leftHeaderScrollViewer != null)
        leftHeaderScrollViewer.ScrollChanged += leftHeaderScrollViewer_ScrollChanged;
    }

    private void leftHeaderScrollViewer_ScrollChanged(object sender, ScrollChangedEventArgs e)
    {
      if (e.OriginalSource == leftHeaderScrollViewer)
        ScrollToVerticalOffset(e.VerticalOffset);
    }

    private void topHeaderScrollViewer_ScrollChanged(object sender, ScrollChangedEventArgs e)
    {
      if (e.OriginalSource == topHeaderScrollViewer)
        ScrollToHorizontalOffset(e.HorizontalOffset);
    }

    protected override void OnScrollChanged(ScrollChangedEventArgs e)
    {
      base.OnScrollChanged(e);
      if (topHeaderScrollViewer != null)
        topHeaderScrollViewer.ScrollToHorizontalOffset(e.HorizontalOffset);
      if (leftHeaderScrollViewer != null)
        leftHeaderScrollViewer.ScrollToVerticalOffset(e.VerticalOffset);
    }
  }
}

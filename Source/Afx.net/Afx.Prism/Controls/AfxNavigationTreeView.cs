﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Afx.Prism.Controls
{
  public class AfxNavigationTreeView : AfxOperationTreeView
  {
    protected override void OnSelectedItemChanged(RoutedPropertyChangedEventArgs<object> e)
    {
      if (SelectedItem == null)
      {
        ISelectableViewModel svm = null;
        if (Items.Count > 0) svm = Items[0] as ISelectableViewModel;
        if (svm != null)
        {
          this.Focus();
          svm.IsFocused = true;
        }
      }

      if (SelectedItem != null)
      {
        ISelectableViewModel svm = SelectedItem as ISelectableViewModel;
        if (svm != null)
        {
          Dispatcher.BeginInvoke(new Action(() =>
          {
            try
            {
              if (svm.IsSelected) svm.IsFocused = true;
            }
            catch
            {
#if DEBUG
              if (System.Diagnostics.Debugger.IsAttached) System.Diagnostics.Debugger.Break();
#endif
            }
          }), System.Windows.Threading.DispatcherPriority.Input);
        }
      }

      base.OnSelectedItemChanged(e);
    }
  }
}

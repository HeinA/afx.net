﻿using Afx.Business;
using Afx.Business.Tabular;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace Afx.Prism.Controls
{
  public class AfxTabularDataGrid : AfxDataGrid
  {
    public AfxTabularDataGrid()
    {
      AutoGenerateColumns = false;
      CanUserAddRows = false;
      CanUserDeleteRows = false;
      CanUserSortColumns = false;
    }

    public static readonly DependencyProperty HeaderDisplayMemberPathProperty = DependencyProperty.Register("HeaderDisplayMemberPath", typeof(string), typeof(AfxTabularDataGrid), new PropertyMetadata(string.Empty, OnHeaderDisplayMemberPathPropertyChanged));
    public static readonly DependencyProperty CellDataMemberPathProperty = DependencyProperty.Register("CellDataMemberPath", typeof(string), typeof(AfxTabularDataGrid), new PropertyMetadata(string.Empty, OnCellDataMemberPathPropertyChanged));
    public static readonly DependencyProperty CellFormatStringProperty = DependencyProperty.Register("CellFormatString", typeof(string), typeof(AfxTabularDataGrid), new PropertyMetadata(string.Empty, OnCellFormatStringPropertyChanged));

    public static readonly DependencyProperty ItemProperty = DependencyProperty.Register("Item", typeof(ITabularBusinessObject), typeof(AfxTabularDataGrid), new PropertyMetadata(null, OnItemPropertyChanged));

    private static void OnHeaderDisplayMemberPathPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
    }

    private static void OnCellDataMemberPathPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
    }

    private static void OnCellFormatStringPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
    }

    static Dictionary<ITabularBusinessObject, AfxTabularDataGrid> mGridReferences = new Dictionary<ITabularBusinessObject, AfxTabularDataGrid>();

    private static void OnItemPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
      AfxTabularDataGrid dg = (AfxTabularDataGrid)d;

      if (e.OldValue != null)
      {
        dg.Item.LayoutChanged -= OnLayoutChanged;
        mGridReferences.Remove((ITabularBusinessObject)e.OldValue);
      }

      if (e.NewValue != null)
      {
        dg.Item.LayoutChanged -= OnLayoutChanged;
        //WeakEventManager<ITabularBusinessObject, EventArgs>.AddHandler(dg.Item, "LayoutChanged", OnLayoutChanged);
        //mGridReferences.Add((ITabularBusinessObject)e.NewValue, dg);
        ITabularBusinessObject tbo = (ITabularBusinessObject)e.NewValue;
        if (!mGridReferences.ContainsKey(tbo)) mGridReferences.Add(tbo, dg);
        else mGridReferences[tbo] = dg;
        dg.BuildGrid();
        dg.Item.LayoutChanged += OnLayoutChanged;
      }
    }

    void BuildGrid()
    {
      Columns.Clear();
      ItemsSource = null;

      int i = 0;
      foreach (ITableColumn col in Item.Columns) //
      {
        BusinessObject bo = col as BusinessObject;
        if (!bo.IsDeleted)
        {
          DataGridTextColumn dgc = new DataGridTextColumn();
          dgc.Header = col.GetType().GetProperty(HeaderDisplayMemberPath).GetValue(col);
          string bindingString = string.Format("[{0}].{1}", i++, CellDataMemberPath);
          dgc.Binding = new Binding(bindingString);
          if (!string.IsNullOrWhiteSpace(CellFormatString)) dgc.Binding.StringFormat = CellFormatString;
          Columns.Add(dgc);
        }
        else
        {
          i++;
        }
      }

      //foreach (ITableRow row in Item.Rows.OfType<BusinessObject>().Where(c => !c.IsDeleted).Cast<ITableRow>())
      //{
      //  Items.Add(row);
      //}


      ICollectionView cv = CollectionViewSource.GetDefaultView(Item.Rows);
      cv.Filter = RowFilter;
      ItemsSource = cv; 

      //ItemsSource = Item.Rows;
      //EditableCollectionView cv = new CollectionView(Item.Rows);
      //cv.Filter = RowFilter;
      //ItemsSource = cv; // new ObservableCollection<ITableRow>(Item.Rows.OfType<BusinessObject>().Where(c => !c.IsDeleted).Cast<ITableRow>());
    }

    private bool RowFilter(object item)
    {
      BusinessObject bo = item as BusinessObject;
      return !bo.IsDeleted;
    }

    private static void OnLayoutChanged(object sender, EventArgs e)
    {
      AfxTabularDataGrid dg = mGridReferences[(ITabularBusinessObject)sender];
      dg.BuildGrid();
    }

    public ITabularBusinessObject Item
    {
      get { return (ITabularBusinessObject)GetValue(ItemProperty); }
      set { SetValue(ItemProperty, value); }
    }

    public string HeaderDisplayMemberPath
    {
      get { return (string)GetValue(HeaderDisplayMemberPathProperty); }
      set { SetValue(HeaderDisplayMemberPathProperty, value); }
    }

    public string CellDataMemberPath
    {
      get { return (string)GetValue(CellDataMemberPathProperty); }
      set { SetValue(CellDataMemberPathProperty, value); }
    }

    public string CellFormatString
    {
      get { return (string)GetValue(CellFormatStringProperty); }
      set { SetValue(CellFormatStringProperty, value); }
    }
  }
}

﻿namespace Afx.Prism.Controls.Charts
{
  using System;
  using System.Collections.Generic;
  using System.Collections.Specialized;
  using System.Linq;
  using System.Text;
  using System.Windows;

#if NETFX_CORE
    using Windows.UI.Xaml.Controls;
    using Windows.UI.Xaml.Media;
    using Windows.UI.Xaml.Shapes;
    using Windows.UI.Xaml.Markup;
    using Windows.UI.Xaml;
    using Windows.Foundation;
    using Windows.UI;
    using Windows.UI.Xaml.Media.Animation;
    using Windows.UI.Core;
#else
  using System.Windows.Media;
  using System.Windows.Controls;
  using System.Windows.Data;
#endif


  public class NumberChart : ChartBase
  {
    #region Constructors

    /// <summary>
    /// Initializes the <see cref="PieChart"/> class.
    /// </summary>
    static NumberChart()
    {
#if NETFX_CORE
                        
#elif SILVERLIGHT

#else
      DefaultStyleKeyProperty.OverrideMetadata(typeof(NumberChart), new FrameworkPropertyMetadata(typeof(NumberChart)));
#endif
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="NumberChart"/> class.
    /// </summary>
    public NumberChart()
    {
#if NETFX_CORE
            this.DefaultStyleKey = typeof(NumberChart);
#endif
#if SILVERLIGHT
            this.DefaultStyleKey = typeof(NumberChart);
#endif
    }

    #endregion Constructors

    protected override double GridLinesMaxValue
    {
      get
      {
        return 0.0;
      }
    }
  }
}

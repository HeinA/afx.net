﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Afx.Prism.Controls
{
  public class ItemActivatedEventArgs : RoutedEventArgs
  {
    public ItemActivatedEventArgs()
    {
    }

    public ItemActivatedEventArgs(RoutedEvent routedEvent)
      : base(routedEvent)
    {
    }

    public ItemActivatedEventArgs(RoutedEvent routedEvent, Object source)
      : base(routedEvent, source)
    {
    }

  }
}

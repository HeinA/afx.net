﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Interactivity;
using System.Windows.Media;

namespace Afx.Prism.Controls
{
  public class AfxOperationListView : AfxListView
  {
    public AfxOperationListView()
    {
      System.Windows.Interactivity.TriggerCollection triggerCollection = Interaction.GetTriggers(this);
      System.Windows.Interactivity.EventTrigger trigger = new System.Windows.Interactivity.EventTrigger("ContextMenuOpening");
      InteractiveCommand commandAction = new InteractiveCommand();
      BindingOperations.SetBinding(commandAction, InteractiveCommand.CommandProperty, new Binding("ContextMenuOpeningCommand"));
      trigger.Actions.Add(commandAction);
      triggerCollection.Add(trigger);
    }

  }
}

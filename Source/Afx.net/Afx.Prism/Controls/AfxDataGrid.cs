﻿using Afx.Business;
using Afx.Business.Collections;
using Afx.Prism.Extensions;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Markup;
using System.Xml;

namespace Afx.Prism.Controls
{
  public class AfxDataGrid : DataGrid
  {
    const string RegistryKey = @"GridViews\{0}";

    public AfxDataGrid()
    {
      this.BeginningEdit += AfxDataGrid_BeginningEdit;

      if (!DesignerProperties.GetIsInDesignMode(this))
      {
        this.Unloaded += (s, e) =>
        {
          foreach (var c in this.Columns)
          {
            DependencyPropertyDescriptor.FromProperty(DataGridColumn.WidthProperty, typeof(DataGridColumn)).RemoveValueChanged(c, OnColumnWidthChanged);
          }
        };

        this.Loaded += (sender, e) =>
        {
          if (!string.IsNullOrWhiteSpace(Identifier))
          {
            LoadColumnWidths();
          }
        };
      }
    }

    void AfxDataGrid_BeginningEdit(object sender, DataGridBeginningEditEventArgs e)
    {
      var isReadOnlyRow = ReadOnlyExtension.GetIsReadOnly(e.Row);
      if (isReadOnlyRow)
        e.Cancel = true;
    }

    public string Identifier
    {
      get { return (string)GetValue(IdentifierProperty); }
      set { SetValue(IdentifierProperty, value); }
    }

    public static readonly DependencyProperty IdentifierProperty = DependencyProperty.Register("Identifier", typeof(string), typeof(AfxDataGrid));

    void LoadColumnWidths()
    {
      foreach (var c in this.Columns)
      {
        string header = (string)c.Header;
        uint width = 0;
        if (RegistryHelper.GetRegistryValue<uint>(Microsoft.Win32.RegistryHive.CurrentUser, string.Format(RegistryHelper.ApplicationKey, string.Format(RegistryKey, Identifier)), header, Microsoft.Win32.RegistryValueKind.DWord, out width))
        {
          if (width == 0) c.Width = double.NaN;
          else c.Width = width;
        }

        DependencyPropertyDescriptor.FromProperty(DataGridColumn.WidthProperty, typeof(DataGridColumn)).AddValueChanged(c, OnColumnWidthChanged);
      }
    }

    private void OnColumnWidthChanged(object sender, EventArgs e)
    {
      DataGridColumn gvc = sender as DataGridColumn;
      string header = (string)gvc.Header;
      RegistryHelper.SetRegistryValue<uint>(Microsoft.Win32.RegistryHive.CurrentUser, string.Format(RegistryHelper.ApplicationKey, string.Format(RegistryKey, Identifier)), header, Microsoft.Win32.RegistryValueKind.DWord, (uint)gvc.Width.Value);
    }

    public static readonly RoutedEvent ItemDeletingEvent = EventManager.RegisterRoutedEvent("ItemDeleting", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(AfxDataGrid));

    public event RoutedEventHandler ItemDeleting
    {
      add { AddHandler(ItemDeletingEvent, value); }
      remove { RemoveHandler(ItemDeletingEvent, value); }
    }

    bool RaiseItemDeletingEvent()
    {
      ItemDeletingEventArgs args = new ItemDeletingEventArgs(this.SelectedItem, AfxDataGrid.ItemDeletingEvent, this);
      RaiseEvent(args);
      return args.Cancel;
    }

    protected override void OnPreviewKeyDown(System.Windows.Input.KeyEventArgs e)
    {
      if (this.SelectedIndex >= 0)
      {
        DataGridRow dgr = (DataGridRow)(this.ItemContainerGenerator.ContainerFromIndex(this.SelectedIndex));
        if (e.Key == Key.Delete && dgr != null && !dgr.IsEditing && this.CanUserDeleteRows)
        {
          if (IsReadOnly) return;
          if (RaiseItemDeletingEvent())
          {
            e.Handled = true;
            return;
          }

          BusinessObject bo = this.SelectedItem as BusinessObject;
          if (bo != null)
          {
            IAssociativeObjectCollection col = this.ItemsSource as IAssociativeObjectCollection;
            if (col == null)
            {
              bo.IsDeleted = !bo.IsDeleted;
              e.Handled = true;
            }
          }
        }
      }
      base.OnPreviewKeyDown(e);
    }

  }
}

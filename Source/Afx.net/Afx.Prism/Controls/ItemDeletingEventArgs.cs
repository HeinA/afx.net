﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Afx.Prism.Controls
{
  public class ItemDeletingEventArgs : RoutedEventArgs
  {
    public ItemDeletingEventArgs(object item, RoutedEvent routedEvent, object source)
      : base(routedEvent, source)
    {
      Item = item;
    }

    #region object Item

    object mItem;
    public object Item
    {
      get { return mItem; }
      private set { mItem = value; }
    }

    #endregion

    #region bool Cancel

    bool mCancel = false;
    public bool Cancel
    {
      get { return mCancel; }
      set { mCancel = value; }
    }

    #endregion
  }
}

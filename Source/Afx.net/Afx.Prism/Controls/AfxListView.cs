﻿using Afx.Business;
using Afx.Business.Collections;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Threading;
using System.Xml;

namespace Afx.Prism.Controls
{
  public class AfxListView : ListView, ICommandSource
  {
    const string RegistryKey = @"ListViews\{0}";

    public AfxListView()
    {
      if (!DesignerProperties.GetIsInDesignMode(this))
      {
        this.Loaded += (sender, e) =>
        {
          if (!string.IsNullOrWhiteSpace(Identifier))
          {
            LoadColumnWidths();
          }
        };
      }
    }

    public static readonly RoutedEvent ItemDeletingEvent = EventManager.RegisterRoutedEvent("ItemDeleting", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(AfxListView));

    public event RoutedEventHandler ItemDeleting
    {
      add { AddHandler(ItemDeletingEvent, value); }
      remove { RemoveHandler(ItemDeletingEvent, value); }
    }

    bool RaiseItemDeletingEvent()
    {
      ItemDeletingEventArgs args = new ItemDeletingEventArgs(this.SelectedItem, AfxListView.ItemDeletingEvent, this);
      RaiseEvent(args);
      return args.Cancel;
    }

    protected override void OnPreviewKeyDown(KeyEventArgs e)
    {
      if (e.Key == Key.Delete)
      {
        if (RaiseItemDeletingEvent())
        {
          e.Handled = true;
          return;
        }

        IList list = this.ItemsSource as IList;
        IInternalOwnedCollection oc = this.ItemsSource as IInternalOwnedCollection;
        if (oc != null && !IsReadOnly)
        {
          IAssociativeObjectCollection aoc = oc as IAssociativeObjectCollection;
          if (aoc != null)
          {
            Queue<BusinessObject> removed = new Queue<BusinessObject>();

            foreach(BusinessObject o in this.SelectedItems)
            {
              removed.Enqueue(o);
            }

            while (removed.Count > 0)
            {
              list.Remove(removed.Dequeue());
            }
          }
          else
          {
            foreach (BusinessObject o in this.SelectedItems)
            {
              o.IsDeleted = true;
            }
          }

          e.Handled = true;
        }
      }

      base.OnPreviewKeyDown(e);
    }

    Dictionary<string, GridViewColumn> mColumnDictionary = new Dictionary<string, GridViewColumn>();
    public Dictionary<string, GridViewColumn> ColumnDictionary
    {
      get { return mColumnDictionary; }
    }

    void LoadColumnWidths()
    {
      GridView gv = (GridView)this.View;
      if (gv == null) return;

      foreach (var c in gv.Columns)
      {
        string header = (string)c.Header;
        uint width = 0;
        if (RegistryHelper.GetRegistryValue<uint>(Microsoft.Win32.RegistryHive.CurrentUser, string.Format(RegistryHelper.ApplicationKey, string.Format(RegistryKey, Identifier)), header, Microsoft.Win32.RegistryValueKind.DWord, out width))
        {
          if (width == 0) c.Width = double.NaN;
          else c.Width = width;
        }

        WeakEventManager<INotifyPropertyChanged, PropertyChangedEventArgs>.AddHandler(c, "PropertyChanged", OnColumnPropertyChanged);
      }
    }

    private void OnColumnPropertyChanged(object sender, PropertyChangedEventArgs e)
    {
      GridViewColumn gvc = (GridViewColumn)sender;
      if (e.PropertyName == "Width")
      {
        string header = (string)gvc.Header;
        RegistryHelper.SetRegistryValue<uint>(Microsoft.Win32.RegistryHive.CurrentUser, string.Format(RegistryHelper.ApplicationKey, string.Format(RegistryKey, Identifier)), header, Microsoft.Win32.RegistryValueKind.DWord, (uint)gvc.Width);
      }
    }

    public bool IsReadOnly
    {
      get { return (bool)GetValue(IsReadOnlyProperty); }
      set { SetValue(IsReadOnlyProperty, value); }
    }

    public static readonly DependencyProperty IsReadOnlyProperty = DependencyProperty.Register("IsReadOnly", typeof(bool), typeof(AfxListView), new PropertyMetadata(false));

    public string Identifier
    {
      get { return (string)GetValue(IdentifierProperty); }
      set { SetValue(IdentifierProperty, value); }
    }

    public static readonly DependencyProperty IdentifierProperty = DependencyProperty.Register("Identifier", typeof(string), typeof(AfxListView));

    #region ICommandSource

    /// <summary>
    /// Identifies the <see cref="Command"/> property.
    /// </summary>
    public static readonly DependencyProperty CommandProperty = DependencyProperty.Register("Command",
            typeof(ICommand),
            typeof(ListView));

    /// <summary>
    /// Identifies the <see cref="CommandParameter"/> property.
    /// </summary>
    public static readonly DependencyProperty CommandParameterProperty = DependencyProperty.Register("CommandParameter",
            typeof(object),
            typeof(ListView),
            new FrameworkPropertyMetadata(null));

    /// <summary>
    /// Identifies the <see cref="CommandTarget"/> property.
    /// </summary>
    public static readonly DependencyProperty CommandTargetProperty = DependencyProperty.Register("CommandTarget",
            typeof(IInputElement),
            typeof(ListView),
            new FrameworkPropertyMetadata(null));

    /// <summary>
    /// Identifies the <see cref="ItemActivated"/> event.
    /// </summary>
    public static readonly RoutedEvent ItemActivatedEvent = EventManager.RegisterRoutedEvent("ItemActivated",
            RoutingStrategy.Bubble,
            typeof(EventHandler<ItemActivatedEventArgs>),
            typeof(ListView));

    /// <summary>
    /// Gets or sets the <see cref="ICommand"/> to execute whenever an item is activated.
    /// </summary>
    public ICommand Command
    {
      get
      {
        return GetValue(CommandProperty) as ICommand;
      }
      set
      {
        SetValue(CommandProperty, value);
      }
    }

    /// <summary>
    /// Gets or sets the parameter to be passed to the executed <see cref="Command"/>.
    /// </summary>
    public object CommandParameter
    {
      get
      {
        return GetValue(CommandParameterProperty);
      }
      set
      {
        SetValue(CommandParameterProperty, value);
      }
    }

    /// <summary>
    /// Gets or sets the element on which to raise the specified <see cref="Command"/>.
    /// </summary>
    public IInputElement CommandTarget
    {
      get
      {
        return GetValue(CommandTargetProperty) as IInputElement;
      }
      set
      {
        SetValue(CommandTargetProperty, value);
      }
    }

    /// <summary>
    /// Occurs whenever an item in this <c>ListView</c> is activated.
    /// </summary>
    public event EventHandler<ItemActivatedEventArgs> ItemActivated
    {
      add
      {
        AddHandler(ItemActivatedEvent, value);
      }
      remove
      {
        RemoveHandler(ItemActivatedEvent, value);
      }
    }

    static AfxListView()
    {
      //register a handler for any double-clicks on ListViewItems
      EventManager.RegisterClassHandler(typeof(ListViewItem), ListViewItem.MouseDoubleClickEvent, new MouseButtonEventHandler(MouseDoubleClickHandler));
    }

    protected override void OnKeyDown(KeyEventArgs e)
    {
      base.OnKeyDown(e);

      //hitting enter activates an item too
      if ((e.Key == Key.Enter) && (SelectedItem != null))
      {
        OnItemActivated(SelectedItem);
      }
    }

    protected virtual void OnItemActivated(object item)
    {
      RaiseEvent(new ItemActivatedEventArgs(ItemActivatedEvent, item));

      //execute the command if there is one
      if (Command != null)
      {
        RoutedCommand routedCommand = Command as RoutedCommand;

        if (routedCommand != null)
        {
          routedCommand.Execute(CommandParameter, CommandTarget);
        }
        else
        {
          Command.Execute(CommandParameter);
        }
      }
    }

    private static void MouseDoubleClickHandler(object sender, MouseEventArgs e)
    {
      ListViewItem listViewItem = sender as ListViewItem;
      Debug.Assert(listViewItem != null);
      AfxListView listView = FindListViewForItem(listViewItem);

      if (listView != null)
      {
        listView.OnItemActivated(listViewItem.Content);
      }
    }

    private static AfxListView FindListViewForItem(ListViewItem listViewItem)
    {
      DependencyObject parent = VisualTreeHelper.GetParent(listViewItem);

      while (parent != null)
      {
        if (parent is AfxListView)
        {
          return parent as AfxListView;
        }

        parent = VisualTreeHelper.GetParent(parent);
      }

      return null;
    }

    #endregion
  }
}

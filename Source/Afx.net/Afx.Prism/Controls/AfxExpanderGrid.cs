﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace Afx.Prism.Controls
{
  public class AfxExpanderGrid : Grid
  {
    private readonly Dictionary<UIElement, double> m_ElementSizes;

    public AfxExpanderGrid()
    {
      m_ElementSizes = new Dictionary<UIElement, double>();
    }

    protected override Size ArrangeOverride(Size arrangeSize)
    {
      foreach (UIElement child in Children) //.OfType<Expander>().Where(expander => !expander.IsExpanded))
      {
        var expander = child as Expander;
        if (expander == null)
        {
          expander = GetChildOfType<Expander>(child);
        }

        if (expander != null && !expander.IsExpanded)
        {
          if (expander.ExpandDirection == ExpandDirection.Down || expander.ExpandDirection == ExpandDirection.Up)
          {
            var row = GetRow(child);
            if (!RowDefinitions.Any())
              continue;

            RowDefinitions[row].Height = new GridLength(0, GridUnitType.Auto);
          }
          else
          {
            var column = GetColumn(child);
            if (!ColumnDefinitions.Any())
              continue;

            ColumnDefinitions[column].Width = new GridLength(0, GridUnitType.Auto);
          }
        }
      }
      return base.ArrangeOverride(arrangeSize);
    }

    protected override void OnChildDesiredSizeChanged(UIElement child)
    {
      var expander = child as Expander;
      if (expander == null)
      {
        expander = GetChildOfType<Expander>(child);
      }

      if (expander != null)
      {
        bool verticalExpansion = expander.ExpandDirection == ExpandDirection.Down ||
                                 expander.ExpandDirection == ExpandDirection.Up;

        int row = GetRow(child);
        int column = GetColumn(child);

        if (expander.IsExpanded)
        {
          double oldSize;
          if (!m_ElementSizes.TryGetValue(child, out oldSize))
          {
            oldSize = 1;
          }
          var gridLength = new GridLength(oldSize, GridUnitType.Star);
          if (verticalExpansion)
          {
            RowDefinitions[row].Height = gridLength;
          }
          else
          {
            ColumnDefinitions[column].Width = gridLength;
          }
        }
        else
        {
          var gridLength = new GridLength(0, GridUnitType.Auto);
          if (verticalExpansion)
          {
            m_ElementSizes[child] = RowDefinitions[row].Height.Value;
            RowDefinitions[row].Height = gridLength;
          }
          else
          {
            m_ElementSizes[child] = ColumnDefinitions[column].Width.Value;
            ColumnDefinitions[column].Width = gridLength;
          }
        }
      }
      base.OnChildDesiredSizeChanged(child);
    }

    T GetChildOfType<T>(DependencyObject depObj)
        where T : DependencyObject
    {
      if (depObj == null) return null;

      for (int i = 0; i < VisualTreeHelper.GetChildrenCount(depObj); i++)
      {
        var child = VisualTreeHelper.GetChild(depObj, i);

        var result = (child as T) ?? GetChildOfType<T>(child);
        if (result != null) return result;
      }
      return null;
    }
  }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace Afx.Prism.RibbonMdi.Controls
{
  public class AfxDatePicker : DatePicker
  {
    public AfxDatePicker()
    {
      AddHandler(PreviewMouseLeftButtonDownEvent, new MouseButtonEventHandler(SelectivelyIgnoreMouseButton), true);
      AddHandler(GotKeyboardFocusEvent, new RoutedEventHandler(SelectAllText), true);
      AddHandler(MouseDoubleClickEvent, new RoutedEventHandler(SelectAllText), true);
    }

    private static void SelectivelyIgnoreMouseButton(object sender, MouseButtonEventArgs e)
    {
      // Find the DatePicker
      DependencyObject parent = e.OriginalSource as UIElement;
      while (parent != null && !(parent is DatePicker))
        parent = VisualTreeHelper.GetParent(parent);

      if (parent != null)
      {
        var textBox = (DatePicker)parent;
        if (!textBox.IsKeyboardFocusWithin)
        {
          // If the text box is not yet focussed, give it the focus and
          // stop further processing of this click event.
          textBox.Focus();
          e.Handled = true;
        }
      }
    }

    private static void SelectAllText(object sender, RoutedEventArgs e)
    {
      var textBox = e.OriginalSource as DatePicker;
      if (textBox != null)
        textBox.TextB.SelectAll();
    }
  }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism
{
  public interface IContextController : IController
  {
    //void ClearContextViews();
    void SetContextViewModel(ISelectableViewModel context);
  }
}

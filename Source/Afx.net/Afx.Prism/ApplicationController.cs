﻿using Afx.Business.Data;
using Afx.Business.Service;
using Afx.Prism.Events;
using Afx.Prism.Extensions;
using Microsoft.Practices.Prism.Modularity;
using Microsoft.Practices.Prism.PubSubEvents;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace Afx.Prism
{
  public abstract class ApplicationController : Controller, IApplicationController
  {
    protected ApplicationController(IUnityContainer container)
      : base(container)
    {
      container.RegisterInstance<IApplicationController>(this);
      EventAggregator = Container.Resolve<IEventAggregator>();
      RegionManager = Container.Resolve<IRegionManager>();
      mCurrent = this;
      Cache.Instance.CacheUpdated += Instance_CacheUpdated;
    }

    protected ApplicationController(string key, IUnityContainer container)
      : base(key, container)
    {
      container.RegisterInstance<IApplicationController>(this);
      EventAggregator = Container.Resolve<IEventAggregator>();
      RegionManager = Container.Resolve<IRegionManager>();
      mCurrent = this;
      Cache.Instance.CacheUpdated += Instance_CacheUpdated;
    }

    void Instance_CacheUpdated(object sender, EventArgs e)
    {
      EventAggregator.GetEvent<CacheUpdatedEvent>().Publish(EventArgs.Empty);
    }

    IRegionManager mRegionManager;
    public IRegionManager RegionManager
    {
      get { return mRegionManager; }
      private set { mRegionManager = value; }
    }

    IEventAggregator mEventAggregator;
    public IEventAggregator EventAggregator
    {
      get { return mEventAggregator; }
      private set { mEventAggregator = value; }
    }

    protected override bool OnRun()
    {
      //ServiceFactory.InterceptionBehaviors.Add(GetInstance<TransientFaultInterceptionBehavior>());
      return base.OnRun();
    }

    static ApplicationController mCurrent;
    public static ApplicationController Current
    {
      get { return ApplicationController.mCurrent; }
    }

    public Dispatcher Dispatcher
    {
      get { return ApplicationView.Dispatcher; }
    }

    public IApplicationView ApplicationView
    {
      get { return Container.Resolve<IApplicationView>(); }
    }

    protected override void RegisterContainerExtensions()
    {
      Container.AddNewExtension<BuilderAwareExtension>();
      base.RegisterContainerExtensions();
    }
  }
}

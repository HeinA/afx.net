﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism
{
  public interface IDropTarget
  {
    bool CanDrop(object data);
    void DoDrop(object data);
  }
}

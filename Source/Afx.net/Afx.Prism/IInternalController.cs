﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism
{
  internal interface IInternalController : IController
  {
    void FindControllers<T>(Collection<T> controllers) where T : class, IController;
  }
}

﻿using Afx.Business;
using Afx.Business.Collections;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Mvvm;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace Afx.Prism
{
  public class ViewModel<TModel> : ViewModel, IViewModel<TModel>
    where TModel : class, IBusinessObject
  {
    protected ViewModel(IController controller)
      : base(controller)
    {
    }

    protected ViewModel(TModel context, IController controller)
      : this(controller)
    {
      Model = context;
    }

    string mError;
    public const string ErrorProperty = "Error";
    public string Error
    {
      get { return mError; }
      protected set { SetProperty<string>(ref mError, value); }
    }

    #region bool IsReadOnly

    public virtual bool IsReadOnly
    {
      get { return false; }
    }

    #endregion

    //bool mIsReadOnly = false;
    //public bool IsReadOnly
    //{
    //  get { return mIsReadOnly; }
    //  set { SetProperty<bool>(ref mIsReadOnly, value); }
    //}

    #region Visibility ErrorVisibility

    public const string ErrorVisibilityProperty = "ErrorVisibility";
    public Visibility ErrorVisibility
    {
      get
      {
        if (IsValid) return Visibility.Collapsed;
        return Visibility.Visible;
      }
    }

    #endregion

    public const string IsValidProperty = "IsValid";
    public virtual bool IsValid
    {
      get { return string.IsNullOrWhiteSpace(Error); }
    }

    public virtual bool Validate()
    {
      Error = GetError();
      return string.IsNullOrWhiteSpace(Error);
    }

    TModel mModel;
    public TModel Model 
    {
      get { return mModel; }
      set
      {
        if (SetProperty<TModel>(ref mModel, value))
        {
          OnModelChanged();
          if (Model != null)
          {
            WeakEventManager<BusinessObject, PropertyChangedEventArgs>.AddHandler(Model as BusinessObject, "PropertyChanged", Model_PropertyChanged);
            WeakEventManager<BusinessObject, CompositionChangedEventArgs>.AddHandler(Model as BusinessObject, "CompositionChanged", Model_CompositionChanged);
          }
        }
      }
    }

    void Model_CompositionChanged(object sender, CompositionChangedEventArgs e)
    {
      OnModelCompositionChanged(e);
    }

    protected virtual void OnModelCompositionChanged(CompositionChangedEventArgs e)
    {
      Model.Validate();
    }

    IBusinessObject IViewModel.Model
    {
      get { return Model; }
      set { Model = (TModel)value; }
    }

    private void Model_PropertyChanged(object sender, PropertyChangedEventArgs e)
    {
      OnModelPropertyChanged(e.PropertyName);
    }

    protected virtual void OnModelPropertyChanged(string propertyName)
    {
      switch (propertyName)
      {
        case BusinessObject.ErrorProperty:
          Validate();
          OnPropertyChanged(ErrorVisibilityProperty);
          break;
      }
      OnPropertyChanged(propertyName);
    }

    protected virtual void OnModelChanged()
    {
      using (new WriteSuppressor())
      {
        OnPropertyChanged(null);
        ResetViewModelCollections();
      }
    }

    public bool IsViewModelOf(BusinessObject model)
    {
      if (model == null) throw new ArgumentNullException("model");
      if (this.Model == null) return false;
      return Model.Equals(model);
    }

    #region IDataErrorInfo

    string IDataErrorInfo.Error
    {
      get
      {
        return GetError();
      }
    }

    string IDataErrorInfo.this[string columnName]
    {
      get
      {
        return GetError(columnName);
      }
    }

    protected virtual string GetError()
    {
      if (Model == null) return null;
      Model.Validate();
      return Model.Error;
    }

    protected virtual string GetError(string propertyName)
    {
      if (Model.IsDeleted) return null;
      if (propertyName.StartsWith("Model.")) return ((IDataErrorInfo)Model)[propertyName.Replace("Model.", string.Empty)];
      return ((IDataErrorInfo)Model)[propertyName];
    }

    #endregion
  }

  public abstract class ViewModel : IViewModelBase, IInternalViewModelBase
  {
    protected ViewModel(IController controller)
    {
      mController = controller;
    }

    protected T GetDefaultValue<T>()
    {
      if (typeof(BusinessObject).IsAssignableFrom(typeof(T))) return (T)Activator.CreateInstance(typeof(T), new object[] { true });
      return default(T);
    }

    public virtual void OnEnterPressed()
    {
    }

#if DISPOSE
    ~ViewModel()
    {
      Debug.WriteLine("- {0}", this.GetType());
    }

#endif

    #region DelegateCommand LoadedCommand

    DelegateCommand<RoutedEventArgs> mLoadedCommand;
    public DelegateCommand<RoutedEventArgs> LoadedCommand
    {
      get { return mLoadedCommand ?? (mLoadedCommand = new DelegateCommand<RoutedEventArgs>(ExecuteLoaded)); }
    }

    bool bLoaded = false;
    protected virtual void ExecuteLoaded(RoutedEventArgs obj)
    {
      try
      {
        if (bLoaded) return;
        bLoaded = true;
        OnLoaded();
      }
      catch (Exception ex)
      {
        if (ExceptionHelper.HandleException(ex)) throw ex;
        Controller.Terminate();
      }
    }

    #endregion

    /// <summary>
    /// Requires a control on the view to have NotifyOnLoaded.Notify="True" set
    /// </summary>
    protected virtual void OnLoaded()
    {
    }

    IViewModelBase mParent;
    public  IViewModelBase Parent
    {
      get { return mParent; }
    }

    IViewModelBase IInternalViewModelBase.Parent
    {
      get { return mParent; }
      set { mParent = value; }
    }

    protected IController Controller
    {
      get { return (IController)mController; }
    }

    #region bool IsFocused

    const string IsFocusedProperty = "IsFocused";
    bool mIsFocused;
    public virtual bool IsFocused
    {
      get { return mIsFocused; }
      set
      {
        if (!SetProperty<bool>(ref mIsFocused, value) && value)
        {
          OnPropertyChanged("IsFocused");
        }
      }
    }

    #endregion

    IController mController;
    IController IViewModelBase.Controller
    {
      get { return mController; }
    }

    #region INotifyPropertyChanged

    public event PropertyChangedEventHandler PropertyChanged;

    protected virtual void OnPropertyChanged(string propertyName)
    {
      if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
    }

    /// <summary>
    /// Checks if a property already matches a desired value. Sets the property and
    /// notifies listeners only when necessary.
    /// </summary>
    /// <typeparam name="T">Type of the property.</typeparam>
    /// <param name="storage">Reference to a property with both getter and setter.</param>
    /// <param name="value">Desired value for the property.</param>
    /// <param name="propertyName">Name of the property used to notify listeners. This
    /// value is optional and can be provided automatically when invoked from compilers that
    /// support CallerMemberName.</param>
    /// <returns>True if the value was changed, false if the existing value matched the
    /// desired value.</returns>
    protected virtual bool SetProperty<T>(ref T storage, T value, [System.Runtime.CompilerServices.CallerMemberName] string propertyName = null)
    {
      if (typeof(T).IsValueType && Object.Equals(storage, value)) return false;
      else if ((object)storage == (object)value) return false;

      storage = value;
      this.OnPropertyChanged(propertyName);

      return true;
    }

    /// <summary>
    /// Raises this object's PropertyChanged event.
    /// </summary>
    /// <typeparam name="T">The type of the property that has a new value</typeparam>
    /// <param name="propertyExpression">A Lambda expression representing the property that has a new value.</param>
    protected void OnPropertyChanged<T>(Expression<Func<T>> propertyExpression)
    {
      var propertyName = PropertySupport.ExtractPropertyName(propertyExpression);
      this.OnPropertyChanged(propertyName);
    }

    #endregion

    #region ViewModelCollections

    Collection<string> mViewModelCollections = new Collection<string>();
    Dictionary<string, object> mViewModelCollectionDictionary = new Dictionary<string, object>();

    public IEnumerable<TViewModel> ResolveViewModels<TViewModel, TModel>(IList models, [CallerMemberName] string propertyName = null)
      where TViewModel : class, IViewModel
      where TModel : class, IBusinessObject
    {
      if (models == null) return null;

      try
      {
        if (!mViewModelCollections.Contains(propertyName)) mViewModelCollections.Add(propertyName);
        //if (!mViewModelCollectionDictionary.ContainsKey(propertyName))
        //{
          ViewModelCollection<TViewModel, TModel> viewModels = new ViewModelCollection<TViewModel, TModel>(models, this);
          //mViewModelCollectionDictionary.Add(propertyName, viewModels);
          return viewModels;
        //}
        //else
        //{
        //  ViewModelCollection<TViewModel, T> vm = (ViewModelCollection<TViewModel, T>)mViewModelCollectionDictionary[propertyName];
        //  return vm;
        //}
      }
      catch
      {
        throw;
      }
    }

    public void ResetViewModelCollections()
    {
      mViewModelCollectionDictionary = new Dictionary<string, object>();
      foreach (string propertyName in mViewModelCollections)
      {
        OnPropertyChanged(propertyName);
      }
    }

    #endregion

  }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace Afx.Prism
{
  public interface IApplicationView
  {
    //IApplicationViewModel ViewModel { get; }
    Dispatcher Dispatcher { get; }
  }
}

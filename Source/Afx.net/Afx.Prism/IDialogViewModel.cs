﻿using Dataforge.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Prism
{
  public interface IDialogViewModel<TModel> : IViewModel<TModel>
      where TModel : BusinessObject
  {
  }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism
{
  public abstract class ResourceInfo
  {
    public abstract Uri GetUri();

    public virtual int Priority
    {
      get { return 0; }
    }
  }
}

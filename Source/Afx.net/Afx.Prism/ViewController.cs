﻿using Afx.Business;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Afx.Prism
{
  public abstract class ViewController<TModel, TViewModel> : Controller, IViewController
    where TModel : class, IBusinessObject
    where TViewModel : class, IViewModel
  {
    protected ViewController(IController controller)
      : base(controller)
    {
    }

    protected ViewController(string key, IController controller)
      : base(key, controller)
    {
    }

    #region IRegionManager RegionManager

    IRegionManager mRegionManager;
    [Dependency]
    public IRegionManager RegionManager
    {
      get { return mRegionManager; }
      set { mRegionManager = value; }
    }

    #endregion

    #region TModel DataContext

    TModel mDataContext;
    public virtual TModel DataContext
    {
      get { return mDataContext; }
      set
      {
        if (mDataContext != value)
        {
          //if (mDataContext != null) DataContext.PropertyChanged -= DataContext_PropertyChanged;
          mDataContext = value;
          ViewModel.Model = value;
          if (mDataContext != null) WeakEventManager<BusinessObject, PropertyChangedEventArgs>.AddHandler(mDataContext as BusinessObject, "PropertyChanged", DataContext_PropertyChanged);
          //DataContext.PropertyChanged += DataContext_PropertyChanged;
          OnDataContextChanged();
        }
      }
    }

    void DataContext_PropertyChanged(object sender, PropertyChangedEventArgs e)
    {
      OnDataContextPropertyChanged(e.PropertyName);
    }

    protected virtual void OnDataContextPropertyChanged(string propertyName)
    {
    }

    protected virtual void OnDataContextChanged()
    {
    }

    protected override bool OnRun()
    {
      if (DataContext == null) throw new InvalidOperationException("DataContext may not be null.");
      return base.OnRun();
    }

    IBusinessObject IViewController.DataContext
    {
      get { return DataContext; }
      set { DataContext = (TModel)value; }
    }

    #endregion

    protected override void PurgeViewModels()
    {
      for (int i = mViewModels.Count - 1; i >= 0; i--)
      {
        if (mViewModels[i] != ViewModel) mViewModels.RemoveAt(i);
      }
    }

    #region TViewModel ViewModel

    TViewModel mViewModel;
    public virtual TViewModel ViewModel
    {
      get
      {
        if (mViewModel == null)
        {
          if (DataContext == null) return null;
          mViewModel = GetCreateViewModel<TViewModel>(DataContext, null);
          if (mViewModel != null) mViewModel.Model = DataContext;
        }
        return mViewModel;
      }
      private set { mViewModel = value; }
    }

    IViewModel IViewController.ViewModel
    {
      get { return ViewModel; }
    }

    #endregion

    protected override void OnTerminated()
    {
      ViewModel = null;
      base.OnTerminated();
    }
  }
}

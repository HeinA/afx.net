﻿using Afx.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism
{
  public interface ISelectableViewModel
  {
    IBusinessObject Model { get; }
    bool IsSelected { get; set; }
    bool IsFocused { set; }
    bool MayDelete { get; }
  }
}

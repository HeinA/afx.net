﻿using Afx.Business;
using Microsoft.Practices.Prism.PubSubEvents;
using Microsoft.Practices.Prism.Regions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism
{
  public interface IViewController : IController
  {
    IRegionManager RegionManager { get; }
    IViewModel ViewModel { get; }
    IBusinessObject DataContext { get; set; }
  }
}

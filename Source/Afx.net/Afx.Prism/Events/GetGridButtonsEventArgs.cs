﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.Events
{
  public class GetGridButtonsEventArgs : EventArgs
  {
    public GetGridButtonsEventArgs(object vm)
    {
      ViewModel = vm;
      Buttons = new Collection<GridButtonViewModel>();
    }

    public object ViewModel { get; private set; }
    public Collection<GridButtonViewModel> Buttons { get; private set; }
  }
}

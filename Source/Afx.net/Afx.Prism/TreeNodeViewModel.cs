﻿using Afx.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Afx.Prism
{
  public abstract class TreeNodeViewModel<TModel> : SelectableViewModel<TModel>, ITreeNodeViewModel
    where TModel : class, IBusinessObject
  {
    protected TreeNodeViewModel(IController controller)
      : base(controller)
    {
    }

    #region bool IsExpanded

    public bool IsExpanded
    {
      get { return GetState<bool>(); }
      set { SetState<bool>(value); }
    }

    #endregion
  }
}

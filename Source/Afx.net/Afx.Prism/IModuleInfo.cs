﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism
{
  public interface IModuleInfo
  {
    string ModuleNamespace { get; }
    string[] ModuleDependencies { get; }
    Type ModuleController { get; }
  }
}

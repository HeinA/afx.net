﻿using Afx.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism
{
  public abstract class Controller : IInternalController, IDisposable
  {
    #region Constructors

    protected Controller()
      : this(new UnityContainer())
    {
    }

    protected Controller(string key)
      : this(key, new UnityContainer())
    {
    }

    protected Controller(IUnityContainer container)
    {
      AttachContainer(container);
    }

    protected Controller(string key, IUnityContainer container)
    {
      Key = key;
      AttachContainer(container);
    }

    protected Controller(IController controller)
      : this(controller.GetInstance<IUnityContainer>().CreateChildContainer())
    {
      Parent = controller;
      ((IInternalControllerCollection)Parent.Controllers).Add(this);
    }

    protected Controller(string id, IController controller)
      : this(id, controller.GetInstance<IUnityContainer>().CreateChildContainer())
    {
      Parent = controller;
      ((IInternalControllerCollection)Parent.Controllers).Add(this);
    }


#if DISPOSE
    ~Controller()
    {
      Dispose(false);
      Debug.WriteLine("- Controller {0}", (object)Key);
    }
#endif

    #endregion

    #region Properties

    string mKey = Guid.NewGuid().ToString("B");
    public string Key
    {
      get { return mKey; }
      set
      {
        if (mKey == value) return;
        if (Parent != null) ((IInternalControllerCollection)Parent.Controllers).Remove(this);
        mKey = value;
        if (Parent != null) ((IInternalControllerCollection)Parent.Controllers).Add(this);
      }
    }

    IUnityContainer mContainer;
    protected internal IUnityContainer Container
    {
      get { return mContainer; }
      private set { mContainer = value; }
    }

    IController mParent;
    public IController Parent
    {
      get { return mParent; }
      private set { mParent = value; }
    }

    ControllerCollection mControllers = new ControllerCollection();
    public ControllerCollection Controllers
    {
      get { return mControllers; }
    }

    ControllerState mState = ControllerState.Created;
    public ControllerState State
    {
      get { return mState; }
      private set { mState = value; }
    }

    #endregion

    #region Methods

    public TController CreateChildController<TController>()
      where TController : Controller
    {
      ConstructorInfo ci = typeof(TController).GetConstructor(new Type[] { typeof(IController) });
      if (ci == null) throw new InvalidOperationException("Constructor not found.");
      TController c = (TController)ci.Invoke(new object[] { this });
      c.ConfigureContainer();
      c.Container.BuildUp(c.GetType(), c);
      c.AfterContainerConfigured();
      return c;
    }

    public TController GetCreateChildController<TController>(string key)
      where TController : Controller
    {
      if (Controllers.Contains(key)) return (TController)Controllers[key];
      return CreateChildController<TController>();
    }

    public Controller CreateChildController(Type controllerType)
    {
      ConstructorInfo ci = controllerType.GetConstructor(new Type[] { typeof(IController) });
      if (ci == null) throw new InvalidOperationException("Constructor not found.");
      Controller c = (Controller)ci.Invoke(new object[] { this });
      c.ConfigureContainer();
      c.Container.BuildUp(c.GetType(), c);
      c.AfterContainerConfigured();
      return c;
    }

    public Controller GetCreateChildController(Type controllerType, string key)
    {
      if (Controllers.Contains(key)) return (Controller)Controllers[key];
      return CreateChildController(controllerType);
    }

    public bool Run()
    {
      if (State == ControllerState.Terminated) throw new InvalidOperationException("Controller is in an invalid state.");
      if (!OnRun()) return false;
      State = ControllerState.Running;
      OnRunning();
      return true;
    }

    protected virtual bool OnRun()
    {
      return true;
    }

    protected virtual void OnRunning()
    {
    }

    void FindControllers<T>(Collection<T> controllers)
      where T : class, IController
    {
      foreach (IInternalController c in Controllers)
      {
        if (typeof(T).IsAssignableFrom(c.GetType())) controllers.Add((T)c);
        c.FindControllers<T>(controllers);
      }
    }

    void IInternalController.FindControllers<T>(Collection<T> controllers)
    {
      FindControllers<T>(controllers);
    }

    public Collection<T> FindControllers<T>()
      where T : class, IController
    {
      Collection<T> controllers = new Collection<T>();
      FindControllers<T>(controllers);
      return controllers;
    }

    public virtual void Terminate()
    {
      if (State == ControllerState.Terminated) return;
      if (Parent != null) ((IInternalControllerCollection)Parent.Controllers).Remove(this);
      State = ControllerState.Terminated;
      OnTerminated();
      Dispose();
    }

    public event EventHandler<EventArgs> Terminated;

    protected virtual void OnTerminated()
    {
      if (Terminated != null) Terminated(this, EventArgs.Empty);
    }

    void AttachContainer(IUnityContainer container)
    {
      if (container == null) throw new ArgumentNullException("container");
      if (State != ControllerState.Created) throw new InvalidOperationException("Controller is in an invalid state.");
      if (Container != null) throw new InvalidOperationException("Container already assigned.");

      Container = container;
      RegisterContainerExtensions();
      Container.RegisterInstance<IController>(this);
      Container.RegisterInstance(this.GetType(), this);
    }

    protected virtual void ConfigureContainer()
    {
    }

    protected virtual void AfterContainerConfigured()
    {
    }

    public event EventHandler Disposing;

    bool mDisposed = false;
    void Dispose()
    {
      if (mDisposed) return;
      mDisposed = true;
      Dispose(true);
    }

    void IDisposable.Dispose()
    {
      Dispose();
    }

    protected virtual void RegisterContainerExtensions()
    {
    }

    protected virtual void Dispose(bool disposing)
    {
      if (disposing)
      {
        if (Disposing != null) Disposing(this, EventArgs.Empty);
        if (State != ControllerState.Terminated) Terminate();
        Container.Dispose();
        Container.Teardown(this);
        Container = null;
        PurgeViewModels();
        StateDictionary = null;

#if !DISPOSE
        GC.SuppressFinalize(this);
#endif
      }
    }

    #endregion

    #region State

    Dictionary<Guid, Dictionary<string, object>> mStateDictionary;
    Dictionary<Guid, Dictionary<string, object>> StateDictionary
    {
      get { return mStateDictionary ?? (mStateDictionary = new Dictionary<Guid, Dictionary<string, object>>()); }
      set { mStateDictionary = value; }
    }

    public bool SetObjectState<TPropertyType>(IBusinessObject obj, string propertyName, TPropertyType value)
    {
      if (!StateDictionary.ContainsKey(obj.GlobalIdentifier)) StateDictionary.Add(obj.GlobalIdentifier, new Dictionary<string, object>());
      Dictionary<string, object> objectStateDictionary = StateDictionary[obj.GlobalIdentifier];
      if (!objectStateDictionary.ContainsKey(propertyName))
      {
        objectStateDictionary.Add(propertyName, value);
      }
      else
      {
        object o = objectStateDictionary[propertyName];
        if (o.GetType().IsValueType && Object.Equals(o, value)) return false;
        else if (o == (object)value) return false;
        objectStateDictionary[propertyName] = value;
      }
      return true;
    }

    public TPropertyType GetObjectState<TPropertyType>(IBusinessObject obj, TPropertyType defaultValue, string propertyName)
    {
      if (!StateDictionary.ContainsKey(obj.GlobalIdentifier))
      {
        SetObjectState<TPropertyType>(obj, propertyName, defaultValue);
        return defaultValue;
      }
      Dictionary<string, object> objectStateDictionary = StateDictionary[obj.GlobalIdentifier];
      if (!objectStateDictionary.ContainsKey(propertyName))
      {
        SetObjectState<TPropertyType>(obj, propertyName, defaultValue);
        return defaultValue;
      }
      else
        return (TPropertyType)objectStateDictionary[propertyName];
    }

    Dictionary<string, object> mGlobalStateDictionary;
    Dictionary<string, object> GlobalStateDictionary
    {
      get { return mGlobalStateDictionary ?? (mGlobalStateDictionary = new Dictionary<string, object>()); }
      set { mGlobalStateDictionary = value; }
    }

    public bool SetState<TPropertyType>(string stateName, TPropertyType value)
    {
      if (!GlobalStateDictionary.ContainsKey(stateName))
      {
        GlobalStateDictionary.Add(stateName, value);
      }
      else
      {
        object o = GlobalStateDictionary[stateName];
        if (o.GetType().IsValueType && Object.Equals(o, value)) return false;
        else if (o == (object)value) return false;
        GlobalStateDictionary[stateName] = value;
      }
      return true;
    }

    public TPropertyType GetState<TPropertyType>(string stateName)
    {
      if (!GlobalStateDictionary.ContainsKey(stateName))
        return default(TPropertyType);
      else
        return (TPropertyType)GlobalStateDictionary[stateName];
    }

    #endregion

    #region IServiceLocator

    public IEnumerable<TService> GetAllInstances<TService>()
    {
      return Container.ResolveAll<TService>();
    }

    public IEnumerable<object> GetAllInstances(Type serviceType)
    {
      return Container.ResolveAll(serviceType);
    }

    public TService GetInstance<TService>(string key)
    {
      return Container.Resolve<TService>(key);
    }

    public TService GetInstance<TService>()
    {
      try
      {
        return Container.Resolve<TService>();
      }
      catch
      {
#if DEBUG
        if (Debugger.IsAttached) Debugger.Break();
#endif
        throw;
      }
    }

    public object GetInstance(Type serviceType, string key)
    {
      return Container.Resolve(serviceType, key);
    }

    public object GetInstance(Type serviceType)
    {
      return Container.Resolve(serviceType);
    }

    public object GetService(Type serviceType)
    {
      return Container.Resolve(serviceType);
    }

    #endregion

    #region IViewModelProvider

    protected Collection<IViewModelBase> mViewModels = new Collection<IViewModelBase>();
    public Collection<IViewModelBase> ViewModels
    {
      get { return ViewModels; }
    }

    protected virtual void PurgeViewModels()
    {
      mViewModels.Clear();
    }

    object mLock = new object();

    public TViewModel CreateViewModel<TViewModel>(IViewModelBase parent)
      where TViewModel : class, IViewModelBase
    {
      TViewModel vm = GetInstance<TViewModel>();
      OnViewModelCreated(vm);
      ((IInternalViewModelBase)vm).Parent = parent;
      return vm;
    }

    public IViewModelBase CreateViewModel(Type viewType, IViewModelBase parent)
    {
      IViewModelBase vm = (IViewModelBase)GetInstance(viewType);
      OnViewModelCreated(vm);
      ((IInternalViewModelBase)vm).Parent = parent;
      return vm;
    }

    public TViewModel GetCreateViewModel<TViewModel>()
      where TViewModel : class, IViewModelBase
    {
      return GetCreateViewModel<TViewModel>((IViewModelBase)null);
    }

    public TViewModel GetCreateViewModel<TViewModel>(IViewModelBase parent)
      where TViewModel : class, IViewModelBase
    {
      lock (mLock)
      {
        TViewModel vm = (TViewModel)mViewModels.FirstOrDefault((r) => typeof(TViewModel).IsAssignableFrom(r.GetType()));
        if (vm != null) return vm;

        vm = GetInstance<TViewModel>();
        ((IInternalViewModelBase)vm).Parent = parent;
        OnViewModelCreated(vm);

        mViewModels.Add(vm);
        return vm;
      }
    }

    public TViewModel GetCreateViewModel<TViewModel>(IBusinessObject model, IViewModelBase parent)
      where TViewModel : class, IViewModel
    {
      if (model == null) throw new ArgumentNullException("model");
      lock (mLock)
      {
        TViewModel vm = (TViewModel)mViewModels.OfType<IViewModel>().FirstOrDefault((r) => typeof(TViewModel).IsAssignableFrom(r.GetType()) && r.Model == model);
        if (vm != null)
        {
          // TODO: TEST!!!! Cache undone
          ((IInternalViewModelBase)vm).Parent = parent;

          return vm;
        }

        vm = GetInstance<TViewModel>();
        ((IInternalViewModelBase)vm).Parent = parent;
        vm.Model = (BusinessObject)model;
        OnViewModelCreated(vm);

        mViewModels.Add(vm);
        return vm;
      }
    }

    public TViewModel GetCreateViewModel<TViewModel>(IBusinessObject model)
      where TViewModel : class, IViewModel
    {
      return GetCreateViewModel<TViewModel>(model, null);
    }

    public TViewModel GetViewModel<TViewModel>(IBusinessObject model)
      where TViewModel : class, IViewModel
    {
      if (model == null) throw new ArgumentNullException("model");
      lock (mLock)
      {
        TViewModel vm = (TViewModel)mViewModels.OfType<IViewModel>().FirstOrDefault((r) => typeof(TViewModel).IsAssignableFrom(r.GetType()) && r.Model == model);
        if (vm != null) return vm;
        return null;
      }
    }

    public TViewModel GetViewModel<TViewModel>()
      where TViewModel : class, IViewModel
    {
      lock (mLock)
      {
        TViewModel vm = (TViewModel)mViewModels.FirstOrDefault((r) => typeof(TViewModel).IsAssignableFrom(r.GetType()));
        if (vm != null) return vm;
        return null;
      }
    }

    internal IViewModel GetViewModel(Type viewModelType, object model)
    {
      if (model == null) throw new ArgumentNullException("model");
      lock (mLock)
      {
        IViewModel vm = (IViewModel)mViewModels.OfType<IViewModel>().FirstOrDefault((r) => viewModelType.IsAssignableFrom(r.GetType()) && r.Model == model);
        if (vm != null) return vm;
        return null;
      }
    }

    internal IViewModel GetCreateViewModel(Type viewModelType, object model, IViewModelBase parent)
    {
      if (model == null) throw new ArgumentNullException("model");
      lock (mLock)
      {
        IViewModel vm = (IViewModel)mViewModels.OfType<IViewModel>().FirstOrDefault((r) => viewModelType.IsAssignableFrom(r.GetType()) && r.Model == model);
        if (vm != null) return vm;

        vm = (IViewModel)GetInstance(viewModelType);
        ((IInternalViewModelBase)vm).Parent = parent;
        vm.Model = (BusinessObject)model;
        OnViewModelCreated(vm);

        mViewModels.Add(vm);
        return vm;
      }
    }

    protected virtual void OnViewModelCreated(IViewModelBase viewModel)
    {
    }

    #endregion
  }
}

﻿using Afx.Business;
using Afx.Business.Activities;
using Afx.Business.Security;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.Activities
{
  public class ActivityPrintViewModel
  {
    public ActivityPrintViewModel(Activity activity)
    {
      Activity = activity;
    }

    #region Activity Activity

    Activity mActivity;
    protected Activity Activity
    {
      get { return mActivity; }
      set { mActivity = value; }
    }

    #endregion

    #region virtual string ActivityTypeName

    public virtual string ActivityTypeName
    {
      get { return Activity.Name; }
    }

    #endregion

    #region string Company

    public string Company
    {
      get { return SecurityContext.OrganizationalUnit.Root.Name; }
    }

    #endregion

    #region byte[] Logo

    public virtual byte[] Logo
    {
      get { return Convert.FromBase64String(SecurityContext.OrganizationalUnit.Base64Image); }
    }

    #endregion
  }
}

﻿using Afx.Business;
using Afx.Business.Activities;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.Activities
{
  public interface IActivityPrintInfo
  {
    int Priority { get; }
    short Copies { get; }
    Stream ReportStream(object argument);
    void RefreshReportData(LocalReport report, BusinessObject activity, object scope, object argument);
    object GetScope(IController controller);
  }
}

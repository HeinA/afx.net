﻿using Dataforge.Business;
using Microsoft.Practices.Prism.Mvvm;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Prism
{
  public abstract class UnityDialogController : UnityViewController, IDialogController
  {
    public UnityDialogController(IController controller)
      : base(controller)
    {
    }

    public UnityDialogController(string id, IController controller)
      : base(id, controller)
    {
    }
  }
}

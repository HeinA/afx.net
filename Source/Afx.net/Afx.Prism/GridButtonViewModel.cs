﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace Afx.Prism
{
  public class GridButtonViewModel
  {
    public GridButtonViewModel(string text, Action<object> onClick)
    {
      Text = text;
      OnClick = onClick;
    }

    public string Text { get; private set; }
    public Action<object> OnClick { get; private set; }

    internal void Stub(object sender, RoutedEventArgs e)
    {
      Button b = sender as Button;
      OnClick(b.DataContext);
    }
  }
}

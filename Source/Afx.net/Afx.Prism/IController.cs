﻿using Afx.Business;
using Microsoft.Practices.ServiceLocation;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace Afx.Prism
{
  public interface IController : IServiceLocator, IViewModelProvider
  {
    string Key { get; }
    ControllerState State { get; }
    ControllerCollection Controllers { get; }
    IController Parent { get; }

    bool SetObjectState<TPropertyType>(IBusinessObject obj, string propertyName, TPropertyType value);
    TPropertyType GetObjectState<TPropertyType>(IBusinessObject obj, TPropertyType defaultValue, string propertyName);

    bool SetState<TPropertyType>(string stateName, TPropertyType value);
    TPropertyType GetState<TPropertyType>(string stateName);
    TController CreateChildController<TController>()
      where TController : Controller;
    TController GetCreateChildController<TController>(string key)
      where TController : Controller;

    bool Run();
    void Terminate();

    event EventHandler<EventArgs> Terminated;
    Collection<T> FindControllers<T>() where T : class, IController;
  }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Controls;
using System.Windows;
using System.ComponentModel;
using System.Windows.Data;
using System.Reflection;
using System.Windows.Threading;
using System.Windows.Media;
using System.Windows.Controls.Primitives;
using System.Collections.ObjectModel;
using Afx.Prism.Controls;
using System.Linq;
using Afx.Business;

namespace Afx.Prism
{
  public class ErrorProvider : Decorator
  {
    #region Constructors

    public ErrorProvider()
    {
      this.DataContextChanged += new DependencyPropertyChangedEventHandler(ErrorProvider_DataContextChanged);
      this.Loaded += new RoutedEventHandler(ErrorProvider_Loaded);
    }

    #endregion

    #region string IgnoreProperties

    string mIgnoreProperties = string.Empty;
    public string IgnoreProperties
    {
      get { return mIgnoreProperties; }
      set { mIgnoreProperties = value; }
    }

    #endregion

    #region void Validate(...)

    public void Validate()
    {
      if (!mLoaded) return;
      Collection<DependencyObject> markedElements = new Collection<DependencyObject>();

      if (this.DataContext is IDataErrorInfo)
      {
        this.Dispatcher.BeginInvoke(new System.Action(() =>
        {
          try
          {
            Dictionary<FrameworkElement, List<PropertyBinding>> elementBindings = GetBindings();

            foreach (FrameworkElement element in elementBindings.Keys)
            {
              foreach (PropertyBinding pb in elementBindings[element])
              {
                IDataErrorInfo ei = this.DataContext as IDataErrorInfo;
                if (ei == null) return;
                if (string.IsNullOrWhiteSpace(pb.Binding.Path.Path))
                  continue;
                if (pb.Binding.Path.Path.Contains('.'))
                {
                }
                string errorMessage = ei[pb.Binding.Path.Path];
                if (!string.IsNullOrEmpty(errorMessage))
                {
                  BindingExpression expression = element.GetBindingExpression(pb.Dp);
                  ValidationError error = new ValidationError(new ExceptionValidationRule(), expression, errorMessage, null);

                  System.Windows.Controls.Validation.MarkInvalid(expression, error);
                  markedElements.Add(element);
                }
                else
                {
                  if (markedElements.Contains(element)) continue;
                  BindingExpression expression = element.GetBindingExpression(pb.Dp);
                  System.Windows.Controls.Validation.ClearInvalid(expression);
                }
              }
            }
          }
          catch
          {
#if DEBUG
            if (System.Diagnostics.Debugger.IsAttached) System.Diagnostics.Debugger.Break();
#endif
          }
        }), DispatcherPriority.Background, null);
      }
    }

    #endregion


    #region Private

    private delegate void FoundBindingCallbackDelegate(FrameworkElement element, Binding binding, DependencyProperty dp);
    private Dictionary<DependencyObject, Style> mBackupStyles = new Dictionary<DependencyObject, Style>();

    #region class PropertyBinding

    private class PropertyBinding
    {
      #region DependencyProperty Dp

      DependencyProperty mDp;
      public DependencyProperty Dp
      {
        get { return mDp; }
        set { mDp = value; }
      }

      #endregion

      #region Binding Binding

      Binding mBinding;
      public Binding Binding
      {
        get { return mBinding; }
        set { mBinding = value; }
      }

      #endregion
    }

    #endregion

    #region Event Handlers

    bool mLoaded = false;
    private void ErrorProvider_Loaded(object sender, RoutedEventArgs e)
    {
      mLoaded = true;
      Validate();
    }

    private void ErrorProvider_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
    {
      if (e.OldValue != null && e.OldValue is INotifyPropertyChanged)
      {
        ((INotifyPropertyChanged)e.OldValue).PropertyChanged -= new PropertyChangedEventHandler(DataContext_PropertyChanged);
      }

      if (e.NewValue != null && e.NewValue is INotifyPropertyChanged)
      {
        ((INotifyPropertyChanged)e.NewValue).PropertyChanged += new PropertyChangedEventHandler(DataContext_PropertyChanged);
      }

      Validate();
    }

    #endregion

    #region Dictionary<FrameworkElement, List<PropertyBinding>> GetBindings(...)

    private Dictionary<FrameworkElement, List<PropertyBinding>> GetBindings()
    {
      Dictionary<FrameworkElement, List<PropertyBinding>> elementBindings = new Dictionary<FrameworkElement, List<PropertyBinding>>();

      FindBindingsRecursively(this,
        delegate(FrameworkElement element, Binding binding, DependencyProperty dp)
        {
          if (!elementBindings.ContainsKey(element)) elementBindings.Add(element, new List<PropertyBinding>());
          elementBindings[element].Add(new PropertyBinding() { Binding = binding, Dp = dp });
        }, this.DataContext, new Collection<DependencyObject>());

      return elementBindings;
    }

    #endregion

    #region void DataContext_PropertyChanged(...)

    private void DataContext_PropertyChanged(object sender, PropertyChangedEventArgs e)
    {
      if (e.PropertyName != null && IgnorePropertiesArray.Contains(e.PropertyName))
      {
        return;
      }

      Validate();
    }

    #endregion

    #region string[] IgnorePropertiesArray

    string[] harcodedProperties = new string[] { "IsValid", "HasError", "ErrorVisibility", "Error", "ToolbarVisibility", "GlobalOperations" };
    Collection<string> mIgnorePropertiesArray;
    Collection<string> IgnorePropertiesArray
    {
      get { return mIgnorePropertiesArray ?? (mIgnorePropertiesArray = new Collection<string>(new Collection<string>(harcodedProperties).Concat(mIgnoreProperties.Split(',')).ToList())); }
    }

    #endregion

    #region void FindBindingsRecursively(...)

    private void FindBindingsRecursively(DependencyObject element, FoundBindingCallbackDelegate callbackDelegate, object context, Collection<DependencyObject> elements)
    {
      if (elements.Contains(element)) return;
      elements.Add(element);

      MemberInfo[] members = element.GetType().GetMembers(BindingFlags.Static |
              BindingFlags.Public |
              BindingFlags.FlattenHierarchy);

      foreach (MemberInfo member in members)
      {
        DependencyProperty dp = null;

        if (member.MemberType == MemberTypes.Field)
        {
          FieldInfo field = (FieldInfo)member;
          if (typeof(DependencyProperty).IsAssignableFrom(field.FieldType))
          {
            dp = (DependencyProperty)field.GetValue(element);
          }
        }
        else if (member.MemberType == MemberTypes.Property)
        {
          PropertyInfo prop = (PropertyInfo)member;
          if (typeof(DependencyProperty).IsAssignableFrom(prop.PropertyType))
          {
            dp = (DependencyProperty)prop.GetValue(element, null);
          }
        }

        if (dp != null)
        {
          Binding bb = BindingOperations.GetBinding(element, dp);
          if (bb != null)
          {
            if (element is FrameworkElement)
            {
              if (((FrameworkElement)element).DataContext == context)
              {
                callbackDelegate((FrameworkElement)element, bb, dp);
              }
            }
          }
        }
      }

      if (element is FrameworkElement || element is FrameworkContentElement)
      {
        foreach (object childElement in LogicalTreeHelper.GetChildren(element))
        {
          if (childElement is DependencyObject)
          {
            FindBindingsRecursively((DependencyObject)childElement, callbackDelegate, context, elements);
          }
        }
      }
    }

    #endregion

    #endregion
  }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Afx.Prism
{
  public class WaitCursor : IDisposable
  {
    Cursor mPreviousCursor;

    public WaitCursor()
    {
      mPreviousCursor = Mouse.OverrideCursor;
      Mouse.SetCursor(Cursors.Wait);
    }

    public void Dispose()
    {
      Mouse.SetCursor(mPreviousCursor);
      Mouse.UpdateCursor();
    }
  }
}

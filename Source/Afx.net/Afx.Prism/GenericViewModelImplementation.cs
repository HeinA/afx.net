﻿using Afx.Business;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism
{
  public static class GenericViewModelImplementation
  {
    public static string GetErrorMessage<T>(T model) where T : BusinessObject
    {
      return model.Error;
    }

    public static bool GetIsValid<T>(T model) where T : BusinessObject
    {
      return model.IsValid;
    }

    public static bool Validate<T>(T model) where T : BusinessObject
    {
      return model.Validate();
    }

    public static void SetModel<T>(ref T model, Action<CompositionChangedEventArgs> CompositionChangedCallback, Action<PropertyChangedEventArgs> PropertyChangedCallback
  }
}

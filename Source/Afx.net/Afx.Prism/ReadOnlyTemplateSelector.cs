﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace Afx.Prism
{
  public class ReadOnlyTemplateSelector : DataTemplateSelector
  {
    public DataTemplate EditableTemplate { get; set; }
    public DataTemplate ReadOnlyTemplate { get; set; }

    public override DataTemplate SelectTemplate(object item, DependencyObject container)
    {
      IViewModel vm = item as IViewModel;
      if (vm != null)
      {
        if (vm.IsReadOnly) return ReadOnlyTemplate;
        else return EditableTemplate;
      }

      return base.SelectTemplate(item, container);
    }
  }
}

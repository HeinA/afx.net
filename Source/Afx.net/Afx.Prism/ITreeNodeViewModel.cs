﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism
{
  public interface ITreeNodeViewModel : ISelectableViewModel
  {
    bool IsExpanded { get; set; }
  }
}

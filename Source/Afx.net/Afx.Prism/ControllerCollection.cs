﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism
{
  public class ControllerCollection : IEnumerable<IController>, IInternalControllerCollection
  {
    Collection<IController> mInternalCollection = new Collection<IController>();
    protected Collection<IController> InternalCollection
    {
      get { return mInternalCollection; }
    }

    public ControllerCollection()
    {
    }

    Dictionary<string, IController> mControllerNameDictionary = new Dictionary<string,IController>();
    Dictionary<string, IController> ControllerNameDictionary
    {
      get { return mControllerNameDictionary; }
    }

    public IController this[string key]
    {
      get { return ControllerNameDictionary[key]; }
    }

    public bool Contains(string key)
    {
      return ControllerNameDictionary.ContainsKey(key);
    }

    public bool Contains(IController controller)
    {
      return InternalCollection.Contains(controller);
    }

    #region IInternalControllerCollection

    void IInternalControllerCollection.Add(IController controller)
    {
      if (!InternalCollection.Contains(controller))
      {
        InternalCollection.Add(controller);
        ControllerNameDictionary.Add(controller.Key, controller);
      }
    }

    void IInternalControllerCollection.Remove(IController controller)
    {
      if (InternalCollection.Contains(controller))
      {
        InternalCollection.Remove(controller);
        ControllerNameDictionary.Remove(controller.Key);
      }
    }

    #endregion

    #region IEnumerable<IController>

    public IEnumerator<IController> GetEnumerator()
    {
      return InternalCollection.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
      return InternalCollection.GetEnumerator();
    }

    #endregion
  }
}

﻿using Afx.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism
{
  public abstract class StatefullViewModel<TModel> : ViewModel<TModel>
    where TModel : class, IBusinessObject
  {
    protected StatefullViewModel(IController controller)
      : base(controller)
    {
    }

    #region void SetState<TPropertyType>(TPropertyType value, string propertyName)

    protected bool SetState<TPropertyType>(TPropertyType value, [CallerMemberName] string propertyName = null)
    {
      if (propertyName == null) throw new ArgumentNullException("propertyName");
      if (Controller.SetObjectState<TPropertyType>(Model, propertyName, value))
      {
        OnPropertyChanged(propertyName);
        return true;
      }
      return false;
    }

    #endregion

    #region TPropertyType GetState<TPropertyType>(string propertyName)

    protected TPropertyType GetState<TPropertyType>(TPropertyType defaultValue = default(TPropertyType), [CallerMemberName] string propertyName = null)
    {
      if (propertyName == null) throw new ArgumentNullException("propertyName");
      return Controller.GetObjectState<TPropertyType>(Model, defaultValue, propertyName);
    }

    #endregion
  }
}

﻿using Afx.Business.Documents;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.Documents
{
  public class DocumentReferencePrintViewModel
  {
    public DocumentReferencePrintViewModel(DocumentReference documentReference)
    {
      DocumentReference = documentReference;
    }

    #region DocumentReference DocumentReference

    DocumentReference mDocumentReference;
    public DocumentReference DocumentReference
    {
      get { return mDocumentReference; }
      set { mDocumentReference = value; }
    }

    #endregion

    #region string ReferenceType

    public string ReferenceType
    {
      get { return DocumentReference.DocumentTypeReference.Name; }
    }

    #endregion

    #region string Reference

    public string Reference
    {
      get { return DocumentReference.Reference; }
    }

    #endregion
  }
}

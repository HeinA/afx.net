﻿using Afx.Business.Documents;
using Afx.Business.Security;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.Documents
{
  public class DocumentPrintViewModel
  {
    public DocumentPrintViewModel(Document document)
    {
      Document = document;
    }

    #region Document Document

    Document mDocument;
    protected Document Document
    {
      get { return mDocument; }
      set { mDocument = value; }
    }

    #endregion

    public virtual string DocumentTypeName
    {
      get { return Document.DocumentType.Name; }
    }

    public virtual string DocumentNumber
    {
      get { return Document.DocumentNumber; }
    }

    public virtual string Title
    {
      get { return string.Format("{0} {1}", DocumentTypeName, DocumentNumber); }
    }

    public virtual string Barcode
    {
      get { return BarCode.BarcodeConverter128.StringToBarcode(DocumentNumber); }
    }

    public virtual byte[] Logo
    {
      get { return Convert.FromBase64String(Document.OrganizationalUnit.Base64Image); }
    }

    public DateTime DocumentDate
    {
      get { return Document.DocumentDate; }
    }

    public string Company
    {
      get { return Document.OrganizationalUnit.Root.Name; }
    }

    public string Branch
    {
      get { return SecurityContext.OrganizationalUnit.Name; }
    }

    public string CompanyPhysicalAddress
    {
      get { return Document.OrganizationalUnit.PhysicalAddress; }
    }

    public string CompanyPostalAddress
    {
      get { return Document.OrganizationalUnit.PostalAddress; }
    }

    public string Notes
    {
      get { return Document.Notes; }
    }
  }
}

﻿using Afx.Business.Documents;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.Documents
{
  public interface IDocumentPrintInfo
  {
    int Priority { get; }
    short Copies { get; }
    Stream ReportStream(object argument);
    void RefreshReportData(LocalReport report, Document document, object scope, object argument);
    object GetScope(IController controller); 
  }
}

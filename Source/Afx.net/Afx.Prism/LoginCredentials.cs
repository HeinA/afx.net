﻿using Afx.Business;
using Afx.Business.Security;
using Afx.Business.Service;
using Afx.Business.ServiceModel;
using Afx.Business.Validation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism
{
  public class LoginCredentials : BusinessObject
  {
    public LoginCredentials()
    {
      string user = null;

      //TODO: Build no connection error check here.
      using (var svc = ProxyFactory.GetService<IAfxService>(DefaultServer.ServerName))
      {
        OrganizationalUnits = svc.LoadLoginOrganizationalUnits();
      }

      if (RegistryHelper.GetRegistryValue<string>(Microsoft.Win32.RegistryHive.CurrentUser, string.Format(RegistryHelper.ApplicationKey, SecurityContext.RegistryKey), SecurityContext.LastUser, Microsoft.Win32.RegistryValueKind.String, out user))
      {
        Username = user;
      }

      string guidString = null;
      if (RegistryHelper.GetRegistryValue<string>(Microsoft.Win32.RegistryHive.CurrentUser, string.Format(RegistryHelper.ApplicationKey, SecurityContext.RegistryKey), SecurityContext.LastOU, Microsoft.Win32.RegistryValueKind.String, out guidString))
      {
        Guid guid = Guid.Parse(guidString);
        OrganizationalUnit = OrganizationalUnits.Where(ou => ou.GlobalIdentifier.Equals(guid)).FirstOrDefault();
      }

      //Collection<LoginOrganizationalUnit> ous = null;

      //BackgroundWorker bw = new BackgroundWorker();
      //bw.DoWork += (s, e) =>
      //  {
      //    using (var svc = ProxyFactory.GetService<IAfxService>(DefaultServer.ServerName))
      //    {
      //      ous = svc.LoadLoginOrganizationalUnits();
      //    }
      //  };
      //bw.RunWorkerCompleted += (s, e) =>
      //  {
      //    OrganizationalUnits = ous;

      //    if (RegistryHelper.GetRegistryValue<string>(Microsoft.Win32.RegistryHive.CurrentUser, string.Format(RegistryHelper.ApplicationKey, SecurityContext.RegistryKey), SecurityContext.LastUser, Microsoft.Win32.RegistryValueKind.String, out user))
      //    {
      //      Username = user;
      //    }

      //    string guidString = null;
      //    if (RegistryHelper.GetRegistryValue<string>(Microsoft.Win32.RegistryHive.CurrentUser, string.Format(RegistryHelper.ApplicationKey, SecurityContext.RegistryKey), SecurityContext.LastOU, Microsoft.Win32.RegistryValueKind.String, out guidString))
      //    {
      //      Guid guid = Guid.Parse(guidString);
      //      OrganizationalUnit = OrganizationalUnits.Where(ou => ou.GlobalIdentifier.Equals(guid)).FirstOrDefault();
      //    }
      //  };
      //bw.RunWorkerAsync();

    }

    #region DataMember string Username

    public const string UsernameProperty = "Username";
    string mUsername;
    public string Username
    {
      get { return mUsername; }
      set { SetProperty<string>(ref mUsername, value, UsernameProperty); }
    }

    #endregion

    #region DataMember string PasswordHash

    public const string PasswordHashProperty = "PasswordHash";
    string mPasswordHash;
    public string PasswordHash
    {
      get { return mPasswordHash; }
      set { SetProperty<string>(ref mPasswordHash, value, PasswordHashProperty); }
    }

    #endregion

    #region LoginOrganizationalUnit OrganizationalUnit

    public const string OrganizationalUnitProperty = "OrganizationalUnit";
    LoginOrganizationalUnit mOrganizationalUnit;
    [Mandatory("Organizational Unit is a mandatory field.")]
    public LoginOrganizationalUnit OrganizationalUnit
    {
      get { return mOrganizationalUnit; }
      set { SetProperty<LoginOrganizationalUnit>(ref mOrganizationalUnit, value); }
    }

    #endregion

    #region Collection<LoginOrganizationalUnit> OrganizationalUnits

    public const string OrganizationalUnitsProperty = "OrganizationalUnits";
    Collection<LoginOrganizationalUnit> mOrganizationalUnits;
    public Collection<LoginOrganizationalUnit> OrganizationalUnits
    {
      get { return mOrganizationalUnits; }
      set { SetProperty<Collection<LoginOrganizationalUnit>>(ref mOrganizationalUnits, value); }
    }

    #endregion

    #region DataMember AuthenticationResult AuthenticationResult

    public const string AuthenticationResultProperty = "AuthenticationResult";
    AuthenticationResult mAuthenticationResult;
    public AuthenticationResult AuthenticationResult
    {
      get { return mAuthenticationResult; }
      set { SetProperty<AuthenticationResult>(ref mAuthenticationResult, value, AuthenticationResultProperty); }
    }

    #endregion

    protected override void GetErrors(Collection<string> errors, string propertyName)
    {
      if (propertyName == null || propertyName == UsernameProperty)
      {
        if (string.IsNullOrWhiteSpace(Username)) errors.Add("Username is a mandatory field.");
      }

      if (propertyName == null || propertyName == PasswordHashProperty)
      {
        if (string.IsNullOrWhiteSpace(PasswordHash)) errors.Add("Password is a mandatory field.");
      }

      if (errors.Count == 0)
      {
        try
        {
          using (new WaitCursor())
          {
            AuthenticationResult ar = SecurityContext.Authenticate(OrganizationalUnit, Username, PasswordHash, true);
            if (!string.IsNullOrWhiteSpace(ar.Error)) errors.Add(ar.Error);
            else AuthenticationResult = ar;
          }
        }
        catch (Exception ex)
        {
          if (ExceptionHelper.HandleException(ex)) throw;
        }
      }

      base.GetErrors(errors, propertyName);
    }
  }
}

﻿using Afx.Business;
using Afx.Business.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Data.Sql
{
  [Export(typeof(IPersistanceManager))]
  public class SqlPersistanceManager : PersistanceManager
  {
    protected override char IdentifierStart
    {
      get { return '['; }
    }

    protected override char IdentifierEnd
    {
      get { return ']'; }
    }

    public override IDbCommand GetCommand(string dbName)
    {
      try
      {
        var con = new SqlCommand(string.Empty, ConnectionScope.Current.SqlConnection(dbName));
        con.CommandTimeout = 300;
        return con;
      }
      catch(Exception ex)
      {
        throw new InvalidOperationException("No ConnectionScope", ex);
      }
    }

    public override string AddParameter(object name, object value, IDbCommand command)
    {
      string parameterName = name.ToString();
      if (parameterName.Substring(0, 1) != "@") parameterName = string.Format("@p_{0}", name);
      SqlCommand cmd = command as SqlCommand;
      if (cmd == null) throw new StatementException("Invalid command type.");
      cmd.Parameters.AddWithValue(parameterName, value == null ? DBNull.Value : value);
      return parameterName;
    }

    public override string AddOutputParameter(object name, IDbCommand command)
    {
      string parameterName = name.ToString();
      if (parameterName.Substring(0, 1) != "@") parameterName = string.Format("@p_{0}", name);
      SqlCommand cmd = command as SqlCommand;
      if (cmd == null) throw new StatementException("Invalid command type.");
      cmd.Parameters.Add(parameterName, SqlDbType.Int).Direction = ParameterDirection.Output;
      return parameterName;
    }

    public override IDbCommand GetSetFlagCommand(int id, string schema, string tableName, string flag)
    {
      IDbCommand cmd = GetCommand();
      string sql = string.Format("INSERT INTO [{3}].[{0}Flag] ([Id], [Flag]) SELECT {1}, {2} WHERE NOT EXISTS (SELECT 1 FROM [{3}].[{0}Flag] WHERE [Id]={1} AND [Flag]={2})", tableName, AddParameter("Id", id, cmd), AddParameter("Flag", flag, cmd), schema);
      cmd.CommandText = sql;
      return cmd;
    }

    public override IDbCommand GetUnFlagCommand(int id, string schema, string tableName, string flag)
    {
      IDbCommand cmd = GetCommand();
      string sql = string.Format("DELETE FROM [{3}].[{0}Flag] WHERE [Id]={1} AND [Flag]={2}", tableName, AddParameter("Id", id, cmd), AddParameter("Flag", flag, cmd), schema);
      cmd.CommandText = sql;
      return cmd;
    }

    public override IDbCommand GetQueryFlagsCommand(int id, string schema, string tableName)
    {
      IDbCommand cmd = GetCommand();
      string sql = string.Format("SELECT [Flag] FROM [{2}].[{0}Flag] WHERE [Id]={1}", tableName, AddParameter("Id", id, cmd), schema);
      cmd.CommandText = sql;
      return cmd;
    }
  }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace Afx.SqlExtensions
{
  public static class GuidHelper
  {
    [DllImport("rpcrt4.dll", SetLastError = true)]
    static extern int UuidCreateSequential(out Guid guid);

    public static Guid NewSequentialGuid()
    {
      Guid guid = Guid.Empty;
      int i = UuidCreateSequential(out guid);
      if (i != 0) throw new InvalidOperationException("Guid is not Globally Unique.");

      var s = guid.ToByteArray();
      var t = new byte[16];
      t[3] = s[10];
      t[2] = s[11];
      t[1] = s[12];
      t[0] = s[13];
      t[5] = s[14];
      t[4] = s[15];
      t[7] = s[8];
      t[6] = s[9];
      t[8] = s[1];
      t[9] = s[0];
      t[10] = s[7];
      t[11] = s[6];
      t[12] = s[5];
      t[13] = s[4];
      t[14] = s[3];
      t[15] = s[2];

      return new Guid(t);
    }
  }
}

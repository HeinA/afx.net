﻿using Afx.Business;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Documents;
using Afx.Business.Security;
using Afx.Business.Service;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Security.Cryptography;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace SyncTool
{
  public static class SyncHelper
  {
    class InnerSyncHeader
    {
      #region Guid GlobalIdentifier

      public const string GlobalIdentifierProperty = "GlobalIdentifier";
      Guid mGlobalIdentifier;
      public Guid GlobalIdentifier
      {
        get { return mGlobalIdentifier; }
        set { mGlobalIdentifier = value; }
      }

      #endregion

      #region string DocumentNumber

      public const string DocumentNumberProperty = "DocumentNumber";
      string mDocumentNumber;
      public string DocumentNumber
      {
        get { return mDocumentNumber; }
        set { mDocumentNumber = value; }
      }

      #endregion

      #region int SourceRevision

      public const string SourceRevisionProperty = "SourceRevision";
      int mSourceRevision;
      public int SourceRevision
      {
        get { return mSourceRevision; }
        set { mSourceRevision = value; }
      }

      #endregion

      #region int DestinationRevision

      public const string DestinationRevisionProperty = "DestinationRevision";
      int mDestinationRevision;
      public int DestinationRevision
      {
        get { return mDestinationRevision; }
        set { mDestinationRevision = value; }
      }

      #endregion
    }

    public static void Sync()
    {
      try
      {
        BasicCollection<BusinessObject> sourceCache = null;
        BasicCollection<BusinessObject> destinationCache = null;

        AuthenticationResult sourceAuthentication = null;
        AuthenticationResult destinationAuthentication = null;

        BasicCollection<SyncHeader> sourceHeader = null;
        BasicCollection<SyncHeader> destinationHeader = null;

        BasicHttpBinding binding = new BasicHttpBinding(BasicHttpSecurityMode.None);
        binding.MaxReceivedMessageSize = 31457280;
        binding.ReceiveTimeout = new TimeSpan(0, 10, 0);
        binding.SendTimeout = new TimeSpan(0, 10, 0);

        using (var svc = ServiceFactory.GetDynamicService<IAfxService>(binding, ConfigurationManager.AppSettings["source"]))
        {
          var loginOU = svc.Instance.LoadLoginOrganizationalUnits().FirstOrDefault();

          HashAlgorithm algoritm = new SHA256Managed();
          var hash = ASCIIEncoding.ASCII.GetString(algoritm.ComputeHash(ASCIIEncoding.ASCII.GetBytes(ConfigurationManager.AppSettings["password"])));

          sourceAuthentication = svc.Instance.Login(ConfigurationManager.AppSettings["user"], hash, loginOU);
          SecurityContext.Login(sourceAuthentication);
          sourceCache = svc.Instance.LoadCache();
          Cache.Instance.LoadCache(sourceCache);

          sourceHeader = svc.Instance.GetDocumentSyncHeaders(0);
        }

        using (var svc = ServiceFactory.GetDynamicService<IAfxService>(binding, ConfigurationManager.AppSettings["destination"]))
        {
          var loginOU = svc.Instance.LoadLoginOrganizationalUnits().FirstOrDefault();

          HashAlgorithm algoritm = new SHA256Managed();
          var hash = ASCIIEncoding.ASCII.GetString(algoritm.ComputeHash(ASCIIEncoding.ASCII.GetBytes(ConfigurationManager.AppSettings["password"])));

          destinationAuthentication = svc.Instance.Login(ConfigurationManager.AppSettings["user"], hash, loginOU);
          SecurityContext.Login(destinationAuthentication);

          svc.Instance.Import(sourceCache);
          destinationCache = svc.Instance.LoadCache();

          destinationHeader = svc.Instance.GetDocumentSyncHeaders(0);
        }

        List<InnerSyncHeader> items = new List<InnerSyncHeader>();
        foreach (var h in sourceHeader)
        {
          var ish = new InnerSyncHeader() { GlobalIdentifier = h.GlobalIdentifier, DocumentNumber = h.DocumentNumber, SourceRevision = h.Revision };
          items.Add(ish);
        }

        foreach (var h in destinationHeader)
        {
          var ish = items.FirstOrDefault(ish1 => ish1.GlobalIdentifier.Equals(h.GlobalIdentifier));
          if (ish == null)
          {
            ish = new InnerSyncHeader() { GlobalIdentifier = h.GlobalIdentifier, DocumentNumber = h.DocumentNumber, DestinationRevision = h.Revision };
          }
          else
          {
            ish.DestinationRevision = h.Revision;
          }
        }

        foreach (var ish in items)
        {
          try
          {
            if (ish.SourceRevision > ish.DestinationRevision)
            {
              Document d = null;
              SecurityContext.Login(sourceAuthentication);
              using (var svc = ServiceFactory.GetDynamicService<IAfxService>(binding, ConfigurationManager.AppSettings["source"]))
              {
                d = svc.Instance.LoadDocument(ish.GlobalIdentifier);
              }

              SecurityContext.Login(destinationAuthentication);
              using (var svc = ServiceFactory.GetDynamicService<IAfxService>(binding, ConfigurationManager.AppSettings["destination"]))
              {
                svc.Instance.Import(new BasicCollection<BusinessObject>(new BusinessObject[] { d }));
              }

              Debug.WriteLine(string.Format("Processed Document {0}", ish.DocumentNumber));
            }

            if (ish.SourceRevision < ish.DestinationRevision)
            {
              Document d = null;
              SecurityContext.Login(destinationAuthentication);
              using (var svc = ServiceFactory.GetDynamicService<IAfxService>(binding, ConfigurationManager.AppSettings["destination"]))
              {
                d = svc.Instance.LoadDocument(ish.GlobalIdentifier);
              }

              SecurityContext.Login(sourceAuthentication);
              using (var svc = ServiceFactory.GetDynamicService<IAfxService>(binding, ConfigurationManager.AppSettings["source"]))
              {
                svc.Instance.Import(new BasicCollection<BusinessObject>(new BusinessObject[] { d }));
              }

              Debug.WriteLine(string.Format("Processed Document {0}", ish.DocumentNumber));
            }

          }
          catch { }
        }
      }
      catch
      {
      }
    }
  }
}

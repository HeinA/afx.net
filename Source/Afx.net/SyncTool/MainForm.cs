﻿using Afx.Business;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Documents;
using Afx.Business.Security;
using Afx.Business.Service;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SyncTool
{
  public partial class MainForm : Form
  {
    public MainForm()
    {
      InitializeComponent();

      txtSourceService.Text = ConfigurationManager.AppSettings["source"];
      txtDestinationService.Text = ConfigurationManager.AppSettings["destination"];
    }

    private void cmdCompare_Click(object sender, EventArgs e)
    {
      try
      {
        listView1.Items.Clear();

        BasicCollection<SyncHeader> sourceHeader = null;
        BasicCollection<SyncHeader> destinationHeader = null;

        AuthenticationResult sourceAuthentication = null;
        AuthenticationResult destinationAuthentication = null;

        BasicHttpBinding httpBinding = new BasicHttpBinding(BasicHttpSecurityMode.None);
        httpBinding.MaxReceivedMessageSize = 31457280;
        httpBinding.ReceiveTimeout = new TimeSpan(0, 10, 0);
        httpBinding.SendTimeout = new TimeSpan(0, 10, 0);

        using (var svc = ServiceFactory.GetDynamicService<IAfxService>(httpBinding, txtSourceService.Text))
        {
          var ous = svc.Instance.LoadLoginOrganizationalUnits();
          var loginOU = ous.FirstOrDefault();

          HashAlgorithm algoritm = new SHA256Managed();
          var hash = ASCIIEncoding.ASCII.GetString(algoritm.ComputeHash(ASCIIEncoding.ASCII.GetBytes(txtPassword.Text)));

          sourceAuthentication = svc.Instance.Login(txtUsername.Text, hash, loginOU);
          SecurityContext.Login(sourceAuthentication);
          Cache.Instance.LoadCache(svc.Instance.LoadCache());

          sourceHeader = svc.Instance.GetDocumentSyncHeaders(int.Parse(txtMaxAge.Text));
        }

        using (var svc = ServiceFactory.GetDynamicService<IAfxService>(httpBinding, txtDestinationService.Text))
        {
          var loginOU = svc.Instance.LoadLoginOrganizationalUnits().FirstOrDefault();

          HashAlgorithm algoritm = new SHA256Managed();
          var hash = ASCIIEncoding.ASCII.GetString(algoritm.ComputeHash(ASCIIEncoding.ASCII.GetBytes(txtPassword.Text)));

          destinationAuthentication = svc.Instance.Login(txtUsername.Text, hash, loginOU);
          SecurityContext.Login(destinationAuthentication);

          destinationHeader = svc.Instance.GetDocumentSyncHeaders(int.Parse(txtMaxAge.Text));
        }

        List<ListViewItem> items = new List<ListViewItem>();
        foreach (var h in sourceHeader)
        {
          var lvi = new ListViewItem(h.DocumentNumber);
          lvi.Tag = h.GlobalIdentifier;
          lvi.SubItems.Add(h.Revision.ToString());
          lvi.SubItems.Add(string.Empty);
          lvi.SubItems.Add(string.Empty);
          items.Add(lvi);
        }

        foreach (var h in destinationHeader)
        {
          var lvi = items.FirstOrDefault(lvi1 => lvi1.Tag.Equals(h.GlobalIdentifier));
          if (lvi == null)
          {
            lvi = new ListViewItem(h.DocumentNumber);
            lvi.Tag = h.GlobalIdentifier;
            lvi.SubItems.Add(string.Empty);
            lvi.SubItems.Add(h.Revision.ToString());
            lvi.SubItems.Add(string.Empty);
          }
          else
          {
            lvi.SubItems[2].Text = h.Revision.ToString();
          }
        }

        foreach (var lvi1 in items.Where(lvi2 => !lvi2.SubItems[1].Text.Equals(lvi2.SubItems[2].Text)).OrderBy(lvi2 => lvi2.Text))
        {
          listView1.Items.Add(lvi1);
        }
      }
      catch (Exception ex)
      {
        MessageBox.Show(this, ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
      }
    }

    private void cmdSyncCache_Click(object sender, EventArgs e)
    {
      try
      {
        BasicCollection<BusinessObject> sourceCache = null;
        BasicCollection<BusinessObject> destinationCache = null;

        AuthenticationResult sourceAuthentication = null;
        AuthenticationResult destinationAuthentication = null;

        BasicHttpBinding binding = new BasicHttpBinding(BasicHttpSecurityMode.None);
        binding.MaxReceivedMessageSize = 31457280;
        binding.ReceiveTimeout = new TimeSpan(0, 10, 0);
        binding.SendTimeout = new TimeSpan(0, 10, 0);

        using (var svc = ServiceFactory.GetDynamicService<IAfxService>(binding, txtSourceService.Text))
        {
          var loginOU = svc.Instance.LoadLoginOrganizationalUnits().FirstOrDefault();

          HashAlgorithm algoritm = new SHA256Managed();
          var hash = ASCIIEncoding.ASCII.GetString(algoritm.ComputeHash(ASCIIEncoding.ASCII.GetBytes(txtPassword.Text)));

          sourceAuthentication = svc.Instance.Login(txtUsername.Text, hash, loginOU);
          SecurityContext.Login(sourceAuthentication);
          sourceCache = svc.Instance.LoadCache();
          Cache.Instance.LoadCache(sourceCache);
        }

        using (var svc = ServiceFactory.GetDynamicService<IAfxService>(binding, txtDestinationService.Text))
        {
          var loginOU = svc.Instance.LoadLoginOrganizationalUnits().FirstOrDefault();

          HashAlgorithm algoritm = new SHA256Managed();
          var hash = ASCIIEncoding.ASCII.GetString(algoritm.ComputeHash(ASCIIEncoding.ASCII.GetBytes(txtPassword.Text)));

          destinationAuthentication = svc.Instance.Login(txtUsername.Text, hash, loginOU);
          SecurityContext.Login(destinationAuthentication);

          svc.Instance.Import(sourceCache);
          destinationCache = svc.Instance.LoadCache();
        }

        txtSourceCacheItems.Text = string.Format("{0:#,##0}", sourceCache.Count);
        txtDestinationCacheItems.Text = string.Format("{0:#,##0}", destinationCache.Count);
      }
      catch (Exception ex)
      {
        MessageBox.Show(this, ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
      }
    }

    private void cmdSyncDocuments_Click(object sender, EventArgs e)
    {
      try
      {
        AuthenticationResult sourceAuthentication = null;
        AuthenticationResult destinationAuthentication = null;

        BasicHttpBinding binding = new BasicHttpBinding(BasicHttpSecurityMode.None);
        binding.MaxReceivedMessageSize = 31457280;
        binding.ReceiveTimeout = new TimeSpan(0, 10, 0);
        binding.SendTimeout = new TimeSpan(0, 10, 0);

        using (var svc = ServiceFactory.GetDynamicService<IAfxService>(binding, txtSourceService.Text))
        {
          var loginOU = svc.Instance.LoadLoginOrganizationalUnits().FirstOrDefault();

          HashAlgorithm algoritm = new SHA256Managed();
          var hash = ASCIIEncoding.ASCII.GetString(algoritm.ComputeHash(ASCIIEncoding.ASCII.GetBytes(txtPassword.Text)));

          sourceAuthentication = svc.Instance.Login(txtUsername.Text, hash, loginOU);
        }

        using (var svc = ServiceFactory.GetDynamicService<IAfxService>(binding, txtDestinationService.Text))
        {
          var loginOU = svc.Instance.LoadLoginOrganizationalUnits().FirstOrDefault();

          HashAlgorithm algoritm = new SHA256Managed();
          var hash = ASCIIEncoding.ASCII.GetString(algoritm.ComputeHash(ASCIIEncoding.ASCII.GetBytes(txtPassword.Text)));

          destinationAuthentication = svc.Instance.Login(txtUsername.Text, hash, loginOU);
        }

        foreach (ListViewItem lvi in listView1.Items.OfType<ListViewItem>().ToArray())
        {
          lvi.SubItems[3].Text = string.Empty;
        }

        Application.DoEvents();

        foreach (ListViewItem lvi in listView1.Items.OfType<ListViewItem>().ToArray())
        {
          try
          {
            var id = (Guid)lvi.Tag;
            int sourceRevision = 0;
            int destinationRevision = 0;
            int.TryParse(lvi.SubItems[1].Text, out sourceRevision);
            int.TryParse(lvi.SubItems[2].Text, out destinationRevision);

            if (sourceRevision > destinationRevision)
            {
              Document d = null;
              SecurityContext.Login(sourceAuthentication);
              using (var svc = ServiceFactory.GetDynamicService<IAfxService>(binding, txtSourceService.Text))
              {
                d = svc.Instance.LoadDocument(id);
              }

              SecurityContext.Login(destinationAuthentication);
              using (var svc = ServiceFactory.GetDynamicService<IAfxService>(binding, txtDestinationService.Text))
              {
                svc.Instance.Import(new BasicCollection<BusinessObject>(new BusinessObject[] { d }));
              }

            }

            if (sourceRevision < destinationRevision)
            {
              Document d = null;
              SecurityContext.Login(destinationAuthentication);
              using (var svc = ServiceFactory.GetDynamicService<IAfxService>(binding, txtDestinationService.Text))
              {
                d = svc.Instance.LoadDocument(id);
              }

              SecurityContext.Login(sourceAuthentication);
              using (var svc = ServiceFactory.GetDynamicService<IAfxService>(binding, txtSourceService.Text))
              {
                svc.Instance.Import(new BasicCollection<BusinessObject>(new BusinessObject[] { d }));
              }
            }

            listView1.Items.Remove(lvi);
            Application.DoEvents();
          }
          catch(Exception ex)
          {
            lvi.SubItems[3].Text = ex.Message;
            Application.DoEvents();
          }
        }

        //listView1.Items.Clear();
      }
      catch (Exception ex)
      {
        MessageBox.Show(this, ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
      }
    }
  }
}

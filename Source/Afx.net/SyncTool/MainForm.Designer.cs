﻿namespace SyncTool
{
  partial class MainForm
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
      this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
      this.label5 = new System.Windows.Forms.Label();
      this.txtMaxAge = new System.Windows.Forms.TextBox();
      this.label1 = new System.Windows.Forms.Label();
      this.txtSourceService = new System.Windows.Forms.TextBox();
      this.txtDestinationService = new System.Windows.Forms.TextBox();
      this.label6 = new System.Windows.Forms.Label();
      this.label7 = new System.Windows.Forms.Label();
      this.lblPassword = new System.Windows.Forms.Label();
      this.txtUsername = new System.Windows.Forms.TextBox();
      this.txtPassword = new System.Windows.Forms.TextBox();
      this.label8 = new System.Windows.Forms.Label();
      this.label9 = new System.Windows.Forms.Label();
      this.txtSourceCacheItems = new System.Windows.Forms.TextBox();
      this.txtDestinationCacheItems = new System.Windows.Forms.TextBox();
      this.panel1 = new System.Windows.Forms.Panel();
      this.cmdSyncDocuments = new System.Windows.Forms.Button();
      this.cmdSyncCache = new System.Windows.Forms.Button();
      this.cmdCompare = new System.Windows.Forms.Button();
      this.listView1 = new System.Windows.Forms.ListView();
      this.chDocument = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
      this.chSourceRevision = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
      this.chDestinationRevision = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
      this.chMessage = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
      this.tableLayoutPanel1.SuspendLayout();
      this.panel1.SuspendLayout();
      this.SuspendLayout();
      // 
      // tableLayoutPanel1
      // 
      this.tableLayoutPanel1.AutoSize = true;
      this.tableLayoutPanel1.ColumnCount = 2;
      this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
      this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
      this.tableLayoutPanel1.Controls.Add(this.label5, 0, 4);
      this.tableLayoutPanel1.Controls.Add(this.txtMaxAge, 1, 4);
      this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
      this.tableLayoutPanel1.Controls.Add(this.txtSourceService, 1, 0);
      this.tableLayoutPanel1.Controls.Add(this.txtDestinationService, 1, 1);
      this.tableLayoutPanel1.Controls.Add(this.label6, 0, 1);
      this.tableLayoutPanel1.Controls.Add(this.label7, 0, 2);
      this.tableLayoutPanel1.Controls.Add(this.lblPassword, 0, 3);
      this.tableLayoutPanel1.Controls.Add(this.txtUsername, 1, 2);
      this.tableLayoutPanel1.Controls.Add(this.txtPassword, 1, 3);
      this.tableLayoutPanel1.Controls.Add(this.label8, 0, 5);
      this.tableLayoutPanel1.Controls.Add(this.label9, 0, 6);
      this.tableLayoutPanel1.Controls.Add(this.txtSourceCacheItems, 1, 5);
      this.tableLayoutPanel1.Controls.Add(this.txtDestinationCacheItems, 1, 6);
      this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
      this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
      this.tableLayoutPanel1.Name = "tableLayoutPanel1";
      this.tableLayoutPanel1.RowCount = 7;
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tableLayoutPanel1.Size = new System.Drawing.Size(697, 182);
      this.tableLayoutPanel1.TabIndex = 0;
      // 
      // label5
      // 
      this.label5.AutoSize = true;
      this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
      this.label5.Location = new System.Drawing.Point(3, 104);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size(144, 26);
      this.label5.TabIndex = 15;
      this.label5.Text = "Max Age in Days";
      this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // txtMaxAge
      // 
      this.txtMaxAge.Location = new System.Drawing.Point(153, 107);
      this.txtMaxAge.Name = "txtMaxAge";
      this.txtMaxAge.Size = new System.Drawing.Size(100, 20);
      this.txtMaxAge.TabIndex = 16;
      this.txtMaxAge.Text = "30";
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
      this.label1.Location = new System.Drawing.Point(3, 0);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(144, 26);
      this.label1.TabIndex = 0;
      this.label1.Text = "Source Service";
      this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // txtSourceService
      // 
      this.txtSourceService.Dock = System.Windows.Forms.DockStyle.Fill;
      this.txtSourceService.Location = new System.Drawing.Point(153, 3);
      this.txtSourceService.Name = "txtSourceService";
      this.txtSourceService.Size = new System.Drawing.Size(541, 20);
      this.txtSourceService.TabIndex = 1;
      this.txtSourceService.Text = "http://tdlnamibia.ddns.net/IFMS.Master.Imberbe/AfxService.svc\r\n";
      // 
      // txtDestinationService
      // 
      this.txtDestinationService.Dock = System.Windows.Forms.DockStyle.Fill;
      this.txtDestinationService.Location = new System.Drawing.Point(153, 29);
      this.txtDestinationService.Name = "txtDestinationService";
      this.txtDestinationService.Size = new System.Drawing.Size(541, 20);
      this.txtDestinationService.TabIndex = 5;
      this.txtDestinationService.Text = "http://indilinga01.cloudapp.net/AfxImberbe/AfxService.svc\r\n";
      // 
      // label6
      // 
      this.label6.AutoSize = true;
      this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
      this.label6.Location = new System.Drawing.Point(3, 26);
      this.label6.Name = "label6";
      this.label6.Size = new System.Drawing.Size(144, 26);
      this.label6.TabIndex = 4;
      this.label6.Text = "Destination Service";
      this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // label7
      // 
      this.label7.AutoSize = true;
      this.label7.Dock = System.Windows.Forms.DockStyle.Fill;
      this.label7.Location = new System.Drawing.Point(3, 52);
      this.label7.Name = "label7";
      this.label7.Size = new System.Drawing.Size(144, 26);
      this.label7.TabIndex = 8;
      this.label7.Text = "Username";
      this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lblPassword
      // 
      this.lblPassword.AutoSize = true;
      this.lblPassword.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lblPassword.Location = new System.Drawing.Point(3, 78);
      this.lblPassword.Name = "lblPassword";
      this.lblPassword.Size = new System.Drawing.Size(144, 26);
      this.lblPassword.TabIndex = 10;
      this.lblPassword.Text = "Password";
      this.lblPassword.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // txtUsername
      // 
      this.txtUsername.Dock = System.Windows.Forms.DockStyle.Fill;
      this.txtUsername.Location = new System.Drawing.Point(153, 55);
      this.txtUsername.Name = "txtUsername";
      this.txtUsername.Size = new System.Drawing.Size(541, 20);
      this.txtUsername.TabIndex = 9;
      this.txtUsername.Text = "HeinA";
      // 
      // txtPassword
      // 
      this.txtPassword.Dock = System.Windows.Forms.DockStyle.Fill;
      this.txtPassword.Location = new System.Drawing.Point(153, 81);
      this.txtPassword.Name = "txtPassword";
      this.txtPassword.PasswordChar = '*';
      this.txtPassword.Size = new System.Drawing.Size(541, 20);
      this.txtPassword.TabIndex = 11;
      // 
      // label8
      // 
      this.label8.AutoSize = true;
      this.label8.Dock = System.Windows.Forms.DockStyle.Fill;
      this.label8.Location = new System.Drawing.Point(3, 130);
      this.label8.Name = "label8";
      this.label8.Size = new System.Drawing.Size(144, 26);
      this.label8.TabIndex = 17;
      this.label8.Text = "Source Cache Items";
      this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // label9
      // 
      this.label9.AutoSize = true;
      this.label9.Dock = System.Windows.Forms.DockStyle.Fill;
      this.label9.Location = new System.Drawing.Point(3, 156);
      this.label9.Name = "label9";
      this.label9.Size = new System.Drawing.Size(144, 26);
      this.label9.TabIndex = 18;
      this.label9.Text = "Destination Cache Items";
      this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // txtSourceCacheItems
      // 
      this.txtSourceCacheItems.Location = new System.Drawing.Point(153, 133);
      this.txtSourceCacheItems.Name = "txtSourceCacheItems";
      this.txtSourceCacheItems.ReadOnly = true;
      this.txtSourceCacheItems.Size = new System.Drawing.Size(100, 20);
      this.txtSourceCacheItems.TabIndex = 19;
      // 
      // txtDestinationCacheItems
      // 
      this.txtDestinationCacheItems.Location = new System.Drawing.Point(153, 159);
      this.txtDestinationCacheItems.Name = "txtDestinationCacheItems";
      this.txtDestinationCacheItems.ReadOnly = true;
      this.txtDestinationCacheItems.Size = new System.Drawing.Size(100, 20);
      this.txtDestinationCacheItems.TabIndex = 20;
      // 
      // panel1
      // 
      this.panel1.Controls.Add(this.cmdSyncDocuments);
      this.panel1.Controls.Add(this.cmdSyncCache);
      this.panel1.Controls.Add(this.cmdCompare);
      this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.panel1.Location = new System.Drawing.Point(0, 683);
      this.panel1.Name = "panel1";
      this.panel1.Size = new System.Drawing.Size(697, 44);
      this.panel1.TabIndex = 1;
      // 
      // cmdSyncDocuments
      // 
      this.cmdSyncDocuments.Location = new System.Drawing.Point(191, 7);
      this.cmdSyncDocuments.Name = "cmdSyncDocuments";
      this.cmdSyncDocuments.Size = new System.Drawing.Size(114, 23);
      this.cmdSyncDocuments.TabIndex = 20;
      this.cmdSyncDocuments.Text = "Sync Documents";
      this.cmdSyncDocuments.UseVisualStyleBackColor = true;
      this.cmdSyncDocuments.Click += new System.EventHandler(this.cmdSyncDocuments_Click);
      // 
      // cmdSyncCache
      // 
      this.cmdSyncCache.Location = new System.Drawing.Point(13, 7);
      this.cmdSyncCache.Name = "cmdSyncCache";
      this.cmdSyncCache.Size = new System.Drawing.Size(91, 23);
      this.cmdSyncCache.TabIndex = 19;
      this.cmdSyncCache.Text = "Sync Cache";
      this.cmdSyncCache.UseVisualStyleBackColor = true;
      this.cmdSyncCache.Click += new System.EventHandler(this.cmdSyncCache_Click);
      // 
      // cmdCompare
      // 
      this.cmdCompare.Location = new System.Drawing.Point(110, 7);
      this.cmdCompare.Name = "cmdCompare";
      this.cmdCompare.Size = new System.Drawing.Size(75, 23);
      this.cmdCompare.TabIndex = 18;
      this.cmdCompare.Text = "Compare";
      this.cmdCompare.UseVisualStyleBackColor = true;
      this.cmdCompare.Click += new System.EventHandler(this.cmdCompare_Click);
      // 
      // listView1
      // 
      this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.chDocument,
            this.chSourceRevision,
            this.chDestinationRevision,
            this.chMessage});
      this.listView1.Dock = System.Windows.Forms.DockStyle.Fill;
      this.listView1.Location = new System.Drawing.Point(0, 182);
      this.listView1.Name = "listView1";
      this.listView1.Size = new System.Drawing.Size(697, 501);
      this.listView1.TabIndex = 17;
      this.listView1.UseCompatibleStateImageBehavior = false;
      this.listView1.View = System.Windows.Forms.View.Details;
      // 
      // chDocument
      // 
      this.chDocument.Text = "Document";
      this.chDocument.Width = 100;
      // 
      // chSourceRevision
      // 
      this.chSourceRevision.Text = "Source Revision";
      this.chSourceRevision.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
      this.chSourceRevision.Width = 100;
      // 
      // chDestinationRevision
      // 
      this.chDestinationRevision.Text = "Destination Revision";
      this.chDestinationRevision.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
      this.chDestinationRevision.Width = 125;
      // 
      // chMessage
      // 
      this.chMessage.Text = "Message";
      this.chMessage.Width = 300;
      // 
      // MainForm
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(697, 727);
      this.Controls.Add(this.listView1);
      this.Controls.Add(this.panel1);
      this.Controls.Add(this.tableLayoutPanel1);
      this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.Name = "MainForm";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
      this.Text = "Sync Tool";
      this.tableLayoutPanel1.ResumeLayout(false);
      this.tableLayoutPanel1.PerformLayout();
      this.panel1.ResumeLayout(false);
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
    private System.Windows.Forms.Panel panel1;
    private System.Windows.Forms.Button cmdCompare;
    private System.Windows.Forms.ListView listView1;
    private System.Windows.Forms.ColumnHeader chDocument;
    private System.Windows.Forms.ColumnHeader chSourceRevision;
    private System.Windows.Forms.ColumnHeader chDestinationRevision;
    private System.Windows.Forms.Label label5;
    private System.Windows.Forms.TextBox txtMaxAge;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.TextBox txtSourceService;
    private System.Windows.Forms.TextBox txtDestinationService;
    private System.Windows.Forms.Label label6;
    private System.Windows.Forms.Label label7;
    private System.Windows.Forms.Label lblPassword;
    private System.Windows.Forms.TextBox txtUsername;
    private System.Windows.Forms.TextBox txtPassword;
    private System.Windows.Forms.Button cmdSyncCache;
    private System.Windows.Forms.Label label8;
    private System.Windows.Forms.Label label9;
    private System.Windows.Forms.TextBox txtSourceCacheItems;
    private System.Windows.Forms.TextBox txtDestinationCacheItems;
    private System.Windows.Forms.Button cmdSyncDocuments;
    private System.Windows.Forms.ColumnHeader chMessage;
  }
}


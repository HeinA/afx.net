﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Afx.Codesmith
{
  public class BusinessObjectHelper
  {
    public static string GetBaseClass(string ownerType)
    {
      if (!string.IsNullOrWhiteSpace(ownerType)) return string.Format("BusinessObject<{0}>", ownerType);
      return "BusinessObject";
    }

    public static string GetOwnerColumnProperty(string ownerType)
    {
      if (!string.IsNullOrWhiteSpace(ownerType)) return string.Format(", OwnerColumn = \"{0}\"", ownerType);
      return string.Empty;
    }
  }
}

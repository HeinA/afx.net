﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Afx.Codesmith
{
  public class DocumentHelper
  {
    public static string GetDocumentTypeName(Guid identifier, string connectionString)
    {
      string sql = @"select	replace(DT.Name, ' ', '') as TypeName
                      ,		replace(DTB.Name, ' ', '') as BaseName
                      ,		DTS.GlobalIdentifier as InitalState
                      from Afx.DocumentType DT
                      left outer join Afx.DocumentType DTB on DT.BaseDocument=DTB.Id
                      left outer join Afx.DocumentTypeState DTS on DT.InitialState=DTS.Id
                      where DT.GlobalIdentifier = @di";

      using (System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(connectionString))
      {
        con.Open();

        using (System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand(sql, con))
        {
          cmd.Parameters.AddWithValue("@di", identifier);
          System.Data.DataSet ds = DataHelper.ExecuteDataSet(cmd);
          if (ds.Tables[0].Rows.Count != 1) throw new ArgumentException("Invalid Document!");
          object o = ds.Tables[0].Rows[0]["TypeName"];
          if (o == DBNull.Value) return null;
          return (string)o;
        }
      }
    }

    public static bool IsAbstract(Guid identifier, string connectionString)
    {
      string sql = @"select	IsAbstract
                      from Afx.DocumentType DT
                      where DT.GlobalIdentifier = @di";

      using (System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(connectionString))
      {
        con.Open();

        using (System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand(sql, con))
        {
          cmd.Parameters.AddWithValue("@di", identifier);
          return (bool)cmd.ExecuteScalar();
        }
      }
    }

    public static string GetDocumentTypeBaseName(Guid identifier, string connectionString)
    {
      string sql = @"select	replace(DT.Name, ' ', '') as TypeName
                      ,		replace(DTB.Name, ' ', '') as BaseName
                      ,		DTS.GlobalIdentifier as InitalState
                      from Afx.DocumentType DT
                      left outer join Afx.DocumentType DTB on DT.BaseDocument=DTB.Id
                      left outer join Afx.DocumentTypeState DTS on DT.InitialState=DTS.Id
                      where DT.GlobalIdentifier = @di";

      using (System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(connectionString))
      {
        con.Open();

        using (System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand(sql, con))
        {
          cmd.Parameters.AddWithValue("@di", identifier);
          System.Data.DataSet ds = DataHelper.ExecuteDataSet(cmd);
          if (ds.Tables[0].Rows.Count != 1) throw new ArgumentException("Invalid Document!");
          object o = ds.Tables[0].Rows[0]["BaseName"];
          if (o == DBNull.Value) return null;
          return (string)o;
        }
      }
    }

    public static System.Data.DataSet GetStates(Guid identifier, string connectionString)
    {
      string sql = @"select	replace(replace(DTS.Name, ' ', ''), '-', '') as Name
                    ,		DTS.GlobalIdentifier
                    from Afx.DocumentTypeState DTS
                    inner join Afx.DocumentType DT on DT.Id=DTS.DocumentType
                    where DT.GlobalIdentifier = @di

                    union all

                    select	replace(replace(DTS.Name, ' ', ''), '-', '') as Name
                    ,		DTS.GlobalIdentifier
                    from Afx.DocumentTypeState DTS
                    inner join Afx.DocumentType DT on DT.BaseDocument=DTS.DocumentType
                    where DT.GlobalIdentifier = @di";

      using (System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(connectionString))
      {
        con.Open();

        using (System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand(sql, con))
        {
          cmd.Parameters.AddWithValue("@di", identifier);
          System.Data.DataSet ds = DataHelper.ExecuteDataSet(cmd);
          return ds;
        }
      }
    }
  }
}

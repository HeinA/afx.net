﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Afx.Codesmith
{
  public class ActivityHelper
  {
    public static System.Data.DataSet GetOperations(Guid identifier, string connectionString)
    {
      string sql = @"select	distinct OperationName
                    ,		[Operation]
                    from Afx.Activity A
                    inner join (
	                    select	Activity
	                    ,		O.GlobalIdentifier as [Operation]
	                    ,		replace(O.Text, ' ', '') as OperationName
	                    ,		G.GlobalIdentifier as [Group]
	                    ,		G.GroupName
	                    from	Afx.ActivityGlobalOperation	OG
	                    inner join Afx.Operation O on O.Id=OG.GlobalOperation
	                    inner join Afx.OperationGroup G on G.Id=O.[Group]
	
	                    union all
	
	                    select	distinct ContextualActivity
	                    ,		O.GlobalIdentifier as [Operation]
	                    ,		replace(O.Text, ' ', '') as OperationName
	                    ,		G.GlobalIdentifier as [Group]
	                    ,		G.GroupName
	                    from	Afx.ContextualActivityContextOperation OC
	                    inner join Afx.Operation O on O.Id=OC.ContextOperation
	                    inner join Afx.OperationGroup G on G.Id=O.[Group]
	                    ) as O on O.Activity=A.id and O.[Group] <> '28E80BCC-2A67-4C9A-91E3-4217414F2AD0'
                    where A.GlobalIdentifier = @ai";

      using (System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(connectionString))
      {
        con.Open();

        using (System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand(sql, con))
        {
          cmd.Parameters.AddWithValue("@ai", identifier);
          System.Data.DataSet ds = DataHelper.ExecuteDataSet(cmd);
          return ds;
        }
      }
    }
  }
}

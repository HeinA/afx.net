﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Afx.Codesmith
{
  public class NamespaceHelper
  {
    NamespaceHelper(string defaultNamespace)
    {
      DefaultNamespace = defaultNamespace;
    }

    #region string DefaultNamespace

    string mDefaultNamespace;
    public string DefaultNamespace
    {
      get { return mDefaultNamespace; }
      private set { mDefaultNamespace = value; }
    }

    #endregion

    #region static NamespaceHelper Instance

    static NamespaceHelper mInstance;
    public static NamespaceHelper Instance
    {
      get { return NamespaceHelper.mInstance; }
      private set { NamespaceHelper.mInstance = value; }
    }

    #endregion

    public static void Initialize(string defaultNamespace)
    {
      Instance = new NamespaceHelper(defaultNamespace);
    }

    public string GetFolder(string ns)
    {
      string s = ns.Replace(DefaultNamespace, string.Empty).Replace(".", @"\");
      s = s.Trim('\\');
      if (string.IsNullOrWhiteSpace(s)) return string.Empty;
      return string.Format(@"{0}\", s);
    }

    public string GetUriFolder(string ns)
    {
      string s = ns.Replace(DefaultNamespace, string.Empty).Replace(".", @"/");
      s = s.Trim('/');
      if (string.IsNullOrWhiteSpace(s)) return string.Empty;
      return string.Format(@"{0}/", s);
    }

    public string ModuleName
    {
      get { return DefaultNamespace.Split(new char[] { '.' }, StringSplitOptions.RemoveEmptyEntries)[0]; }
    }
  }
}

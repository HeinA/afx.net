﻿using Afx.Business;
using Afx.Business.Service;
using Afx.Business.ServiceModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Afx.LogViewer
{
  /// <summary>
  /// Interaction logic for MainWindow.xaml
  /// </summary>
  [ServiceBehavior(Namespace = Namespace.Afx
  , ConcurrencyMode = ConcurrencyMode.Multiple
  , InstanceContextMode = InstanceContextMode.Single)]
  public partial class MainWindow : Window, IAfxLogService, INotifyPropertyChanged
  {
    DateTime mStartTime = DateTime.Now;

    public MainWindow()
    {
      InitializeComponent();

      CollectionView view = (CollectionView)CollectionViewSource.GetDefaultView(mLogMessages);
      view.Filter = MessageSourceFilter;

      CollectionView view1 = (CollectionView)CollectionViewSource.GetDefaultView(mSqlMessages);
      view1.Filter = SqlSourceFilter;

      try
      {
        mHost = new ServiceHost(this);
        mHost.Open();
      }
      catch
      {
        throw;
      }

      string filter = null;
      if (RegistryHelper.GetRegistryValue<string>(Microsoft.Win32.RegistryHive.CurrentUser, string.Format(RegistryHelper.ApplicationKey, "Filters"), "SourceFilterString", Microsoft.Win32.RegistryValueKind.String, out filter))
      {
        if (!string.IsNullOrWhiteSpace(filter)) SourceFilterString = filter;
      }

      filter = null;
      if (RegistryHelper.GetRegistryValue<string>(Microsoft.Win32.RegistryHive.CurrentUser, string.Format(RegistryHelper.ApplicationKey, "Filters"), "ScopeInclusionFilterString", Microsoft.Win32.RegistryValueKind.String, out filter))
      {
        if (!string.IsNullOrWhiteSpace(filter)) ScopeInclusionFilterString = filter;
      }

      filter = null;
      if (RegistryHelper.GetRegistryValue<string>(Microsoft.Win32.RegistryHive.CurrentUser, string.Format(RegistryHelper.ApplicationKey, "Filters"), "ScopeExclusionFilterString", Microsoft.Win32.RegistryValueKind.String, out filter))
      {
        if (!string.IsNullOrWhiteSpace(filter)) ScopeExclusionFilterString = filter;
      }


      this.DataContext = this;
    }

    object mLock = new object();

    bool MessageSourceFilter(object item)
    {
      lock (mLock)
      {
        try
        {
          if (string.IsNullOrWhiteSpace(SourceFilterString)) return true;

          string[] filters = SourceFilterString.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
          foreach (string s in filters)
          {
            if (((LogMessage)item).Source.Equals(s.Trim(), StringComparison.CurrentCultureIgnoreCase)) return true;
          }
          return false;
        }
        catch (Exception ex)
        {
          using (StreamWriter sw = new StreamWriter(@"C:\Logs\LogViewer.txt", true))
          {
            sw.WriteLine(ex);
          }
          throw;
        }
      }
    }

    bool SqlSourceFilter(object item)
    {
      lock (mLock)
      {
        try
        {
          string[] sourceInclusionFilters = SourceFilterString.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
          string[] scopeInclusionFilters = ScopeInclusionFilterString.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
          string[] scopeExclusionFilters = ScopeExclusionFilterString.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);

          if (!string.IsNullOrWhiteSpace(SourceFilterString))
          {
            foreach (string s in sourceInclusionFilters)
            {
              if (((SqlMessage)item).Source.Equals(s.Trim(), StringComparison.CurrentCultureIgnoreCase))
              {
                foreach (string s1 in scopeExclusionFilters)
                {
                  if (((SqlMessage)item).Scope.Equals(s1.Trim(), StringComparison.CurrentCultureIgnoreCase)) return false;
                }

                if (string.IsNullOrWhiteSpace(ScopeInclusionFilterString)) return true;

                foreach (string s1 in scopeInclusionFilters)
                {
                  if (((SqlMessage)item).Scope.Equals(s1.Trim(), StringComparison.CurrentCultureIgnoreCase)) return true;
                }
              }
            }

            return false;
          }

          foreach (string s1 in scopeExclusionFilters)
          {
            if (((SqlMessage)item).Scope.Equals(s1.Trim(), StringComparison.CurrentCultureIgnoreCase)) return false;
          }

          if (string.IsNullOrWhiteSpace(ScopeInclusionFilterString)) return true;

          foreach (string s1 in scopeInclusionFilters)
          {
            if (((SqlMessage)item).Scope.Equals(s1.Trim(), StringComparison.CurrentCultureIgnoreCase)) return true;
          }

          return false;
        }
        catch(Exception ex)
        {
          using (StreamWriter sw = new StreamWriter(@"C:\Logs\LogViewer.txt", true))
          {
            sw.WriteLine(ex);
          }
          throw;
        }
      }
    }

    #region string SourceFilterString

    string mSourceFilterString = string.Empty;
    public string SourceFilterString
    {
      get { return mSourceFilterString ?? string.Empty; }
      set
      {
        if (SetProperty<string>(ref mSourceFilterString, value))
        {
          RegistryHelper.SetRegistryValue<string>(Microsoft.Win32.RegistryHive.CurrentUser, string.Format(RegistryHelper.ApplicationKey, "Filters"), "SourceFilterString", Microsoft.Win32.RegistryValueKind.String, mSourceFilterString);
          RefreshLogMessages();
          RefreshSqlMessages();
        }
      }
    }

    #endregion

    #region string ScopeInclusionFilterString

    string mScopeInclusionFilterString = string.Empty;
    public string ScopeInclusionFilterString
    {
      get { return mScopeInclusionFilterString ?? string.Empty; }
      set
      {
        if (SetProperty<string>(ref mScopeInclusionFilterString, value))
        {
          RegistryHelper.SetRegistryValue<string>(Microsoft.Win32.RegistryHive.CurrentUser, string.Format(RegistryHelper.ApplicationKey, "Filters"), "ScopeInclusionFilterString", Microsoft.Win32.RegistryValueKind.String, mScopeInclusionFilterString);
          RefreshSqlMessages();
        }
      }
    }

    #endregion

    #region string ScopeExclusionFilterString

    string mScopeExclusionFilterString = string.Empty;
    public string ScopeExclusionFilterString
    {
      get { return mScopeExclusionFilterString ?? string.Empty; }
      set
      {
        if (SetProperty<string>(ref mScopeExclusionFilterString, value))
        {
          RegistryHelper.SetRegistryValue<string>(Microsoft.Win32.RegistryHive.CurrentUser, string.Format(RegistryHelper.ApplicationKey, "Filters"), "ScopeExclusionFilterString", Microsoft.Win32.RegistryValueKind.String, mScopeExclusionFilterString);
          RefreshSqlMessages();
        }
      }
    }

    #endregion

    #region SqlMessage SelectedMessage

    SqlMessage mSelectedMessage;
    public SqlMessage SelectedMessage
    {
      get { return mSelectedMessage; }
      set
      {
        if (SetProperty<SqlMessage>(ref mSelectedMessage, value))
        {
        }
      }
    }

    #endregion

    ServiceHost mHost;
    public ServiceHost Host
    {
      get { return mHost; }
    }

    void RefreshLogMessages()
    {
      ICollectionView cv = CollectionViewSource.GetDefaultView(mLogMessages);
      if (cv != null) cv.Refresh();
    }

    void RefreshSqlMessages()
    {
      ICollectionView cv = CollectionViewSource.GetDefaultView(mSqlMessages);
      if (cv != null) cv.Refresh();
    }

    [SuppressAuthentication]
    public void LogMessage(string source, DateTime timestamp, string message)
    {
      lock (mLock)
      {
        mLogMessages.Add(new LogMessage(source, timestamp, message));
        RefreshLogMessages();
      }
    }

    [SuppressAuthentication]
    public void LogSql(string source, string scope, DateTime timestamp, string message, SqlLogParameter[] parameters)
    {
      lock (mLock)
      {
        mSqlMessages.Add(new SqlMessage(source, scope, timestamp, message, parameters));
        RefreshSqlMessages();
      }
    }

    ObservableCollection<LogMessage> mLogMessages = new ObservableCollection<LogMessage>();
    public IEnumerable<LogMessage> LogMessages
    {
      get { return mLogMessages; } 
    }


    ObservableCollection<SqlMessage> mSqlMessages = new ObservableCollection<SqlMessage>();
    public IEnumerable<SqlMessage> SqlMessages
    {
      get { return mSqlMessages; }
    }

    protected override void OnClosed(EventArgs e)
    {
      mHost.Close();
      base.OnClosed(e);
    }

    #region INotifyPropertyChanged

    public event PropertyChangedEventHandler PropertyChanged;

    protected virtual void OnPropertyChanged(string propertyName)
    {
      if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
    }

    /// <summary>
    /// Checks if a property already matches a desired value. Sets the property and
    /// notifies listeners only when necessary.
    /// </summary>
    /// <typeparam name="T">Type of the property.</typeparam>
    /// <param name="storage">Reference to a property with both getter and setter.</param>
    /// <param name="value">Desired value for the property.</param>
    /// <param name="propertyName">Name of the property used to notify listeners. This
    /// value is optional and can be provided automatically when invoked from compilers that
    /// support CallerMemberName.</param>
    /// <returns>True if the value was changed, false if the existing value matched the
    /// desired value.</returns>
    protected virtual bool SetProperty<T>(ref T storage, T value, [System.Runtime.CompilerServices.CallerMemberName] string propertyName = null)
    {
      if (typeof(T).IsValueType && Object.Equals(storage, value)) return false;
      else if ((object)storage == (object)value) return false;

      storage = value;
      this.OnPropertyChanged(propertyName);

      return true;
    }

    #endregion

    private void Clear_Click(object sender, RoutedEventArgs e)
    {
      mLogMessages.Clear();
      mSqlMessages.Clear();
    }
  }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.LogViewer
{
  public class LogMessage : BindableBase
  {
    public LogMessage(string source, DateTime timestamp, string message)
    {
      Source = source;
      Timestamp = timestamp;
      Message = message;
    }

    #region string Source

    string mSource;
    public string Source
    {
      get { return mSource; }
      set { SetProperty<string>(ref mSource, value); }
    }

    #endregion

    #region DateTime Timestamp

    DateTime mTimestamp;
    public DateTime Timestamp
    {
      get { return mTimestamp; }
      set { SetProperty<DateTime>(ref mTimestamp, value); }
    }

    #endregion

    #region string Message

    string mMessage;
    public string Message
    {
      get { return mMessage; }
      set { SetProperty<string>(ref mMessage, value); }
    }

    #endregion
  }
}

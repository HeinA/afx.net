﻿using Afx.Business;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.LogViewer
{
  public class SqlMessage : BindableBase
  {
    public SqlMessage(string source, string scope, DateTime timestamp, string message, SqlLogParameter[] parameters)
    {
      Source = source;
      Scope = scope;
      Timestamp = timestamp;
      Message = message;
      mParameters = parameters;
    }

    #region string Source

    string mSource;
    public string Source
    {
      get { return mSource; }
      set { SetProperty<string>(ref mSource, value ?? string.Empty); }
    }

    #endregion

    #region string Scope

    string mScope;
    public string Scope
    {
      get { return mScope; }
      set { SetProperty<string>(ref mScope, value ?? string.Empty); }
    }

    #endregion

    #region DateTime Timestamp

    DateTime mTimestamp;
    public DateTime Timestamp
    {
      get { return mTimestamp; }
      set { SetProperty<DateTime>(ref mTimestamp, value); }
    }

    #endregion

    #region string Message

    string mMessage;
    public string Message
    {
      get { return mMessage; }
      set { SetProperty<string>(ref mMessage, ReformatMessage(value)); }
    }

    #endregion

    public string MessageLine
    {
      get { return Message.Replace('\n', ' '); }
    }

    #region SqlLogParameter[] Parameters

    SqlLogParameter[] mParameters;
    public IEnumerable<SqlLogParameter> Parameters
    {
      get { return mParameters; }
    }

    #endregion

    string ReformatMessage(string message)
    {
      if (string.IsNullOrWhiteSpace(message)) return string.Empty;
      Collection<string> trimmedLines = new Collection<string>();
      foreach (string s in message.Split(new string[] { "\n" }, StringSplitOptions.RemoveEmptyEntries))
      {
        if (string.IsNullOrWhiteSpace(s)) continue;
        trimmedLines.Add(s.Trim());
      }
      return string.Join("\n", trimmedLines);
    }
  }
}

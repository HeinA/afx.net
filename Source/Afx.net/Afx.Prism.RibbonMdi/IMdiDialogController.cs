﻿using Afx.Business;
using Afx.Prism.RibbonMdi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi
{
  public interface IMdiDialogController : IViewController
  {
    IMdiApplicationController ApplicationController { get; }
    bool Validate();
    void OnApply();
    void OnOk();
  }
}

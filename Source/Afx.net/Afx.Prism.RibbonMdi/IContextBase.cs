﻿using Afx.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi
{
  public interface IContextBase : IBusinessObject
  {
    string Title { get; }
    string TitleText { get; }
    string ContextIdentifier { get; }
    void LoadData();
    void SaveData();
  }
}

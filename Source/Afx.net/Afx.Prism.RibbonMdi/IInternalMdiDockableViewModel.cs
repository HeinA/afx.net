﻿using Afx.Business;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi
{
  internal interface IInternalMdiDockableViewModel
  {
    void OnLoaded();
  }
}

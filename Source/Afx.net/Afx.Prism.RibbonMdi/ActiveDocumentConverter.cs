﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace Afx.Prism.RibbonMdi
{
  class ActiveDocumentConverter : IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      if (value == null) return Binding.DoNothing;
      if (typeof(IMdiDockableDocumentViewModel).IsAssignableFrom(value.GetType())) return value;
      return Binding.DoNothing;
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      if (value == null) return Binding.DoNothing;
      if (typeof(IMdiDockableDocumentViewModel).IsAssignableFrom(value.GetType())) return value;
      return Binding.DoNothing;
    }
  }
}

﻿using Afx.Business.Security;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace Afx.Prism.RibbonMdi.Login
{
  class SessionExpiredController : MdiDialogController<LoginCredentials, SessionExpiredViewModel>
  {
    public const string SessionExpiredControllerKey = "Ribbon Mdi Login Controller";

    public SessionExpiredController(IController controller)
      : base(SessionExpiredControllerKey, controller)
    {
    }

    RetryAction mRetryAction;
    public RetryAction RetryAction
    {
      get { return mRetryAction; }
      set { mRetryAction = value; }
    }

    [Dependency]
    public RibbonMdiModuleController ModuleController { get; set; }

    protected override bool OnRun()
    {
      DataContext = new LoginCredentials() { Username = SecurityContext.User.Username };
      ViewModel.Caption = "Login";
      return base.OnRun();
    }

    protected override void ApplyChanges()
    {
      SecurityContext.RemoveAuthentication(SecurityContext.SessionToken);
      SecurityContext.AddAuthentication(DataContext.AuthenticationResult);
      if (RetryAction != null) RetryAction.Retry();
      base.ApplyChanges();
    }
  }
}

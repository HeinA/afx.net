﻿using Afx.Business;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Business.Service;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace Afx.Prism.RibbonMdi.Login
{
  public class LoginController : MdiDialogController<LoginCredentials, LoginViewModel>
  {
    public const string LoginControllerKey = "Ribbon Mdi Login Controller";

    public LoginController(IController controller)
      : base(LoginControllerKey, controller)
    {
    }

    [Dependency]
    public RibbonMdiModuleController ModuleController { get; set; }

    protected override bool OnRun()
    {
      DataContext = new LoginCredentials();
      ViewModel.Caption = "Login";

      //using (var svc = ProxyFactory.GetService<IAfxService>(DefaultServer.ServerName))
      //{
      //  ViewModel.OrganizationalUnits = svc.LoadLoginOrganizationalUnits();
      //}

      return base.OnRun();
    }

    protected override void ApplyChanges()
    {
      if (DataContext.AuthenticationResult != null)
      {
        ApplicationController.Dispatcher.BeginInvoke(new Action(() =>
        {
          try
          {
            ModuleController.LoginUser(DataContext.AuthenticationResult);
          }
          catch
          {
#if DEBUG
            if (System.Diagnostics.Debugger.IsAttached) System.Diagnostics.Debugger.Break();
#endif
          }
        }), DispatcherPriority.Normal);
      }

      base.ApplyChanges();
    }
  }
}

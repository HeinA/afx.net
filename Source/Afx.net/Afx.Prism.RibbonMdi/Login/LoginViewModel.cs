﻿using Afx.Business.Security;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Login
{
  public class LoginViewModel : MdiDialogViewModel<LoginCredentials>
  {
    [InjectionConstructor]
    public LoginViewModel(IController controller)
      : base(controller)
    {
    }

    //#region Model Propertry string Username

    //public string Username
    //{
    //  get
    //  {
    //    if (Model == null) return default(string);
    //    return Model.Username;
    //  }
    //  set { Model.Username = value; }
    //}

    //#endregion

    //#region Model Propertry string PasswordHash

    //public string PasswordHash
    //{
    //  get
    //  {
    //    if (Model == null) return default(string);
    //    return Model.PasswordHash;
    //  }
    //  set { Model.PasswordHash = value; }
    //}

    //#endregion

    //#region string Server

    //public string Server
    //{
    //  get
    //  {
    //    if (Model == null) return GetDefaultValue<string>();
    //    return Model.Server;
    //  }
    //  set { Model.Server = value; }
    //}

    //#endregion

    //public IEnumerable<string> Servers
    //{
    //  get { return ConfigurationManager.AppSettings["Servers"].Split(','); }
    //}

    //#region Collection<LoginOrganizationalUnit> OrganizationalUnits

    //public const string OrganizationalUnitsProperty = "OrganizationalUnits";
    //Collection<LoginOrganizationalUnit> mOrganizationalUnits;
    //public Collection<LoginOrganizationalUnit> OrganizationalUnits
    //{
    //  get { return mOrganizationalUnits; }
    //  set { SetProperty<Collection<LoginOrganizationalUnit>>(ref mOrganizationalUnits, value); }
    //}

    //#endregion

    //#region LoginOrganizationalUnit SelectedOrganizationalUnit

    //public const string SelectedOrganizationalUnitProperty = "SelectedOrganizationalUnit";
    //LoginOrganizationalUnit mSelectedOrganizationalUnit;
    //public LoginOrganizationalUnit SelectedOrganizationalUnit
    //{
    //  get { return mSelectedOrganizationalUnit; }
    //  set { SetProperty<LoginOrganizationalUnit>(ref mSelectedOrganizationalUnit, value); }
    //}

    //#endregion
  }
}

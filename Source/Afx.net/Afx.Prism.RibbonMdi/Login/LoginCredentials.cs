﻿using Afx.Business;
using Afx.Business.Security;
using Afx.Business.Service;
using Afx.Business.ServiceModel;
using Afx.Business.Validation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Login
{
  public class LoginCredentials : BusinessObject
  {
    #region DataMember string Username

    public const string UsernameProperty = "Username";
    string mUsername;
    public string Username
    {
      get { return mUsername; }
      set { SetProperty<string>(ref mUsername, value, UsernameProperty); }
    }

    #endregion

    #region DataMember string PasswordHash

    public const string PasswordHashProperty = "PasswordHash";
    string mPasswordHash;
    public string PasswordHash
    {
      get { return mPasswordHash; }
      set { SetProperty<string>(ref mPasswordHash, value, PasswordHashProperty); }
    }

    #endregion

    #region string Server

    public const string ServerProperty = "Server";
    string mServer;
    [Mandatory("Server is a mandatory field.")]
    public string Server
    {
      get { return mServer; }
      set { SetProperty<string>(ref mServer, value); }
    }

    #endregion

    #region DataMember AuthenticationResult AuthenticationResult

    public const string AuthenticationResultProperty = "AuthenticationResult";
    AuthenticationResult mAuthenticationResult;
    public AuthenticationResult AuthenticationResult
    {
      get { return mAuthenticationResult; }
      set { SetProperty<AuthenticationResult>(ref mAuthenticationResult, value, AuthenticationResultProperty); }
    }

    #endregion

    protected override void GetErrors(Collection<string> errors, string propertyName)
    {
      if (propertyName == null || propertyName == UsernameProperty)
      {
        if (string.IsNullOrWhiteSpace(Username)) errors.Add("Username is a mandatory field.");
      }

      if (propertyName == null || propertyName == PasswordHashProperty)
      {
        if (string.IsNullOrWhiteSpace(PasswordHash)) errors.Add("Password is a mandatory field.");
      }

      if (errors.Count == 0)
      {
        try
        {
          using (new WaitCursor())
          {
            AuthenticationResult ar = SecurityContext.Authenticate(Server, Username, PasswordHash, true);
            if (!string.IsNullOrWhiteSpace(ar.Error)) errors.Add(ar.Error);
            else AuthenticationResult = ar;
          }
        }
        catch (Exception ex)
        {
          if (ExceptionHelper.HandleException(ex)) throw;
        }
      }

      base.GetErrors(errors, propertyName);
    }
  }
}

﻿using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Login
{
  public class SessionExpiredViewModel : MdiDialogViewModel<LoginCredentials>
  {
    [InjectionConstructor]
    public SessionExpiredViewModel(IController controller)
      : base(controller)
    {
    }

    #region Model Propertry string Username

    public string Username
    {
      get
      {
        if (Model == null) return default(string);
        return Model.Username;
      }
      set { Model.Username = value; }
    }

    #endregion

    #region Model Propertry string PasswordHash

    public string PasswordHash
    {
      get
      {
        if (Model == null) return default(string);
        return Model.PasswordHash;
      }
      set { Model.PasswordHash = value; }
    }

    #endregion
  }
}

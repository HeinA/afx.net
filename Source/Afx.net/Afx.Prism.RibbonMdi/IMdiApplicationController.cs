﻿using Afx.Business.Activities;
using Afx.Prism.RibbonMdi.Tools;
using Microsoft.Practices.Prism.PubSubEvents;
using Microsoft.Practices.Prism.Regions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi
{
  public interface IMdiApplicationController : IApplicationController
  {
    MdiApplication ApplicationContext { get; }
    void AddDialog(IMdiDialogController controller);
    void RemoveDialog(IMdiDialogController controller);
    void ExecuteActivity(Activity activity);
    //void AddActivityTabs();
    void Logout();
    void ShowLoginDialog();
    T RegisterSearchViewModel<T>()
      where T : SearchItemViewModel;
  }
}

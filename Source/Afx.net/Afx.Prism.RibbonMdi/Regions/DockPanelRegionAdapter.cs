﻿using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace Afx.Prism.RibbonMdi.Regions
{
  public class DockPanelRegionAdapter : RegionAdapterBase<DockPanel>
  {
    [InjectionConstructor]
    public DockPanelRegionAdapter(IRegionBehaviorFactory regionBehaviorFactory)
      :base(regionBehaviorFactory)
    {
    }

    Dictionary<object, FrameworkElement> mFrameworkElements = new Dictionary<object, FrameworkElement>();
    public Dictionary<object, FrameworkElement> FrameworkElements
    {
      get { return mFrameworkElements; }
    }

    protected override void Adapt(IRegion region, DockPanel regionTarget)
    {
      region.Views.CollectionChanged += (s, e) =>
      {
        if (e.Action == NotifyCollectionChangedAction.Add)
          foreach (object item in e.NewItems)
          {
            FrameworkElement element = null;
            if (item is FrameworkElement)
            {
              element = (FrameworkElement)item;
            }
            else
            {
              DataTemplateKey dtk = new DataTemplateKey(item.GetType());
              DataTemplate dt = (DataTemplate)Application.Current.FindResource(dtk);
              element = (FrameworkElement)dt.LoadContent();
              element.DataContext = item;
            }
            regionTarget.Children.Add(element);
            if (!FrameworkElements.ContainsKey(element))
            {
              element.Unloaded += Element_Unloaded;
              FrameworkElements.Add(item, element);
            }
          }

        if (e.Action == NotifyCollectionChangedAction.Remove)
          foreach (object element in e.OldItems)
          {
            if (FrameworkElements.ContainsKey(element))
            {
              FrameworkElement cw = FrameworkElements[element];
              BindingOperations.ClearAllBindings(cw);
              cw.DataContext = null;
              FrameworkElements.Remove(element);
              cw.Unloaded -= Element_Unloaded;
              regionTarget.Children.Remove(cw);
            }
          }
      };
    }

    void Element_Unloaded(object sender, RoutedEventArgs e)
    {
      FrameworkElement element = (FrameworkElement)sender;
      FrameworkElements.Remove(element);
      element.Unloaded -= Element_Unloaded;
    }

    protected override IRegion CreateRegion()
    {
      return new AllActiveRegion();
    }
  }
}

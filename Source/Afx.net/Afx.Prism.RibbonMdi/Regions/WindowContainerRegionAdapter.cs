﻿using Afx.Prism.RibbonMdi;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Xceed.Wpf.Toolkit;
using Xceed.Wpf.Toolkit.Primitives;

namespace Afx.Prism.RibbonMdi.Regions
{
  public class WindowContainerRegionAdapter : RegionAdapterBase<WindowContainer>
  {
    [InjectionConstructor]
    public WindowContainerRegionAdapter(IRegionBehaviorFactory regionBehaviorFactory)
      :base(regionBehaviorFactory)
    {
    }

    Dictionary<object, ChildWindow> mChildWindows = new Dictionary<object, ChildWindow>();
    public Dictionary<object, ChildWindow> ChildWindows
    {
      get { return mChildWindows; }
    }

    protected override void Adapt(IRegion region, WindowContainer regionTarget)
    {
      region.Views.CollectionChanged += (s, e) =>
      {
        if (e.Action == NotifyCollectionChangedAction.Add)
          foreach (object element in e.NewItems)
          {
            ChildWindow cw = null;
            if (element is ChildWindow)
            {
              cw = (ChildWindow)element;
            }
            else
            {
              DataTemplateKey dtk = new DataTemplateKey(element.GetType());
              DataTemplate dt = (DataTemplate)Application.Current.FindResource(dtk);
              cw = (ChildWindow)dt.LoadContent();
              cw.DataContext = element;
            }
            regionTarget.Children.Add(cw);
            ChildWindows.Add(element, cw);
          }

        if (e.Action == NotifyCollectionChangedAction.Remove)
          foreach (object element in e.OldItems)
          {
            if (ChildWindows.ContainsKey(element))
            {
              ChildWindow cw = ChildWindows[element];
              BindingOperations.ClearAllBindings(cw);
              cw.WindowState = Xceed.Wpf.Toolkit.WindowState.Closed;
              cw.Content = null;
              cw.DataContext = null;
              ChildWindows.Remove(element);
              regionTarget.Children.Remove(cw);
            }
          }
      };
    }

    protected override IRegion CreateRegion()
    {
      return new SingleActiveRegion();
    }
  }
}

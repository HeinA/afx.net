﻿using Afx.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi
{
  public abstract class MdiNavigationListItemViewModel<TModel> : SelectableViewModel<TModel>
    where TModel : class, IBusinessObject
  {
    protected MdiNavigationListItemViewModel(IController controller)
      : base(controller)
    {
    }

    public override bool IsSelected
    {
      get
      {
        if (base.IsSelected) ((IContextController)Controller).SetContextViewModel(this);
        return base.IsSelected;
      }
      set
      {
        base.IsSelected = value;
        if (base.IsSelected) ((IContextController)Controller).SetContextViewModel(this);
      }
    }

    public override bool IsFocused
    {
      get { return base.IsFocused; }
      set
      {
        base.IsFocused = value;
        if (value && !base.IsSelected) IsSelected = true;
      }
    }
  }
}

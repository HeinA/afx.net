﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi
{
  internal interface IInternalDockableController
  {
    void OnLoaded();
    void OnRefresh();
  }
}

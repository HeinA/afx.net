﻿using Afx.Business;
using CrystalDecisions.CrystalReports.Engine;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi
{
  public abstract class MdiReportParameterController<TModel, TViewModel> : MdiViewController<TModel, TViewModel>, IMdiReportParameterController
    where TModel : MdiReportParameterContext, new()
    where TViewModel : class, IMdiReportParameterViewModel
  {
    protected MdiReportParameterController(IController controller)
      : base(controller)
    {
    }

    protected MdiReportParameterController(string id, IController controller)
      : base(id, controller)
    {
    }

    protected override void ConfigureContainer()
    {
      Container.RegisterInstance<IMdiReportParameterController>(this);
      Container.RegisterInstance<IMdiDialogController>(this);
      base.ConfigureContainer();
    }

    public virtual bool Validate()
    {
      if (DataContext == null) return true;
      return ViewModel.Validate();
    }

    public ReportDocument ReportDocument { get; set; }
    public string Description { get; set; }

    public void OnApply()
    {
      try
      {
        if (Validate())
        {
          using (new WaitCursor())
          {
            ApplyChanges();
          }
        }
      }
      catch (Exception ex)
      {
        if (ExceptionHelper.HandleException(ex)) throw;
      }
    }

    #region bool CanClose

    bool mCanClose = true;
    public bool CanClose
    {
      get { return mCanClose; }
      set { mCanClose = value; }
    }

    #endregion

    public void OnOk()
    {
      try
      {
        if (Validate())
        {
          using (new WaitCursor())
          {
            ApplyChanges();
          }
          if (CanClose)
          {
            Task.Run(() =>
            {
              try
              {
                try
                {
                  string fn = string.Format("{0}{1} - {2:yyyy.MM.dd.HH.mm.ss}.pdf", Path.GetTempPath(), ReportDocument.SummaryInfo.ReportTitle, DateTime.Now);
                  ReportDocument.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, fn);
                  Process.Start(fn);
                }
                catch (ArgumentException)
                {
                  string fn = string.Format("{0}{1}.pdf", Path.GetTempPath(), Guid.NewGuid());
                  ReportDocument.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, fn);
                  Process.Start(fn);
                }
              }
              catch (Exception ex)
              {
                if (ExceptionHelper.HandleException(ex)) throw;
              }
            });
            Terminate();
          }
        }
      }
      catch (Exception ex)
      {
        if (ExceptionHelper.HandleException(ex)) throw;
      }
    }

    protected virtual void ApplyChanges()
    {
    }

    protected override bool OnRun()
    {
      DataContext = new TModel();
      ViewModel.Caption = ReportDocument.SummaryInfo.ReportTitle;
      DataContext.Description = ReportDocument.SummaryInfo.ReportComments;
      ApplicationController.AddDialog(this);
      ApplicationController.RegionManager.Regions[MdiApplication.DialogRegion].Add(ViewModel);
      return base.OnRun();
    }

    public override void Terminate()
    {
      IRegion region = ApplicationController.RegionManager.Regions[MdiApplication.DialogRegion];
      if (ViewModel != null && region.Views.Contains(ViewModel)) region.Remove(ViewModel);
      ApplicationController.RemoveDialog(this);

      base.Terminate();
    }
  }
}

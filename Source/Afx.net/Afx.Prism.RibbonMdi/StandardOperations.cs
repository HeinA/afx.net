﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi
{
  public class StandardOperations
  {
    public const string Save = "{efc8eaa5-9889-4a08-a215-e4c5e3642914}";
    public const string ToggleDelete = "{96890b9e-7562-4124-bd08-aa980b37ff8f}";
    public const string Refresh = "{96f474ad-e623-46bb-9996-76dd1b07b742}";
    public const string Print = "{98dbdedb-2dd1-437d-a978-0592f0ee0e64}";
    public const string Print_Crystal = "{11fa9b35-3dad-4c94-abd4-456894f0293a}";
    public const string PrintPreview = "{a90ed5f1-915c-4ee5-b95c-d04c71f456d1}";
    public const string Transfer = "{94ebde37-70fb-41eb-a61c-97458c756e4d}";
    public const string CaptureReferences = "{3ca2d311-c366-4198-acca-f4d0c318a7a2}";
    public const string AttachFile = "{a5087204-80ff-470e-8982-874143533c2a}";
    public const string EditFlags = "{bd074028-e82c-4296-a7d2-b3183ef133d9}";
  }
}

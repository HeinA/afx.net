﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xceed.Wpf.AvalonDock.Layout;

namespace Afx.Prism.RibbonMdi
{
  public enum DockType
  {
    Document,
    Side,
    Grouped
  }

  public enum GroupType
  {
    Left,
    Right,
    Top,
    Bottom,
    Inside
  }

  public enum DockSide
  {
    Left,
    Right,
    Top,
    Bottom
  }

  public class DockingStrategy
  {
    DockingStrategy(object view)
    {
      if (!view.GetType().IsSubclassOf(typeof(LayoutDocument))) throw new ArgumentException("View must be a subclass of LayoutDocument.");

      DockType = DockType.Document;
      View = view;
    }

    DockingStrategy(DockType dockType, DockSide side, object view)
    {
      if (!view.GetType().IsSubclassOf(typeof(LayoutAnchorable))) throw new ArgumentException("View must be a subclass of LayoutAnchorable.");

      if (dockType != DockType.Side) throw new InvalidOperationException("Invalid DockType.");
      DockType = dockType;
      DockSide = side;
      View = view;
    }

    DockingStrategy(DockType dockType, GroupType position, object targetView, object view)
    {
      if (!view.GetType().IsSubclassOf(typeof(LayoutAnchorable))) throw new ArgumentException("View must be a subclass of LayoutAnchorable.");
      if (!targetView.GetType().IsSubclassOf(typeof(LayoutAnchorable))) throw new ArgumentException("TargetView must be a subclass of LayoutAnchorable.");

      if (dockType != DockType.Grouped) throw new InvalidOperationException("Invalid DockType.");
      DockType = dockType;
      GroupType = position;
      TargetView = targetView;
      View = view;
    }

    DockType mDockType;
    public DockType DockType
    {
      get { return mDockType; }
      private set { mDockType = value; }
    }

    GroupType mGroupType;
    public GroupType GroupType
    {
      get { return mGroupType; }
      private set { mGroupType = value; }
    }

    DockSide mDockSide;

    public DockSide DockSide
    {
      get { return mDockSide; }
      private set { mDockSide = value; }
    }

    object mView;
    public object View
    {
      get { return mView; }
      private set { mView = value; }
    }

    object mTargetView;
    public object TargetView
    {
      get { return mTargetView; }
      private set { mTargetView = value; }
    }

    public static DockingStrategy Document(object view)
    {
      return new DockingStrategy(view);
    }

    public static DockingStrategy Side(DockSide side, object view)
    {
      return new DockingStrategy(DockType.Side, side, view);
    }

    public static DockingStrategy Group(GroupType position, object targetView, object view)
    {
      return new DockingStrategy(DockType.Grouped, position, targetView, view);
    }
  }
}

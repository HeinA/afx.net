﻿using Afx.Business;
using Afx.Business.Activities;
using Afx.Business.Documents;
using Afx.Business.Security;
using Afx.Business.Service;
using Afx.Business.ServiceModel;
using Afx.Prism.Events;
using Afx.Prism.RibbonMdi.Activities;
using Afx.Prism.RibbonMdi.Documents;
using Afx.Prism.RibbonMdi.Events;
using Afx.Prism.RibbonMdi.Login;
using Afx.Prism.RibbonMdi.Tools;
using Microsoft.Practices.Prism.Modularity;
using Microsoft.Practices.Prism.PubSubEvents;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi
{
  public abstract class MdiModuleController : Controller, IModule, IMdiModuleController
  {
    protected MdiModuleController(IController controller)
      : base(controller)
    {
      Controller c = controller as Controller;
      if (c != null)
      {
        c.Container.RegisterInstance<IMdiModuleController>(this);
      }
    }

    protected MdiModuleController(string id, IController controller)
      : base(id, controller)
    {
      Controller c = controller as Controller;
      if (c != null)
      {
        c.Container.RegisterInstance<IMdiModuleController>(this);
      }
    }

    [Dependency]
    public IEventAggregator EventAggregator { get; set; }

    public virtual void Initialize()
    {
      ConfigureContainer();
      AfterContainerConfigured();
      Run();
    }

    protected override void AfterContainerConfigured()
    {
      ApplicationController.EventAggregator.GetEvent<UserAuthenticatedEvent>().Subscribe(OnUserAuthenticated, ThreadOption.UIThread);
      ApplicationController.EventAggregator.GetEvent<ExecuteActivityEvent>().Subscribe(OnExecuteActivity);
      ApplicationController.EventAggregator.GetEvent<EditDocumentEvent>().Subscribe(OnEditDocument);
      base.AfterContainerConfigured();
    }

    protected virtual void OnUserAuthenticated(EventArgs obj)
    {
      if (SecurityContext.User != null)
      {
        //MdiApplicationController.Current.ComposeRibbonTabs();
        //ConfigureRibbon();
        //ConfigureRibbonEnd();
      }
    }

    protected virtual void ConfigureRibbon()
    {
    }

    internal virtual void ConfigureRibbonEnd()
    {
    }

    //protected T RegisterSearchViewModel<T>()
    //  where T : SearchItemViewModel
    //{
    //  return ApplicationController.RegisterSearchViewModel<T>();
    //}

    IMdiApplicationController mApplicationController;
    [Dependency]
    public IMdiApplicationController ApplicationController
    {
      get { return mApplicationController; }
      set { mApplicationController = value; }
    }

    [Dependency]
    public IRegionManager RegionManager { get; set; }

    #region EditDocument

    protected TController EditDocument<TContext, TController>(Guid documentIdentifier)
      where TContext : class, IDocumentContext, new()
      where TController : Controller, IMdiDocumentController
    {
      TController c = null;
      try
      {
        TContext cx = new TContext();
        cx.DocumentIdentifier = documentIdentifier;
        c = GetCreateChildController<TController>(cx.ContextIdentifier);
        c.DataContext = cx;
        c.Run();
        return c;
      }
      catch
      {
        if (c != null) c.Terminate();
        throw;
      }
    }

    protected IController EditDocument(Type controllerType, Type contextType, Guid documentIdentifier)
    {
      IMdiDocumentController c = null;
      try
      {
        object o = Activator.CreateInstance(contextType);
        IDocumentContext context = (IDocumentContext)o;
        context.DocumentIdentifier = documentIdentifier;
        c = (IMdiDocumentController)GetCreateChildController(controllerType, context.ContextIdentifier);
        c.DataContext = context;
        c.Run();
        return c;
      }
      catch
      {
        if (c != null) c.Terminate();
        throw;
      }
    }

    protected IController EditDocument(Type controllerType, Type contextType, Document document)
    {
      IMdiDocumentController c = null;
      try
      {
        object o = Activator.CreateInstance(contextType);
        IDocumentContext context = (IDocumentContext)o;
        context.DocumentIdentifier = document.GlobalIdentifier;
        context.Document = document;
        c = (IMdiDocumentController)GetCreateChildController(controllerType, context.ContextIdentifier);
        c.DataContext = context;
        c.Run();
        return c;
      }
      catch
      {
        if (c != null) c.Terminate();
        throw;
      }
    }

    #endregion

    #region OpenActivity

    protected TController OpenActivity<TContext, TController>(Activity activity)
      where TContext : BusinessObject, IActivityContext, new()
      where TController : Controller, IMdiActivityController
    {
      TController c = null;
      try
      {
        TContext context = new TContext();
        context.Activity = activity;
        c = GetCreateChildController<TController>(context.ContextIdentifier);
        if (c.DataContext == null) c.DataContext = context;
        c.Run();
        return c;
      }
      catch
      {
        if (c != null) c.Terminate();
        throw;
      }
    }

    protected IController OpenActivity(Type controllerType, Type contextType, Activity activity, Guid contextId)
    {
      IMdiActivityController c = null;
      try
      {
        IActivityContext context = (IActivityContext)Activator.CreateInstance(contextType);
        context.Activity = activity;
        context.ContextId = contextId;
        c = (IMdiActivityController)GetCreateChildController(controllerType, context.ContextIdentifier);
        if (c.DataContext == null) c.DataContext = context;
        c.Run();
        return c;
      }
      catch
      {
        if (c != null) c.Terminate();
        throw;
      }
    }

    #endregion

    void IMdiModuleController.ExecuteActivity(Activity activity, Guid contextId)
    {
      ExecuteActivityInternal(activity, contextId);
    }

    void OnExecuteActivity(ExecuteActivityEventArgs e)
    {
      BeforeExecuteActivity(e.Activity, e.ContextId);
      ExecuteActivityInternal(e.Activity, e.ContextId);
    }

    void IMdiModuleController.EditDocument(DocumentType dt, Guid documentIdentifier)
    {
      EditDocumentInternal(dt, documentIdentifier);
    }

    private void OnEditDocument(EditDocumentEventArgs obj)
    {
      BeforeEditDocument(obj.DocumentType, obj.DocumentIdentifier);
      if (obj.Document != null)
      {
        EditDocumentInternal(obj.Document);
      }
      else
      {
        EditDocumentInternal(obj.DocumentType, obj.DocumentIdentifier);
      }
    }

    void ExecuteActivityInternal(Activity activity, Guid contextId)
    {
      using (new WaitCursor())
      {
        try
        {
          ExecuteActivity(activity, contextId);
        }
        catch (Exception ex)
        {
          if (ExceptionHelper.HandleException(ex)) throw;
          //HandleActivityExecutionException(activity, contextId, ex);
        }
      }
    }

    void EditDocumentInternal(Activity activity, Guid contextId)
    {
      using (new WaitCursor())
      {
        try
        {
          ExecuteActivity(activity, contextId);
        }
        catch (Exception ex)
        {
          if (ExceptionHelper.HandleException(ex)) throw;
          //HandleActivityExecutionException(activity, contextId, ex);
        }
      }
    }


    protected virtual void BeforeExecuteActivity(Activity activity, Guid contextId)
    {
    }

    protected virtual void ExecuteActivity(Activity activity, Guid contextId)
    {
      Type controllerType = ExtensibilityManager.GetExportedType(string.Format("Controller:{0}", activity.Identifier));
      Type contextType = ExtensibilityManager.GetExportedType(string.Format("Context:{0}", activity.Identifier));
      if (controllerType != null && contextType != null && controllerType.Assembly == this.GetType().Assembly) OpenActivity(controllerType, contextType, activity, contextId);
    }

    void EditDocumentInternal(DocumentType dt, Guid documentIdentifier)
    {
      using (new WaitCursor())
      {
        try
        {
          EditDocument(dt, documentIdentifier);
        }
        catch (Exception ex)
        {
          if (ExceptionHelper.HandleException(ex)) throw;
        }
      }
    }

    void EditDocumentInternal(Document document)
    {
      using (new WaitCursor())
      {
        try
        {
          EditDocument(document);
        }
        catch (Exception ex)
        {
          if (ExceptionHelper.HandleException(ex)) throw;
        }
      }
    }

    protected virtual void BeforeEditDocument(DocumentType dt, Guid documentIdentifier)
    {
    }

    protected virtual void EditDocument(DocumentType dt, Guid documentIdentifier)
    {
      Type controllerType = ExtensibilityManager.GetExportedType(string.Format("Controller:{0}", dt.Identifier));
      Type contextType = ExtensibilityManager.GetExportedType(string.Format("Context:{0}", dt.Identifier));
      if (controllerType != null && contextType != null && controllerType.Assembly == this.GetType().Assembly) EditDocument(controllerType, contextType, documentIdentifier);
    }

    protected virtual void EditDocument(Document document)
    {
      Type controllerType = ExtensibilityManager.GetExportedType(string.Format("Controller:{0}", document.DocumentType.Identifier));
      Type contextType = ExtensibilityManager.GetExportedType(string.Format("Context:{0}", document.DocumentType.Identifier));
      if (controllerType != null && contextType != null && controllerType.Assembly == this.GetType().Assembly) EditDocument(controllerType, contextType, document);
    }


//    void HandleActivityExecutionException(Activity activity, int contextId, Exception ex)
//    {
//      RetryActivityExecution rt = new RetryActivityExecution(this, activity, contextId);

//      if (ex is FaultException<AuthorizationFault>)
//      {
//      }
//      else if (ex is FaultException<MessageFault>)
//      {
//        if (ExceptionHelper.HandleException(ex)) throw ex;
//      }
//      else if (ex is CommunicationException)
//      {
//#if DEBUG
//        if (Debugger.IsAttached) Debugger.Break();
//#endif
//        Thread.Sleep(1000);
//        rt.Retry();
//      }
//      else if (ExceptionHelper.HandleException(ex)) throw ex;
//    }

//    void HandleEditDocumentException(DocumentType dt, Guid documentIdentifier, Exception ex)
//    {
//      RetryDocumentEdit rt = new RetryDocumentEdit(this, dt, documentIdentifier);

//      if (ex is FaultException<AuthorizationFault>)
//      {
//      }
//      else if (ex is FaultException<MessageFault>)
//      {
//        if (ExceptionHelper.HandleException(ex)) throw ex;
//      }
//      else if (ex is CommunicationException)
//      {
//#if DEBUG
//        if (Debugger.IsAttached) Debugger.Break();
//#endif
//        Thread.Sleep(1000);
//        rt.Retry();
//      }
//      else if (ExceptionHelper.HandleException(ex)) throw ex;
//    }
  }
}

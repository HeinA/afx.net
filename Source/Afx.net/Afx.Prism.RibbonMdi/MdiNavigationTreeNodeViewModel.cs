﻿using Afx.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi
{
  public abstract class MdiNavigationTreeNodeViewModel<TModel> : TreeNodeViewModel<TModel>
    where TModel : class, IBusinessObject
  {
    protected MdiNavigationTreeNodeViewModel(IController controller)
      : base(controller)
    {
    }

    public override bool IsSelected
    {
      get
      {
        if (base.IsSelected && typeof(IContextController).IsAssignableFrom(Controller.GetType())) ((IContextController)Controller).SetContextViewModel(this); 
        return base.IsSelected;
      }
      set
      {
        base.IsSelected = value;
        if (base.IsSelected && typeof(IContextController).IsAssignableFrom(Controller.GetType())) ((IContextController)Controller).SetContextViewModel(this);
      }
    }
  }
}

﻿using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi
{
  class SaveChangesViewModel : MdiDialogViewModel<ContextBase>
  {
    [InjectionConstructor]
    public SaveChangesViewModel(IController controller)
      : base(controller)
    {
    }

    public string Title
    {
      get { return Model.TitleText; }
    }

    [Dependency]
    public SaveChangesController SaveChangesController { get; set; }

    #region DelegateCommand DiscardChangesCommand

    DelegateCommand mDiscardChangesCommand;
    public DelegateCommand DiscardChangesCommand
    {
      get { return mDiscardChangesCommand ?? (mDiscardChangesCommand = new DelegateCommand(ExecuteDiscardChanges)); }
    }

    void ExecuteDiscardChanges()
    {
      SaveChangesController.Discard();
    }

    #endregion
  }
}

﻿using Afx.Business;
using Afx.Business.Collections;
using Microsoft.Practices.Prism.Mvvm;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi
{
  public abstract class MdiRibbonTabViewModel<TModel, TGroupModel, TGroupViewModel> : ViewModel<TModel>
    where TModel : BusinessObject
    where TGroupModel : BusinessObject
    where TGroupViewModel : class, IViewModel
  {
    protected MdiRibbonTabViewModel(IController controller)
      : base(controller)
    {
    }

    ViewModelCollection<TGroupViewModel, TGroupModel> mViewModels;
    public ViewModelCollection<TGroupViewModel, TGroupModel> ViewModels
    {
      get { return mViewModels; }
      private set { mViewModels = value; }
    }

    protected override void OnDataContextChanged()
    {
      //ViewModels = new ViewModelCollection<TGroupViewModel,TGroupModel>(Model.
      base.OnDataContextChanged();
    }

    //override 
    //BasicCollection<TGroupModel> mGroups;
    //public BasicCollection<TGroupModel> Groups
    //{
    //  get { return mGroups; }
    //  set
    //  {
    //    if (mGroups != value)
    //    {
    //      if (Groups != null) Groups.CollectionChanged -= Groups_CollectionChanged;
    //      mGroups = value;
    //      if (Groups != null) Groups.CollectionChanged += Groups_CollectionChanged;
    //      InternalGroupViewModels.Clear();
    //      foreach (TGroupModel model in Groups)
    //      {
    //        InternalGroupViewModels.Add(CreateViewModel(model));
    //      }
    //    }
    //  }
    //}

    //void Groups_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
    //{
    //  switch (e.Action)
    //  {
    //    case NotifyCollectionChangedAction.Add:
    //      foreach (TGroupModel model in e.NewItems)
    //      {
    //        InternalGroupViewModels.Add(CreateViewModel(model));
    //      }
    //      break;

    //    case NotifyCollectionChangedAction.Remove:
    //      foreach (TGroupModel group in e.OldItems)
    //      {
    //        MdiRibbonGroupBoxViewModel vm = GroupViewModels.OfType<MdiRibbonGroupBoxViewModel>().FirstOrDefault((g) => g.ItemViewModels.Equals(group));
    //        if (vm != null) InternalGroupViewModels.Remove(vm);
    //      }
    //      break;

    //    case NotifyCollectionChangedAction.Reset:
    //      InternalGroupViewModels.Clear();
    //      break;

    //    default:
    //      throw new NotImplementedException();
    //  }
    //}

    //protected abstract MdiRibbonGroupBoxViewModel CreateViewModel(TGroupModel model);

    //ViewModelCollection<MdiRibbonGroupBoxViewModel<TGroupModel>, TGroupModel> mGroupViewModels1;
    //public ViewModelCollection<MdiRibbonGroupBoxViewModel<TGroupModel>, TGroupModel> GroupViewModels1
    //{
    //  get { return mGroupViewModels1; }
    //  private set { SetProperty<ViewModelCollection<MdiRibbonGroupBoxViewModel<TGroupModel>, TGroupModel>>(ref mGroupViewModels1, value); }
    //}
  
    //ObservableCollection<MdiRibbonGroupBoxViewModel> mInternalGroupViewModels;
    //protected ObservableCollection<MdiRibbonGroupBoxViewModel> InternalGroupViewModels
    //{
    //  get { return mInternalGroupViewModels ?? (mInternalGroupViewModels = new ObservableCollection<MdiRibbonGroupBoxViewModel>()); }
    //  set
    //  {
    //    if (SetProperty<ObservableCollection<MdiRibbonGroupBoxViewModel>>(ref mInternalGroupViewModels, value))
    //    {
    //      mGroupViewModels = null;
    //      OnPropertyChanged("GroupViewModels");
    //    }
    //  }
    //}

    //ReadOnlyObservableCollection<MdiRibbonGroupBoxViewModel> mGroupViewModels;
    //public ReadOnlyObservableCollection<MdiRibbonGroupBoxViewModel> GroupViewModels
    //{
    //  get { return mGroupViewModels ?? (mGroupViewModels = new ReadOnlyObservableCollection<MdiRibbonGroupBoxViewModel>(InternalGroupViewModels)); }
    //}
  }

  //public abstract class MdiRibbonTabViewModel<TModel> : ViewModel<TModel>
  //  where TModel : BusinessObject
  //{
  //  protected MdiRibbonTabViewModel(IController controller)
  //    : base(controller)
  //  {
  //  }
  //}
}

﻿using Microsoft.Practices.Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace Afx.Prism.RibbonMdi.WaitAnimation
{
  class WaitAnimationViewModel : BindableBase
  {
    #region string Caption

    string mCaption;
    public string Caption
    {
      get { return mCaption; }
      set { SetProperty<string>(ref mCaption, value); }
    }

    #endregion

    #region double Height

    double mHeight = 75;
    public double Height
    {
      get { return mHeight; }
      set { SetProperty<double>(ref mHeight, value); }
    }

    #endregion

    #region double Width

    double mWidth = 75;
    public double Width
    {
      get { return mWidth; }
      set { SetProperty<double>(ref mWidth, value); }
    }

    #endregion

    #region Style Style

    Style mStyle;
    public Style Style
    {
      get { return mStyle; }
      set { SetProperty<Style>(ref mStyle, value); }
    }

    #endregion

    #region WindowStyle WindowStyle

    WindowStyle mWindowStyle = WindowStyle.None;
    public WindowStyle WindowStyle
    {
      get { return mWindowStyle; }
      set { SetProperty<WindowStyle>(ref mWindowStyle, value); }
    }

    #endregion

    #region Brush WindowBackground

    Brush mWindowBackground = Brushes.Transparent;
    public Brush WindowBackground
    {
      get { return mWindowBackground; }
      set { SetProperty<Brush>(ref mWindowBackground, value); }
    }

    #endregion

    #region WindowState WindowState

    Xceed.Wpf.Toolkit.WindowState mWindowState = Xceed.Wpf.Toolkit.WindowState.Open;
    public Xceed.Wpf.Toolkit.WindowState WindowState
    {
      get { return mWindowState; }
    }

    #endregion  
  }
}

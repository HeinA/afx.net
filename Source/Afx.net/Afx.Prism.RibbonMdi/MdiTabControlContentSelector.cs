﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace Afx.Prism.RibbonMdi
{
  public class MdiTabControlContentSelector : DataTemplateSelector
  {
    public MdiTabControlContentSelector()
    {
    }

    public override System.Windows.DataTemplate SelectTemplate(object item, System.Windows.DependencyObject container)
    {
      DataTemplateKey dtk = new DataTemplateKey(item.GetType());
      DataTemplate dt = (DataTemplate)Application.Current.FindResource(dtk);
      //FrameworkElement element = (FrameworkElement)dt.LoadContent();
      return base.SelectTemplate(item, container);
    }
  }
}

﻿using Afx.Business.Security;
using Afx.Prism.RibbonMdi.Events;
using Fluent;
using Microsoft.Practices.Prism.PubSubEvents;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi
{
  public class MdiRibbonController<TView, TViewModel> : Controller
    where TView : RibbonTabItem
  {
    protected MdiRibbonController(IController controller)
      : base(controller)
    {
    }

    protected MdiRibbonController(string key, IController controller)
      : base(key, controller)
    {
    }

    [Dependency]
    public IRegionManager RegionManager { get; set; }

    [Dependency]
    public TView View { get; set; }

    [Dependency]
    public TViewModel ViewModel { get; set; }

    [Dependency]
    public IMdiApplicationController ApplicationController { get; set; }

    protected override void OnRun()
    {
      ApplicationController.ApplicationEventAggregator.GetEvent<UserAuthenticatedEvent>().Subscribe(OnUserAuthenticated, ThreadOption.UIThread);
      View.DataContext = ViewModel;
      RegionManager.Regions[MdiApplication.RibbonRegion].Add(View);
      base.OnRun();
    }

    protected virtual void OnUserAuthenticated(EventArgs obj)
    {
      if (SecurityContext.User == null) Terminate();
    }

    public override void Terminate()
    {
      IRegion region = RegionManager.Regions[MdiApplication.RibbonRegion];
      if (region.Views.Contains(View)) region.Remove(View);
      base.Terminate();
    }
  }
}

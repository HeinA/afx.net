﻿using Afx.Business;
using Afx.Business.Activities;
using Afx.Business.Security;
using Afx.Business.ServiceModel;
using Afx.Prism.Events;
using Afx.Prism.RibbonMdi.Dialogs.EditFlags;
using Afx.Prism.RibbonMdi.Dialogs.RefreshDialog;
using Afx.Prism.RibbonMdi.Events;
using Afx.Prism.RibbonMdi.Login;
using Microsoft.Practices.ObjectBuilder2;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi
{
  public abstract class MdiDockableDocumentController<TModel, TViewModel> : MdiViewController<TModel, TViewModel>, IMdiDockableDocumentController, IInternalDockableController, IBuilderAware
    where TModel : ContextBase, IContextBase
    where TViewModel : MdiDockableDocumentViewModel<TModel>
  {
    protected MdiDockableDocumentController(IController controller)
      : base(controller)
    {
    }

    protected MdiDockableDocumentController(string key, IController controller)
      : base(key, controller)
    {
    }

    protected override void ConfigureContainer()
    {
      IRegionManager rm = GetInstance<IRegionManager>();
      IRegionManager scoped = rm.CreateRegionManager();
      Container.RegisterInstance<IRegionManager>(scoped);
      Container.RegisterInstance<IMdiDockableDocumentController>(this);
      base.ConfigureContainer();
    }

    private void OnCacheUpdatedInternal(EventArgs obj)
    {
      if (State == ControllerState.Terminated) return;
      OnCacheUpdated();
    }

    protected virtual void OnCacheUpdated()
    {
      if (CacheUpdated != null) CacheUpdated(this, EventArgs.Empty);
    }

    public event EventHandler<EventArgs> CacheUpdated;

    public void OnBuiltUp(NamedTypeBuildKey buildKey)
    {
      ApplicationController.EventAggregator.GetEvent<CacheUpdatedEvent>().Subscribe(OnCacheUpdatedInternal);
    }

    public void OnTearingDown()
    {
    }

    protected override bool OnRun()
    {
      if (State == ControllerState.Running)
      {
        Activate();
        return false;
      }

      if (DataContext == null) throw new InvalidOperationException("DataContext may not be null.");
      InitialDataContextLoad();
      Key = DataContext.ContextIdentifier;
      //else ViewModel.ResetOperations();
      ApplicationController.ApplicationContext.DocumentViewModels.Add(ViewModel);
      return base.OnRun();
    }

    protected virtual void InitialDataContextLoad()
    {
      LoadDataContext();
    }

    //protected override void OnRunning()
    //{
    //  base.OnRunning();
    //  ViewModel.IsActive = true;
    //}

    protected override void OnTerminated()
    {
      if (MdiApplicationController.Current.ActiveDocument == this)
      {
        MdiApplicationController.Current.ActiveDocument = null;
      }

      base.OnTerminated();
    }

    protected override void OnDataContextChanged()
    {
      Key = DataContext.ContextIdentifier;
      if (DataContext != null && State == ControllerState.Running)
      {
        ViewModel.Model = DataContext;
        LoadDataContext();
      }
      base.OnDataContextChanged();
    }

    protected override void OnDataContextPropertyChanged(string propertyName)
    {
      switch (propertyName)
      {
        case "ContextIdentifier":
          Key = DataContext.ContextIdentifier;
          break;
      }

      base.OnDataContextPropertyChanged(propertyName);
    }


    public void FocusViewModel<T>(BusinessObject model)
      where T : IViewModel
    {
      IViewModel vm = GetCreateViewModel(typeof(T), model, ViewModel);
      vm.IsFocused = true;
    }

    public virtual bool IsOperationVisible(Operation op)
    {
      return true;
    }

    void IMdiDockableDocumentController.ExecuteOperation(Operation op)
    {
      ExecuteOperationInternal(op, GetArgument());
    }

    protected virtual BusinessObject GetArgument()
    {
      return null;
    }

    void IMdiDockableDocumentController.ExecuteOperation(Operation op, BusinessObject argument)
    {
      ExecuteOperationInternal(op, argument);
    }

    protected void ExecuteOperationInternal(Operation op, BusinessObject argument)
    {
      try
      {
        ExecuteOperation(op, argument);
      }
      catch (Exception ex)
      {
        if (ExceptionHelper.HandleException(ex)) throw ex;
        //HandleOperationExecutionException(op, argument, ex);
      }
    }

    //    void HandleOperationExecutionException(Operation op, BusinessObject argument, Exception ex)
    //    {
    //      RetryOperationExecution rt = new RetryOperationExecution(this, op, argument);

    //      //if (ex is FaultException<SessionExpiredFault>)
    //      //{
    //      //  SessionExpiredController c = MdiApplicationController.Current.CreateChildController<SessionExpiredController>();
    //      //  c.RetryAction = rt;
    //      //  c.Run();
    //      //}
    //      //else 
    //      if (ex is FaultException<AuthorizationFault>)
    //      {
    //      }
    //      else if (ex is FaultException)
    //      {
    //        if (ExceptionHelper.HandleException(ex)) throw ex;
    //      }
    //      else if (ex is CommunicationException)
    //      {
    //#if DEBUG
    //        if (Debugger.IsAttached) Debugger.Break();
    //#endif
    //        Thread.Sleep(1000);
    //        rt.Retry();
    //      }
    //      else if (ExceptionHelper.HandleException(ex)) throw ex;
    //    }

    public virtual void OnRefresh()
    {
      LoadDataContext();
    }

    protected virtual void ExecuteOperation(Operation op, BusinessObject argument)
    {
      switch (op.Identifier)
      {
        case StandardOperations.EditFlags:
          EditFlagsDialogController efc = CreateChildController<EditFlagsDialogController>();
          efc.DataContext = new EditFlagsDialogContext() { TargetObject = argument };
          efc.Run();
          break;

        case StandardOperations.Refresh:
          Refresh();
          break;

        case StandardOperations.Save:
          Save(true);
          break;
      }

      if (this.Container != null) ViewModel.ResetGlobalOperations();
    }

    public void Refresh(bool force = false)
    {
      if (force)
      {
        OnRefresh();
      }
      else
      {
        if (IsDirty)
        {
          RefreshDialogController c = CreateChildController<RefreshDialogController>();
          c.DockableController = this;
          c.Run();
        }
        else
        {
          OnRefresh();
        }
      }
    }

    protected bool Save(bool userClickedSaved = false)
    {
      if (!DataContext.Validate())
      {
        System.Media.SystemSounds.Hand.Play();
        return false;
      }
      SaveDataContext();
      DataContext.IsNew = false;
      OnSaved(userClickedSaved);
      return true;
    }

    public event EventHandler Saved;

    protected virtual void OnSaved(bool userClickedSaved)
    {
      if (Saved != null) Saved(this, EventArgs.Empty);
    }

    #region void OnLoaded()

    protected virtual void OnLoaded()
    {
    }

    void IInternalDockableController.OnLoaded()
    {
      OnLoaded();
    }

    #endregion

    protected override void OnViewModelCreated(IViewModelBase viewModel)
    {
      IViewModel vm = viewModel as IViewModel;
      if (vm != null) vm.Validate();

      base.OnViewModelCreated(viewModel);
    }

    protected virtual void LoadDataContext()
    {
    }

    protected virtual void SaveDataContext()
    {
    }

    public string Title
    {
      get { return ViewModel.Title; }
    }

    void IMdiDockableDocumentController.SaveDataContext()
    {
      SaveDataContext();
    }

    public virtual bool IsDirty
    {
      get { return DataContext.IsDirty; }
    }

    public virtual bool IsValid
    {
      get { return DataContext.IsValid; }
    }

    public override void Terminate()
    {
      base.Terminate();
      ApplicationController.ApplicationContext.DocumentViewModels.Remove(ViewModel);
    }

    public virtual void Activate()
    {
      ViewModel.Activate();
    }

    public virtual bool IsReadOnly
    {
      get { return DataContext.IsReadOnly; }
    }

    public void RequestClose()
    {
      if (IsReadOnly)
      {
        Terminate();
      }

      if (State == ControllerState.Running && IsDirty)
      {
        if (IsValid)
        {
          SaveChangesController cc = CreateChildController<SaveChangesController>();
          cc.DataContext = DataContext;
          cc.Run();
        }
        else
        {
          DiscardChangesController cc = CreateChildController<DiscardChangesController>();
          cc.DataContext = DataContext;
          cc.Run();
        }
      }
      else
      {
        Terminate();
      }
    }
  }
}

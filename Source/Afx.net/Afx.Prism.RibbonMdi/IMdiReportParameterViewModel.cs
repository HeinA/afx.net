﻿using Microsoft.Practices.Prism.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Afx.Prism.RibbonMdi
{
  public interface IMdiReportParameterViewModel : IViewModel
  {
    IMdiDialogController ReportParameterController { get; }
    string Caption { get; set; }
    Xceed.Wpf.Toolkit.WindowState WindowState { get; set; }
    Visibility ErrorVisibility { get; }

    DelegateCommand ApplyCommand { get; }
    DelegateCommand OkCommand { get; }
    DelegateCommand CancelCommand { get; }
  }
}

﻿using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace Afx.Prism.RibbonMdi.Dialogs.Import
{
  public class ImportDialogController : MdiDialogController<ImportDialogContext, ImportDialogViewModel>
  {
    public const string ImportDialogControllerKey = "Afx.Prism.RibbonMdi.Dialogs.Import.ImportDialogController";

    public ImportDialogController(IController controller)
      : base(ImportDialogControllerKey, controller)
    {
    }

    #region IImportController ImportController

    IImportController mImportController;
    public IImportController ImportController
    {
      get { return mImportController; }
      set { mImportController = value; }
    }

    #endregion

    #region string Identifier

    string mIdentifier;
    public string Identifier
    {
      get { return mIdentifier; }
      set { mIdentifier = value; }
    }

    #endregion

    protected override bool OnRun()
    {
      if (DataContext == null) DataContext = new ImportDialogContext();
      ViewModel.Caption = "Import from external system...";
      return base.OnRun();
    }

    protected override void ApplyChanges()
    {
      ImportController.DoImport(DataContext.ExternalSystem, Identifier);
      base.ApplyChanges();
    }
  }
}

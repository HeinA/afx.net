﻿using Afx.Business;
using Afx.Business.Validation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Dialogs.Import
{   
  public class ImportDialogContext : BusinessObject
  {
    #region ExternalSystem ExternalSystem

    ExternalSystem mExternalSystem;
    [Mandatory("Please select an External System.")]
    public ExternalSystem ExternalSystem
    {
      get { return mExternalSystem; }
      set { SetProperty<ExternalSystem>(ref mExternalSystem, value); }
    }

    #endregion
  }
}
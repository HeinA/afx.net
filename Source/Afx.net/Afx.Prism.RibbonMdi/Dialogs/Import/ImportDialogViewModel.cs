﻿using Afx.Business;
using Afx.Business.Data;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Dialogs.Import
{
  public class ImportDialogViewModel : MdiDialogViewModel<ImportDialogContext>
  {
    [InjectionConstructor]
    public ImportDialogViewModel(IController controller)
      : base(controller)
    {
    }

    #region ExternalSystem ExternalSystem

    Collection<ExternalSystem> mExternalSystems;
    public IEnumerable<ExternalSystem> ExternalSystems
    {
      get { return mExternalSystems ?? (mExternalSystems = Cache.Instance.GetObjects<ExternalSystem>(true, es => es.Name, true)); }
    }

    public ExternalSystem ExternalSystem
    {
      get
      {
        if (Model == null) return GetDefaultValue<ExternalSystem>();
        return Model.ExternalSystem;
      }
      set { Model.ExternalSystem = value; }
    }

    #endregion

  }
}

﻿using Afx.Business;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Dialogs.EditFlags
{
  public class EditFlagsDialogContext : BusinessObject
  {
    #region BusinessObject TargetObject

    public const string TargetObjectProperty = "TargetObject";
    BusinessObject mTargetObject;
    public BusinessObject TargetObject
    {
      get { return mTargetObject; }
      set { mTargetObject = value; }
    }

    #endregion
  }
}
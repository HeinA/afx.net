﻿using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Dialogs.EditFlags
{
  public class FlagItemViewModel
  {
    public FlagItemViewModel(EditFlagsDialogController controller)
    {
      EditFlagsDialogController = controller;
    }

    EditFlagsDialogController EditFlagsDialogController { get; set; }

    #region string Flag

    public const string FlagProperty = "Flag";
    string mFlag;
    public string Flag
    {
      get { return mFlag; }
      set { mFlag = value; }
    }

    #endregion

    #region bool IsFlagged

    public bool IsFlagged
    {
      get { return EditFlagsDialogController.Flags.Contains(Flag); }
      set
      {
        if (value)
        {
          if (!EditFlagsDialogController.Flags.Contains(Flag)) EditFlagsDialogController.Flags.Add(Flag);
        }
        else
        {
          if (EditFlagsDialogController.Flags.Contains(Flag)) EditFlagsDialogController.Flags.Remove(Flag);
        }
      }
    }

    #endregion
  }
}

﻿using Afx.Business;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace Afx.Prism.RibbonMdi.Dialogs.EditFlags
{
  public class EditFlagsDialogController : MdiDialogController<EditFlagsDialogContext, EditFlagsDialogViewModel>
  {
    public const string EditFlagsDialogControllerKey = "Afx.Prism.RibbonMdi.Dialogs.EditFlags.EditFlagsDialogController";

    public EditFlagsDialogController(IController controller)
      : base(EditFlagsDialogControllerKey, controller)
    {
    }

    protected override bool OnRun()
    {
      if (DataContext == null) DataContext = new EditFlagsDialogContext();
      ViewModel.Caption = "Edit Flags";

      foreach (var f in DataContext.TargetObject.Flags)
      {
        Flags.Add(f);
      }

      return base.OnRun();
    }

    Collection<string> mFlags = new Collection<string>();
    public Collection<string> Flags
    {
      get { return mFlags; }
    }

    protected override void ApplyChanges()
    {
      foreach (var f in ExtensibilityManager.GetFlags(DataContext.TargetObject.GetType()))
      {
        if (Flags.Contains(f))
        {
          DataContext.TargetObject.SetFlag(f);
        }
        else
        {
          DataContext.TargetObject.UnFlag(f);
        }
      }

      base.ApplyChanges();
    }
  }
}

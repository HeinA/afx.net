﻿using Afx.Business;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Dialogs.EditFlags
{
  public class EditFlagsDialogViewModel : MdiDialogViewModel<EditFlagsDialogContext>
  {
    [InjectionConstructor]
    public EditFlagsDialogViewModel(IController controller)
      : base(controller)
    {
    }

    protected override void OnLoaded()
    {
      base.OnLoaded();
    }

    public IEnumerable<FlagItemViewModel> Flags
    {
      get
      {
        Collection<FlagItemViewModel> col = new Collection<FlagItemViewModel>();
        foreach (var f in ExtensibilityManager.GetFlags(Model.TargetObject.GetType()))
        {
          FlagItemViewModel vm = new FlagItemViewModel((EditFlagsDialogController)Controller);
          vm.Flag = f;
          col.Add(vm);
        }
        return col;
      }
    }
  }
}

﻿using Afx.Prism;
using Afx.Prism.RibbonMdi;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Dialogs.RefreshDialog
{
  public class RefreshDialogViewModel : MdiDialogViewModel<RefreshDialogContext>
  {
    [InjectionConstructor]
    public RefreshDialogViewModel(IController controller)
      : base(controller)
    {
    }
  }
}

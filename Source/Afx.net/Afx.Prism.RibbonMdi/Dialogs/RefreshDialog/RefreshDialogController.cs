﻿using Afx.Prism;
using Afx.Prism.RibbonMdi;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace Afx.Prism.RibbonMdi.Dialogs.RefreshDialog
{
  public class RefreshDialogController : MdiDialogController<RefreshDialogContext, RefreshDialogViewModel>
  {
    public const string RefreshDialogControllerKey = "Afx.Prism.RibbonMdi.Dialogs.RefreshDialog.RefreshDialogController";

    public RefreshDialogController(IController controller)
      : base(RefreshDialogControllerKey, controller)
    {
    }

    IInternalDockableController mDockableController;
    internal IInternalDockableController DockableController
    {
      get { return mDockableController; }
      set { mDockableController = value; }
    }

    protected override bool OnRun()
    {
      if (DataContext == null) DataContext = new RefreshDialogContext();
      ViewModel.Caption = "Refresh?";
      return base.OnRun();
    }

    protected override void ApplyChanges()
    {
      DockableController.OnRefresh();
      base.ApplyChanges();
    }
  }
}

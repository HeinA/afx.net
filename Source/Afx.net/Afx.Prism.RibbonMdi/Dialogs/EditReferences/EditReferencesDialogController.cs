﻿using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace Afx.Prism.RibbonMdi.Dialogs.EditReferences
{
  public class EditReferencesDialogController : MdiDialogController<EditReferencesDialogContext, EditReferencesDialogViewModel>
  {
    public const string EditReferencesDialogControllerKey = "Afx.Prism.RibbonMdi.Dialogs.EditReferences.EditReferencesDialogController";

    public EditReferencesDialogController(IController controller)
      : base(EditReferencesDialogControllerKey, controller)
    {
    }

    protected override bool OnRun()
    {
      if (DataContext == null) DataContext = new EditReferencesDialogContext();
      ViewModel.Caption = "Edit External References";
      return base.OnRun();
    }

    protected override void ApplyChanges()
    {
      DataContext.ExternalReferenceCollection.ExternalReferences.Clear();
      foreach(var o in DataContext.ExternalReferences)
      {
        DataContext.ExternalReferenceCollection.ExternalReferences.Add(o);
      }
      base.ApplyChanges();
    }
  }
}

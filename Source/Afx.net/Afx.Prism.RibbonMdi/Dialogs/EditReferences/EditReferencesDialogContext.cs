﻿using Afx.Business;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Dialogs.EditReferences
{
  public class EditReferencesDialogContext : BusinessObject
  {
    #region IExternalReferenceCollection ExternalReferences

    IExternalReferenceCollection mExternalReferenceCollection;
    public IExternalReferenceCollection ExternalReferenceCollection
    {
      get { return mExternalReferenceCollection; }
      set { SetProperty<IExternalReferenceCollection>(ref mExternalReferenceCollection, value); }
    }

    #endregion

    IList mExternalReferences;
    public IList ExternalReferences
    {
      get
      {
        if (mExternalReferences == null)
        {
          mExternalReferences = (IList)Activator.CreateInstance(ExternalReferenceCollection.ExternalReferences.GetType());
          foreach (var er in ExternalReferenceCollection.ExternalReferences)
          {
            mExternalReferences.Add(((BusinessObject)er).Clone());
          }
        }
        return mExternalReferences;
      }
    }

    protected override void GetErrors(Collection<string> errors, string propertyName)
    {
      if (IsPropertyApplicable(propertyName, "ExternalReferences"))
      {
        bool invalid = false;
        foreach (BusinessObject er in ExternalReferences)
        {
          if (!er.Validate()) invalid = true;
        }
        if (invalid) errors.Add("An external reference has a validation error.");

        if (ExternalReferences.OfType<IExternalReference>().Where(er => !((BusinessObject)er).IsDeleted).GroupBy(er => er.ExternalSystem).Any(a => a.Count() > 1))
        {
          errors.Add("Ony one reference per external system is allowed.");
        }
      }
      base.GetErrors(errors, propertyName);
      Revalidate = true;
    }
  }
}
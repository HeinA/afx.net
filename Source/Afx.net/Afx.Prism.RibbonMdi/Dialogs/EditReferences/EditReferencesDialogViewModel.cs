﻿using Afx.Business;
using Afx.Business.Collections;
using Afx.Business.Data;
using Microsoft.Practices.Unity;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Dialogs.EditReferences
{
  public class EditReferencesDialogViewModel : MdiDialogViewModel<EditReferencesDialogContext>
  {
    [InjectionConstructor]
    public EditReferencesDialogViewModel(IController controller)
      : base(controller)
    {
    }

    BasicCollection<ExternalSystem> mExternalSystems;
    public IEnumerable<ExternalSystem> ExternalSystems
    {
      get { return mExternalSystems ?? (mExternalSystems = Cache.Instance.GetObjects<ExternalSystem>(true, es => es.Name, true)); }
    }

    #region IList ExternalReferences

    public IList ExternalReferences
    {
      get { return Model == null ? null : Model.ExternalReferences; }
    }

    #endregion
  }
}

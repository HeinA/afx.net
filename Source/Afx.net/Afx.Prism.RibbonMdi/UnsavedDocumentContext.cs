﻿using Afx.Business;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi
{
  public class UnsavedDocumentContext : BusinessObject
  {
    public UnsavedDocumentContext(Collection<IMdiDockableDocumentController> controllers)
    {
      mControllers = controllers;
    }

    private bool mShutdown;
    public bool Shutdown
    {
      get { return mShutdown; }
      set { mShutdown = value; }
    }

    private bool mLogout;
    public bool Logout
    {
      get { return mLogout; }
      set { mLogout = value; }
    }

    Collection<IMdiDockableDocumentController> mControllers;
    public IEnumerable<IMdiDockableDocumentController> Controllers
    {
      get { return mControllers; }
    }    
  }
}

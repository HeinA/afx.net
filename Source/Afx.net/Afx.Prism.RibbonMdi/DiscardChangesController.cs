﻿using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi
{
  class DiscardChangesController : MdiDialogController<ContextBase, DiscardChangesViewModel>
  {
    public DiscardChangesController(IController controller)
      : base(controller)
    {
    }

    public override bool Validate()
    {
      return true;
    }

    protected override void ApplyChanges()
    {
      Parent.Terminate();
      base.ApplyChanges();
    }  
   }
}

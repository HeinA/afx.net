﻿using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi
{
  class DiscardChangesViewModel : MdiDialogViewModel<ContextBase>
  {
    [InjectionConstructor]
    public DiscardChangesViewModel(IController controller)
      : base(controller)
    {
    }

    public string Title
    {
      get { return Model.TitleText; }
    }
  }
}

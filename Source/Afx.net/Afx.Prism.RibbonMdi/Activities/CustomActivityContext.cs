﻿using Afx.Business.Activities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Activities
{
  public class CustomActivityContext : ActivityContext, ICustomActivityContext
  {
    protected CustomActivityContext()
    {
    }

    protected CustomActivityContext(Activity activity)
      : base(activity)
    {
    }


    public new SimpleActivity Activity
    {
      get { return (SimpleActivity)base.Activity; }
      set { base.Activity = value; }
    }

    public override string ContextIdentifier
    {
      get { return Activity.Name; }
    }
  }
}

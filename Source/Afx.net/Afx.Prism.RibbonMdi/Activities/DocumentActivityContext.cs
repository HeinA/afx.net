﻿using Afx.Business;
using Afx.Business.Activities;
using Afx.Business.Documents;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Afx.Prism.RibbonMdi.Activities
{
  public class DocumentActivityContext<TData> : ActivityContext<TData>
    where TData : Document
  {
    protected DocumentActivityContext(DocumentActivity activity)
      :base(activity)
    {
    }

    public new DocumentActivity Activity
    {
      get { return (DocumentActivity)base.Activity; }
      set { base.Activity = value; }
    }

    public override string ContextIdentifier
    {
      get
      {
        if (Data != null) return string.Format("{0}[{1}]", Activity.Name, Data.Identifier);
        return Activity.Name;
      }
    }

    TData mData;
    public TData Data
    {
      get { return mData; }
      set
      {
        if (SetProperty<TData>(ref mData, value))
        {
          WeakEventManager<BusinessObject, CompositionChangedEventArgs>.AddHandler(Data, "CompositionChanged", OnDataCompositionChanged);
          OnPropertyChanged(ContextIdentifierProperty);
        }
      }
    }

    private void OnDataCompositionChanged(object sender, CompositionChangedEventArgs e)
    {
      IsDirty = true;
      OnPropertyChanged("Title");
    }

    public override bool IsValid
    {
      get
      {
        Data.Validate();
        return Data.IsValid;
      }
    }
  }
}

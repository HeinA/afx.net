﻿using Afx.Business;
using Afx.Business.Activities;
using Afx.Business.Collections;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Afx.Prism.RibbonMdi.Activities
{
  public abstract class ActivityContext : ContextBase, IActivityContext
  {
    protected ActivityContext()
    {
    }

    protected ActivityContext(Activity activity)
    {
      Activity = activity;
    }

    public Guid ContextId { get; set; }

    public const string ActivityProperty = "Activity";
    Activity mActivity;
    public Activity Activity
    {
      get { return mActivity; }
      set
      {
        mActivity = value;
        OnPropertyChanged(TitleTextProperty);
        OnPropertyChanged(ActivityProperty);
      }
    }

    public const string TitleTextProperty = "TitleText";
    public override string TitleText
    {
      get { return Activity.Name; }
    }
  }

  public abstract class ActivityContext<TData> : ActivityContext
    where TData : BusinessObject
  {
    protected ActivityContext()
    {
    }

    protected ActivityContext(Activity activity)
      : base(activity)
    {
    }
  }
}

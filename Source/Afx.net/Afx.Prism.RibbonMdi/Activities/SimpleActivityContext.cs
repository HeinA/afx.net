﻿using Afx.Business;
using Afx.Business.Activities;
using Afx.Business.Collections;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Afx.Prism.RibbonMdi.Activities
{
  public class SimpleActivityContext<TData> : ActivityContext<TData>, ISimpleActivityContext
    where TData : BusinessObject
  {
    protected SimpleActivityContext()
    {
    }

    protected SimpleActivityContext(Activity activity)
      : base(activity)
    {
    }

    public new SimpleActivity Activity
    {
      get { return (SimpleActivity)base.Activity; }
      set { base.Activity = value; }
    }

    public override string ContextIdentifier
    {
      get { return Activity.Name; }
    }

    BasicCollection<TData> mData;
    public BasicCollection<TData> Data
    {
      get
      {
        if (mData == null)
        {
          mData = new BasicCollection<TData>();
          WeakEventManager<INotifyCollectionChanged, NotifyCollectionChangedEventArgs>.AddHandler(mData, "CollectionChanged", OnDataCollectionChanged);
        }
        return mData;
      }
      set
      {
        if (SetProperty<BasicCollection<TData>>(ref mData, value))
        {
          foreach (BusinessObject bo in Data)
          {
            WeakEventManager<BusinessObject, CompositionChangedEventArgs>.AddHandler(bo, "CompositionChanged", OnDataCompositionChanged);
          }
          WeakEventManager<INotifyCollectionChanged, NotifyCollectionChangedEventArgs>.AddHandler(mData, "CollectionChanged", OnDataCollectionChanged);
          OnPropertyChanged(null);
        }
      }
    }

    private void OnDataCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
    {
      IsDirty = true;
      if (e.Action == NotifyCollectionChangedAction.Add || e.Action == NotifyCollectionChangedAction.Reset || e.Action == NotifyCollectionChangedAction.Replace)
      {
        foreach (BusinessObject bo in e.NewItems)
        {
          WeakEventManager<BusinessObject, CompositionChangedEventArgs>.AddHandler(bo, "CompositionChanged", OnDataCompositionChanged);
        }
      }
      OnPropertyChanged("Title");
    }

    private void OnDataCompositionChanged(object sender, CompositionChangedEventArgs e)
    {
      IsDirty = true;
      if (e.CompositionChangedType == CompositionChangedType.PropertyChanged && e.PropertyName != ErrorProperty && e.PropertyName != IsValidProperty && e.PropertyName != HasErrorProperty)
      {
        Validate();
      }
      OnPropertyChanged("Title");
    }

    protected internal override bool Revalidate
    {
      get { return true; }
    }

    protected override void GetErrors(Collection<string> errors, string propertyName)
    {
      ValidationHelper.ValidateCollection<TData>(Data, errors);
      base.GetErrors(errors, propertyName);
    }

    public override bool Validate()
    {
      bool b = base.Validate();
      foreach (var bo in Data)
      {
        bo.Validate();
        if (!bo.IsValid) return false;
      }
      return b;
    }
  }
}

﻿using Afx.Business.Activities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Activities
{
  public interface IDocumentActivityContext : IActivityContext
  {
    new DocumentActivity Activity { get; set; }
  }
}

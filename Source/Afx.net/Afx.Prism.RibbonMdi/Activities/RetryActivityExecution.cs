﻿using Afx.Business.Activities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Activities
{
  public class RetryActivityExecution : RetryAction
  {
    public RetryActivityExecution(IMdiModuleController module, Activity activity, int contextId)
    {
      Module = module;
      Activity = activity;
      ContextId = contextId;
    }

    IMdiModuleController mModule;
    public IMdiModuleController Module
    {
      get { return mModule; }
      private set { mModule = value; }
    }

    Activity mActivity;
    public Activity Activity
    {
      get { return mActivity; }
      private set { mActivity = value; }
    }

    int mContextId;
    public int ContextId
    {
      get { return mContextId; }
      private set { mContextId = value; }
    }

    public override void Retry()
    {
      MdiApplicationController.Current.Dispatcher.BeginInvoke(new Action(() =>
      {
        Module.ExecuteActivity(Activity, ContextId);
      }), System.Windows.Threading.DispatcherPriority.Background);
    }
  }
}

﻿using Afx.Business.Security;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Activities.UserManagement
{
  public class UserItemViewModel : MdiNavigationListItemViewModel<User>
  {
    #region Constructors

    [InjectionConstructor]
    public UserItemViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    #region string Username

    public string Username
    {
      get
      {
        if (Model == null) return default(string);
        if (string.IsNullOrWhiteSpace(Model.Username)) return "*** Unnamed ***";
        return Model.Username;
      }
    }

    #endregion

    #region string Firstnames

    public string Firstnames
    {
      get
      {
        if (Model == null) return default(string);
        return Model.Firstnames;
      }
    }

    #endregion

    #region string Lastname

    public string Lastname
    {
      get
      {
        if (Model == null) return default(string);
        return Model.Lastname;
      }
    }

    #endregion
  }
}

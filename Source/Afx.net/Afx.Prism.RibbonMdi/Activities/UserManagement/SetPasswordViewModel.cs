﻿using Afx.Business.Security;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Activities.UserManagement
{
  public class SetPasswordViewModel : MdiDialogViewModel<User>
  {
    [InjectionConstructor]
    public SetPasswordViewModel(IController controller)
      : base(controller)
    {
    }

    string mPassword;
    public string Password
    {
      get { return mPassword; }
      set { SetProperty<string>(ref mPassword, value); }
    }

    string mConfirmPassword;
    public string ConfirmPassword
    {
      get { return mConfirmPassword; }
      set { SetProperty<string>(ref mConfirmPassword, value); }
    }

    protected override string GetError()
    {
      string error = null;

      if (string.IsNullOrWhiteSpace(Password)) error = "Password may not be blank.";
      if (Password != ConfirmPassword) error += string.Format("{0}{1}{2}", error, error == null ? "" : "\n", "Password does not match confirmation.");

      return error;
    }
  }
}

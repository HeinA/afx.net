﻿using Afx.Business.Activities;
using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Business.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Activities.UserManagement
{
  public class UserManagement : ContextualActivityContext<User>
  {
    public const string Key = "{26b70882-cd1d-4fb9-9e97-932336dac02a}";
    public const string TabRegion = "TabRegion";
    public const string DetailsRegion = "DetailsRegion";

    public UserManagement()
    {
    }

    public UserManagement(ContextualActivity activity)
      : base(activity)
    {
    }

    public override void LoadData()
    {
      using (var svc = ProxyFactory.GetService<IAfxService>(SecurityContext.MasterServer.Name))
      {
        Data = svc.LoadUsers();
      }
      base.LoadData();
    }

    public override void SaveData()
    {
      using (var svc = ProxyFactory.GetService<IAfxService>(SecurityContext.MasterServer.Name))
      {
        Data = svc.SaveUsers(Data);
      }
      base.SaveData();
    }
  }
}

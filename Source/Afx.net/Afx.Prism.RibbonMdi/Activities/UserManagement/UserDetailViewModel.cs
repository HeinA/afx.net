﻿using Afx.Business.Security;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Activities.UserManagement
{
  public class UserDetailViewModel : ActivityDetailViewModel<User>
  {
    #region Constructors

    [InjectionConstructor]
    public UserDetailViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    #region string Username

    public string Username
    {
      get
      {
        if (Model == null) return default(string);
        return Model.Username;
      }
      set { Model.Username = value; }
    }

    #endregion

    #region Model Propertry string Firstnames

    public string Firstnames
    {
      get
      {
        if (Model == null) return default(string);
        return Model.Firstnames;
      }
      set { Model.Firstnames = value; }
    }

    #endregion

    #region Model Propertry string Lastname

    public string Lastname
    {
      get
      {
        if (Model == null) return default(string);
        return Model.Lastname;
      }
      set { Model.Lastname = value; }
    }

    #endregion
  }
}

﻿using Afx.Business;
using Afx.Business.Activities;
using Afx.Business.Security;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Activities.UserManagement
{
  public class UserManagementController : MdiContextualActivityController<UserManagement, UserManagementViewModel>
  {
    [InjectionConstructor]
    public UserManagementController(IController controller)
      : base(controller)
    {
    }

    protected override void OnLoaded()
    {
      if (DataContext.Data.Count > 0)
      {
        UserItemViewModel ulvm = GetCreateViewModel<UserItemViewModel>(DataContext.Data[0], ViewModel);
        ulvm.IsFocused = true;
      }

      RegisterContextRegion(RegionManager.Regions[UserManagement.TabRegion]);
      RegisterContextRegion(RegionManager.Regions[UserManagement.DetailsRegion]);

      base.OnLoaded();
    }

    protected override void OnContextViewModelChanged(ISelectableViewModel context)
    {
      if (context.Model is User)
      {
        UserDetailViewModel vm = GetCreateViewModel<UserDetailViewModel>(context.Model, ViewModel);
        RegionManager.Regions[UserManagement.DetailsRegion].Add(vm);

        IRegion tabRegion = RegionManager.Regions[UserManagement.TabRegion];

        RolesViewModel rvm = GetCreateViewModel<RolesViewModel>(context.Model, ViewModel);
        tabRegion.Add(rvm);
        if (rvm.IsActive) tabRegion.Activate(rvm);

        OrganizationalUnitsViewModel ouvm = GetCreateViewModel<OrganizationalUnitsViewModel>(context.Model, ViewModel);
        tabRegion.Add(ouvm);
        if (ouvm.IsActive) tabRegion.Activate(ouvm);
      }

      base.OnContextViewModelChanged(context);
    }

    protected override void ExecuteOperation(Operation op, BusinessObject argument)
    {
      switch (op.Identifier)
      {
        case Operations.AddUser:
          User u = new User();
          DataContext.Data.Add(u);
          SelectContextViewModel(u);
          FocusViewModel<UserDetailViewModel>(u);
          break;

        case Operations.SetPassword:
          SetPasswordController spc = CreateChildController<SetPasswordController>();
          spc.DataContext = (User)argument;
          spc.Run();
          break;
      }
      base.ExecuteOperation(op, argument);
    }
  }
}

﻿using Afx.Business.Security;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Activities.UserManagement
{
  public class UserManagementViewModel : MdiContextualActivityViewModel<UserManagement>
  {
    [InjectionConstructor]
    public UserManagementViewModel(IController controller)
      : base(controller)
    {
    }

    public IEnumerable<UserItemViewModel> Users
    {
      get
      {
        if (Model == null) return null;
        return this.ResolveViewModels<UserItemViewModel, User>(Model.Data);
      }
    }

    protected override void OnLoaded()
    {
      base.OnLoaded();
    }
  }
}

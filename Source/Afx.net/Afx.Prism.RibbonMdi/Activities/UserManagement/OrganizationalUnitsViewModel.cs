﻿using Afx.Business;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Security;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Afx.Prism.RibbonMdi.Activities.UserManagement
{
  public class OrganizationalUnitsViewModel : ActivityTabDetailViewModel<User>
  {
    #region Constructors

    [InjectionConstructor]
    public OrganizationalUnitsViewModel(IController controller)
      : base(controller)
    {
      Title = "_Organizational Units";
    }

    #endregion

    #region DelegateCommand KeyDownCommand

    DelegateCommand<KeyEventArgs> mKeyDownCommand;
    public DelegateCommand<KeyEventArgs> KeyDownCommand
    {
      get { return mKeyDownCommand ?? (mKeyDownCommand = new DelegateCommand<KeyEventArgs>(ExecuteKeyDown)); }
    }

    void ExecuteKeyDown(KeyEventArgs e)
    {
      if (e.Key != Key.Space) return;
      if (SelectedOrganizationalUnitItemViewModel != null) SelectedOrganizationalUnitItemViewModel.HasOrganizationalUnit = !SelectedOrganizationalUnitItemViewModel.HasOrganizationalUnit;
      e.Handled = true;
    }

    #endregion

    #region ComboBox

    #region BasicCollection<OrganizationalUnit> UserOrganizationalUnits

    BasicCollection<OrganizationalUnit> mUserOrganizationalUnits;
    public BasicCollection<OrganizationalUnit> UserOrganizationalUnits
    {
      get
      {
        if (mUserOrganizationalUnits == null)
        {
          mUserOrganizationalUnits = new BasicCollection<OrganizationalUnit>();
          //mUserOrganizationalUnits.Add(new OrganizationalUnit(true));
          foreach (var ou in Model.OrganizationalUnits.OrderBy((ou) => ou.Name)) mUserOrganizationalUnits.Add(ou);
        }
        return mUserOrganizationalUnits;
      }
    }

    #endregion

    #region OrganizationalUnit DefaultOrganizationalUnit

    public OrganizationalUnit DefaultOrganizationalUnit
    {
      get
      {
        if (Model == null) return default(OrganizationalUnit);
        return Model.DefaultOrganizationalUnit;
      }
      set { Model.DefaultOrganizationalUnit = value; }
    }

    #endregion

    #region void OnModelCompositionChanged(CompositionChangedEventArgs e)

    protected override void OnModelCompositionChanged(CompositionChangedEventArgs e)
    {
      if (e.CompositionChangedType == CompositionChangedType.ChildAdded || e.CompositionChangedType == CompositionChangedType.ChildRemoved)
      {
        if (e.Item is OrganizationalUnit)
        {
          mUserOrganizationalUnits = null;
          OnPropertyChanged("UserOrganizationalUnits");
          OnPropertyChanged("DefaultOrganizationalUnit");
        }
      }
      base.OnModelCompositionChanged(e);
    }

    #endregion

    #endregion

    #region ListView

    #region BasicCollection<OrganizationalUnit> OrganizationalUnits

    BasicCollection<OrganizationalUnit> mOrganizationalUnits;
    public BasicCollection<OrganizationalUnit> OrganizationalUnits
    {
      get { return mOrganizationalUnits ?? (mOrganizationalUnits = Cache.Instance.GetObjects<OrganizationalUnit>((ou) => ou.UserAssignable)); }
    }

    #endregion

    #region IEnumerable<OrganizationalUnitItemViewModel> OrganizationalUnitViewModels

    public IEnumerable<OrganizationalUnitItemViewModel> OrganizationalUnitViewModels
    {
      get
      {
        if (Model == null) return null;
        return this.ResolveViewModels<OrganizationalUnitItemViewModel, OrganizationalUnit>(OrganizationalUnits);
      }
    }

    #endregion

    #region OrganizationalUnitItemViewModel SelectedOrganizationalUnitItemViewModel

    OrganizationalUnitItemViewModel mSelectedOrganizationalUnitItemViewModel;
    public OrganizationalUnitItemViewModel SelectedOrganizationalUnitItemViewModel
    {
      get { return mSelectedOrganizationalUnitItemViewModel; }
      set { mSelectedOrganizationalUnitItemViewModel = value; }
    }

    #endregion

    #endregion
  }
}

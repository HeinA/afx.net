﻿using Afx.Business.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Activities.UserManagement
{
  public class SetPasswordController : MdiDialogController<User, SetPasswordViewModel>
  {
    public const string SetPasswordControllerKey = "Set Password Controller";

    public SetPasswordController(IController controller)
      : base(SetPasswordControllerKey, controller)
    {
    }

    protected override bool OnRun()
    {
      ViewModel.Caption = "Set Password";
      return base.OnRun();
    }

    protected override void ApplyChanges()
    {
      DataContext.PasswordHash = ViewModel.Password;

      base.ApplyChanges();
    }
  }
}

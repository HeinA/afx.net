﻿using Afx.Business.Security;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Activities.UserManagement
{
  public class RoleItemViewModel : SelectableViewModel<Role>
  {
    #region Constructors

    [InjectionConstructor]
    public RoleItemViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    #region RolesViewModel Parent

    public new RolesViewModel Parent
    {
      get { return (RolesViewModel)base.Parent; }
    }

    #endregion

    #region string Name

    public string Name
    {
      get
      {
        if (Model == null) return default(string);
        return Model.Name;
      }
      set { Model.Name = value; }
    }

    #endregion

    #region bool IsSelected

    public override bool IsSelected
    {
      get
      {
        if (base.IsSelected)
        {
          Parent.SelectedRoleItemViewModel = this;
        }
        return base.IsSelected;
      }
      set
      {
        base.IsSelected = value;
        if (base.IsSelected)
        {
          Parent.SelectedRoleItemViewModel = this;
        }
      }
    }

    #endregion 

    #region bool HasRole

    public bool HasRole
    {
      get { return Parent.Model.Roles.Contains(Model); }
      set
      {
        if (value) Parent.Model.Roles.Add(Model);
        else Parent.Model.Roles.Remove(Model);
        OnPropertyChanged("HasRole");
      }
    }

    #endregion
  }
}

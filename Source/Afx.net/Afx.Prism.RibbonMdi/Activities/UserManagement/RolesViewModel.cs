﻿using Afx.Business;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Security;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Afx.Prism.RibbonMdi.Activities.UserManagement
{
  public class RolesViewModel : ActivityTabDetailViewModel<User>
  {
    #region Constructors

    [InjectionConstructor]
    public RolesViewModel(IController controller)
      : base(controller)
    {
      Title = "_Roles";
    }

    #endregion

    #region DelegateCommand KeyDownCommand

    DelegateCommand<KeyEventArgs> mKeyDownCommand;
    public DelegateCommand<KeyEventArgs> KeyDownCommand
    {
      get { return mKeyDownCommand ?? (mKeyDownCommand = new DelegateCommand<KeyEventArgs>(ExecuteKeyDown)); }
    }

    void ExecuteKeyDown(KeyEventArgs e)
    {
      if (e.Key != Key.Space) return;
      if (SelectedRoleItemViewModel != null) SelectedRoleItemViewModel.HasRole = !SelectedRoleItemViewModel.HasRole;
      e.Handled = true;
    }

    #endregion

    #region ListView

    #region BasicCollection<Role> Roles

    BasicCollection<Role> mRoles;
    public BasicCollection<Role> Roles
    {
      get { return mRoles ?? (mRoles = Cache.Instance.GetObjects<Role>((r) => r.Name, true)); }
    }

    #endregion

    #region IEnumerable<RoleItemViewModel> RoleViewModels

    public IEnumerable<RoleItemViewModel> RoleViewModels
    {
      get
      {
        if (Model == null) return null;
        return this.ResolveViewModels<RoleItemViewModel, Role>(Roles);
      }
    }

    #endregion

    #region RoleItemViewModel SelectedRoleItemViewModel

    RoleItemViewModel mSelectedRoleItemViewModel;
    public RoleItemViewModel SelectedRoleItemViewModel
    {
      get { return mSelectedRoleItemViewModel; }
      set { mSelectedRoleItemViewModel = value; }
    }

    #endregion

    #endregion
  }
}

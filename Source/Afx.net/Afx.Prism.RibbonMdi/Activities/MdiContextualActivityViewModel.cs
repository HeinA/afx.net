﻿using Afx.Business;
using Afx.Business.Activities;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Unity;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace Afx.Prism.RibbonMdi.Activities
{
  public abstract class MdiContextualActivityViewModel<TModel> : MdiActivityViewModel<TModel>
    where TModel : ContextBase, IContextualActivityContext
  {
    protected MdiContextualActivityViewModel(IController controller)
      : base(controller)
    {
    }

    [Dependency]
    public IMdiContextualActivityController ActivityController { get; set; }

    ObservableCollection<MdiOperationViewModel> mContextOperations;
    public IEnumerable ContextOperations
    {
      get { return mContextOperations; }
    }

    internal void ResetContextOperations()
    {
      mContextOperations = new ObservableCollection<MdiOperationViewModel>();
      ResetOperations(mContextOperations, Model.Activity.ContextOperations);
      OnPropertyChanged("ContextOperations");
    }

    public override void ResetOperations()
    {
      ResetContextOperations();
      base.ResetOperations();
    }

    internal override void ClearOperations()
    {
      mContextOperations = null;
      base.ClearOperations();
    }

    protected override void ResetOperations(ObservableCollection<MdiOperationViewModel> operationViewModels, IEnumerable<Operation> operations)
    {
      OperationGroup group = null;
      foreach (var op in operations.Where((o) => ActivityController.IsOperationVisible(o)))
      {
        if (group != null && !op.Owner.Equals(group) && operationViewModels.Count > 0 && !(operationViewModels[operationViewModels.Count - 1] is MdiOperationSeperatorViewModel)) operationViewModels.Add(Controller.CreateViewModel<MdiOperationSeperatorViewModel>(this));
        group = op.Owner;
        foreach (Operation opr in ActivityController.ReplaceOperation(op))
        {
          MdiOperationViewModel vm = Controller.GetCreateViewModel<MdiOperationViewModel>(opr, this);
          if (opr is ContextOperation) vm.TextOverride = ActivityController.GetOperationText(opr);
          operationViewModels.Add(vm);

        }
      } 

      if (operationViewModels.Count > 0 && operationViewModels[operationViewModels.Count - 1] is MdiOperationSeperatorViewModel)
      {
        operationViewModels.RemoveAt(operationViewModels.Count - 1);
      }

      OnPropertyChanged("ToolbarVisibility");
    }

    #region DelegateCommand ContextMenuOpeningCommand

    public DelegateCommand<ContextMenuEventArgs> mContextMenuOpeningCommand;
    public DelegateCommand<ContextMenuEventArgs> ContextMenuOpeningCommand
    {
      get { return mContextMenuOpeningCommand ?? (mContextMenuOpeningCommand = new DelegateCommand<ContextMenuEventArgs>(OnContextMenuOpening)); }
    }

    void OnContextMenuOpening(ContextMenuEventArgs e)
    {
      try
      {
        ActivityController.ConfigureContextMenu();
        if (mContextOperations.Count == 0 || ActivityController.ContextViewModel == null) e.Handled = true;
      }
      catch
      {
        throw;
      }
    }

    #endregion
  }
}

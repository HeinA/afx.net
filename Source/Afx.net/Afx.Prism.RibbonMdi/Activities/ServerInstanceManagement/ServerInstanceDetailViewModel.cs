﻿using Afx.Business.Security;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Activities.ServerInstanceManagement
{
  public class ServerInstanceDetailViewModel : ActivityDetailViewModel<ServerInstance>
  {
    [InjectionConstructor]
    public ServerInstanceDetailViewModel(IController controller)
      : base(controller)
    {
    }

    #region Model Propertry string Name

    public string Name
    {
      get
      {
        if (Model == null) return default(string);
        return Model.Name;
      }
      set { Model.Name = value; }
    }

    #endregion

    #region Model Propertry string Identifier

    public string Identifier
    {
      get
      {
        if (Model == null) return default(string);
        return Model.Identifier;
      }
    }

    #endregion
  }
}

﻿using Afx.Business.Security;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Activities.ServerInstanceManagement
{
  public class ServerInstanceManagementViewModel : MdiContextualActivityViewModel<ServerInstanceManagement>
  {
    [InjectionConstructor]
    public ServerInstanceManagementViewModel(IController controller)
      : base(controller)
    {
    }

    public IEnumerable<ServerInstanceNodeViewModel> Servers
    {
      get
      {
        if (Model == null) return null;
        return this.ResolveViewModels<ServerInstanceNodeViewModel, ServerInstance>(Model.Data);
      }
    }
  }
}

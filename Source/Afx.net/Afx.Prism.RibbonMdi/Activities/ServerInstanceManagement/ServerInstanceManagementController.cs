﻿using Afx.Business;
using Afx.Business.Security;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Activities.ServerInstanceManagement
{
  public class ServerInstanceManagementController : MdiContextualActivityController<ServerInstanceManagement, ServerInstanceManagementViewModel>
  {
    [InjectionConstructor]
    public ServerInstanceManagementController(IController controller)
      : base(controller)
    {
    }

    protected override void OnLoaded()
    {
      if (DataContext.Data.Count > 0)
      {
        //Focus the first element 
        ServerInstanceNodeViewModel nvm = GetCreateViewModel<ServerInstanceNodeViewModel>(DataContext.Data[0], ViewModel);
        nvm.IsFocused = true;
      }

      RegisterContextRegion(RegionManager.Regions[ServerInstanceManagement.ContextRegion]);

      base.OnLoaded();
    }

    protected override void OnContextViewModelChanged(ISelectableViewModel context)
    {
      if (context.Model is ServerInstance)
      {
        ServerInstanceDetailViewModel vm = GetCreateViewModel<ServerInstanceDetailViewModel>(context.Model, ViewModel);
        RegionManager.Regions[ServerInstanceManagement.ContextRegion].Add(vm);
      }

      base.OnContextViewModelChanged(context);
    }

    public override bool IsOperationVisible(Operation op)
    {
      // Only allow one root element
      if (op.Identifier == Operations.AddMasterServer && DataContext.Data.Count > 0) return false;
      return base.IsOperationVisible(op);
    }

    protected override void ExecuteOperation(Operation op, BusinessObject argument)
    {
      switch (op.Identifier)
      {
        case Operations.AddMasterServer:
          {
            ServerInstance si = new ServerInstance();
            DataContext.Data.Add(si);
            SelectContextViewModel(si);
            FocusViewModel<ServerInstanceDetailViewModel>(si);
          }
          break;

        case Operations.AddSatelliteServer:
          {
            ServerInstance si = (ServerInstance)argument;
            ServerInstance newSi = new ServerInstance();
            si.Satellites.Add(newSi);
            SelectContextViewModel(newSi);
            FocusViewModel<ServerInstanceDetailViewModel>(newSi);
          }
          break;
      }
      base.ExecuteOperation(op, argument);
    }
  }
}

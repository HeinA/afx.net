﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Activities.ServerInstanceManagement
{
  public class Operations : StandardOperations
  {
    public const string AddMasterServer = "{d827fd12-8c83-4b07-bb33-cbb2b62f8dd6}";
    public const string AddSatelliteServer = "{3786330a-3a4f-4e8f-855f-16c343ce6fd8}";
  }
}

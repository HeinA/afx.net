﻿using Afx.Business.Activities;
using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Business.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Activities.ServerInstanceManagement
{
  public class ServerInstanceManagement : ContextualActivityContext<ServerInstance>
  {
    public const string Key = "{be7fc5bb-cdb2-4bc2-bbf6-d7dba1a2e197}";
    public const string ContextRegion = "ContextRegion";

    public ServerInstanceManagement()
    {
    }

    public ServerInstanceManagement(ContextualActivity activity)
      : base(activity)
    {
    }

    public override void LoadData()
    {
      using (var svc = ProxyFactory.GetService<IAfxService>(SecurityContext.MasterServer.Name))
      {
        Data = svc.LoadServerInstances();
      }
      base.LoadData();
    }

    public override void SaveData()
    {
      using (var svc = ProxyFactory.GetService<IAfxService>(SecurityContext.MasterServer.Name))
      {
        Data = svc.SaveServerInstances(Data);
      }
      base.SaveData();
    }
  }
}

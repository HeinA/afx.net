﻿using Afx.Business.Security;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Activities.ServerInstanceManagement
{
  public class ServerInstanceNodeViewModel : MdiNavigationTreeNodeViewModel<ServerInstance>
  {
    [InjectionConstructor]
    public ServerInstanceNodeViewModel(IController controller)
      : base(controller)
    {
    }

    public string Name
    {
      get
      {
        if (Model == null) return string.Empty;
        if (string.IsNullOrWhiteSpace(Model.Name)) return "*** Unnamed ***";
        return Model.Name;
      }
    }

    public IEnumerable<ServerInstanceNodeViewModel> Satellites
    {
      get { return this.ResolveViewModels<ServerInstanceNodeViewModel, ServerInstance>(Model.Satellites); }
    }
  }
}

﻿using Afx.Business;
using Afx.Business.Activities;
using Afx.Business.Security;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Unity;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace Afx.Prism.RibbonMdi.Activities
{
  public abstract class MdiActivityViewModel<TModel> : MdiDockableDocumentViewModel<TModel>
    where TModel : ContextBase, IActivityContext
  {
    protected MdiActivityViewModel(IController controller)
      : base(controller)
    {
    }

    protected override void OnModelChanged()
    {
      if (Model != null) Title = Model.Title;
      else Title = string.Empty;

      base.OnModelChanged();
    }

    #region string Error

    public new string Error
    {
      get
      {
        if (Model == null) return null;
        return Model.Error;
      }
    }

    #endregion

    #region bool HasError

    public bool HasError
    {
      get
      {
        if (Model == null) return false;
        return Model.HasError;
      }
    }

    #endregion

    public string TitleText
    {
      get { return Model.TitleText; }
    }

    protected override void OnModelPropertyChanged(string propertyName)
    {
      if (propertyName == "Title")
      {
        Title = Model.Title;
      }

      if (propertyName == "Activity")
      {
        ResetOperations();
      }

      base.OnModelPropertyChanged(propertyName);
    }

    protected override IEnumerable<Operation> GetGlobalOperations()
    {
      return Model.Activity.GlobalOperations;
    }

    public override bool IsReadOnly
    {
      get { return Model.IsReadOnly; }
    }
  }
}

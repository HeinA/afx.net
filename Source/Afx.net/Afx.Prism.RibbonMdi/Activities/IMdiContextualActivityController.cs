﻿using Afx.Business;
using Afx.Business.Activities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Activities
{
  public interface IMdiContextualActivityController : IMdiActivityController, IContextController
  {
    ISelectableViewModel ContextViewModel { get; }
    void ConfigureContextMenu();
    Operation[] ReplaceOperation(Operation op);
    string GetOperationText(Operation op);
  }
}

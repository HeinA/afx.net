﻿using Afx.Business.Activities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Activities
{
  public abstract class MdiOperationViewModelBase : ViewModel
  {
    protected MdiOperationViewModelBase(IController controller)
      : base(controller)
    {
    }
  }
}

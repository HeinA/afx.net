﻿using Afx.Business.Activities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Activities
{
  public interface IActivityContext : IContextBase
  {
    Activity Activity { get; set; }
    Guid ContextId { get; set; }
  }
}

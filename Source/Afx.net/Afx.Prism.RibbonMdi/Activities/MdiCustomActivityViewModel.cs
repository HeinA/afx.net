﻿using Afx.Business;
using Afx.Business.Activities;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Unity;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace Afx.Prism.RibbonMdi.Activities
{
  public abstract class MdiCustomActivityViewModel<TModel> : MdiActivityViewModel<TModel>
    where TModel : ContextBase, ICustomActivityContext
  {
    protected MdiCustomActivityViewModel(IController controller)
      : base(controller)
    {
    }

    [Dependency]
    public IMdiActivityController ActivityController { get; set; }
  }
}

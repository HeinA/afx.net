﻿using Afx.Business;
using Afx.Business.Activities;
using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Business.ServiceModel;
using Afx.Prism.Events;
using Afx.Prism.RibbonMdi.Events;
using Afx.Prism.RibbonMdi.Login;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Microsoft.Practices.ObjectBuilder2;
using Microsoft.Practices.Prism.PubSubEvents;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.ServiceModel;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;

namespace Afx.Prism.RibbonMdi.Activities
{
  public abstract class MdiContextualActivityController<TModel, TViewModel> : MdiActivityController<TModel, TViewModel>, IMdiContextualActivityController//, IBuilderAware
    where TModel : ContextBase, IContextualActivityContext
    where TViewModel : MdiContextualActivityViewModel<TModel>
  {
    protected MdiContextualActivityController(IController controller)
      : base(controller)
    {
    }

    protected MdiContextualActivityController(string key, IController controller)
      : base(key, controller)
    {
    }

    protected override void ConfigureContainer()
    {
      Container.RegisterInstance<IMdiContextualActivityController>(this);
      base.ConfigureContainer();
    }

    Collection<IRegion> mContextRegions = new Collection<IRegion>();
    protected IEnumerable<IRegion> ContextRegions
    {
      get { return mContextRegions; }
    }

    protected void RegisterContextRegion(IRegion region)
    {
      mContextRegions.Add(region);
    }

    public ISelectableViewModel ContextViewModel { get; protected set; }

    ISelectableViewModel IMdiContextualActivityController.ContextViewModel
    {
      get { return ContextViewModel; }
    }

    #region OnContextViewModelChanged(ISelectableViewModel context)

    void IContextController.SetContextViewModel(ISelectableViewModel context)
    {
      if (ContextViewModel == context) return;
      context.IsFocused = true;
      ContextViewModel = context;
      ClearContextRegions();
      OnContextViewModelChanged(context);
    }

    protected virtual void OnContextViewModelChanged(ISelectableViewModel context)
    {
      ContextChangedEventArgs args = new ContextChangedEventArgs(this, context.Model as BusinessObject, RegionManager);
      ApplicationController.EventAggregator.GetEvent<ContextChangedEvent>().Publish(args);
    }

    protected void ClearContextRegions()
    {
      if (ContextRegions == null) return;
      try
      {
        SetState<bool>(TabViewModel.SuspendSelection, true);
        foreach (IRegion region in ContextRegions)
        {
          List<object> list = region.Views.ToList<object>();
          for (int i = list.Count - 1; i >= 0; i--)
          {
            object o = list[i];
            if (typeof(IViewModel).IsAssignableFrom(o.GetType())) region.Remove(o);
          }
        }
      }
      finally
      {
        SetState<bool>(TabViewModel.SuspendSelection, false);
      }
    }

    #endregion

    public void ConfigureContextMenu()
    {
      if (ContextViewModel == null) return;
      ViewModel.ResetContextOperations();
    }

    public override bool IsOperationVisible(Operation op)
    {
      if (!DataContext.Activity.CanEdit) return false;
      if (!DataContext.Activity.CanExecute(op)) return false;

      ContextOperation cop = op as ContextOperation;
      if (cop != null && ContextViewModel != null)
      {
        if (cop.Identifier == StandardOperations.ToggleDelete && !ContextViewModel.MayDelete)
        {
          return false;
        }

        if (cop.ContextType == null || cop.ContextType.IsAssignableFrom(ContextViewModel.Model.GetType()))
        {
          return true;
        }
        else
        {
          return false;
        }
      }

      return base.IsOperationVisible(op);
    }

    public virtual string GetOperationText(Operation op)
    {
      if (ContextViewModel == null) return op.Text;

      if (op.Identifier == StandardOperations.ToggleDelete)
      {
        if (ContextViewModel.Model.IsDeleted) return "Undelete";
        else return "Delete";
      }
      return op.Text;
    }

    public void FocusContextViewModel(BusinessObject model)
    {
      SelectContextViewModel(model).IsFocused = true;
    }

    public ISelectableViewModel SelectContextViewModel(BusinessObject model)
    {
      ISelectableViewModel vm = (ISelectableViewModel)GetViewModel(typeof(ISelectableViewModel), model);
      vm.IsSelected = true;

      ITreeNodeViewModel tvm = vm as ITreeNodeViewModel;
      if (tvm != null)
      {
        IBusinessObject owner = ((IBusinessObject)model).Owner;
        while (owner != null)
        {
          tvm = (ITreeNodeViewModel)GetViewModel(typeof(ITreeNodeViewModel), owner);
          tvm.IsExpanded = true;
          owner = owner.Owner;
        }
      }

      return vm;
    }

    protected override BusinessObject GetArgument()
    {
      if (ContextViewModel == null) return null;
      return ContextViewModel.Model as BusinessObject;
    }

    protected override void ExecuteOperation(Operation op, BusinessObject argument)
    {
      switch (op.Identifier)
      {
        case StandardOperations.ToggleDelete:
          ContextViewModel.Model.IsDeleted = !ContextViewModel.Model.IsDeleted;
          break;
      }

      base.ExecuteOperation(op, argument);
    }

    public virtual Operation[] ReplaceOperation(Operation op)
    {
      Collection<Operation> ops = new Collection<Operation>();
      ops.Add(op);
      return ops.ToArray();
    }

    protected override void OnCacheUpdated()
    {
      ViewModel.ResetOperations();
      base.OnCacheUpdated();
    }

    protected override void LoadDataContext()
    {
      DataContext.LoadData();
      DataContextUpdated();
      base.LoadDataContext();
    }

    protected override void SaveDataContext()
    {
      if (!DataContext.Validate())
      {
        System.Media.SystemSounds.Hand.Play();
        return;
      }
      DataContext.SaveData();
      DataContextUpdated();
      base.SaveDataContext();
    }

    protected void DataContextUpdated()
    {
      PurgeViewModels();
      ViewModel.ResetOperations();
      ResetViewModelCollections();
    }

    public override void Terminate()
    {
      ViewModel.ClearOperations();
      ClearContextRegions();
      base.Terminate();
    }

    void ResetViewModelCollections()
    {
      ViewModel.ResetViewModelCollections();
      ViewModel.IsFocused = true;
    }
  }
}

﻿using Afx.Business;
using Afx.Business.Activities;
using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Business.ServiceModel;
using Afx.Prism.Events;
using Afx.Prism.RibbonMdi.Events;
using Afx.Prism.RibbonMdi.Login;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Microsoft.Practices.ObjectBuilder2;
using Microsoft.Practices.Prism.PubSubEvents;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.ServiceModel;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;

namespace Afx.Prism.RibbonMdi.Activities
{
  public abstract class MdiCustomActivityController<TModel, TViewModel> : MdiActivityController<TModel, TViewModel>
    where TModel : ContextBase, ICustomActivityContext
    where TViewModel : MdiCustomActivityViewModel<TModel>
  {
    protected MdiCustomActivityController(IController controller)
      : base(controller)
    {
    }

    protected MdiCustomActivityController(string key, IController controller)
      : base(key, controller)
    {
    }

    protected override void ConfigureContainer()
    {
      Container.RegisterInstance<IMdiActivityController>(this);
      base.ConfigureContainer();
    }

    public override bool IsOperationVisible(Operation op)
    {
      if (!DataContext.Activity.CanEdit) return false;
      if (!DataContext.Activity.CanExecute(op)) return false;

      return base.IsOperationVisible(op);
    }

    protected override void OnCacheUpdated()
    {
      ViewModel.ResetOperations();
      base.OnCacheUpdated();
    }

    protected override void LoadDataContext()
    {
      DataContext.LoadData();
      DataContextUpdated();
      base.LoadDataContext();
    }

    protected override void SaveDataContext()
    {
      if (!DataContext.Validate())
      {
        System.Media.SystemSounds.Hand.Play();
        return;
      }
      DataContext.SaveData();
      DataContextUpdated();
      base.SaveDataContext();
    }

    protected virtual void DataContextUpdated()
    {
      PurgeViewModels();
      ViewModel.ResetOperations();
      ResetViewModelCollections();
    }

    public override void Terminate()
    {
      ViewModel.ClearOperations();
      base.Terminate();
    }

    void ResetViewModelCollections()
    {
      ViewModel.ResetViewModelCollections();
      ViewModel.IsFocused = true;
    }
  }
}

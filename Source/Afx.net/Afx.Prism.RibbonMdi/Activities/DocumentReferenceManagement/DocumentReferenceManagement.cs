﻿using Afx.Business.Activities;
using Afx.Business;
using Afx.Business.Service;
using Afx.Prism.RibbonMdi.Activities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Activities.DocumentReferenceManagement
{
  public partial class DocumentReferenceManagement : SimpleActivityContext<DocumentReference>
  {
    public DocumentReferenceManagement()
    {
    }

    public DocumentReferenceManagement(ContextualActivity activity)
      : base(activity)
    {
    }
  }
}

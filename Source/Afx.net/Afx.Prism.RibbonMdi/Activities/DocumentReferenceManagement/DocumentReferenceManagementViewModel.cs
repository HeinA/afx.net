﻿using Afx.Business.Security;
using Afx.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Activities;

namespace Afx.Prism.RibbonMdi.Activities.DocumentReferenceManagement
{
  public partial class DocumentReferenceManagementViewModel : MdiSimpleActivityViewModel<DocumentReferenceManagement>
  {
    #region Constructors

    [InjectionConstructor]
    public DocumentReferenceManagementViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion
  }
}

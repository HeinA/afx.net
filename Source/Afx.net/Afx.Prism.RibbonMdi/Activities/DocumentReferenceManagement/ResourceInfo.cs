﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Activities.DocumentReferenceManagement
{
  [Export(typeof(Afx.Prism.RibbonMdi.ResourceInfo))]
  public class ResourceInfo : Afx.Prism.RibbonMdi.ResourceInfo
  {
    public override Uri GetUri()
    {
      return new Uri("pack://application:,,,/Afx.Prism.RibbonMdi;component/Activities/DocumentReferenceManagement/Resources.xaml", UriKind.Absolute);
    }
  }
}
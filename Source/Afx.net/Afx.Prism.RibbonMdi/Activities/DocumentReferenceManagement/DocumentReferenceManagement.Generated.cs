﻿using Afx.Business;
using Afx.Business.Service;
using Afx.Business.Service;
using Afx.Prism.RibbonMdi.Activities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.Composition;
using Afx.Business.Security;

namespace Afx.Prism.RibbonMdi.Activities.DocumentReferenceManagement
{
  //**************************************************************************************************************
  //**************************************************************************************************************
  //*                                                                                                            *
  //*     This code is auto-generated.  Do not place any cutom code here, it will be lost on re-generation.      *
  //*                                                                                                            *
  //**************************************************************************************************************
  //**************************************************************************************************************
  [Export("Context:" + DocumentReferenceManagement.Key)]
  public partial class DocumentReferenceManagement
  {
    public const string Key = "{156b5e17-c6d8-4beb-9ab0-15bcec2ded62}";

    public override void LoadData()
    {
      using (var svc = ProxyFactory.GetService<IAfxService>(SecurityContext.MasterServer.Name))
      {
        Data = svc.LoadDocumentReferenceCollection();
      }
      base.LoadData();
    }

    public override void SaveData()
    {
      using (var svc = ProxyFactory.GetService<IAfxService>(SecurityContext.MasterServer.Name))
      {
        Data = svc.SaveDocumentReferenceCollection(Data);
      }
      base.SaveData();
    }
  }
}

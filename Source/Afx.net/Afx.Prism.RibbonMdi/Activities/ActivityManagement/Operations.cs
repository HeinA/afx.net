﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Activities.ActivityManagement
{
  public class Operations : StandardOperations
  {
    public const string AddTab = "{ad83c996-26ec-428f-b01b-55ccea8fb32e}";
    public const string AddGroup = "{d16351a5-0605-4d3f-932c-ecf97224b38d}";
    public const string AddSimpleActivity = "{9781e245-3330-45b7-af8f-c0059c40c984}";
    public const string AddContextualActivity = "{b6837b12-7d1d-4898-ab8a-52233d99cdf8}";
    public const string AddOperation = "{9211d144-39f4-4dcc-aec8-4fce3762e15d}";
  }
}

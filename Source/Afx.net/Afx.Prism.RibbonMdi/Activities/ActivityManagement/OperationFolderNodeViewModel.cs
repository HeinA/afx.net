﻿using Afx.Business.Activities;
using Afx.Business.Collections;
using Microsoft.Practices.Unity;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Activities.ActivityManagement
{
  public class OperationFolderNodeViewModel : MdiNavigationTreeNodeViewModel<OperationFolder>
  {
    [InjectionConstructor]
    public OperationFolderNodeViewModel(IController controller)
      : base(controller)
    {
    }

    string mName;
    public string Name
    {
      get { return mName; }
      set { SetProperty<string>(ref mName, value); }
    }

    public override bool MayDelete
    {
      get { return false; }
    }
  }
}

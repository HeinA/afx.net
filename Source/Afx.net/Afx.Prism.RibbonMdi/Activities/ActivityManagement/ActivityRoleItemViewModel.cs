﻿using Afx.Business.Activities;
using Afx.Business.Security;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Activities.ActivityManagement
{
  public class ActivityRoleItemViewModel : SelectableViewModel<Role>
  {
    [InjectionConstructor]
    public ActivityRoleItemViewModel(IController controller)
      : base(controller)
    {
    }

    new ActivityRolesDetailViewModel Parent
    {
      get { return (ActivityRolesDetailViewModel)base.Parent; }
    }

    Activity Activity
    {
      get { return Parent.Model; }
    }

    public override bool IsSelected
    {
      get
      {
        if (base.IsSelected) Parent.SelectedRoleViewModel = this;
        return base.IsSelected;
      }
      set
      {
        base.IsSelected = value;
        if (base.IsSelected) Parent.SelectedRoleViewModel = this;
        else if (Parent.SelectedRoleViewModel == this) Parent.SelectedRoleViewModel = null;
      }
    }

    public void UpdatePermissions()
    {
      OnPropertyChanged("Permissions");
    }

    #region string Name

    public string Name
    {
      get
      {
        if (Model == null) return default(string);
        return Model.Name;
      }
      set { Model.Name = value; }
    }

    #endregion

    public string Permissions
    {
      get
      {
        if (Activity.Roles.Contains(Model))
        {
          ActivityRole ar = Activity.GetAssociativeObject<ActivityRole>(Model);
          if (ar.EditPermission) return "Edit";
          return "View";
        }
        return string.Empty;
      }
    }
  }
}

﻿using Afx.Business;
using Afx.Business.Activities;
using Afx.Business.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Activities.ActivityManagement
{
  public class EditPermissionsContext : BusinessObject
  {
    public EditPermissionsContext(Activity activity)
    {
      Activity = activity;
    }

    Activity mActivity;
    public Activity Activity
    {
      get { return mActivity; }
      private set { mActivity = value; }
    }
  }
}

﻿using Afx.Business.Activities;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Activities.ActivityManagement
{
  public class ActivityManagementViewModel : MdiContextualActivityViewModel<ActivityManagement>
  {
    [InjectionConstructor]
    public ActivityManagementViewModel(IController controller)
      : base(controller)
    {
    }

    public IEnumerable<ActivityTabNodeViewModel> Tabs
    {
      get
      {
        if (Model == null) return null;
        return this.ResolveViewModels<ActivityTabNodeViewModel, ActivityTab>(Model.Data);
      }
    }
  }
}

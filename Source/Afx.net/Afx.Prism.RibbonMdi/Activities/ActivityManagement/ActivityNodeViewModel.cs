﻿using Afx.Business.Activities;
using Afx.Business.Collections;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Activities.ActivityManagement
{
  public class ActivityNodeViewModel : MdiNavigationTreeNodeViewModel<Activity>
  {
    [InjectionConstructor]
    public ActivityNodeViewModel(IController controller)
      : base(controller)
    {
    }

    public string Name
    {
      get
      {
        if (Model == null) return string.Empty;
        if (string.IsNullOrWhiteSpace(Model.Name)) return "*** Unnamed ***";
        return Model.Name;
      }
    }

    BasicCollection<OperationFolderNodeViewModel> mOperationFolders;
    public IEnumerable<OperationFolderNodeViewModel> OperationFolders
    {
      get
      {
        if (mOperationFolders == null)
        {
          mOperationFolders = new BasicCollection<OperationFolderNodeViewModel>();

          Guid guid = GetState<Guid>(Guid.NewGuid(), "GlobalOperations");
          OperationFolderNodeViewModel f = Controller.GetCreateViewModel<OperationFolderNodeViewModel>(new OperationFolder(guid, Model, false, Model.GlobalOperations), this);
          f.Name = "Global Operations";
          mOperationFolders.Add(f);

          ContextualActivity ca = Model as ContextualActivity;
          if (ca != null)
          {
            guid = GetState<Guid>(Guid.NewGuid(), "ContextOperations");
            f = Controller.GetCreateViewModel<OperationFolderNodeViewModel>(new OperationFolder(guid, Model, true, ca.ContextOperations), this);
            f.Name = "Context Operations";
            mOperationFolders.Add(f);
          }
        }
        return mOperationFolders;
      }
    }
  }
}

﻿using Afx.Business;
using Afx.Business.Collections;
using Afx.Business.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Activities.ActivityManagement
{
  public class RoleSelectionContext : BusinessObject
  {
    public RoleSelectionContext(BasicCollection<Role> roles)
    {
      OperationRoles = roles;
      Roles = new BasicCollection<Role>(roles);
    }

    BasicCollection<Role> mOperationRoles;
    public BasicCollection<Role> OperationRoles
    {
      get { return mOperationRoles; }
      private set { mOperationRoles = value; }
    }

    BasicCollection<Role> mRoles;
    public BasicCollection<Role> Roles
    {
      get { return mRoles; }
      private set { mRoles = value; }
    }
  }
}

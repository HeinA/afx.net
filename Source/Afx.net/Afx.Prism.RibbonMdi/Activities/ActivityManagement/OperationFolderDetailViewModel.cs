﻿using Afx.Business;
using Afx.Business.Activities;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Afx.Prism.RibbonMdi.Activities.ActivityManagement
{
  public class OperationFolderDetailViewModel : ActivityDetailViewModel<OperationFolder>
  {
    [InjectionConstructor]
    public OperationFolderDetailViewModel(IController controller)
      : base(controller)
    {
    }

    public IEnumerable<OperationFolderItemViewModel> Operations
    {
      get { return ResolveViewModels<OperationFolderItemViewModel, Operation>(Model.Operations); }
    }

    protected override void OnModelChanged()
    {
      //((INotifyCollectionChanged)Model.Operations).CollectionChanged += OperationFolderDetailViewModel_CollectionChanged;
      base.OnModelChanged();
    }

    //void OperationFolderDetailViewModel_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
    //{
    //}

    OperationFolderItemViewModel mSelectedOperationViewModel;
    public OperationFolderItemViewModel SelectedOperationViewModel
    {
      get { return mSelectedOperationViewModel; }
      set
      {
        if (SetProperty<OperationFolderItemViewModel>(ref mSelectedOperationViewModel, value))
        {
          MoveUpCommand.RaiseCanExecuteChanged();
          MoveDownCommand.RaiseCanExecuteChanged();
          EditRolesCommand.RaiseCanExecuteChanged();
          DeleteOperationCommand.RaiseCanExecuteChanged();
        }
      }
    }

    #region DelegateCommand MoveUpCommand

    DelegateCommand mMoveUpCommand;
    public DelegateCommand MoveUpCommand
    {
      get { return mMoveUpCommand ?? (mMoveUpCommand = new DelegateCommand(ExecuteMoveUp, CanExecuteMoveUp)); }
    }

    bool CanExecuteMoveUp()
    {
      if (SelectedOperationViewModel == null) return false;
      if (Model.Operations.IndexOf(SelectedOperationViewModel.Model) == 0) return false;
      return true;
    }

    void ExecuteMoveUp()
    {
      int i = Model.Operations.IndexOf(SelectedOperationViewModel.Model);
      Model.Operations.RemoveAt(i);
      Model.Operations.Insert(i - 1, SelectedOperationViewModel.Model);
      if (Model.Operations[i] is ContextOperation)
      {
        Model.Activity.GetAssociativeObject<ContextualActivityContextOperation>(((Operation)Model.Operations[i])).SortOrder = i;
        Model.Activity.GetAssociativeObject<ContextualActivityContextOperation>(((Operation)Model.Operations[i - 1])).SortOrder = i - 1;
      }
      else
      {
        Model.Activity.GetAssociativeObject<ActivityGlobalOperation>(((Operation)Model.Operations[i])).SortOrder = i;
        Model.Activity.GetAssociativeObject<ActivityGlobalOperation>(((Operation)Model.Operations[i - 1])).SortOrder = i - 1;
      }
      MoveUpCommand.RaiseCanExecuteChanged();
      MoveDownCommand.RaiseCanExecuteChanged();
      OnPropertyChanged("Operations");
      IsFocused = true;
      SelectedOperationViewModel.IsFocused = true;
    }

    #endregion

    #region DelegateCommand MoveDownCommand

    DelegateCommand mMoveDownCommand;
    public DelegateCommand MoveDownCommand
    {
      get { return mMoveDownCommand ?? (mMoveDownCommand = new DelegateCommand(ExecuteMoveDown, CanExecuteMoveDown)); }
    }

    bool CanExecuteMoveDown()
    {
      if (SelectedOperationViewModel == null) return false;
      if (Model.Operations.IndexOf(SelectedOperationViewModel.Model) == (Model.Operations.Count - 1)) return false;
      return true;
    }

    void ExecuteMoveDown()
    {
      int i = Model.Operations.IndexOf(SelectedOperationViewModel.Model);
      Model.Operations.RemoveAt(i);
      Model.Operations.Insert(i + 1, SelectedOperationViewModel.Model);
      if (Model.Operations[i] is ContextOperation)
      {
        Model.Activity.GetAssociativeObject<ContextualActivityContextOperation>(((Operation)Model.Operations[i])).SortOrder = i;
        Model.Activity.GetAssociativeObject<ContextualActivityContextOperation>(((Operation)Model.Operations[i + 1])).SortOrder = i + 1;
      }
      else
      {
        Model.Activity.GetAssociativeObject<ActivityGlobalOperation>(((Operation)Model.Operations[i])).SortOrder = i;
        Model.Activity.GetAssociativeObject<ActivityGlobalOperation>(((Operation)Model.Operations[i + 1])).SortOrder = i + 1;
      }
      MoveUpCommand.RaiseCanExecuteChanged();
      MoveDownCommand.RaiseCanExecuteChanged();
      OnPropertyChanged("Operations");
      IsFocused = true;
      SelectedOperationViewModel.IsFocused = true;
    }

    #endregion

    #region DelegateCommand EditRolesCommand

    DelegateCommand mEditRolesCommand;
    public DelegateCommand EditRolesCommand
    {
      get { return mEditRolesCommand ?? (mEditRolesCommand = new DelegateCommand(ExecuteEditRoles, CanExecuteEditRoles)); }
    }

    bool CanExecuteEditRoles()
    {
      return SelectedOperationViewModel != null;
    }

    void ExecuteEditRoles()
    {
      RoleSelectionContext rsc;
      if (SelectedOperationViewModel.Model is ContextOperation)
      {
        rsc = new RoleSelectionContext(Model.Activity.GetAssociativeObject<ContextualActivityContextOperation>(SelectedOperationViewModel.Model).Roles);
      }
      else
      {
        rsc = new RoleSelectionContext(Model.Activity.GetAssociativeObject<ActivityGlobalOperation>(SelectedOperationViewModel.Model).Roles);
      }
      RoleSelectionController c = Controller.CreateChildController<RoleSelectionController>();
      c.DataContext = rsc;
      c.SelectedOperationViewModel = SelectedOperationViewModel;
      c.Run();
    }

    #endregion

    #region DelegateCommand DeleteOperationCommand

    DelegateCommand mDeleteOperationCommand;
    public DelegateCommand DeleteOperationCommand
    {
      get { return mDeleteOperationCommand ?? (mDeleteOperationCommand = new DelegateCommand(ExecuteDeleteOperation, CanExecuteDeleteOperation)); }
    }

    bool CanExecuteDeleteOperation()
    {
      return SelectedOperationViewModel != null;
    }

    void ExecuteDeleteOperation()
    {
      Model.Operations.Remove(SelectedOperationViewModel.Model);
    }

    #endregion

  }
}

﻿using Afx.Business.Activities;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Activities.ActivityManagement
{
  public class EditPermissionsViewModel : MdiDialogViewModel<EditPermissionsContext>
  {
    [InjectionConstructor]
    public EditPermissionsViewModel(IController controller)
      : base(controller)
    {
    }

    [Dependency]
    public EditPermissionsController EditPermissionsController { get; set; }

    public bool HasViewPermissions
    {
      get { return EditPermissionsController.HasView; }
      set
      {
        if (EditPermissionsController.HasView != value)
        {
          EditPermissionsController.HasView = value;
          OnPropertyChanged("HasViewPermissions");
          OnPropertyChanged("HasEditPermissions");
        }
      }
    }

    public bool HasEditPermissions
    {
      get { return EditPermissionsController.HasEdit; }
      set
      {
        if (EditPermissionsController.HasEdit != value)
        {
          EditPermissionsController.HasEdit = value;
          OnPropertyChanged("HasEditPermissions");
        }
      }
    }
  }
}

﻿using Afx.Business;
using Afx.Business.Activities;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Activities.ActivityManagement
{
  public class ActivityManagementController : MdiContextualActivityController<ActivityManagement, ActivityManagementViewModel>
  {
    [InjectionConstructor]
    public ActivityManagementController(IController controller)
      : base(controller)
    {
    }

    protected override void OnLoaded()
    {
      if (DataContext.Data.Count > 0)
      {
        //Focus the first element 
        ActivityTabNodeViewModel nvm = GetCreateViewModel<ActivityTabNodeViewModel>(DataContext.Data[0], ViewModel);
        nvm.IsFocused = true;
      }

      RegisterContextRegion(RegionManager.Regions[ActivityManagement.ContextRegion]);
      RegisterContextRegion(RegionManager.Regions[ActivityManagement.FillRegion]);

      base.OnLoaded();
    }

    protected override void OnContextViewModelChanged(ISelectableViewModel context)
    {
      if (context.Model is ActivityTab)
      {
        ActivityTabDetailViewModel vm = GetCreateViewModel<ActivityTabDetailViewModel>(context.Model, ViewModel);
        RegionManager.Regions[ActivityManagement.ContextRegion].Add(vm);
      }

      if (context.Model is ActivityGroup)
      {
        ActivityTabGroupDetailViewModel vm = GetCreateViewModel<ActivityTabGroupDetailViewModel>(context.Model, ViewModel);
        RegionManager.Regions[ActivityManagement.ContextRegion].Add(vm);
      }

      if (context.Model is Activity)
      {
        ActivityDetailViewModel vm = GetCreateViewModel<ActivityDetailViewModel>(context.Model, ViewModel);
        RegionManager.Regions[ActivityManagement.ContextRegion].Add(vm);

        ActivityRolesDetailViewModel vm1 = GetCreateViewModel<ActivityRolesDetailViewModel>(context.Model, ViewModel);
        RegionManager.Regions[ActivityManagement.FillRegion].Add(vm1);
      }

      if (context.Model is OperationFolder)
      {
        OperationFolderDetailViewModel vm = GetCreateViewModel<OperationFolderDetailViewModel>(context.Model, ViewModel);
        RegionManager.Regions[ActivityManagement.FillRegion].Add(vm);
      }

      base.OnContextViewModelChanged(context);
    }

    protected override void ExecuteOperation(Operation op, BusinessObject argument)
    {
      switch (op.Identifier)
      {
        case Operations.AddTab:
          {
            ActivityTab at = new ActivityTab();
            DataContext.Data.Add(at);
            SelectContextViewModel(at);
            FocusViewModel<ActivityTabDetailViewModel>(at);
          }
          break;

        case Operations.AddGroup:
          {
            ActivityTab at = (ActivityTab)argument;
            ActivityGroup ag = new ActivityGroup();
            at.Groups.Add(ag);
            SelectContextViewModel(ag);
            FocusViewModel<ActivityTabGroupDetailViewModel>(ag);
          }
          break;

        case Operations.AddContextualActivity:
          {
            ActivityGroup ag = (ActivityGroup)argument;
            Activity a = new ContextualActivity();
            ag.Activities.Add(a);
            SelectContextViewModel(a);
            FocusViewModel<ActivityDetailViewModel>(a);
          }
          break;

        case Operations.AddSimpleActivity:
          {
            ActivityGroup ag = (ActivityGroup)argument;
            Activity a = new SimpleActivity();
            ag.Activities.Add(a);
            SelectContextViewModel(a);
            FocusViewModel<ActivityDetailViewModel>(a);
          }
          break;

        case Operations.AddOperation:
          AddOperationsController aoc = CreateChildController<AddOperationsController>();
          aoc.DataContext = (OperationFolder)argument;
          aoc.SelectedNode = ContextViewModel; 
          aoc.Run();
          break;
      }

      base.ExecuteOperation(op, argument);
    }
  }
}

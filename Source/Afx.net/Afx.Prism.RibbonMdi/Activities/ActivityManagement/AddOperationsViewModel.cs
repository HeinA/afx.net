﻿using Afx.Business;
using Afx.Business.Activities;
using Afx.Business.Collections;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Afx.Prism.RibbonMdi.Activities.ActivityManagement
{
  public class AddOperationsViewModel : MdiDialogViewModel<OperationFolder>
  {
    [InjectionConstructor]
    public AddOperationsViewModel(IController controller)
      : base(controller)
    {
    }

    [Dependency]
    public AddOperationsController AddOperationsController { get; set; }

    public IEnumerable<AddOperationsOperationItemViewModel> AvailableOperationViewModels
    {
      get { return ResolveViewModels<AddOperationsOperationItemViewModel, Operation>(AvailableOperations); }
    }

    public BasicCollection<Operation> AvailableOperations
    {
      get { return AddOperationsController.AvailableOperations; }
    }

    public BasicCollection<Operation> SelectedOperations
    {
      get { return AddOperationsController.SelectedOperations; }
    }

    AddOperationsOperationItemViewModel mSelectedItemViewModel;
    public AddOperationsOperationItemViewModel SelectedItemViewModel
    {
      get { return mSelectedItemViewModel; }
      set { SetProperty<AddOperationsOperationItemViewModel>(ref mSelectedItemViewModel, value); }
    }

    #region DelegateCommand KeyDownCommand

    DelegateCommand<KeyEventArgs> mKeyDownCommand;
    public DelegateCommand<KeyEventArgs> KeyDownCommand
    {
      get { return mKeyDownCommand ?? (mKeyDownCommand = new DelegateCommand<KeyEventArgs>(ExecuteKeyDown)); }
    }

    void ExecuteKeyDown(KeyEventArgs e)
    {
      if (e.Key != Key.Space) return;
      if (SelectedItemViewModel != null) SelectedItemViewModel.HasOperation = !SelectedItemViewModel.HasOperation;
      e.Handled = true;
    }

    #endregion
  }
}

﻿using Afx.Business.Activities;
using Afx.Business;
using Afx.Business.Service;
using Afx.Prism.RibbonMdi.Activities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Activities.ExternalSystemManagement
{
  public partial class ExternalSystemManagement : SimpleActivityContext<ExternalSystem>
  {
    public ExternalSystemManagement()
    {
    }

    public ExternalSystemManagement(ContextualActivity activity)
      : base(activity)
    {
    }
  }
}

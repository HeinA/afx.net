﻿using Afx.Business.Security;
using Afx.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Afx.Prism;
using Afx.Prism.RibbonMdi.Activities;
using Syncfusion.Windows.Shared;
using Afx.Prism.RibbonMdi.Dialogs.EditFlags;

namespace Afx.Prism.RibbonMdi.Activities.ExternalSystemManagement
{
  public partial class ExternalSystemManagementViewModel : MdiSimpleActivityViewModel<ExternalSystemManagement>
  {
    #region Constructors

    [InjectionConstructor]
    public ExternalSystemManagementViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    #region DelegateCommand<ExternalSystem> EditFlagsCommand

    DelegateCommand<ExternalSystem> mEditFlagsCommand;
    public DelegateCommand<ExternalSystem> EditFlagsCommand
    {
      get { return mEditFlagsCommand ?? (mEditFlagsCommand = new DelegateCommand<ExternalSystem>(ExecuteEditFlags, CanExecuteEditFlags)); }
    }

    bool CanExecuteEditFlags(ExternalSystem args)
    {
      return true;
    }

    void ExecuteEditFlags(ExternalSystem args)
    {
      EditFlagsDialogController efc = Controller.CreateChildController<EditFlagsDialogController>();
      efc.DataContext = new EditFlagsDialogContext() { TargetObject = args };
      efc.Run();
    }

    #endregion  
  }
}

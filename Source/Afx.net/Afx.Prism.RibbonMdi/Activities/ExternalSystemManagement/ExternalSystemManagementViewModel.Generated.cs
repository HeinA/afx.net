﻿using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Afx.Prism.RibbonMdi.Activities;
using Afx.Business;

namespace Afx.Prism.RibbonMdi.Activities.ExternalSystemManagement
{
  //**************************************************************************************************************
  //**************************************************************************************************************
  //*                                                                                                            *
  //*     This code is auto-generated.  Do not place any cutom code here, it will be lost on re-generation.      *
  //*                                                                                                            *
  //**************************************************************************************************************
  //**************************************************************************************************************
  public partial class ExternalSystemManagementViewModel
  {
    public IEnumerable<ExternalSystem> Items
    {
      get
      {
        if (Model == null) return null;
        return Model.Data;
      }
    }
  }
}

﻿using Afx.Business;
using Afx.Prism.Events;
using Microsoft.Practices.ObjectBuilder2;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Afx.Prism.RibbonMdi.Activities
{
  public class ActivityDetailViewModel<TModel> : ViewModel<TModel>
    where TModel : class, IBusinessObject
  {
    protected ActivityDetailViewModel(IController controller)
      : base(controller)
    {
    }

    protected ActivityDetailViewModel(TModel context, IController controller)
      : this(controller)
    {
      Model = context;
    }

    IMdiActivityController mActivityController;
    [Dependency]
    public IMdiActivityController ActivityController
    {
      get { return mActivityController; }
      set
      {
        mActivityController = value;
        WeakEventManager<IMdiDockableDocumentController, EventArgs>.AddHandler(mActivityController, "CacheUpdated", OnCacheUpdatedInternal);
      }
    }

    private void OnCacheUpdatedInternal(object sender, EventArgs e)
    {
      if (ActivityController.State == ControllerState.Terminated) return;
      OnCacheUpdated();
    }

    protected virtual void OnCacheUpdated()
    {
      OnPropertyChanged(IsReadOnlyProperty);
    }

    public const string IsReadOnlyProperty = "IsReadOnly";
    public override bool IsReadOnly
    {
      get
      {
        if (ActivityController.DataContext.Activity.CanEdit) return false;
        return true;
      }
    }
  }
}

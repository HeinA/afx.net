﻿using Afx.Business.Activities;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Business.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Activities.OrganizationalUnitManagement
{
  public class OrganizationalUnitManagement : ContextualActivityContext<OrganizationalUnit>
  {
    public const string Key = "{b0cdd68e-e88b-4258-b70e-d321240cb334}";
    public const string ContextRegion = "ContextRegion";

    public OrganizationalUnitManagement()
    {
    }

    public OrganizationalUnitManagement(ContextualActivity activity)
      : base(activity)
    {
    }

    public override void LoadData()
    {
      using (var svc = ProxyFactory.GetService<IAfxService>(SecurityContext.MasterServer.Name))
      {
        Data = svc.LoadOrganizationalUnits();
      }
      base.LoadData();
    }

    public override void SaveData()
    {
      using (var svc = ProxyFactory.GetService<IAfxService>(SecurityContext.MasterServer.Name))
      {
        Data = svc.SaveOrganizationalUnits(Data);
      }
      base.SaveData();
    }
  }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Activities.OrganizationalUnitManagement
{
  public class Operations : StandardOperations
  {
    public const string AddRootUnit = "{ef7c5630-cd55-4f5a-b40f-e1d428bca7ba}";
    public const string AddSubUnit = "{5f13eddf-009d-4588-8502-937c1f5ec304}";
    public const string SetLogo = "{015363d0-0ea8-43eb-9bd9-56095704ae0e}";
  }
}

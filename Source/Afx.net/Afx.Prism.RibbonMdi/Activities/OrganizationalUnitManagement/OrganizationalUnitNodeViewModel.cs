﻿using Afx.Business.Security;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Afx.Prism.RibbonMdi.Activities.OrganizationalUnitManagement
{
  public class OrganizationalUnitNodeViewModel : MdiNavigationTreeNodeViewModel<OrganizationalUnit>
  {
    [InjectionConstructor]
    public OrganizationalUnitNodeViewModel(IController controller)
      : base(controller)
    {
    }

    public string Name
    {
      get
      {
        if (Model == null) return string.Empty;
        if (string.IsNullOrWhiteSpace(Model.Name)) return "*** Unnamed ***";
        return Model.Name;
      }
    }

    public IEnumerable<OrganizationalUnitNodeViewModel> SubUnits
    {
      get { return this.ResolveViewModels<OrganizationalUnitNodeViewModel, OrganizationalUnit>(Model.SubUnits); }
    }
  }
}

﻿using Afx.Business;
using Afx.Business.Activities;
using Afx.Business.Data;
using Afx.Business.Security;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Unity;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Activities.OrganizationalUnitManagement
{
  public class OrganizationalUnitManagementController : MdiContextualActivityController<OrganizationalUnitManagement, OrganizationalUnitManagementViewModel>
  {
    [InjectionConstructor]
    public OrganizationalUnitManagementController(IController controller)
      : base(controller)
    {
    }

    protected override void OnLoaded()
    {
      if (DataContext.Data.Count > 0)
      {
        //Focus the first element 
        OrganizationalUnitNodeViewModel nvm = GetCreateViewModel<OrganizationalUnitNodeViewModel>(DataContext.Data[0], ViewModel);
        nvm.IsFocused = true;
      }

      RegisterContextRegion(RegionManager.Regions[OrganizationalUnitManagement.ContextRegion]);

      base.OnLoaded();
    }

    protected override void OnContextViewModelChanged(ISelectableViewModel context)
    {
      if (context.Model is OrganizationalUnit)
      {
        OrganizationalUnitDetailViewModel vm = GetCreateViewModel<OrganizationalUnitDetailViewModel>(context.Model, ViewModel);
        RegionManager.Regions[OrganizationalUnitManagement.ContextRegion].Add(vm);
      }

      base.OnContextViewModelChanged(context);
    }

    public override Operation[] ReplaceOperation(Operation op)
    {
      if (op.Identifier == Operations.AddSubUnit && ContextViewModel != null)
      {
        Collection<Operation> ops = new Collection<Operation>();
        OrganizationalUnit ou = ContextViewModel.Model as OrganizationalUnit;
        foreach (var st in ou.StructureType.SubTypes.OrderBy((st) => st.Name))
        {
          Operation rop = op.CreateReplicate();
          rop.Tag = st;
          ops.Add(rop);
        }
        return ops.ToArray();
      }
      return base.ReplaceOperation(op);
    }

    public override string GetOperationText(Operation op)
    {
      OrganizationalStructureType ost = op.Tag as OrganizationalStructureType;
      if (ost != null)
      {
        return string.Format("Add {0}", ost.Name);
      }
      return base.GetOperationText(op);
    }

    protected override void ExecuteOperation(Operation op, BusinessObject argument)
    {
      switch (op.Identifier)
      {
        case Operations.AddRootUnit:
          {
            OrganizationalUnit ou = new OrganizationalUnit();
            ou.StructureType = Cache.Instance.GetObjects<OrganizationalStructureType>((ost) => ost.Owner == null).FirstOrDefault();
            DataContext.Data.Add(ou);
            SelectContextViewModel(ou);
            FocusViewModel<OrganizationalUnitDetailViewModel>(ou);
          }
          break;

        case Operations.AddSubUnit:
          {
            OrganizationalUnit ou = (OrganizationalUnit)argument;
            OrganizationalUnit newOu = new OrganizationalUnit();
            newOu.StructureType = (OrganizationalStructureType)op.Tag;
            ou.SubUnits.Add(newOu);
            SelectContextViewModel(newOu);
            FocusViewModel<OrganizationalUnitDetailViewModel>(newOu);
          }
          break;

        case Operations.SetLogo:
          {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Multiselect = false;
            ofd.Filter = "Image Files|*.png;*.jpg|All Files|*.*";
            if (ofd.ShowDialog().Value)
            {
              OrganizationalUnit ou = (OrganizationalUnit)argument;
              ou.SetImageFromFile(ofd.FileName);
            }
          }
          break;
      }
      base.ExecuteOperation(op, argument);
    }
  }
}

﻿using Afx.Business.Security;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Activities.OrganizationalUnitManagement
{
  public class OrganizationalUnitManagementViewModel : MdiContextualActivityViewModel<OrganizationalUnitManagement>
  {
    [InjectionConstructor]
    public OrganizationalUnitManagementViewModel(IController controller)
      : base(controller)
    {
    }

    public IEnumerable<OrganizationalUnitNodeViewModel> Units
    {
      get
      {
        if (Model == null) return null;
        return this.ResolveViewModels<OrganizationalUnitNodeViewModel, OrganizationalUnit>(Model.Data);
      }
    }
  }
}

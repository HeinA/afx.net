﻿using Afx.Business;
using Afx.Business.Data;
using Afx.Business.Security;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Afx.Prism.RibbonMdi.Activities.OrganizationalUnitManagement
{
  public class OrganizationalUnitDetailViewModel : ActivityDetailViewModel<OrganizationalUnit>
  {
    [InjectionConstructor]
    public OrganizationalUnitDetailViewModel(IController controller)
      : base(controller)
    {
    }

    #region Model Propertry string Name

    public string Name
    {
      get
      {
        if (Model == null) return default(string);
        return Model.Name;
      }
      set { Model.Name = value; }
    }

    #endregion

    #region Model Propertry bool UserAssignable

    public bool UserAssignable
    {
      get
      {
        if (Model == null) return default(bool);
        return Model.UserAssignable;
      }
      set { Model.UserAssignable = value; }
    }

    #endregion

    #region IEnumerable<ServerInstance> ServerInstances

    static object mLock = new object();
    static Collection<ServerInstance> mServerInstances;
    public IEnumerable<ServerInstance> ServerInstances
    {
      get
      {
        return mServerInstances ?? (mServerInstances = Cache.Instance.GetObjects<ServerInstance>(true, (si) => si.Name, true));
      }
    }

    #endregion

    #region ServerInstance Server

    public ServerInstance Server
    {
      get
      {
        if (Model == null) return typeof(ServerInstance).IsSubclassOf(typeof(BusinessObject)) ? new ServerInstance(true) : default(ServerInstance);
        return Model.Server;
      }
      set { Model.Server = value; }
    }

    #endregion

    #region ImageSource Logo

    ImageSource mLogo;
    public ImageSource Logo
    {
      get
      {
        if (Model == null) return default(ImageSource);
        if (string.IsNullOrWhiteSpace(Model.Base64Image)) return null;
        if (mLogo == null)
        {
          byte[] imgData = Convert.FromBase64String(Model.Base64Image);
          BitmapImage img = new BitmapImage();
          using (MemoryStream ms = new MemoryStream(imgData))
          {
            img.BeginInit();
            img.CacheOption = BitmapCacheOption.OnLoad;
            img.StreamSource = ms;
            img.EndInit();
          }
          mLogo = img as ImageSource;
        }
        return mLogo;
      }
    }

    #endregion

    #region Model Propertry string PhysicalAddress

    public string PhysicalAddress
    {
      get
      {
        if (Model == null) return default(string);
        return Model.PhysicalAddress;
      }
      set { Model.PhysicalAddress = value; }
    }

    #endregion

    #region Model Propertry string PostalAddress

    public string PostalAddress
    {
      get
      {
        if (Model == null) return default(string);
        return Model.PostalAddress;
      }
      set { Model.PostalAddress = value; }
    }

    #endregion

    protected override void OnModelPropertyChanged(string propertyName)
    {
      switch (propertyName)
      {
        case OrganizationalUnit.Base64ImageProperty:
          OnPropertyChanged("Logo");
          break;
      }

      base.OnModelPropertyChanged(propertyName);
    }

    public string StructureType
    {
      get { return Model.StructureType.Name; }
    }
  }
}

﻿using Afx.Business.Security;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Activities.OrganizationalStructureManagement
{
  public class OrganizationalStructureTypeNodeViewModel : MdiNavigationTreeNodeViewModel<OrganizationalStructureType>
  {
    [InjectionConstructor]
    public OrganizationalStructureTypeNodeViewModel(IController controller)
      : base(controller)
    {
    }

    public string Name
    {
      get
      {
        if (Model == null) return string.Empty;
        if (string.IsNullOrWhiteSpace(Model.Name)) return "*** Unnamed ***";
        return Model.Name;
      }
    }

    public IEnumerable<OrganizationalStructureTypeNodeViewModel> SubTypes
    {
      get { return this.ResolveViewModels<OrganizationalStructureTypeNodeViewModel, OrganizationalStructureType>(Model.SubTypes); }
    }
  }
}

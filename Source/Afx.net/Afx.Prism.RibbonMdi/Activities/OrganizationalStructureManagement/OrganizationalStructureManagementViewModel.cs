﻿using Afx.Business.Security;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Activities.OrganizationalStructureManagement
{
  public class OrganizationalStructureManagementViewModel : MdiContextualActivityViewModel<OrganizationalStructureManagement>
  {
    [InjectionConstructor]
    public OrganizationalStructureManagementViewModel(IController controller)
      : base(controller)
    {
    }

    public IEnumerable<OrganizationalStructureTypeNodeViewModel> StructureTypes
    {
      get
      {
        if (Model == null) return null;
        return this.ResolveViewModels<OrganizationalStructureTypeNodeViewModel, OrganizationalStructureType>(Model.Data);
      }
    }
  }
}

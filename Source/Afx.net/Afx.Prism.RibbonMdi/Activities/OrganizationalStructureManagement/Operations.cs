﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Activities.OrganizationalStructureManagement
{
  public class Operations : StandardOperations
  {
    public const string AddRootType = "{4a00be98-18b6-48e0-bfcd-ccac44861187}";
    public const string AddSubType = "{d395e5bf-e3b9-468a-a18e-7128ff201a5c}";
  }
}

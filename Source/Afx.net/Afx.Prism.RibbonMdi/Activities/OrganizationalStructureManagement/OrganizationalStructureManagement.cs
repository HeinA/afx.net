﻿using Afx.Business;
using Afx.Business.Activities;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Business.Service;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Afx.Prism.RibbonMdi.Activities.OrganizationalStructureManagement
{
  public class OrganizationalStructureManagement : ContextualActivityContext<OrganizationalStructureType>
  {
    public const string Key = "{b2ab1cc8-9472-4611-b1c2-0d9ba0be55f8}";
    public const string ContextRegion = "ContextRegion";

    public OrganizationalStructureManagement()
    {
    }

    public OrganizationalStructureManagement(ContextualActivity activity)
      : base(activity)
    {
    }

    public override void LoadData()
    {
      using (var svc = ProxyFactory.GetService<IAfxService>(SecurityContext.MasterServer.Name))
      {
        Data = svc.LoadOrganizationalStructure();
      } 
      base.LoadData();
    }

    public override void SaveData()
    {
      using (var svc = ProxyFactory.GetService<IAfxService>(SecurityContext.MasterServer.Name))
      {
        Data = svc.SaveOrganizationalStructure(Data);
      }
      base.SaveData();
    }
  }
}

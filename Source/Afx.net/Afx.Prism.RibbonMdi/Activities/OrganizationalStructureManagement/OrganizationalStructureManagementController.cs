﻿using Afx.Business;
using Afx.Business.Activities;
using Afx.Business.Security;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace Afx.Prism.RibbonMdi.Activities.OrganizationalStructureManagement
{
  public class OrganizationalStructureManagementController : MdiContextualActivityController<OrganizationalStructureManagement, OrganizationalStructureManagementViewModel>
  {
    [InjectionConstructor]
    public OrganizationalStructureManagementController(IController controller)
      : base(controller)
    {
    }

    protected override void OnLoaded()
    {
      if (DataContext.Data.Count > 0)
      {
        //Focus the first element 
        OrganizationalStructureTypeNodeViewModel nvm = GetCreateViewModel<OrganizationalStructureTypeNodeViewModel>(DataContext.Data[0], ViewModel);
        nvm.IsFocused = true;
      }

      RegisterContextRegion(RegionManager.Regions[OrganizationalStructureManagement.ContextRegion]);

      base.OnLoaded();
    }

    protected override void OnContextViewModelChanged(ISelectableViewModel context)
    {
      if (context.Model is OrganizationalStructureType)
      {
        OrganizationalStructureTypeDetailViewModel vm = GetCreateViewModel<OrganizationalStructureTypeDetailViewModel>(context.Model, ViewModel);
        RegionManager.Regions[OrganizationalStructureManagement.ContextRegion].Add(vm);
      }

      base.OnContextViewModelChanged(context);
    }

    public override bool IsOperationVisible(Operation op)
    {
      // Only allow one root element
      if (op.Identifier == Operations.AddRootType && DataContext.Data.Count > 0) return false;
      return base.IsOperationVisible(op);
    }

    protected override void ExecuteOperation(Operation op, BusinessObject argument)
    {
      switch (op.Identifier)
      {
        case Operations.AddRootType:
          {
            OrganizationalStructureType ost = new OrganizationalStructureType();
            DataContext.Data.Add(ost);
            SelectContextViewModel(ost);
            FocusViewModel<OrganizationalStructureTypeDetailViewModel>(ost);
          }
          break;

        case Operations.AddSubType:
          {
            OrganizationalStructureType ost = (OrganizationalStructureType)argument;
            OrganizationalStructureType newOst = new OrganizationalStructureType();
            ost.SubTypes.Add(newOst);
            SelectContextViewModel(newOst);
            FocusViewModel<OrganizationalStructureTypeDetailViewModel>(newOst);
          }
          break;
      }
      base.ExecuteOperation(op, argument);
    }
  }
}

﻿using Afx.Business;
using Afx.Business.Activities;
using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Business.ServiceModel;
using Afx.Prism.Activities;
using Afx.Prism.Events;
using Afx.Prism.RibbonMdi.Events;
using Afx.Prism.RibbonMdi.Login;
using Microsoft.Practices.ObjectBuilder2;
using Microsoft.Practices.Unity;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Activities
{
  public abstract class MdiActivityController<TModel, TViewModel> : MdiDockableDocumentController<TModel, TViewModel>, IMdiActivityController
    where TModel : ContextBase, IActivityContext
    where TViewModel : MdiActivityViewModel<TModel>
  {
    protected MdiActivityController(IController controller)
      : base(controller)
    {
    }

    protected MdiActivityController(string key, IController controller)
      : base(key, controller)
    {
    }

    protected override void ConfigureContainer()
    {
      Container.RegisterInstance<IMdiActivityController>(this);
      base.ConfigureContainer();
    }

    protected override void OnCacheUpdated()
    {
      DataContext.Activity = Cache.Instance.GetObject<Activity>(DataContext.Activity.GlobalIdentifier);
      base.OnCacheUpdated();
    }

    IActivityContext IMdiActivityController.DataContext
    {
      get { return (IActivityContext)DataContext; }
      set { DataContext = value as TModel; }
    }


    #region Printing

    private void PrintReport(bool preview, object argument)
    {
      if (ReportInfo == null)
      {
        string contractName = this.DataContext.Activity.GlobalIdentifier.ToString("b");
        ReportInfo = ExtensibilityManager.CompositionContainer.GetExportedValues<IActivityPrintInfo>(contractName).OrderBy(ri1 => ri1.Priority).FirstOrDefault();
        if (ReportInfo == null)
        {
          System.Media.SystemSounds.Hand.Play();
          return;
        }
      }

      #region Background Worker

      BackgroundWorker bw = new BackgroundWorker();
      bw.DoWork += (s, e) =>
      {
        try
        {
          // Variables
          Warning[] warnings;
          string[] streamIds;
          string mimeType = string.Empty;
          string encoding = string.Empty;
          string extension = string.Empty;


          // Setup the report viewer object and get the array of bytes
          ReportViewer viewer = new ReportViewer();
          viewer.LocalReport.LoadReportDefinition(ReportInfo.ReportStream(argument));

          SetupReport(viewer);
          object scope = ReportInfo.GetScope(this);
          RefreshReportData(viewer, scope, argument);

          if (preview)
          {
            byte[] bytes = viewer.LocalReport.Render("PDF", null, out mimeType, out encoding, out extension, out streamIds, out warnings);
            string file = string.Format("{0}.pdf", Path.GetTempFileName());
            File.WriteAllBytes(file, bytes);
            Process.Start(file);
          }
          else
          {
            ReportPrintDocument pd = new ReportPrintDocument(viewer.LocalReport);
            pd.PrinterSettings.Copies = ReportInfo.Copies;
            pd.Print();
          }
        }
        catch
        {
          throw;
        }
      };
      bw.RunWorkerCompleted += (s, e) =>
      {
        bw.Dispose();
      };
      bw.RunWorkerAsync();
      
      #endregion

    }

    #region IReportInfo ReportInfo

    IActivityPrintInfo mReportInfo;
    public IActivityPrintInfo ReportInfo
    {
      get { return mReportInfo; }
      set { mReportInfo = value; }
    }

    #endregion

    protected void SetupReport(ReportViewer viewer)
    {
      PageSettings pageSettings = viewer.GetPageSettings();
      PaperSize psA4 = null;
      bool b = viewer.LocalReport.GetDefaultPageSettings().IsLandscape;
      foreach (PaperSize ps in pageSettings.PrinterSettings.PaperSizes)
      {
        if (ps.Kind == PaperKind.A4)
        {
          psA4 = ps;
          break;
        }
      }
      if (psA4 != null)
      {
        pageSettings.PaperSize = psA4;
        viewer.SetPageSettings(pageSettings);
      }

      viewer.LocalReport.DataSources.Clear();
      viewer.ProcessingMode = ProcessingMode.Local;
    }

    protected void RefreshReportData(ReportViewer rv, object scope, object argument)
    {
      ReportInfo.RefreshReportData(rv.LocalReport, this.DataContext, scope, argument);
      rv.LocalReport.Refresh();
      rv.RefreshReport();
    }

    public void Print(bool preview)
    {
      Print(preview, true);
    }

    protected void Print(bool preview = false, bool doNotSave = false, object argument = null)
    {
      if (!doNotSave)
      {
        if (!Save()) return;
      }

      if (!OnBeforePrint()) return;
      PrintReport(preview, argument);
      OnPrinted();
    }

    protected virtual bool OnBeforePrint()
    {
      return true;
    }

    protected virtual void OnPrinted()
    {
    }

    #endregion




    protected override void ExecuteOperation(Operation op, BusinessObject argument)
    {
      switch (op.Identifier)
      {
        case StandardOperations.Print:
          Print();
          break;

        case StandardOperations.PrintPreview:
          Print(true);
          break;
      }

      ApplicationController.EventAggregator.GetEvent<ExecuteOperationEvent>().Publish(new ExecuteOperationEventArgs(this, op, argument));
      base.ExecuteOperation(op, argument);
    }
  }
}

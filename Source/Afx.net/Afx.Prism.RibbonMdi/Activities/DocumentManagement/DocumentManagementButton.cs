﻿using Afx.Business.Activities;
using Afx.Business.Data;
using Afx.Prism.RibbonMdi.Ribbon;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Activities.DocumentManagement
{
  [Export(typeof(IRibbonItem))]
  public class DocumentManagementButton : RibbonButtonViewModel
  {
    public override string TabName
    {
      get { return AdministrationTab.TabName; }
    }

    public override string GroupName
    {
      get { return ApplicationGroup.GroupName; }
    }

    public override string ItemIdentifier
    {
      get { return "DocumentManagement"; }
    }

    public override int Index
    {
      get { return 2; }
    }

    #region Activity Activity

    public Activity Activity
    {
      get { return Cache.Instance.GetObject<Activity>(DocumentManagement.Key); }
    }

    #endregion

    public bool IsEnabled
    {
      get { return Activity.CanView; }
    }

    protected override void Execute()
    {
      MdiApplicationController.Current.ExecuteActivity(Activity);
    }
  }
}

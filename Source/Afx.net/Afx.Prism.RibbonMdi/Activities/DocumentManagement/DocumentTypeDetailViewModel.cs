﻿using Afx.Business;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Documents;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Activities.DocumentManagement
{
  public class DocumentTypeDetailViewModel : ActivityDetailViewModel<DocumentType>
  {
    [InjectionConstructor]
    public DocumentTypeDetailViewModel(IController controller)
      : base(controller)
    {
    }

    public new DocumentManagementViewModel Parent
    {
      get { return (DocumentManagementViewModel)base.Parent; }
    }

    #region string Name

    public string Name
    {
      get
      {
        if (Model == null) return default(string);
        return Model.Name;
      }
      set { Model.Name = value; }
    }

    #endregion

    #region string Identifier

    public string Identifier
    {
      get
      {
        if (Model == null) return default(string);
        return Model.Identifier;
      }
    }

    #endregion

    #region DocumentTypeState InitialState

    public DocumentTypeState InitialState
    {
      get
      {
        if (Model == null) return typeof(DocumentTypeState).IsSubclassOf(typeof(BusinessObject)) ? new DocumentTypeState(true) : default(DocumentTypeState);
        return Model.InitialState;
      }
      set { Model.InitialState = value; }
    }

    #endregion

    #region DocumentType BaseDocument

    public DocumentType BaseDocument
    {
      get
      {
        if (Model == null) return default(DocumentType);
        return Model.BaseDocument;
      }
      set { Model.BaseDocument = value; }
    }

    #endregion

    #region string Namespace

    public IEnumerable<string> Namespaces
    {
      get { return ExtensibilityManager.Namespaces; }
    }

    public string Namespace
    {
      get
      {
        if (Model == null) return string.Empty;
        return Model.Namespace ?? string.Empty;
      }
      set { Model.Namespace = string.IsNullOrWhiteSpace(value) ? null : value; }
    }

    #endregion

    #region bool Enabled

    public bool Enabled
    {
      get { return Model == null ? GetDefaultValue<bool>() : Model.Enabled; }
      set { Model.Enabled = value; }
    }

    #endregion

    public IEnumerable<DocumentType> BaseDocumentTypes
    {
      get
      {
        BasicCollection<DocumentType> dts = new BasicCollection<DocumentType>();
        dts.Add(new DocumentType(true));
        foreach (var dt in Parent.DocumentTypes.Where(dt1 => !IsOfType(this.Model, dt1.Model)).OrderBy(dt1 => dt1.Name)) dts.Add(dt.Model);
        return dts;
      }
    }

    internal bool IsOfType(DocumentType source, DocumentType target)
    {
      if (source.Equals(target)) return true;
      if (BusinessObject.IsNull(target.BaseDocument)) return false;
      return IsOfType(source, target.BaseDocument); 
    }

    #region IEnumerable<DocumentTypeState> States

    public IEnumerable<DocumentTypeState> States
    {
      get
      {
        BasicCollection<DocumentTypeState> states = new BasicCollection<DocumentTypeState>();
        states.Add(new DocumentTypeState(true));
        foreach (var ds in Model.HeirarchyStates.OrderBy(ds1 => ds1.Name)) states.Add(ds);
        return states;
      }
    }

    #endregion

    #region IEnumerable<DocumentTypeReference> References

    public IEnumerable<DocumentTypeReference> References
    {
      get { return Model == null ? null : Model.References; }
    }

    #endregion

    #region string ClassDefinitions

    public string ClassDefinitions
    {
      get
      {
        try
        {
          StringBuilder sb = new StringBuilder();
          sb.AppendFormat(CultureInfo.InvariantCulture, "public{1} const string DocumentTypeIdentifier = \"{0}\";\n", Model.GlobalIdentifier.ToString("B"), BusinessObject.IsNull(BaseDocument) ? string.Empty : " new");
          sb.AppendFormat(CultureInfo.InvariantCulture, "public{0} class States{1}\n{2}\n", BusinessObject.IsNull(BaseDocument) ? string.Empty : " new", BusinessObject.IsNull(BaseDocument) ? string.Empty : string.Format(CultureInfo.InvariantCulture, " : {0}.States", BaseDocument.Name.Replace(" ", string.Empty)), "{");
          foreach (var dts in Model.States)
          {
            sb.AppendFormat(CultureInfo.InvariantCulture, "\tpublic const string {0} = \"{1}\";\n", dts.Name == null ? null : dts.Name.Replace(" ", string.Empty), dts.GlobalIdentifier.ToString("B"));
          }
          sb.Append("}");
          return sb.ToString();
        }
        catch
        {
          throw;
        }
      }
    }

    #endregion

    #region void OnModelCompositionChanged(CompositionChangedEventArgs e)

    protected override void OnModelCompositionChanged(CompositionChangedEventArgs e)
    {
      if (e.CompositionChangedType == CompositionChangedType.ChildAdded || e.CompositionChangedType == CompositionChangedType.ChildRemoved)
      {
        if (e.Item is DocumentTypeState)
        {
          OnPropertyChanged(DocumentType.StatesProperty);
          OnPropertyChanged("ClassDefinitions");
        }
      }

      if (e.CompositionChangedType == CompositionChangedType.PropertyChanged && e.Item == this.Model && e.PropertyName == DocumentType.BaseDocumentProperty)
      {
        OnPropertyChanged(DocumentType.StatesProperty);
        OnPropertyChanged("ClassDefinitions");
      }

      base.OnModelCompositionChanged(e);
    }

    #endregion
  }
}

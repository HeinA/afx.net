﻿using Afx.Business.Documents;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Activities.DocumentManagement
{
  public class DocumentTypeStateNodeViewModel : MdiNavigationTreeNodeViewModel<DocumentTypeState>
  {
    [InjectionConstructor]
    public DocumentTypeStateNodeViewModel(IController controller)
      : base(controller)
    {
    }

    public string Name
    {
      get
      {
        if (Model == null) return string.Empty;
        if (string.IsNullOrWhiteSpace(Model.Name)) return "*** Unnamed ***";
        return Model.Name;
      }
    }
  }
}

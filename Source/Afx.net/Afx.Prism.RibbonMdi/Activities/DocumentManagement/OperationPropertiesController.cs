﻿using Afx.Business.Data;
using Afx.Business.Security;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace Afx.Prism.RibbonMdi.Activities.DocumentManagement
{
  public class OperationPropertiesController : MdiDialogController<OperationPropertiesContext, OperationPropertiesViewModel>
  {
    public const string RoleSelectionDialogControllerKey = "Role Selection Controller";

    [InjectionConstructor]
    public OperationPropertiesController(IController controller)
      : base(RoleSelectionDialogControllerKey, controller)
    {
    }

    OperationItemViewModel mSelectedOperationViewModel;
    public OperationItemViewModel SelectedOperationViewModel
    {
      get { return mSelectedOperationViewModel; }
      set { mSelectedOperationViewModel = value; }
    }

    protected override void OnTerminated()
    {
      SelectedOperationViewModel.IsFocused = true;
      base.OnTerminated();
    }

    protected override void ApplyChanges()
    {
      foreach (Role r in Cache.Instance.GetObjects<Role>())
      {
        if (DataContext.Roles.Contains(r) && !DataContext.OperationRoles.Contains(r)) DataContext.OperationRoles.Add(r);
        if (!DataContext.Roles.Contains(r) && DataContext.OperationRoles.Contains(r)) DataContext.OperationRoles.Remove(r);
      }
      if (DataContext.DocumentTypeGlobalOperation != null) DataContext.DocumentTypeGlobalOperation.ReadOnlyAvailability = DataContext.ReadOnlyAvailability;
      SelectedOperationViewModel.UpdateAllowedRoles();
      base.ApplyChanges();
    }
  }
}

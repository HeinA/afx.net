﻿using Afx.Business;
using Afx.Business.Activities;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Activities.DocumentManagement
{
  public class AddOperationsOperationItemViewModel : SelectableViewModel<GlobalOperation>
  {
    [InjectionConstructor]
    public AddOperationsOperationItemViewModel(IController controller)
      : base(controller)
    {
    }


    new AddOperationsViewModel Parent
    {
      get { return (AddOperationsViewModel)base.Parent; }
    }

    #region string Text

    public string Text
    {
      get
      {
        if (Model == null) return default(string);
        return Model.Text;
      }
    }

    #endregion

    public string GroupName
    {
      get
      {
        if (Model == null) return default(string);
        return Model.Owner.GroupName;
      }
    }

    #region Model Propertry string Description

    public string Description
    {
      get
      {
        if (Model == null) return default(string);
        return Model.Description;
      }
    }

    #endregion

    public override bool IsSelected
    {
      get
      {
        if (base.IsSelected) Parent.SelectedItemViewModel = this;
        return base.IsSelected;
      }
      set
      {
        base.IsSelected = value;
        if (base.IsSelected) Parent.SelectedItemViewModel = this;
        else if (Parent.SelectedItemViewModel == this) Parent.SelectedItemViewModel = null;
      }
    }

    public bool HasOperation
    {
      get
      {
        if (Parent.SelectedOperations.Contains(Model)) return true;
        return false;
      }
      set
      {
        if (value)
        {
          if (!Parent.SelectedOperations.Contains(Model))
          {
            Parent.SelectedOperations.Add(Model);
            OnPropertyChanged("HasOperation");
          }
        }
        else
        {
          if (Parent.SelectedOperations.Contains(Model))
          {
            Parent.SelectedOperations.Remove(Model);
            OnPropertyChanged("HasOperation");
          }
        }
      }
    }
  }
}

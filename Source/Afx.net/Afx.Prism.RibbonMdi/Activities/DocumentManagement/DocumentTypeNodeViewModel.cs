﻿using Afx.Business.Documents;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Activities.DocumentManagement
{
  public class DocumentTypeNodeViewModel : MdiNavigationTreeNodeViewModel<DocumentType>
  {
    [InjectionConstructor]
    public DocumentTypeNodeViewModel(IController controller)
      : base(controller)
    {
    }

    public string Name
    {
      get
      {
        if (Model == null) return string.Empty;
        if (string.IsNullOrWhiteSpace(Model.Name)) return "*** Unnamed ***";
        return Model.Name;
      }
    }

    public IEnumerable<DocumentTypeStateNodeViewModel> States
    {
      get { return this.ResolveViewModels<DocumentTypeStateNodeViewModel, DocumentTypeState>(Model.States); }
    }
  }
}

﻿using Afx.Business;
using Afx.Business.Collections;
using Afx.Business.Documents;
using Afx.Business.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Afx.Prism.RibbonMdi.Activities.DocumentManagement
{
  public class OperationPropertiesContext : BusinessObject
  {
    public OperationPropertiesContext(BasicCollection<Role> roles, DocumentTypeGlobalOperation dtop = null)
    {
      OperationRoles = roles;
      Roles = new BasicCollection<Role>(roles);
      if (dtop != null)
      {
        ReadOnlyAvailability = dtop.ReadOnlyAvailability;
        DocumentTypeGlobalOperation = dtop;
      }
    }

    BasicCollection<Role> mOperationRoles;
    public BasicCollection<Role> OperationRoles
    {
      get { return mOperationRoles; }
      private set { mOperationRoles = value; }
    }

    #region DocumentTypeGlobalOperation DocumentTypeGlobalOperation

    DocumentTypeGlobalOperation mDocumentTypeGlobalOperation;
    public DocumentTypeGlobalOperation DocumentTypeGlobalOperation
    {
      get { return mDocumentTypeGlobalOperation; }
      private set { mDocumentTypeGlobalOperation = value; }
    }

    #endregion

    BasicCollection<Role> mRoles;
    public BasicCollection<Role> Roles
    {
      get { return mRoles; }
      private set { mRoles = value; }
    }

    #region bool ReadOnlyAvailability

    bool mReadOnlyAvailability;
    public bool ReadOnlyAvailability
    {
      get { return mReadOnlyAvailability; }
      set { SetProperty<bool>(ref mReadOnlyAvailability, value); }
    }

    #endregion
  }
}

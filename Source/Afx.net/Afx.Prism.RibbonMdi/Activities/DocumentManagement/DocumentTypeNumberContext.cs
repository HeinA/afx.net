﻿using Afx.Business;
using Afx.Business.Documents;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Activities.DocumentManagement
{
  public class DocumentTypeNumberContext : BusinessObject
  {
    public DocumentTypeNumberContext(DocumentTypeNumber documentTypeNumber)
    {
      mDocumentTypeNumber = documentTypeNumber;
      Acronymn = DocumentTypeNumber.Acronymn;
      NextId = DocumentTypeNumber.NextId;
    }

    DocumentTypeNumber mDocumentTypeNumber;
    public DocumentTypeNumber DocumentTypeNumber
    {
      get { return mDocumentTypeNumber; }
    }

    string mAcronymn;
    public string Acronymn
    {
      get { return mAcronymn; }
      set { SetProperty<string>(ref mAcronymn, value); }
    }

    int mNextId;
    public int NextId
    {
      get { return mNextId; }
      set { SetProperty<int>(ref mNextId, value); }
    }

    protected override void GetErrors(Collection<string> errors, string propertyName)
    {
      if (propertyName == null || propertyName == DocumentTypeNumber.AcronymnProperty)
      {
        if (string.IsNullOrWhiteSpace(Acronymn)) errors.Add("Acronymn is a mandatory field.");
      }

      if (propertyName == null || propertyName == DocumentTypeNumber.NextIdProperty)
      {
        if (NextId <= 0) errors.Add("NextId must be greater than zero.");
      }

      base.GetErrors(errors, propertyName);
    }
  }
}

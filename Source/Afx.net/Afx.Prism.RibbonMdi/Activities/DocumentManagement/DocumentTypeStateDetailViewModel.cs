﻿using Afx.Business.Documents;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Activities.DocumentManagement
{
  public class DocumentTypeStateDetailViewModel : ActivityDetailViewModel<DocumentTypeState>
  {
    [InjectionConstructor]
    public DocumentTypeStateDetailViewModel(IController controller)
      : base(controller)
    {
    }

    #region string Name

    public string Name
    {
      get
      {
        if (Model == null) return default(string);
        return Model.Name;
      }
      set { Model.Name = value; }
    }

    #endregion

    #region bool IsDocumentReadOnly

    public bool IsDocumentReadOnly
    {
      get { return Model == null ? GetDefaultValue<bool>() : Model.IsReadOnly; }
      set { Model.IsReadOnly = value; }
    }

    #endregion

    #region bool IsInRecentList

    public bool IsInRecentList
    {
      get { return Model == null ? GetDefaultValue<bool>() : Model.IsInRecentList; }
      set { Model.IsInRecentList = value; }
    }

    #endregion

    #region string Identifier

    public string Identifier
    {
      get
      {
        if (Model == null) return default(string);
        return Model.Identifier;
      }
    }

    #endregion
  }
}

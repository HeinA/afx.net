﻿using Afx.Business.Security;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Activities.DocumentManagement
{
  public class OperationPropertiesRoleViewModel : SelectableViewModel<Role>
  {
    [InjectionConstructor]
    public OperationPropertiesRoleViewModel(IController controller)
      : base(controller)
    {
    }

    new OperationPropertiesViewModel Parent
    {
      get { return (OperationPropertiesViewModel)base.Parent; }
    }

    #region string Name

    public string Name
    {
      get
      {
        if (Model == null) return default(string);
        return Model.Name;
      }
      set { Model.Name = value; }
    }

    #endregion

    public override bool IsSelected
    {
      get
      {
        if (base.IsSelected) Parent.SelectedItemViewModel = this;
        return base.IsSelected;
      }
      set
      {
        base.IsSelected = value;
        if (base.IsSelected) Parent.SelectedItemViewModel = this;
      }
    }

    public bool HasRole
    {
      get
      {
        if (Parent.Model.Roles.Contains(Model)) return true;
        return false;
      }
      set
      {
        if (value)
        {
          if (!Parent.Model.Roles.Contains(Model))
          {
            Parent.Model.Roles.Add(Model);
            OnPropertyChanged("HasRole");
          }
        }
        else
        {
          if (Parent.Model.Roles.Contains(Model))
          {
            Parent.Model.Roles.Remove(Model);
            OnPropertyChanged("HasRole");
          }
        }
      }
    }
  }
}

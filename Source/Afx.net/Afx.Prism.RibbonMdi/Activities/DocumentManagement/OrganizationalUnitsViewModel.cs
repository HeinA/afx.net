﻿using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Documents;
using Afx.Business.Security;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Afx.Prism.RibbonMdi.Activities.DocumentManagement
{
  public class OrganizationalUnitsViewModel : TabViewModel<DocumentType>
  {
    #region Constructors

    [InjectionConstructor]
    public OrganizationalUnitsViewModel(IController controller)
      : base(controller)
    {
      Title = "_Organizational Units";
    }

    #endregion

    #region DelegateCommand KeyDownCommand

    DelegateCommand<KeyEventArgs> mKeyDownCommand;
    public DelegateCommand<KeyEventArgs> KeyDownCommand
    {
      get { return mKeyDownCommand ?? (mKeyDownCommand = new DelegateCommand<KeyEventArgs>(ExecuteKeyDown)); }
    }

    void ExecuteKeyDown(KeyEventArgs e)
    {
      if (e.Key != Key.Space) return;
      if (SelectedOrganizationalUnitItemViewModel != null) SelectedOrganizationalUnitItemViewModel.HasOrganizationalUnit = !SelectedOrganizationalUnitItemViewModel.HasOrganizationalUnit;
      e.Handled = true;
    }

    #endregion

    #region ListView

    #region BasicCollection<OrganizationalUnits> OrganizationalUnits

    BasicCollection<OrganizationalUnit> mOrganizationalUnits;
    public BasicCollection<OrganizationalUnit> OrganizationalUnits
    {
      get { return mOrganizationalUnits ?? (mOrganizationalUnits = Cache.Instance.GetObjects<OrganizationalUnit>(ou => ou.UserAssignable, ou => ou.Name, true)); }
    }

    #endregion

    #region IEnumerable<OrganizationalUnitItemViewModel> OrganizationalUnitViewModels

    public IEnumerable<OrganizationalUnitItemViewModel> OrganizationalUnitViewModels
    {
      get
      {
        if (Model == null) return null;
        return this.ResolveViewModels<OrganizationalUnitItemViewModel, OrganizationalUnit>(OrganizationalUnits);
      }
    }

    #endregion

    #region OrganizationalUnitItemViewModel SelectedOrganizationalUnitItemViewModel

    OrganizationalUnitItemViewModel mSelectedROrganizationalUnitItemViewModel;
    public OrganizationalUnitItemViewModel SelectedOrganizationalUnitItemViewModel
    {
      get { return mSelectedROrganizationalUnitItemViewModel; }
      set { mSelectedROrganizationalUnitItemViewModel = value; }
    }

    #endregion

    #endregion

    #region DelegateCommand EditDocumentNumberDetailsCommand

    DelegateCommand mEditDocumentNumberDetailsCommand;
    public DelegateCommand EditDocumentNumberDetailsCommand
    {
      get { return mEditDocumentNumberDetailsCommand ?? (mEditDocumentNumberDetailsCommand = new DelegateCommand(ExecuteEditDocumentNumberDetails, CanExecuteEditDocumentNumberDetails)); }
    }

    bool CanExecuteEditDocumentNumberDetails()
    {
      return SelectedOrganizationalUnitItemViewModel != null && SelectedOrganizationalUnitItemViewModel.HasOrganizationalUnit;
    }

    public void ExecuteEditDocumentNumberDetails()
    {
      DocumentTypeNumberDetailsController c = Controller.CreateChildController<DocumentTypeNumberDetailsController>();
      c.DataContext = new DocumentTypeNumberContext(Model.GetAssociativeObject<DocumentTypeNumber>(SelectedOrganizationalUnitItemViewModel.Model));
      c.SelectedItemViewModel = SelectedOrganizationalUnitItemViewModel;
      c.Run();
    }

    #endregion
  }
}

﻿using Afx.Business;
using Afx.Business.Documents;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Afx.Prism.RibbonMdi.Activities.DocumentManagement
{
  public class OperationItemViewModel : SelectableViewModel<GlobalOperation>
  {
    [InjectionConstructor]
    public OperationItemViewModel(IController controller)
      : base(controller)
    {
    }

    //public DocumentType DocumentType
    //{
    //  get { return Parent.Model; }
    //}

    new IOperationCollection Parent
    {
      get { return (IOperationCollection)base.Parent; }
    }

    public override bool IsSelected
    {
      get
      {
        if (base.IsSelected) Parent.SelectedOperationViewModel = this;
        return base.IsSelected;
      }
      set
      {
        base.IsSelected = value;
        if (value) Parent.SelectedOperationViewModel = this;
        else if (Parent.SelectedOperationViewModel == this) Parent.SelectedOperationViewModel = null;
      }
    }

    public void UpdateAllowedRoles()
    {
      OnPropertyChanged("AllowedRoles");
    }

    #region string Text

    public string Text
    {
      get
      {
        if (Model == null) return default(string);
        return Model.Text;
      }
      set { Model.Text = value; }
    }

    #endregion

    #region ImageSource ImageSource

    ImageSource mImageSource;
    public ImageSource ImageSource
    {
      get
      {
        if (Model == null) return default(ImageSource);
        if (string.IsNullOrWhiteSpace(Model.Base64Image)) return null;
        if (mImageSource == null)
        {
          byte[] imgData = Convert.FromBase64String(Model.Base64Image);
          BitmapImage img = new BitmapImage();
          using (MemoryStream ms = new MemoryStream(imgData))
          {
            img.BeginInit();
            img.CacheOption = BitmapCacheOption.OnLoad;
            img.StreamSource = ms;
            img.EndInit();
          }
          mImageSource = img as ImageSource;
        }
        return mImageSource;
      }
    }

    public void SetImageFromFile(string filename)
    {
      Model.SetImageFromFile(filename);
      mImageSource = null;
      OnPropertyChanged("ImageSource");
      OnPropertyChanged("ImageVisibility");
    }

    #endregion

    #region Model Propertry string Description

    public string Description
    {
      get
      {
        if (Model == null) return default(string);
        return Model.Description;
      }
      set { Model.Description = value; }
    }

    #endregion

    public string AllowedRoles
    {
      get
      {
        Collection<string> roles = new Collection<string>();
        if (Parent.Model is DocumentType)
        {
          foreach (var role in ((DocumentType)Parent.Model).GetAssociativeObject<DocumentTypeGlobalOperation>(Model).Roles.OrderBy((r) => r.Name))
          {
            roles.Add(role.Name);
          }
        }
        if (Parent.Model is DocumentTypeState)
        {
          foreach (var role in ((DocumentTypeState)Parent.Model).GetAssociativeObject<DocumentTypeStateOperation>(Model).Roles.OrderBy((r) => r.Name))
          {
            roles.Add(role.Name);
          }
        }
        return string.Join(", ", roles);
      }
    }
  }
}

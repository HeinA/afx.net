﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Activities.OperationManagement
{
  public class Operations : StandardOperations
  {
    public const string AddGroup = "{308287e6-1d14-4b88-9777-d08e2009a5e7}";
    public const string AddOperation = "{ef332a83-d165-4180-92a7-8a51389987d8}";
    public const string AddContextOperation = "{b202fe2c-266b-4eaf-ad8a-52fbf738b9e0}";
    public const string SetIcon = "{ab765020-fc42-4697-92ab-ca0bb8e71ae7}";
  }
}

﻿using Afx.Business;
using Afx.Business.Activities;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Afx.Prism.RibbonMdi.Activities.OperationManagement
{
  public class OperationNodeViewModel : MdiNavigationTreeNodeViewModel<Operation>
  {
    [InjectionConstructor]
    public OperationNodeViewModel(IController controller)
      : base(controller)
    {
    }

    #region string Text

    public string Text
    {
      get
      {
        if (Model == null) return default(string);
        if (string.IsNullOrWhiteSpace(Model.Text)) return "*** Unnamed ***";
        return Model.Text;
      }
      set { Model.Text = value; }
    }

    #endregion

    #region Visibility ImageVisibility

    public Visibility ImageVisibility
    {
      get
      {
        if (Model == null) return Visibility.Collapsed;
        if (string.IsNullOrWhiteSpace(Model.Base64Image)) return Visibility.Collapsed;
        return Visibility.Visible;
      }
    }

    #endregion

    #region ImageSource ImageSource

    ImageSource mImageSource;
    public ImageSource ImageSource
    {
      get
      {
        if (Model == null) return default(ImageSource);
        if (string.IsNullOrWhiteSpace(Model.Base64Image)) return null;
        if (mImageSource == null)
        {
          byte[] imgData = Convert.FromBase64String(Model.Base64Image);
          BitmapImage img = new BitmapImage();
          using (MemoryStream ms = new MemoryStream(imgData))
          {
            img.BeginInit();
            img.CacheOption = BitmapCacheOption.OnLoad;
            img.StreamSource = ms;
            img.EndInit();
          }
          mImageSource = img as ImageSource;
        }
        return mImageSource;
      }
    }

    public void SetImageFromFile(string filename)
    {
      Model.SetImageFromFile(filename);
      mImageSource = null;
      OnPropertyChanged("ImageSource");
      OnPropertyChanged("ImageVisibility");
    }

    #endregion
  }
}

﻿using Afx.Business;
using Afx.Business.Activities;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Activities.OperationManagement
{
  public class OperationGroupDetailViewModel : ActivityDetailViewModel<OperationGroup>
  {
    [InjectionConstructor]
    public OperationGroupDetailViewModel(IController controller)
      : base(controller)
    {
    }

    #region Model Propertry string GroupName

    public string GroupName
    {
      get
      {
        if (Model == null) return default(string);
        return Model.GroupName;
      }
      set { Model.GroupName = value; }
    }

    #endregion

    #region string Namespace

    public IEnumerable<string> Namespaces
    {
      get { return ExtensibilityManager.Namespaces; }
    }

    public string Namespace
    {
      get
      {
        if (Model == null) return string.Empty;
        return Model.Namespace ?? string.Empty;
      }
      set { Model.Namespace = string.IsNullOrWhiteSpace(value) ? null : value; }
    }

    #endregion
  }
}

﻿using Afx.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Activities.OperationManagement
{
  public class TypeWrapper : BusinessObject
  {
    public TypeWrapper(bool isNull)
      : base(isNull)
    {
    }

    public TypeWrapper(Type contextType)
    {
      ContextType = contextType;
    }

    Type mContextType;
    public Type ContextType
    {
      get { return mContextType; }
      set { SetProperty<Type>(ref mContextType, value); }
    }

    public string ContextTypeName
    {
      get
      {
        if (ContextType == null) return string.Empty;
        return string.Format("{0}, {1}", ContextType.FullName, ContextType.Assembly.GetName().Name);
      }
    }

    public override int GetHashCode()
    {
      if (GlobalIdentifier == Guid.Empty) return Guid.Empty.GetHashCode();
      return ContextType.GetHashCode();
    }

    public override bool Equals(object obj)
    {
      TypeWrapper tw = obj as TypeWrapper;
      if (tw == null) return false;
      if (tw.GlobalIdentifier == Guid.Empty)
      {
        if (GlobalIdentifier == Guid.Empty) return true;
        return false;
      }
      if (ContextType == null) return false;
      return ContextType.Equals(tw.ContextType);
    }
  }
}

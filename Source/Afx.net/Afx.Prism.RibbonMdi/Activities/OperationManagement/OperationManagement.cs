﻿using Afx.Business;
using Afx.Business.Activities;
using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Business.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Activities.OperationManagement
{
  public class OperationManagement : ContextualActivityContext<OperationGroup>
  {
    public const string Key = "{4a6de7a3-65b6-4cf1-98f2-c6f2539d16d5}";
    public const string ContextRegion = "ContextRegion";

    public OperationManagement()
    {
    }

    public OperationManagement(ContextualActivity activity)
      : base(activity)
    {
    }

    public override void LoadData()
    {
      using (var svc = ProxyFactory.GetService<IAfxService>(SecurityContext.MasterServer.Name))
      {
        Data = svc.LoadOperationGroups();
      }
      base.LoadData();
    }

    public override void SaveData()
    {
      using (var svc = ProxyFactory.GetService<IAfxService>(SecurityContext.MasterServer.Name))
      {
        Data = svc.SaveOperationGroups(Data);
      }
      base.SaveData();
    }
  }
}

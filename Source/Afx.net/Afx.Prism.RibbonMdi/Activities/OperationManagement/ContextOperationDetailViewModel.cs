﻿using Afx.Business;
using Afx.Business.Activities;
using Afx.Business.Collections;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Activities.OperationManagement
{
  public class ContextOperationDetailViewModel : ActivityDetailViewModel<ContextOperation>
  {
    [InjectionConstructor]
    public ContextOperationDetailViewModel(IController controller)
      : base(controller)
    {
    }

    //#region bool IsContextOperation

    //public bool IsContextOperation
    //{
    //  get
    //  {
    //    if (Model == null) return default(bool);
    //    return Model.IsContextOperation;
    //  }
    //  set
    //  {
    //    if (Model.IsContextOperation == value) return;
    //    Model.IsContextOperation = value;
    //    if (!value) ContextType = null;
    //  }
    //}

    //#endregion

    #region TypeWrapper ContextType

    TypeWrapper mTypeWrapper;
    public TypeWrapper ContextType
    {
      get
      {
        if (Model == null) return new TypeWrapper(true);
        return mTypeWrapper ?? (mTypeWrapper = new TypeWrapper(Model.ContextType));
      }
      set
      {
        mTypeWrapper = null;
        Model.ContextType = value.ContextType;
      }
    }

    #endregion

    #region IEnumerable<TypeWrapper> ContextTypes

    static object mLock = new object();
    static Collection<TypeWrapper> mContextTypes;
    public IEnumerable<TypeWrapper> ContextTypes
    {
      get
      {
        lock (mLock)
        {
          if (mContextTypes != null) return mContextTypes;
          mContextTypes = new BasicCollection<TypeWrapper>();
          mContextTypes.Add(new TypeWrapper(true));
          foreach (Assembly a in AppDomain.CurrentDomain.GetAssemblies())
          {
            try
            {
              foreach (Type t in a.GetTypes().Where(t => !t.IsAbstract && t.IsSubclassOf(typeof(BusinessObject))))
              {
                mContextTypes.Add(new TypeWrapper(t));
              }
            }
            catch
            {
              throw;
            }
          }
          return mContextTypes;
        }
      }
    }

    #endregion
  }
}

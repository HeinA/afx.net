﻿using Afx.Business;
using Afx.Business.Activities;
using Afx.Business.Collections;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Afx.Prism.RibbonMdi.Activities
{
  public class ContextualActivityContext<TData> : ActivityContext<TData>, IContextualActivityContext
    where TData : BusinessObject
  {
    protected ContextualActivityContext()
    {
    }

    protected ContextualActivityContext(ContextualActivity activity)
      : base(activity)
    {
    }

    public new ContextualActivity Activity
    {
      get { return (ContextualActivity)base.Activity; }
      set { base.Activity = value; }
    }

    public override string ContextIdentifier
    {
      get { return Activity.Name; }
    }

    BasicCollection<TData> mData;
    public BasicCollection<TData> Data
    {
      get
      {
        if (mData == null)
        {
          mData = new BasicCollection<TData>();
          WeakEventManager<INotifyCollectionChanged, NotifyCollectionChangedEventArgs>.AddHandler(mData, "CollectionChanged", OnDataCollectionChanged);
        }
        return mData;
      }
      set
      {
        if (SetProperty<BasicCollection<TData>>(ref mData, value))
        {
          foreach (BusinessObject bo in Data)
          {
            WeakEventManager<BusinessObject, CompositionChangedEventArgs>.AddHandler(bo, "CompositionChanged", OnDataCompositionChanged);
          }
          WeakEventManager<INotifyCollectionChanged, NotifyCollectionChangedEventArgs>.AddHandler(mData, "CollectionChanged", OnDataCollectionChanged);
          OnPropertyChanged(null);
        }
      }
    }

    private void OnDataCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
    {
      IsDirty = true;
      if (e.Action == NotifyCollectionChangedAction.Add || e.Action == NotifyCollectionChangedAction.Reset || e.Action == NotifyCollectionChangedAction.Replace)
      {
        foreach (BusinessObject bo in e.NewItems)
        {
          WeakEventManager<BusinessObject, CompositionChangedEventArgs>.AddHandler(bo, "CompositionChanged", OnDataCompositionChanged);
        }
      }
      OnPropertyChanged("Title");
    }

    private void OnDataCompositionChanged(object sender, CompositionChangedEventArgs e)
    {
      IsDirty = true;
      //if (ValidateOnDataChanged && e.CompositionChangedType == CompositionChangedType.PropertyChanged && e.PropertyName != ErrorProperty && e.PropertyName != IsValidProperty && e.PropertyName != HasErrorProperty)
      //{
      //  Validate();
      //}
      OnPropertyChanged("Title");
    }

    public override bool Validate()
    {
      bool b = base.Validate();
      foreach (var bo in Data)
      {
        bo.Validate();
        if (!bo.IsValid) return false;
      }
      return b;
    }
  }
}

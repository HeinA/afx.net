﻿using System;
using System.Collections.Generic;
using System.Composition.Hosting;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi
{
  public class ExtensibilityManager
  {
    ExtensibilityManager()
    {
    }

    CompositionHost mHost = new CompositionHost();
    CompositionHost Host
    {
      get { return mHost; }
    }

    static ExtensibilityManager()
    {
      mCurrent = new ExtensibilityManager();
    }

    static ExtensibilityManager mCurrent;
    public static ExtensibilityManager Current
    {
      get { return mCurrent; }
    }
  }
}

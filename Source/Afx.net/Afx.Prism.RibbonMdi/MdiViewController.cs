﻿using Afx.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi
{
  public class MdiViewController<TModel, TViewModel> : ViewController<TModel, TViewModel>//, IMdiViewController
    where TModel : class, IBusinessObject
    where TViewModel : class, IViewModel
  {
    protected MdiViewController(IController controller)
      : base(controller)
    {
    }

    protected MdiViewController(string key, IController controller)
      : base(key, controller)
    {
    }

    IMdiApplicationController mApplicationController;
    [Dependency]
    public IMdiApplicationController ApplicationController
    {
      get { return mApplicationController; }
      set { mApplicationController = value; }
    }
  }
}

﻿using Afx.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi
{
  public class MdiReportParameterContext : BusinessObject
  {
    #region string Description

    public const string DescriptionProperty = "Description";
    string mDescription;
    public string Description
    {
      get { return mDescription; }
      set { SetProperty<string>(ref mDescription, value); }
    }

    #endregion
  }
}

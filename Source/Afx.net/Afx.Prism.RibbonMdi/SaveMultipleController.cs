﻿using Afx.Business;
using Afx.Business.Security;
using Afx.Prism.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Afx.Prism.RibbonMdi
{
  class SaveMultipleController : MdiDialogController<UnsavedDocumentContext, SaveMultipleViewModel>
  {
    public const string ControllerKey = "Afx.Prism.RibbonMdi.SaveChangesController";

    public SaveMultipleController(IController controller)
      : base(ControllerKey, controller)
    {
    }

    protected override void ApplyChanges()
    {
      try
      {
        using (new WaitCursor())
        {
          foreach (IMdiDockableDocumentController dc in DataContext.Controllers)
          {
            if (dc.IsValid && !dc.IsReadOnly)
            {
              dc.Run();
              dc.SaveDataContext();
            }
            dc.Terminate();
          }
          Terminate();
          DoPostAction();
        }
      }
      catch (Exception ex)
      {
        if (ExceptionHelper.HandleException(ex))
        {
          Terminate();
          throw;
        }
      }
      base.ApplyChanges();
    }

    public void Discard()
    {
      foreach (IMdiDockableDocumentController dc in DataContext.Controllers)
      {
        dc.Terminate();
      }
      Terminate();
      DoPostAction();
    }

    void DoPostAction()
    {
      if (DataContext.Shutdown) Application.Current.Shutdown();
      if (DataContext.Logout)
      {
        SecurityContext.Logout();
        ApplicationController.EventAggregator.GetEvent<UserAuthenticatedEvent>().Publish(EventArgs.Empty);
      }
    }
  }
}

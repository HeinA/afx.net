﻿using Afx.Business;
using Afx.Business.Collections;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Afx.Prism.RibbonMdi
{
  public abstract class ContextBase : BusinessObject, IContextBase
  {
    public ContextBase()
    {
      IsNew = false;
    }

    public const string ContextIdentifierProperty = "ContextIdentifier";
    public virtual string ContextIdentifier
    {
      get
      {
        return Guid.NewGuid().ToString("B");
      }
    }

    //protected virtual bool ValidateOnDataChanged
    //{
    //  get { return false; }
    //}

    protected override void OnPropertyChanged(string propertyName)
    {
      if (propertyName != "Title")
      {
        base.OnPropertyChanged("Title");
      }
      base.OnPropertyChanged(propertyName);
    }

    public string Title
    {
      get
      {
        if (IsDirty) return string.Format("{0}*", TitleText);
        return TitleText;
      }
    }

    public virtual string TitleText
    {
      get { return "Document"; }
    }

    public override bool IsDirty
    {
      get { return base.IsDirty; }
      set
      {
        base.IsDirty = value;
        OnPropertyChanged("Title");
      }
    }

    public virtual void LoadData()
    {
      IsDirty = false;
    }

    public virtual void SaveData()
    {
      IsDirty = false;
    }

    public virtual bool IsReadOnly
    {
      get { return false; }
    }
  }
}

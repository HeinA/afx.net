﻿using Afx.Business;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi
{
  public abstract class MdiDockableToolController<TModel, TViewModel> : MdiViewController<TModel, TViewModel>, IMdiDockableToolController, IInternalDockableController
    where TModel : class, IBusinessObject
    where TViewModel : MdiDockableToolViewModel<TModel>
  {
    protected MdiDockableToolController(IController controller)
      : base(controller)
    {
    }

    protected MdiDockableToolController(string key, IController controller)
      : base(key, controller)
    {
    }

    protected override void ConfigureContainer()
    {
      IRegionManager rm = GetInstance<IRegionManager>();
      IRegionManager scoped = rm.CreateRegionManager();
      Container.RegisterInstance<IRegionManager>(scoped);
      Container.RegisterInstance<IMdiDockableToolController>(this);
      base.ConfigureContainer();
    }

    protected override bool OnRun()
    {
      if (State == ControllerState.Running)
      {
        Activate();
        return false;
      }

      if (DataContext == null) throw new InvalidOperationException("DataContext may not be null.");
      ApplicationController.ApplicationContext.ToolViewModels.Add(ViewModel);
      return base.OnRun();
    }

    public void OnRefresh()
    {
    }

    public override void Terminate()
    {
      base.Terminate();
      ApplicationController.ApplicationContext.ToolViewModels.Remove(ViewModel);
    }

    public virtual void Activate()
    {
      ViewModel.Activate();
    }

    #region void OnLoaded()

    protected virtual void OnLoaded()
    {
    }

    void IInternalDockableController.OnLoaded()
    {
      OnLoaded();
    }

    #endregion
  }
}

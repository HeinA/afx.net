﻿using Afx.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace Afx.Prism.RibbonMdi
{
  class SaveChangesController : MdiDialogController<ContextBase, SaveChangesViewModel>
  {
    public SaveChangesController(IController controller)
      : base(controller)
    {
    }

    [Dependency]
    public IMdiDockableDocumentController DocumentController { get; set; }

    protected override bool OnRun()
    {
      DocumentController.Activate();
      return base.OnRun();
    }

    protected override void ApplyChanges()
    {
      try
      {
        using (new WaitCursor())
        {
          DocumentController.SaveDataContext();
          DocumentController.Terminate();
        }
      }
      catch (Exception ex)
      {
        if (ExceptionHelper.HandleException(ex))
        {
          Terminate();
          throw;
        }
      }
      base.ApplyChanges();
    }

    public void Discard()
    {
      DocumentController.Terminate();
      Terminate();
    }
  }
}

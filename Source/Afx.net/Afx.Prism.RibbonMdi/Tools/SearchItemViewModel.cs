﻿using Afx.Business;
using Afx.Business.Documents;
using Afx.Prism.RibbonMdi.Events;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Afx.Prism.RibbonMdi.Tools
{
  public abstract class SearchItemViewModel : ViewModel, IDragable
  {
    protected SearchItemViewModel(IController controller)
      : base(controller)
    {
    }

    public virtual string Name
    {
      get { return DocumentType.Name; }
    }

    #region DelegateCommand SearchCommand

    DelegateCommand mSearchCommand;
    public DelegateCommand SearchCommand
    {
      get { return mSearchCommand ?? (mSearchCommand = new DelegateCommand(OnExecuteSearch, CanExecuteSearch)); }
    }

    bool CanExecuteSearch()
    {
      return true;
    }

    void OnExecuteSearch()
    {
      try
      {
        using (new WaitCursor())
        {
          if (SelectedResults != null) SelectedResults.Clear();
          Results = ExecuteSearch();
        }
      }
      catch (Exception ex)
      {
        if (ExceptionHelper.HandleException(ex)) throw;
      }
    }

    #endregion

    protected virtual SearchResults ExecuteSearch()
    {
      return null;
    }

    #region DelegateCommand ItemActivatedCommand

    DelegateCommand mItemActivatedCommand;
    public DelegateCommand ItemActivatedCommand
    {
      get { return mItemActivatedCommand ?? (mItemActivatedCommand = new DelegateCommand(ExecuteItemActivated, CanExecuteItemActivated)); }
    }

    private void ExecuteItemActivated()
    {
      ActivateItem(LastActivatedResult);
    }

    bool CanExecuteItemActivated()
    {
      return true;
    }

    #endregion

    #region SearchResults Results

    SearchResults mResults;
    public SearchResults Results
    {
      get { return mResults; }
      set { SetProperty<SearchResults>(ref mResults, value); }
    }

    #endregion

    #region Guid LastActivatedResult

    Guid mLastActivatedResult;
    public Guid LastActivatedResult
    {
      get { return mLastActivatedResult; }
      set { SetProperty<Guid>(ref mLastActivatedResult, value); }
    }

    #endregion

    #region ObservableCollection<SearchResult> SelectedResults

    ObservableCollection<SearchResult> mSelectedResults;
    public ObservableCollection<SearchResult> SelectedResults
    {
      get { return mSelectedResults; }
      set { SetProperty<ObservableCollection<SearchResult>>(ref mSelectedResults, value); }
    }

    #endregion

    #region void ActivateItem(Guid globalIdentifier)

    protected virtual void ActivateItem(Guid globalIdentifier)
    {
      ApplicationController.Current.EventAggregator.GetEvent<EditDocumentEvent>().Publish(new EditDocumentEventArgs(DocumentType, globalIdentifier));
    }

    #endregion

    public abstract DocumentType DocumentType
    {
      get;
    }

    object IDragable.GetDragData()
    {
      if (SelectedResults == null) return null;
      Collection<DraggedDocumentReference> references = new Collection<DraggedDocumentReference>();
      foreach(var sr in SelectedResults)
      {
        references.Add(new DraggedDocumentReference(DocumentType, sr.GlobalIdentifier));
      }
      return references;
    }
  }
}

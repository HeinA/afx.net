﻿using Afx.Business;
using Afx.Business.Data;
using Afx.Business.Documents;
using Afx.Business.Security;
using Afx.Business.Service;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Xceed.Wpf.DataGrid;

namespace Afx.Prism.RibbonMdi.Tools
{
  public class SearchViewModel : MdiDockableToolViewModel<SearchContext>
  {
    [InjectionConstructor]
    public SearchViewModel(IController controller)
      : base(controller)
    {
      Title = "Search";

      WeakEventManager<INotifyCollectionChanged, NotifyCollectionChangedEventArgs>.AddHandler((INotifyCollectionChanged)MdiApplicationController.Current.SearchViewModels, "CollectionChanged", OnSearchViewModelsChanged);
    }

    private void OnSearchViewModelsChanged(object sender, NotifyCollectionChangedEventArgs e)
    {
      OnPropertyChanged("SearchViewModels");
    }

    #region string ContentId

    public override string ContentId
    {
      get { return SearchContext.ControllerKey; }
    }

    #endregion

    public IEnumerable<SearchItemViewModel> SearchViewModels
    {
      get { return MdiApplicationController.Current.SearchViewModels.OrderBy(vm => vm.Name); }
    }

    #region SearchItemViewModel SelectedSearchViewModel

    SearchItemViewModel mSelectedSearchViewModel;
    public SearchItemViewModel SelectedSearchViewModel
    {
      get { return mSelectedSearchViewModel; }
      set
      {
        IRegion svr = RegionManager.Regions[SearchContext.SearchViewRegion];
        if (svr.Views != null && svr.Views.Count() > 0)
        {
          foreach (object o in svr.Views)
          {
            if (o != null) svr.Remove(o);
          }
        }
        SetProperty<SearchItemViewModel>(ref mSelectedSearchViewModel, value);
        if (svr.Views != null && mSelectedSearchViewModel != null) svr.Add(mSelectedSearchViewModel);
      }
    }

    #endregion
  }
}

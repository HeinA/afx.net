﻿using Afx.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Tools.Keyboard
{
  public class KeyboardContext : BusinessObject
  {
    public const string ControllerKey = "Afx.Prism.RibbonMdi.Tools.Keyboard.KeyboardController";
  }
}

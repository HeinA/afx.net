﻿using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Tools.Keyboard
{
  public class KeyboardController : MdiDockableToolController<KeyboardContext, KeyboardViewModel>
  {
    [InjectionConstructor]
    public KeyboardController(IController controller)
      : base(KeyboardContext.ControllerKey, controller)
    {
      DataContext = new KeyboardContext();
    }
  }
}

﻿using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Tools
{
  public class SearchController : MdiDockableToolController<SearchContext, SearchViewModel>
  {
    [InjectionConstructor]
    public SearchController(IController controller)
      : base(SearchContext.ControllerKey, controller)
    {
      DataContext = new SearchContext();
    }

    protected override void ConfigureContainer()
    {
      MdiApplicationController.Current.Container.RegisterInstance<SearchController>(this);
      base.ConfigureContainer();
    }
  }
}

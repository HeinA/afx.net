﻿using Afx.Prism.RibbonMdi.Events;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Tools.Output
{
  public class OutputController : MdiDockableToolController<OutputContext, OutputViewModel>
  {
    [InjectionConstructor]
    public OutputController(IController controller)
      : base(OutputContext.ControllerKey, controller)
    {
      DataContext = new OutputContext();
    }

    protected override void ConfigureContainer()
    {
      MdiApplicationController.Current.Container.RegisterInstance<OutputController>(this);
      base.ConfigureContainer();
    }

    protected override void OnRunning()
    {
      MdiApplicationController.Current.EventAggregator.GetEvent<DisplayOutputEvent>().Subscribe(OnDisplayText);
      base.OnRunning();
    }

    private void OnDisplayText(DisplayOutputEventArgs obj)
    {
      //if (!ViewModel.IsVisible) ViewModel.IsVisible = true;
      if (obj.ClearOutput)
      {
        ViewModel.Text = string.Format("{0}\n", obj.Text);
      }
      else
      {
        ViewModel.Text += string.Format("{0}\n", obj.Text);
      }
    }
  }
}

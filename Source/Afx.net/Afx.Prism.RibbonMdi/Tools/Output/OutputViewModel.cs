﻿using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Tools.Output
{
  public class OutputViewModel : MdiDockableToolViewModel<OutputContext>
  {
    [InjectionConstructor]
    public OutputViewModel(IController controller)
      : base(controller)
    {
      Title = "Output";
    }

    #region string ContentId

    public override string ContentId
    {
      get { return OutputContext.ControllerKey; }
    }

    #endregion

    #region string Text

    public const string TextProperty = "Text";
    string mText;
    public string Text
    {
      get { return mText; }
      set { SetProperty<string>(ref mText, value); }
    }

    #endregion
  }
}

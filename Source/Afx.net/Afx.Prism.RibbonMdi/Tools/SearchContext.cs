﻿using Afx.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Tools
{
  public class SearchContext : BusinessObject
  {
    public const string ControllerKey = "Afx.Prism.RibbonMdi.Tools.SearchController";
    public const string SearchViewRegion = "Afx.Prism.RibbonMdi.Tools.SearchViewRegion";
  }
}

﻿using Afx.Business;
using Afx.Business.Activities;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Business.Service;
using Afx.Prism.Events;
using Afx.Prism.RibbonMdi.Activities;
using Afx.Prism.RibbonMdi.Activities.OrganizationalStructureManagement;
using Afx.Prism.RibbonMdi.Activities.OrganizationalUnitManagement;
using Afx.Prism.RibbonMdi.Activities.ServerInstanceManagement;
using Afx.Prism.RibbonMdi.Activities.UserManagement;
using Afx.Prism.RibbonMdi.Events;
using Afx.Prism.RibbonMdi.Login;
using Afx.Prism.RibbonMdi.Ribbon;
using Afx.Prism.RibbonMdi.Tools;
using Afx.Prism.RibbonMdi.Tools.Keyboard;
using Afx.Prism.RibbonMdi.Tools.Output;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using Xceed.Wpf.Toolkit;

namespace Afx.Prism.RibbonMdi
{
  public class RibbonMdiModuleController : MdiModuleController
  {
    [InjectionConstructor]
    public RibbonMdiModuleController(IController controller)
      : base("Ribbon Mdi Module Controller", controller)
    {
    }

    protected override bool OnRun()
    {
      //DebugHelper.LogMessage("RibbonMdiModuleController OnRun()...");

      ApplicationController.Dispatcher.BeginInvoke(new Action(() =>
      {
        try
        {
          AuthenticationResult ar = null;
          int retries = 0;
          bool loggedin = false;
          while (!loggedin)
          {
            try
            {
              ar = MdiBootstrapper.Current.GetLoginDetails();
              if (ar == null) break;
              //DebugHelper.LogMessage("Attempting Automated Login...");
              LoginUser(ar);
              loggedin = true;
            }
            catch (Exception ex)
            {
              if (retries++ > 5)
              {
                ExceptionHelper.HandleException(ex);
                break;
              }
              Thread.Sleep(1000);
            }
          }
          if (ar == null)
          {
            ApplicationController.ShowLoginDialog();
          }
        }
        catch (Exception ex)
        {
          if (ExceptionHelper.HandleException(ex)) throw;
        }
      }), DispatcherPriority.ApplicationIdle);

      bool b = base.OnRun();
      //DebugHelper.LogMessage("RibbonMdiModuleController OnRun() done.");
      return b;
    }

    SearchController mSearchController;
    public SearchController SearchController
    {
      get { return mSearchController; }
      private set { mSearchController = value; }
    }

    OutputController mOutputController;
    public OutputController OutputController
    {
      get { return mOutputController; }
      private set { mOutputController = value; }
    }

    //KeyboardController mKeyboardController;
    //public KeyboardController KeyboardController
    //{
    //  get { return mKeyboardController; }
    //  private set { mKeyboardController = value; }
    //}

    protected override void OnRunning()
    {
      try
      {
        SearchController = GetCreateChildController<SearchController>(SearchContext.ControllerKey);
        SearchController.Run();

        OutputController = GetCreateChildController<OutputController>(OutputContext.ControllerKey);
        OutputController.Run();

        //KeyboardController = GetCreateChildController<KeyboardController>(KeyboardContext.ControllerKey);
        //KeyboardController.Run();
      }
      catch
      {
        throw;
      }

      EventAggregator.GetEvent<ExecuteSearchEvent>().Subscribe(OnExecuteSearch);
      base.OnRunning();
    }

    private void OnExecuteSearch(EventArgs obj)
    {
      SearchController.Activate();
    }

    protected override void OnUserAuthenticated(EventArgs obj)
    {
      if (SecurityContext.User == null)
      {
        ApplicationController.ShowLoginDialog();
      }
      base.OnUserAuthenticated(obj);
    }

    protected override void ConfigureRibbon()
    {
      //ApplicationController.ApplicationContext.StaticRibbonTabs.Add(GetInstance<HomeTabViewModel>());

      base.ConfigureRibbon();
    }

    internal override void ConfigureRibbonEnd()
    {
      base.ConfigureRibbonEnd();
    }

    public void LoginUser(AuthenticationResult ar)
    {
      if (ar.User != null)
      {
        //DebugHelper.LogMessage("Logging in user...");
        DataTemplate dt = (DataTemplate)Application.Current.Resources["Afx.Prism.RibbonMdi.SplashScreen"];
        ChildWindow cw = (ChildWindow)dt.LoadContent();
        RegionManager.Regions[MdiApplication.DialogRegion].Add(cw);
        ApplicationController.ApplicationContext.IsRibbonEnabled = false;

        SecurityContext.Login(ar);


        BasicCollection<BusinessObject> cache = null;
        Stopwatch sw = new Stopwatch();
        sw.Start();

        BackgroundWorker bw = new BackgroundWorker();
        bw.DoWork += (s, e) =>
          {
            using (var svc = ProxyFactory.GetService<IAfxService>(SecurityContext.ServerName))
            {
              cache = svc.LoadCache();
            }
          };
        bw.RunWorkerCompleted += (s, e) =>
          {
            try
            {
              if (e.Error != null) throw e.Error;
              Cache.Instance.LoadCache(cache);
              sw.Stop();

              ApplicationController.EventAggregator.GetEvent<UserAuthenticatedEvent>().Publish(EventArgs.Empty);
              bw.Dispose();
              RegionManager.Regions[MdiApplication.DialogRegion].Remove(cw);
            }
            catch(Exception ex)
            {
              if (ExceptionHelper.HandleException(ex)) throw;
            }
            finally
            {
              ApplicationController.ApplicationContext.IsRibbonEnabled = true;
            }
          };
        bw.RunWorkerAsync();
      }
    }


    protected override void ExecuteActivity(Activity activity, Guid ContextId)
    {
      switch (activity.Identifier)
      {
        case OrganizationalStructureManagement.Key:
          OpenActivity<OrganizationalStructureManagement, OrganizationalStructureManagementController>(activity);
          break;

        case UserManagement.Key:
          OpenActivity<UserManagement, UserManagementController>(activity);
          break;

        case OrganizationalUnitManagement.Key:
          OpenActivity<OrganizationalUnitManagement, OrganizationalUnitManagementController>(activity);
          break;

        //case OperationManagement.Key:
        //  OpenActivity<OperationManagement, OperationManagementController>(activity);
        //  break;

        //case ActivityManagement.Key:
        //  OpenActivity<ActivityManagement, ActivityManagementController>(activity);
        //  break;

        //case DocumentManagement.Key:
        //  OpenActivity<DocumentManagement, DocumentManagementController>(activity);
        //  break;

        case ServerInstanceManagement.Key:
          OpenActivity<ServerInstanceManagement, ServerInstanceManagementController>(activity);
          break;
      }

      base.ExecuteActivity(activity, ContextId);
    }


  }
}

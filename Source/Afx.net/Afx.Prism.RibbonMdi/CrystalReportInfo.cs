﻿using CrystalDecisions.CrystalReports.Engine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi
{
  public abstract class CrystalReportInfo<T> : ICrystalReportInfo
    where T : Afx.Prism.Controller, IMdiReportParameterController
  {
    public abstract string GroupName { get; }
    public abstract string ReportName { get; }
    public abstract byte[] GetReport();


    public void SetParameters(ReportDocument doc) 
    {
      var c = MdiApplicationController.Current.CreateChildController<T>();
      try
      {
        c.ReportDocument = doc;
        c.Run();
      }
      catch
      {
        c.Terminate();
      }
    }
  }
}

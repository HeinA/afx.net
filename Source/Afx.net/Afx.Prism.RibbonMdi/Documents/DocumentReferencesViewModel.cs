﻿using Afx.Business.Documents;
using Afx.Business.Security;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Documents
{
  public class DocumentReferencesViewModel : ExtensionViewModel<Document>
  {
    #region Constructors

    [InjectionConstructor]
    public DocumentReferencesViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    public override bool IsReadOnly
    {
      get
      {
        return Model.IsReadOnly;
      }
    }

    public string Title
    {
      get { return "_References"; }
    }

    IEnumerable<DocumentTypeReference> mDocumentTypeReferences;
    public IEnumerable<DocumentTypeReference> DocumentTypeReferences
    {
      get { return mDocumentTypeReferences ?? (mDocumentTypeReferences = Model.DocumentType.References.OrderBy(r => r.Name)); }
    }

    public IEnumerable<DocumentReference> References
    {
      get { return Model == null ? null : Model.References; }
    }
  }
}

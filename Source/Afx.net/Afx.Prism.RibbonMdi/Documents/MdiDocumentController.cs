﻿using Afx.Business;
using Afx.Business.Activities;
using Afx.Business.Data;
using Afx.Business.Documents;
using Afx.Business.Security;
using Afx.Business.Service;
using Afx.Business.ServiceModel;
using Afx.Prism.Documents;
using Afx.Prism.Events;
using Afx.Prism.RibbonMdi.Events;
using Afx.Prism.RibbonMdi.Login;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.ReportAppServer.Controllers;
using Microsoft.Practices.ObjectBuilder2;
using Microsoft.Practices.Prism.PubSubEvents;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Unity;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;

namespace Afx.Prism.RibbonMdi.Documents
{
  public abstract class MdiDocumentController<TModel, TViewModel> : MdiDockableDocumentController<TModel, TViewModel>, IMdiDocumentController
    where TModel : ContextBase, IDocumentContext
    where TViewModel : MdiDocumentViewModel<TModel>
  {
    public const string TabRegion = Afx.Prism.RibbonMdi.Documents.Regions.TabRegion;
    public const string DockRegion = Afx.Prism.RibbonMdi.Documents.Regions.DockRegion;
    public const string HeaderRegion = Afx.Prism.RibbonMdi.Documents.Regions.HeaderRegion;
    public const string FooterRegion = Afx.Prism.RibbonMdi.Documents.Regions.FooterRegion;

    protected MdiDocumentController(IController controller)
      : base(controller)
    {
    }

    protected MdiDocumentController(string key, IController controller)
      : base(key, controller)
    {
    }

    protected ICrystalPrintDocumentInfo GetCrystalPrintDocument(string key)
    {
      if (key == null) return ExtensibilityManager.CompositionContainer.GetExportedValues<ICrystalPrintDocumentInfo>().FirstOrDefault(r => r.DocumentType.Equals(DataContext.ContextType) && r.Key == null);
      return ExtensibilityManager.CompositionContainer.GetExportedValues<ICrystalPrintDocumentInfo>().FirstOrDefault(r => r.DocumentType.Equals(DataContext.ContextType) && r.Key.Equals(key));
    }
    
    protected string PrintCrystal()
    {
      return PrintCrystal(null);
    }

    protected string PrintCrystal(string key, params object[] args)
    {
      string fn = string.Format("{0}{1}.rpt", Path.GetTempPath(), Guid.NewGuid().ToString());
      try
      {
        ICrystalPrintDocumentInfo info = GetCrystalPrintDocument(key);
        if (info == null) throw new InvalidOperationException("Could not find Print Document");

        var bytes = info.GetReport();
        File.WriteAllBytes(fn, bytes);

        ReportDocument boReportDocument = new ReportDocument();
        boReportDocument.Load(fn);
        UpdateReportDetails(boReportDocument);

        info.SetParameters(boReportDocument, DataContext.Document, args);

        try
        {
          fn = string.Format("{0}{1} - {2:yyyy.MM.dd.HH.mm.ss}.pdf", Path.GetTempPath(), boReportDocument.SummaryInfo.ReportTitle, DateTime.Now);
          boReportDocument.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, fn);
          Process.Start(fn);
        }
        catch (ArgumentException)
        {
          fn = string.Format("{0}{1}.pdf", Path.GetTempPath(), Guid.NewGuid());
          boReportDocument.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, fn);
          Process.Start(fn);
        }
      }
      catch (Exception ex)
      {
        if (ExceptionHelper.HandleException(ex)) throw;
      }

      return fn;
    }
    
    void UpdateReportDetails(ReportDocument boReportDocument)
    {
      var boReportClientDocument = boReportDocument.ReportClientDocument;
      var boDatabaseController = boReportClientDocument.DatabaseController;
      var boDatabase = boDatabaseController.Database;
      var boTables = boDatabase.Tables;

      foreach (CrystalDecisions.ReportAppServer.DataDefModel.ISCRTable boTableOld in boTables)
      {
        CrystalDecisions.ReportAppServer.DataDefModel.ISCRTable boTableNew = boTableOld.Clone(true);

        var boConnectionInfo = boTableNew.ConnectionInfo;
        var boAttributesPropertyBag = (CrystalDecisions.ReportAppServer.DataDefModel.PropertyBag)boConnectionInfo.Attributes;

        string oldDb = boAttributesPropertyBag["QE_DatabaseName"];
        boAttributesPropertyBag["QE_DatabaseName"] = ReportingHelper.ReportCredentials.Database;
        boAttributesPropertyBag["QE_ServerDescription"] = ReportingHelper.ReportCredentials.Server;

        var boLogonPropertyBag = (CrystalDecisions.ReportAppServer.DataDefModel.PropertyBag)boAttributesPropertyBag["QE_LogonProperties"];
        boLogonPropertyBag["Data Source"] = ReportingHelper.ReportCredentials.Server;
        boLogonPropertyBag["Initial Catalog"] = ReportingHelper.ReportCredentials.Database;

        boTableNew.QualifiedName = string.Format("{0}{1}", ReportingHelper.ReportCredentials.Database, boTableNew.QualifiedName.Substring(oldDb.Length));

        boTableNew.ConnectionInfo.UserName = ReportingHelper.ReportCredentials.UserName;
        boTableNew.ConnectionInfo.Password = ReportingHelper.ReportCredentials.Password;

        boDatabaseController.SetTableLocation(boTableOld, boTableNew);
      }

      foreach (ReportDocument boSubReport in boReportDocument.Subreports)
      {
        UpdateSubReportDetails(boReportClientDocument.SubreportController.GetSubreport(boSubReport.Name));
      }
    }

    void UpdateSubReportDetails(SubreportClientDocument boReportClientDocument)
    {
      var boDatabaseController = boReportClientDocument.DatabaseController;
      var boDatabase = boDatabaseController.Database;
      var boTables = boDatabase.Tables;

      foreach (CrystalDecisions.ReportAppServer.DataDefModel.ISCRTable boTableOld in boTables)
      {
        CrystalDecisions.ReportAppServer.DataDefModel.ISCRTable boTableNew = boTableOld.Clone(true);

        var boConnectionInfo = boTableNew.ConnectionInfo;
        var boAttributesPropertyBag = (CrystalDecisions.ReportAppServer.DataDefModel.PropertyBag)boConnectionInfo.Attributes;

        string oldDb = boAttributesPropertyBag["QE_DatabaseName"];
        boAttributesPropertyBag["QE_DatabaseName"] = ReportingHelper.ReportCredentials.Database;
        boAttributesPropertyBag["QE_ServerDescription"] = ReportingHelper.ReportCredentials.Server;

        var boLogonPropertyBag = (CrystalDecisions.ReportAppServer.DataDefModel.PropertyBag)boAttributesPropertyBag["QE_LogonProperties"];
        boLogonPropertyBag["Data Source"] = ReportingHelper.ReportCredentials.Server;
        boLogonPropertyBag["Initial Catalog"] = ReportingHelper.ReportCredentials.Database;

        boTableNew.QualifiedName = string.Format("{0}{1}", ReportingHelper.ReportCredentials.Database, boTableNew.QualifiedName.Substring(oldDb.Length));

        boTableNew.ConnectionInfo.UserName = ReportingHelper.ReportCredentials.UserName;
        boTableNew.ConnectionInfo.Password = ReportingHelper.ReportCredentials.Password;

        boDatabaseController.SetTableLocation(boTableOld, boTableNew);
      }
    }

    protected override void ConfigureContainer()
    {
      Container.RegisterInstance<IMdiDocumentController>(this);
      this.EventAggregator.GetEvent<UpdateDocumentInfoEvent>().Subscribe(OnUpdateDocumentInfo);
      base.ConfigureContainer();
    }

    private void OnUpdateDocumentInfo(EventArgs obj)
    {
      ViewModel.Refresh();
    }

    IDocumentContext IMdiDocumentController.DataContext
    {
      get { return DataContext; }
      set { DataContext = (TModel)value; }
    }

    protected override void OnCacheUpdated()
    {
      using (new EventStateSuppressor(DataContext))
      {
        DataContext.DocumentType = Cache.Instance.GetObject<DocumentType>(DataContext.DocumentType.GlobalIdentifier);
      }
      ViewModel.ResetOperations();
      base.OnCacheUpdated();
    }

    public virtual string AdditionalStateText
    {
      get { return null; }
    }

    internal bool mLoaded = false;
    protected override void OnLoaded()
    {
      ExtendDocumentIntenal();
      base.OnLoaded();
      mLoaded = true;
    }

    #region IEventAggregator EventAggregator

    IEventAggregator mEventAggregator;
    public IEventAggregator EventAggregator
    {
      get { return mEventAggregator ?? (mEventAggregator = new EventAggregator()); }
    }

    #endregion

    public TView ExtendDocument<TExtension, TView>(string regionName)
      where TExtension : class, IExtensionObject, new()
      where TView : class, IExtensionViewModel
    {
      TExtension eo = DataContext.Document.GetExtensionObject<TExtension>();
      if (eo == null) return null;

      TView vm = GetViewModel<TView>();
      IRegion region = RegionManager.Regions[regionName];

      if (vm != null)
      {
        vm.Model = eo;
      }
      else
      {
        vm = GetCreateViewModel<TView>(eo, ViewModel);
      }

      vm.IsReadOnly = DataContext.IsReadOnly;
      if (!region.Views.Contains(vm))
      {
        region.Add(vm);
      }

      return vm;
    }

    public TView ExtendDocument<TView>(string regionName)
      where TView : class, IExtensionViewModel
    {
      TView vm = GetViewModel<TView>();
      IRegion region = RegionManager.Regions[regionName];

      if (vm != null)
      {
        vm.Model = this.DataContext.Document;
      }
      else
      {
        vm = GetCreateViewModel<TView>(this.DataContext.Document, ViewModel);
      }

      vm.IsReadOnly = DataContext.IsReadOnly;

      if (!region.Views.Contains(vm))
      {
        region.Add(vm);
      }

      return vm;
    }

    const int DocumentRegions = 4;
    protected void ExtendDocumentIntenal()
    {
      if (RegionManager.Regions.Count() != DocumentRegions)
      {
        RegionManager.Regions.CollectionChanged += Regions_CollectionChanged;
      }
      else
      {
        ExtendDocument();
      }
    }

    void Regions_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
    {
      if (RegionManager.Regions.Count() == DocumentRegions)
      {
        RegionManager.Regions.CollectionChanged -= Regions_CollectionChanged;
        ExtendDocument();
      }
    }

    protected virtual void ExtendDocument()
    {
      ApplicationController.EventAggregator.GetEvent<ExtendDocumentEvent>().Publish(new ExtendDocumentEventArgs(this, this.DataContext.Document));
    }

    protected override void OnDataContextPropertyChanged(string propertyName)
    {
      if (propertyName == "Document")
      {
        ViewModel.ResetOperations();
      }
      base.OnDataContextPropertyChanged(propertyName);
    }

    protected override void OnDataContextChanged()
    {
      if (DataContext != null) WeakEventManager<TModel, EventArgs>.AddHandler(DataContext, "DocumentUpdated", DocumentUpdated);
      base.OnDataContextChanged();
    }

    private void DocumentUpdated(object sender, EventArgs e)
    {
      if (mLoaded) ExtendDocumentIntenal();
    }

    public override bool IsOperationVisible(Operation op)
    {
      if (!DataContext.Document.CanEdit) return false;
      if (!DataContext.Document.CanExecute(op)) return false;

      if (op.Identifier == StandardOperations.Transfer)
      {
        //COU
        //if (DataContext.Document.ControllingOrganizationalUnit.Server.Equals(SecurityContext.OrganizationalUnit.Server)) 
        return false;
      }
      else
      {
        //COU
        //if (DataContext.Document.State.DocumentTypeState.Operations.Contains(op))
        //{
        //  if (!DataContext.Document.ControllingOrganizationalUnit.Server.Equals(SecurityContext.OrganizationalUnit.Server)) return false;
        //}
      }

      return base.IsOperationVisible(op);
    }

    public override bool IsValid
    {
      get
      {
        return DataContext.Validate();
      }
    }

    protected override void InitialDataContextLoad()
    {
      if (DataContext.Document == null) LoadDataContext();
      else ViewModel.ResetOperations();
    }

    protected override void LoadDataContext()
    {
      if (DataContext.DocumentIdentifier == Guid.Empty || DataContext.IsNew)
      {
        DataContext.CreateDocument();
      }
      else
      {
        DataContext.LoadData();
      }
      ViewModel.ResetOperations();

      base.LoadDataContext();
    }

    #region bool CancelSave

    public const string CancelSaveProperty = "CancelSave";
    bool mCancelSave = false;
    public bool CancelSave
    {
      get { return mCancelSave; }
      set { mCancelSave = value; }
    }

    #endregion

    protected override void SaveDataContext()
    {
      if (!DataContext.Validate() || CancelSave) return;
      DataContext.SaveData();
      ViewModel.ResetOperations();
      MdiApplicationController.Current.EventAggregator.GetEvent<DocumentSavedEvent>().Publish(new DocumentSavedEventArgs(this.DataContext.Document));

      base.SaveDataContext();
    }

    private void PrintReport(bool preview, object argument)
    {
      if (ReportInfo == null)
      {
        string contractName = this.DataContext.DocumentType.GlobalIdentifier.ToString("b");
        ReportInfo = ExtensibilityManager.CompositionContainer.GetExportedValues<IDocumentPrintInfo>(contractName).OrderBy(ri1 => ri1.Priority).FirstOrDefault();
        if (ReportInfo == null)
        {
          System.Media.SystemSounds.Hand.Play();
          return;
        }
      }

      BackgroundWorker bw = new BackgroundWorker();
      bw.DoWork += (s, e) =>
        {
          try
          {
            // Variables
            Warning[] warnings;
            string[] streamIds;
            string mimeType = string.Empty;
            string encoding = string.Empty;
            string extension = string.Empty;


            // Setup the report viewer object and get the array of bytes
            ReportViewer viewer = new ReportViewer();
            viewer.LocalReport.LoadReportDefinition(ReportInfo.ReportStream(argument));

            SetupReport(viewer);
            object scope = ReportInfo.GetScope(this);
            RefreshReportData(viewer, scope, argument);

            if (preview)
            {
              byte[] bytes = viewer.LocalReport.Render("PDF", null, out mimeType, out encoding, out extension, out streamIds, out warnings);
              string file = string.Format("{0}.pdf", Path.GetTempFileName());
              File.WriteAllBytes(file, bytes);
              Process.Start(file);
            }
            else
            {
              ReportPrintDocument pd = new ReportPrintDocument(viewer.LocalReport);
              pd.PrinterSettings.Copies = ReportInfo.Copies;
              pd.Print();
            }
          }
          catch
          {
            throw;
          }
        };
      bw.RunWorkerCompleted += (s, e) =>
        {
          bw.Dispose();
        };
      bw.RunWorkerAsync();
    }

    #region IReportInfo ReportInfo

    IDocumentPrintInfo mReportInfo;
    public IDocumentPrintInfo ReportInfo
    {
      get { return mReportInfo; }
      set { mReportInfo = value; }
    }

    #endregion

    protected void SetupReport(ReportViewer viewer)
    {
      PageSettings pageSettings = viewer.GetPageSettings();
      PaperSize psA4 = null;
      bool b = viewer.LocalReport.GetDefaultPageSettings().IsLandscape;
      foreach (PaperSize ps in pageSettings.PrinterSettings.PaperSizes)
      {
        if (ps.Kind == PaperKind.A4)
        {
          psA4 = ps;
          break;
        }
      }
      if (psA4 != null)
      {
        pageSettings.PaperSize = psA4;
        viewer.SetPageSettings(pageSettings);
      }

      viewer.LocalReport.DataSources.Clear();
      viewer.ProcessingMode = ProcessingMode.Local;
    }

    protected void RefreshReportData(ReportViewer rv, object scope, object argument)
    {
      ReportInfo.RefreshReportData(rv.LocalReport, this.DataContext.Document, scope, argument);
      rv.LocalReport.Refresh();
      rv.RefreshReport();
    }

    public void Print(bool preview)
    {
      Print(preview, true, null);
    }

    protected void Print(bool preview = false, bool doNotSave = false, object argument = null)
    {
      if (!OnBeforePrint()) return;

      if (!doNotSave)
      {
        if (!Save()) return;
      }

      PrintReport(preview, argument);
      OnPrinted();
    }

    protected virtual bool OnBeforePrint()
    {
      return true;
    }

    protected virtual void OnPrinted()
    {
    }

    protected override void ExecuteOperation(Operation op, BusinessObject argument)
    {
      switch (op.Identifier)
      {
        case StandardOperations.Print:
          Print();
          break;

        case StandardOperations.PrintPreview:
          Print(true);
          break;

        case StandardOperations.Transfer:
          using (var svc = ProxyFactory.GetService<IAfxService>(SecurityContext.ServerName))
          {
            DataContext.Document = svc.TransferAllDocuments(DataContext.Document.GlobalIdentifier, SecurityContext.OrganizationalUnit.GlobalIdentifier);
          }
          ViewModel.ResetOperations();
          break;
      }

      ApplicationController.EventAggregator.GetEvent<ExecuteOperationEvent>().Publish(new ExecuteOperationEventArgs(this, op, argument));
      base.ExecuteOperation(op, argument);
    }
  }
}

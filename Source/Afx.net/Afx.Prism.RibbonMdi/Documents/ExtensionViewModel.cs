﻿using Afx.Business;
using Microsoft.Practices.Prism;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Documents
{
  public class ExtensionViewModel<TModel> : ViewModel<TModel>, IExtensionViewModel, IActiveAware
    where TModel : class, IBusinessObject
  {
    #region Constructors

    [InjectionConstructor]
    public ExtensionViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    #region bool IsReadOnly

    bool mIsReadOnly = false;
    public override bool IsReadOnly
    {
      get { return mIsReadOnly; }
    }

    bool IExtensionViewModel.IsReadOnly
    {
      set
      {
        mIsReadOnly = value;
        OnPropertyChanged("IsReadOnly");
      }
    }

    #endregion

    public bool IsActive
    {
      get { return Controller.GetState<bool>(this.GetType().FullName); }
      set
      {
        if (Controller.GetState<bool>(TabViewModel.SuspendSelection)) return;
        if (Controller.SetState<bool>(this.GetType().FullName, value))
        {
          if (IsActiveChanged != null) IsActiveChanged(this, EventArgs.Empty);
        }
      }
    }

    public event EventHandler IsActiveChanged;
  }
}

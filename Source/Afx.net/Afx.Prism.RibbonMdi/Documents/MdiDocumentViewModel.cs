﻿using Afx.Business;
using Afx.Business.Activities;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Documents;
using Afx.Business.Security;
using Afx.Business.Service;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Unity;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Afx.Prism.RibbonMdi.Documents
{
  public abstract class MdiDocumentViewModel<TModel> : MdiDockableDocumentViewModel<TModel>
    where TModel : ContextBase, IDocumentContext
  {
    protected MdiDocumentViewModel(IController controller)
      : base(controller)
    {
    }

    protected override void OnModelChanged()
    {
      if (Model != null) Title = Model.Title;
      else Title = string.Empty;
      //WeakEventManager<IDocumentContext, EventArgs>.AddHandler(Model, "DocumentUpdated", OnDocumentUpdated);
      base.OnModelChanged();
    }

    //private void OnDocumentUpdated(object sender, EventArgs e)
    //{
    //  Validate();
    //}

    public string TitleText
    {
      get { return Model.TitleText; }
    }

    public string AdditionalStateText
    {
      get { return ((IMdiDocumentController)Controller).AdditionalStateText; }
    }

    public const string StateTextProperty = "StateText";
    public string StateText
    {
      get { return Model.Document == null ? string.Empty : string.Format(AdditionalStateText == null ? "{0}" : "{0}", Model.Document.State.DocumentTypeState.Name); }
    }

    public const string StateTimestampTextProperty = "StateTimestampText";
    public string StateTimestampText
    {
      get
      {
        if (Model.Document == null) return string.Empty;
        if (!Model.Document.State.Timestamp.HasValue) return string.Empty;
        return string.Format("{0:dddd, d MMMM yyyy} by {1}", Model.Document.State.Timestamp, Model.Document.State.User.FullName);
      }
    }

    public void Refresh()
    {
      OnPropertyChanged(StateTextProperty);
      OnPropertyChanged(StateTimestampTextProperty);
    }

    protected override IEnumerable<Operation> GetGlobalOperations()
    {
      if (Model.Document == null) return new Collection<Operation>();
      return Model.DocumentType.Operations.Union(Model.Document.State.DocumentTypeState.Operations);
    }

    protected override void OnModelPropertyChanged(string propertyName)
    {
      if (propertyName == "Title")
      {
        Title = Model.Title;
      }

      if (propertyName == "Document")
      {
        WeakEventManager<Document, PropertyChangedEventArgs>.AddHandler(Model.Document as Document, "PropertyChanged", Document_PropertyChanged);
        WeakEventManager<Document, CompositionChangedEventArgs>.AddHandler(Model.Document as Document, "CompositionChanged", Document_CompositionChanged);
        ResetOperations();
        OnPropertyChanged(null);
      }

      base.OnModelPropertyChanged(propertyName);
    }

    private void Document_CompositionChanged(object sender, CompositionChangedEventArgs e)
    {
      OnDocumentCompositionChanged(e.PropertyName);
    }

    private void Document_PropertyChanged(object sender, PropertyChangedEventArgs e)
    {
      OnDocumentPropertyChanged(e.PropertyName);
    }

    protected virtual void OnDocumentPropertyChanged(string propertyName)
    {
      //if (propertyName != ErrorProperty && propertyName != ErrorVisibilityProperty && propertyName != IsValidProperty && propertyName != BusinessObject.HasErrorProperty && propertyName != "Title")
      //{
      //  Validate();
      //}

      //if (propertyName != ErrorProperty)
      //{
      //  this.Error = this.Model.Document.Error;
      //  OnPropertyChanged(ErrorVisibilityProperty);
      //}

      OnModelPropertyChanged(propertyName);
    }

    protected virtual void OnDocumentCompositionChanged(string propertyName)
    {
      if (propertyName != ErrorProperty && propertyName != ErrorVisibilityProperty && propertyName != IsValidProperty && propertyName != BusinessObject.HasErrorProperty && propertyName != "Title")
      {
        Validate();
      }

      if (propertyName != ErrorProperty)
      {
        this.Error = this.Model.Document.Error;
        OnPropertyChanged(ErrorVisibilityProperty);
      }

      OnPropertyChanged("DummyForceErrorProvider");
    }

    public override bool IsReadOnly
    {
      get
      {
        return Model == null ? true : Model.Document == null ? true : Model.Document.IsReadOnly;
      }
    }

    protected override void ResetOperations(ObservableCollection<MdiOperationViewModel> operationViewModels, IEnumerable<Operation> operations)
    {
      OperationGroup group = null;
      foreach (var op in operations.Where((o) => DocumentController.IsOperationVisible(o)))
      {
        if (op is GlobalOperation)
        {
          DocumentTypeGlobalOperation dtop = Model.Document.DocumentType.GetAssociativeObject<DocumentTypeGlobalOperation>(op);
          if (dtop != null)
          {
            if (IsReadOnly && !dtop.ReadOnlyAvailability)
              continue;
          }
        }
        if (group != null && !op.Owner.Equals(group) && operationViewModels.Count > 0 && !(operationViewModels[operationViewModels.Count - 1] is MdiOperationSeperatorViewModel)) operationViewModels.Add(Controller.CreateViewModel<MdiOperationSeperatorViewModel>(this));
        group = op.Owner;
        MdiOperationViewModel vm = Controller.GetCreateViewModel<MdiOperationViewModel>(op, this);
        operationViewModels.Add(vm);
      }

      if (operationViewModels.Count > 0 && operationViewModels[operationViewModels.Count - 1] is MdiOperationSeperatorViewModel)
      {
        operationViewModels.RemoveAt(operationViewModels.Count - 1);
      }

      OnPropertyChanged("ToolbarVisibility");
    }

    //#region string DocumentNumber

    //public string DocumentNumber
    //{
    //  get { return Model == null ? GetDefaultValue<string>() : Model.Document == null ? GetDefaultValue<string>() : Model.Document.DocumentNumber; }
    //  set { Model.Document.DocumentNumber = value; }
    //}

    //public bool IsDocumentNumberReadOnly
    //{
    //  get { return Model == null ? true : Model.Document == null ? true : Model.Document.IsNew ? IsDocumentNumberReadOnlyCore : true; }
    //}

    //protected virtual bool IsDocumentNumberReadOnlyCore
    //{
    //  get { return true; }
    //}

    //public virtual Visibility DocumentNumberVisibility
    //{
    //  get { return Visibility.Visible; }
    //}

    //public virtual string DocumentNumberText
    //{
    //  get { return "Document Number"; }
    //}


    //#endregion

    //#region DateTime DocumentDate

    //public DateTime DocumentDate
    //{
    //  get { return Model == null ? GetDefaultValue<DateTime>() : Model.Document == null ? GetDefaultValue<DateTime>() : Model.Document.DocumentDate; }
    //  set { Model.Document.DocumentDate = value; }
    //}

    //public virtual Visibility DocumentDateVisibility
    //{
    //  get { return Visibility.Visible; }
    //}

    //public virtual string DocumentDateText
    //{
    //  get { return "Document Date"; }
    //}

    //#endregion

    //#region OrganizationalUnit OrganizationalUnit

    //IEnumerable<OrganizationalUnit> mOrganizationalUnits;
    //public IEnumerable<OrganizationalUnit> OrganizationalUnits
    //{
    //  get { return mOrganizationalUnits ?? (mOrganizationalUnits = Cache.Instance.GetObjects<OrganizationalUnit>(ou => ou.UserAssignable, ou => ou.Name, true)); }
    //}

    //public OrganizationalUnit OrganizationalUnit
    //{
    //  get { return Model == null ? GetDefaultValue<OrganizationalUnit>() : Model.Document == null ? GetDefaultValue<OrganizationalUnit>() : Model.Document.OrganizationalUnit; }
    //  set { Model.Document.OrganizationalUnit = value; }
    //}

    //public virtual Visibility OrganizationalUnitVisibility
    //{
    //  get { return Visibility.Visible; }
    //}

    //#endregion

    //#region References

    //IEnumerable<DocumentTypeReference> mDocumentTypeReferences;
    //public IEnumerable<DocumentTypeReference> DocumentTypeReferences
    //{
    //  get { return mDocumentTypeReferences ?? (mDocumentTypeReferences = Model.DocumentType.References.OrderBy(r => r.Name)); }
    //}

    //public IEnumerable<DocumentReference> References
    //{
    //  get { return Model == null ? null : Model.Document == null ? null : Model.Document.References; }
    //}

    //int ReferenceCount
    //{
    //  get { return Model == null ? 0 : Model.Document == null ? 0 : Model.Document.DocumentType.References.Count; }
    //}

    //public Visibility ReferencesVisibility
    //{
    //  get { return ReferenceCount == 0 ? Visibility.Collapsed : ReferencesVisibilityCore; }
    //}

    //public virtual Visibility ReferencesVisibilityCore
    //{
    //  get { return Visibility.Visible; }
    //}

    //#endregion
  }
}

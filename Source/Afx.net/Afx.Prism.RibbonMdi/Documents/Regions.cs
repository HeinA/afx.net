﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Documents
{
  public class Regions
  {
    public const string TabRegion = "Afx.Documents.TabRegion";
    public const string DockRegion = "Afx.Documents.DockRegion";
    public const string HeaderRegion = "Afx.Documents.HeaderRegion";
    public const string FooterRegion = "Afx.Documents.FooterRegion";
  }
}

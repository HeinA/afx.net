﻿using Afx.Business;
using Afx.Prism;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Documents
{
  public interface IRecentDocumentsProvider
  {
    Guid DocumentType { get; }
    IViewModel GetDetailViewModel(IBusinessObject model, IViewModelBase parent);
    Collection<RecentDocumentBase> GetRecentDocuments();
  }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Markup;

namespace Afx.Prism.RibbonMdi.Documents
{
  public class DocumentDataTemplate : DataTemplate
  {
    public DocumentDataTemplate()
    {
      string template = @"
    <DataTemplate xmlns=""http://schemas.microsoft.com/winfx/2006/xaml/presentation"" xmlns:afx=""http://openafx.net/Prism"">  
        <ContentControl Template=""{StaticResource ResourceKey={x:Static afx:MdiResourceKeys.PrintPreview}}"" />
    </DataTemplate>";

      Setter s = new Setter();
      s.TargetName = "Presenter";
      s.Property = ContentPresenter.ContentTemplateProperty;
      s.Value = (DataTemplate)XamlReader.Parse(template);

      DataTrigger dt = new DataTrigger();
      dt.Binding = new Binding("IsPrintPreview");
      dt.Value = true;
      dt.Setters.Add(s);

      this.Triggers.Add(dt);

      if (EditableTemplate != null)
      {
        template = string.Format(@"
    <DataTemplate xmlns=""http://schemas.microsoft.com/winfx/2006/xaml/presentation"" xmlns:afx=""http://openafx.net/Prism"">  
        <ContentControl Template=""{StaticResource ResourceKey={x:Static afx:MdiResourceKeys.Document}}"">
{0}      
        </ContentControl>
    </DataTemplate>", XamlWriter.Save(EditableTemplate));

        s = new Setter();
        s.TargetName = "Presenter";
        s.Property = ContentPresenter.ContentTemplateProperty;
        s.Value = (DataTemplate)XamlReader.Parse(template); ;

        MultiDataTrigger mdt = new MultiDataTrigger();
        mdt.Conditions.Add(new Condition(new Binding("IsPrintPreview"), false));
        mdt.Conditions.Add(new Condition(new Binding("IsReadOnly"), false));
        mdt.Setters.Add(s);

        this.Triggers.Add(dt);
      }

      if (ReadOnlyTemplate != null)
      {
        template = string.Format(@"
    <DataTemplate xmlns=""http://schemas.microsoft.com/winfx/2006/xaml/presentation"" xmlns:afx=""http://openafx.net/Prism"">  
        <ContentControl Template=""{StaticResource ResourceKey={x:Static afx:MdiResourceKeys.Document}}"">
{0}      
        </ContentControl>
    </DataTemplate>", XamlWriter.Save(ReadOnlyTemplate));

        s = new Setter();
        s.TargetName = "Presenter";
        s.Property = ContentPresenter.ContentTemplateProperty;
        s.Value = (DataTemplate)XamlReader.Parse(template); ;

        MultiDataTrigger mdt = new MultiDataTrigger();
        mdt.Conditions.Add(new Condition(new Binding("IsPrintPreview"), false));
        mdt.Conditions.Add(new Condition(new Binding("IsReadOnly"), true));
        mdt.Setters.Add(s);

        this.Triggers.Add(dt);
      }
    }

    //public static readonly DependencyProperty ReadOnlyTemplateProperty = DependencyProperty.Register("ReadOnlyTemplate", typeof(DataTemplate), typeof(DocumentDataTemplate));

    //public DataTemplate ReportViewer
    //{
    //  get { return (DataTemplate)GetValue(ReadOnlyTemplateProperty); }
    //  set { SetValue(ReadOnlyTemplateProperty, value); }
    //}

    #region DataTemplate ReadOnlyTemplate

    DataTemplate mReadOnlyTemplate;
    public DataTemplate ReadOnlyTemplate
    {
      get { return mReadOnlyTemplate; }
      set { mReadOnlyTemplate = value; }
    }

    #endregion

    #region DataTemplate EditableTemplate

    DataTemplate mEditableTemplate;
    public DataTemplate EditableTemplate
    {
      get { return mEditableTemplate; }
      set { mEditableTemplate = value; }
    }

    #endregion
  }
}

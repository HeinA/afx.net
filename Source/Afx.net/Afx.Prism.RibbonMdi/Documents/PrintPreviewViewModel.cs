﻿using Afx.Business.Documents;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Documents
{
  public class PrintPreviewViewModel : TabViewModel<Document>
  {
    #region Constructors

    [InjectionConstructor]
    public PrintPreviewViewModel(IController controller)
      : base(controller)
    {
      Title = "_Print Preview";
    }

    #endregion
  }
}

﻿using Afx.Business;
using Afx.Business.Activities;
using Afx.Business.Documents;
using Microsoft.Practices.Prism.PubSubEvents;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Documents
{
  public interface IMdiDocumentController : IMdiDockableDocumentController
  {
    IEventAggregator EventAggregator { get; }

    void Print(bool preview);

    new IDocumentContext DataContext { get; set; }

    TView ExtendDocument<TExtension, TView>(string regionName)
      where TExtension : class, IExtensionObject, new()
      where TView : class, IExtensionViewModel;

    string AdditionalStateText { get; }
  }
}

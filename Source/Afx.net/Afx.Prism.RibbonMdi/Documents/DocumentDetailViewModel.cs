﻿using Afx.Business.Documents;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Documents
{
  class DocumentDetailViewModel : ViewModel<Document>
  {
    [InjectionConstructor]
    public DocumentDetailViewModel(IController controller)
      : base(controller)
    {
    }

    #region string DocumentNumber

    public string DocumentNumber
    {
      get { return Model == null ? GetDefaultValue<string>() : Model.DocumentNumber; }
      set { Model.DocumentNumber = value; }
    }

    #endregion

    #region DateTime DocumentDate

    public DateTime DocumentDate
    {
      get { return Model == null ? GetDefaultValue<DateTime>() : Model.DocumentDate; }
      set { Model.DocumentDate = value; }
    }

    #endregion

  }
}

﻿using Afx.Business;
using Afx.Business.Data;
using Afx.Business.Documents;
using Afx.Business.Security;
using Afx.Business.Service;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Documents
{
  public class DocumentHeaderViewModel<TModel> : ExtensionViewModel<TModel>
    where TModel : class, IBusinessObject
  {
    #region Constructors

    [InjectionConstructor]
    public DocumentHeaderViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    #region IEnumerable<OrganizationalUnit> OrganizationalUnits

    IEnumerable<OrganizationalUnit> mOrganizationalUnits;
    public IEnumerable<OrganizationalUnit> OrganizationalUnits
    {
      get { return mOrganizationalUnits ?? (mOrganizationalUnits = Cache.Instance.GetObjects<OrganizationalUnit>(ou => ou.UserAssignable, ou => ou.Name, true)); }
    }

    #endregion



    protected override void OnModelPropertyChanged(string propertyName)
    {
      switch (propertyName)
      {
        case Document.DocumentNumberProperty:
          Document d = this.Model as Document;
          if (d == null) break;
          ValidateNewDocumentNumberAsync(d);
          break;
      }

      base.OnModelPropertyChanged(propertyName);
    }

    public async void ValidateNewDocumentNumberAsync(Document doc)
    {
      bool valid = false;
      await Task.Run(() =>
      {
        using (var svc = ServiceFactory.GetService<IAfxService>(SecurityContext.ServerName))
        {
          valid = svc.Instance.ValidateNewDocumentNumber(doc.DocumentNumber, doc.GlobalIdentifier);
        }
      });

      if (valid)
      {
        DocumentNumberError = string.Empty;
      }
      else
      {
        DocumentNumberError = "In use";
        System.Media.SystemSounds.Exclamation.Play();
      }
    }

    #region string DocumentNumberError

    public const string DocumentNumberErrorProperty = "DocumentNumberError";
    string mDocumentNumberError;
    public string DocumentNumberError
    {
      get { return mDocumentNumberError; }
      set { SetProperty<string>(ref mDocumentNumberError, value); }
    }

    #endregion
  }
}

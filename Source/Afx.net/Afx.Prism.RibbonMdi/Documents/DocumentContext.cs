﻿using Afx.Business;
using Afx.Business.Data;
using Afx.Business.Documents;
using Afx.Business.Security;
using Afx.Prism.RibbonMdi.Events;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Afx.Prism.RibbonMdi.Documents
{
  public abstract class DocumentContext<TDocument> : ContextBase, IDocumentContext
    where TDocument : Document, new()
  {
    protected DocumentContext(string documentTypeIdentifier)
    {
      using (new EventStateSuppressor(this))
      {
        DocumentType = Cache.Instance.GetObject<DocumentType>(documentTypeIdentifier);
      }
    }

    public Type ContextType
    {
      get { return typeof(TDocument); }
    }

    protected DocumentContext(string documentTypeIdentifier, Guid documentIdentifier)
      : this(documentTypeIdentifier)
    {
      DocumentIdentifier = documentIdentifier;
    }

    protected virtual TDocument CreateDocument()
    {
      IsNew = true;
      return new TDocument();
    }

    void IDocumentContext.CreateDocument()
    {
      Document = CreateDocument();
    }

    DocumentType mDocumentType;
    public DocumentType DocumentType
    {
      get { return mDocumentType; }
      set { SetProperty<DocumentType>(ref mDocumentType, value); }
    }

    public const string TitleTextProperty = "TitleText";
    public override string TitleText
    {
      get
      {
        if (Document == null) return string.Empty;
        if (string.IsNullOrWhiteSpace(Document.DocumentNumber)) return string.Format("New {0}", DocumentType.Name);
        return string.Format("{0} {1}", DocumentType.Name, Document.DocumentNumber);
      }
    }

    public override string ContextIdentifier
    {
      get
      {
        return string.Format("{0}[{1}]", DocumentType.Name, DocumentIdentifier);
      }
    }

    TDocument mDocument;
    public TDocument Document
    {
      get { return mDocument; }// ?? (Document = CreateDocument()); }
      set
      {
        if (SetProperty<TDocument>(ref mDocument, value))
        {
          if (value == null)
          {
            this.IsDirty = false;
            DocumentIdentifier = Guid.Empty;
          }
          else
          {
            this.IsDirty = value.IsDirty;
            this.IsNew = value.IsNew;
            DocumentIdentifier = value.GlobalIdentifier;
            WeakEventManager<BusinessObject, CompositionChangedEventArgs>.AddHandler(value, "CompositionChanged", OnDataCompositionChanged);
          }
          FireDocumentUpdated();
          OnPropertyChanged(null);
        }
      }
    }

    public event EventHandler<EventArgs> DocumentUpdated;
    void FireDocumentUpdated()
    {
      if (DocumentUpdated != null) DocumentUpdated.Invoke(this, EventArgs.Empty);
    }

    Guid mDocumentIdentifier = Guid.Empty;
    public Guid DocumentIdentifier
    {
      get { return mDocumentIdentifier; }
      set { mDocumentIdentifier = value; }
    }

    public override void LoadData()
    {
      base.LoadData();
    }

    public override void SaveData()
    {
      base.SaveData();
      IsNew = false;
    }

    Document IDocumentContext.Document
    {
      get { return Document; }
      set { Document = (TDocument)value; }
    }

    private void OnDataCompositionChanged(object sender, CompositionChangedEventArgs e)
    {
      IsDirty = true;
      OnPropertyChanged("Title");
    }

    public override bool Validate()
    {
      if (Document == null) return true;
      Document.Validate();
      Error = Document.Error;
      return Document.IsValid;
    }

    protected override void GetErrors(System.Collections.ObjectModel.Collection<string> errors, string propertyName)
    {
      IDataErrorInfo ei = Document as IDataErrorInfo;
      if (ei != null)
      {
        string e = ei[propertyName];
        if (e != null) errors.Add(e);
      }

      base.GetErrors(errors, propertyName);
    }

    public override bool HasError
    {
      get { return Document == null ? base.HasError : base.HasError || Document.HasError; }
    }

    public override bool IsReadOnly
    {
      get
      {
        return Document == null ? true : Document.IsReadOnly;
      }
    }
  }
}

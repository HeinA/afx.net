﻿using Afx.Business;
using Afx.Business.Documents;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Documents
{
  public interface IDocumentContext : IBusinessObject
  {
    Type ContextType { get; }
    DocumentType DocumentType { get; set; }
    Guid DocumentIdentifier { get; set; }
    string ContextIdentifier { get; }
    Document Document { get; set; }
    void CreateDocument();
    //bool IsPrintPreview { get; set; }
    //ReportViewer ReportViewer { get; set; }

    event EventHandler<EventArgs> DocumentUpdated;
  }
}

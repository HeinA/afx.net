﻿using Afx.Business.Documents;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Documents
{
  public class RetryDocumentEdit : RetryAction
  {
    public RetryDocumentEdit(IMdiModuleController module, DocumentType dt, Guid documentIdentifier)
    {
      Module = module;
      DocumentType = dt;
      DocumentIdentifier = documentIdentifier;
    }

    IMdiModuleController mModule;
    public IMdiModuleController Module
    {
      get { return mModule; }
      private set { mModule = value; }
    }

    DocumentType mDocumentType;
    public DocumentType DocumentType
    {
      get { return mDocumentType; }
      private set { mDocumentType = value; }
    }

    Guid mDocumentIdentifier;
    public Guid DocumentIdentifier
    {
      get { return mDocumentIdentifier; }
      private set { mDocumentIdentifier = value; }
    }

    public override void Retry()
    {
      MdiApplicationController.Current.Dispatcher.BeginInvoke(new Action(() =>
      {
        Module.EditDocument(DocumentType, DocumentIdentifier);
      }), System.Windows.Threading.DispatcherPriority.Background);
    }
  }
}

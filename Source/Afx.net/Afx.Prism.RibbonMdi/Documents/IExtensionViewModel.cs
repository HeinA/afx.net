﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Documents
{
  public interface IExtensionViewModel : IViewModel
  {
    new bool IsReadOnly { set; }
  }
}

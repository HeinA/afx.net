﻿using Afx.Business.Documents;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Documents
{
  public class DocumentNotesViewModel : ExtensionViewModel<Document>
  {
    #region Constructors

    [InjectionConstructor]
    public DocumentNotesViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    public override bool IsReadOnly
    {
      get
      {
        return Model.IsReadOnly;
      }
    }

    public string Title
    {
      get { return "_Notes"; }
    }
  }
}

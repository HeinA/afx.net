﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi
{
  public static class ResourceHelper
  {
    public static byte[] GetResource(Assembly assembly, string name)
    {
      using (Stream resFilestream = assembly.GetManifestResourceStream(name))
      {
        return StreamToByteArray(resFilestream);
      }
    }

    public static byte[] StreamToByteArray(Stream stream)
    {
      if (stream == null) return null;
      byte[] ba = new byte[stream.Length];

      int numBytesToRead = (int)stream.Length;
      int numBytesRead = 0;
      while (numBytesToRead > 0)
      {
        int n = stream.Read(ba, numBytesRead, numBytesToRead);
        if (n == 0)
        {
          break;
        }
        numBytesRead += n;
        numBytesToRead -= n;
      }
      return ba;
    }
  }
}

﻿using Afx.Business;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Afx.Prism.RibbonMdi
{
  public interface IMdiDocumentViewModel : INotifyPropertyChanged 
  {
  }

  public interface IMdiDocumentViewModel<TModel> : IMdiDocumentViewModel, IViewModel<TModel>
    where TModel : BusinessObject
  {
  }
}

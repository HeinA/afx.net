﻿using Afx.Business;
using Afx.Business.Activities;
using Afx.Business.Collections;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Mvvm;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Unity;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Xceed.Wpf.AvalonDock.Layout;

namespace Afx.Prism.RibbonMdi
{
  public abstract class MdiDockableDocumentViewModel<TModel> : MdiDockableViewModel<TModel>, IMdiDockableDocumentViewModel, IInternalViewModelBase
    where TModel : class, IBusinessObject
  {
    protected MdiDockableDocumentViewModel(IController controller)
      : base(controller)
    {
    }

    [Dependency]
    public IMdiDockableDocumentController DocumentController { get; set; }
    
    #region DelegateCommand CloseCommand

    DelegateCommand mCloseCommand;
    public DelegateCommand CloseCommand
    {
      get { return mCloseCommand ?? (mCloseCommand = new DelegateCommand(ExecuteClose)); }
    }

    void ExecuteClose()
    {
      Close();
    }

    #endregion

    //protected virtual void Close()
    //{
    //}

    //#region DelegateCommand ActivateCommand

    //DelegateCommand mActivateCommand;
    //public DelegateCommand ActivateCommand
    //{
    //  get { return mActivateCommand ?? (mActivateCommand = new DelegateCommand(ExecuteActivate, CanExecuteActivate)); }
    //}

    //bool CanExecuteActivate()
    //{
    //  return true;
    //}

    //void ExecuteActivate()
    //{
    //}

    //#endregion

    public void Activate()
    {
      IsActive = true;
      IsFocused = true;
    }

    string mContentId = Guid.NewGuid().ToString();
    public string ContentId
    {
      get { return mContentId; }
    }

    public virtual void ResetOperations()
    {
      ResetGlobalOperations();
    }

    ObservableCollection<MdiOperationViewModel> mGlobalOperations;
    public IEnumerable GlobalOperations
    {
      get { return mGlobalOperations; }
    }

    public void ResetGlobalOperations()
    {
      mGlobalOperations = new ObservableCollection<MdiOperationViewModel>();
      ResetOperations(mGlobalOperations, GetGlobalOperations());
      OnPropertyChanged("GlobalOperations");
    }

    protected abstract IEnumerable<Operation> GetGlobalOperations();

    internal virtual void ClearOperations()
    {
      mGlobalOperations = null;
    }

    protected virtual void ResetOperations(ObservableCollection<MdiOperationViewModel> operationViewModels, IEnumerable<Operation> operations)
    {
      OperationGroup group = null;
      foreach (var op in operations.Where((o) => DocumentController.IsOperationVisible(o)))
      {
        if (group != null && !op.Owner.Equals(group) && operationViewModels.Count > 0 && !(operationViewModels[operationViewModels.Count - 1] is MdiOperationSeperatorViewModel)) operationViewModels.Add(Controller.CreateViewModel<MdiOperationSeperatorViewModel>(this));
        group = op.Owner;
        MdiOperationViewModel vm = Controller.GetCreateViewModel<MdiOperationViewModel>(op, this);
        operationViewModels.Add(vm);
      }

      if (operationViewModels.Count > 0 && operationViewModels[operationViewModels.Count - 1] is MdiOperationSeperatorViewModel)
      {
        operationViewModels.RemoveAt(operationViewModels.Count - 1);
      }

      OnPropertyChanged("ToolbarVisibility");
    }

    public virtual Visibility ToolbarVisibility
    {
      get
      {
        if (mGlobalOperations == null || mGlobalOperations.Count == 0) return Visibility.Collapsed;
        return Visibility.Visible;
      }
    }

    protected override void OnLoaded()
    {
      ((IInternalDockableController)Controller).OnLoaded();
      base.OnLoaded();
    }

    protected virtual void Close()
    {
      DocumentController.RequestClose();
    }

    public override bool IsActive
    {
      get { return base.IsActive; }
      set
      {
        base.IsActive = value;
        if (value)
        {
          if (MdiApplicationController.Current.ActiveDocument != this.DocumentController)
          {
            MdiApplicationController.Current.ActiveDocument = this.DocumentController;
          }
        }
      }
    }
  }
}

﻿using Afx.Business;
using Afx.Business.Activities;
using Afx.Prism.RibbonMdi.Documents;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi
{
  public interface IMdiDockableDocumentController : IViewController
  {
    void ExecuteOperation(Operation op);
    void ExecuteOperation(Operation op, BusinessObject argument);
    bool IsOperationVisible(Operation op);

    event EventHandler<EventArgs> CacheUpdated;

    IMdiApplicationController ApplicationController { get; }
    bool IsValid { get; }
    void Activate();
    bool IsDirty { get; }
    bool IsReadOnly { get; }
    void RequestClose();
    void SaveDataContext();
    string Title { get; }
    void Refresh(bool force);
  }
}

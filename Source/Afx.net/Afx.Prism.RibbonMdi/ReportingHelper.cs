﻿using Afx.Business;
using Afx.Business.Security;
using Afx.Business.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi
{
  public static class ReportingHelper
  {
    static ReportCredentials mReportCredentials;
    public static ReportCredentials ReportCredentials
    {
      get
      {
        if (mReportCredentials == null)
        {
          using (var svc = ServiceFactory.GetService<IAfxService>(SecurityContext.ServerName))
          {
            mReportCredentials = svc.Instance.GetReportCredentials();
          }
        }
        return mReportCredentials;
      }
    }
  }
}

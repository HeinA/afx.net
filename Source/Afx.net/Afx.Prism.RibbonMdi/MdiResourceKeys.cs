﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi
{
  public static class MdiResourceKeys
  {
    public const string MessageDialog = "RibbonMdi.MessageDialog";
    public const string OkCancelDialog = "RibbonMdi.OkCancelDialog";
    public const string OkCancelDialogNoDefaultButton = "RibbonMdi.OkCancelDialogNoDefaultButton";
    public const string YesNoDialog = "RibbonMdi.YesNoDialog";

    public const string Document = "RibbonMdi.Document";
    public const string PrintPreview = "RibbonMdi.PrintPreview";
  }
}

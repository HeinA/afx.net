﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi
{
  public abstract class ExcelReportInfo<T> : IExcelReportInfo
    where T : Afx.Prism.Controller, IMdiExcelReportParameterController
  {
    public abstract string GroupName { get; }
    public abstract string ReportName { get; }
    public abstract string ReportDescription { get; }

    public void SetParameters()
    {
      var c = MdiApplicationController.Current.CreateChildController<T>();
      try
      {
        c.Title = ReportName;
        c.Description = ReportDescription;

        c.Run();
      }
      catch
      {
        c.Terminate();
      }
    }
  }
}

﻿using Afx.Business.Documents;
using CrystalDecisions.CrystalReports.Engine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi
{
  public interface ICrystalPrintDocumentInfo
  {
    byte[] GetReport();
    Type DocumentType { get; }
    string Key { get; }
    void SetParameters(ReportDocument rpt, Document doc, params object[] args);
  }

  public interface ICrystalPrintDocumentInfo<T> : ICrystalPrintDocumentInfo
    where T : Document
  {
    void SetParameters(ReportDocument rpt, T doc, params object[] args);
  }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi
{
  [Export(typeof(Afx.Prism.ResourceInfo))]
  public class MdiResourceInfo : ResourceInfo
  {
    public override Uri GetUri()
    {
      return new Uri("pack://application:,,,/Afx.Prism.RibbonMdi;component/Resources.xaml", UriKind.Absolute);
    }
  }
}

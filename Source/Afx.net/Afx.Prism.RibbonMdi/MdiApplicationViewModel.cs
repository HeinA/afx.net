﻿using Afx.Business;
using Afx.Business.Activities;
using Afx.Business.Data;
using Afx.Prism.RibbonMdi.Ribbon;
using Microsoft.Practices.ObjectBuilder2;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Unity;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace Afx.Prism.RibbonMdi
{
  public class MdiApplicationViewModel : ViewModel<MdiApplication>, IMdiApplicationViewModel
  {
    [InjectionConstructor]
    public MdiApplicationViewModel(MdiApplication context, IController controller)
      : base(context, controller)
    {
    }

    public const string TitleProperty = "Title";
    public string Title
    {
      get { return Model.Title; }
    }

    public const string IsModalProperty = "IsModal";
    public bool IsModal
    {
      get { return Model.IsModal; }
    }

    public const string IsRibbonEnabledProperty = "IsRibbonEnabled";
    public bool IsRibbonEnabled
    {
      get { return Model.IsRibbonEnabled; }
    }

    #region IEnumerable<IRibbonTab> RibbonTabs

    public IEnumerable<IRibbonTab> RibbonTabs
    {
      get { return MdiApplicationController.Current.RibbonTabs; }
    }

    #endregion

    void MdiApplicationViewModel_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
    {
    }

    protected override void OnModelChanged()
    {
      base.OnModelChanged();
    }

    private void RibbonTabs_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
    {
      OnPropertyChanged("RibbonTabs");
    }

    public IEnumerable<FileMenuItemViewModel> FileMenuItems
    {
      get { return Model.FileMenuItems; }
    }

    public IEnumerable<IMdiDockableDocumentViewModel> DocumentViewModels
    {
      get { return Model.DocumentViewModels; }
    }

    public IEnumerable<IMdiDockableToolViewModel> ToolViewModels
    {
      get { return Model.ToolViewModels; }
    }

    #region DelegateCommand ClosingCommand

    DelegateCommand<CancelEventArgs> mClosingCommand;
    public DelegateCommand<CancelEventArgs> ClosingCommand
    {
      get
      {
        if (mClosingCommand == null) mClosingCommand = new DelegateCommand<CancelEventArgs>(ExecuteClosing);
        return mClosingCommand;
      }
    }

    void ExecuteClosing(CancelEventArgs args)
    {
      MdiApplicationController.Current.Shutdown();
      args.Cancel = true;
    }

    #endregion

    #region DelegateCommand SearchCommand

    DelegateCommand mSearchCommand;
    public DelegateCommand SearchCommand
    {
      get { return mSearchCommand ?? (mSearchCommand = new DelegateCommand(ExecuteSearch, CanExecuteSearch)); }
    }

    bool CanExecuteSearch()
    {
      return true;
    }

    void ExecuteSearch()
    {
      MdiApplicationController.Current.ExecuteFind();
    }

    #endregion

    #region DelegateCommand TabCommand

    DelegateCommand mTabCommand;
    public DelegateCommand TabCommand
    {
      get { return mTabCommand ?? (mTabCommand = new DelegateCommand(ExecuteTab, CanExecuteTab)); }
    }

    bool CanExecuteTab()
    {
      return true;
    }

    void ExecuteTab()
    {
    }

    #endregion

    IMdiDockableDocumentViewModel mActiveDocument;
    public IMdiDockableDocumentViewModel ActiveDocument
    {
      get { return mActiveDocument; }
      set { SetProperty<IMdiDockableDocumentViewModel>(ref mActiveDocument, value); }
    }
  }
}

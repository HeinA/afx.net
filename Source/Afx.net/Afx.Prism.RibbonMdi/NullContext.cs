﻿using Afx.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi
{
  public class NullContext : BusinessObject
  {
    public NullContext(Guid identifier)
    {
      GlobalIdentifier = identifier;
    }
  }
}

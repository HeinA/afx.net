﻿using Microsoft.Practices.Prism.Regions.Behaviors;
using Microsoft.Practices.ServiceLocation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;

namespace Afx.Prism.RibbonMdi
{
  /// <summary>
  /// This is a hack to get regions in data templates on Avalondock LayoutElements working.
  /// </summary>
  public class MdiRegionManager
  {
    public static readonly DependencyProperty RegionNameProperty = DependencyProperty.RegisterAttached(
        "RegionName",
        typeof(string),
        typeof(MdiRegionManager),
        new PropertyMetadata(OnSetRegionNameCallback));

    public static void SetRegionName(DependencyObject regionTarget, string regionName)
    {
      if (regionTarget == null) throw new ArgumentNullException("regionTarget");
      regionTarget.SetValue(Microsoft.Practices.Prism.Regions.RegionManager.RegionNameProperty, regionName);
    }

    public static string GetRegionName(DependencyObject regionTarget)
    {
      if (regionTarget == null) throw new ArgumentNullException("regionTarget");
      return regionTarget.GetValue(Microsoft.Practices.Prism.Regions.RegionManager.RegionNameProperty) as string;
    }

    private static bool IsInDesignMode(DependencyObject element)
    {
      return DesignerProperties.GetIsInDesignMode(element);
    }

    private static void OnSetRegionNameCallback(DependencyObject element, DependencyPropertyChangedEventArgs args)
    {
      if (!IsInDesignMode(element))
      {
        if (GetRegionName(element) != (string)args.NewValue && args.NewValue != null)
        {
          SetRegionName(element, (string)args.NewValue);
        }
      }
    }
  }
}

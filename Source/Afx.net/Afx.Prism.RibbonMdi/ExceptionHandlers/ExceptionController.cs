﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.ExceptionHandlers
{
  public class ExceptionController : MdiDialogController<ExceptionContext, ExceptionViewModel>
  {
    public ExceptionController(IController controller)
      : base(controller)
    {
    }

    protected override bool OnRun()
    {
      ViewModel.Caption = "Error";
      return base.OnRun();
    }
  }
}

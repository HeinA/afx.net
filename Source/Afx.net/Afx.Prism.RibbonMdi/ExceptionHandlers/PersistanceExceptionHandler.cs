﻿using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.Configuration;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.ExceptionHandlers
{
  [ConfigurationElementType(typeof(CustomHandlerData))]
  public class PersistanceExceptionHandler : IExceptionHandler
  {
    public PersistanceExceptionHandler(NameValueCollection values)
    {
    }

    #region IExceptionHandler Members

    public Exception HandleException(Exception exception, Guid handlingInstanceId)
    {
      return null;
    }

    #endregion
  }
}

﻿using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Afx.Prism.RibbonMdi.ExceptionHandlers
{
  public class ExceptionViewModel : MdiDialogViewModel<ExceptionContext>
  {
    [InjectionConstructor]
    public ExceptionViewModel(IController controller)
      : base(controller)
    {
    }

    #region Model Propertry string Message

    public string Message
    {
      get
      {
        if (Model == null) return default(string);
        return Model.Message;
      }
    }

    #endregion

    #region DelegateCommand CopyErrorDetailsCommand

    DelegateCommand mCopyErrorDetailsCommand;
    public DelegateCommand CopyErrorDetailsCommand
    {
      get { return mCopyErrorDetailsCommand ?? (mCopyErrorDetailsCommand = new DelegateCommand(ExecuteCopyErrorDetails)); }
    }

    void ExecuteCopyErrorDetails()
    {
      Clipboard.SetText(Model.Exception.ToString());
    }

    #endregion

  }
}

﻿using Afx.Business;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.Configuration;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.ExceptionHandlers
{
  [ConfigurationElementType(typeof(CustomHandlerData))]
  public class ExceptionHandler : IExceptionHandler
  {
    public ExceptionHandler(NameValueCollection values)
    {
    }

    #region IExceptionHandler Members

    public Exception HandleException(Exception exception, Guid handlingInstanceId)
    {
      if (MdiApplicationController.Current == null)
      {
        using (StreamWriter sw = new StreamWriter("exceptions.txt", false))
        {
          TypeLoadException tle = exception as TypeLoadException;
          if (tle != null)
          {
            sw.WriteLine(tle.TypeName);
            sw.WriteLine("*****************************************************");
          }

          ReflectionTypeLoadException rtle = exception as ReflectionTypeLoadException;
          if (rtle != null)
          {
            if (rtle.LoaderExceptions != null)
            {
              foreach (var le in rtle.LoaderExceptions)
              {
                sw.WriteLine(le.ToString());
              }
            }

            //if (rtle.Types != null)
            //{
            //  foreach (var t in rtle.Types)
            //  {
            //    sw.WriteLine(t.ToString()); //TypeHelper.GetTypeName(t));
            //  }
            //}
            sw.WriteLine("*****************************************************");
          }

          sw.WriteLine(exception.ToString());
        }
      }
      else
      {
        MdiApplicationController.Current.Dispatcher.BeginInvoke(new Action(() =>
        {
          try
          {
            ExceptionController ec = MdiApplicationController.Current.CreateChildController<ExceptionController>();
            ec.DataContext = new ExceptionContext(exception);
            ec.Run();
          }
          catch
          {
#if DEBUG
          if (Debugger.IsAttached) Debugger.Break();
#endif
          }
        }), System.Windows.Threading.DispatcherPriority.Normal);
      }
      return null;
    }

    #endregion
  }
}

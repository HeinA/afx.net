﻿using Afx.Business.Data;
using Afx.Business.Service;
using Afx.Business.ServiceModel;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.Configuration;
using Microsoft.Practices.Prism.Regions;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.ExceptionHandlers
{
  [ConfigurationElementType(typeof(CustomHandlerData))]
  public class MessageExceptionHandler : IExceptionHandler
  {
    public MessageExceptionHandler(NameValueCollection values)
    {
    }

    #region IExceptionHandler Members

    public Exception HandleException(Exception exception, Guid handlingInstanceId)
    {
      try
      {
        MessageFaultController fc = MdiApplicationController.Current.CreateChildController<MessageFaultController>();
        if (exception is FaultException<MessageFault>) fc.DataContext = ((FaultException<MessageFault>)exception).Detail;
        else
        {
          fc.DataContext = new MessageFault();
          fc.DataContext.Message = exception.Message;
        }
        fc.Run();

        return null;
      }
      catch
      {
#if DEBUG
        if (Debugger.IsAttached) Debugger.Break();
#endif
        throw;
      }
    }

    #endregion
  }
}

﻿using Afx.Business.Service;
using Afx.Business.ServiceModel;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.ExceptionHandlers
{
  public class MessageFaultViewModel : MdiDialogViewModel<MessageFault>
  {
    [InjectionConstructor]
    public MessageFaultViewModel(IController controller)
      : base(controller)
    {
    }

    #region Model Propertry string Message

    public string Message
    {
      get
      {
        if (Model == null) return default(string);
        return Model.Message;
      }
    }

    #endregion
  }
}

﻿using Afx.Business.Service;
using Afx.Business.ServiceModel;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.ExceptionHandlers
{
  public class MessageFaultController : MdiDialogController<MessageFault, MessageFaultViewModel>
  {
    public MessageFaultController(IController controller)
      : base(controller)
    {
    }

    protected override bool OnRun()
    {
      ViewModel.Caption = "Error";
      return base.OnRun();
    }
  }
}

﻿using Afx.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.ExceptionHandlers
{
  public class ExceptionContext : BusinessObject
  {
    public ExceptionContext(Exception exception)
    {
      Exception = exception;
    }

    Exception mException;
    public Exception Exception
    {
      get { return mException; }
      private set { mException = value; }
    }

    public string Message
    {
      get { return "An unexpected error has occured."; }
    }
  }
}

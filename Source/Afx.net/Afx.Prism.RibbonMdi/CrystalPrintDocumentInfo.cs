﻿using Afx.Business.Documents;
using CrystalDecisions.CrystalReports.Engine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi
{
  public abstract class CrystalPrintDocumentInfo<T> : ICrystalPrintDocumentInfo<T>
    where T : Document
  {
    public abstract byte[] GetReport();

    public Type DocumentType
    {
      get { return typeof(T); }
    }

    public virtual string Key
    {
      get { return null; }
    }

    void ICrystalPrintDocumentInfo.SetParameters(ReportDocument rpt, Document doc, params object[] args)
    {
      SetParameters(rpt, (T)doc, args);
    }

    public virtual void SetParameters(ReportDocument rpt, T doc, params object[] args)
    {
      rpt.SetParameterValue("DocumentId", doc.Id);
    }
  }
}

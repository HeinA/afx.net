﻿using Afx.Business;
using Afx.Business.Collections;
using Afx.Business.Security;
using Afx.Prism.Events;
using Microsoft.Practices.Prism.Mvvm;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Xceed.Wpf.AvalonDock.Layout;

namespace Afx.Prism.RibbonMdi
{
  public abstract class MdiDockableToolViewModel<TModel> : MdiDockableViewModel<TModel>, IMdiDockableToolViewModel, IInternalViewModelBase
    where TModel : class, IBusinessObject

  {
    protected MdiDockableToolViewModel(IController controller)
      : base(controller)
    {
      MdiApplicationController.Current.EventAggregator.GetEvent<UserAuthenticatedEvent>().Subscribe(OnUserAuthenticated);
    }

    private void OnUserAuthenticated(EventArgs obj)
    {
      if (SecurityContext.User == null)
      {
        IsVisible = false;
      }
    }

    bool mIsVisible = true;
    public bool IsVisible
    {
      get { return mIsVisible; }
      set { SetProperty<bool>(ref mIsVisible, value); }
    }

    public abstract string ContentId
    {
      get;
    }

    public void Activate()
    {
      IsVisible = true;
      IsActive = true;
      IsFocused = true;
    }
  }
}

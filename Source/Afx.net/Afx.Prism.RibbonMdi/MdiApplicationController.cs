﻿using Afx.Business;
using Afx.Business.Activities;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Documents;
using Afx.Business.Security;
using Afx.Prism.Events;
using Afx.Prism.RibbonMdi.Activities;
using Afx.Prism.RibbonMdi.Events;
using Afx.Prism.RibbonMdi.Login;
using Afx.Prism.RibbonMdi.Ribbon;
using Afx.Prism.RibbonMdi.Tools;
using Microsoft.Practices.Prism.PubSubEvents;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;

namespace Afx.Prism.RibbonMdi
{
  public class MdiApplicationController : ApplicationController, IMdiApplicationController
  {
    public MdiApplicationController(IUnityContainer container, MdiApplication applicationContext)
      : base("Mdi Application Controller", container)
    {
      mApplicationContext = applicationContext;
      Container.RegisterInstance<MdiApplication>(mApplicationContext);
      Container.RegisterInstance<IMdiApplicationController>(this);
      mCurrent = this;

      this.EventAggregator.GetEvent<UserAuthenticatedEvent>().Subscribe(OnUserAuthenticated);
      //this.EventAggregator.GetEvent<CacheUpdatedEvent>().Subscribe(OnCacheUpdated);
    }

    //private void OnCacheUpdated(EventArgs obj)
    //{
    //  Setting.Refresh(true);
    //  SettingsMenuItem.Reset();
    //}

    NewMenuItemViewModel mNewMenuItem;
    public NewMenuItemViewModel NewMenuItem
    {
      get { return mNewMenuItem ?? (mNewMenuItem = CreateViewModel<NewMenuItemViewModel>(ApplicationViewModel)); }
    }

    RecentMenuItemViewModel mRecentMenuItem;
    public RecentMenuItemViewModel RecentMenuItem
    {
      get { return mRecentMenuItem ?? (mRecentMenuItem = CreateViewModel<RecentMenuItemViewModel>(ApplicationViewModel)); }
    }

    SettingGroupMenuItemViewModel mSettingsMenuItem;
    public SettingGroupMenuItemViewModel SettingsMenuItem
    {
      get { return mSettingsMenuItem ?? (mSettingsMenuItem = CreateViewModel<SettingGroupMenuItemViewModel>(ApplicationViewModel)); }
    }

    #region IMdiDockableDocumentController ActiveDocument

    public const string ActiveDocumentProperty = "ActiveDocument";
    IMdiDockableDocumentController mActiveDocument;
    public IMdiDockableDocumentController ActiveDocument
    {
      get { return mActiveDocument; }
      set { mActiveDocument = value; }
    }

    #endregion

    private void OnUserAuthenticated(EventArgs obj)
    {
      if (SecurityContext.User != null)
      {
        foreach (Type t in ExtensibilityManager.GetExportedTypes<SearchItemViewModel>())
        {
          RegisterSearchViewModel(t);
        }

        ComposeRibbonTabs();

        ApplicationContext.FileMenuItems.Insert(0, NewMenuItem);
        ApplicationContext.FileMenuItems.Insert(1, RecentMenuItem);
        ApplicationContext.FileMenuItems.Insert(2, SettingsMenuItem);


        string oldTitle = ApplicationContext.Title;
        ApplicationContext.Title = String.Format("{0}\t{2}, {3} (OU: {1})", "IFMS", SecurityContext.OrganizationalUnit.Name, SecurityContext.User.Lastname, SecurityContext.User.Firstnames);

        //((MdiApplicationView)ApplicationView).LoadLayout(null);
      }
      else
      {
        mSearchViewModels.Clear();

        ClearRibbonTabs();

        ApplicationContext.FileMenuItems.Remove(NewMenuItem);
        ApplicationContext.FileMenuItems.Remove(RecentMenuItem);
        ApplicationContext.FileMenuItems.Remove(SettingsMenuItem);
      }
    }

    public MdiApplicationController(string id, IUnityContainer container, MdiApplication applicationContext)
      : base(id, container)
    {
      mApplicationContext = applicationContext;
      Container.RegisterInstance<MdiApplication>(mApplicationContext);
      Container.RegisterInstance<IMdiApplicationController>(this);
      mCurrent = this;
    }

    static MdiApplicationController mCurrent;
    public static new MdiApplicationController Current
    {
      get { return MdiApplicationController.mCurrent; }
    }

    public new IUnityContainer Container
    {
      get { return base.Container; }
    }

    MdiApplication mApplicationContext;
    public MdiApplication ApplicationContext
    {
      get { return mApplicationContext; }
    }

    MdiApplicationViewModel mApplicationViewModel;
    public MdiApplicationViewModel ApplicationViewModel
    {
      get { return mApplicationViewModel ?? (mApplicationViewModel = (MdiApplicationViewModel)GetInstance<IMdiApplicationViewModel>()); }
    }

    Collection<IMdiDialogController> mDialogs = new Collection<IMdiDialogController>();
    Collection<IMdiDialogController> Dialogs
    {
      get { return mDialogs; }
    }

    protected override void OnRunning()
    {
      ApplicationContext.FileMenuItems.Add(CreateViewModel<AboutMenuItemViewModel>(ApplicationViewModel));
      ApplicationContext.FileMenuItems.Add(CreateViewModel<LoginOutMenuItemViewModel>(ApplicationViewModel));
      ApplicationContext.FileMenuItems.Add(CreateViewModel<ExitMenuItemViewModel>(ApplicationViewModel));
      base.OnRunning();
    }

    public virtual void AddDialog(IMdiDialogController controller)
    {
      Dialogs.Add(controller);
      SetUiState();
    }

    public virtual void RemoveDialog(IMdiDialogController controller)
    {
      Dialogs.Remove(controller);
      SetUiState();
    }

    protected virtual void SetUiState()
    {
      ApplicationContext.IsModal = Dialogs.Count != 0;
      ApplicationContext.IsRibbonEnabled = Dialogs.Count == 0;
      if (Dialogs.Count != 0) EventAggregator.GetEvent<DialogDisplayedEvent>().Publish(EventArgs.Empty);
    }

    BasicCollection<SearchItemViewModel> mSearchViewModels;
    public IEnumerable<SearchItemViewModel> SearchViewModels
    {
      get { return mSearchViewModels ?? (mSearchViewModels = new BasicCollection<SearchItemViewModel>()); }
    }

    public T RegisterSearchViewModel<T>()
      where T : SearchItemViewModel
    {
      T vm = CreateViewModel<T>(ApplicationViewModel);
      if (!vm.DocumentType.Enabled) return null;
      if (mSearchViewModels == null) mSearchViewModels = new BasicCollection<SearchItemViewModel>();
      mSearchViewModels.Add(vm);
      return vm;
    }

    public void RegisterSearchViewModel(Type viewType)
    {
      SearchItemViewModel vm = (SearchItemViewModel)CreateViewModel(viewType, ApplicationViewModel);
      if (vm.DocumentType == null) return;
      if (!vm.DocumentType.Enabled) return;
      if (mSearchViewModels == null) mSearchViewModels = new BasicCollection<SearchItemViewModel>();
      mSearchViewModels.Add(vm);
    }

    public void ExecuteActivity(Activity activity)
    {
      if (activity == null) throw new ArgumentNullException("Activity may not be null.");
      EventAggregator.GetEvent<ExecuteActivityEvent>().Publish(new ExecuteActivityEventArgs((Activity)activity.Clone()));
    }

    public void ExecuteActivity(Activity activity, Guid contextId)
    {
      if (activity == null) throw new ArgumentNullException("Activity may not be null.");
      EventAggregator.GetEvent<ExecuteActivityEvent>().Publish(new ExecuteActivityEventArgs((Activity)activity.Clone(), contextId));
    }

    public void ExecuteFind()
    {
      EventAggregator.GetEvent<ExecuteSearchEvent>().Publish(EventArgs.Empty);
    }

    public void CloseDocuments(bool shutDown = false, bool logout = false, bool force = false)
    {
      Collection<IMdiDockableDocumentController> controllers = FindControllers<IMdiDockableDocumentController>();
      foreach (IMdiDockableDocumentController c in controllers)
      {
        if (force || !c.IsDirty) c.Terminate();
      }

      controllers = FindControllers<IMdiDockableDocumentController>();

      if (shutDown)
      {
        if (controllers.Count == 0) Application.Current.Shutdown();
        else
        {
          UnsavedDocumentContext udc = new UnsavedDocumentContext(controllers);
          udc.Shutdown = true;
          SaveMultipleController mdc = GetCreateChildController<SaveMultipleController>(SaveMultipleController.ControllerKey);
          if (mdc.State == ControllerState.Created)
          {
            mdc.DataContext = udc;
            mdc.Run();
          }
        }
      }

      if (logout)
      {
        if (controllers.Count == 0)
        {
          SecurityContext.Logout();
          EventAggregator.GetEvent<UserAuthenticatedEvent>().Publish(EventArgs.Empty);
        }
        else
        {
          UnsavedDocumentContext udc = new UnsavedDocumentContext(controllers);
          udc.Logout = true;
          SaveMultipleController mdc = GetCreateChildController<SaveMultipleController>(SaveMultipleController.ControllerKey);
          if (mdc.State == ControllerState.Created)
          {
            mdc.DataContext = udc;
            mdc.Run();
          }
        }
      }
      ApplicationContext.Title = "IFMS";
    }

    public IMdiDockableDocumentController EditDocument(Document doc)
    {
      EventAggregator.GetEvent<EditDocumentEvent>().Publish(new EditDocumentEventArgs(doc));
      return ActiveDocument;
    }

    public IMdiDockableDocumentController EditDocument(DocumentType documentType)
    {
      EventAggregator.GetEvent<EditDocumentEvent>().Publish(new EditDocumentEventArgs(documentType));
      return ActiveDocument;
    }

    public IMdiDockableDocumentController EditDocument(string documentTypeIdentifier)
    {
      EditDocument(Cache.Instance.GetObject<DocumentType>(documentTypeIdentifier));
      return ActiveDocument;
    }

    public IMdiDockableDocumentController EditDocument(DocumentType documentType, Guid globalIdentifier)
    {
      EventAggregator.GetEvent<EditDocumentEvent>().Publish(new EditDocumentEventArgs(documentType, globalIdentifier));
      return ActiveDocument;
    }

    public IMdiDockableDocumentController EditDocument(string documentTypeIdentifier, Guid globalIdentifier)
    {
      EditDocument(Cache.Instance.GetObject<DocumentType>(documentTypeIdentifier), globalIdentifier);
      return ActiveDocument;
    }

    public void Logout()
    {
      CloseDocuments(logout: true);
    }

    public void ShowLoginDialog()
    {
      CreateChildController<LoginController>().Run();
    }

    public void Shutdown()
    {
      CloseDocuments(shutDown: true);
    }

    protected override bool OnRun()
    {
      return base.OnRun();
    }

    Setting mSelectedSetting;
    internal void ShowSettingDetailView(Setting setting)
    {
      if (mSelectedSetting == setting) return;
      mSelectedSetting = setting;
      IRegion settingsRegion = RegionManager.Regions[MdiApplication.SettingRegion];
      Collection<object> views = new Collection<object>(settingsRegion.Views.ToList());
      foreach (object v in views)
      {
        settingsRegion.Remove(v);
      }

      if (mSelectedSetting == null) return;

      Type t = typeof(MdiSettingViewModel<>);
      t = t.MakeGenericType(setting.GetType());

      Type t1 = ExtensibilityManager.GetExportedTypes(t).FirstOrDefault();
      IViewModel vm = GetCreateViewModel(t1, setting, ApplicationViewModel);

      if (vm != null) settingsRegion.Add(vm);
    }




    #region IEnumerable<IRibbonTab> RibbonTabs

    ObservableCollection<IRibbonTab> mRibbonTabs = new ObservableCollection<IRibbonTab>();
    public IEnumerable<IRibbonTab> RibbonTabs
    {
      get { return mRibbonTabs; }
    }

    #endregion

    #region IRibbonTab GetRibbonTab(...)

    public IRibbonTab GetRibbonTab(string name)
    {
      return RibbonTabs.FirstOrDefault(rt => rt.Name == name);
    }

    #endregion

    #region void ComposeRibbonTabs(...)

    internal void ClearRibbonTabs()
    {
      mRibbonTabs.Clear();
    }

    internal void ComposeRibbonTabs()
    {
      IRibbonTab rtHome = Container.Resolve<HomeTab>();
      Container.RegisterInstance<IRibbonTab>(rtHome.Name, rtHome);

      IRibbonTab rtReporting = Container.Resolve<ReportingTab>();
      Container.RegisterInstance<IRibbonTab>(rtReporting.Name, rtReporting);

      IRibbonTab rtAdministration = Container.Resolve<AdministrationTab>();
      Container.RegisterInstance<IRibbonTab>(rtAdministration.Name, rtAdministration);

      //Add the Home tab first
      mRibbonTabs.Add(rtHome);

      //Get RibbonTabTypes for each module in module load order
      IEnumerable<Type> moduleTabTypes = ExtensibilityManager.GetExportedTypes(typeof(IRibbonTab));
      foreach (Controller mc in this.Controllers)
      {
        Collection<IRibbonTab> moduleTabs = new Collection<IRibbonTab>();
        foreach (Type t in moduleTabTypes.Where(t1 => t1.Assembly.Equals(mc.GetType().Assembly)))
        {
          IRibbonTab rt = (IRibbonTab)Container.Resolve(t);
          Container.RegisterInstance<IRibbonTab>(rt.Name, rt);
          moduleTabs.Add(rt);
        }

        //Add each module's tabs in their indexed order
        foreach (IRibbonTab rt1 in moduleTabs.OrderBy(rt1 => rt1.Index))
        {
          mRibbonTabs.Add(rt1);
        }
      }

      //Add the Administration tab last
      mRibbonTabs.Add(rtReporting);

      //Add the Administration tab last
      mRibbonTabs.Add(rtAdministration);

      //Add the ribbon tab groups
      IEnumerable<Type> groupTypes = ExtensibilityManager.GetExportedTypes(typeof(IRibbonGroup));
      foreach (Controller mc in this.Controllers)
      {
        AddRibbonGroups(groupTypes.Where(t1 => t1.Assembly.Equals(mc.GetType().Assembly)), false);
      }

      foreach (Controller mc in this.Controllers)
      {
        AddRibbonGroups(groupTypes.Where(t1 => t1.Assembly.Equals(mc.GetType().Assembly)), true);
      }

      //Add the ribbon items
      IEnumerable<Type> itemTypes = ExtensibilityManager.GetExportedTypes(typeof(IRibbonItem));
      foreach (Controller mc in this.Controllers)
      {
        AddRibbonItems(itemTypes.Where(t1 => t1.Assembly.Equals(mc.GetType().Assembly)));
      }
    }

    void AddRibbonGroups(IEnumerable<Type> moduleGroupTypes, bool placeBehind)
    {
      Collection<IRibbonGroup> tabGroups = new Collection<IRibbonGroup>();
      foreach (Type t in moduleGroupTypes)
      {
        IRibbonGroup rg = (IRibbonGroup)Container.Resolve(t);
        //IRibbonTab ribbonTab = GetRibbonTab(rg.TabName);
        tabGroups.Add(rg);
      }

      //Add each module's groups to their tabs in their indexed order
      foreach (IRibbonGroup rg in tabGroups.Where(rg1 => rg1.PlaceBehind == placeBehind).OrderBy(rg1 => rg1.Index))
      {
        IRibbonTab ribbonTab = GetRibbonTab(rg.TabName);
        if (!ribbonTab.Groups.Any(rt => rt.Name == rg.Name))
        {
          string key = string.Format(CultureInfo.InvariantCulture, "{0}.{1}", rg.TabName, rg.Name);
          Container.RegisterInstance<IRibbonGroup>(key, rg);
          ribbonTab.AddGroup(rg);
        }
      }
    }

    void AddRibbonItems(IEnumerable<Type> itemTypes)
    {
      Collection<IRibbonItem> groupsItems = new Collection<IRibbonItem>();
      foreach (Type t in itemTypes)
      {
        Controller mc = (Controller)Controllers.FirstOrDefault(c => c.GetType().Assembly.Equals(t.Assembly));
        IRibbonItem ri = (IRibbonItem)mc.Container.Resolve(t);
        mc.Container.RegisterInstance<IRibbonItem>(ri.ItemIdentifier, ri);
        groupsItems.Add(ri);
      }

      //Add each module's items to their groups in their indexed order
      foreach (IRibbonItem ri in groupsItems.OrderBy(ri1 => ri1.Index))
      {
        IRibbonTab ribbonTab = GetRibbonTab(ri.TabName);
        IRibbonGroup ribbonGroup = ribbonTab.GetGroup(ri.GroupName);
        RibbonItemViewModel rivm = ri as RibbonItemViewModel;
        if (rivm != null) rivm.RibbonGroup = ribbonGroup as RibbonGroup;
        try
        {
          ribbonGroup.AddItem(ri);
        }
        catch
        {
          throw;
        }
      }
    }

    #endregion
  }
}

﻿using Afx.Business.Settings;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Settings
{
  [Export(typeof(MdiSettingViewModel<ReportingServerSetting>))]
  public class ReportingServerSettingViewModel : MdiSettingViewModel<ReportingServerSetting>
  {
    #region Constructors

    [InjectionConstructor]
    public ReportingServerSettingViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion
  }
}

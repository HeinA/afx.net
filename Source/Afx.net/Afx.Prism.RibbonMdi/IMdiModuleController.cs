﻿using Afx.Business.Activities;
using Afx.Business.Documents;
using Microsoft.Practices.Prism.Modularity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi
{
  public interface IMdiModuleController : IController, IModule
  {
    IMdiApplicationController ApplicationController { get; }
    void ExecuteActivity(Activity activity, Guid ContextId);
    void EditDocument(DocumentType dt, Guid documentIdentifier);
  }
}

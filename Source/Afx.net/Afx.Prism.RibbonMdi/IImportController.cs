﻿using Afx.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi
{
  public interface IImportController
  {
    void DoImport(ExternalSystem system, string Identifier);
  }
}

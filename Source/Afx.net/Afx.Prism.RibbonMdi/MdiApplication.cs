﻿using Afx.Business;
using Afx.Business.Activities;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Prism.RibbonMdi.Ribbon;
using Afx.Prism.RibbonMdi.Tools;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Afx.Prism.RibbonMdi
{
  public class MdiApplication : BusinessObject
  {
    public const string RibbonRegion = "MdiApplication.RibbonRegion";
    public const string DockingRegion = "MdiApplication.DockingRegion";
    public const string DialogRegion = "MdiApplication.DialogRegion";
    public const string SettingRegion = "MdiApplication.SettingRegion";
    
    internal const string ExceptionRegion = "MdiApplication.ExceptionRegion";
    internal const string WaitAnimationRegion = "MdiApplication.WaitAnimationRegion";

    public MdiApplication()
    {
    }

    #region string Title

    public const string TitleProperty = "Title";
    string mTitle = "Mdi Application";
    public string Title
    {
      get { return mTitle; }
      set { SetProperty<string>(ref mTitle, value, TitleProperty); }
    }

    #endregion

    #region DataMember bool IsModal

    public const string IsModalProperty = "IsModal";
    bool mIsModal;
    public bool IsModal
    {
      get { return mIsModal; }
      set { SetProperty<bool>(ref mIsModal, value, IsModalProperty); }
    }

    #endregion

    #region bool IsRibbonEnabled

    public const string IsRibbonEnabledProperty = "IsRibbonEnabled";
    bool mIsRibbonEnabled = true;
    public bool IsRibbonEnabled
    {
      get { return mIsRibbonEnabled; }
      set { SetProperty<bool>(ref mIsRibbonEnabled, value, IsRibbonEnabledProperty); }
    }

    #endregion

    bool mIsKeyboardVisible = false;
    public bool IsKeyboardVisible
    {
      get { return mIsKeyboardVisible; }
      set { mIsKeyboardVisible = value; }
    }

    ObservableCollection<IMdiDockableDocumentViewModel> mDocumentViewModels = new ObservableCollection<IMdiDockableDocumentViewModel>();
    public ObservableCollection<IMdiDockableDocumentViewModel> DocumentViewModels
    {
      get { return mDocumentViewModels; }
    }

    ObservableCollection<IMdiDockableToolViewModel> mToolViewModels = new ObservableCollection<IMdiDockableToolViewModel>();
    public ObservableCollection<IMdiDockableToolViewModel> ToolViewModels
    {
      get { return mToolViewModels; }
    }

    BasicCollection<FileMenuItemViewModel> mFileMenuItems;
    public BasicCollection<FileMenuItemViewModel> FileMenuItems
    {
      get { return mFileMenuItems ?? (mFileMenuItems = new BasicCollection<FileMenuItemViewModel>()); }
    }
  }
}

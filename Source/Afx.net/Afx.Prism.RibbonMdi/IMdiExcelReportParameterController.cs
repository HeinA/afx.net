﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi
{
  public interface IMdiExcelReportParameterController : IMdiDialogController
  {
    string Description { get; set; }
    string Title { get; set; }
  }
}

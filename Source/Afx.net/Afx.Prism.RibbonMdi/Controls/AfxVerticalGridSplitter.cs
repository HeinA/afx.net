﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace Afx.Prism.RibbonMdi.Controls
{
  public class AfxVerticalGridSplitter : GridSplitter
  {
    const string RegistryKey = @"GridSplitters\{0}\";

    public AfxVerticalGridSplitter()
    {
      if (!DesignerProperties.GetIsInDesignMode(this))
      {
        this.Unloaded += (sender, e) =>
        {
          SaveWidth();
        };

        this.Loaded += (sender, e) =>
        {
          LoadWidth();
        };
      }
    }

    void LoadWidth()
    {
      try
      {
        Grid gv = (Grid)this.Parent;
        foreach (var c in gv.ColumnDefinitions)
        {
          if (!string.IsNullOrWhiteSpace(Identifier))
          {
            int header = gv.ColumnDefinitions.IndexOf(c);
            int width = 0;
            if (RegistryHelper.GetRegistryValue<int>(Microsoft.Win32.RegistryHive.CurrentUser, string.Format(RegistryHelper.ApplicationKey, string.Format(RegistryKey, Identifier)), header.ToString(), Microsoft.Win32.RegistryValueKind.DWord, out width))
            {
              c.Width = new GridLength(width);
            }
          }
        }
      }
      catch { }
    }

    void SaveWidth()
    {
      try
      {
        Grid gv = (Grid)this.Parent;
        foreach (var c in gv.ColumnDefinitions)
        {
          if (!string.IsNullOrWhiteSpace(Identifier))
          {
            int header = gv.ColumnDefinitions.IndexOf(c);
            if (!c.Width.IsAuto && !c.Width.IsStar)
            {
              int width = (int)c.Width.Value;
              RegistryHelper.SetRegistryValue<int>(Microsoft.Win32.RegistryHive.CurrentUser, string.Format(RegistryHelper.ApplicationKey, string.Format(RegistryKey, Identifier)), header.ToString(), Microsoft.Win32.RegistryValueKind.DWord, width);
            }
          }
        }
      }
      catch { }
    }

    string mIdentifier;
    public string Identifier
    {
      get { return mIdentifier; }
      set
      {
        if (mIdentifier == value) return;
        mIdentifier = value;
      }
    }

    public static readonly DependencyProperty IdentifierProperty = DependencyProperty.Register("Identifier", typeof(string), typeof(AfxVerticalGridSplitter), new PropertyMetadata(string.Empty, OnIdentifierChanged));

    private static void OnIdentifierChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
      AfxVerticalGridSplitter vgs = d as AfxVerticalGridSplitter;
      vgs.Identifier = (string)e.NewValue;
    }
  }
}

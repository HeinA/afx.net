﻿using Afx.Prism.RibbonMdi.Ribbon;
using Fluent;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace Afx.Prism.RibbonMdi.Controls
{
  public class AfxRibbon : Fluent.Ribbon
  {
    #region ItemSource

    public IEnumerable ItemsSource
    {
      get { return (IEnumerable)GetValue(ItemsSourceProperty); }
      set { SetValue(ItemsSourceProperty, value); }
    }

    public static readonly DependencyProperty ItemsSourceProperty = DependencyProperty.Register("ItemsSource", typeof(IEnumerable), typeof(AfxRibbon), new PropertyMetadata(new PropertyChangedCallback(OnItemsSourcePropertyChanged)));

    private static void OnItemsSourcePropertyChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
    {
      var control = sender as AfxRibbon;
      if (control != null)
      {
        control.OnItemsSourceChanged((IEnumerable)e.OldValue, (IEnumerable)e.NewValue);
      }
    }

    private void OnItemsSourceChanged(IEnumerable oldValue, IEnumerable newValue)
    {
      Stack<object> stack = new Stack<object>(mTabDictionary.Keys);
      List<object> list = null;

      if (newValue != null)
      {
        list = newValue.Cast<object>().ToList();
        while (stack.Count > 0)
        {
          object o = stack.Pop();
          if (!list.Contains(o)) RemoveItem(o);
        }
      }
      else
      {
        while (stack.Count > 0)
        {
          object o = stack.Pop();
          RemoveItem(o);
        }
      }

      if (newValue != null)
      {
        foreach (object o in newValue)
        {
          if (!mTabDictionary.ContainsKey(o)) AddItem(o, list.IndexOf(o));
        }
      }

      var newValueINotifyCollectionChanged = newValue as INotifyCollectionChanged;
      if (newValueINotifyCollectionChanged != null)
      {
        WeakEventManager<INotifyCollectionChanged, NotifyCollectionChangedEventArgs>.AddHandler(newValueINotifyCollectionChanged, "CollectionChanged", Tabs_CollectionChanged);
      }

    }

    Dictionary<object, AfxRibbonTabItem> mTabDictionary = new Dictionary<object, AfxRibbonTabItem>();
    ItemsControl mItemsControl = new ItemsControl();

    void AddItem(object tvm, int index = -1)
    {
      RibbonTabItem srt = SelectedTabItem;
      DataTemplateKey dtk = new DataTemplateKey(tvm.GetType());
      DataTemplate dt = (DataTemplate)this.FindResource(dtk);
      AfxRibbonTabItem tab = (AfxRibbonTabItem)dt.LoadContent();
      tab.DataContext = tvm;
      mTabDictionary.Add(tvm, tab);
      if (index >= 0) Tabs.Insert(index, tab);
      else Tabs.Add(tab);
      if (tab.IsSelected)
      {
        SelectedTabItem = srt; //HACK: otherwise certain tabs can not be selected
        SelectedTabItem = tab;
      }
    }

    void RemoveItem(object tvm)
    {
      if (mTabDictionary.ContainsKey(tvm))
      {
        AfxRibbonTabItem tab = mTabDictionary[tvm];
        BindingOperations.ClearAllBindings(tab);
        tab.DataContext = null;
        if (SelectedTabItem == tab && Tabs.Count > 0) SelectedTabIndex = 0;
        Tabs.Remove(tab);
        mTabDictionary.Remove(tvm);
      }
    }

    void Tabs_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
    {
      switch (e.Action)
      {
        case NotifyCollectionChangedAction.Add:
          foreach (object tvm in e.NewItems)
          {
            AddItem(tvm);
          }
          break;

        case NotifyCollectionChangedAction.Remove:
          foreach (object tvm in e.OldItems)
          {
            RemoveItem(tvm);
          }
          break;

        case NotifyCollectionChangedAction.Reset:
          Stack<object> stack = new Stack<object>(mTabDictionary.Keys);
          while (stack.Count > 0)
          {
            RemoveItem(stack.Pop());
          }
          break;
      }
    }

    #endregion

    #region MenuItemsSource

    public IEnumerable MenuItemsSource
    {
      get { return (IEnumerable)GetValue(MenuItemsSourceProperty); }
      set { SetValue(MenuItemsSourceProperty, value); }
    }

    public static readonly DependencyProperty MenuItemsSourceProperty = DependencyProperty.Register("MenuItemsSource", typeof(IEnumerable), typeof(AfxRibbon), new PropertyMetadata(new PropertyChangedCallback(OnMenuItemsSourcePropertyChanged)));

    private static void OnMenuItemsSourcePropertyChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
    {
      var control = sender as AfxRibbon;
      if (control != null)
      {
        control.OnMenuItemsSourceChanged((IEnumerable)e.OldValue, (IEnumerable)e.NewValue);
      }
    }

    private void OnMenuItemsSourceChanged(IEnumerable oldValue, IEnumerable newValue)
    {
      var newValueINotifyCollectionChanged = newValue as INotifyCollectionChanged;
      if (newValueINotifyCollectionChanged != null)
      {
        WeakEventManager<INotifyCollectionChanged, NotifyCollectionChangedEventArgs>.AddHandler(newValueINotifyCollectionChanged, "CollectionChanged", MenuItems_CollectionChanged);
      }

    }

    Dictionary<object, FrameworkElement> mMenuDictionary = new Dictionary<object, FrameworkElement>();

    void AddMenuItem(object mivm, int index = -1)
    {
      RibbonTabItem srt = SelectedTabItem;
      DataTemplateKey dtk = new DataTemplateKey(mivm.GetType());
      DataTemplate dt = (DataTemplate)this.FindResource(dtk);
      FrameworkElement menuItem = (FrameworkElement)dt.LoadContent();
      menuItem.DataContext = mivm;
      mMenuDictionary.Add(mivm, menuItem);
      Backstage bs = (Backstage)Menu;
      BackstageTabControl bstc = (BackstageTabControl)bs.Content;
      if (index >= 0) bstc.Items.Insert(index, menuItem);
      else bstc.Items.Add(menuItem);
    }

    void RemoveMenuItem(object mivm)
    {
      if (mMenuDictionary.ContainsKey(mivm))
      {
        FrameworkElement menuItem = mMenuDictionary[mivm];
        BindingOperations.ClearAllBindings(menuItem);
        menuItem.DataContext = null;
        Backstage bs = (Backstage)Menu;
        BackstageTabControl bstc = (BackstageTabControl)bs.Content;
        bstc.Items.Remove(menuItem);
        mMenuDictionary.Remove(mivm);
      }
    }

    void MenuItems_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
    {
      switch (e.Action)
      {
        case NotifyCollectionChangedAction.Add:
          foreach (object tvm in e.NewItems)
          {
            AddMenuItem(tvm, e.NewStartingIndex);
          }
          break;

        case NotifyCollectionChangedAction.Remove:
          foreach (object tvm in e.OldItems)
          {
            RemoveMenuItem(tvm);
          }
          break;

        case NotifyCollectionChangedAction.Reset:
          Stack<object> stack = new Stack<object>(mMenuDictionary.Keys);
          while (stack.Count > 0)
          {
            RemoveMenuItem(stack.Pop());
          }
          break;
      }
    }

    #endregion
  }
}

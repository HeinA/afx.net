﻿using Afx.Business;
using Afx.Prism.Extensions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace Afx.Prism.RibbonMdi.Controls
{
  public class AfxSearchResultListView : AfxListView
  {
    public AfxSearchResultListView()
    {
    }

    public static readonly DependencyProperty ResultsProperty = DependencyProperty.Register("Results", typeof(SearchResults), typeof(AfxSearchResultListView), new FrameworkPropertyMetadata(ResultsChanged));
    public static readonly DependencyProperty LastActivatedResultProperty = DependencyProperty.Register("LastActivatedResult", typeof(Guid), typeof(AfxSearchResultListView), new FrameworkPropertyMetadata(Guid.Empty, LastActivatedResultChanged) { BindsTwoWayByDefault = true });

    private static void LastActivatedResultChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
    }

    private static void ResultsChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
      SearchResults sr = (SearchResults)e.NewValue;
      AfxSearchResultListView lv = (AfxSearchResultListView)d;
      if (!SortableList.GetIsGridSortable(lv) && sr.Headers.GetLength(0) > 0)
      {
        GridView gv = new GridView();
        if (sr != null)
        {
          for (int i = 0; i < sr.Headers.GetLength(0); i++)
          {
            GridViewColumn col = new GridViewColumn();
            col.Header = sr.Headers[i];
            col.DisplayMemberBinding = new Binding("[" + i + "]") { StringFormat = sr.Formats[i] == null ? null : "{0:" + sr.Formats[i] + "}" };
            gv.Columns.Add(col);
          }
        }
        lv.View = gv;
        SortableList.SetIsGridSortable(lv, true);
      }

      if (sr != null)
      {
        DataTable dt = ConvertToDataTable(sr);
        lv.ItemsSource = dt.Rows;
      }
    }

    private static DataTable ConvertToDataTable(SearchResults data)
    {
      DataTable dt = new DataTable();
      foreach(string s in data.Headers)
      {
        dt.Columns.Add(s, typeof(object));
      }
      dt.Columns.Add("GlobalIdentifier", typeof(Guid));

      foreach(SearchResult sr1 in data.Results)
      {
        DataRow dr = dt.NewRow();
        for (int col = 0; col < sr1.Values.GetLength(0); col++)
        {
          dr[col] = sr1.Values[col];
        }
        dr["GlobalIdentifier"] = sr1.GlobalIdentifier;
        dt.Rows.Add(dr);
      }

      return dt;
    }

    public SearchResults Results
    {
      get { return (SearchResults)GetValue(ResultsProperty); }
      set { SetValue(ResultsProperty, value); }
    }

    public Guid LastActivatedResult
    {
      get { return (Guid)GetValue(LastActivatedResultProperty); }
      set { SetValue(LastActivatedResultProperty, value); }
    }

    protected override void OnItemActivated(object item)
    {
      DataRow dr = (DataRow)item;
      Guid g = (Guid)dr["GlobalIdentifier"];
      SetValue(LastActivatedResultProperty, g);

      base.OnItemActivated(item);
    }
  }
}

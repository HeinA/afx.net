﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace Afx.Prism.RibbonMdi.Controls
{
  public class AfxListBox : ListBox
  {
    #region ICommandSource

    /// <summary>
    /// Identifies the <see cref="Command"/> property.
    /// </summary>
    public static readonly DependencyProperty CommandProperty = DependencyProperty.Register("Command",
            typeof(ICommand),
            typeof(ListBox));

    /// <summary>
    /// Identifies the <see cref="CommandParameter"/> property.
    /// </summary>
    public static readonly DependencyProperty CommandParameterProperty = DependencyProperty.Register("CommandParameter",
            typeof(object),
            typeof(ListBox),
            new FrameworkPropertyMetadata(null));

    /// <summary>
    /// Identifies the <see cref="CommandTarget"/> property.
    /// </summary>
    public static readonly DependencyProperty CommandTargetProperty = DependencyProperty.Register("CommandTarget",
            typeof(IInputElement),
            typeof(ListBox),
            new FrameworkPropertyMetadata(null));

    /// <summary>
    /// Identifies the <see cref="ItemActivated"/> event.
    /// </summary>
    public static readonly RoutedEvent ItemActivatedEvent = EventManager.RegisterRoutedEvent("ItemActivated",
            RoutingStrategy.Bubble,
            typeof(EventHandler<ItemActivatedEventArgs>),
            typeof(ListBox));

    /// <summary>
    /// Gets or sets the <see cref="ICommand"/> to execute whenever an item is activated.
    /// </summary>
    public ICommand Command
    {
      get
      {
        return GetValue(CommandProperty) as ICommand;
      }
      set
      {
        SetValue(CommandProperty, value);
      }
    }

    /// <summary>
    /// Gets or sets the parameter to be passed to the executed <see cref="Command"/>.
    /// </summary>
    public object CommandParameter
    {
      get
      {
        return GetValue(CommandParameterProperty);
      }
      set
      {
        SetValue(CommandParameterProperty, value);
      }
    }

    /// <summary>
    /// Gets or sets the element on which to raise the specified <see cref="Command"/>.
    /// </summary>
    public IInputElement CommandTarget
    {
      get
      {
        return GetValue(CommandTargetProperty) as IInputElement;
      }
      set
      {
        SetValue(CommandTargetProperty, value);
      }
    }

    /// <summary>
    /// Occurs whenever an item in this <c>ListView</c> is activated.
    /// </summary>
    public event EventHandler<ItemActivatedEventArgs> ItemActivated
    {
      add
      {
        AddHandler(ItemActivatedEvent, value);
      }
      remove
      {
        RemoveHandler(ItemActivatedEvent, value);
      }
    }

    static AfxListBox()
    {
      //register a handler for any double-clicks on ListViewItems
      EventManager.RegisterClassHandler(typeof(ListBoxItem), ListBoxItem.MouseDoubleClickEvent, new MouseButtonEventHandler(MouseDoubleClickHandler));
    }

    protected override void OnKeyDown(KeyEventArgs e)
    {
      base.OnKeyDown(e);

      //hitting enter activates an item too
      if ((e.Key == Key.Enter) && (SelectedItem != null))
      {
        OnItemActivated(SelectedItem);
      }
    }

    private void OnItemActivated(object item)
    {
      RaiseEvent(new ItemActivatedEventArgs(ItemActivatedEvent, item));

      //execute the command if there is one
      if (Command != null)
      {
        RoutedCommand routedCommand = Command as RoutedCommand;

        if (routedCommand != null)
        {
          routedCommand.Execute(CommandParameter, CommandTarget);
        }
        else
        {
          Command.Execute(CommandParameter);
        }
      }
    }

    private static void MouseDoubleClickHandler(object sender, MouseEventArgs e)
    {
      ListBoxItem listBoxItem = sender as ListBoxItem;
      Debug.Assert(listBoxItem != null);
      AfxListBox listBox = FindListViewForItem(listBoxItem);

      if (listBox != null)
      {
        listBox.OnItemActivated(listBoxItem.Content);
      }
    }

    private static AfxListBox FindListViewForItem(ListBoxItem listBoxItem)
    {
      DependencyObject parent = VisualTreeHelper.GetParent(listBoxItem);

      while (parent != null)
      {
        if (parent is AfxListBox)
        {
          return parent as AfxListBox;
        }

        parent = VisualTreeHelper.GetParent(parent);
      }

      return null;
    }

    #endregion
  }
}

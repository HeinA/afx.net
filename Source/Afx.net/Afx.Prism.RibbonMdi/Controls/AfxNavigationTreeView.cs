﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Afx.Prism.RibbonMdi.Controls
{
  public class AfxNavigationTreeView : AfxOperationTreeView
  {
    protected override void OnSelectedItemChanged(RoutedPropertyChangedEventArgs<object> e)
    {
      if (SelectedItem == null)
      {
        ISelectableViewModel svm = Items[0] as ISelectableViewModel;
        if (svm != null)
        {
          this.Focus();
          svm.IsFocused = true;
        }
      }

      if (SelectedItem != null)
      {
        ISelectableViewModel svm = SelectedItem as ISelectableViewModel;
        if (svm != null)
        {
          Dispatcher.BeginInvoke(new Action(() =>
          {
            if (svm.IsSelected)
              svm.IsFocused = true;
          }), System.Windows.Threading.DispatcherPriority.Input);
        }
      }

      base.OnSelectedItemChanged(e);
    }
  }
}

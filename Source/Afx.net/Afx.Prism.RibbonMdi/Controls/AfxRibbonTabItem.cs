﻿using Fluent;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace Afx.Prism.RibbonMdi.Controls
{
  public class AfxRibbonTabItem : RibbonTabItem
  {
    public IEnumerable ItemsSource
    {
      get { return (IEnumerable)GetValue(ItemsSourceProperty); }
      set { SetValue(ItemsSourceProperty, value); }
    }

    public static readonly DependencyProperty ItemsSourceProperty = DependencyProperty.Register("ItemsSource", typeof(IEnumerable), typeof(AfxRibbonTabItem), new PropertyMetadata(new PropertyChangedCallback(OnItemsSourcePropertyChanged)));

    private static void OnItemsSourcePropertyChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
    {
      var control = sender as AfxRibbonTabItem;
      if (control != null)
      {
        control.Dispatcher.BeginInvoke(new Action(() =>
        {
          try
          {
            control.OnItemsSourceChanged((IEnumerable)e.OldValue, (IEnumerable)e.NewValue);

            if (e.NewValue != null)
            {
              foreach (object gvm in (IEnumerable)e.NewValue)
              {
                control.AddItem(gvm);
              }
            }
          }
          catch
          {
#if DEBUG
            if (System.Diagnostics.Debugger.IsAttached) System.Diagnostics.Debugger.Break();
#endif
          }
        }), System.Windows.Threading.DispatcherPriority.Normal);
      }
    }

    private void OnItemsSourceChanged(IEnumerable oldValue, IEnumerable newValue)
    {
      foreach (object gvm in mGroupDictionary.Keys.ToList())
      {
        RemoveItem(gvm);
      }

      var newValueINotifyCollectionChanged = newValue as INotifyCollectionChanged;
      if (newValueINotifyCollectionChanged != null)
      {
        WeakEventManager<INotifyCollectionChanged, NotifyCollectionChangedEventArgs>.AddHandler(newValueINotifyCollectionChanged, "CollectionChanged", newValueINotifyCollectionChanged_CollectionChanged);
      }

    }

    Dictionary<object, RibbonGroupBox> mGroupDictionary = new Dictionary<object, RibbonGroupBox>();

    void AddItem(object gvm)
    {
      DataTemplateKey dtk = new DataTemplateKey(gvm.GetType());
      DataTemplate dt = (DataTemplate)this.FindResource(dtk);
      RibbonGroupBox group = (RibbonGroupBox)dt.LoadContent();
      group.DataContext = gvm;
      mGroupDictionary.Add(gvm, group);
      Groups.Add(group);
    }

    void RemoveItem(object gvm)
    {
      RibbonGroupBox group = mGroupDictionary[gvm];
      BindingOperations.ClearAllBindings(group);
      group.DataContext = null;
      Groups.Remove(group);
      mGroupDictionary.Remove(gvm);
    }

    void newValueINotifyCollectionChanged_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
    {
      switch (e.Action)
      {
        case NotifyCollectionChangedAction.Add:
          foreach (object gvm in e.NewItems)
          {
            AddItem(gvm);
          }
          break;

        case NotifyCollectionChangedAction.Remove:
          foreach (object gvm in e.OldItems)
          {
            RemoveItem(gvm);
          }
          break;

        case NotifyCollectionChangedAction.Reset:
          foreach (object gvm in mGroupDictionary.Keys.ToList())
          {
            RemoveItem(gvm);
          }
          break;
      }
    }
  }
}

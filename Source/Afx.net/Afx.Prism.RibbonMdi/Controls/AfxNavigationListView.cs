﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace Afx.Prism.RibbonMdi.Controls
{
  public class AfxNavigationListView : AfxOperationListView
  {
    public AfxNavigationListView()
    {
      this.SelectionMode = SelectionMode.Single;
    }

    protected override void OnSelectionChanged(SelectionChangedEventArgs e)
    {
      if (SelectedItems.Count == 0 && Items.Count > 0 && e.RemovedItems.Count > 0)
      {
        ISelectableViewModel svm = Items[0] as ISelectableViewModel;
        if (svm != null)
        {
          this.Focus();
          svm.IsFocused = true;
        }
      }

      if (SelectedItems.Count > 0)
      {
        ISelectableViewModel svm = SelectedItems[0] as ISelectableViewModel;
        if (svm != null)
        {
          Dispatcher.BeginInvoke(new Action(() =>
          {
            if (svm.IsSelected) svm.IsFocused = true;
          }), System.Windows.Threading.DispatcherPriority.Input);
        }
      }

      base.OnSelectionChanged(e);
    }   
  }
}

﻿using Afx.Business;
using Afx.Business.Security;
using Afx.Business.Service;
using Afx.Prism.Regions;
using Afx.Prism.Regions.Behaviors;
using Microsoft.Practices.Prism.Modularity;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Prism.UnityExtensions;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using Xceed.Wpf.AvalonDock;
using Xceed.Wpf.Toolkit;
using Xceed.Wpf.Toolkit.Primitives;

namespace Afx.Prism.RibbonMdi
{
  public abstract class MdiBootstrapper : UnityBootstrapper
  {
    public MdiBootstrapper()
    {
      mCurrent = this;

      ProxyHelper.SetupProxy();

      try
      {
        foreach (ResourceInfo ri in ExtensibilityManager.CompositionContainer.GetExportedValues<ResourceInfo>().OrderBy(ri1 => ri1.Priority))
        {
          ResourceDictionary dict = new ResourceDictionary();
          dict.Source = ri.GetUri();
          Application.Current.Resources.MergedDictionaries.Add(dict);
        }
      }
      catch (ReflectionTypeLoadException ex)
      {
        using (StreamWriter sw = new StreamWriter("ReflectionErrors.txt", true))
        {
          sw.WriteLine(ex.ToString());
        }
        throw;
      }
      catch
      {
        throw;
      }
    }

    static MdiBootstrapper mCurrent;
    public static MdiBootstrapper Current
    {
      get { return mCurrent; }
    }

    MdiApplicationView mApplicationView;

    public abstract Stream GetDefaultDockingLayout();

    protected override DependencyObject CreateShell()
    {
      MdiApplicationViewModel vm = CreateApplicationViewModel();
      Container.RegisterInstance<IMdiApplicationViewModel>(vm);

      mApplicationView = ApplicationController.GetInstance<MdiApplicationView>();
      Container.RegisterInstance<IApplicationView>(mApplicationView);

      return mApplicationView;
    }


    protected override void InitializeShell()
    {
      base.InitializeShell();
      Application.Current.MainWindow = (Window)this.Shell;
      Application.Current.MainWindow.Show();
    }

    internal AuthenticationResult GetLoginDetails()
    {
      return DefaultLoginDetails();
    }

    protected virtual AuthenticationResult DefaultLoginDetails()
    {
      ICredentialProvider cp = ExtensibilityManager.CompositionContainer.GetExportedValues<ICredentialProvider>().FirstOrDefault();
      if (cp == null) return null;
      LoginOrganizationalUnit loginOU = null;
      using (var svc = ProxyFactory.GetService<IAfxService>(DefaultServer.ServerName))
      {
        loginOU = svc.LoadLoginOrganizationalUnits().Where(ou => ou.GlobalIdentifier.Equals(cp.OrganizationalUnit)).FirstOrDefault();
      }
      return SecurityContext.Authenticate(loginOU, cp.Username, cp.Password);
    }

    protected override RegionAdapterMappings ConfigureRegionAdapterMappings()
    {
      RegionAdapterMappings mappings = base.ConfigureRegionAdapterMappings();
      mappings.RegisterMapping(typeof(WindowContainer), ApplicationController.GetInstance<WindowContainerRegionAdapter>());
      mappings.RegisterMapping(typeof(DockPanel), ApplicationController.GetInstance<DockPanelRegionAdapter>());
      return mappings;
    }

    //protected override IRegionBehaviorFactory ConfigureDefaultRegionBehaviors()
    //{
    //  IRegionBehaviorFactory factory = base.ConfigureDefaultRegionBehaviors();
    //  factory.AddIfMissing(RegionUnloadedBehavior.BehaviorKey, typeof(RegionUnloadedBehavior));
    //  return factory;
    //}

    protected override void ConfigureContainer()
    {
      base.ConfigureContainer();
      IUnityContainer uc = this.Container;
      ApplicationController = new MdiApplicationController(uc, CreateApplicationContext()); 
    }

    protected virtual MdiApplication CreateApplicationContext()
    {
      return new MdiApplication();
    }

    protected virtual MdiApplicationViewModel CreateApplicationViewModel()
    {
      return ApplicationController.GetInstance<MdiApplicationViewModel>();
    }

    IMdiApplicationController mApplicationController;
    public IMdiApplicationController ApplicationController
    {
      get { return mApplicationController; }
      private set { mApplicationController = value; }
    }

    protected override IModuleCatalog CreateModuleCatalog()
    {
      IModuleCatalog mc = base.CreateModuleCatalog();
      mc.AddModule(new ModuleInfo(Namespace.Afx, "Afx.Prism.RibbonMdi.RibbonMdiModuleController, Afx.Prism.RibbonMdi"));

      foreach (IModuleInfo mi in ExtensibilityManager.CompositionContainer.GetExportedValues<IModuleInfo>())
      {
        mc.AddModule(new ModuleInfo(mi.ModuleNamespace, TypeHelper.GetTypeName(mi.ModuleController), mi.ModuleDependencies));
      }
      return mc;
    }

    protected override void ConfigureModuleCatalog()
    {
      base.ConfigureModuleCatalog();
    }

    protected override void InitializeModules()
    {
      ApplicationController.Run();
      base.InitializeModules();

      mApplicationView.LoadLayout(null);
      //foreach (var tw in MdiApplicationController.Current.ApplicationViewModel.ToolViewModels)
      //{
      //  tw.IsVisible = false;
      //}
      //
    }
  }
}

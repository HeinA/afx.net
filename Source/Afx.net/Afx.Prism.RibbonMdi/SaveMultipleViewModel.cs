﻿using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi
{
  class SaveMultipleViewModel : MdiDialogViewModel<UnsavedDocumentContext>
  {
    [InjectionConstructor]
    public SaveMultipleViewModel(IController controller)
      : base(controller)
    {
    }

    public string Title
    {
      get { return MdiApplicationController.Current.ApplicationContext.Title; }
    }

    Collection<UnsavedDocumentViewModel> mViewModels;
    public IEnumerable<UnsavedDocumentViewModel> Controllers
    {
      get
      {
        if (mViewModels == null)
        {
          mViewModels = new Collection<UnsavedDocumentViewModel>();
          foreach (var c in Model.Controllers)
          {
            mViewModels.Add(new UnsavedDocumentViewModel(c));
          }
        }
        return mViewModels;
      }
    }

    [Dependency]
    public SaveMultipleController SaveMultipleController { get; set; }

    #region DelegateCommand DiscardChangesCommand

    DelegateCommand mDiscardChangesCommand;
    public DelegateCommand DiscardChangesCommand
    {
      get { return mDiscardChangesCommand ?? (mDiscardChangesCommand = new DelegateCommand(ExecuteDiscardChanges)); }
    }

    void ExecuteDiscardChanges()
    {
      SaveMultipleController.Discard();
    }

    #endregion
  }
}

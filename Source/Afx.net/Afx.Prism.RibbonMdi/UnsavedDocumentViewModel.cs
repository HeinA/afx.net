﻿using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace Afx.Prism.RibbonMdi
{
  class UnsavedDocumentViewModel
  {
    public UnsavedDocumentViewModel(IMdiDockableDocumentController controller)
    {
      DocumentTitle = controller.Title;
      if (controller.IsValid) mForeground = new SolidColorBrush(Colors.Black);
      else mForeground = new SolidColorBrush(Colors.Red);
    }

    string mDocumentTitle;
    public string DocumentTitle
    {
      get { return mDocumentTitle; }
      private set { mDocumentTitle = value; }
    }


    #region Brush Foreground

    SolidColorBrush mForeground;
    public SolidColorBrush Foreground
    {
      get { return mForeground; }
    }

    #endregion

  }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi
{
  public interface IReportInfo
  {
    string GroupName { get; }
    string ReportName { get; }
  }
}

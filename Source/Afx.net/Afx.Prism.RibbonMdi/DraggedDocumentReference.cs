﻿using Afx.Business;
using Afx.Business.Documents;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi
{
  public class DraggedDocumentReference
  {
    public DraggedDocumentReference(DocumentType documentType, Guid documentGuid)
    {
      DocumentType = documentType;
      DocumentGuid = documentGuid;
    }

    #region DocumentType DocumentType

    DocumentType mDocumentType;
    public DocumentType DocumentType
    {
      get { return mDocumentType; }
      private set { mDocumentType = value; }
    }

    #endregion

    #region Guid DocumentGuid

    Guid mDocumentGuid;
    public Guid DocumentGuid
    {
      get { return mDocumentGuid; }
      private set { mDocumentGuid = value; }
    }

    #endregion
  }
}

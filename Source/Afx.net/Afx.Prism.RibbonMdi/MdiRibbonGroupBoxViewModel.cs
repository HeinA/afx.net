﻿using Afx.Business;
using Afx.Business.Collections;
using Microsoft.Practices.Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi
{
  public abstract class MdiRibbonGroupBoxViewModel<TModel, TItemModel> : ViewModel<TModel> // MdiRibbonGroupBoxViewModel
    where TModel : BusinessObject
    where TItemModel : BusinessObject
  {
    protected MdiRibbonGroupBoxViewModel(IController controller)
      : base(controller)
    {
    }

    BasicCollection<TItemModel> mItems;
    public BasicCollection<TItemModel> Items
    {
      get { return mItems; }
      set
      {
        if (Items != null) Items.CollectionChanged -= Model_CollectionChanged;
        mItems = value;
        if (Items != null) Items.CollectionChanged += Model_CollectionChanged;
        InternalItemViewModels.Clear();
        foreach (TItemModel model in Items)
        {
          InternalItemViewModels.Add(CreateViewModel(model));
        }
      }
    }

    void Model_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
    {
      switch (e.Action)
      {
        case NotifyCollectionChangedAction.Add:
          foreach (TItemModel model in e.NewItems)
          {
            InternalItemViewModels.Add(CreateViewModel(model));
          }
          break;

        case NotifyCollectionChangedAction.Remove:
          throw new NotImplementedException();
          //foreach (TItemModel group in e.OldItems)
          //{
          //  //MdiRibbonGroupBoxViewModel vm = Groups.OfType<MdiRibbonGroupBoxViewModel>().FirstOrDefault((g) => g.Group.Equals(group));
          //  //if (vm != null) InternalItems.Remove(vm);
          //}
          //break;

        case NotifyCollectionChangedAction.Reset:
          InternalItemViewModels.Clear();
          break;

        default:
          throw new NotImplementedException();
      }
    }

    protected abstract object CreateViewModel(TItemModel model);

    ObservableCollection<object> mInternalItemViewModels;
    protected ObservableCollection<object> InternalItemViewModels
    {
      get { return mInternalItemViewModels ?? (mInternalItemViewModels = new ObservableCollection<object>()); }
      set
      {
        if (SetProperty<ObservableCollection<object>>(ref mInternalItemViewModels, value))
        {
          mItemViewModels = null;
          OnPropertyChanged("Items");
        }
      }
    }

    ReadOnlyObservableCollection<object> mItemViewModels;
    public ReadOnlyObservableCollection<object> ItemViewModels
    {
      get { return mItemViewModels ?? (mItemViewModels = new ReadOnlyObservableCollection<object>(InternalItemViewModels)); }
    }
  }

  //public abstract class MdiRibbonGroupBoxViewModel : ViewModel
  //{
  //  protected MdiRibbonGroupBoxViewModel(IController controller)
  //    : base(controller)
  //  {
  //  }
  //}
}

﻿using Afx.Business;
using Afx.Business.Activities;
using Afx.Business.Collections;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Mvvm;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Afx.Prism.RibbonMdi
{
  public class MdiOperationViewModel : ViewModel<Operation> // MdiOperationViewModelBase, IViewModel<Operation>
  {
    public MdiOperationViewModel(IController controller)
      : base(controller)
    {
    }

    [Dependency]
    public IMdiDockableDocumentController OperationController { get; set; }

    #region string Text

    public string Text
    {
      get
      {
        if (!string.IsNullOrWhiteSpace(TextOverride)) return TextOverride;
        return Model != null ? Model.Text : null;
      }
    }

    #endregion

    #region string TextOverride

    string mTextOverride;
    public string TextOverride
    {
      get { return mTextOverride; }
      set
      {
        if (SetProperty<string>(ref mTextOverride, value))
        {
          OnPropertyChanged("Text");
        }
      }
    }

    #endregion

    #region string Description

    public string Description
    {
      get { return Model != null ? Model.Description : null; }
    }

    #endregion

    Visibility mVisibility = Visibility.Visible;
    public Visibility Visibility
    {
      get { return mVisibility; }
      set { SetProperty<Visibility>(ref mVisibility, value); }
    }

    #region Visibility ImageVisibility

    public Visibility ImageVisibility
    {
      get
      {
        if (Model == null) return Visibility.Collapsed;
        if (string.IsNullOrWhiteSpace(Model.Base64Image)) return Visibility.Collapsed;
        return Visibility.Visible;
      }
    }

    #endregion

    #region ImageSource ImageSource

    ImageSource mImageSource;
    public ImageSource ImageSource
    {
      get
      {
        if (Model == null) return default(ImageSource);
        if (string.IsNullOrWhiteSpace(Model.Base64Image)) return null;
        if (mImageSource == null)
        {
          byte[] imgData = Convert.FromBase64String(Model.Base64Image);
          BitmapImage img = new BitmapImage();
          using (MemoryStream ms = new MemoryStream(imgData))
          {
            img.BeginInit();
            img.CacheOption = BitmapCacheOption.OnLoad;
            img.StreamSource = ms;
            img.EndInit();
          }
          mImageSource = img as ImageSource;
        }
        return mImageSource;
      }
    }

    #endregion

    public Image Image
    {
      get { return new Image() { Source = ImageSource }; }
    }

    #region DelegateCommand ExecuteOperationCommand

    DelegateCommand mExecuteOperationCommand;
    public DelegateCommand ExecuteOperationCommand
    {
      get
      {
        if (mExecuteOperationCommand == null) mExecuteOperationCommand = new DelegateCommand(ExecuteOperation);
        return mExecuteOperationCommand;
      }
    }

    private void ExecuteOperation()
    {
      if (Model != null) using (new WaitCursor()) { OperationController.ExecuteOperation(Model); }
    }

    #endregion
  }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace Afx.Prism.RibbonMdi
{
  class PanesStyleSelector : StyleSelector
  {
    public override Style SelectStyle(object item, DependencyObject container)
    {
      if (typeof(IMdiDockableDocumentViewModel).IsAssignableFrom(item.GetType())) return (Style)Application.Current.FindResource("Document");
      if (typeof(IMdiDockableToolViewModel).IsAssignableFrom(item.GetType())) return (Style)Application.Current.FindResource("Tool");
      return base.SelectStyle(item, container);
    }
  }
}

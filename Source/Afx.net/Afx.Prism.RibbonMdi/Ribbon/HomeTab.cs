﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Ribbon
{
  public class HomeTab : RibbonTab
  {
    public const string TabName = "Home";

    public override string Name
    {
      get { return TabName; }
    }
  }
}

﻿using Afx.Business;
using Afx.Business.Security;
using Afx.Prism.RibbonMdi.Ribbon;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Ribbon
{
  public class ReportingTab : RibbonTab
  {
    public ReportingTab()
    {
      IEnumerable<IReportInfo> reports = ExtensibilityManager.CompositionContainer.GetExportedValues<ICrystalReportInfo>().Cast<IReportInfo>().Union(ExtensibilityManager.CompositionContainer.GetExportedValues<IExcelReportInfo>().Cast<IReportInfo>());
      foreach (var groupName in reports.Select(ri => ri.GroupName).Distinct().OrderBy(ri => ri))
      {
        RibbonGroup rg = new ReportingRibbonGroup(groupName);
        foreach (var reportInfo in reports.Where(ri => ri.GroupName == groupName).OrderBy(ri => ri.ReportName))
        {
          var atts = reportInfo.GetType().GetCustomAttributes<AuthorizationAttribute>();
          if (atts.Count() == 0 || atts.Any(a => SecurityContext.User.HasRole(a.Role)))
          {
            ReportingRibbonItem rri = new ReportingRibbonItem(reportInfo);
            rg.AddItem(rri);
          }
        }
        if (rg.Items.Count() > 0) AddGroup(rg);
      }
    }

    public const string TabName = "Reporting";

    public override string Name
    {
      get { return TabName; }
    }

    public override int Index
    {
      get { return 1; }
    }
  }
}

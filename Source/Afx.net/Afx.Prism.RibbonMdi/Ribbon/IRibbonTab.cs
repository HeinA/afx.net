﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Ribbon
{
  public interface IRibbonTab
  {
    string Name { get; }
    int Index { get; }
    IEnumerable<IRibbonGroup> Groups { get; }

    void AddGroup(IRibbonGroup group);
    IRibbonGroup GetGroup(string name);
  }
}

﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Ribbon
{
  public interface IRibbonGroup
  {
    string TabName { get; }
    string Name { get; }
    int Index { get; }
    bool PlaceBehind { get; }
    IEnumerable<IRibbonItem> Items { get; }

    void AddItem(IRibbonItem item);
  }
}

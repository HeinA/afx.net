﻿using Afx.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Ribbon
{
  public class HomeTabContext : BusinessObject
  {
    public HomeTabContext()
    {
      GlobalIdentifier = Guid.Parse("{F3DFBC87-C82D-4088-9305-8D875A88A618}");
      mCurrent = this;
    }

    static HomeTabContext mCurrent;
    public static HomeTabContext Current
    {
      get { return HomeTabContext.mCurrent; }
    }
  }
}

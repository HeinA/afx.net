﻿using Afx.Business.Documents;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Ribbon
{
  public interface ISelectableDocumentType
  {
    ViewModel<DocumentType> SelectedViewModel { get; set; }
  }
}

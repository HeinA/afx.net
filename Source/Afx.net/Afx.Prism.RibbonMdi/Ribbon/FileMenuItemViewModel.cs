﻿using Microsoft.Practices.Prism.Mvvm;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Ribbon
{
  public abstract class FileMenuItemViewModel : ViewModel
  {
    [InjectionConstructor]
    public FileMenuItemViewModel(IController controller)
      : base(controller)
    {
    }

    string mHeader;
    public string Header
    {
      get { return mHeader; }
      set { SetProperty<string>(ref mHeader, value); }
    }
  }
}

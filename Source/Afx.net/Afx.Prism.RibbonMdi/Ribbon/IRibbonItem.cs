﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Ribbon
{
  public interface IRibbonItem
  {
    string TabName { get; }
    string GroupName { get; }
    string ItemIdentifier { get; }
    int Index { get; }
  }
}

﻿using Microsoft.Practices.ObjectBuilder2;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Mvvm;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Afx.Prism.RibbonMdi.Ribbon
{
  class HomeTabViewModel : RibbonTabViewModel<HomeTabContext>, IBuilderAware
  {
    [InjectionConstructor]
    public HomeTabViewModel(IController controller)
      : base(controller)
    {
      Model = new HomeTabContext();
    }

    [Dependency]
    public RibbonMdiModuleController ModuleController { get; set; }

    public void OnBuiltUp(NamedTypeBuildKey buildKey)
    {
      ModuleController.SearchController.ViewModel.PropertyChanged += SearchControllerViewModel_PropertyChanged;
      ModuleController.KeyboardController.ViewModel.PropertyChanged += KeyboardControllerViewModel_PropertyChanged;
      IsSelected = true;
    }

    public void OnTearingDown()
    {
    }

    void SearchControllerViewModel_PropertyChanged(object sender, PropertyChangedEventArgs e)
    {
      if (e.PropertyName == "IsVisible")
      {
        OnPropertyChanged("IsSearchVisible");
      }
    }

    private void KeyboardControllerViewModel_PropertyChanged(object sender, PropertyChangedEventArgs e)
    {
      if (e.PropertyName == "IsVisible")
      {
        OnPropertyChanged("IsKeyboardVisible");
      }
    }

    public bool IsSearchVisible
    {
      get { return ModuleController.SearchController.ViewModel.IsVisible; }
      set { ModuleController.SearchController.ViewModel.IsVisible = value; }
    }

    public bool IsKeyboardVisible
    {
      get
      {
        MdiApplicationController.Current.ApplicationContext.IsKeyboardVisible = ModuleController.KeyboardController.ViewModel.IsVisible;
        return ModuleController.KeyboardController.ViewModel.IsVisible;
      }
      set
      {
        MdiApplicationController.Current.ApplicationContext.IsKeyboardVisible = value;
        ModuleController.KeyboardController.ViewModel.IsVisible = value;
      }
    }
  }
}

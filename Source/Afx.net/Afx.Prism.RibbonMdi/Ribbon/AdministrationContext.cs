﻿using Afx.Business;
using Afx.Business.Activities;
using Afx.Business.Collections;
using Afx.Business.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Ribbon
{
  public class AdministrationContext : BusinessObject
  {
    BasicCollection<ActivityGroup> mActivityGroups;
    public BasicCollection<ActivityGroup> ActivityGroups
    {
      get
      {
        return mActivityGroups ?? (mActivityGroups = Cache.Instance.GetObjects<ActivityGroup>((g) => g.GroupName, true));
      }
    }
  }
}

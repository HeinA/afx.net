﻿using Afx.Business;
using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Business.Service;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Ribbon
{
  [Export(typeof(IRibbonItem))]
  public class RefreshButton : RibbonButtonViewModel
  {
    public override string TabName
    {
      get { return HomeTab.TabName; }
    }

    public override string GroupName
    {
      get { return ToolsGroup.GroupName; }
    }

    public override string ItemIdentifier
    {
      get { return "Re3freshButton"; }
    }

    public override int Index
    {
      get { return 10; }
    }

    protected override void OnExecute()
    {
      try
      {
        using(new WaitCursor())
        using (var svc = ProxyFactory.GetService<IAfxService>(SecurityContext.MasterServer.Name))
        {
          Cache.Instance.LoadCache(svc.LoadCache());
        }
      }
      catch (Exception ex)
      {
        if (ExceptionHelper.HandleException(ex)) throw ex;
      }
    }
  }
}

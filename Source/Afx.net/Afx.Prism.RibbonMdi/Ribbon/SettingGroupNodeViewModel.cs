﻿using Afx.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Ribbon
{
  public class SettingGroupNodeViewModel : MdiNavigationTreeNodeViewModel<SettingGroupView>
  {
    #region Constructors

    [InjectionConstructor]
    public SettingGroupNodeViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    protected override void OnModelChanged()
    {
      base.OnModelChanged();
      IsExpanded = true;
    }

    #region string Text

    public string Text
    {
      get { return Model == null ? GetDefaultValue<string>() : Model.Text; }
    }

    #endregion

    #region IEnumerable<SettingNodeViewModel> Settings

    public IEnumerable<SettingNodeViewModel> Settings
    {
      get
      {
        IEnumerable<SettingNodeViewModel> s = ResolveViewModels<SettingNodeViewModel, Setting>(Model.Settings as IList);
        return s;
      }
    }

    #endregion

    #region bool IsSelected

    public override bool IsSelected
    {
      get
      {
        if (base.IsSelected) ((MdiApplicationController)Controller).ShowSettingDetailView(null);
        return base.IsSelected;
      }
      set
      {
        base.IsSelected = value;
        if (base.IsSelected) ((MdiApplicationController)Controller).ShowSettingDetailView(null);
      }
    }

    #endregion
  }
}

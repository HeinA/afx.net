﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Ribbon
{
  public abstract class RibbonTab : IRibbonTab
  {
    public abstract string Name { get; }

    public virtual int Index
    {
      get { return 0; }
    }

    ObservableCollection<IRibbonGroup> mGroups = new ObservableCollection<IRibbonGroup>();
    public IEnumerable<IRibbonGroup> Groups
    {
      get { return mGroups; }
    }

    public void AddGroup(IRibbonGroup group)
    {
      RibbonGroup rg = group as RibbonGroup;
      if (rg != null) rg.RibbonTab = this;
      mGroups.Add(group);
    }

    public IRibbonGroup GetGroup(string name)
    {
      return Groups.FirstOrDefault(rg => rg.Name == name);
    }
  }
}

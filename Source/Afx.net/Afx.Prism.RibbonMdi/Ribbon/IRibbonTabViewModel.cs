﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Ribbon
{
  public interface IRibbonTabViewModel
  {
    bool IsSelected { get; set; }
    bool IsOpen { get; set; }
  }
}

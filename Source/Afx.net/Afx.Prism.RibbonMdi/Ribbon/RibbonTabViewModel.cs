﻿using Afx.Business;
using Microsoft.Practices.Prism.Mvvm;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Ribbon
{
  public class RibbonTabViewModel<TModel> : StatefullViewModel<TModel>, IRibbonTabViewModel
    where TModel : class, IBusinessObject
  {
    protected RibbonTabViewModel(IController controller)
      : base(controller)
    {
    }

    [Dependency]
    public IMdiApplicationController ApplicationController { get; set; }


    public bool IsSelected
    {
      get { return GetState<bool>(); }
      set
      {
        SetState<bool>(value);
        //if (value) ApplicationController.ApplicationContext.SelectedTabViewModel = this;
      }
    }

    public bool IsOpen
    {
      get { return GetState<bool>(); }
      set
      {
        SetState<bool>(value);
        //if (value) ApplicationController.ApplicationContext.SelectedTabViewModel = this;
      }
    }
  }
}

﻿using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Mvvm;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Ribbon
{
  class LoggedOutViewModel : RibbonTabViewModel
  {
    #region DelegateCommand LoginCommand

    DelegateCommand _loginCommand;
    public DelegateCommand LoginCommand
    {
      get { return _loginCommand ?? (_loginCommand = new DelegateCommand(ExecuteLogin)); }
    }

    void ExecuteLogin()
    {
      LoggedOutController.ShowLoginDialog();
    }

    #endregion

    [Dependency]
    public LoggedOutController LoggedOutController { get; set; }
  }
}

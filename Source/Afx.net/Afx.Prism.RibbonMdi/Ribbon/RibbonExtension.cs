﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace Afx.Prism.RibbonMdi.Ribbon
{
  static class RibbonExtension
  {
    public static IEnumerable<IRibbonTab> GetItems(DependencyObject obj)
    {
      return (IEnumerable<IRibbonTab>)obj.GetValue(ItemsProperty);
    }

    public static void SetItems(DependencyObject obj, IEnumerable<IRibbonTab> value)
    {
      obj.SetValue(ItemsProperty, value);
    }

    public static readonly DependencyProperty ItemsProperty = DependencyProperty.RegisterAttached("Items", typeof(IEnumerable<IRibbonTab>), typeof(RibbonExtension), new FrameworkPropertyMetadata(null, OnItemsPropertyChanged));

    static Fluent.Ribbon mRibbon;

    private static void OnItemsPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
      if (mRibbon == null) mRibbon = (Fluent.Ribbon)d;

      if (e.OldValue != null)
      {
        IEnumerable<IRibbonTab> tabs = (IEnumerable<IRibbonTab>)e.OldValue;
        ClearTabs(tabs);
        INotifyCollectionChanged ncc = e.OldValue as INotifyCollectionChanged;
        if (ncc != null) ncc.CollectionChanged -= Tabs_CollectionChanged;
      }

      if (e.NewValue != null)
      {
        IEnumerable<IRibbonTab> tabs = (IEnumerable<IRibbonTab>)e.NewValue;
        PopulateTabs((IList)e.NewValue, 0);
        INotifyCollectionChanged ncc = tabs as INotifyCollectionChanged;
        if (ncc != null) ncc.CollectionChanged += Tabs_CollectionChanged;
      }
    }

    static void Groups_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
    {
      if (e.NewItems != null)
      {
        PopulateGroups(e.NewItems, e.NewStartingIndex);
      }
    }

    static void Tabs_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
    {
      if (e.Action == NotifyCollectionChangedAction.Reset)
      {
        Collection<IRibbonTab> tabs = new Collection<IRibbonTab>();
        foreach (IRibbonTab tab in mTabDictionary.Keys)
        {
          tabs.Add(tab);
        }
        ClearTabs(tabs);
      }
      else
      {
        if (e.OldItems != null)
        {
          ClearTabs(e.OldItems);
        }

        if (e.NewItems != null)
        {
          PopulateTabs(e.NewItems, e.NewStartingIndex);
        }
      }
    }

    static Dictionary<IRibbonTab, Fluent.RibbonTabItem> mTabDictionary = new Dictionary<IRibbonTab, Fluent.RibbonTabItem>();
    static Dictionary<IRibbonGroup, Fluent.RibbonGroupBox> mGroupDictionary = new Dictionary<IRibbonGroup, Fluent.RibbonGroupBox>();

    static void ClearTabs(IEnumerable tabs)
    {
      foreach (IRibbonTab tab in tabs)
      {
        Fluent.RibbonTabItem rt = mTabDictionary[tab];
        mRibbon.Tabs.Remove(rt);
        INotifyCollectionChanged nccg = tab.Groups as INotifyCollectionChanged;
        if (nccg != null) nccg.CollectionChanged -= Groups_CollectionChanged;
      }
    }

    static void PopulateTabs(IEnumerable tabs, int index)
    {
      int i = index;
      foreach (IRibbonTab tab in tabs)
      {
        Fluent.RibbonTabItem ti = new Fluent.RibbonTabItem();
        ti.Header = tab.Name;
        mTabDictionary.Add(tab, ti);

        PopulateGroups(tab.Groups, 0);
        INotifyCollectionChanged nccg = tab.Groups as INotifyCollectionChanged;
        if (nccg != null) nccg.CollectionChanged += Groups_CollectionChanged;

        int count = mRibbon.Tabs.Count;
        mRibbon.Tabs.Insert(index++, ti);
        if (count == 0) mRibbon.SelectedTabItem = ti;
      }
    }

    static void PopulateGroups(IEnumerable groups, int index)
    {
      foreach (RibbonGroup group in groups)
      {
        Fluent.RibbonGroupBox gb = new Fluent.RibbonGroupBox();
        gb.Header = group.Name;
        if (group.Items.Count() == 0) gb.Visibility = Visibility.Collapsed;
        mGroupDictionary.Add(group, gb);

        Binding binding = new Binding();
        binding.Source = group.Items;
        gb.SetBinding(Fluent.RibbonGroupBox.ItemsSourceProperty, binding);

        INotifyCollectionChanged ncci = group.Items as INotifyCollectionChanged;
        WeakEventManager<INotifyCollectionChanged, NotifyCollectionChangedEventArgs>.AddHandler(ncci, "CollectionChanged", Items_CollectionChanged);

        Fluent.RibbonTabItem ti = mTabDictionary[group.RibbonTab];
        ti.Groups.Insert(index++, gb);
      }
    }

    static void Items_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
    {
      RibbonItemViewModel rivm = null;
      if (e.OldItems != null) rivm = e.OldItems.OfType<RibbonItemViewModel>().FirstOrDefault();
      if (e.NewItems != null) rivm = e.NewItems.OfType<RibbonItemViewModel>().FirstOrDefault();
      Fluent.RibbonGroupBox gb = mGroupDictionary[(IRibbonGroup)rivm.RibbonGroup];

      IList list = sender as IList;
      if (list.Count > 0)
      {
        gb.Visibility = Visibility.Visible;
      }
      else
      {
        gb.Visibility = Visibility.Collapsed;
      }
    }
  }
}

﻿using Afx.Prism.RibbonMdi.Tools.Output;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Ribbon
{
  [Export(typeof(IRibbonItem))]
  public class OutputToggleButton : RibbonToggleButtonViewModel
  {
    public override string TabName
    {
      get { return HomeTab.TabName; }
    }

    public override string GroupName
    {
      get { return ToolsGroup.GroupName; }
    }

    public override string ItemIdentifier
    {
      get { return "Output"; }
    }

    public override int Index
    {
      get { return 1; }
    }

    OutputController mOutputController = null;
    [Dependency]
    public OutputController OutputController
    {
      get { return mOutputController; }
      set
      {
        mOutputController = value;
        mOutputController.ViewModel.PropertyChanged += ViewModel_PropertyChanged;
      }
    }

    void ViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
    {
      if (e.PropertyName == "IsVisible")
      {
        OnPropertyChanged("IsChecked");
      }
    }

    public override bool IsChecked
    {
      get { return OutputController.ViewModel.IsVisible; }
      set { OutputController.ViewModel.IsVisible = value; }
    }
  }
}

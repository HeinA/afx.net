﻿using Afx.Business.Activities;
using Afx.Business.Collections;
using Afx.Business.Data;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Ribbon
{
  public class ActivityGroupViewModel : ViewModel<ActivityGroup>, IVisibilityAware
  {
    [InjectionConstructor]
    public ActivityGroupViewModel(IController controller)
      : base(controller)
    {
    }

    public IEnumerable<ActivityViewModel> ItemViewModels
    {
      get { return ResolveViewModels<ActivityViewModel, Activity>(new BasicCollection<Activity>(Model.Activities.Where((a) => a.CanView))); }
    }

    public string GroupName
    {
      get { return Model.GroupName; }
    }

    public bool IsVisible
    {
      get { return Model.Activities.Count((a) => a.CanView) > 0; }
    }
  }
}

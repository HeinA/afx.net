﻿using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Mvvm;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Ribbon
{
  class HomeViewModel : RibbonTabViewModel
  {
    #region DelegateCommand LogoutCommand

    DelegateCommand _logoutCommand;
    public DelegateCommand LogoutCommand
    {
      get { return _logoutCommand ?? (_logoutCommand = new DelegateCommand(ExecuteLogout)); }
    }

    void ExecuteLogout()
    {
      ModuleController.LogoutUser();
    }

    #endregion

    #region DelegateCommand OrganizationalStructureManagementCommand

    DelegateCommand _organizationalStructureManagementCommand;
    public DelegateCommand OrganizationalStructureManagementCommand
    {
      get { return _organizationalStructureManagementCommand ?? (_organizationalStructureManagementCommand = new DelegateCommand(ExecuteOrganizationalStructureManagement)); }
    }

    void ExecuteOrganizationalStructureManagement()
    {
      ModuleController.ManageOrganizationalStructure();
    }

    #endregion
  }
}

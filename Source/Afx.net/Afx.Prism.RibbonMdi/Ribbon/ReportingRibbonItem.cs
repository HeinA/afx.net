﻿using Afx.Business;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.ReportAppServer.ClientDoc;
using CrystalDecisions.ReportAppServer.Controllers;
using CrystalDecisions.ReportAppServer.DataDefModel;
using CrystalDecisions.Shared;
using Microsoft.Practices.Prism.Commands;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Afx.Prism.RibbonMdi.Ribbon
{
  public class ReportingRibbonItem : IRibbonItem
  {
    public ReportingRibbonItem(IReportInfo info)
    {
      ReportInfo = info;
    }

    IReportInfo ReportInfo { get; set; }

    public string TabName
    {
      get { return ReportingTab.TabName; }
    }

    public string GroupName
    {
      get { return ReportInfo.GroupName; }
    }

    public string ReportName
    {
      get { return ReportInfo.ReportName; }
    }

    public string ItemIdentifier
    {
      get { return string.Format("{0}.{1}.{2}", TabName, GroupName, ReportName); }
    }

    public int Index
    {
      get { return 0; }
    }

    #region bool CanExecute

    bool mCanExecute = true;
    public bool CanExecute
    {
      get { return mCanExecute; }
      set
      {
        if (mCanExecute != value)
        {
          mCanExecute = value;
          mExecuteCommand.RaiseCanExecuteChanged();
        }
      }
    }

    #endregion

    #region DelegateCommand ExecuteCommand

    DelegateCommand mExecuteCommand;
    public ICommand ExecuteCommand
    {
      get { return mExecuteCommand ?? (mExecuteCommand = new DelegateCommand(Execute, CanExecute_Internal)); }
    }

    bool CanExecute_Internal()
    {
      return CanExecute;
    }

    void Execute()
    {
      try
      {
        OnExecute();
      }
      catch (Exception ex)
      {
        if (ExceptionHelper.HandleException(ex)) throw;
      }
    }

    protected void OnExecute()
    {
      string fn = string.Format("{0}{1}.rpt", Path.GetTempPath(), Guid.NewGuid().ToString());

      ICrystalReportInfo cri = ReportInfo as ICrystalReportInfo;
      if (cri != null)
      {
        var bytes = cri.GetReport();
        File.WriteAllBytes(fn, bytes);
        ReportDocument boReportDocument = new ReportDocument();
        boReportDocument.Load(fn);
        UpdateReportDetails(boReportDocument);
        cri.SetParameters(boReportDocument);
      }


      IExcelReportInfo eri = ReportInfo as IExcelReportInfo;
      if (eri != null)
      {
        eri.SetParameters();
      }
    }

    void UpdateReportDetails(ReportDocument boReportDocument)
    {
      var boReportClientDocument = boReportDocument.ReportClientDocument;
      var boDatabaseController = boReportClientDocument.DatabaseController;
      var boDatabase = boDatabaseController.Database;
      var boTables = boDatabase.Tables;

      foreach (CrystalDecisions.ReportAppServer.DataDefModel.ISCRTable boTableOld in boTables)
      {
        CrystalDecisions.ReportAppServer.DataDefModel.ISCRTable boTableNew = boTableOld.Clone(true);

        var boConnectionInfo = boTableNew.ConnectionInfo;
        var boAttributesPropertyBag = (CrystalDecisions.ReportAppServer.DataDefModel.PropertyBag)boConnectionInfo.Attributes;

        string oldDb = boAttributesPropertyBag["QE_DatabaseName"];
        boAttributesPropertyBag["QE_DatabaseName"] = ReportingHelper.ReportCredentials.Database;
        boAttributesPropertyBag["QE_ServerDescription"] = ReportingHelper.ReportCredentials.Server;

        var boLogonPropertyBag = (CrystalDecisions.ReportAppServer.DataDefModel.PropertyBag)boAttributesPropertyBag["QE_LogonProperties"];
        boLogonPropertyBag["Data Source"] = ReportingHelper.ReportCredentials.Server;
        boLogonPropertyBag["Initial Catalog"] = ReportingHelper.ReportCredentials.Database;

        boTableNew.QualifiedName = string.Format("{0}{1}", ReportingHelper.ReportCredentials.Database, boTableNew.QualifiedName.Substring(oldDb.Length));

        boTableNew.ConnectionInfo.UserName = ReportingHelper.ReportCredentials.UserName;
        boTableNew.ConnectionInfo.Password = ReportingHelper.ReportCredentials.Password;

        boDatabaseController.SetTableLocation(boTableOld, boTableNew);
      }

      foreach (ReportDocument boSubReport in boReportDocument.Subreports)
      {
        UpdateSubReportDetails(boReportClientDocument.SubreportController.GetSubreport(boSubReport.Name));
      }
    }

    void UpdateSubReportDetails(SubreportClientDocument boReportClientDocument)
    {
      var boDatabaseController = boReportClientDocument.DatabaseController;
      var boDatabase = boDatabaseController.Database;
      var boTables = boDatabase.Tables;

      foreach (CrystalDecisions.ReportAppServer.DataDefModel.ISCRTable boTableOld in boTables)
      {
        CrystalDecisions.ReportAppServer.DataDefModel.ISCRTable boTableNew = boTableOld.Clone(true);

        var boConnectionInfo = boTableNew.ConnectionInfo;
        var boAttributesPropertyBag = (CrystalDecisions.ReportAppServer.DataDefModel.PropertyBag)boConnectionInfo.Attributes;

        string oldDb = boAttributesPropertyBag["QE_DatabaseName"];
        boAttributesPropertyBag["QE_DatabaseName"] = ReportingHelper.ReportCredentials.Database;
        boAttributesPropertyBag["QE_ServerDescription"] = ReportingHelper.ReportCredentials.Server;

        var boLogonPropertyBag = (CrystalDecisions.ReportAppServer.DataDefModel.PropertyBag)boAttributesPropertyBag["QE_LogonProperties"];
        boLogonPropertyBag["Data Source"] = ReportingHelper.ReportCredentials.Server;
        boLogonPropertyBag["Initial Catalog"] = ReportingHelper.ReportCredentials.Database;

        boTableNew.QualifiedName = string.Format("{0}{1}", ReportingHelper.ReportCredentials.Database, boTableNew.QualifiedName.Substring(oldDb.Length));

        boTableNew.ConnectionInfo.UserName = ReportingHelper.ReportCredentials.UserName;
        boTableNew.ConnectionInfo.Password = ReportingHelper.ReportCredentials.Password;

        boDatabaseController.SetTableLocation(boTableOld, boTableNew);
      }
    }

    #endregion
  }
}

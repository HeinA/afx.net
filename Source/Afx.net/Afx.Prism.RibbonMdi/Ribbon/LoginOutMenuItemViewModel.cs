﻿using Afx.Business.Security;
using Afx.Prism.Events;
using Microsoft.Practices.ObjectBuilder2;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.PubSubEvents;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Ribbon
{
  public class LoginOutMenuItemViewModel : FileMenuItemViewModel, IBuilderAware
  {
    [InjectionConstructor]
    public LoginOutMenuItemViewModel(IController controller)
      : base(controller)
    {
      Header = "Login";
    }


    IEventAggregator mEventAggregator;
    [Dependency]
    public IEventAggregator EventAggregator
    {
      get { return mEventAggregator; }
      set { mEventAggregator = value; }
    }

    public void OnBuiltUp(NamedTypeBuildKey buildKey)
    {
      this.EventAggregator.GetEvent<UserAuthenticatedEvent>().Subscribe(OnUserAuthenticated);
    }

    private void OnUserAuthenticated(EventArgs obj)
    {
      if (SecurityContext.User == null) Header = "Login";
      else Header = "Logout";
    }

    public void OnTearingDown()
    {
    }

    #region DelegateCommand LoginOutCommand

    DelegateCommand mLoginOutCommand;
    public DelegateCommand LoginOutCommand
    {
      get { return mLoginOutCommand ?? (mLoginOutCommand = new DelegateCommand(ExecuteLoginOut)); }
    }

    void ExecuteLoginOut()
    {
      if (SecurityContext.User == null) MdiApplicationController.Current.ShowLoginDialog();
      else MdiApplicationController.Current.Logout();
    }

    #endregion
  }
}

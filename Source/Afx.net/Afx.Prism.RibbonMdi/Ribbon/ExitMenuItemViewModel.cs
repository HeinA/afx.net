﻿using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Afx.Prism.RibbonMdi.Ribbon
{
  public class ExitMenuItemViewModel : FileMenuItemViewModel
  {
    [InjectionConstructor]
    public ExitMenuItemViewModel(IController controller)
      : base(controller)
    {
      Header = "Exit";
    }

    #region DelegateCommand ExitCommand

    DelegateCommand mExitCommand;
    public DelegateCommand ExitCommand
    {
      get { return mExitCommand ?? (mExitCommand = new DelegateCommand(ExecuteExit, CanExecuteExit)); }
    }

    bool CanExecuteExit()
    {
      return true;
    }

    void ExecuteExit()
    {
      Application.Current.MainWindow.Close();
    }

    #endregion

  }
}

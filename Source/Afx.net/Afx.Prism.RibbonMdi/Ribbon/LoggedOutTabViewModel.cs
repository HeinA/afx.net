﻿using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Mvvm;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Ribbon
{
  class LoggedOutTabViewModel : RibbonTabViewModel<NullContext>
  {
    [InjectionConstructor]
    public LoggedOutTabViewModel(IController controller)
      : base(controller)
    {
      Model = new NullContext(Guid.Parse("{80D1030D-E462-4E75-BCF9-DDF942CFE749}"));
    }

    [Dependency]
    public RibbonMdiModuleController ModuleController { get; set; }
    
    #region DelegateCommand LoginCommand

    DelegateCommand _loginCommand;
    public DelegateCommand LoginCommand
    {
      get { return _loginCommand ?? (_loginCommand = new DelegateCommand(ExecuteLogin)); }
    }

    void ExecuteLogin()
    {
      ModuleController.ShowLoginDialog();
    }

    #endregion
  }
}

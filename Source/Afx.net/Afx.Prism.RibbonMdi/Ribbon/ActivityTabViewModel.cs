﻿using Afx.Business.Activities;
using Afx.Business.Collections;
using Afx.Business.Data;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Ribbon
{
  public class ActivityTabViewModel : RibbonTabViewModel<ActivityTab>
  {
    [InjectionConstructor]
    public ActivityTabViewModel(IController controller)
      : base(controller)
    {
    }

    #region string Name

    public string Name
    {
      get
      {
        if (Model == null) return default(string);
        return Model.Name;
      }
      set { Model.Name = value; }
    }

    #endregion

    IEnumerable<ActivityGroupViewModel> mGroupViewModels;
    public IEnumerable<ActivityGroupViewModel> GroupViewModels
    {
      get
      {
        try
        {
          if (mGroupViewModels == null)
          {
            mGroupViewModels = ResolveViewModels<ActivityGroupViewModel, ActivityGroup>(Model.Groups);
          }
          return mGroupViewModels;
        }
        catch
        {
          throw;
        }
      }
    }
  }
}

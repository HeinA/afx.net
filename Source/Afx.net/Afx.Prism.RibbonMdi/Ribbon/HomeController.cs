﻿using Afx.Business.Security;
using Afx.Prism.RibbonMdi.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Ribbon
{
  class HomeController : MdiRibbonController<HomeView, HomeViewModel>
  {
    public HomeController(IController controller)
      : base("Home Controller", controller)
    {
    }

    protected override void Running()
    {
      RegionManager.Regions[MdiApplication.RibbonRegion].Activate(View);
      base.Running();
    }
  }
}

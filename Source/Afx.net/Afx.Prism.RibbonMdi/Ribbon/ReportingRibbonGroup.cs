﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Ribbon
{
  public class ReportingRibbonGroup : RibbonGroup
  {
    public ReportingRibbonGroup(string name)
    {
      mName = name;
    }

    public override string TabName
    {
      get { return ReportingTab.TabName; }
    }

    string mName;
    public override string Name
    {
      get { return mName; }
    }
  }
}

﻿using Afx.Business.Activities;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Ribbon
{
  public class ActivityViewModel : ViewModel<Activity>
  {
    [InjectionConstructor]
    public ActivityViewModel(IController controller)
      : base(controller)
    {
    }

    [Dependency]
    public IMdiApplicationController ApplicationController { get; set; }

    public string Name
    {
      get { return Model.Name; }
    }

    #region DelegateCommand ExecuteActivityCommand

    DelegateCommand mExecuteActivityCommand;
    public DelegateCommand ExecuteActivityCommand
    {
      get { return mExecuteActivityCommand ?? (mExecuteActivityCommand = new DelegateCommand(ExecuteActivity)); }
    }

    void ExecuteActivity()
    {
      ApplicationController.ExecuteActivity(this.Model);
    }

    #endregion

  }
}

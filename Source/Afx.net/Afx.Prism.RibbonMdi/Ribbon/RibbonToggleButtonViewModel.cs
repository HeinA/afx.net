﻿using Microsoft.Practices.Prism.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Afx.Prism.RibbonMdi.Ribbon
{
  public abstract class RibbonToggleButtonViewModel : RibbonItemViewModel
  {
    public abstract bool IsChecked { get; set; }
  }
}

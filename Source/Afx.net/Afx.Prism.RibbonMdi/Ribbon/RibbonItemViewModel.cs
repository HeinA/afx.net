﻿using Afx.Business.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Afx.Prism.RibbonMdi.Ribbon
{
  public abstract class RibbonItemViewModel : IRibbonItem, INotifyPropertyChanged
  {
    protected RibbonItemViewModel()
    {
      WeakEventManager<Cache, EventArgs>.AddHandler(Cache.Instance, "CacheUpdated", CacheUpdated);
    }

    void CacheUpdated(object sender, EventArgs e)
    {
      OnPropertyChanged(null);
    }

    RibbonGroup mRibbonGroup;
    internal RibbonGroup RibbonGroup
    {
      get { return mRibbonGroup; }
      set { mRibbonGroup = value; }
    }

    public abstract string TabName { get; }
    public abstract string GroupName { get; }
    public abstract string ItemIdentifier { get; }

    public virtual int Index
    {
      get { return 0; }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected virtual void OnPropertyChanged(string propertyName)
    {
      if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
    }
  }
}

﻿using Afx.Business;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Business.Service;
using Afx.Prism.RibbonMdi.Events;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Unity;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Ribbon
{
  public class SettingGroupMenuItemViewModel : FileMenuItemViewModel
  {
    #region Constructors

    [InjectionConstructor]
    public SettingGroupMenuItemViewModel(IController controller)
      : base(controller)
    {
      Header = "Settings";
    }

    #endregion

    #region bool IsSelected

    bool mIsSelected;
    public bool IsSelected
    {
      get { return mIsSelected; }
      set
      {
        if (SetProperty<bool>(ref mIsSelected, value))
        {
          using (new WaitCursor())
          {
            Setting.Refresh(true);
          }
        }
      }
    }

    #endregion

    #region IEnumerable<SettingGroupNodeViewModel> Groups

    BasicCollection<SettingGroupView> mGroups;
    BasicCollection<Setting> mSettings;
    public IEnumerable<SettingGroupNodeViewModel> Groups
    {
      get
      {
        if (mGroups == null)
        {
          mSettings = new BasicCollection<Setting>();
          BasicCollection<SettingGroupView> groups = new BasicCollection<SettingGroupView>();
          foreach (SettingGroupView sg in Setting.Groups)
          {
            SettingGroupView sgc = new SettingGroupView(sg.GlobalIdentifier, sg.Text);
            using (new EventStateSuppressor(sgc))
            { 
              sgc.Id = sg.Id;
              sgc.Text = sg.Text;
              sgc.IsNew = false;
              sgc.IsDirty = false;

              foreach (Setting s in sg.Settings)
              {
                Setting s1 = (Setting)s.Clone();
                sgc.mSettings.Add(s1);
                mSettings.Add(s1);
              }
            }

            groups.Add(sgc);
          }
          mGroups = groups;
        }

        return ResolveViewModels<SettingGroupNodeViewModel, SettingGroupView>(mGroups as IList);
      }
    }

    #endregion

    #region DelegateCommand ApplyCommand

    DelegateCommand mApplyCommand;
    public DelegateCommand ApplyCommand
    {
      get { return mApplyCommand ?? (mApplyCommand = new DelegateCommand(ExecuteApply, CanExecuteApply)); }
    }

    bool CanExecuteApply()
    {
      return true;
    }

    void ExecuteApply()
    {
      try
      {
        using(new WaitCursor())
        using (var svc = ProxyFactory.GetService<IAfxService>(SecurityContext.ServerName))
        {
          Setting.Refresh(svc.SaveSettings(mSettings, Environment.MachineName));
          Reset();
          MdiApplicationController.Current.EventAggregator.GetEvent<SettingsChangedEvent>().Publish(EventArgs.Empty);
        }
      }
      catch (Exception ex)
      {
        if (ExceptionHelper.HandleException(ex)) throw ex;
      }
    }

    #endregion

    #region DelegateCommand RevertCommand

    DelegateCommand mRevertCommand;
    public DelegateCommand RevertCommand
    {
      get { return mRevertCommand ?? (mRevertCommand = new DelegateCommand(ExecuteRevert, CanExecuteRevert)); }
    }

    bool CanExecuteRevert()
    {
      return true;
    }

    void ExecuteRevert()
    {
      Reset();
    }

    #endregion

    #region DelegateCommand ContextMenuOpeningCommand

    DelegateCommand mContextMenuOpeningCommand;
    public DelegateCommand ContextMenuOpeningCommand
    {
      get { return mContextMenuOpeningCommand ?? (mContextMenuOpeningCommand = new DelegateCommand(ExecuteContextMenuOpening, CanExecuteContextMenuOpening)); }
    }

    bool CanExecuteContextMenuOpening()
    {
      return true;
    }

    void ExecuteContextMenuOpening()
    {
    }

    #endregion

    #region void Reset()

    void Reset()
    {
      mGroups = null;
      OnPropertyChanged("Groups");
    }

    #endregion
  }
}

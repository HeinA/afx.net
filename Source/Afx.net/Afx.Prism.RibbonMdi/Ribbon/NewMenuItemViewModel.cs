﻿using Afx.Business;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Documents;
using Afx.Business.Security;
using Afx.Prism.RibbonMdi.Events;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Ribbon
{
  public class NewMenuItemViewModel : FileMenuItemViewModel, ISelectableDocumentType
  {
    [InjectionConstructor]
    public NewMenuItemViewModel(IController controller)
      : base(controller)
    {
      Header = "New";
    }

    BasicCollection<DocumentType> mDocumentTypes;
    BasicCollection<DocumentType> DocumentTypes
    {
      get { return mDocumentTypes ?? (mDocumentTypes = Cache.Instance.GetObjects<DocumentType>(dt => dt.Enabled && SecurityContext.User.HasRole(dt.InitialState.EditPermissions.ToArray()), dt => dt.Name, true)); }
    }

    public IEnumerable<NewDocumentTypeItemViewModel> DocumentTypeViewModels
    {
      get { return ResolveViewModels<NewDocumentTypeItemViewModel, DocumentType>(DocumentTypes); }
    }

    ViewModel<DocumentType> mSelectedViewModel;
    public ViewModel<DocumentType> SelectedViewModel
    {
      get { return mSelectedViewModel; }
      set { SetProperty<ViewModel<DocumentType>>(ref mSelectedViewModel, value); }
    }

    #region DelegateCommand NewDocumentCommand

    DelegateCommand mNewDocumentCommand;
    public DelegateCommand NewDocumentCommand
    {
      get { return mNewDocumentCommand ?? (mNewDocumentCommand = new DelegateCommand(ExecuteNewDocument)); }
    }

    void ExecuteNewDocument()
    {
      try
      {
        ApplicationController.Current.EventAggregator.GetEvent<EditDocumentEvent>().Publish(new EditDocumentEventArgs(SelectedViewModel.Model));
      }
      catch (Exception ex)
      {
        if (ExceptionHelper.HandleException(ex)) throw ex;
      }
    }

    #endregion

  }
}

﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Ribbon
{
  public abstract class RibbonGroup : IRibbonGroup
  {
    RibbonTab mRibbonTab;
    internal RibbonTab RibbonTab
    {
      get { return mRibbonTab; }
      set { mRibbonTab = value; }
    }

    public abstract string TabName { get; }
    public abstract string Name { get; }

    public virtual int Index
    {
      get { return 0; }
    }

    public virtual bool PlaceBehind
    {
      get { return false; }
    }

    #region Items

    ObservableCollection<IRibbonItem> mItems = new ObservableCollection<IRibbonItem>();
    public IEnumerable<IRibbonItem> Items
    {
      get { return mItems; }
    }

    public void AddItem(IRibbonItem item)
    {
      mItems.Add(item);
    }

    #endregion
  }
}

﻿using Afx.Business.Documents;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Ribbon
{
  public class RecentDocumentTypeItemViewModel : SelectableViewModel<DocumentType>
  {
    [InjectionConstructor]
    public RecentDocumentTypeItemViewModel(IController controller)
      : base(controller)
    {
    }

    new ISelectableDocumentType Parent
    {
      get { return (ISelectableDocumentType)base.Parent; }
    }

    #region string Name

    public string Name
    {
      get
      {
        if (Model == null) return default(string);
        return Model.Name;
      }
    }

    #endregion

    public override bool IsSelected
    {
      get { return base.IsSelected; }
      set
      {
        base.IsSelected = value;
        if (value) Parent.SelectedViewModel = this;
      }
    }
  }
}

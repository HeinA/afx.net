﻿using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Ribbon
{
  public class AboutMenuItemViewModel : FileMenuItemViewModel
  {
    [InjectionConstructor]
    public AboutMenuItemViewModel(IController controller)
      : base(controller)
    {
      Header = "About";
    }
  }
}

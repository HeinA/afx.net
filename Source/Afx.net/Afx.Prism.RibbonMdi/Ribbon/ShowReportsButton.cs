﻿using Afx.Business;
using Afx.Business.Activities;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Business.Service;
using Afx.Business.Settings;
using Afx.Prism.RibbonMdi;
using Afx.Prism.RibbonMdi.Ribbon;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Ribbon
{
  [Export(typeof(IRibbonItem))]
  public class ShowReportsButton : RibbonButtonViewModel
  {
    public override string TabName
    {
      get { return HomeTab.TabName; }
    }

    public override string GroupName
    {
      get { return ToolsGroup.GroupName; }
    }

    public override string ItemIdentifier
    {
      get { return "ShowReportsButtonHome"; }
    }

    public override int Index
    {
      get { return 1; }
    }

    protected override void OnExecute()
    {
      if (string.IsNullOrWhiteSpace(Setting.GetSetting<ReportingServerSetting>().URL)) return;
      System.Diagnostics.Process.Start(Setting.GetSetting<ReportingServerSetting>().URL);
    }
  }
}

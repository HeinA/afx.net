﻿using Afx.Business.Documents;
using Afx.Prism.RibbonMdi.Documents;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Ribbon
{
  public class RecentDocumentItemViewModel : SelectableViewModel<RecentDocumentBase>
  {
    [InjectionConstructor]
    public RecentDocumentItemViewModel(IController controller)
      : base(controller)
    {
    }

    #region string DocumentNumber

    public string DocumentNumber
    {
      get { return Model == null ? GetDefaultValue<string>() : Model.DocumentNumber; }
      set { Model.DocumentNumber = value; }
    }

    #endregion

    #region DateTime DocumentDate

    public DateTime DocumentDate
    {
      get { return Model == null ? GetDefaultValue<DateTime>() : Model.DocumentDate; }
      set { Model.DocumentDate = value; }
    }

    #endregion

    #region string DocumentState

    public string DocumentState
    {
      get { return Model == null ? GetDefaultValue<string>() : Model.DocumentState; }
      set { Model.DocumentState = value; }
    }

    #endregion

  }
}

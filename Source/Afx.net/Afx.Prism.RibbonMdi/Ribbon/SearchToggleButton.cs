﻿using Afx.Business.Activities;
using Afx.Business.Data;
using Afx.Prism.RibbonMdi.Ribbon;
using Afx.Prism.RibbonMdi.Tools;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Ribbon
{
  [Export(typeof(IRibbonItem))]
  public class SearchToggleButton : RibbonToggleButtonViewModel
  {
    public override string TabName
    {
      get { return HomeTab.TabName; }
    }

    public override string GroupName
    {
      get { return ToolsGroup.GroupName; }
    }

    public override string ItemIdentifier
    {
      get { return "Search"; }
    }

    public override int Index
    {
      get { return 0; }
    }

    SearchController mSearchController = null;
    [Dependency]
    public SearchController SearchController
    {
      get { return mSearchController; }
      set
      {
        mSearchController = value;
        mSearchController.ViewModel.PropertyChanged += ViewModel_PropertyChanged;
      }
    }

    void ViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
    {
      if (e.PropertyName == "IsVisible")
      {
        OnPropertyChanged("IsChecked");
      }
    }

    public override bool IsChecked
    {
      get { return SearchController.ViewModel.IsVisible; }
      set { SearchController.ViewModel.IsVisible = value; }
    }
  }
}

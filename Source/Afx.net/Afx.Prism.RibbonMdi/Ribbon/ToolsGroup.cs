﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Ribbon
{
  [Export(typeof(IRibbonGroup))]
  public class ToolsGroup : RibbonGroup
  {
    public const string GroupName = "Tools";

    public override string TabName
    {
      get { return HomeTab.TabName; }
    }

    public override string Name
    {
      get { return GroupName; }
    }

    public override int Index
    {
      get { return 0; }
    }
  }

  [Export(typeof(IRibbonGroup))]
  public class ToolsGroupAdministration : RibbonGroup
  {
    public const string GroupName = "Tools";

    public override string TabName
    {
      get { return AdministrationTab.TabName; }
    }

    public override string Name
    {
      get { return GroupName; }
    }

    public override int Index
    {
      get { return 0; }
    }
  }
}

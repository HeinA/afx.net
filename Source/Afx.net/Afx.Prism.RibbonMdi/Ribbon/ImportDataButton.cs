﻿using Afx.Business;
using Afx.Business.Activities;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Business.Service;
using Afx.Prism.RibbonMdi;
using Afx.Prism.RibbonMdi.Ribbon;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Ribbon
{
  [Export(typeof(IRibbonItem))]
  public class ImportDataButton : RibbonButtonViewModel
  {
    public override string TabName
    {
      get { return AdministrationTab.TabName; }
    }

    public override string GroupName
    {
      get { return ToolsGroup.GroupName; }
    }

    public override string ItemIdentifier
    {
      get { return "ImportApplicationSettingsButtonAdministration"; }
    }

    public override int Index
    {
      get { return 1; }
    }

    protected override void OnExecute()
    {
      OpenFileDialog ofd = new OpenFileDialog();
      ofd.Filter = "Xml File (.xml)|*.xml";
      ofd.DefaultExt = "xml";

      if (ofd.ShowDialog() == false) return;

      using (var svc = ProxyFactory.GetService<IAfxService>(SecurityContext.ServerName))
      {
        BasicCollection<BusinessObject> col = null;

        using (FileStream sw = new FileStream(ofd.FileName, FileMode.Open))
        {
          NetDataContractSerializer dcs = new NetDataContractSerializer();
          col = (BasicCollection<BusinessObject>)dcs.ReadObject(sw);
        }

        svc.Import(col);
      }
    }
  }
}

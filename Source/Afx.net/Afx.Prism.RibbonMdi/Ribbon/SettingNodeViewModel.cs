﻿using Afx.Business;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Ribbon
{
  public class SettingNodeViewModel : MdiNavigationTreeNodeViewModel<Setting>
  {
    #region Constructors

    [InjectionConstructor]
    public SettingNodeViewModel(IController controller)
      : base(controller)
    {
    }

    #endregion

    #region string Name

    public string Name
    {
      get { return Model == null ? GetDefaultValue<string>() : Model.Name; }
    }

    #endregion

    #region bool IsSelected

    public override bool IsSelected
    {
      get
      {
        if (base.IsSelected) ((MdiApplicationController)Controller).ShowSettingDetailView(this.Model);
        return base.IsSelected;
      }
      set
      {
        base.IsSelected = value;
        if (base.IsSelected) ((MdiApplicationController)Controller).ShowSettingDetailView(this.Model);
      }
    }

    #endregion
  }
}

﻿using Afx.Business.Security;
using Afx.Prism.RibbonMdi.Events;
using Afx.Prism.RibbonMdi.Login;
using Microsoft.Practices.Prism.PubSubEvents;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Ribbon
{
  class LoggedOutController : MdiRibbonController<LoggedOutView, LoggedOutViewModel>
  {
    public LoggedOutController(IController controller)
      : base("Logged Out Controller", controller)
    {
    }

    protected override void OnRun()
    {
      ShowLoginDialog();
      base.OnRun();
    }

    protected override void Running()
    {
      RegionManager.Regions[MdiApplication.RibbonRegion].Activate(View);
      base.Running();
    }

    public void ShowLoginDialog()
    {
      CreateChildController<LoginController>().Run();
    }

    protected override void OnUserAuthenticated(EventArgs obj)
    {
      if (SecurityContext.User != null) Terminate();
    }
  }
}

﻿using Afx.Business;
using Microsoft.Practices.Prism.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Afx.Prism.RibbonMdi.Ribbon
{
  public abstract class RibbonButtonViewModel : RibbonItemViewModel
  {
    #region bool CanExecute

    bool mCanExecute = true;
    public bool CanExecute
    {
      get { return mCanExecute; }
      set
      {
        if (mCanExecute != value)
        {
          mCanExecute = value;
          mExecuteCommand.RaiseCanExecuteChanged();
        }
      }
    }

    #endregion

    #region DelegateCommand ExecuteCommand

    DelegateCommand mExecuteCommand;
    public ICommand ExecuteCommand
    {
      get { return mExecuteCommand ?? (mExecuteCommand = new DelegateCommand(Execute, CanExecute_Internal)); }
    }

    bool CanExecute_Internal()
    {
      return CanExecute;
    }

    void Execute()
    {
      try
      {
        OnExecute();
      }
      catch (Exception ex)
      {
        if (ExceptionHelper.HandleException(ex)) throw;
      }
    }

    protected abstract void OnExecute();

    #endregion
  }
}

﻿using Afx.Business;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Documents;
using Afx.Prism.RibbonMdi.Documents;
using Afx.Prism.RibbonMdi.Events;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Ribbon
{
  public class RecentMenuItemViewModel : FileMenuItemViewModel, ISelectableDocumentType
  {
    public const string DetailsRegion = "Afx.Prism.RibbonMdi.Ribbon.RecentMenuItemViewModel.DetailsRegion";

    [InjectionConstructor]
    public RecentMenuItemViewModel(IController controller)
      : base(controller)
    {
      Header = "Recent";

      MdiApplicationController.Current.EventAggregator.GetEvent<DocumentSavedEvent>().Subscribe(OnDocumentSaved);
    }

    private void OnDocumentSaved(DocumentSavedEventArgs obj)
    {
      RefreshRecent();
    }

    #region IRegionManager RegionManager

    public IRegionManager RegionManager
    {
      get { return MdiApplicationController.Current.RegionManager; }
    }

    #endregion

    BasicCollection<DocumentType> mDocumentTypes;
    BasicCollection<DocumentType> DocumentTypes
    {
      get { return mDocumentTypes ?? (mDocumentTypes = Cache.Instance.GetObjects<DocumentType>(dt => dt.Enabled && dt.States.Count(dts => dts.IsInRecentList) > 0 , dt => dt.Name, true)); }
    }

    public IEnumerable<RecentDocumentTypeItemViewModel> DocumentTypeViewModels
    {
      get { return ResolveViewModels<RecentDocumentTypeItemViewModel, DocumentType>(DocumentTypes); }
    }

    ViewModel<DocumentType> mSelectedViewModel;
    public ViewModel<DocumentType> SelectedViewModel
    {
      get { return mSelectedViewModel ?? (SelectedViewModel = DocumentTypeViewModels.FirstOrDefault()); }
      set
      {
        if (SetProperty<ViewModel<DocumentType>>(ref mSelectedViewModel, value))
        {
          RefreshRecent();
        }
      }
    }

    void RefreshRecent()
    {
      if (SelectedViewModel == null) return;
      IRecentDocumentsProvider rdp = ExtensibilityManager.CompositionContainer.GetExportedValues<IRecentDocumentsProvider>().FirstOrDefault(rdp1 => rdp1.DocumentType == SelectedViewModel.Model.GlobalIdentifier);

      Collection<RecentDocumentBase> rds = null;
      BackgroundWorker bw = new BackgroundWorker();
      bw.DoWork += (s, e) =>
      {
        rds = rdp.GetRecentDocuments();
      };
      bw.RunWorkerCompleted += (s, e) =>
      {
        if (e.Error != null)
        {
          if (ExceptionHelper.HandleException(e.Error)) throw e.Error;
        }
        else
        {
          if (rdp.DocumentType == SelectedViewModel.Model.GlobalIdentifier)
          {
            RecentDocuments = rds;
          }
        }
        bw.Dispose();
      };
      bw.RunWorkerAsync();
    }

    #region bool ShowDocumentTypeList

    #region IEnumerable<RecentDocumentBase> RecentDocuments

    Collection<RecentDocumentBase> mRecentDocuments;
    Collection<RecentDocumentBase> RecentDocuments
    {
      get { return mRecentDocuments; }
      set
      {
        if (SetProperty<Collection<RecentDocumentBase>>(ref mRecentDocuments, value))
        {
          OnPropertyChanged("RecentDocumentViewModels");
        }
      }
    }

    #endregion

    public IEnumerable<RecentDocumentItemViewModel> RecentDocumentViewModels
    {
      get
      {
        try
        {
          if (RecentDocuments == null) return null;
          return ResolveViewModels<RecentDocumentItemViewModel, RecentDocumentBase>(RecentDocuments);
        }
        catch
        {
          throw;
        }
      }
    }

    bool mShowDocumentTypeList = true;
    public bool ShowDocumentTypeList
    {
      get { return mShowDocumentTypeList; }
      set { SetProperty<bool>(ref mShowDocumentTypeList, value); }
    }

    #endregion

    #region bool IsSelected

    bool mIsSelected;
    public bool IsSelected
    {
      get { return mIsSelected; }
      set
      {
        if (SetProperty<bool>(ref mIsSelected, value))
        {
          RefreshRecent();
        }
      }
    }

    #endregion

    #region RecentDocumentItemViewModel SelectedDocument

    RecentDocumentItemViewModel mSelectedDocument;
    public RecentDocumentItemViewModel SelectedDocument
    {
      get { return mSelectedDocument; }
      set
      {
        if (SetProperty<RecentDocumentItemViewModel>(ref mSelectedDocument, value))
        {
          IRegion region = RegionManager.Regions[DetailsRegion];
          Stack<object> remove = new Stack<object>();
          foreach(object o in region.Views)
          {
            remove.Push(o);
          }
          while(remove.Count > 0)
          {
            region.Remove(remove.Pop());
          }
          region.Add(SelectedDocument.Model.Provider.GetDetailViewModel(SelectedDocument.Model, this));
        }
      }
    }

    #endregion

    #region DelegateCommand SelectDocumentCommand

    DelegateCommand mSelectDocumentCommand;
    public DelegateCommand SelectDocumentCommand
    {
      get { return mSelectDocumentCommand ?? (mSelectDocumentCommand = new DelegateCommand(ExecuteSelectDocument, CanExecuteSelectDocument)); }
    }

    bool CanExecuteSelectDocument()
    {
      return true;
    }

    void ExecuteSelectDocument()
    {
      ApplicationController.Current.EventAggregator.GetEvent<EditDocumentEvent>().Publish(new EditDocumentEventArgs(SelectedViewModel.Model, SelectedDocument.Model.GlobalIdentifier));
    }

    #endregion

    #region DelegateCommand RecentDocumentCommand

    DelegateCommand mRecentDocumentCommand;
    public DelegateCommand RecentDocumentCommand
    {
      get { return mRecentDocumentCommand ?? (mRecentDocumentCommand = new DelegateCommand(ExecuteRecentDocument)); }
    }

    void ExecuteRecentDocument()
    {
      ShowDocumentTypeList = false;
    }

    #endregion
  }
}

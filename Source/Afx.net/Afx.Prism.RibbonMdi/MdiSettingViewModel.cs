﻿using Afx.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi
{
  public class MdiSettingViewModel<TModel> : ViewModel<TModel>
    where TModel : Setting
  {
    protected MdiSettingViewModel(IController controller)
      : base(controller)
    {
    }
  }
}

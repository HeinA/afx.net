﻿using Afx.Business.Activities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Afx.Prism.RibbonMdi
{
  public class MdiOperationSeperatorViewModel : MdiOperationViewModel
  {
    public MdiOperationSeperatorViewModel(IController controller)
      : base(controller)
    {
      Model = new OperationSeperator(true);
    }
  }
}

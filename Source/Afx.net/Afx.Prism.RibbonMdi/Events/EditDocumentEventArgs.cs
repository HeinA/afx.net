﻿using Afx.Business.Documents;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Events
{
  public class EditDocumentEventArgs : EventArgs
  {
    public EditDocumentEventArgs(Document document)
    {
      Document = document;
    } 

    public EditDocumentEventArgs(DocumentType documentType)
    {
      DocumentType = documentType;
    }

    public EditDocumentEventArgs(DocumentType documentType, Guid documentIdentifier)
    {
      DocumentType = documentType;
      DocumentIdentifier = documentIdentifier;
    }

    #region Document Document

    Document mDocument;
    public Document Document
    {
      get { return mDocument; }
      private set { mDocument = value; }
    }

    #endregion

    DocumentType mDocumentType;
    public DocumentType DocumentType
    {
      get { return mDocumentType; }
      private set { mDocumentType = value; }
    }

    Guid mDocumentIdentifier = Guid.Empty;
    public Guid DocumentIdentifier
    {
      get { return mDocumentIdentifier; }
      private set { mDocumentIdentifier = value; }
    }
  }
}

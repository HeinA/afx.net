﻿using Afx.Business.Documents;
using Afx.Prism.RibbonMdi.Documents;
using Microsoft.Practices.Prism.Regions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Events
{
  public class ExtendDocumentEventArgs : EventArgs
  {
    public ExtendDocumentEventArgs(IMdiDocumentController controller, Document document)
    {
      mController = controller;
      mDocument = document;
    }

    #region IMdiDocumentController Controller

    IMdiDocumentController mController;
    public IMdiDocumentController Controller
    {
      get { return mController; }
    }

    #endregion

    #region Document Document

    Document mDocument;
    public Document Document
    {
      get { return mDocument; }
    }

    #endregion
  }
}

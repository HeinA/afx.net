﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Events
{
  public class DisplayOutputEventArgs : EventArgs
  {
    public DisplayOutputEventArgs(string text)
    {
      Text = text;
    }

    public DisplayOutputEventArgs(string text, bool clearOutput)
    {
      Text = text;
      ClearOutput = clearOutput;
    }

    #region string Text

    string mText;
    public string Text
    {
      get { return mText; }
      private set { mText = value; }
    }

    #endregion

    #region bool ClearOutput

    public const string ClearOutputProperty = "ClearOutput";
    bool mClearOutput;
    public bool ClearOutput
    {
      get { return mClearOutput; }
      set { mClearOutput = value; }
    }

    #endregion
  }
}

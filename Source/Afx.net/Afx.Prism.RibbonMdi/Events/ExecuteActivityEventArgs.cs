﻿using Afx.Business.Activities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Events
{
  public class ExecuteActivityEventArgs : EventArgs
  {
    public ExecuteActivityEventArgs(Activity activity)
    {
      mActivity = activity;
    }

    public ExecuteActivityEventArgs(Activity activity, Guid contextId)
    {
      Activity = activity;
      ContextId = contextId;
    }

    Activity mActivity;
    public Activity Activity
    {
      get { return mActivity; }
      private set { mActivity = value; }
    }

    Guid mContextId = Guid.Empty;

    public Guid ContextId
    {
      get { return mContextId; }
      private set { mContextId = value; }
    }
  }
}

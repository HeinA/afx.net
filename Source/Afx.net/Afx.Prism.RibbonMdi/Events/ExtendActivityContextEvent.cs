﻿using Microsoft.Practices.Prism.PubSubEvents;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Events
{
  public class ExtendActivityContextEvent : PubSubEvent<ExtendActivityContextEventArgs>
  {
  }
}

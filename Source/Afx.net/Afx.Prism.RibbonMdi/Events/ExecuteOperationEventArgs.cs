﻿using Afx.Business;
using Afx.Prism.RibbonMdi.Documents;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Events
{
  public class ExecuteOperationEventArgs : EventArgs
  {
    public ExecuteOperationEventArgs(IMdiDockableDocumentController controller, Operation operation, BusinessObject argument)
    {
      mController = controller;
      mOperation = operation;
      Argument = argument;
    }

    public BusinessObject Argument { get; private set; }

    #region IMdiDocumentController Controller

    IMdiDockableDocumentController mController;
    public IMdiDockableDocumentController Controller
    {
      get { return mController; }
    }

    #endregion

    #region Operation Operation

    Operation mOperation;
    public Operation Operation
    {
      get { return mOperation; }
      set { mOperation = value; }
    }

    #endregion
  }
}

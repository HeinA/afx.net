﻿using Afx.Business.Documents;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Events
{
  public class DocumentSavedEventArgs : EventArgs
  {
    public DocumentSavedEventArgs(Document document)
    {
      Document = document;
    }

    #region Document Document

    Document mDocument;
    public Document Document
    {
      get { return mDocument; }
      private set { mDocument = value; }
    }

    #endregion
  }
}

﻿using Afx.Business;
using Microsoft.Practices.Prism.Regions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi.Events
{
  public class ContextChangedEventArgs : EventArgs
  {
    public ContextChangedEventArgs(IMdiDockableDocumentController controller, BusinessObject context, IRegionManager regionManager)
    {
      Controller = controller;
      Context = context;
      RegionManager = regionManager;
    }

    IMdiDockableDocumentController mController;
    public IMdiDockableDocumentController Controller
    {
      get { return mController; }
      private set { mController = value; }
    }

    BusinessObject mContext;
    public BusinessObject Context
    {
      get { return mContext; }
      private set { mContext = value; }
    }

    IRegionManager mRegionManager;
    public IRegionManager RegionManager
    {
      get { return mRegionManager; }
      private set { mRegionManager = value; }
    }
  }
}

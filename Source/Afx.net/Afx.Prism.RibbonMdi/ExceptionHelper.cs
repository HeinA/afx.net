﻿using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi
{
  public static class ExceptionHelper
  {
    public static bool HandleException(Exception ex)
    {
      return ExceptionPolicy.HandleException(ex, "Default Policy");
    }
  }
}

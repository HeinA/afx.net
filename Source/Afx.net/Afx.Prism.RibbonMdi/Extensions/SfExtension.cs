﻿using Afx.Business;
using Syncfusion.SfSkinManager;
using Syncfusion.UI.Xaml.Grid;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Threading;

namespace Afx.Prism.RibbonMdi.Extensions
{
  public static class SfExtension
  {
    public static bool GetHookAfx(DependencyObject obj)
    {
      return (bool)obj.GetValue(HookAfxProperty);
    }

    public static void SetHookAfx(DependencyObject obj, bool value)
    {
      obj.SetValue(HookAfxProperty, value);
    }

    public static readonly DependencyProperty HookAfxProperty = DependencyProperty.RegisterAttached("HookAfx", typeof(bool), typeof(SfExtension), new FrameworkPropertyMetadata(false, OnHookAfxPropertyChanged));

    private static void OnHookAfxPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
      SfDataGrid dg = d as SfDataGrid;
      if (dg != null && (bool)e.NewValue)
      {
        var o = new DGManager(dg, false);
      }
    }

    public static bool GetHookAfxReadOnly(DependencyObject obj)
    {
      return (bool)obj.GetValue(HookAfxReadOnlyProperty);
    }

    public static void SetHookAfxReadOnly(DependencyObject obj, bool value)
    {
      obj.SetValue(HookAfxReadOnlyProperty, value);
    }

    public static readonly DependencyProperty HookAfxReadOnlyProperty = DependencyProperty.RegisterAttached("HookAfxReadOnly", typeof(bool), typeof(SfExtension), new FrameworkPropertyMetadata(false, OnHookAfxReadOnlyPropertyChanged));

    private static void OnHookAfxReadOnlyPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
      SfDataGrid dg = d as SfDataGrid;
      if (dg != null && (bool)e.NewValue)
      {
        var o = new DGManager(dg, true);
      }
    }



    class DGManager
    {
      public DGManager(SfDataGrid dataGrid, bool readOnly)
      {
        DataGrid = dataGrid;
        DataGrid.ItemsSourceChanged += DataGrid_ItemsSourceChanged;

        SfSkinManager.SetVisualStyle(DataGrid, VisualStyles.Office2013White);

        DataGrid.SelectionMode = GridSelectionMode.Single;
        DataGrid.AllowTriStateSorting = true;
        DataGrid.AutoGenerateColumns = false;
        DataGrid.ShowRowHeader = true;
        DataGrid.IsTabStop = true;

        if (!readOnly)
        {
          DataGrid.AllowEditing = true;
          DataGrid.AddNewRowPosition = AddNewRowPosition.Bottom;
          DataGrid.AllowDeleting = true;
          DataGrid.RecordDeleting += DataGrid_RecordDeleting;
        }
        else
        {
          DataGrid.AllowEditing = false;
          DataGrid.AddNewRowPosition = AddNewRowPosition.None;
          DataGrid.AllowDeleting = false;
          DataGrid.RecordDeleting -= DataGrid_RecordDeleting;
        }
      }

      void DataGrid_ItemsSourceChanged(object sender, GridItemsSourceChangedEventArgs e)
      {
        if (DataGrid.View != null)
        {
          DataGrid.View.Filter = Filter;
          DataGrid.View.RefreshFilter();
        }
      }

      void DataGrid_RecordDeleting(object sender, RecordDeletingEventArgs args)
      {
        foreach (BusinessObject bo in args.Items)
        {
          bo.IsDeleted = true;
        }

        DataGrid.View.RefreshFilter();
        args.Cancel = true;
      }

      #region SfDataGrid DataGrid

      SfDataGrid mDataGrid;
      public SfDataGrid DataGrid
      {
        get { return mDataGrid; }
        private set { mDataGrid = value; }
      }

      #endregion

      bool Filter(object obj)
      {
        return !((BusinessObject)obj).IsDeleted;
      }
    }
  }
}

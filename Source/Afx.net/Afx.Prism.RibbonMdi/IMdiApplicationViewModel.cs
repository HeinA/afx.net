﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Afx.Prism.RibbonMdi
{
  public interface IMdiApplicationViewModel
  {
    bool IsModal { get; }
    bool IsRibbonEnabled { get; }
  }
}

﻿using Afx.Business;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi
{
  public abstract class MdiDialogController<TModel, TViewModel> : MdiViewController<TModel, TViewModel>, IMdiDialogController
    where TModel : class, IBusinessObject
    where TViewModel : class, IMdiDialogViewModel
  {
    protected MdiDialogController(IController controller)
      : base(controller)
    {
    }

    protected MdiDialogController(string id, IController controller)
      : base(id, controller)
    {
    }

    protected override void ConfigureContainer()
    {
      Container.RegisterInstance<IMdiDialogController>(this);
      base.ConfigureContainer();
    }

    public virtual bool Validate()
    {
      if (DataContext == null) return true;
      return ViewModel.Validate();
    }

    public void OnApply()
    {
      try
      {
        if (Validate())
        {
          using (new WaitCursor())
          {
            ApplyChanges();
          }
        }
      }
      catch (Exception ex)
      {
        if (ExceptionHelper.HandleException(ex)) throw ex;
      }
    }

    #region bool CanClose

    bool mCanClose = true;
    public bool CanClose
    {
      get { return mCanClose; }
      set { mCanClose = value; }
    }
    
    #endregion

    public void OnOk()
    {
      try
      {
        if (Validate())
        {
          using (new WaitCursor())
          {
            ApplyChanges();
          }
          if (CanClose) Terminate();
        }
      }
      catch (Exception ex)
      {
        if (ExceptionHelper.HandleException(ex)) throw ex;
      }
    }

    protected virtual void ApplyChanges()
    {
    }

    protected override bool OnRun()
    {
      ApplicationController.AddDialog(this);
      ApplicationController.RegionManager.Regions[MdiApplication.DialogRegion].Add(ViewModel);
      return base.OnRun();
    }

    public override void Terminate()
    {
      IRegion region = ApplicationController.RegionManager.Regions[MdiApplication.DialogRegion];
      if (ViewModel != null && region.Views.Contains(ViewModel)) region.Remove(ViewModel);
      ApplicationController.RemoveDialog(this);

      base.Terminate();
    }
  }
}

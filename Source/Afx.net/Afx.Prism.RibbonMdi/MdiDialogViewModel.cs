﻿using Afx.Business;
using Afx.Prism.RibbonMdi.Tools.Keyboard;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Mvvm;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using WindowsInput;
using Xceed.Wpf.Toolkit;

namespace Afx.Prism.RibbonMdi
{
  public class MdiDialogViewModel<TModel> : ViewModel<TModel>, IMdiDialogViewModel
    where TModel : class, IBusinessObject
  {
    protected MdiDialogViewModel(IController controller)
      : base(controller)
    {
    }

    #region IMdiDialogController Controller

    private IMdiDialogController mDialogController;
    [Dependency]
    public IMdiDialogController DialogController
    {
      get { return mDialogController; }
      set { mDialogController = value; }
    }

    #endregion

    #region string Caption

    string mCaption;
    public string Caption
    {
      get { return mCaption; }
      set { SetProperty<string>(ref mCaption, value); }
    }

    #endregion

    protected virtual bool IsKeyboardAvailable
    {
      get { return false; }
    }

    public Visibility KeyboardVisibility
    {
      get
      {
        if (IsKeyboardAvailable && MdiApplicationController.Current.ApplicationContext.IsKeyboardVisible) return Visibility.Visible;
        return Visibility.Collapsed;
      }
    }

    #region WindowState WindowState

    Xceed.Wpf.Toolkit.WindowState mWindowState = Xceed.Wpf.Toolkit.WindowState.Open;
    public Xceed.Wpf.Toolkit.WindowState WindowState
    {
      get { return mWindowState; }
      set
      {
        if (SetProperty<Xceed.Wpf.Toolkit.WindowState>(ref mWindowState, value))
        {
          if (WindowState == Xceed.Wpf.Toolkit.WindowState.Closed) Controller.Terminate();
        }
      }
    }

    #endregion

    #region DelegateCommand ApplyCommand

    DelegateCommand mApplyCommand;
    public DelegateCommand ApplyCommand
    {
      get { return mApplyCommand ?? (mApplyCommand = new DelegateCommand(ExecuteApply, CanExecuteApply)); }
    }

    protected virtual bool CanExecuteApply()
    {
      if (Model == null) return true;
      return Model.IsValid;
    }

    protected virtual void ExecuteApply()
    {
      DialogController.OnApply();
    }

    #endregion

    #region DelegateCommand OkCommand

    DelegateCommand mOkCommand;
    public DelegateCommand OkCommand
    {
      get { return mOkCommand ?? (mOkCommand = new DelegateCommand(ExecuteOk)); }
    }

    protected virtual void ExecuteOk()
    {
      DialogController.OnOk();
    }

    #endregion

    #region DelegateCommand CancelCommand

    DelegateCommand mCancelCommand;
    public DelegateCommand CancelCommand
    {
      get { return mCancelCommand ?? (mCancelCommand = new DelegateCommand(ExecuteCancel)); }
    }

    void ExecuteCancel()
    {
      WindowState = Xceed.Wpf.Toolkit.WindowState.Closed;
    }

    #endregion


    protected override void OnPropertyChanged(string propertyName)
    {
      switch (propertyName)
      {
        case "Error":
          OnPropertyChanged("ErrorVisibility");
          break;
      }

      base.OnPropertyChanged(propertyName);
    }

    //Prevent Validation on Model Composition changed
    protected override void OnModelCompositionChanged(CompositionChangedEventArgs e)
    {
    }

    #region Keyboard

    InputSimulator mInput = new InputSimulator();

    #region bool IsCaps

    bool mIsCaps;
    public bool IsCaps
    {
      get { return mIsCaps; }
      set { SetProperty<bool>(ref mIsCaps, value); }
    }

    #endregion

    #region bool IsShift

    bool mIsShift;
    public bool IsShift
    {
      get { return mIsShift; }
      set { SetProperty<bool>(ref mIsShift, value); }
    }

    #endregion

    #region DelegateCommand<string> KeyPressCommand

    DelegateCommand<string> mKeyPressCommand;
    public DelegateCommand<string> KeyPressCommand
    {
      get { return mKeyPressCommand ?? (mKeyPressCommand = new DelegateCommand<string>(ExecuteKeyPress, CanExecuteKeyPress)); }
    }

    bool CanExecuteKeyPress(string args)
    {
      return true;
    }

    void ExecuteKeyPress(string args)
    {
      switch (args)
      {
        case "A":
          SendKey(WindowsInput.Native.VirtualKeyCode.VK_A);
          break;
        case "B":
          SendKey(WindowsInput.Native.VirtualKeyCode.VK_B);
          break;
        case "C":
          SendKey(WindowsInput.Native.VirtualKeyCode.VK_C);
          break;
        case "D":
          SendKey(WindowsInput.Native.VirtualKeyCode.VK_D);
          break;
        case "E":
          SendKey(WindowsInput.Native.VirtualKeyCode.VK_E);
          break;
        case "F":
          SendKey(WindowsInput.Native.VirtualKeyCode.VK_F);
          break;
        case "G":
          SendKey(WindowsInput.Native.VirtualKeyCode.VK_G);
          break;
        case "H":
          SendKey(WindowsInput.Native.VirtualKeyCode.VK_H);
          break;
        case "I":
          SendKey(WindowsInput.Native.VirtualKeyCode.VK_I);
          break;
        case "J":
          SendKey(WindowsInput.Native.VirtualKeyCode.VK_J);
          break;
        case "K":
          SendKey(WindowsInput.Native.VirtualKeyCode.VK_K);
          break;
        case "L":
          SendKey(WindowsInput.Native.VirtualKeyCode.VK_L);
          break;
        case "M":
          SendKey(WindowsInput.Native.VirtualKeyCode.VK_M);
          break;
        case "N":
          SendKey(WindowsInput.Native.VirtualKeyCode.VK_N);
          break;
        case "O":
          SendKey(WindowsInput.Native.VirtualKeyCode.VK_O);
          break;
        case "P":
          SendKey(WindowsInput.Native.VirtualKeyCode.VK_P);
          break;
        case "Q":
          SendKey(WindowsInput.Native.VirtualKeyCode.VK_Q);
          break;
        case "R":
          SendKey(WindowsInput.Native.VirtualKeyCode.VK_R);
          break;
        case "S":
          SendKey(WindowsInput.Native.VirtualKeyCode.VK_S);
          break;
        case "T":
          SendKey(WindowsInput.Native.VirtualKeyCode.VK_T);
          break;
        case "U":
          SendKey(WindowsInput.Native.VirtualKeyCode.VK_U);
          break;
        case "V":
          SendKey(WindowsInput.Native.VirtualKeyCode.VK_V);
          break;
        case "W":
          SendKey(WindowsInput.Native.VirtualKeyCode.VK_W);
          break;
        case "X":
          SendKey(WindowsInput.Native.VirtualKeyCode.VK_X);
          break;
        case "Y":
          SendKey(WindowsInput.Native.VirtualKeyCode.VK_Y);
          break;
        case "Z":
          SendKey(WindowsInput.Native.VirtualKeyCode.VK_Z);
          break;

        case "0":
          SendKey(WindowsInput.Native.VirtualKeyCode.VK_0);
          break;
        case "1":
          SendKey(WindowsInput.Native.VirtualKeyCode.VK_1);
          break;
        case "2":
          SendKey(WindowsInput.Native.VirtualKeyCode.VK_2);
          break;
        case "3":
          SendKey(WindowsInput.Native.VirtualKeyCode.VK_3);
          break;
        case "4":
          SendKey(WindowsInput.Native.VirtualKeyCode.VK_4);
          break;
        case "5":
          SendKey(WindowsInput.Native.VirtualKeyCode.VK_5);
          break;
        case "6":
          SendKey(WindowsInput.Native.VirtualKeyCode.VK_6);
          break;
        case "7":
          SendKey(WindowsInput.Native.VirtualKeyCode.VK_7);
          break;
        case "8":
          SendKey(WindowsInput.Native.VirtualKeyCode.VK_8);
          break;
        case "9":
          SendKey(WindowsInput.Native.VirtualKeyCode.VK_9);
          break;

        case "Caps":
          SendKey(WindowsInput.Native.VirtualKeyCode.CAPITAL);
          break;
        case "Backspace":
          SendKey(WindowsInput.Native.VirtualKeyCode.BACK);
          break;
        case "Tab":
          SendKey(WindowsInput.Native.VirtualKeyCode.TAB);
          break;
        case "Enter":
          SendKey(WindowsInput.Native.VirtualKeyCode.RETURN);
          break;
        case "Space":
          SendKey(WindowsInput.Native.VirtualKeyCode.SPACE);
          break;
      }
    }

    private void SendKey(WindowsInput.Native.VirtualKeyCode virtualKeyCode)
    {
      if (IsShift)
      {
        mInput.Keyboard.ModifiedKeyStroke(WindowsInput.Native.VirtualKeyCode.SHIFT, virtualKeyCode);
        IsShift = false;
      }
      else
      {
        mInput.Keyboard.KeyDown(virtualKeyCode);
      }
    }

    #endregion


    #endregion
  }
}

﻿using Afx.Business;
using Afx.Business.Security;
using Afx.Prism.RibbonMdi.Events;
using Fluent;
using Microsoft.Practices.ObjectBuilder2;
using Microsoft.Practices.Prism.PubSubEvents;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Resources;
using System.Windows.Shapes;
using Xceed.Wpf.AvalonDock.Layout.Serialization;

namespace Afx.Prism.RibbonMdi
{
  /// <summary>
  /// Interaction logic for RibbonMdiWindow.xaml
  /// </summary>
  public partial class MdiApplicationView : RibbonWindow, IApplicationView, IBuilderAware
  {
    public MdiApplicationView()
    {
      InitializeComponent();
      Closed += (s, e) =>
      {
        if (SecurityContext.User != null) SaveLayout(null);
      };
    }

    [Dependency]
    public IMdiApplicationViewModel ViewModel
    {
      get { return (IMdiApplicationViewModel)DataContext; }
      set { DataContext = value; }
    }

    IEventAggregator mEventAggregator;
    [Dependency]
    public IEventAggregator EventAggregator
    {
      get { return mEventAggregator; }
      set { mEventAggregator = value; }
    }

    public void LoadLayout(object parameter)
    {
      var layoutSerializer = new XmlLayoutSerializer(dockingManager);
      layoutSerializer.LayoutSerializationCallback += (s, e) =>
      {
        if (e.Content == null || typeof(IInternalMdiDockableViewModel).IsAssignableFrom(e.Content.GetType()))
          e.Cancel = true;
      };
      string folder = Environment.GetFolderPath(System.Environment.SpecialFolder.ApplicationData);
      string file = string.Format(@"{0}\Afx.net\{1}.layout.config", folder, Process.GetCurrentProcess().ProcessName); 
      FileInfo fi = new FileInfo(file);
      if (!fi.Exists)
      {
        try
        {
          Stream s = MdiBootstrapper.Current.GetDefaultDockingLayout();
          if (s != null) layoutSerializer.Deserialize(s);
        }
        catch
        {
          throw;
        }
      }
      else
      {
        try
        {
          layoutSerializer.Deserialize(file);
        }
        catch { }
      }
    }

    private void SaveLayout(object parameter)
    {
      var layoutSerializer = new XmlLayoutSerializer(dockingManager);
      string folder = Environment.GetFolderPath(System.Environment.SpecialFolder.ApplicationData);
      string file = string.Format(@"{0}\Afx.net\{1}.layout.config", folder, Process.GetCurrentProcess().ProcessName);
      FileInfo fi = new FileInfo(file);
      if (!fi.Directory.Exists) fi.Directory.Create();
      layoutSerializer.Serialize(file);
    }

    public void OnBuiltUp(NamedTypeBuildKey buildKey)
    {
      this.EventAggregator.GetEvent<EditDocumentEvent>().Subscribe(OnEditDocument);
      this.EventAggregator.GetEvent<DialogDisplayedEvent>().Subscribe(DialogDisplayed);

      //TODO: Not MVVM
      Backstage bs = (Backstage)Ribbon.Menu;
      bs.IsOpenChanged += BackStage_IsOpenChanged;
    }

    private void DialogDisplayed(EventArgs obj)
    {
      //TODO: Not MVVM
      Backstage bs = (Backstage)Ribbon.Menu;
      bs.IsOpen = false;
    }

    void BackStage_IsOpenChanged(object sender, DependencyPropertyChangedEventArgs e)
    {
      Backstage bs = (Backstage)Ribbon.Menu;
      if (bs.IsOpen)
      {
        this.EventAggregator.GetEvent<BackStageOpenedEvent>().Publish(EventArgs.Empty);
      }
    }

    private void OnEditDocument(EditDocumentEventArgs obj)
    {
      //TODO: Not MVVM
      Backstage bs = (Backstage)Ribbon.Menu;
      bs.IsOpen = false;
    }

    public void OnTearingDown()
    {
    }
  }
}

﻿using Afx.Business;
using Afx.Business.Activities;
using Afx.Prism.RibbonMdi.Activities;
using Afx.Prism.RibbonMdi.Documents;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi
{
  public class RetryOperationExecution : RetryAction
  {
    public RetryOperationExecution(IMdiDockableDocumentController controller, Operation operation, BusinessObject argument)
    {
      Controller = controller;
      Operation = operation;
      Argument = argument;
    }

    IMdiDockableDocumentController mController;
    public IMdiDockableDocumentController Controller
    {
      get { return mController; }
      private set { mController = value; }
    }

    Operation mOperation;
    public Operation Operation
    {
      get { return mOperation; }
      private set { mOperation = value; }
    }

    BusinessObject mArgument;
    public BusinessObject Argument
    {
      get { return mArgument; }
      private set { mArgument = value; }
    }

    public override void Retry()
    {
      MdiApplicationController.Current.Dispatcher.BeginInvoke(new Action(() =>
      {
        Controller.ExecuteOperation(Operation, Argument);
      }), System.Windows.Threading.DispatcherPriority.Background);
    }
  }
}

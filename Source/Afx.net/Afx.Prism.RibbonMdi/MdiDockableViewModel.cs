﻿using Afx.Business;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Prism.RibbonMdi
{
  public abstract class MdiDockableViewModel<TModel> : ViewModel<TModel>
    where TModel : class, IBusinessObject
  {
    protected MdiDockableViewModel(IController controller)
      :base(controller)
    {
    }

    [Dependency]
    public IRegionManager RegionManager { get; set; }

    string mTitle;
    public string Title
    {
      get { return mTitle; }
      set { SetProperty<string>(ref mTitle, value); }
    }

    bool mIsActive = true;
    public virtual bool IsActive
    {
      get { return mIsActive; }
      set { SetProperty<bool>(ref mIsActive, value); }
    }
  }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business
{
  static class EventHandlerExtensions
  {
    public static void SafeInvoke<T>(this EventHandler<T> eventHandler, object sender, T e) where T : EventArgs
    {
      EventHandler<T> handler = eventHandler;

      if (handler != null)
      {
        handler(sender, e);
      }
    }
  }
}

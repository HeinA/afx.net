﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business
{
  public class ExternalReferenceHelper
  {
    public static string GetReferenceText(IExternalReferenceCollection rc)
    {
      Collection<string> col = new Collection<string>();
      foreach (IExternalReference er in rc.ExternalReferences)
      {
        if (!((BusinessObject)er).IsDeleted) col.Add(string.Format("{0}={1}", er.ExternalSystem.Name, er.Reference));
      }
      if (col.Count == 0) return string.Empty;
      return string.Join("; ", col);
    }
  }
}

﻿using Afx.Business.Collections;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business
{
  [DataContract(IsReference = true, Namespace = Namespace.Afx)]
  public class SearchResults
  {
    internal SearchResults(Collection<string> headers, Collection<string> formats)
    {
      mHeaders = headers;
      mFormats = formats;
    }

    [DataMember(Name = "Headers")]
    internal Collection<string> mHeaders;
    public string[] Headers
    {
      get { return mHeaders.ToArray(); }
    }

    [DataMember(Name = "Formats")]
    Collection<string> mFormats;
    public string[] Formats
    {
      get { return mFormats.ToArray(); }
    }

    [DataMember(Name = "Results")]
    BasicCollection<SearchResult> mResults;
    public SearchResult[] Results
    {
      get
      {
        if (mResults == null) mResults = new BasicCollection<SearchResult>();
        return mResults.ToArray();
      }
    }

    internal void AddResult(SearchResult result)
    {
      if (mResults == null) mResults = new BasicCollection<SearchResult>();
      result.SearchResultOwner = this;
      mResults.Add(result);
    }

    public override string ToString()
    {
      string s = null;
      foreach (string v in Headers)
      {
        s += string.Format(s == null ? "\" {0}\"" : ", \" {0}\"", v);
      }
      return s;
    }
  }
}

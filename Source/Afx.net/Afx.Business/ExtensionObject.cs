﻿using Afx.Business.Attributes;
using Afx.Business.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business
{
  [PersistantObject(PersistToDerivedClass = true)]
  [DataContract(IsReference = true, Namespace = Namespace.Afx)]
  [NonCacheable]
  public abstract class ExtensionObject<TOwner> : BusinessObject<TOwner>, IExtensionObject
    where TOwner : BusinessObject
  {
  }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business
{
  public interface ICollectionValidator
  {
    Type ObjectType
    {
      get;
    }

    void GetErrors(IList col, Collection<string> errors);
  }
}

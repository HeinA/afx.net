﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business
{
  public abstract class CollectionValidator<T> : ICollectionValidator
    where T: BusinessObject
  {
    Type ICollectionValidator.ObjectType
    {
      get { return typeof(T); }
    }

    void ICollectionValidator.GetErrors(IList col, Collection<string> errors)
    {
      GetErrors((Collection<T>)col, errors);
    }

    public abstract void GetErrors(Collection<T> col, Collection<string> errors);
  }
}

﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business
{
  internal class RegistryHelper
  {
    public const string ApplicationKey = @"Software\Afx\{0}";

    public static bool GetRegistryValue<T>(RegistryHive hive, string key, string value, RegistryValueKind kind, out T data)
    {
      bool success = false;
      data = default(T);

      using (RegistryKey baseKey = RegistryKey.OpenRemoteBaseKey(hive, String.Empty))
      {
        if (baseKey != null)
        {
          using (RegistryKey registryKey = baseKey.OpenSubKey(key, RegistryKeyPermissionCheck.ReadSubTree))
          {
            if (registryKey != null)
            {
              // If the key was opened, try to retrieve the value.
              object regValue = registryKey.GetValue(value, null);
              if (regValue != null)
              {
                RegistryValueKind kindFound = registryKey.GetValueKind(value);
                if (kindFound == kind)
                {
                  data = (T)Convert.ChangeType(regValue, typeof(T), CultureInfo.InvariantCulture);
                  success = true;
                }
              }
            }
          }
        }
      }
      return success;
    }

    public static bool SetRegistryValue<T>(RegistryHive hive, string key, string value, RegistryValueKind kind, T data)
    {
      bool success = false;
      using (RegistryKey baseKey = RegistryKey.OpenRemoteBaseKey(hive, String.Empty))
      {
        if (baseKey != null)
        {
          using (RegistryKey registryKey = baseKey.OpenSubKey(key, true))
          {
            if (registryKey != null)
            {
              registryKey.SetValue(value, data, kind);
            }
            else
            {
              using (RegistryKey newRegistryKey = baseKey.CreateSubKey(key))
              {
                newRegistryKey.SetValue(value, data, kind);
              }
            }
          }
        }
      }
      return success;
    }
  }
}

﻿using ClosedXML.Excel;
using DocumentFormat.OpenXml.Spreadsheet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business
{
  public interface IExcelRow
  {
    void ExportHeader(IXLWorksheet sheet, int row, string exportKey);
    void ExportRow(IXLWorksheet sheet, int row, string exportKey);
  }
}

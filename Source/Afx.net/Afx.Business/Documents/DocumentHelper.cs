﻿using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Business.Service;
using Afx.Business.ServiceModel;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace Afx.Business.Documents
{
  public static class DocumentHelper
  {
    [ThreadStatic]
    static Collection<Guid> mActiveDocuments;
    static Collection<Guid> ActiveDocuments
    {
      get { return mActiveDocuments ?? (mActiveDocuments = new Collection<Guid>()); }
    }

    public static Document TransferAllDocuments(Document document, Guid targetOU, IPersistanceManager persistanceManager)
    {
      //COU
      //DocumentTransferCollection dtc = new DocumentTransferCollection(persistanceManager);
      //ObjectRepository<Document> or = persistanceManager.GetRepository<Document>();

      //dtc.Add(document);

      //foreach (Document doc in dtc)
      //{
      //  if (!doc.ControllingOrganizationalUnit.GlobalIdentifier.Equals(targetOU))
      //  {
      //    if (doc.ControllingOrganizationalUnit.Server.Equals(ServiceBase.ServerInstance))
      //    {
      //      TransferDocument(doc, targetOU, persistanceManager);
      //    }
      //    else
      //    {
      //      using (var svc = ProxyFactory.GetService<IAfxTransferService>(doc.ControllingOrganizationalUnit.Server.Name))
      //      {
      //        Document d1 = svc.TransferDocument(doc.GlobalIdentifier, targetOU);
      //        or.Persist(d1, PersistanceFlags.Import | PersistanceFlags.IgnoreRevision);
      //      }
      //    }
      //  }
      //}

      //Document doc1 = or.GetInstance(document.GlobalIdentifier);
      //return doc1;
      return document;
    }

    internal static Document TransferDocument(Document document, Guid targetOU, IPersistanceManager persistanceManager)
    {
      //COU
      //OrganizationalUnit ou = Cache.Instance.GetObject<OrganizationalUnit>(targetOU);

      //if (!document.ControllingOrganizationalUnit.Server.Equals(ServiceBase.ServerInstance))
      //  throw new DocumentControlException(string.Format("Document {0} is not controlled by this server.", document.DocumentNumber));

      //document.ControllingOrganizationalUnit = ou;

      //ObjectRepository<Document> or = persistanceManager.GetRepository<Document>();
      //or.Persist(document, PersistanceFlags.IgnoreControllingOU);

      //Document d2 = or.GetInstance(document.GlobalIdentifier);
      //return d2;

      return document;
    }

    public static bool IsSettingState(Document doc)
    {
      return ActiveDocuments.Contains(doc.GlobalIdentifier);
    }

    public static void SetState(Document doc, string documentTypeStateIdentifier)
    {
      SetState(doc, documentTypeStateIdentifier, new DocumentStateEventScope());
    }

    public static void SetState(Document doc, string documentTypeStateIdentifier, DocumentStateEventScope scope)
    {
      SetState(doc, Cache.Instance.GetObject<DocumentTypeState>(documentTypeStateIdentifier), scope, PersistanceFlags.None);
    }

    public static bool SetDocumentNumber(Document doc)
    {
      if (string.IsNullOrWhiteSpace(doc.DocumentNumber))
      {
        IInternalPersistanceManager pm = (IInternalPersistanceManager)UnityProvider.Instance.Resolve<IPersistanceManager>();

        string documentNumber = null;
        using (ConnectionScope cs = new ConnectionScope())
        using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Suppress))
        {
          IDbCommand cmd = pm.GetCommand();
          cmd.CommandText = string.Format("{0}.{1}", pm.GetIdentifier("Afx"), pm.GetIdentifier("GetNextID"));
          cmd.CommandType = CommandType.StoredProcedure;
          pm.AddParameter("@DocumentTypeID", doc.DocumentType.Id, cmd);
          pm.AddParameter("@OrganizationalUnitID", doc.OrganizationalUnit.Id, cmd);
          string outParam = pm.AddOutputParameter("@NextID", cmd);
          cmd.ExecuteNonQuery();
          DbParameter p = (DbParameter)cmd.Parameters[outParam];
          if ((int)p.Value == -1) throw new MessageException("An error occured getting the next document number.");

          documentNumber = string.Format("{0}{1:0000000}", doc.DocumentType.GetAssociativeObject<DocumentTypeNumber>(doc.OrganizationalUnit).Acronymn, p.Value);
          ts.Complete();
        }

        //IDbCommand cmdUpdate = PersistanceManager.GetCommand();
        //cmdUpdate.CommandText = string.Format("UPDATE {0}.{1} WITH (ROWLOCK) SET {2}={3} WHERE {4}={5}", PersistanceManager.GetIdentifier("Afx"), PersistanceManager.GetIdentifier("Document"), PersistanceManager.GetIdentifier("DocumentNumber"), PersistanceManager.AddParameter(1, documentNumber, cmdUpdate), PersistanceManager.GetIdentifier("GlobalIdentifier"), PersistanceManager.AddParameter(2, obj.GlobalIdentifier, cmdUpdate));
        //PrintCommand(cmdUpdate);
        //int i = cmdUpdate.ExecuteNonQuery();
        //if (i != 1) throw new MessageException("An error occured updating the document number.");

        using (new EventStateSuppressor(doc))
        {
          doc.DocumentNumber = documentNumber;
        }

        return true;
      }

      return false;
    }


    public static void SetState(Document doc, DocumentTypeState dts, DocumentStateEventScope scope, PersistanceFlags flags) 
    {
      //COU
      //if (!doc.ControllingOrganizationalUnit.Server.Equals(ServiceBase.ServerInstance))
      //{
      //  throw new DocumentControlException(string.Format("Document {0} is not controlled by this server.", doc.DocumentNumber));
      //}

      try
      {
        if (ActiveDocuments.Contains(doc.GlobalIdentifier)) return;
        ActiveDocuments.Add(doc.GlobalIdentifier);

        IInternalPersistanceManager pm = (IInternalPersistanceManager)UnityProvider.Instance.Resolve<IPersistanceManager>();

        SetDocumentNumber(doc);

        DocumentState dsOriginal = doc.State;
        IInternalObjectRepository repo = pm.GetRepository(doc.GetType());

        if (!dsOriginal.DocumentTypeState.Equals(dts)) doc.State = new DocumentState(SecurityContext.User, dts);

        foreach (IDocumentStateEventManager em in pm.GetDocumentStateEventManagers(doc.GetType()).Where(d => d.TargetState.Equals(dts)))
        {
          em.Process(doc, dsOriginal.DocumentTypeState, scope);
        }

        repo.Persist(doc, flags);
      }
      catch (SqlException ex)
      {
        switch (ex.ErrorCode)
        {
          case -2146232060:
            if (ex.Message.Contains("insert duplicate key"))
            {
              throw new UniqueIndexViolationException(string.Format("Can not create duplicate key ({0})", doc.DocumentNumber), doc.GetType(), ex);
            }
            break;
        }

        throw;
      }
      finally
      {
        ActiveDocuments.Remove(doc.GlobalIdentifier);
      }
    }
    
    //public static void SetState(Document doc, DocumentTypeState dts) //, bool persist)
    //{
    //  SetState(doc, dts, null, PersistanceFlags.None);
    //}
  }
}

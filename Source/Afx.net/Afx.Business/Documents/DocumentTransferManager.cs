﻿using Afx.Business.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.Documents
{
  public abstract class DocumentTransferManager<TDocument> : IDocumentTransferManager
    where TDocument : Document 
  {
    public Type DocumentType
    {
      get { return typeof(TDocument); }
    }

    IPersistanceManager mPersistanceManager;
    public IPersistanceManager PersistanceManager
    {
      get { return mPersistanceManager; }
      internal set { mPersistanceManager = value; }
    }

    IPersistanceManager IDocumentTransferManager.PersistanceManager
    {
      get { return PersistanceManager; }
      set { PersistanceManager = value; }
    }

    void IDocumentTransferManager.AppendRelatedDocuments(Document document, DocumentTransferCollection documents)
    {
      AppendRelatedDocuments((TDocument)document, documents);
    }

    public abstract void AppendRelatedDocuments(TDocument document, DocumentTransferCollection documents);
  }
}

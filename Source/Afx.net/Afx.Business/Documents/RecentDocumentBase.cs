﻿using Afx.Business.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.Documents
{
  public class RecentDocumentBase : BusinessObject
  {
    #region Guid GlobalIdentifier

    Guid mGlobalIdentifier;
    public Guid GlobalIdentifier
    {
      get { return mGlobalIdentifier; }
      set { mGlobalIdentifier = value; }
    }

    #endregion

    #region string DocumentNumber

    string mDocumentNumber;
    public string DocumentNumber
    {
      get { return mDocumentNumber; }
      set { mDocumentNumber = value; }
    }

    #endregion

    #region DateTime DocumentDate

    DateTime mDocumentDate;
    public DateTime DocumentDate
    {
      get { return mDocumentDate; }
      set { mDocumentDate = value; }
    }

    #endregion

    #region string OrganizationalUnit

    string mOrganizationalUnit;
    public string OrganizationalUnit
    {
      get { return mOrganizationalUnit; }
      set { mOrganizationalUnit = value; }
    }

    #endregion

    #region string DocumentState

    string mDocumentState;
    public string DocumentState
    {
      get { return mDocumentState; }
      set { mDocumentState = value; }
    }

    #endregion
  }
}

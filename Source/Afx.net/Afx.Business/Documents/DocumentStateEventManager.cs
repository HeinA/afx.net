﻿using Afx.Business.Data;
using Afx.Business.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.Documents
{
  public abstract class DocumentStateEventManager<T> : IDocumentStateEventManager
    where T : Document
  {
    public abstract string TargetStateIdentifier { get; }

    DocumentTypeState mTargetState;
    public DocumentTypeState TargetState
    {
      get { return mTargetState ?? (mTargetState = Cache.Instance.GetObject<DocumentTypeState>(TargetStateIdentifier)); }
    }

    public abstract void Process(T doc, DocumentTypeState originalState, DocumentStateEventScope scope);

    IPersistanceManager mPersistanceManager;
    public IPersistanceManager PersistanceManager
    {
      get { return mPersistanceManager; }
      internal set { mPersistanceManager = value; }
    }

    IPersistanceManager IDocumentStateEventManager.PersistanceManager
    {
      get { return PersistanceManager; }
      set { PersistanceManager = value; }
    }

    Type IDocumentStateEventManager.TargetType
    {
      get { return typeof(T); }
    }

    void IDocumentStateEventManager.Process(Document doc, DocumentTypeState originalState, DocumentStateEventScope scope)
    {
      Process((T)doc, originalState, scope);
    }

    protected TDocument TransferIfRequired<TDocument>(TDocument doc)
      where TDocument : Document
    {
      //COU
      //if (!doc.ControllingOrganizationalUnit.Equals(SecurityContext.OrganizationalUnit)) //Transfer Document
      //{
      //  return (TDocument)DocumentHelper.TransferAllDocuments(doc, SecurityContext.OrganizationalUnit.GlobalIdentifier, PersistanceManager);
      //}
      return doc;
    }
  }
}

﻿using Afx.Business.Collections;
using Afx.Business.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.Documents
{
  public class DocumentTransferCollection : BasicCollection<Document>
  {
    internal DocumentTransferCollection(IPersistanceManager persistanceManager)
    {
      PersistanceManager = persistanceManager;
    }

    #region IPersistanceManager PersistanceManager

    public const string PersistanceManagerProperty = "PersistanceManager";
    IPersistanceManager mPersistanceManager;
    public IPersistanceManager PersistanceManager
    {
      get { return mPersistanceManager; }
      private set { mPersistanceManager = value; }
    }

    #endregion

    protected override void InsertItem(int index, Document item)
    {
      Stack<Type> types = new Stack<Type>();
      
      Type t = item.GetType();
      while(t != typeof(Document))
      {
        foreach (var tm in ((IInternalPersistanceManager)PersistanceManager).GetDocumentTransferManagers(t)) 
        {
          tm.AppendRelatedDocuments(item, this);
        }

        t = t.BaseType;
      }

      base.InsertItem(index, item);
    }
  }
}

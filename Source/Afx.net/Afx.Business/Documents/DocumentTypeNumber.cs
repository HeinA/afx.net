﻿using Afx.Business.Data;
using Afx.Business.Security;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.Documents
{
  [DataContract(IsReference = true, Namespace = Namespace.Afx)]
  [PersistantObject(Schema = "Afx")]
  public class DocumentTypeNumber : AssociativeObject<DocumentType, OrganizationalUnit>
  {
    #region string Acronymn

    public const string AcronymnProperty = "Acronymn";
    [DataMember(Name = AcronymnProperty, EmitDefaultValue = false)]
    string mAcronymn;
    [PersistantProperty]
    public string Acronymn
    {
      get { return mAcronymn; }
      set { SetProperty<string>(ref mAcronymn, value); }
    }

    #endregion

    #region int NextId

    public const string NextIdProperty = "NextId";
    [DataMember(Name = NextIdProperty, EmitDefaultValue = false)]
    int mNextId;
    [PersistantProperty(ImportOverwrite = false)]
    public int NextId
    {
      get { return mNextId; }
      set { SetProperty<int>(ref mNextId, value); }
    }

    #endregion

    protected override void GetErrors(Collection<string> errors, string propertyName)
    {
      if (propertyName == null || propertyName == AcronymnProperty)
      {
        if (string.IsNullOrWhiteSpace(Acronymn)) errors.Add("Acronymn is a mandatory field.");
      }

      if (propertyName == null || propertyName == NextIdProperty)
      {
        if (NextId <= 0) errors.Add("NextId must be greater than zero.");
      }

      base.GetErrors(errors, propertyName);
    }
  }
}

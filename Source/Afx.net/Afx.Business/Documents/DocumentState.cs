﻿using Afx.Business.Data;
using Afx.Business.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.Documents
{
  [DataContract(IsReference = true, Namespace = Namespace.Afx)]
  [PersistantObject(Schema = "Afx")]
  public class DocumentState : BusinessObject<Document>
  {
    public DocumentState()
    {
    }

    public DocumentState(UserReference user, DocumentTypeState state)
    {
      using (new EventStateSuppressor(this))
      {
        User = user;
        DocumentTypeState = state;
        Timestamp = DateTime.Now;
      }
    }

    public DocumentState(User user, DocumentTypeState state)
    {
      using (new EventStateSuppressor(this))
      {
        User = Cache.Instance.GetObject<UserReference>(user.GlobalIdentifier);
        DocumentTypeState = state;
      }
    }

    #region UserReference User

    public const string UserProperty = "User";
    [PersistantProperty]
    public UserReference User
    {
      get { return GetCachedObject<UserReference>(); }
      set { SetCachedObject<UserReference>(value); }
    }

    #endregion

    #region DocumentTypeState DocumentTypeState

    public const string DocumentTypeStateProperty = "DocumentTypeState";
    [PersistantProperty]
    public DocumentTypeState DocumentTypeState
    {
      get { return GetCachedObject<DocumentTypeState>(); }
      set { SetCachedObject<DocumentTypeState>(value); }
    }

    #endregion

    #region DateTime? Timestamp

    public const string TimestampProperty = "Timestamp";
    [DataMember(Name = TimestampProperty, EmitDefaultValue = false)]
    DateTime? mTimestamp;
    [PersistantProperty(IgnoreNull = true)]
    public DateTime? Timestamp
    {
      get { return mTimestamp; }
      set { SetProperty<DateTime?>(ref mTimestamp, value); }
    }

    #endregion
  }
}

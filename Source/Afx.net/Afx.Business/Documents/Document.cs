﻿using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.Documents
{
  [PersistantObject(Schema="Afx")]
  [DataContract(IsReference = true, Namespace = Namespace.Afx)]
  public abstract class Document : BusinessObject, IRevisable
  {
    #region Constructors

    protected Document(DocumentType documentType)
    {
      using (new EventStateSuppressor(this))
      {
        SetCachedObject<DocumentType>(documentType, DocumentTypeProperty);
        State = new DocumentState(SecurityContext.User, DocumentType.InitialState);
        OrganizationalUnit = SecurityContext.OrganizationalUnit;
        //COU ControllingOrganizationalUnit = OrganizationalUnit;
      }
    }

    #endregion

    #region DocumentType DocumentType

    public const string DocumentTypeProperty = "DocumentType";
    [PersistantProperty]
    public DocumentType DocumentType
    {
      get { return GetCachedObject<DocumentType>(); }
    }

    #endregion

    #region OrganizationalUnit OrganizationalUnit

    public const string OrganizationalUnitProperty = "OrganizationalUnit";
    [PersistantProperty]
    public OrganizationalUnit OrganizationalUnit
    {
      get { return GetCachedObject<OrganizationalUnit>(); }
      set { SetCachedObject<OrganizationalUnit>(value); }
    }

    #endregion

    //COU
    #region OrganizationalUnit ControllingOrganizationalUnit

    public const string ControllingOrganizationalUnitProperty = "ControllingOrganizationalUnit";
    [PersistantProperty]
    public OrganizationalUnit ControllingOrganizationalUnit
    {
      get { return GetCachedObject<OrganizationalUnit>(); }
      set { SetCachedObject<OrganizationalUnit>(value); }
    }

    #endregion

    #region DocumentState State

    public const string StateProperty = "State";
    [DataMember(Name = StateProperty, EmitDefaultValue = false)]
    DocumentState mState;
    [PersistantProperty(PostProcess = true)]
    public DocumentState State
    {
      get { return mState; }
      set { SetOwnedReference<DocumentState>(ref mState, value); }
    }

    #endregion

    #region DateTime DocumentDate

    public const string DocumentDateProperty = "DocumentDate";
    [DataMember(Name = DocumentDateProperty, EmitDefaultValue = false)]
    DateTime mDocumentDate = DateTime.Now;
    [Search(Alias = "Date", Format = "dd MMM yyyy")]
    [PersistantProperty]
    public DateTime DocumentDate
    {
      get { return mDocumentDate; }
      set { SetProperty<DateTime>(ref mDocumentDate, value); }
    }

    #endregion

    #region string DocumentNumber

    public const string DocumentNumberProperty = "DocumentNumber";
    [DataMember(Name = DocumentNumberProperty, EmitDefaultValue = false)]
    string mDocumentNumber;
    [Search(Alias = "Document Number")]
    [PersistantProperty(SortOrder = 0, SortDirection = SortDirection.Descending)]
    public string DocumentNumber
    {
      get { return mDocumentNumber; }
      set { SetProperty<string>(ref mDocumentNumber, value); }
    }

    #endregion

    #region int Revision

    public const string RevisionProperty = "Revision";
    [DataMember(Name = RevisionProperty, EmitDefaultValue = false)]
    int mRevision = 1;
    [PersistantProperty]
    public int Revision
    {
      get { return mRevision; }
      set { SetProperty<int>(ref mRevision, value); }
    }

    #endregion

    #region BusinessObjectCollection<DocumentReference> References

    public const string ReferencesProperty = "References";
    BusinessObjectCollection<DocumentReference> mReferences;
    [PersistantCollection]
    [DataMember]
    public BusinessObjectCollection<DocumentReference> References
    {
      get { return mReferences ?? (mReferences = new BusinessObjectCollection<DocumentReference>(this)); }
    }

    #endregion

    #region BusinessObjectCollection<DocumentDocumentReference> DocumentReferences

    public const string DocumentReferencesProperty = "DocumentReferences";
    BusinessObjectCollection<DocumentDocumentReference> mDocumentReferences;
    [PersistantCollection]
    [DataMember]
    public BusinessObjectCollection<DocumentDocumentReference> DocumentReferences
    {
      get { return mDocumentReferences ?? (mDocumentReferences = new BusinessObjectCollection<DocumentDocumentReference>(this)); }
    }

    #endregion

    #region string Notes

    public const string NotesProperty = "Notes";
    [DataMember(Name = NotesProperty, EmitDefaultValue = false)]
    string mNotes;
    [PersistantProperty]
    public string Notes
    {
      get { return mNotes; }
      set { SetProperty<string>(ref mNotes, value); }
    }

    #endregion

    #region bool IsReadOnly

    public const string IsReadOnlyProperty = "IsReadOnly";
    public bool IsReadOnly
    {
      get { return State.DocumentTypeState.IsReadOnly; }  //COU || !ControllingOrganizationalUnit.Server.Equals(SecurityContext.OrganizationalUnit.Server); }
    }

    #endregion

    #region bool CanEdit

    public bool CanEdit
    {
      get
      {
        return true;
        //foreach (Role r in SecurityContext.User.Roles)
        //{
        //  if (Roles.Contains(r) && this.GetAssociativeObject<ActivityRole>(r).EditPermission) return true;
        //}
        //return false;
      }
    }

    #endregion

    #region bool CanView

    public bool CanView
    {
      get
      {
        return true;
        //foreach (Role r in SecurityContext.User.Roles)
        //{
        //  if (Roles.Contains(r)) return true;
        //}
        //return false;
      }
    }

    #endregion

    #region bool CanExecute(Operation op)

    public bool CanExecute(Operation op)
    {
      if (op is GlobalOperation)
      {
        DocumentTypeGlobalOperation dtgop = this.DocumentType.GetAssociativeObject<DocumentTypeGlobalOperation>(op);
        DocumentTypeStateOperation dtsop = this.State.DocumentTypeState.GetAssociativeObject<DocumentTypeStateOperation>(op);
        if (dtgop == null && dtsop == null) return false;

        foreach (Role r in SecurityContext.User.Roles)
        {
          if (dtsop != null && dtsop.Roles.Contains(r)) return true;
          if (dtgop != null && dtgop.Roles.Contains(r)) return true;
        }
      }

      return false;
    }

    #endregion
  }
}

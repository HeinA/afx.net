﻿using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Business.Service;
using Afx.Business.Service.Configuration;
using Afx.Business.ServiceModel;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace Afx.Business.Documents
{
  class DocumentEventManager : PersistanceEventManager<Document>
  {
    public override bool BeforePersist(Document obj, PersistanceFlags flags)
    {
      foreach (var dr in obj.References)
      {
        if(string.IsNullOrWhiteSpace(dr.Reference)) dr.IsDeleted = true;
      }

      return base.BeforePersist(obj, flags);
    }

    public override void AfterPersist(Document obj, PersistanceFlags flags)
    {
      try
      {
        if (DocumentHelper.SetDocumentNumber(obj))
        {
          IDbCommand cmdUpdate = PersistanceManager.GetCommand();
          cmdUpdate.CommandText = string.Format("UPDATE {0}.{1} WITH (ROWLOCK) SET {2}={3} WHERE {4}={5}", PersistanceManager.GetIdentifier("Afx"), PersistanceManager.GetIdentifier("Document"), PersistanceManager.GetIdentifier("DocumentNumber"), PersistanceManager.AddParameter(1, obj.DocumentNumber, cmdUpdate), PersistanceManager.GetIdentifier("GlobalIdentifier"), PersistanceManager.AddParameter(2, obj.GlobalIdentifier, cmdUpdate));
          PrintCommand(cmdUpdate);
          int i = cmdUpdate.ExecuteNonQuery();
          if (i != 1) throw new MessageException("An error occured updating the document number.");
        }

        if ((flags & PersistanceFlags.Import) == 0)
        {
          if ((flags & PersistanceFlags.IgnoreControllingOU) == 0)
          {
            object previousState = null;
            bool b = obj.GetOriginalValue(Document.StateProperty, ref previousState);
            if (!b || previousState == null)
            {
              DocumentHelper.SetState(obj, obj.State.DocumentTypeState, new DocumentStateEventScope(), PersistanceFlags.IgnoreRevision);
            }
          }

          if (DocumentReplicationExtension.Current != null && !DocumentReplicationExtension.Current.DocumentStack.Contains(obj)) DocumentReplicationExtension.Current.DocumentStack.Push(obj);
        }
      }
      catch (SqlException ex)
      {
        switch (ex.ErrorCode)
        {
          case -2146232060:
            if (ex.Message.Contains("insert duplicate key"))
            {
              throw new UniqueIndexViolationException(string.Format("Can not create duplicate key ({0})", obj.DocumentNumber), obj.GetType(), ex);
            }
            break;

          default:
            throw;
        }
      }
      catch
      {
#if DEBUG
        if (Debugger.IsAttached) Debugger.Break();
#endif
        throw;
      }

      base.AfterPersist(obj, flags);
    }

    public override void AfterDelete(Document obj, PersistanceFlags flags)
    {
      if (DocumentReplicationExtension.Current != null) DocumentReplicationExtension.Current.DocumentStack.Push(obj);
      //Replicate(obj);
      base.AfterDelete(obj, flags);
    }

    //void Replicate(Document obj)
    //{
    //  ServerInstance master = ServiceBase.ServerInstance.Root;
    //  if (obj.ControllingOrganizationalUnit.Server.Equals(ServiceBase.ServerInstance)) 
    //  {
    //    using (var svc = ServiceFactory.GetService<IAfxReplicationService>(master.Name))
    //    {
    //      svc.Instance.Replicate(obj, ServiceBase.ServerInstance.GlobalIdentifier);
    //    }
    //  }
    //}
  }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.Documents
{
  [Serializable]
  public class DocumentControlException : Exception
  {
    protected DocumentControlException(SerializationInfo info, StreamingContext context)
      :base(info, context)
    {
    }

    public DocumentControlException(string message)
      : base(message)
    {
    }

    public DocumentControlException(string message, Exception innerException)
      : base(message, innerException)
    {
    }  
  }
}

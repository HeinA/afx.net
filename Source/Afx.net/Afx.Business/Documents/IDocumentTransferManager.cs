﻿using Afx.Business.Data;
using Afx.Business.Service;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.Documents
{
  public interface IDocumentTransferManager
  {
    Type DocumentType { get; }
    IPersistanceManager PersistanceManager { get; set; }

    void AppendRelatedDocuments(Document document, DocumentTransferCollection documents);
  }
}

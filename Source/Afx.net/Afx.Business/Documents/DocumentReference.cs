﻿using Afx.Business;
using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Validation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace Afx.Business.Documents
{
  [PersistantObject(Schema = "Afx", OwnerColumn = "Document")]
  public partial class DocumentReference : BusinessObject<Document> 
  {
    #region Constructors

    public DocumentReference()
    {
    }

    public DocumentReference(bool isNull)
      : base(isNull)
    {
    }

    #endregion

    #region DocumentTypeReference DocumentTypeReference

    public const string DocumentTypeReferenceProperty = "DocumentTypeReference";
    [Mandatory("Reference Type is mandatory.")]
    [PersistantProperty]
    public DocumentTypeReference DocumentTypeReference
    {
      get { return GetCachedObject<DocumentTypeReference>(); }
      set { SetCachedObject<DocumentTypeReference>(value); }
    }

    #endregion

    #region string Reference

    public const string ReferenceProperty = "Reference";
    [DataMember(Name = ReferenceProperty, EmitDefaultValue = false)]
    string mReference;
    [Mandatory("Reference is mandatory.")]
    [PersistantProperty]
    public string Reference
    {
      get { return mReference; }
      set { SetProperty<string>(ref mReference, value); }
    }

    #endregion
  }
}

﻿using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.Documents
{
  [DataContract(IsReference = true, Namespace = Namespace.Afx)]
  [PersistantObject(Schema = "Afx")]
  public class DocumentTypeState : BusinessObject<DocumentType>, IGlobalOperationsOwner
  {
    #region Constructors

    public DocumentTypeState()
    {
    }

    public DocumentTypeState(bool isNull)
      : base(isNull)
    {
    }

    #endregion

    #region string Name

    public const string NameProperty = "Name";
    [DataMember(Name = NameProperty, EmitDefaultValue = false)]
    string mName;
    [PersistantProperty]
    public string Name
    {
      get { return mName; }
      set { SetProperty<string>(ref mName, value); }
    }

    #endregion

    //#region bool IsReadOnly

    //public const string IsReadOnlyProperty = "IsReadOnly";
    //[DataMember(Name = IsReadOnlyProperty, EmitDefaultValue = false)]
    //bool mIsReadOnly;
    //[PersistantProperty]
    //public bool IsReadOnly
    //{
    //  get { return mIsReadOnly; }
    //  set { SetProperty<bool>(ref mIsReadOnly, value); }
    //}

    //#endregion

    #region bool IsReadOnly

    public const string IsReadOnlyProperty = "IsReadOnly";
    public bool IsReadOnly
    {
      get { return !SecurityContext.User.HasRole(EditPermissions.ToArray()); }
    }

    #endregion

    #region bool IsInRecentList

    public const string IsInRecentListProperty = "IsInRecentList";
    [DataMember(Name = IsInRecentListProperty, EmitDefaultValue = false)]
    bool mIsInRecentList;
    [PersistantProperty]
    public bool IsInRecentList
    {
      get { return mIsInRecentList; }
      set { SetProperty<bool>(ref mIsInRecentList, value); }
    }

    #endregion

    #region Validation

    protected override void GetErrors(System.Collections.ObjectModel.Collection<string> errors, string propertyName)
    {
      if (propertyName == null || propertyName == NameProperty)
      {
        if (string.IsNullOrWhiteSpace(Name)) errors.Add("Name is a mandatory field.");
      }

      base.GetErrors(errors, propertyName);
    }

    #endregion

    #region Associative BusinessObjectCollection<GlobalOperation> Operations

    public const string OperationsProperty = "Operations";
    AssociativeObjectCollection<DocumentTypeStateOperation, GlobalOperation> mOperations;
    [PersistantCollection(AssociativeType = typeof(DocumentTypeStateOperation))]
    public BusinessObjectCollection<GlobalOperation> Operations
    {
      get
      {
        if (mOperations == null) using (new EventStateSuppressor(this)) { mOperations = new AssociativeObjectCollection<DocumentTypeStateOperation, GlobalOperation>(this); }
        return mOperations;
      }
    }

    #endregion

    #region Associative BusinessObjectCollection<Role> EditPermissions

    public const string EditPermissionsProperty = "EditPermissions";
    AssociativeObjectCollection<DocumentTypeStateEditRole, Role> mEditPermissions;
    [PersistantCollection(AssociativeType = typeof(DocumentTypeStateEditRole))]
    public BusinessObjectCollection<Role> EditPermissions
    {
      get
      {
        if (mEditPermissions == null) using (new EventStateSuppressor(this)) { mEditPermissions = new AssociativeObjectCollection<DocumentTypeStateEditRole, Role>(this); }
        return mEditPermissions;
      }
    }

    #endregion

    #region IGlobalOperationsOwner

    IEnumerable<GlobalOperation> IGlobalOperationsOwner.Operations
    {
      get { return Operations; }
    }

    void IGlobalOperationsOwner.AddOperation(GlobalOperation op)
    {
      Operations.Add(op);
    }

    #endregion
  }
}

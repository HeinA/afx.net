﻿using Afx.Business.Activities;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Business.Validation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.Documents
{
  [DataContract(IsReference = true, Namespace = Afx.Business.Namespace.Afx)]
  [PersistantObject(Schema = "Afx")]
  [Cache]
  public class DocumentType : BusinessObject, IGlobalOperationsOwner, INamespaceSensitive, IRevisable
  {
    public DocumentType()
    {
    }

    public DocumentType(bool isNull)
      : base(isNull)
    {
    }

    #region bool Enabled

    public const string EnabledProperty = "Enabled";
    [DataMember(Name = EnabledProperty, EmitDefaultValue = false)]
    bool mEnabled = true;
    [PersistantProperty]
    public bool Enabled
    {
      get { return mEnabled; }
      set { SetProperty<bool>(ref mEnabled, value); }
    }

    #endregion

    #region int Revision

    public const string RevisionProperty = "Revision";
    [DataMember(Name = RevisionProperty, EmitDefaultValue = false)]
    int mRevision = 1;
    [PersistantProperty]
    public int Revision
    {
      get { return mRevision; }
      set { SetProperty<int>(ref mRevision, value); }
    }

    #endregion

    #region string Name

    public const string NameProperty = "Name";
    [DataMember(Name = NameProperty, EmitDefaultValue = false)]
    string mName;
    [PersistantProperty]
    public string Name
    {
      get { return mName; }
      set { SetProperty<string>(ref mName, value); }
    }

    #endregion

    #region string Namespace

    public const string NamespaceProperty = "Namespace";
    [DataMember(Name = NamespaceProperty, EmitDefaultValue = false)]
    string mNamespace;
    [PersistantProperty]
    [Mandatory("Namespace is mandatory.")]
    public string Namespace
    {
      get { return mNamespace; }
      set { SetProperty<string>(ref mNamespace, value); }
    }

    #endregion

    #region DocumentType BaseDocument

    public const string BaseDocumentProperty = "BaseDocument";
    [PersistantProperty]
    public DocumentType BaseDocument
    {
      get { return GetCachedObject<DocumentType>(); }
      set
      {
        if (SetCachedObject<DocumentType>(value))
        {
          OnPropertyChanged(HeirarchyStatesProperty);
        }
      }
    }

    #endregion

    #region DocumentTypeState InitialState

    public const string InitialStateProperty = "InitialState";
    [PersistantProperty(PostProcess = true)]
    public DocumentTypeState InitialState
    {
      get { return GetChildObject<DocumentTypeState>(HeirarchyStates); }
      set { SetChildObject<DocumentTypeState>(value); }
    }

    #endregion

    #region BusinessObjectCollection<DocumentTypeState> States

    public const string StatesProperty = "States";
    BusinessObjectCollection<DocumentTypeState> mStates;
    [PersistantCollection]
    [DataMember]
    public BusinessObjectCollection<DocumentTypeState> States
    {
      get { return mStates ?? (mStates = new BusinessObjectCollection<DocumentTypeState>(this)); }
    }

    #endregion

    #region IEnumerable<DocumentTypeState> HeirarchyStates

    public const string HeirarchyStatesProperty = "HeirarchyStates";
    public IEnumerable<DocumentTypeState> HeirarchyStates
    {
      get
      {
        if (!IsNull(BaseDocument)) return BaseDocument.HeirarchyStates.Union(States);
        else return States;
      }
    }

    #endregion

    #region Associative BusinessObjectCollection<OrganizationalUnit> ApplicableOrganizationalUnits

    public const string ApplicableOrganizationalUnitsProperty = "ApplicableOrganizationalUnits";
    AssociativeObjectCollection<DocumentTypeNumber, OrganizationalUnit> mApplicableOrganizationalUnits;
    [PersistantCollection(AssociativeType = typeof(DocumentTypeNumber))]
    public BusinessObjectCollection<OrganizationalUnit> ApplicableOrganizationalUnits
    {
      get
      {
        if (mApplicableOrganizationalUnits == null) using (new EventStateSuppressor(this)) { mApplicableOrganizationalUnits = new AssociativeObjectCollection<DocumentTypeNumber, OrganizationalUnit>(this); }
        return mApplicableOrganizationalUnits;
      }
    }

    #endregion

    #region Associative BusinessObjectCollection<GlobalOperation> Operations

    public const string OperationsProperty = "Operations";
    AssociativeObjectCollection<DocumentTypeGlobalOperation, GlobalOperation> mOperations;
    [PersistantCollection(AssociativeType = typeof(DocumentTypeGlobalOperation))]
    public BusinessObjectCollection<GlobalOperation> Operations
    {
      get
      {
        if (mOperations == null) using (new EventStateSuppressor(this)) { mOperations = new AssociativeObjectCollection<DocumentTypeGlobalOperation, GlobalOperation>(this); }
        return mOperations;
      }
    }

    #endregion

    public IEnumerable<GlobalOperation> HeirarchyOperations
    {
      get
      {
        if (!IsNull(BaseDocument)) return BaseDocument.HeirarchyOperations.Union(Operations);
        return Operations;
      }
    }

    #region bool IsAbstract

    public const string IsAbstractProperty = "IsAbstract";
    [DataMember(Name = IsAbstractProperty, EmitDefaultValue = false)]
    bool mIsAbstract = false;
    [PersistantProperty]
    public bool IsAbstract
    {
      get { return mIsAbstract; }
      set
      {
        if (SetProperty<bool>(ref mIsAbstract, value))
        {
          Validate();
        }
      }
    }

    #endregion

    #region Validation

    protected override void GetErrors(Collection<string> errors, string propertyName)
    {
      if (propertyName == null || propertyName == NameProperty)
      {
        if (string.IsNullOrWhiteSpace(Name)) errors.Add("Name is a mandatory field.");
      }

      if (propertyName == null || propertyName == StatesProperty)
      {
        if (IsNull(InitialState) && !IsAbstract) errors.Add("Initial State is a mandatory property.");
      }

      base.GetErrors(errors, propertyName);
    }

    #endregion

    #region BusinessObjectCollection<DocumentTypeReference> References

    public const string ReferencesProperty = "References";
    BusinessObjectCollection<DocumentTypeReference> mReferences;
    [PersistantCollection]
    [DataMember]
    public BusinessObjectCollection<DocumentTypeReference> References
    {
      get { return mReferences ?? (mReferences = new BusinessObjectCollection<DocumentTypeReference>(this)); }
    }

    #endregion

    #region BusinessObjectCollection<DocumentTypeScanType> ScanTypes

    public const string ScanTypesProperty = "ScanTypes";
    BusinessObjectCollection<DocumentTypeScanType> mScanTypes;
    [PersistantCollection]
    [DataMember]
    public BusinessObjectCollection<DocumentTypeScanType> ScanTypes
    {
      get { return mScanTypes ?? (mScanTypes = new BusinessObjectCollection<DocumentTypeScanType>(this)); }
    }

    #endregion

    IEnumerable<GlobalOperation> IGlobalOperationsOwner.Operations
    {
      get { return HeirarchyOperations; }
    }

    void IGlobalOperationsOwner.AddOperation(GlobalOperation op)
    {
      Operations.Add(op);
    }
  }
}

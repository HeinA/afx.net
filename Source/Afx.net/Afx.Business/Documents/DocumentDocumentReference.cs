﻿using Afx.Business.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.Documents
{
  [DataContract(IsReference = true, Namespace = Afx.Business.Namespace.Afx)]
  public class DocumentDocumentReference : BusinessObject<Document> 
  {
    #region Constructors

    public DocumentDocumentReference()
    {
    }

    #endregion

    #region DocumentType DocumentType

    public const string DocumentTypeProperty = "DocumentType";
    [PersistantProperty(IsCached = true)]
    public DocumentType DocumentType
    {
      get { return GetCachedObject<DocumentType>(); }
      set { SetCachedObject<DocumentType>(value); }
    }

    #endregion

    #region Guid DocumentIdentifier

    public const string DocumentIdentifierProperty = "DocumentIdentifier";
    [DataMember(Name = DocumentIdentifierProperty, EmitDefaultValue = false)]
    Guid mDocumentIdentifier;
    [PersistantProperty]
    public Guid DocumentIdentifier
    {
      get { return mDocumentIdentifier; }
      set { SetProperty<Guid>(ref mDocumentIdentifier, value); }
    }

    #endregion
  }
}

﻿using Afx.Business.Activities;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.Documents
{
  [DataContract(IsReference = true, Namespace = Namespace.Afx)]
  [PersistantObject(Schema = "Afx")]
  public class DocumentTypeGlobalOperation : AssociativeObject<DocumentType, GlobalOperation>
  {
    #region int SortOrder

    public const string SortOrderProperty = "SortOrder";
    [DataMember(Name = SortOrderProperty, EmitDefaultValue = false)]
    int mSortOrder;
    [PersistantProperty(SortOrder = 0)]
    public int SortOrder
    {
      get { return mSortOrder; }
      set { SetProperty<int>(ref mSortOrder, value); }
    }

    #endregion

    #region bool ReadOnlyAvailability

    public const string ReadOnlyAvailabilityProperty = "ReadOnlyAvailability";
    [DataMember(Name = ReadOnlyAvailabilityProperty, EmitDefaultValue = false)]
    bool mReadOnlyAvailability;
    [PersistantProperty]
    public bool ReadOnlyAvailability
    {
      get { return mReadOnlyAvailability; }
      set { SetProperty<bool>(ref mReadOnlyAvailability, value); }
    }

    #endregion

    #region Associative BusinessObjectCollection<Role> Roles

    public const string RolesProperty = "Roles";
    AssociativeObjectCollection<DocumentTypeGlobalOperationRole, Role> mRoles;
    [PersistantCollection(AssociativeType = typeof(DocumentTypeGlobalOperationRole))]
    public BusinessObjectCollection<Role> Roles
    {
      get
      {
        if (mRoles == null) using (new EventStateSuppressor(this)) { mRoles = new AssociativeObjectCollection<DocumentTypeGlobalOperationRole, Role>(this); }
        return mRoles;
      }
    }

    #endregion
  }
}

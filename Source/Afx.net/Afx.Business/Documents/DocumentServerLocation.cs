﻿using Afx.Business.Data;
using Afx.Business.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.Documents
{
  [PersistantObject(Schema = "Afx", OwnerColumn="Document")]
  [DataContract(IsReference = true, Namespace = Namespace.Afx)]
  public class DocumentServerLocation : BusinessObject<Document>
  {
    public DocumentServerLocation()
    {
    }

    public DocumentServerLocation(ServerInstance server)
    {
      using (new EventStateSuppressor(this))
      {
        Server = server;
      }
    }

    #region ServerInstance Server

    public const string ServerProperty = "Server";
    [PersistantProperty]
    public ServerInstance Server
    {
      get { return GetCachedObject<ServerInstance>(); }
      set { SetCachedObject<ServerInstance>(value); }
    }

    #endregion

  }
}

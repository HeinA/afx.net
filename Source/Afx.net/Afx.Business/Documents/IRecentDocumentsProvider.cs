﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.Documents
{
  public interface IRecentDocumentsProvider
  {
    Guid DocumentType { get; }
    IViewModelBase
    Collection<RecentDocumentBase> GetRecentDocuments();
  }
}

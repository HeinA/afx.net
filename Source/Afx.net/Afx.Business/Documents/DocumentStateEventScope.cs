﻿using Afx.Business.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.Documents
{
  [DataContract(IsReference = true, Namespace = Namespace.Afx)]
  public class DocumentStateEventScope : BusinessObject
  {
    public DocumentStateEventScope()
    {
    }

    public DocumentStateEventScope(OrganizationalUnit targetOU)
    {
      TargetOU = targetOU;
    }

    #region OrganizationalUnit TargetOU

    public const string TargetOUProperty = "TargetOU";
    [DataMember(Name = TargetOUProperty, EmitDefaultValue = false)]
    OrganizationalUnit mTargetOU;
    public OrganizationalUnit TargetOU
    {
      get { return mTargetOU; }
      protected set { SetProperty<OrganizationalUnit>(ref mTargetOU, value); }
    }

    #endregion
  }
}

﻿using Afx.Business;
using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace Afx.Business.Documents
{
  [PersistantObject(Schema = "Afx", OwnerColumn = "DocumentType")]
  public partial class DocumentTypeReference : BusinessObject<DocumentType> 
  {
    #region Constructors

    public DocumentTypeReference()
    {
    }

    #endregion

    #region string Name

    public const string NameProperty = "Name";
    [DataMember(Name = NameProperty, EmitDefaultValue = false)]
    string mName;
    [PersistantProperty]
    public string Name
    {
      get { return mName; }
      set { SetProperty<string>(ref mName, value); }
    }

    #endregion
  }
}

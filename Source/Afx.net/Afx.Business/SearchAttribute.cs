﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.Attributes
{
  /// <summary>
  /// The property or owned collection is searchable
  /// </summary>
  [AttributeUsage(AttributeTargets.Property)]
  public class SearchAttribute : Attribute
  {
    string mAlias;
    /// <summary>
    /// The alias of the searchable property, for return to the UI
    /// </summary>
    public string Alias
    {
      get { return mAlias; }
      set { mAlias = value; }
    }

    string mFormat;
    /// <summary>
    /// The format that should be applied to the resulting values.
    /// </summary>
    public string Format
    {
      get { return mFormat; }
      set { mFormat = value; }
    }
  }
}

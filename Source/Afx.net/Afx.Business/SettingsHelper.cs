﻿using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business
{
  public static class SettingsHelper
  {
    public static BasicCollection<Setting> LoadSettings(IPersistanceManager pm, string machine)
    {
      try
      {
        using (new ConnectionScope())
        {
          BasicCollection<Setting> ret = new BasicCollection<Setting>();
          BasicCollection<Setting> col = pm.GetRepository<Setting>().GetAllInstances(); // GetAllInstances<Setting>();
          foreach (Type t in ExtensibilityManager.GetExportedTypes<Setting>())
          {
            if (typeof(MachineSetting).IsAssignableFrom(t))
            {
              MachineSetting ms = col.OfType<MachineSetting>().Where(s => s.GetType() == t && s.MachineName == machine).FirstOrDefault();
              if (ms == null)
              {
                ms = (MachineSetting)Activator.CreateInstance(t);
                ms.MachineName = machine;
              }
              ret.Add(ms);
            }
            else if (typeof(UserSetting).IsAssignableFrom(t))
            {
              if (Afx.Business.Security.SecurityContext.User != null)
              {
                UserSetting us = col.OfType<UserSetting>().Where(s => s.GetType() == t && s.User.GlobalIdentifier == Afx.Business.Security.SecurityContext.User.GlobalIdentifier).FirstOrDefault();
                if (us == null)
                {
                  us = (UserSetting)Activator.CreateInstance(t);
                  us.User = Cache.Instance.GetObject<UserReference>(Afx.Business.Security.SecurityContext.User.GlobalIdentifier);
                }
                ret.Add(us);
              }
            }
            else
            {
              Setting s1 = col.OfType<Setting>().Where(s => s.GetType() == t).FirstOrDefault();
              if (s1 == null)
              {
                s1 = (Setting)Activator.CreateInstance(t);
              }
              ret.Add(s1);
            }
          }

          return ret;
        }
      }
      catch
      {
        throw;
      }
    }
  }
}

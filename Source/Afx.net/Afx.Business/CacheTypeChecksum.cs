﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Business
{
  [Serializable]
  public class CacheTypeChecksum
  {
    Type _objectType;
    public Type ObjectType
    {
      get { return _objectType; }
      set { _objectType = value; }
    }

    int _id;
    public int Id
    {
      get { return _id; }
      set { _id = value; }
    }

    int _checksum;
    public int Checksum
    {
      get { return _checksum; }
      set { _checksum = value; }
    }

    public static byte[] Compress(Collection<CacheTypeChecksum> col)
    {
      var bf = new BinaryFormatter();
      using (var ms = new MemoryStream())
      {
        using (DeflateStream zip = new DeflateStream(ms, CompressionMode.Compress, true))
        {
          bf.Serialize(zip, col);
        }
        return ms.ToArray();
      }
    }

    public static Collection<CacheTypeChecksum> Decompress(byte[] compressed)
    {
      var bf = new BinaryFormatter();
      using (var ms = new MemoryStream(compressed))
      {
        using (DeflateStream zip = new DeflateStream(ms, CompressionMode.Decompress, true))
        {
          return (Collection<CacheTypeChecksum>)bf.Deserialize(zip);
        }
      }
    }
  }
}

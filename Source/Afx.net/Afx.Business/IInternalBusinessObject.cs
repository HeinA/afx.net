﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business
{
  interface IInternalBusinessObject : IBusinessObject
  {
    void OnCompositionChanged(CompositionChangedType compositionChangedType, string propertyName, object originalSource, BusinessObject item);
    void AddChild(BusinessObject child);
    void RemoveChild(BusinessObject child);
    bool SuppressEventState { get; set; }

    IAssociativeObject GetAssociativeObject<T>(Guid id) where T : IAssociativeObject;
    IEnumerable GetAssociativeObjects(Type type);
    BusinessObject SetAssociativeObject<T>(T obj) where T : IAssociativeObject;
  }
}

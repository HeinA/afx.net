﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.Tabular
{
  public interface ITableCell
  {
    ITableRow Row { get; set; }
    ITableColumn Column { get; set; }
  }
}

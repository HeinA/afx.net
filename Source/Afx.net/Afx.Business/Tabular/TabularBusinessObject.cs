﻿using Afx.Business.Collections;
using Afx.Business.Data;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.Tabular
{
  [PersistantObject(Schema = "Afx", PersistToDerivedClass = true)]
  [DataContract(IsReference = true, Namespace = Namespace.Afx)]
  [NonCacheable]
  public abstract class TabularBusinessObject<TOwner, TRow, TColumn, TCell> : TabularBusinessObject<TRow, TColumn, TCell>
    where TOwner : BusinessObject
    where TRow : BusinessObject, ITableRow
    where TColumn : BusinessObject, ITableColumn
    where TCell : BusinessObject, ITableCell, new()
  {
    public TOwner Owner
    {
      get { return ((TOwner)((IBusinessObject)this).Owner) ?? (ObjectAttributeHelper.IsCached(typeof(TOwner)) ? Cache.Instance.GetObject<TOwner>(GetReferenceValue(OwnerProperty)) : null); }
    }

    protected internal override bool IsOwned
    {
      get { return true; }
    }

    protected internal override bool IsOwnedBy(Type ownerType)
    {
      return typeof(TOwner).IsAssignableFrom(ownerType);
    }
  }



  [PersistantObject(Schema = "Afx", PersistToDerivedClass = true)]
  [DataContract(IsReference = true, Namespace = Namespace.Afx)]
  public abstract class TabularBusinessObject<TRow, TColumn, TCell> : BusinessObject, ITabularBusinessObject
    where TRow : BusinessObject, ITableRow
    where TColumn : BusinessObject, ITableColumn
    where TCell : BusinessObject, ITableCell, new()
  {
    #region BusinessObjectCollection<TRow> RowCollection
    
    public const string RowCollectionProperty = "RowCollection";
    BusinessObjectCollection<TRow> mRowCollection;
    [PersistantCollection]
    [DataMember]
    protected BusinessObjectCollection<TRow> RowCollection
    {
      get { return mRowCollection ?? (mRowCollection = new BusinessObjectCollection<TRow>(this)); }
    }
    
    #endregion

    #region BusinessObjectCollection<TColumn> ColumnCollection
    
    public const string ColumnCollectionProperty = "ColumnCollection";
    BusinessObjectCollection<TColumn> mColumnCollection;
    [PersistantCollection]
    [DataMember]
    protected BusinessObjectCollection<TColumn> ColumnCollection
    {
      get { return mColumnCollection ?? (mColumnCollection = new BusinessObjectCollection<TColumn>(this)); }
    }
    
    #endregion

    BasicCollection<TRow> mRows;
    public IEnumerable<TRow> Rows
    {
      get { return mRows ?? (mRows = new BasicCollection<TRow>(RowCollection.OrderBy(r => r.Index))); }
    }

    IEnumerable ITabularBusinessObject.Rows
    {
      get { return Rows; }
    }

    BasicCollection<TColumn> mColumns;
    public IEnumerable<TColumn> Columns
    {
      get { return mColumns ?? (mColumns = new BasicCollection<TColumn>(ColumnCollection.OrderBy(c => c.Index))); }
    }

    IEnumerable ITabularBusinessObject.Columns
    {
      get { return Columns; }
    }

    public void AddRow(TRow row)
    {
      RowCollection.Add(row);
    }

    public void RemoveRow(TRow row)
    {
      RowCollection.Remove(row);
      foreach(var c in Cells.Where(c1 => c1.Row.Equals(row)).ToArray())
      {
        Cells.Remove(c);
      }
    }

    public void AddColumn(TColumn column)
    {
      ColumnCollection.Add(column);
    }

    public void RemoveColumn(TColumn column)
    {
      ColumnCollection.Remove(column);
      foreach (var c in Cells.Where(c1 => c1.Column.Equals(column)).ToArray())
      {
        Cells.Remove(c);
      }
    }

    public TCell this[int row, int column]
    {
      get { return (TCell)Rows.ElementAt(row)[column]; }
    }

    public TCell this[TRow row, TColumn column]
    {
      get { return (TCell)row[Columns.IndexOf(column)]; }
    }

    #region BusinessObjectCollection<TCell> Cells

    protected const string CellsProperty = "Cells";
    BusinessObjectCollection<TCell> mCells;
    [PersistantCollection]
    [DataMember]
    protected internal BusinessObjectCollection<TCell> Cells
    {
      get { return mCells ?? (mCells = new BusinessObjectCollection<TCell>(this)); }
    }

    #endregion

    IEnumerable ITabularBusinessObject.Cells
    {
      get { return Cells; }
    }

    protected override void OnCompositionChanged(CompositionChangedType compositionChangedType, string propertyName, object originalSource, BusinessObject item)
    {
      try
      {
        if (!IsDeserializing)
        {
        //  if (compositionChangedType == CompositionChangedType.PropertyChanged && propertyName == BusinessObject.IsDeletedProperty && (item is TRow || item is TColumn))
        //  {
        //    if (item is TRow)
        //    {
        //      ITableRow row = item as ITableRow;
        //      foreach (BusinessObject c in row.Cells)
        //      {
        //        c.IsDeleted = ((BusinessObject)row).IsDeleted;
        //      }
        //    }

        //    if (item is TColumn)
        //    {
        //      TColumn col = item as TColumn;
        //      foreach (var r in RowCollection)
        //      {
        //        this[r, col].IsDeleted = col.IsDeleted;
        //      }
        //    }
        //  }

          if (compositionChangedType == CompositionChangedType.ChildAdded || compositionChangedType == CompositionChangedType.ChildRemoved && (propertyName == RowCollectionProperty || propertyName == ColumnCollectionProperty))
          {
            if (propertyName == RowCollectionProperty)
            {
              ITableRow row = item as ITableRow;

              if (compositionChangedType == CompositionChangedType.ChildAdded)
              {
                foreach (ITableColumn col in ColumnCollection)
                {
                  TCell cell = new TCell();
                  cell.Row = row;
                  cell.Column = col;
                  Cells.Add(cell);
                }
              }

              if (compositionChangedType == CompositionChangedType.ChildRemoved)
              {
                Stack<TCell> removed = new Stack<TCell>();
                foreach (var c in row.Cells)
                {
                  removed.Push((TCell)c);
                }
                while (removed.Count > 0)
                {
                  Cells.Remove(removed.Pop());
                }
              }
            }

            if (propertyName == ColumnCollectionProperty)
            {
              ITableColumn col = item as ITableColumn;

              if (compositionChangedType == CompositionChangedType.ChildAdded)
              {
                foreach (ITableRow row in RowCollection)
                {
                  TCell cell = new TCell();
                  cell.Row = row;
                  cell.Column = col;
                  Cells.Add(cell);
                  row.ResetCells();
                }
              }

              if (compositionChangedType == CompositionChangedType.ChildRemoved)
              {
                Stack<TCell> removed = new Stack<TCell>();
                foreach (var r in RowCollection)
                {
                  removed.Push(this[r, (TColumn)col]);
                }
                while (removed.Count > 0)
                {
                  Cells.Remove(removed.Pop());
                }
              }
            }

            OnLayoutChanged();
          }
          else
          {
            if (!typeof(TCell).IsAssignableFrom(originalSource.GetType()) && !originalSource.Equals(this))
            {
              OnLayoutChanged();
            }
          }
        }
      }
      catch
      {
        throw;
      }

      base.OnCompositionChanged(compositionChangedType, propertyName, originalSource, item);
    }

    protected virtual void OnLayoutChanged()
    {
      mColumns = null;
      mRows = null;
      OnPropertyChanged(null);
      //OnPropertyChanged("Columns");

      if (LayoutChanged != null) LayoutChanged(this, EventArgs.Empty);
    }

    public event EventHandler LayoutChanged;
  }
}

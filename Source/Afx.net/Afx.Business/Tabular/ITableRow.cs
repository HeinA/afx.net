﻿using Afx.Business.Collections;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.Tabular
{
  public interface ITableRow 
  { 
    IEnumerable Cells { get; }
    ITableCell this[int column] { get; }
    object Index { get; }
    void ResetCells();
  }
}

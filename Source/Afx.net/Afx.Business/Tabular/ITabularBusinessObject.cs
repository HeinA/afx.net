﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.Tabular
{
  public interface ITabularBusinessObject  
  {
    IEnumerable Cells { get; }
    IEnumerable Columns { get; }
    IEnumerable Rows { get; }

    event EventHandler LayoutChanged;
  }
}

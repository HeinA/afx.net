﻿using Afx.Business.Collections;
using Afx.Business.Data;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.Tabular
{
  [PersistantObject(Schema = "Afx", PersistToDerivedClass = true)]
  [DataContract(IsReference = true, Namespace = Namespace.Afx)]
  [NonCacheable]
  public abstract class TableRow<TTable, TCell> : BusinessObject<TTable>, ITableRow
    where TTable : BusinessObject, ITabularBusinessObject
    where TCell : BusinessObject, ITableCell
  {
    IEnumerable ITableRow.Cells
    {
      get { return Cells; }
    }

    BasicCollection<TCell> mCells;
    public IEnumerable<TCell> Cells
    {
      get { return mCells ?? (mCells = new BasicCollection<TCell>(Owner.Cells.Cast<ITableCell>().Where(c => c.Row.Equals(this)).OrderBy(c => c.Column.Index).Cast<TCell>())); }
    }

    ITableCell ITableRow.this[int column]
    {
      get { return ((BasicCollection<TCell>)Cells)[column]; }
    }

    public TCell this[int column]
    {
      get { return ((BasicCollection<TCell>)Cells)[column]; }
    }

    public const string IndexProperty = "Index";
    public abstract object Index { get; }

    void ITableRow.ResetCells()
    {
      mCells = null;
      OnPropertyChanged("Cells");
    }
  }
}

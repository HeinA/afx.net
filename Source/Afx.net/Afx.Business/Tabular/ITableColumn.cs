﻿using Afx.Business.Collections;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.Tabular
{
  public interface ITableColumn
  {
    object Index { get; }
  }
}

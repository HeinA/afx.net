﻿using Afx.Business.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.Tabular
{
  [PersistantObject(Schema = "Afx", PersistToDerivedClass = true)]
  [DataContract(IsReference = true, Namespace = Namespace.Afx)]
  [NonCacheable]
  public abstract class TableCell<TTable, TRow, TColumn> : BusinessObject<TTable>, ITableCell
    where TTable : BusinessObject, ITabularBusinessObject
    where TRow : BusinessObject, ITableRow
    where TColumn : BusinessObject, ITableColumn
  {
    #region TRow Row

    public const string RowProperty = "Row";
    [DataMember(Name = RowProperty, EmitDefaultValue = false)]
    TRow mRow;
    [PersistantProperty]
    public TRow Row
    {
      get { return mRow; }
      set { SetProperty<TRow>(ref mRow, value); }
    }

    #endregion

    #region TColumn Column

    public const string ColumnProperty = "Column";
    [DataMember(Name = ColumnProperty, EmitDefaultValue = false)]
    TColumn mColumn;
    [PersistantProperty]
    public TColumn Column
    {
      get { return mColumn; }
      set { SetProperty<TColumn>(ref mColumn, value); }
    }

    #endregion

    #region ITableCell

    ITableRow ITableCell.Row
    {
      get { return Row; }
      set { Row = (TRow)value; }
    }

    ITableColumn ITableCell.Column
    {
      get { return Column; }
      set { Column = (TColumn)value; }
    }

    #endregion

    public override bool IsDeleted
    {
      get
      {
        if (Row.IsDeleted || Column.IsDeleted)
        {
          base.IsDeleted = true;
        }
        return base.IsDeleted;
      }
      set
      {
        base.IsDeleted = value;
      }
    }

    public override string ToString()
    {
      return string.Format("[{0}:{1}]{2}", Row.IsDeleted ? "Deleted" : "Active", Column.IsDeleted ? "Deleted" : "Active", this.IsDeleted ? "Deleted" : "Active");
    }
  }
}

﻿using Afx.Business.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.Tabular
{
  [PersistantObject(Schema = "Afx", PersistToDerivedClass = true)]
  [DataContract(IsReference = true, Namespace = Namespace.Afx)]
  [NonCacheable]
  public abstract class TableColumn<TTable> : BusinessObject<TTable>, ITableColumn
    where TTable : BusinessObject, ITabularBusinessObject
  {
    public const string IndexProperty = "Index";
    public abstract object Index { get; }
  }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business
{
  class PropertyError
  {
    public PropertyError(string propertyName, string error)
    {
      PropertyName = propertyName;
      Error = error;
    }

    #region string PropertyName

    string mPropertyName;
    public string PropertyName
    {
      get { return mPropertyName; }
      private set { mPropertyName = value; }
    }

    #endregion

    #region string Error

    string mError;
    public string Error
    {
      get { return mError; }
      private set { mError = value; }
    }

    #endregion
  }
}

﻿using Afx.Business.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business
{
  [DataContract(IsReference = true, Namespace = Afx.Business.Namespace.Afx)]
  [PersistantObject(Schema = "Afx")]
  public class GlobalOperation : Operation
  {
    public GlobalOperation()
    {
    }

    protected GlobalOperation(GlobalOperation op)
      :base(op)
    {
    }

    public override Operation CreateReplicate()
    {
      return new GlobalOperation(this);
    }
  }
}

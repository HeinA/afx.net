﻿using Afx.Business.Attributes;
using Afx.Business.Data;
using Afx.Business.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.Activities
{
  [DataContract(IsReference = true, Namespace = Namespace.Afx)]
  [PersistantObject(Schema = "Afx")]
  public class ActivityGlobalOperationRole : AssociativeObject<ActivityGlobalOperation, Role>
  {
  }
}

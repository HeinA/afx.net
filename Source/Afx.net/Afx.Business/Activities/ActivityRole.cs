﻿using Afx.Business.Attributes;
using Afx.Business.Data;
using Afx.Business.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.Activities
{
  [DataContract(IsReference = true, Namespace = Namespace.Afx)]
  [PersistantObject(Schema = "Afx")] 
  public class ActivityRole : AssociativeObject<Activity, Role>
  {
    #region DataMember bool EditPermission

    [DataMember(Name = "EditPermission", EmitDefaultValue = false)]
    bool mEditPermission;
    [PersistantProperty]
    public bool EditPermission
    {
      get { return mEditPermission; }
      set { SetProperty<bool>(ref mEditPermission, value); }
    }

    #endregion
  }
}

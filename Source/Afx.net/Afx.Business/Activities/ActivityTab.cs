﻿using Afx.Business.Collections;
using Afx.Business.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.Activities
{
  [Cache]
  [DataContract(IsReference = true, Namespace = Afx.Business.Namespace.Afx)]
  [PersistantObject(Schema = "Afx")]
  public class ActivityTab : BusinessObject, INamespaceSensitive, IRevisable
  {
    #region int Revision

    public const string RevisionProperty = "Revision";
    [DataMember(Name = RevisionProperty, EmitDefaultValue = false)]
    int mRevision = 1;
    [PersistantProperty]
    public int Revision
    {
      get { return mRevision; }
      set { SetProperty<int>(ref mRevision, value); }
    }

    #endregion

    #region DataMember string Name

    [DataMember(Name = "Name", EmitDefaultValue = false)]
    string mName;
    [PersistantProperty]
    public string Name
    {
      get { return mName; }
      set { SetProperty<string>(ref mName, value); }
    }

    #endregion

    #region DataMember BusinessObjectCollection<Activity> Activities

    [DataMember(Name = "Groups")]
    BusinessObjectCollection<ActivityGroup> mGroups;
    [PersistantCollection]
    public BusinessObjectCollection<ActivityGroup> Groups
    {
      get
      {
        if (mGroups == null) mGroups = new BusinessObjectCollection<ActivityGroup>(this);
        return mGroups;
      }
    }

    #endregion

    #region string Namespace

    public const string NamespaceProperty = "Namespace";
    [DataMember(Name = NamespaceProperty, EmitDefaultValue = false)]
    string mNamespace;
    [PersistantProperty]
    public string Namespace
    {
      get { return mNamespace; }
      set { SetProperty<string>(ref mNamespace, value); }
    }

    #endregion
  }
}

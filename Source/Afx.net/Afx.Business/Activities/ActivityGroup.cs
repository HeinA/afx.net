﻿using Afx.Business.Collections;
using Afx.Business.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.Activities
{
  [DataContract(IsReference = true, Namespace = Afx.Business.Namespace.Afx)]
  [PersistantObject(Schema = "Afx")]
  public class ActivityGroup : BusinessObject<ActivityTab>, INamespaceSensitive
  {
    #region DataMember string GroupName

    [DataMember(Name = "GroupName", EmitDefaultValue = false)]
    string mGroupName;
    [PersistantProperty]
    public string GroupName
    {
      get { return mGroupName; }
      set { SetProperty<string>(ref mGroupName, value); }
    }

    #endregion

    #region DataMember BusinessObjectCollection<Activity> Activities

    [DataMember(Name = "Activities")]
    BusinessObjectCollection<Activity> mActivities;
    [PersistantCollection]
    public BusinessObjectCollection<Activity> Activities
    {
      get
      {
        if (mActivities == null) mActivities = new BusinessObjectCollection<Activity>(this);
        return mActivities;
      }
    }

    #endregion

    #region string Namespace

    public const string NamespaceProperty = "Namespace";
    [DataMember(Name = NamespaceProperty, EmitDefaultValue = false)]
    string mNamespace;
    [PersistantProperty]
    public string Namespace
    {
      get { return mNamespace; }
      set { SetProperty<string>(ref mNamespace, value); }
    }

    #endregion
  }
}

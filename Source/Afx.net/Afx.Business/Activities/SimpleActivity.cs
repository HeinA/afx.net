﻿using Afx.Business.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.Activities
{
  [DataContract(IsReference = true, Namespace = Afx.Business.Namespace.Afx)]
  [PersistantObject(Schema = "Afx")]
  public class SimpleActivity : Activity
  {
  }
}

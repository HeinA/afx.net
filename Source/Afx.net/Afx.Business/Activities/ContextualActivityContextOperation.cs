﻿using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.Activities
{
  [DataContract(IsReference = true, Namespace = Namespace.Afx)]
  [PersistantObject(Schema = "Afx")]
  public class ContextualActivityContextOperation : AssociativeObject<ContextualActivity, ContextOperation>
  {
    #region int SortOrder

    public const string SortOrderProperty = "SortOrder";
    [DataMember(Name = SortOrderProperty, EmitDefaultValue = false)]
    int mSortOrder;
    [PersistantProperty(SortOrder = 0)]
    public int SortOrder
    {
      get { return mSortOrder; }
      set { SetProperty<int>(ref mSortOrder, value); }
    }

    #endregion

    #region DataMember BusinessObjectCollection<Role> Roles

    AssociativeObjectCollection<ContextualActivityContextOperationRole, Role> mRoles;
    [PersistantCollection(AssociativeType = typeof(ContextualActivityContextOperationRole))]
    public BusinessObjectCollection<Role> Roles
    {
      get
      {
        if (mRoles == null) using (new EventStateSuppressor(this)) { mRoles = new AssociativeObjectCollection<ContextualActivityContextOperationRole, Role>(this); }
        return mRoles;
      }
    }

    #endregion
  }
}

﻿using Afx.Business.Collections;
using Afx.Business.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.Activities
{
  [DataContract(IsReference = true, Namespace = Afx.Business.Namespace.Afx)]
  [PersistantObject(Schema = "Afx")]
  public class ContextualActivity : Activity
  {
    #region DataMember BusinessObjectCollection<ContextualContextOperation> ContextOperations

    AssociativeObjectCollection<ContextualActivityContextOperation, ContextOperation> mContextOperations;
    [PersistantCollection(AssociativeType = typeof(ContextualActivityContextOperation))]
    public BusinessObjectCollection<ContextOperation> ContextOperations
    {
      get
      {
        if (mContextOperations == null) using (new EventStateSuppressor(this)) { mContextOperations = new AssociativeObjectCollection<ContextualActivityContextOperation, ContextOperation>(this); }
        return mContextOperations;
      }
    }

    #endregion
  }
}

﻿using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.Activities
{
  [Cache]
  [DataContract(IsReference = true, Namespace = Afx.Business.Namespace.Afx)]
  [PersistantObject(Schema = "Afx", OwnerColumn="Group")]
  public abstract class Activity : BusinessObject, INamespaceSensitive, IRevisable
  {
    #region Constructors

    public Activity()
    {
      using (new EventStateSuppressor(this))
      {
        Id = 0;
      }
    }

    #endregion

    #region int Revision

    public const string RevisionProperty = "Revision";
    [DataMember(Name = RevisionProperty, EmitDefaultValue = false)]
    int mRevision = 1;
    [PersistantProperty]
    public int Revision
    {
      get { return mRevision; }
      set { SetProperty<int>(ref mRevision, value); }
    }

    #endregion

    #region DataMember string Name

    [DataMember(Name = "Name", EmitDefaultValue = false)]
    string mName;
    [PersistantProperty(SortOrder=0)]
    public string Name
    {
      get { return mName; }
      set { SetProperty<string>(ref mName, value); }
    }

    #endregion

    #region DataMember string Description

    [DataMember(Name = "Description", EmitDefaultValue = false)]
    string mDescription;
    [PersistantProperty]
    public string Description
    {
      get { return mDescription; }
      set { SetProperty<string>(ref mDescription, value); }
    }

    #endregion

    #region string Namespace

    public const string NamespaceProperty = "Namespace";
    [DataMember(Name = NamespaceProperty, EmitDefaultValue = false)]
    string mNamespace;
    [PersistantProperty]
    public string Namespace
    {
      get { return mNamespace; }
      set { SetProperty<string>(ref mNamespace, value); }
    }

    #endregion

    #region DataMember BusinessObjectCollection<Operation> GlobalOperations

    AssociativeObjectCollection<ActivityGlobalOperation, GlobalOperation> mGlobalOperations;
    [PersistantCollection(AssociativeType = typeof(ActivityGlobalOperation))]
    public BusinessObjectCollection<GlobalOperation> GlobalOperations
    {
      get
      {
        if (mGlobalOperations == null) using (new EventStateSuppressor(this)) { mGlobalOperations = new AssociativeObjectCollection<ActivityGlobalOperation, GlobalOperation>(this); }
        return mGlobalOperations;
      }
    }

    #endregion

    #region DataMember BusinessObjectCollection<Role> Roles

    AssociativeObjectCollection<ActivityRole, Role> mRoles;
    [PersistantCollection(AssociativeType = typeof(ActivityRole))]
    public BusinessObjectCollection<Role> Roles
    {
      get
      {
        if (mRoles == null) using (new EventStateSuppressor(this)) { mRoles = new AssociativeObjectCollection<ActivityRole, Role>(this); }
        return mRoles;
      }
    }

    #endregion

    #region bool CanEdit

    public bool CanEdit
    {
      get
      {
        foreach (Role r in SecurityContext.User.Roles)
        {
          if (Roles.Contains(r) && this.GetAssociativeObject<ActivityRole>(r).EditPermission) return true;
        }
        return false;
      }
    }

    #endregion

    #region bool CanView

    public bool CanView
    {
      get
      {
        foreach (Role r in SecurityContext.User.Roles)
        {
          if (Roles.Contains(r)) return true;
        }
        return false;
      }
    }

    #endregion

    #region bool CanExecute(Operation op)

    public bool CanExecute(Operation op)
    {
      if (op is GlobalOperation)
      {
        ActivityGlobalOperation aop = this.GetAssociativeObject<ActivityGlobalOperation>(op);
        foreach (Role r in SecurityContext.User.Roles)
        {
          if (aop.Roles.Contains(r)) return true;
        }
      }

      if (op is ContextOperation)
      {
        ContextualActivityContextOperation aop = this.GetAssociativeObject<ContextualActivityContextOperation>(op);
        foreach (Role r in SecurityContext.User.Roles)
        {
          if (aop.Roles.Contains(r)) return true;
        }
      }
      return false;
    }

    #endregion

    #region Validation

    protected override void GetErrors(System.Collections.ObjectModel.Collection<string> errors, string propertyName)
    {
      if (string.IsNullOrWhiteSpace(propertyName) || propertyName == "Name")
      {
        if (string.IsNullOrWhiteSpace(Name)) errors.Add("Name is a mandatory field.");
      }

      base.GetErrors(errors, propertyName);
    }

    #endregion
  }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Business
{
  public interface IGloballyIdentifiable
  {
    Guid GlobalIdentifier
    {
      get;
    }
  }
}

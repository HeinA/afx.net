﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business
{
  [DataContract(IsReference = true, Namespace = Namespace.Afx)]
  public class SqlLogParameter : BusinessObject
  {
    public SqlLogParameter(string name, object value)
    {
      Name = name;
      if (value != null) Value = value.ToString();
    }

    #region string Name

    public const string NameProperty = "Name";
    [DataMember(Name = NameProperty, EmitDefaultValue = false)]
    string mName;
    public string Name
    {
      get { return mName; }
      set { mName = value; }
    }

    #endregion

    #region string Value

    public const string ValueProperty = "Value";
    [DataMember(Name = ValueProperty, EmitDefaultValue = false)]
    string mValue = string.Empty;
    public string Value
    {
      get { return mValue; }
      set { mValue = value; }
    }

    #endregion
  }
}

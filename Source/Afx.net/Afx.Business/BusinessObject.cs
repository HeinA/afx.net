﻿using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Validation;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business
{
  [DataContract(IsReference = true, Namespace = Namespace.Afx)]
  [NonCacheable]
  public abstract class BusinessObject<TOwner> : BusinessObject
    where TOwner : BusinessObject
  {
    protected BusinessObject()
      : base()
    {
    }

    protected BusinessObject(bool isNull)
      :base(isNull)
    {
    }

    TOwner mTOwner;
    public TOwner Owner
    {
      get
      {
        if (IsDeserializing) return null;
        if (mTOwner == null) mTOwner = ((TOwner)((IBusinessObject)this).Owner) ?? (ObjectAttributeHelper.IsCached(typeof(TOwner)) ? Cache.Instance.GetObject<TOwner>(GetReferenceValue(OwnerProperty)) : null);
        return mTOwner;
      }
    }

    protected internal override bool IsOwned
    {
      get { return true; }
    }

    protected internal override bool IsOwnedBy(Type ownerType)
    {
      return typeof(TOwner).IsAssignableFrom(ownerType);
    }
  }

  [DataContract(IsReference = true, Namespace = Namespace.Afx)]
  [NonCacheable]
  public abstract class BusinessObject : IInternalBusinessObject
  {
    #region Constructors

#if DISPOSE
    ~BusinessObject()
    {
      Debug.WriteLine("- {0}", this.GetType());
    }
#endif

    protected BusinessObject()
    {
      ObjectCatalog.RegisterObject(this);
    }

    protected BusinessObject(bool isNull)
      : this()
    {
      if (isNull)
      {
        using (new EventStateSuppressor(this))
        {
          Id = 0;
          GlobalIdentifier = Guid.Empty;
        }
      }
    }

    ~BusinessObject()
    {
      ObjectCatalog.Purge();
    }

    #endregion

    #region Public Properties

    #region DataMember int Id

    static int mCurrentId = 0;
    public const string IdProperty = "Id";
    [DataMember(Name = IdProperty, EmitDefaultValue = false)]
    int mId = --mCurrentId;
    public int Id
    {
      get { return mId; }
      set { SetProperty<int>(ref mId, value, IdProperty); }
    }

    #endregion

    #region DataMember Guid GlobalIdentifier

    public const string GlobalIdentifierProperty = "GlobalIdentifier";
    [DataMember(Name = GlobalIdentifierProperty, EmitDefaultValue = false)]
    Guid mGlobalIdentifier = Guid.NewGuid();
    [PersistantProperty]
    public Guid GlobalIdentifier
    {
      get { return mGlobalIdentifier; }
      set { SetProperty<Guid>(ref mGlobalIdentifier, value, GlobalIdentifierProperty); }
    }

    #endregion

    #region string Identifier

    public virtual string Identifier
    {
      get { return GlobalIdentifier.ToString("B"); }
    }

    #endregion

    #region Flags

    [DataMember(EmitDefaultValue = false)]
    Collection<string> mFlags = new Collection<string>();

    [DataMember(EmitDefaultValue = false)]
    Collection<string> mRemovedFlags = new Collection<string>();

    public const string FlagsProperty = "Flags";
    public IEnumerable<string> Flags
    {
      get { return mFlags; }
    }

    public const string FlagsTextProperty = "FlagsText";
    public string FlagsText
    {
      get { return string.Join(", ", Flags); }
    }

    internal IEnumerable<string> RemovedFlags
    {
      get { return mRemovedFlags; }
    }

    public bool IsFlagged(string flag)
    {
      return Flags.Contains(flag);
    }

    public void SetFlag(string flag)
    {
      bool raise = false;

      if (mRemovedFlags.Contains(flag))
      {
        raise = true;
        mRemovedFlags.Remove(flag);
      }

      if (!mFlags.Contains(flag))
      {
        raise = true;
        mFlags.Add(flag);
      }

      if (raise && !SuppressEventState)
      {
        IsDirty = true;
        OnPropertyChanged(FlagsTextProperty);
        OnCompositionChanged(CompositionChangedType.PropertyChanged, FlagsProperty, this, null);
      }
    }

    public void UnFlag(string flag)
    {
      bool raise = false;

      if (mFlags.Contains(flag))
      {
        raise = true;
        mFlags.Remove(flag);
      }

      if (!mRemovedFlags.Contains(flag))
      {
        raise = true;
        mRemovedFlags.Add(flag);
      }

      if (raise && !SuppressEventState)
      {
        IsDirty = true;
        OnPropertyChanged(FlagsTextProperty);
        OnCompositionChanged(CompositionChangedType.PropertyChanged, FlagsProperty, this, null);
      }
    }

    #endregion

    #region DataMember bool IsNew

    const string IsNewProperty = "IsNew";
    [DataMember(Name = IsNewProperty, EmitDefaultValue = false)]
    bool mIsNew = true;
    public bool IsNew
    {
      get { return mIsNew; }
      set { mIsNew = value; }
    }

    #endregion

    #region DataMember bool IsDirty

    const string IsDirtyProperty = "IsDirty";
    [DataMember(Name = IsDirtyProperty, EmitDefaultValue = false)]
    bool mIsDirty = false;
    public virtual bool IsDirty
    {
      get { return mIsDirty; }
      set { mIsDirty = value; }
    }

    #endregion

    #region DataMember bool IsDeleted

    public const string IsDeletedProperty = "IsDeleted";
    [DataMember(Name = IsDeletedProperty, EmitDefaultValue = false)]
    bool mIsDeleted = false;
    public virtual bool IsDeleted
    {
      get { return mIsDeleted; }
      set
      {
        if (SetProperty<bool>(ref mIsDeleted, value, IsDeletedProperty))
        {
          OnDeleteStateChanged();
          foreach (var child in Children)
          {
            child.IsDeleted = value;
          }
          foreach (var child in ExtensionObjectCollection)
          {
            child.IsDeleted = value;
          }
          foreach (var child in AssociativeObjectCollection)
          {
            BusinessObject abo = child as BusinessObject;
            abo.IsDeleted = value;
          }
        }
      }
    }

    protected virtual void OnDeleteStateChanged()
    {
    }

    #endregion

    #endregion

    #region Protected Properties

    protected internal virtual bool IsOwnedBy(Type ownerType)
    {
      return false;
    }

    protected internal virtual bool IsOwned
    {
      get { return false; }
    }

    #endregion

    #region References

    [DataMember(Name = "References", EmitDefaultValue = false)]
    BasicCollection<PropertyReference> mReferences;
    BasicCollection<PropertyReference> References
    {
      get
      {
        if (mReferences == null) mReferences = new BasicCollection<PropertyReference>();
        return mReferences;
      }
    }

    protected internal bool SetReferenceValue(Guid value, [CallerMemberName] string propertyName = null)
    {
      if (string.IsNullOrWhiteSpace(propertyName)) throw new ArgumentException(Afx.Business.Properties.Resources.InvalidParameter, "propertiesChanged");

      PropertyReference pv = References.FirstOrDefault(p => p.PropertyName == propertyName);
      Guid oldValue = Guid.Empty;
      if (pv != null)
      {
        oldValue = (Guid)pv.Reference;
      }

      if (oldValue != value)
      {
        if (!SuppressEventState) SetOriginalValue(propertyName, oldValue);
        if (pv != null)
        {
          pv.Reference = value;
        }
        else
        {
          References.Add(new PropertyReference(propertyName, value));
        }
        if (!SuppressEventState)
        {
          IsDirty = true;
          OnPropertyChanged(propertyName);
          OnCompositionChanged(CompositionChangedType.PropertyChanged, propertyName, this, this);
        }
        return true;
      }

      return false;
    }

    protected internal Guid GetReferenceValue([CallerMemberName] string propertyName = null)
    {
      PropertyReference pv = References.FirstOrDefault(p => p.PropertyName == propertyName);
      if (pv != null) return (Guid)pv.Reference;
      return Guid.Empty;
    }

    protected bool SetChildObject<T>(T value, [CallerMemberName] string propertyName = null)
      where T : BusinessObject
    {
      try
      {
        PropertyReference pv = References.FirstOrDefault(p => p.PropertyName == propertyName);
        object oldValue = null;
        if (pv != null)
        {
          oldValue = pv.Reference;
        }

        if (!SuppressEventState) SetOriginalValue(propertyName, oldValue);
        if (pv != null)
        {
          pv.Reference = value.GlobalIdentifier;
        }
        else
        {
          References.Add(new PropertyReference(propertyName, value.GlobalIdentifier));
        }
        if (!SuppressEventState)
        {
          IsDirty = true;
          OnPropertyChanged(propertyName);
          OnCompositionChanged(CompositionChangedType.PropertyChanged, propertyName, this, this);
        }
      }
      catch
      {
        throw;
      }

      return true;
    }

    T GetNullObject<T>()
      where T : BusinessObject
    {
      ConstructorInfo ci = typeof(T).GetConstructor(new Type[] { typeof(bool) });
      if (ci == null) return null; 
      return (T)ci.Invoke(new object[] { true });
    }

    protected T GetChildObject<T>(IEnumerable list, [CallerMemberName] string propertyName = null)
      where T : BusinessObject
    {
      //if (IsDeserializing) return null;
      T ret = null;
      try
      {
        PropertyReference pv = References.FirstOrDefault(p => p.PropertyName == propertyName);
        if (pv != null)
        {

          Guid id = (Guid)pv.Reference;
          ret = list.Cast<T>().FirstOrDefault(i => i.GlobalIdentifier == id);
          //ret.Owner = this;
        }
        if (ret == null) ret = GetNullObject<T>();
      }
      catch
      {
        throw;
      }
      return ret;
    }

    protected T GetCachedObject<T>([CallerMemberName] string propertyName = null)
      where T : BusinessObject
    {
      Guid reference = GetReferenceValue(propertyName);
      if (reference == Guid.Empty)
      {
        ConstructorInfo ci = typeof(T).GetConstructor(new Type[] { typeof(bool) });
        if (ci == null || typeof(T).IsAbstract) return null; 
        return (T)ci.Invoke(new object[] { true });
      }
      return Cache.Instance.GetObject<T>(reference);
    }

    protected bool SetCachedObject<T>(T value, [CallerMemberName] string propertyName = null)
      where T : BusinessObject
    {
      if (WriteSuppressor.IsSuppressed) return false;

      if (value == null) return SetReferenceValue(Guid.Empty, propertyName);
      else return SetReferenceValue(value.GlobalIdentifier, propertyName);
    }

    protected bool SetOwnedReference<T>(ref T reference, T value, [CallerMemberName] string propertyName = null)
      where T : BusinessObject
    {
      if (WriteSuppressor.IsSuppressed) return false;

      if ((reference == null && value != null) || (reference != null && value == null) || (!reference.Equals(value)))
      {
        object o = null;
        if (reference != null) o = reference.GlobalIdentifier;
        GetOriginalValue(propertyName, ref o) ;
        if (!SuppressEventState)
        {
          Guid guid = (Guid)(o ?? (Guid.Empty));
          SetOriginalValue(propertyName, guid);
        }

        reference = value;
        value.Owner = this;
        if (!SuppressEventState)
        {
          IsDirty = true;
          OnPropertyChanged(propertyName);
          OnCompositionChanged(CompositionChangedType.PropertyChanged, propertyName, this, this);
        }
        return true;
      }
      return false;
    }

    #endregion

    #region Associative Objects

    BasicCollection<IAssociativeObject> mAssociativeObjectCollection;
    [DataMember(Name = "AssociativeObjects", EmitDefaultValue = false)]
    internal BasicCollection<IAssociativeObject> AssociativeObjectCollection
    {
      get
      {
        if (mAssociativeObjectCollection == null && !IsSerializing) mAssociativeObjectCollection = new BasicCollection<IAssociativeObject>();
        if (mAssociativeObjectCollection != null && IsSerializing && mAssociativeObjectCollection.Count == 0) return null;
        return mAssociativeObjectCollection;
      }
      set { mAssociativeObjectCollection = value; }
    }

    IEnumerable<IAssociativeObject> IBusinessObject.AssociativeObjects
    {
      get { return AssociativeObjectCollection; }
    }

    IAssociativeObject IInternalBusinessObject.GetAssociativeObject<T>(Guid id)
    {
      return (T)GetAssociativeObject<T>(id);
    }

    public T GetAssociativeObject<T>(Guid id)
      where T : IAssociativeObject
    {
      return (T)AssociativeObjectCollection.FirstOrDefault(o => o.GetType() == typeof(T) && o.ReferenceId == id);
    }

    public T GetAssociativeObject<T>(BusinessObject obj)
      where T : IAssociativeObject
    {
      return (T)AssociativeObjectCollection.FirstOrDefault(o => o.GetType() == typeof(T) && o.ReferenceId == obj.GlobalIdentifier);
    }

    internal IAssociativeObject GetAssociativeObject(IAssociativeObject obj)
    {
      return AssociativeObjectCollection.FirstOrDefault(o => o.GetType() == obj.GetType() && o.ReferenceId == obj.ReferenceId);
    }

    internal IAssociativeObject GetAssociativeObject(Type type, Guid id)
    {
      return AssociativeObjectCollection.FirstOrDefault(o => o.GetType() == type && o.ReferenceId == id);
    }

    IEnumerable IInternalBusinessObject.GetAssociativeObjects(Type type)
    {
      return GetAssociativeObjects(type);
    }

    internal IEnumerable<IAssociativeObject> GetAssociativeObjects(Type type)
    {
      if (AssociativeObjectCollection == null) return new Collection<IAssociativeObject>();
      IEnumerable<IAssociativeObject> col = AssociativeObjectCollection.Where(o => o.GetType() == type);
      foreach (BusinessObject o in col)
      {
        if (o.Owner == null)
        {
          using (new EventStateSuppressor(o))
          using (new EventStateSuppressor(this))
          {
            o.Owner = this;
          }
        }
      }
      return col;
    }

    public BasicCollection<T> GetAssociativeObjects<T>()
    {
      return new BasicCollection<T>(GetAssociativeObjects(typeof(T)).Cast<T>().ToList());
    }

    BusinessObject IInternalBusinessObject.SetAssociativeObject<T>(T obj)
    {
      return SetAssociativeObject<T>(obj);
    }

    internal BusinessObject SetAssociativeObject<T>(T obj)
      where T : IAssociativeObject
    {
      T ass = GetAssociativeObject<T>(obj.ReferenceId);
      BusinessObject bo = obj as BusinessObject;
      if (ass != null)
      {
        bo.Owner = this;
        if (ass.Reference == null)
        {
          using (new EventStateSuppressor(bo))
          {
            ass.Reference = obj.Reference;
          }
        }
        return ass as BusinessObject;
      }
      AssociativeObjectCollection.Add(obj);
      if (bo.Owner == null) bo.Owner = this;
      bo.SuppressEventState = SuppressEventState;
      OnCompositionChanged(CompositionChangedType.ChildAdded, null, this, bo);
      return obj as BusinessObject;
    }

    internal BusinessObject SetAssociativeObject(Type type, IAssociativeObject obj)
    {
      IAssociativeObject ass = GetAssociativeObject(obj);
      BusinessObject bo = obj as BusinessObject;
      if (ass != null)
      {
        if (ass.Reference == null)
          ass.Reference = obj.Reference;
        return ass as BusinessObject;
      }
      AssociativeObjectCollection.Add(obj);
      bo.SuppressEventState = SuppressEventState;
      bo.Owner = this;
      OnCompositionChanged(CompositionChangedType.ChildAdded, null, this, bo);
      return obj as BusinessObject;
    }

    #endregion

    #region Extension Objects

    BasicCollection<IExtensionObject> mExtensionObjectCollection;
    [DataMember(Name = "ExtensionObjects", EmitDefaultValue = false)]
    BasicCollection<IExtensionObject> ExtensionObjectCollection
    {
      get
      {
        if (mExtensionObjectCollection == null && !IsSerializing) mExtensionObjectCollection = new BasicCollection<IExtensionObject>();
        if (mExtensionObjectCollection != null && IsSerializing && mExtensionObjectCollection.Count == 0) return null;
        return mExtensionObjectCollection;
      }
      set { mExtensionObjectCollection = value; }
    }

    Dictionary<Type, IExtensionObject> mExtensionObjectDictionary;
    Dictionary<Type, IExtensionObject> ExtensionObjectDictionary
    {
      get
      {
        if (mExtensionObjectDictionary == null)
        {
          mExtensionObjectDictionary = new Dictionary<Type, IExtensionObject>();
          foreach (var eo in ExtensionObjectCollection)
          {
            mExtensionObjectDictionary.Add(eo.GetType(), eo);
          }
        }
        return mExtensionObjectDictionary;
      }
    }

    //public void AddExtensionObject<T>(T obj)
    //  where T : class, IExtensionObject
    //{
    //  if (obj == null) throw new ArgumentNullException("obj");
    //  if (ExtensionObjectCollection.FirstOrDefault(o => o.GetType() == typeof(T) && o.Id == obj.Id) != null) throw new Exception("Extension object already added.");
    //  obj.SuppressEventState = SuppressEventState;
    //  ExtensionObjectCollection.Add(obj);
    //}

    //public Collection<T> GetExtensionObjects<T>()
    //  where T : class, IExtensionObject
    //{
    //  Collection<T> col = new Collection<T>();
    //  foreach (T obj in ExtensionObjectCollection.Where(o => o.GetType() == typeof(T)))
    //  {
    //    col.Add(obj);
    //  }
    //  return col;
    //}

    IEnumerable<IExtensionObject> IBusinessObject.ExtensionObjects
    {
      get { return ExtensionObjectCollection; }
    }

    void IBusinessObject.AddExtensionObject(IExtensionObject obj)
    {
      AddExtensionObject(obj);
    }

    void AddExtensionObject(IExtensionObject obj)
    {
      if (ExtensionObjectDictionary.ContainsKey(obj.GetType())) 
        throw new InvalidOperationException("Extension Object Type already added.");
      ExtensionObjectCollection.Add(obj);
      ExtensionObjectDictionary.Add(obj.GetType(), obj);
      ((BusinessObject)obj).Owner = this;
      OnCompositionChanged(CompositionChangedType.ChildAdded, null, null, (BusinessObject)obj);
      //Revalidate = true;
      //Validate();
    }

    //internal Collection<ExtensionObject> GetExtensionObjects()
    //{
    //  Collection<ExtensionObject> col = new Collection<ExtensionObject>();
    //  foreach (ExtensionObject obj in ExtensionObjectCollection)
    //  {
    //    col.Add(obj);
    //  }
    //  return col;
    //}

    //public T GetExtensionObject<T>(int id)
    //  where T : ExtensionObject
    //{
    //  return (T)ExtensionObjectCollection.FirstOrDefault(o => o.GetType() == typeof(T) && o.Id == id);
    //}

    public T GetExtensionObject<T>()
      where T : class, IExtensionObject, new()
    {
      if (ExtensionObjectDictionary.ContainsKey(typeof(T)))
      {
        return (T) ExtensionObjectDictionary[typeof(T)];
      }
      //T obj = (T)ExtensionObjectCollection.FirstOrDefault(o => o.GetType() == typeof(T));
      if (ObjectAttributeHelper.ExtensionObjects.Contains(typeof(T))) //obj == null && 
      {
        using (new EventStateSuppressor(this))
        {
          T obj = new T();
          AddExtensionObject(obj);
          return obj;
        }
      }
      return null; //obj;
    }

    #endregion

    #region Utilities

    #region Guid GenerateGuid(...)

    [DllImport("rpcrt4.dll", SetLastError = true)]
    static extern int UuidCreateSequential(out Guid guid);

    public static Guid GenerateGuid()
    {
      Guid guid = Guid.Empty;
      int i = UuidCreateSequential(out guid);
      if (i != 0) throw new InvalidOperationException("Guid is not Globally Unique.");

      var s = guid.ToByteArray();
      var t = new byte[16];
      t[3] = s[10];
      t[2] = s[11];
      t[1] = s[12];
      t[0] = s[13];
      t[5] = s[14];
      t[4] = s[15];
      t[7] = s[8];
      t[6] = s[9];
      t[8] = s[1];
      t[9] = s[0];
      t[10] = s[7];
      t[11] = s[6];
      t[12] = s[5];
      t[13] = s[4];
      t[14] = s[3];
      t[15] = s[2];

      return new Guid(t);
    }

    #endregion

    #region bool IsNull(...)

    public static bool IsNull(BusinessObject obj)
    {
      if (obj == null) return true;
      if (obj.Id == 0 && obj.GlobalIdentifier == Guid.Empty) return true;
      return false;
    }

    #endregion

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1045:DoNotPassTypesByReference", MessageId = "0#")]
    protected bool SetProperty<T>(ref T field, T value, [CallerMemberName] string propertyName = null)
    {
      if (WriteSuppressor.IsSuppressed) return false;

      if (string.IsNullOrWhiteSpace(propertyName)) throw new ArgumentException(Afx.Business.Properties.Resources.InvalidParameter, "propertyName");

      if (typeof(T).IsValueType && Object.Equals(field, value)) return false;
      else if ((object)field == (object)value) return false;

      //if ((field == null && value != null) || (field != null && !field.Equals(value)))
      //{
        if (!SuppressEventState && !IsDeserializing) SetOriginalValue(propertyName, field);
        field = value;
        BusinessObject bo = value as BusinessObject;
        if (bo != null && bo.IsOwnedBy(this.GetType())) bo.Owner = this;
        Revalidate = true;
        if (!SuppressEventState)
        {
          if (!IsDeserializing) IsDirty = true;
          OnPropertyChanged(propertyName);
          OnCompositionChanged(CompositionChangedType.PropertyChanged, propertyName, this, this);
        }
        return true;
      //}

      //return false;
    }

    #endregion

    #region Owner

    public const string OwnerProperty = "Owner";
    internal BusinessObject mOwner;
    BusinessObject Owner
    {
      get
      {
        if (IsDeserializing) return null;
        return mOwner;
      }
      set
      {
        if (!IsOwned) return;
        if (mOwner == value) return;

        if (mOwner == null && value != null)
        {
          mOwner = value;
          SuppressEventState = value.SuppressEventState;
          SetReferenceValue(value.GlobalIdentifier, OwnerProperty);
          OnOwnerChanged();
          return;
        }
        if (mOwner != null && value == null)
        {
          mOwner = null;
          SetReferenceValue(Guid.Empty, OwnerProperty);
          OnOwnerChanged();
          return;
        }
        if (mOwner != null && value != null)
        {
          throw new InvalidOperationException("This object is already ownened by another object.  Please remove it from it's current owner first.");
        }
      }
    }

    protected virtual void OnOwnerChanged()
    {
    }

    IBusinessObject IBusinessObject.Owner
    {
      get { return Owner; }
    }

    #endregion

    #region Children

    List<BusinessObject> mChildren;
    List<BusinessObject> Children
    {
      get
      {
        if (mChildren == null) mChildren = new List<BusinessObject>();
        return mChildren;
      }
    }


    Collection<BusinessObject> mDeletedChildren;
    internal Collection<BusinessObject> DeletedChildren
    {
      get { return mDeletedChildren ?? (mDeletedChildren = new Collection<BusinessObject>()); }
    }

    IEnumerable<IBusinessObject> IBusinessObject.Children
    {
      get { return Children; }
    }

    void IInternalBusinessObject.AddChild(BusinessObject child)
    {
      AddChild(child);
    }

    protected internal void AddChild(BusinessObject child)
    {
      if (child == null) throw new ArgumentNullException("child");
      if (!child.IsOwnedBy(this.GetType())) return;
        //throw new InvalidOperationException("Child is not owned by this object");

      child.SuppressEventState = SuppressEventState;
      if (child.Owner == null || !child.Owner.Equals(this))
      {
        child.Owner = this;
        Children.Add(child);
        if (DeletedChildren.Contains(child)) 
          DeletedChildren.Remove(child);
        //OnCompositionChanged(CompositionChangedType.ChildAdded, null, this, child);
      }
    }

    void IInternalBusinessObject.RemoveChild(BusinessObject child)
    {
      RemoveChild(child);
    }

    protected internal void RemoveChild(BusinessObject child)
    {
      if (child == null) throw new ArgumentNullException("child");

      if ((child.Owner != null) && child.Owner.Equals(this))
      {
        child.Owner = null;
        Children.Remove(child);
        DeletedChildren.Add(child);
        //OnCompositionChanged(CompositionChangedType.ChildRemoved, null, this, child);
      }
    }

    #endregion

    #region CompositionChanged

    public event EventHandler<CompositionChangedEventArgs> CompositionChanged;

    void IInternalBusinessObject.OnCompositionChanged(CompositionChangedType compositionChangedType, string propertyName, object originalSource, BusinessObject item)
    {
      OnCompositionChanged(compositionChangedType, propertyName, originalSource, item);
    }

    bool mIsRevised;
    protected virtual void OnCompositionChanged(CompositionChangedType compositionChangedType, string propertyName, object originalSource, BusinessObject item)
    {
      Revalidate = true;
      if (SuppressEventState) return;

      if (typeof(IRevisable).IsAssignableFrom(this.GetType()) && propertyName != "Revision" && !IsDeserializing && !mIsRevised && !IsNew)
      {
        IRevisable r = this as IRevisable;
        r.Revision++;
        mIsRevised = true;
      }

      if (Owner != null) Owner.OnCompositionChanged(compositionChangedType, propertyName, originalSource, item);
      if (CompositionChanged != null)
      {
        CompositionChanged(this, new CompositionChangedEventArgs(compositionChangedType, propertyName, originalSource, item));
      }
    }

    #endregion

    #region Optimistic Concurrency

    [DataMember(Name = "OriginalValues", EmitDefaultValue = false)]
    BasicCollection<PropertyReference> mOriginalValues;
    internal BasicCollection<PropertyReference> OriginalValues
    {
      get { return mOriginalValues ?? (mOriginalValues = new BasicCollection<PropertyReference>()); }
    }

    void SetOriginalValue(string propertyName, object value)
    {
      try
      {
        if (IsNew || SuppressEventState || IsSerializing || propertyName == IsDirtyProperty || propertyName == IsDeletedProperty) return;
        if (OriginalValues.FirstOrDefault(o => o.PropertyName == propertyName) != null) return;
        BusinessObject bo = value as BusinessObject;
        if (bo != null) OriginalValues.Add(new PropertyReference(propertyName, bo.GlobalIdentifier));
        else OriginalValues.Add(new PropertyReference(propertyName, value));
      }
      catch
      {
        throw;
      }
    }

    internal void OverwriteOriginalValue(string propertyName, object value)
    {
      try
      {
        if (IsNew || SuppressEventState || IsSerializing || propertyName == IsDirtyProperty || propertyName == IsDeletedProperty) return;
        PropertyReference pr = OriginalValues.FirstOrDefault(pr1 => pr1.PropertyName == propertyName);
        if (pr != null) OriginalValues.Remove(pr);
        BusinessObject bo = value as BusinessObject;
        if (bo != null) OriginalValues.Add(new PropertyReference(propertyName, bo.GlobalIdentifier));
        else OriginalValues.Add(new PropertyReference(propertyName, value));
      }
      catch
      {
        throw;
      }
    }

    public bool GetOriginalValue(string propertyName, ref object value)
    {
      PropertyReference pv = OriginalValues.FirstOrDefault(p => p.PropertyName == propertyName);
      if (pv != null)
      {
        value = pv.Reference;
        return true;
      }
      return false;
    }

    #endregion

    #region PropertyChanged

    public event PropertyChangedEventHandler PropertyChanged;


    bool IInternalBusinessObject.SuppressEventState
    {
      get { return SuppressEventState; }
      set { SuppressEventState = value; }
    }

    bool mSuppressEventState;
    protected internal virtual bool SuppressEventState
    {
      get { return mSuppressEventState; }
      set
      {
        try
        {
          mSuppressEventState = value;
          foreach (var child in Children)
          {
            child.SuppressEventState = value;
          }
          if (ExtensionObjectCollection != null)
          {
            foreach (BusinessObject child in ExtensionObjectCollection)
            {
              child.SuppressEventState = value;
            }
          }
          if (AssociativeObjectCollection != null)
          {
            foreach (var child in AssociativeObjectCollection)
            {
              BusinessObject abo = child as BusinessObject;
              abo.SuppressEventState = value;
              if (child.Reference != null && (child.ReferenceIsOwned || !ObjectAttributeHelper.IsCached(child.Reference.GetType())))
              {
                child.Reference.SuppressEventState = value;
              }
            }
          }
        }
        catch
        {
          throw;
        }
      }
    }    

    protected virtual void OnPropertyChanged(string propertyName)
    {
      if (!SuppressEventState && !IsSerializing)
      {
        if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
      }
    }

    #endregion

    #region Error Checking

    //#region INotifyDataErrorInfo

    //internal bool mValidated = false;
    //Collection<PropertyError> mErrors = new Collection<PropertyError>();

    //bool INotifyDataErrorInfo.HasErrors
    //{
    //  get
    //  {
    //    if (!mValidated)
    //    {
    //      ObjectValidationHelper.Validate(this, null, mErrors);
    //      mValidated = true;
    //    }
    //    return mErrors.Count > 0;
    //  }
    //}

    //IEnumerable INotifyDataErrorInfo.GetErrors(string propertyName)
    //{
    //  if (!mValidated)
    //  {
    //    ObjectValidationHelper.Validate(this, null, mErrors);
    //    mValidated = true;
    //  }
    //  if (propertyName == null) return mErrors.Select(pe => pe.Error);
    //  return mErrors.Where(pe => pe.PropertyName == propertyName).Select(pe => pe.Error);
    //}

    //event EventHandler<DataErrorsChangedEventArgs> mErrorsChanged;
    //event EventHandler<DataErrorsChangedEventArgs> INotifyDataErrorInfo.ErrorsChanged
    //{
    //  add { mErrorsChanged += value; }
    //  remove { mErrorsChanged -= value; }
    //}

    //protected void RaiseErrorsChanged(string propertyName)
    //{
    //  mErrorsChanged.SafeInvoke(this, new DataErrorsChangedEventArgs(propertyName));
    //}

    //#endregion


    #region IDataErrorInfo

    bool mRevalidate = true;
    protected internal virtual bool Revalidate
    {
      get { return mRevalidate; }
      set
      {
        mRevalidate = value;
        if (Owner != null && value) Owner.Revalidate = true;
      }
    }

    public const string IsValidProperty = "IsValid";
    public virtual bool IsValid
    {
      get { return IsDeleted || string.IsNullOrWhiteSpace(Error); }
    }

    public const string HasErrorProperty = "HasError";
    public virtual bool HasError
    {
      get { return !IsValid; }
    }

    public const string ErrorProperty = "Error";
    string mError;
    public string Error
    {
      get
      {
        if (IsDeleted) return string.Empty;
        return mError;
      }
      protected set
      {
        if (mError != value)
        {
          mError = value;
          OnPropertyChanged(ErrorProperty);
          OnPropertyChanged(IsValidProperty);
          OnPropertyChanged(HasErrorProperty);
        }
      }
    }

    public virtual bool Validate()
    {
      if (Owner != null) Owner.Validate();
      if (!Revalidate) return IsValid;
      Revalidate = false;

      Collection<string> errors = new Collection<string>();
      GetErrors(errors, null);

      foreach (var pi in this.GetType().GetProperties(BindingFlags.Instance | BindingFlags.Public)
        .Where(p => p.IsDefined(typeof(ValidationAttribute))))
      {
        foreach (var vi in pi.GetCustomAttributes<ValidationAttribute>())
        {
          if (!vi.Validate(pi.GetValue(this))) errors.Add(vi.Message);
        }
      }

      foreach (var bo in Children)
      {
        bo.Validate();
        if (!string.IsNullOrWhiteSpace(bo.Error))
        {
          errors.Add(Afx.Business.Properties.Resources.BusinessObjectChildErrors);
          break;
        }
      }

      foreach (var bo in ExtensionObjectCollection)
      {
        bo.Validate();
        if (!string.IsNullOrWhiteSpace(bo.Error))
        {
          errors.Add(bo.Error);
          break;
        }
      }

      foreach (var bo in AssociativeObjectCollection)
      {
        bo.Validate();
        if (!string.IsNullOrWhiteSpace(bo.Error))
        {
          errors.Add(Afx.Business.Properties.Resources.BusinessObjectChildErrors);
          break;
        }
      }

      string error = string.Join("; ", errors);
      if (string.IsNullOrWhiteSpace(error)) error = null;

      Error = error;
      return (error == null);
    }

    string IDataErrorInfo.this[string columnName]
    {
      get
      {
        if (IsDeleted) return null;
        if (columnName  != null && columnName.Contains('.'))
        {
          int index = columnName.IndexOf('.');
          string propertyName = columnName.Substring(0, index);
          string remainder = columnName.Replace(string.Format("{0}.", propertyName), string.Empty);
          var pi = this.GetType().GetProperty(propertyName, BindingFlags.Instance | BindingFlags.Public);
          if (pi != null)
          {
            IDataErrorInfo ei = pi.GetValue(this) as IDataErrorInfo;
            if (ei != null)
            {
              return ei[remainder];
            }
          }
          return null;
        }
        else
        {
          Collection<string> errors = new Collection<string>();
          if (columnName != null)
          {
            var pi = this.GetType().GetProperty(columnName, BindingFlags.Instance | BindingFlags.Public);
            if (pi != null)
            {
              foreach (var vi in pi.GetCustomAttributes<ValidationAttribute>())
              {
                if (!vi.Validate(pi.GetValue(this))) errors.Add(vi.Message);
              }
            }
          }
          GetErrors(errors, columnName);
          return string.Join("\n", errors);
        }
      }
    }

    protected bool IsPropertyApplicable(string targetPropertyName, string propertyName)
    {
      if (string.IsNullOrWhiteSpace(targetPropertyName) || targetPropertyName == propertyName) return true;
      return false;
    }

    protected virtual void GetErrors(Collection<string> errors, string propertyName)
    {
    }

    #endregion

    #endregion

    #region Serialization

    bool mIsSerializing;
    protected bool IsSerializing
    {
      get { return mIsSerializing; }
      private set { mIsSerializing = value; }
    }

    bool mIsDeserializing;
    protected bool IsDeserializing
    {
      get { return mIsDeserializing; }
      set { mIsDeserializing = value; }
    }

    [OnSerializing]
    void Serializing(StreamingContext context)
    {
      IsSerializing = true;
    }

    [OnSerialized]
    void Serialized(StreamingContext context)
    {
      IsSerializing = false;
    }

    [OnDeserializing]
    void OnDeserializing(StreamingContext context)
    {
      IsDeserializing = true;
    }

    [OnDeserialized]
    void OnDeserialized(StreamingContext ctx)
    {
      try
      {
        ObjectCatalog.RegisterObject(this);

        using (new EventStateSuppressor(this))
        {
          mRevalidate = true;

          Tabular.ITabularBusinessObject table = this as Tabular.ITabularBusinessObject;
          if (table != null)
          {
            foreach (BusinessObject bo in table.Rows)
            {
              bo.Owner = this;
            }

            foreach (BusinessObject bo in table.Columns)
            {
              bo.Owner = this;
            }

            foreach (BusinessObject bo in table.Cells)
            {
              bo.Owner = this;
            }
          }

          foreach (PropertyInfo pi in GetPersistantCollections(this.GetType()))
          {
            try
            {
              IInternalOwnedCollection o = (IInternalOwnedCollection)pi.GetValue(this);
              o.PropertyName = pi.Name;
              o.SetOwner(this);
            }
            catch
            {
              throw;
            }
          }

          foreach (PropertyInfo pi in GetOwnedProperties(this.GetType()))
          {
            try
            {
              if (!(pi.Name == "Item" && table != null))
              {
                BusinessObject o = (BusinessObject)pi.GetValue(this);
                if (!IsNull(o))
                {
                  if (o.GetReferenceValue(OwnerProperty) == this.GlobalIdentifier) o.Owner = this;
                }
              }
            }
            catch
            {
              throw;
            }
          }

          foreach (IExtensionObject obj in ExtensionObjectCollection)
          {
            try
            {
              ((BusinessObject)obj).Owner = this;
            }
            catch
            {
              throw;
            }
          }
        }

        IsDeserializing = false;
      }
      catch
      {
        throw;
      }

      OnDeserialized();
    }

    protected virtual void OnDeserialized()
    {
    }

    static object mPCLock = new object();
    static Dictionary<Type, Collection<PropertyInfo>> mPersistantCollectionDictionary = new Dictionary<Type, Collection<PropertyInfo>>();
    static Collection<PropertyInfo> GetPersistantCollections(Type objectType)
    {
      lock (mPCLock)
      {
        if (!mPersistantCollectionDictionary.ContainsKey(objectType))
        {
          Collection<PropertyInfo> properties = new Collection<PropertyInfo>();
          foreach (PropertyInfo pi in objectType.GetProperties(BindingFlags.Public | BindingFlags.Instance)
            .Where(pi => pi.PropertyType.GetInterfaces().Any(x => x == typeof(IInternalOwnedCollection))))
          {
            PersistantCollectionAttribute pca = pi.GetCustomAttribute<PersistantCollectionAttribute>();
            if (pca != null && pca.AssociativeType != null) continue;
            properties.Add(pi);
          }
          mPersistantCollectionDictionary.Add(objectType, properties);
          return properties;
        }
        return mPersistantCollectionDictionary[objectType];
      }
    }

    static object mOPLock = new object();
    static Dictionary<Type, Collection<PropertyInfo>> mOwnedPropertyCollectionDictionary = new Dictionary<Type, Collection<PropertyInfo>>();
    static Collection<PropertyInfo> GetOwnedProperties(Type objectType)
    {
      try
      {
        lock (mOPLock)
        {
          if (!mOwnedPropertyCollectionDictionary.ContainsKey(objectType))
          {
            Collection<PropertyInfo> properties = new Collection<PropertyInfo>();
            foreach (PropertyInfo pi in objectType.GetProperties(BindingFlags.Public | BindingFlags.Instance)
              .Where(pi => typeof(BusinessObject).IsAssignableFrom(pi.PropertyType) && pi.Name != BusinessObject.OwnerProperty))
            {
              PropertyInfo pi1 = pi.PropertyType.GetProperty(BusinessObject.OwnerProperty, BindingFlags.Public | BindingFlags.Instance);
              if (pi1 != null && pi1.PropertyType.IsAssignableFrom(objectType))
              {
                properties.Add(pi);
              }
            }
            mOwnedPropertyCollectionDictionary.Add(objectType, properties);
            return properties;
          }
          return mOwnedPropertyCollectionDictionary[objectType];
        }
      }
      catch
      {
#if DEBUG
        if (Debugger.IsAttached) Debugger.Break();
#endif
        throw;
      }
    }

    #endregion

    #region Equality

    public override bool Equals(object obj)
    {
      if (obj != null && obj.GetType() == this.GetType())
      {
        BusinessObject bo = obj as BusinessObject;
        return bo.GlobalIdentifier.Equals(this.GlobalIdentifier);
      }
      return base.Equals(obj);
    }

    public override int GetHashCode()
    {
      return GlobalIdentifier.GetHashCode();
    }

    #endregion

    #region ICloneable

    public object Clone()
    {
      using (MemoryStream stream = new MemoryStream())
      {
        NetDataContractSerializer dcs = new NetDataContractSerializer();
        dcs.WriteObject(stream, this);
        stream.Position = 0;
        return dcs.ReadObject(stream);
      }
    }

    #endregion
  }
}

﻿using Afx.Business.Collections;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business
{
  [DataContract(IsReference = true, Namespace = Namespace.Afx)]
  public class SearchResult
  {
    internal SearchResult(DataRow dr)
    {
      mValues = new BasicCollection<object>();
      for(int i=1; i<dr.Table.Columns.Count; i++)
      {
        mValues.Add(dr[i]);
      }
      mGlobalIdentifier = (Guid)dr[0];
    }

    #region SearchResults SearchResultOwner

    [DataMember(Name = "SearchResultOwner")]
    SearchResults mSearchResultOwner;
    protected internal SearchResults SearchResultOwner
    {
      get { return mSearchResultOwner; }
      internal set { mSearchResultOwner = value; }
    }

    #endregion

    [DataMember(Name = "GlobalIdentifier")]
    Guid mGlobalIdentifier;
    public Guid GlobalIdentifier
    {
      get { return mGlobalIdentifier; }
    }

    [DataMember(Name = "Values")]
    BasicCollection<object> mValues;
    public object[] Values
    {
      get { return mValues.ToArray(); }
    }

    public object this[int i]
    {
      get { return mValues[i]; }
    }

    public object this[string columnName]
    {
      get { return mValues[SearchResultOwner.mHeaders.IndexOf(columnName)]; }
    }

    public override string ToString()
    {
      string s = string.Format("[{0}] ", GlobalIdentifier);
      foreach (object v in Values)
      {
        s += string.Format(", \"{0}\"", v);
      }
      return s;
    }
  }
}

﻿//using Afx.Business.Service;
//using System;
//using System.Collections.Generic;
//using System.Collections.ObjectModel;
//using System.Configuration;
//using System.Data;
//using System.Globalization;
//using System.IO;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;

//namespace Afx.Business
//{
//  public class LogHelper
//  {
//    static bool mEnabled = true;
//    static object mLock = new object();
//    static Queue<_LogMessage> mLogMessageQueue = new Queue<_LogMessage>();
//    static Queue<_SqlMessage> mSqlMessageQueue = new Queue<_SqlMessage>();
//    static System.Threading.Timer mTimer;

//    #region Private Classes

//    class _LogMessage 
//    {
//      public _LogMessage(string source, DateTime timestamp, string message)
//      {
//        Source = source;
//        Timestamp = timestamp;
//        Message = message;
//      }

//      #region string Source

//      string mSource;

//      public string Source
//      {
//        get { return mSource; }
//        set { mSource = value; }
//      }

//      #endregion

//      #region DateTime Timestamp

//      DateTime mTimestamp;

//      public DateTime Timestamp
//      {
//        get { return mTimestamp; }
//        set { mTimestamp = value; }
//      }

//      #endregion

//      #region string Message

//      string mMessage;

//      public string Message
//      {
//        get { return mMessage; }
//        set { mMessage = value; }
//      }

//      #endregion
//    }

//    class _SqlMessage
//    {
//      public _SqlMessage(string source, string scope, DateTime timestamp, string message, SqlLogParameter[] parameters)
//      {
//        Source = source;
//        Scope = scope;
//        Timestamp = timestamp;
//        Message = message;
//        mParameters = parameters;
//      }

//      #region string Source

//      string mSource;

//      public string Source
//      {
//        get { return mSource; }
//        set { mSource = value; }
//      }

//      #endregion

//      #region string Scope

//      string mScope;
//      public string Scope
//      {
//        get { return mScope; }
//        set { mScope = value; }
//      }

//      #endregion
      
//      #region DateTime Timestamp

//      DateTime mTimestamp;

//      public DateTime Timestamp
//      {
//        get { return mTimestamp; }
//        set { mTimestamp = value; }
//      }

//      #endregion

//      #region string Message

//      string mMessage;

//      public string Message
//      {
//        get { return mMessage; }
//        set { mMessage = value; }
//      }

//      #endregion

//      #region SqlLogParameter[] Parameters

//      SqlLogParameter[] mParameters;
//      public IEnumerable<SqlLogParameter> Parameters
//      {
//        get { return mParameters; }
//      }

//      #endregion
//    }
//    #endregion

//    static LogHelper()
//    {
//      mTimer = new System.Threading.Timer(ProcessQueue, null, 200, 200);
//    }

//    private static void ProcessQueue(object state)
//    {
//      if (!mEnabled) return;

//      lock (mLock)
//      {
//        try
//        {
//          if (mLogMessageQueue.Count == 0 && mSqlMessageQueue.Count == 0) return;

//          using (var svc = ServiceFactory.GetService<IAfxLogService>())
//          {
//            while (mLogMessageQueue.Count > 0)
//            {
//              _LogMessage m = mLogMessageQueue.Dequeue();
//              svc.Instance.LogMessage(m.Source, m.Timestamp, m.Message);
//            }

//            while (mSqlMessageQueue.Count > 0)
//            {
//              _SqlMessage m = mSqlMessageQueue.Dequeue();
//              svc.Instance.LogSql(m.Source, m.Scope, m.Timestamp, m.Message, m.Parameters.ToArray());
//            }
//          }
//        }
//        catch
//        {
//          mEnabled = false;
//        }
//      }
//    }

//    [ThreadStatic]
//    static Stack<string> mScope;
//    public static Stack<string> Scope
//    {
//      get { return LogHelper.mScope ?? (LogHelper.mScope=new Stack<string>()); }
//    }

//    public static void PushScope(string scope)
//    {
//      Scope.Push(scope);
//    }

//    public static void PopScope()
//    {
//      Scope.Pop();
//    }

//    public static string CurrentScope
//    {
//      get
//      {
//        if (Scope.Count == 0) return null;
//        Collection<string> ss = new Collection<string>();
//        foreach (string s in Scope.Reverse())
//        {
//          ss.Add(s);
//        }
//        return string.Join(" -> ", ss);
//      }
//    }

//    public static void LogMessage(string message, params object[] args)
//    {
//      if (!mEnabled) return;

//      lock (mLock)
//      {
//        if (args == null)
//        {
//          mLogMessageQueue.Enqueue(new _LogMessage(ConfigurationManager.AppSettings["LogSource"], DateTime.Now, message));
//        }
//        else
//        {
//          try
//          {
          
//            //TODO:  Check for {Some Text} which is causing string format exception. Jaco's PC is a good example of this happening because of his directory naming.
//            //mLogMessageQueue.Enqueue(new _LogMessage(ConfigurationManager.AppSettings["LogSource"], DateTime.Now, string.Format(CultureInfo.InvariantCulture, message, args)));
//          } catch (Exception ex)
//          {
//            throw (ex);
//          }
//        }
//      }
//    }

//    public static void LogSql(IDbCommand cmd)
//    {
//      if (!mEnabled) return;

//      Collection<SqlLogParameter> parameters = new Collection<SqlLogParameter>();
//      foreach (IDataParameter p in cmd.Parameters)
//      {
//        parameters.Add(new SqlLogParameter(p.ParameterName, p.Value));
//      }

//      lock (mLock)
//      {
//        mSqlMessageQueue.Enqueue(new _SqlMessage(ConfigurationManager.AppSettings["LogSource"], CurrentScope, DateTime.Now, cmd.CommandText, parameters.ToArray()));
//      }
//    }
//  }
//}

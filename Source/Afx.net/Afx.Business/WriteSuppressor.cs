﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business
{
  public class WriteSuppressor : IDisposable
  {
    [ThreadStatic]
    static WriteSuppressor mWriteSuppressor = null;

    public WriteSuppressor()
    {
      if (mWriteSuppressor == null) mWriteSuppressor = this;
    }

    public static bool IsSuppressed { get { return mWriteSuppressor != null; } }

    public void Dispose()
    {
      if (mWriteSuppressor == this) mWriteSuppressor = null;
    }
  }
}

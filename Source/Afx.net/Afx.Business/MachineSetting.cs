﻿using Afx.Business;
using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace Afx.Business
{
  [PersistantObject(Schema = "Afx")]
  public abstract partial class MachineSetting : Setting
  {
    #region Constructors

    public MachineSetting()
    {
    }

    #endregion

    #region string MachineName

    public const string MachineNameProperty = "MachineName";
    [DataMember(Name = MachineNameProperty, EmitDefaultValue = false)]
    string mMachineName;
    [PersistantProperty]
    public string MachineName
    {
      get { return mMachineName; }
      set { SetProperty<string>(ref mMachineName, value); }
    }

    #endregion
  }
}

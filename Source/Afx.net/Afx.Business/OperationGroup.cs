﻿using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business
{
  [DataContract(IsReference = true, Namespace = Afx.Business.Namespace.Afx)]
  [Cache(Priority = 1)]
  [PersistantObject(Schema = "Afx")]
  public class OperationGroup : BusinessObject, INamespaceSensitive, IRevisable
  {
    #region int Revision

    public const string RevisionProperty = "Revision";
    [DataMember(Name = RevisionProperty, EmitDefaultValue = false)]
    int mRevision = 1;
    [PersistantProperty]
    public int Revision
    {
      get { return mRevision; }
      set { SetProperty<int>(ref mRevision, value); }
    }

    #endregion

    #region DataMember string GroupName

    public const string GroupNameProperty = "GroupName";
    [DataMember(Name = GroupNameProperty, EmitDefaultValue = false)]
    string mGroupName;
    [PersistantProperty(SortOrder = 0)]
    public string GroupName
    {
      get { return mGroupName; }
      set { SetProperty<string>(ref mGroupName, value); }
    }

    #endregion

    #region string Namespace

    public const string NamespaceProperty = "Namespace";
    [DataMember(Name = NamespaceProperty, EmitDefaultValue = false)]
    string mNamespace;
    [PersistantProperty]
    public string Namespace
    {
      get { return mNamespace; }
      set { SetProperty<string>(ref mNamespace, value); }
    }

    #endregion

    #region DataMember BusinessObjectCollection<Operation> Operations

    [DataMember(Name = "Operations")]
    BusinessObjectCollection<Operation> mOperations;
    [PersistantCollection]
    public BusinessObjectCollection<Operation> Operations
    {
      get
      {
        if (mOperations == null) mOperations = new BusinessObjectCollection<Operation>(this);
        return mOperations;
      }
    }

    #endregion

    #region Validation

    protected override void GetErrors(System.Collections.ObjectModel.Collection<string> errors, string propertyName)
    {
      if (string.IsNullOrWhiteSpace(propertyName) || propertyName == "GroupName")
      {
        if (string.IsNullOrWhiteSpace(GroupName)) errors.Add("Group Name is a mandatory field");
      }

      base.GetErrors(errors, propertyName);
    }

    #endregion
  }
}

﻿using Afx.Business.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.Security
{
  [PersistantObject(Schema = "Afx")]
  [DataContract(IsReference = true)]
  public class UserSession : BusinessObject
  {
    public UserSession()
    {
    }

    internal UserSession(Guid sessionToken, User user, string server)
    {
      GlobalIdentifier = sessionToken;
      User = user;
      Server = server;
    }

    internal UserSession(Guid sessionToken, User user, string server, DateTime timestamp)
    {
      GlobalIdentifier = sessionToken;
      User = user;
      Server = server;
      Timestamp = timestamp;
    }

    public Guid SessionToken
    {
      get { return GlobalIdentifier; }
    }

    #region User User

    public const string UserProperty = "User";
    [DataMember(Name = UserProperty, EmitDefaultValue = false)]
    User mUser;
    [PersistantProperty]
    public User User
    {
      get { return mUser; }
      private set { SetProperty<User>(ref mUser, value); }
    }

    #endregion

    #region DateTime Timestamp

    public const string TimestampProperty = "Timestamp";
    [DataMember(Name = TimestampProperty, EmitDefaultValue = false)]
    DateTime mTimestamp = DateTime.Now;
    [PersistantProperty(IgnoreConcurrency = true)]
    public DateTime Timestamp
    {
      get { return mTimestamp; }
      private set { SetProperty<DateTime>(ref mTimestamp, value); }
    }

    #endregion

    #region string Server

    public const string ServerProperty = "Server";
    [DataMember(Name = ServerProperty, EmitDefaultValue = false)]
    string mServer;
    [PersistantProperty]
    public string Server
    {
      get { return mServer; }
      private set { SetProperty<string>(ref mServer, value); }
    }

    #endregion

    public void UpdateTimestamp()
    {
      Timestamp = DateTime.Now;
    }
  }
}

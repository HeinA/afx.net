﻿using Afx.Business.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.Security
{
  [DataContract(IsReference = true, Namespace = Namespace.Afx)]
  [PersistantObject(Schema = "Afx", TableName = "vLoginOrganizationalUnit", IsReadOnly = true)]
  public class LoginOrganizationalUnit : BusinessObject
  {
    #region string Name

    public const string NameProperty = "Name";
    [DataMember(Name = NameProperty, EmitDefaultValue = false)]
    string mName;
    [PersistantProperty]
    public string Name
    {
      get { return mName; }
      set { SetProperty<string>(ref mName, value); }
    }

    #endregion

    #region string Server

    public const string ServerProperty = "Server";
    [DataMember(Name = ServerProperty, EmitDefaultValue = false)]
    string mServer;
    [PersistantProperty]
    public string Server
    {
      get { return mServer; }
      set { SetProperty<string>(ref mServer, value); }
    }

    #endregion
  }
}

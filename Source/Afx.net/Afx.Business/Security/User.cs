﻿#region Usings

using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

#endregion

namespace Afx.Business.Security
{
  [DataContract(IsReference = true, Namespace = Namespace.Afx)]
  [PersistantObject(Schema = "Afx")]
  [Replicate]
  public class User : BusinessObject
  {
    #region string Username

    public const string UsernameProperty = "Username";
    [DataMember(Name = UsernameProperty, EmitDefaultValue = false)]
    string mUsername;
    [Search]
    [PersistantProperty]
    public string Username
    {
      get { return mUsername; }
      set { SetProperty<string>(ref mUsername, value); }
    }

    #endregion

    #region string PasswordHash

    public const string PasswordHashProperty = "PasswordHash";
    [DataMember(Name = PasswordHashProperty, EmitDefaultValue = false)]
    string mPasswordHash;
    [PersistantProperty]
    public string PasswordHash
    {
      get { return mPasswordHash; }
      set { SetProperty<string>(ref mPasswordHash, value, PasswordHashProperty); }
    }

    #endregion

    //#region string SaltedPasswordHash

    //public const string SaltedPasswordHashProperty = "SaltedPasswordHash";
    //string mSaltedPasswordHash;
    //[PersistantProperty(IgnoreNull = true)]
    //public string SaltedPasswordHash
    //{
    //  get { return mSaltedPasswordHash; }
    //  set { SetProperty<string>(ref mSaltedPasswordHash, value); }
    //}

    //#endregion

    #region string Firstnames

    public const string FirstnamesProperty = "Firstnames";
    [DataMember(Name = FirstnamesProperty, EmitDefaultValue = false)]
    string mFirstnames;
    [PersistantProperty]
    public string Firstnames
    {
      get { return mFirstnames; }
      set { SetProperty<string>(ref mFirstnames, value, FirstnamesProperty); }
    }

    #endregion

    #region string Lastname

    public const string LastnameProperty = "Lastname";
    [DataMember(Name = LastnameProperty, EmitDefaultValue = false)]
    string mLastname;
    [PersistantProperty]
    public string Lastname
    {
      get { return mLastname; }
      set { SetProperty<string>(ref mLastname, value, LastnameProperty); }
    }

    #endregion

    #region OrganizationalUnit DefaultOrganizationalUnit

    public const string DefaultOrganizationalUnitProperty = "DefaultOrganizationalUnit";
    [PersistantProperty]
    public OrganizationalUnit DefaultOrganizationalUnit
    {
      get
      {
        OrganizationalUnit ou = GetCachedObject<OrganizationalUnit>();
        if (IsNull(ou) && OrganizationalUnits.Count > 0)
        {
          ou = OrganizationalUnits[0];
          SetCachedObject<OrganizationalUnit>(ou);
        }
        return ou;
      }
      set { SetCachedObject<OrganizationalUnit>(value); }
    }

    #endregion

    #region Associative BusinessObjectCollection<Role> Roles

    AssociativeObjectCollection<UserRole, Role> mRoles;
    [PersistantCollection(AssociativeType=typeof(UserRole))]
    public BusinessObjectCollection<Role> Roles
    {
      get
      {
        if (mRoles == null) using (new EventStateSuppressor(this)) { mRoles = new AssociativeObjectCollection<UserRole, Role>(this); }
        return mRoles;
      }
    }

    #endregion

    #region Associative BusinessObjectCollection<OrganizationalUnit> OrganizationalUnits

    AssociativeObjectCollection<UserOrganizationalUnit, OrganizationalUnit> mOrganizationalUnits;
    [PersistantCollection(AssociativeType=typeof(UserOrganizationalUnit))]
    public BusinessObjectCollection<OrganizationalUnit> OrganizationalUnits
    {
      get 
      {
        if (mOrganizationalUnits == null) using (new EventStateSuppressor(this)) { mOrganizationalUnits = new AssociativeObjectCollection<UserOrganizationalUnit, OrganizationalUnit>(this); }
        return mOrganizationalUnits;
      }
    }

    #endregion

    public bool HasRole(params string[] identifiers)
    {
      foreach (var r in Roles)
      {
        foreach (var s in identifiers)
        {
          Guid guid = Guid.Parse(s);
          if (r.GlobalIdentifier.Equals(guid)) return true;
        }
      }
      return false;
    }


    public bool HasRole(params Role[] roles)
    {
      foreach (var r in Roles)
      {
        foreach (var r1 in roles)
        {
          if (r.GlobalIdentifier.Equals(r1.GlobalIdentifier)) return true;
        }
      }
      return false;
    }

    protected override void GetErrors(System.Collections.ObjectModel.Collection<string> errors, string propertyName)
    {
      if (string.IsNullOrWhiteSpace(propertyName) || propertyName == "Username")
      {
        if (string.IsNullOrWhiteSpace(Username)) errors.Add("User name is a mandatory field.");
      }

      if (string.IsNullOrWhiteSpace(propertyName) || propertyName == "PasswordHash")
      {
        if (PasswordHash == null && IsNew) errors.Add("Password is not set.");
      }

      base.GetErrors(errors, propertyName);
    }
  }
}

﻿using Afx.Business.Collections;
using Afx.Business.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.Security
{
  [DataContract(IsReference = true, Namespace = Namespace.Afx)]
  [PersistantObject(Schema = "Afx", OwnerColumn = "Parent")]
  [Cache(Priority = 0)]
  public class ServerInstance : BusinessObject<ServerInstance>, IRevisable
  {
    public ServerInstance()
    {
    }

    public ServerInstance(bool isNull)
      : base(isNull)
    {
    }

    #region int Revision

    public const string RevisionProperty = "Revision";
    [DataMember(Name = RevisionProperty, EmitDefaultValue = false)]
    int mRevision = 1;
    [PersistantProperty]
    public int Revision
    {
      get { return mRevision; }
      set { SetProperty<int>(ref mRevision, value); }
    }

    #endregion

    #region string Name

    public const string NameProperty = "Name";
    [DataMember(Name = NameProperty, EmitDefaultValue = false)]
    string mName;
    [PersistantProperty]
    public string Name
    {
      get { return mName; }
      set { SetProperty<string>(ref mName, value); }
    }

    #endregion

    #region bool IsEnabled

    public const string IsEnabledProperty = "IsEnabled";
    [DataMember(Name = IsEnabledProperty, EmitDefaultValue = false)]
    bool mIsEnabled;
    [PersistantProperty]
    public bool IsEnabled
    {
      get { return mIsEnabled; }
      set { SetProperty<bool>(ref mIsEnabled, value); }
    }

    #endregion

    #region BusinessObjectCollection<ServerInstance> Satellites

    public const string SatellitesProperty = "Satellites";
    BusinessObjectCollection<ServerInstance> mSatellites;
    [PersistantCollection]
    [DataMember]
    public BusinessObjectCollection<ServerInstance> Satellites
    {
      get { return mSatellites ?? (mSatellites = new BusinessObjectCollection<ServerInstance>(this)); }
    }

    #endregion

    public bool IsSatelliteOf(ServerInstance si)
    {
      //if (this.Equals(si)) return true;
      if (IsNull(Owner)) return false;
      if (Owner.Equals(si)) return true;
      return Owner.IsSatelliteOf(si);
    }

    public bool ContainsSatellite(ServerInstance si)
    {
      if (this.Equals(si)) return true;
      foreach (ServerInstance csi in Satellites)
      {
        bool b = csi.ContainsSatellite(si);
        if (b) return true;
      }
      return false;
    }

    public ServerInstance Root
    {
      get
      {
        if (IsDeserializing) return null;
        if (!IsNull(Owner)) return Owner.Root;
        return this;
      }
    }
  }
}

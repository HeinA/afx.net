﻿using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Runtime.Serialization;

namespace Afx.Business.Security
{
  [DataContract(IsReference = true, Namespace = Namespace.Afx)]
  [PersistantObject(Schema = "Afx", OwnerColumn = "Parent")]
  [Cache(Priority = 1)]
  public class OrganizationalUnit : BusinessObject<OrganizationalUnit>, IRevisable
  {
    public OrganizationalUnit()
    {
    }

    public OrganizationalUnit(bool isNull)
      : base(isNull)
    {
    }

    #region int Revision

    public const string RevisionProperty = "Revision";
    [DataMember(Name = RevisionProperty, EmitDefaultValue = false)]
    int mRevision = 1;
    [PersistantProperty]
    public int Revision
    {
      get { return mRevision; }
      set { SetProperty<int>(ref mRevision, value); }
    }

    #endregion

    #region DataMember string Name

    public const string NameProperty = "Name";
    [DataMember(Name = NameProperty, EmitDefaultValue = false)]
    string mName;
    [PersistantProperty(SortOrder=0)]
    public string Name
    {
      get { return mName; }
      set { SetProperty<string>(ref mName, value, NameProperty); }
    }

    #endregion

    #region DataMember bool UserAssignable

    public const string UserAssignableProperty = "UserAssignable";
    [DataMember(Name = UserAssignableProperty, EmitDefaultValue = false)]
    bool mUserAssignable;
    [PersistantProperty]
    public bool UserAssignable
    {
      get { return mUserAssignable; }
      set { SetProperty<bool>(ref mUserAssignable, value, UserAssignableProperty); }
    }

    #endregion

    #region OrganizationalStructureType StructureType

    public const string StructureTypeProperty = "StructureType";
    [PersistantProperty]
    public OrganizationalStructureType StructureType
    {
      get { return GetCachedObject<OrganizationalStructureType>(); }
      set { SetCachedObject<OrganizationalStructureType>(value); }
    }

    #endregion

    #region DataMember BusinessObjectCollection<OrganizationalUnit> SubUnits

    [DataMember(Name = "SubUnits")]
    BusinessObjectCollection<OrganizationalUnit> mSubUnits;
    [PersistantCollection]
    public BusinessObjectCollection<OrganizationalUnit> SubUnits
    {
      get
      {
        if (mSubUnits == null) mSubUnits = new BusinessObjectCollection<OrganizationalUnit>(this);
        return mSubUnits;
      }
    }

    #endregion

    #region string Base64Image

    #region DataMember string Base64Image

    public const string Base64ImageProperty = "Base64Image";
    [DataMember(Name = Base64ImageProperty, EmitDefaultValue = false)]
    string mBase64Image;
    [PersistantProperty(IgnoreConcurrency = true)]
    public string Base64Image
    {
      get { return mBase64Image ?? (BusinessObject.IsNull(Owner) ? null : IsSerializing || IsDeserializing ? null : Owner.Base64Image); }
      set { SetProperty<string>(ref mBase64Image, value, Base64ImageProperty); }
    }

    #endregion

    #region void SetImageFromFile(string filename)

    public void SetImageFromFile(string filename)
    {
      using (FileStream fs = new FileStream(filename, FileMode.Open, FileAccess.Read))
      {
        byte[] imgBytes = new byte[fs.Length];
        fs.Read(imgBytes, 0, Convert.ToInt32(fs.Length));
        Base64Image = Convert.ToBase64String(imgBytes, Base64FormattingOptions.InsertLineBreaks);
      }
      foreach (var subou in SubUnits)
      {
        subou.Base64Image = Base64Image;
      }
      OnPropertyChanged(Base64ImageProperty);
    }

    #endregion

    #endregion

    #region void GetErrors(Collection<string> errors, string propertyName)

    protected override void GetErrors(Collection<string> errors, string propertyName)
    {
      if (propertyName == null || propertyName == NameProperty)
      {
        if (string.IsNullOrWhiteSpace(Name)) errors.Add("Name is a mandatory field.");
      }

      if (propertyName == null || propertyName == ServerProperty)
      {
        if (UserAssignable && IsNull(Server)) errors.Add("Server is a mandatory field.");
      }

      base.GetErrors(errors, propertyName);
    }

    #endregion

    #region ServerInstance Server

    public const string ServerProperty = "Server";
    [PersistantProperty]
    public ServerInstance Server
    {
      get { return GetCachedObject<ServerInstance>(); }
      set { SetCachedObject<ServerInstance>(value); }
    }

    #endregion

    #region OrganizationalUnit Root

    public const string RootProperty = "Root";
    public OrganizationalUnit Root
    {
      get
      {
        if (IsDeserializing) return null;
        if (BusinessObject.IsNull(Owner)) return this;
        return Owner.Root;
      }
    }

    #endregion

    #region string PhysicalAddress

    public const string PhysicalAddressProperty = "PhysicalAddress";
    [DataMember(Name = PhysicalAddressProperty, EmitDefaultValue = false)]
    string mPhysicalAddress;
    [PersistantProperty]
    public string PhysicalAddress
    {
      get { return mPhysicalAddress; }
      set { SetProperty<string>(ref mPhysicalAddress, value, PhysicalAddressProperty); }
    }
    
    #endregion

    #region string PostalAddress

    public const string PostalAddressProperty = "PostalAddress";
    [DataMember(Name = PostalAddressProperty, EmitDefaultValue = false)]
    string mPostalAddress;
    [PersistantProperty]
    public string PostalAddress
    {
      get { return mPostalAddress; }
      set { SetProperty<string>(ref mPostalAddress, value, PostalAddressProperty); }
    }

    #endregion

    #region string Email

    public const string EmailProperty = "Email";
    [DataMember(Name = EmailProperty, EmitDefaultValue = false)]
    string mEmail;
    [PersistantProperty]
    public string Email
    {
      get { return mEmail; }
      set { SetProperty<string>(ref mEmail, value); }
    }

    #endregion

    #region string VATNumber

    public const string VATNumberProperty = "VATNumber";
    [DataMember(Name = VATNumberProperty, EmitDefaultValue = false)]
    string mVATNumber;
    [PersistantProperty]
    public string VATNumber
    {
      get { return mVATNumber; }
      set { SetProperty<string>(ref mVATNumber, value); }
    }

    #endregion

    #region string Telephone

    public const string TelephoneProperty = "Telephone";
    [DataMember(Name = TelephoneProperty, EmitDefaultValue = false)]
    string mTelephone;
    [PersistantProperty]
    public string Telephone
    {
      get { return mTelephone; }
      set { SetProperty<string>(ref mTelephone, value); }
    }

    #endregion

    #region string Fax

    public const string FaxProperty = "Fax";
    [DataMember(Name = FaxProperty, EmitDefaultValue = false)]
    string mFax;
    [PersistantProperty]
    public string Fax
    {
      get { return mFax; }
      set { SetProperty<string>(ref mFax, value); }
    }

    #endregion
  }
}
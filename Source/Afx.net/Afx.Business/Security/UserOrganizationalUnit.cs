﻿using Afx.Business.Attributes;
using Afx.Business.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.Security
{
  [DataContract(IsReference = true, Namespace = Namespace.Afx)]
  [PersistantObject(Schema = "Afx")] //, OwnerColumn = "User", ReferenceColumn = "OrganizationalUnit"
  public class UserOrganizationalUnit : AssociativeObject<User, OrganizationalUnit>
  {
  }
}

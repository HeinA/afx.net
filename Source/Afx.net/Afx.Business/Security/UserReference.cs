﻿using Afx.Business.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.Security
{
  [DataContract(IsReference = true, Namespace = Namespace.Afx)]
  [PersistantObject(Schema = "Afx", TableName="vUserReference", IsReadOnly = false)]
  [Cache(Replicate = false)]
  public class UserReference : BusinessObject
  {
    public UserReference()
    {
    }

    public UserReference(bool isNull)
      : base(isNull)
    {
    }

    #region string Username

    public const string UsernameProperty = "Username";
    [DataMember(Name = UsernameProperty, EmitDefaultValue = false)]
    string mUsername;
    [PersistantProperty]
    public string Username
    {
      get { return mUsername; }
      set { SetProperty<string>(ref mUsername, value); }
    }

    #endregion

    #region string Firstnames

    public const string FirstnamesProperty = "Firstnames";
    [DataMember(Name = FirstnamesProperty, EmitDefaultValue = false)]
    string mFirstnames;
    [PersistantProperty]
    public string Firstnames
    {
      get { return mFirstnames; }
      set { SetProperty<string>(ref mFirstnames, value, FirstnamesProperty); }
    }

    #endregion

    #region string Lastname

    public const string LastnameProperty = "Lastname";
    [DataMember(Name = LastnameProperty, EmitDefaultValue = false)]
    string mLastname;
    [PersistantProperty]
    public string Lastname
    {
      get { return mLastname; }
      set { SetProperty<string>(ref mLastname, value, LastnameProperty); }
    }

    #endregion

    public string FullName
    {
      get { return string.Format("{0} {1}", Firstnames, Lastname); }
    }
  }
}

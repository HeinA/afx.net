﻿using Afx.Business.Attributes;
using Afx.Business.Data;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Afx.Business.Security
{
  [DataContract(IsReference = true, Namespace = Namespace.Afx)]
  [PersistantObject(Schema = "Afx")] 
  public class UserRole : AssociativeObject<User, Role>
  {
  }
}
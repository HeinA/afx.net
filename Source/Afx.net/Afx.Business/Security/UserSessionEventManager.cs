﻿using Afx.Business.Data;
using Afx.Business.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.Security
{
  public class UserSessionEventManager : PersistanceEventManager<UserSession>
  {
    public override void AfterPersist(UserSession obj, PersistanceFlags flags)
    {
      if (!BusinessObject.IsNull(ServiceBase.ServerInstance.Owner) && (flags & PersistanceFlags.Import) == 0)
      {
        using (var svc = ServiceFactory.GetService<IAfxReplicationService>(ServiceBase.ServerInstance.Root.Name))
        {
          svc.Instance.Replicate(obj, ServiceBase.ServerInstance.GlobalIdentifier);
        }
      }

      if (ServiceBase.ServerInstance.Name != obj.Server && (flags & PersistanceFlags.Import) == 0)
      {
        using (var svc = ServiceFactory.GetService<IAfxReplicationService>(obj.Server))
        {
          svc.Instance.Replicate(obj, ServiceBase.ServerInstance.GlobalIdentifier);
        }
      }

      base.AfterPersist(obj, flags);
    }

    public override void BeforeDelete(UserSession obj, PersistanceFlags flags)
    {
      if (!BusinessObject.IsNull(ServiceBase.ServerInstance.Owner) && (flags & PersistanceFlags.Import) == 0)
      {
        using (var svc = ServiceFactory.GetService<IAfxReplicationService>(ServiceBase.ServerInstance.Root.Name))
        {
          svc.Instance.Replicate(obj, ServiceBase.ServerInstance.GlobalIdentifier);
        }
      }
      
      if (ServiceBase.ServerInstance.Name != obj.Server && (flags & PersistanceFlags.Import) == 0)
      {
        using (var svc = ServiceFactory.GetService<IAfxReplicationService>(obj.Server))
        {
          svc.Instance.Replicate(obj, ServiceBase.ServerInstance.GlobalIdentifier);
        }
      }

      base.BeforeDelete(obj, flags);
    }
  }
}

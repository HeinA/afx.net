﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.Security
{
  [AttributeUsage( AttributeTargets.Class, AllowMultiple = true)]
  public class AuthorizationAttribute : Attribute
  {
    public AuthorizationAttribute(string role)
    {
      Role = role;
    }

    public string Role { get; private set; }
  }
}

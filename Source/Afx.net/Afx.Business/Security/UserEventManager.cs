﻿using Afx.Business.Data;
using Afx.Business.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.Security
{
  class UserEventManager : PersistanceEventManager<User>
  {
    string CreateUserSalt()
    {
      byte[] saltbytes = new byte[32];
      RNGCryptoServiceProvider p = new RNGCryptoServiceProvider();
      p.GetNonZeroBytes(saltbytes);
      return ASCIIEncoding.ASCII.GetString(saltbytes);
    }

    public override void AfterPersist(User obj, PersistanceFlags flags)
    {
      ObjectRepository<UserSalt> saltStore = PersistanceManager.GetRepository<UserSalt>();
      UserSalt userSalt = saltStore.GetInstance(obj.Id);
      if (userSalt == null && obj.PasswordHash != null)
      {
        userSalt = new UserSalt();
        userSalt.Id = obj.Id;
        userSalt.Salt = CreateUserSalt();
        saltStore.Persist(userSalt);
      }

      if (userSalt != null && obj.PasswordHash != null)
      {
        HashAlgorithm algoritm = new SHA256Managed();
        ObjectRepository<User> userStore = PersistanceManager.GetRepository<User>();
        obj.SaltedPasswordHash = ASCIIEncoding.ASCII.GetString(algoritm.ComputeHash(ASCIIEncoding.ASCII.GetBytes(string.Format("{0}{1}", userSalt.Salt, obj.PasswordHash))));
        userStore.Persist(obj);
      }

      if (userSalt == null && obj.PasswordHash == null) throw new Exception("New user requires a password.");

      base.AfterPersist(obj, flags);
    }
  }
}

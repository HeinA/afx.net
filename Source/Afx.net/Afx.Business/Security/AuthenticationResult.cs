﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.Security
{
  [DataContract(Namespace = Namespace.Afx, IsReference = true)]
  public class AuthenticationResult
  {
    internal AuthenticationResult(string server, Guid organizationalUnitId, User user)
    {
      mUser = user;
      mServer = server;
      mOrganizationalUnitId = organizationalUnitId;
    }

    internal AuthenticationResult(string error)
    {
      mError = error;
    }

    public const string UserProperty = "User";
    [DataMember(Name = UserProperty, EmitDefaultValue = false)]
    User mUser;
    public User User
    {
      get { return mUser; }
    }

    public const string ErrorProperty = "Error";
    [DataMember(Name = ErrorProperty, EmitDefaultValue = false)]
    string mError;
    public string Error
    {
      get { return mError; }
    }

    #region string Server

    public const string ServerProperty = "Server";
    [DataMember(Name = ServerProperty, EmitDefaultValue = false)]
    string mServer;
    public string Server
    {
      get { return mServer; }
    }

    #endregion

    #region Guid OrganizationalUnitId

    public const string OrganizationalUnitIdProperty = "OrganizationalUnitId";
    [DataMember(Name = OrganizationalUnitIdProperty, EmitDefaultValue = false)]
    Guid mOrganizationalUnitId;
    public Guid OrganizationalUnitId
    {
      get { return mOrganizationalUnitId; }
    }

    #endregion
  }
}

﻿using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Service;
using Afx.Business.ServiceModel;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Security.Cryptography;
using System.ServiceModel;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;

namespace Afx.Business.Security
{
  public class SecurityContext
  {
    internal const string RegistryKey = @"Login";
    internal const string LastUser = @"LastUser";
    internal const string LastServer = @"LastServer";
    internal const string LastOU = @"LastOU";

    public static AuthenticationResult Authenticate(LoginOrganizationalUnit loginOU, string username, string password, bool passwordIsHashed = false)
    {
      ////DebugHelper.LogMessage("Authenticating...");
      string hash = null;
      if (!passwordIsHashed)
      {
        HashAlgorithm algoritm = new SHA256Managed();
        hash = ASCIIEncoding.ASCII.GetString(algoritm.ComputeHash(ASCIIEncoding.ASCII.GetBytes(password)));
      }
      else
      {
        hash = password;
      }

      using (var svc = ProxyFactory.GetService<IAfxService>(loginOU.Server))
      {
        AuthenticationResult ar = svc.Login(username, hash, loginOU);
        ////DebugHelper.LogMessage("Authentication done.");
        return ar;
      }
    }

    public static AuthenticationResult AuthenticateLocal(LoginOrganizationalUnit loginOU, string username, string password, bool passwordIsHashed = false)
    {
      string hash = null;
      if (!passwordIsHashed)
      {
        HashAlgorithm algoritm = new SHA256Managed();
        hash = ASCIIEncoding.ASCII.GetString(algoritm.ComputeHash(ASCIIEncoding.ASCII.GetBytes(password)));
      }
      else
      {
        hash = password;
      }

      try
      {
        Collection<IDataFilter> filters = new Collection<IDataFilter>();
        filters.Add(new DataFilter<User>(User.UsernameProperty, FilterType.Equals, username));
        filters.Add(new DataFilter<User>(User.PasswordHashProperty, FilterType.Equals, hash));
        User user = ExtensibilityManager.PersistanceManager.GetRepository<User>().GetInstance(filters);
        if (user != null)
        {
          AuthenticationResult result = new AuthenticationResult(loginOU.Server, loginOU.GlobalIdentifier, user);
          return result;
        }

        return new AuthenticationResult("Invalid username or password.");
      }
      catch
      {
        throw;
      }
    }

    public static void Login(LoginOrganizationalUnit loginOU, string username, string password)
    {
      Login(Authenticate(loginOU, username, password));
    }

    public static void Login(AuthenticationResult authentication)
    {
      if (authentication.User == null) throw new InvalidOperationException("User is not set.");

      User = authentication.User;
      ServerName = authentication.Server;
      OrganizationalUnitId = authentication.OrganizationalUnitId;

      RegistryHelper.SetRegistryValue<string>(Microsoft.Win32.RegistryHive.CurrentUser, string.Format(RegistryHelper.ApplicationKey, RegistryKey), LastUser, Microsoft.Win32.RegistryValueKind.String, User.Username);
      RegistryHelper.SetRegistryValue<string>(Microsoft.Win32.RegistryHive.CurrentUser, string.Format(RegistryHelper.ApplicationKey, RegistryKey), LastServer, Microsoft.Win32.RegistryValueKind.String, ServerName);
      RegistryHelper.SetRegistryValue<string>(Microsoft.Win32.RegistryHive.CurrentUser, string.Format(RegistryHelper.ApplicationKey, RegistryKey), LastOU, Microsoft.Win32.RegistryValueKind.String, OrganizationalUnitId.ToString());
    }

    public static void Logout()
    {
      User = null;
      ServerName = null;
    }

    static string mServerName = null;
    public static string ServerName
    {
      get
      {
        if (OperationContext.Current != null)
        {
          return ServiceBase.ServerInstance.Name;
        }
        return mServerName;
      }
      private set { mServerName = value; }
    }

    public static ServerInstance Server
    {
      get { return Cache.Instance.GetObjects<ServerInstance>(si => si.Name == ServerName).FirstOrDefault(); }
    }

    #region Guid OrganizationalUnitId

    static Guid mOrganizationalUnitId = Guid.Empty;
    static Guid OrganizationalUnitId
    {
      get { return mOrganizationalUnitId; }
      set { mOrganizationalUnitId = value; }
    }

    #endregion

    public static OrganizationalUnit OrganizationalUnit
    {
      get
      {
        if (OrganizationalUnitId == Guid.Empty)
        {
          if (AuthenticationExtension.OrganizationalUnit != null) return AuthenticationExtension.OrganizationalUnit;
          return null;
        }
        return Cache.Instance.GetObject<OrganizationalUnit>(mOrganizationalUnitId);
      }
    }

    public static ServerInstance MasterServer
    {
      get { return Cache.Instance.GetObjects<ServerInstance>(si => BusinessObject.IsNull(si.Owner)).First(); }
    }

    static User mUser;
    public static User User
    {
      get
      {
        if (AuthenticationExtension.User != null) return AuthenticationExtension.User;
        return mUser;
      }

      private set
      {
        mUser = value;
      }
    }
  }
}

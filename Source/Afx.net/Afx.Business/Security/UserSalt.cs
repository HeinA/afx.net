﻿using Afx.Business.Attributes;
using Afx.Business.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.Security
{
  [PersistantObject(Schema = "Afx")]
  class UserSalt : BusinessObject
  {
    #region string Salt

    public const string PN_SALT = "Salt";
    string _salt;
    [PersistantProperty]
    public string Salt
    {
      get { return _salt; }
      set { SetProperty<string>(ref _salt, value, PN_SALT); }
    }

    #endregion
  }
}

﻿using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace Afx.Business.Security
{
  [DataContract(IsReference = true, Namespace = Namespace.Afx)]
  [PersistantObject(Schema = "Afx", OwnerColumn = "Parent")]
  [Cache(Priority = 0)]
  public class OrganizationalStructureType : BusinessObject<OrganizationalStructureType>, IRevisable
  {
    #region int Revision

    public const string RevisionProperty = "Revision";
    [DataMember(Name = RevisionProperty, EmitDefaultValue = false)]
    int mRevision = 1;
    [PersistantProperty]
    public int Revision
    {
      get { return mRevision; }
      set { SetProperty<int>(ref mRevision, value); }
    }

    #endregion

    #region DataMember string Name

    public const string NameProperty = "Name";
    [DataMember(Name = NameProperty, EmitDefaultValue = false)]
    string mName;
    [PersistantProperty]
    public string Name
    {
      get { return mName; }
      set { SetProperty<string>(ref mName, value, NameProperty); }
    }

    #endregion

    #region DataMember string SystemIdentifier

    public const string SystemIdentifierProperty = "SystemIdentifier";
    [DataMember(Name = SystemIdentifierProperty, EmitDefaultValue = false)]
    string mSystemIdentifier;
    [PersistantProperty]
    public string SystemIdentifier
    {
      get { return mSystemIdentifier; }
      set { SetProperty<string>(ref mSystemIdentifier, value, SystemIdentifierProperty); }
    }

    #endregion

    #region DataMember BusinessObjectCollection<OrganizationalStructureType> SubTypes

    [DataMember(Name = "SubTypes", EmitDefaultValue = false)]
    BusinessObjectCollection<OrganizationalStructureType> mSubTypes;
    [PersistantCollection]
    public BusinessObjectCollection<OrganizationalStructureType> SubTypes
    {
      get
      {
        if (mSubTypes == null) mSubTypes = new BusinessObjectCollection<OrganizationalStructureType>(this);
        return mSubTypes;
      }
    }

    #endregion

    #region void GetErrors(Collection<string> errors, string propertyName)

    protected override void GetErrors(Collection<string> errors, string propertyName)
    {
      if (propertyName == null || propertyName == NameProperty)
      {
        if (string.IsNullOrWhiteSpace(Name)) errors.Add("Name is a mandatory field.");
      }

      base.GetErrors(errors, propertyName);
    }

    #endregion
  }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business
{
  public interface IBusinessObject : INotifyPropertyChanged, IDataErrorInfo, ICloneable //, INotifyDataErrorInfo
  {
    int Id { get; set; }
    Guid GlobalIdentifier { get; }
    //bool IsNew { get; set; }
    bool IsDirty { get; set; }
    bool IsDeleted { get; set; }
    IBusinessObject Owner { get; }
    IEnumerable<IBusinessObject> Children { get; }
    void AddExtensionObject(IExtensionObject obj);
    T GetExtensionObject<T>() where T : class, IExtensionObject, new();
    IEnumerable<IExtensionObject> ExtensionObjects { get; }
    IEnumerable<IAssociativeObject> AssociativeObjects { get; }
    bool Validate();
    bool IsValid { get; }
  }
}

﻿using Microsoft.CSharp;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.Service
{
  public class ProxyFactory
  {
    static Dictionary<string, Assembly> mAssemblyDictionary = new Dictionary<string, Assembly>();

    static object mLock = new object();

    public static T GetService<T>(string serviceName = null)
      where T : class, IDisposable
    {
      lock (mLock)
      {
        string key = string.Format("{0}:{1}", serviceName, typeof(T).FullName);
        string className = string.Format("{0}_ClientProxy", typeof(T).Name);

        if (mAssemblyDictionary.ContainsKey(key))
        {
          Assembly assembly = mAssemblyDictionary[key];
          return assembly.CreateInstance(className) as T;
        }

        CSharpCodeProvider cs = new CSharpCodeProvider();

        CompilerParameters cp = new CompilerParameters();
        cp.GenerateInMemory = true;
        cp.GenerateExecutable = false;
        //cp.ReferencedAssemblies.Add("System.dll");
        cp.ReferencedAssemblies.Add("System.ServiceModel.dll");
        cp.ReferencedAssemblies.Add(typeof(ProxyFactory).Assembly.Location);
        cp.ReferencedAssemblies.Add(typeof(T).Assembly.Location);
        //string s = typeof(T).Assembly.Location;
        //if (!cp.ReferencedAssemblies.Contains(s)) cp.ReferencedAssemblies.Add(s);

        StringBuilder sb = new StringBuilder();
        sb.AppendFormat(@"
public class {0} : {1}
{{
  Afx.Business.Service.IDisposableWrapper<{1}> mSvc;
  string mSvcName;

  public {0}()
  {{
    mSvcName = ""{2}"";
    mSvc = Afx.Business.Service.ServiceFactory.GetService<{1}>(mSvcName);
  }}

", className, typeof(T).FullName, serviceName);
        foreach (MethodInfo mi in typeof(T).GetMethods())
        {
          ImplementMethod(cp, sb, mi, typeof(T).FullName);
        }

        sb.Append(@"
public void Dispose()
{
  mSvc.Dispose();
}
");

        sb.Append("}");

        CompilerResults results = cs.CompileAssemblyFromSource(cp, new string[1] { sb.ToString() });

        if (results.Errors.Count == 0)
        {
          Assembly assembly = results.CompiledAssembly;
          mAssemblyDictionary.Add(key, assembly);
          return assembly.CreateInstance(className) as T;
        }

        throw new InvalidProgramException("Proxy could not be generated");
      }
    }

    static void ImplementMethod(CompilerParameters cp, StringBuilder sb, MethodInfo mi, string className)
    {
      string returnType = GetTypeString(cp, mi.ReturnType);
      sb.AppendFormat("public {0} {1}(", returnType, mi.Name);
      foreach (ParameterInfo pi in mi.GetParameters())
      {
        sb.AppendFormat("{0}{1} {2}", pi.Position == 0 ? string.Empty : ", ", GetTypeString(cp, pi.ParameterType), pi.Name);
      }
      sb.Append(")\n{");
      sb.Append(@"
  bool bRetry = true;
  System.Exception lastException = null;
  int retries = 0;
  while(bRetry && retries++ <= 2)
  {
    bRetry = false;
    try
    {
");
      if (returnType != "void") sb.Append("return ");
      sb.AppendFormat("mSvc.Instance.{0}(", mi.Name);
      foreach (ParameterInfo pi in mi.GetParameters())
      {
        sb.AppendFormat("{0}{1}", pi.Position == 0 ? string.Empty : ", ", pi.Name);
      }
      sb.Append(");\n");
      if (returnType == "void") sb.Append("return;");
      sb.AppendFormat(@"
    }}
    catch(System.ServiceModel.FaultException ex)
    {{
      //Afx.Business.LogHelper.LogMessage(ex.ToString());
      throw ex;
    }}
    catch(System.ServiceModel.CommunicationException ex)
    {{
      lastException = ex;
      mSvc.Dispose();
      mSvc = Afx.Business.Service.ServiceFactory.GetService<{0}>(mSvcName);
      //Afx.Business.LogHelper.LogMessage(ex.ToString());
      System.Media.SystemSounds.Hand.Play();
      System.Threading.Thread.Sleep(1000);
      bRetry = true;
    }}
    catch(System.Exception ex)
    {{
      //Afx.Business.LogHelper.LogMessage(ex.ToString());
      throw ex;
    }}
  }}

  throw lastException;
}}
", className);
    }

    static string GetTypeString(CompilerParameters cp, Type t)
    {
      if (t == typeof(void)) return "void";
      
      string al = t.Assembly.Location;
      if (!cp.ReferencedAssemblies.Contains(al)) cp.ReferencedAssemblies.Add(al);

      foreach (Type ti in t.GetInterfaces())
      {
        al = ti.Assembly.Location;
        if (!cp.ReferencedAssemblies.Contains(al)) cp.ReferencedAssemblies.Add(al);
      }

      StringBuilder sb = new StringBuilder();
      if (t.IsGenericType)
      {
        sb.AppendFormat("{0}<", t.GetGenericTypeDefinition().FullName.Replace("`1", string.Empty));
        bool bFirst = true;
        foreach (Type t1 in t.GenericTypeArguments)
        {
          al = t1.Assembly.Location;
          if (!cp.ReferencedAssemblies.Contains(al)) cp.ReferencedAssemblies.Add(al);

          sb.AppendFormat("{0}{1}", bFirst ? string.Empty : ", ", t1.FullName);
          bFirst = false;
        }
        sb.Append(">");
      }
      else
      {
        sb.Append(t.FullName);
      }
      return sb.ToString();
    }
  }
}

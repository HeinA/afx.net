﻿using Afx.Business.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.Service
{
  public interface IService : IDisposable
  {
    IPersistanceManager PersistanceManager { get; }
  }
}

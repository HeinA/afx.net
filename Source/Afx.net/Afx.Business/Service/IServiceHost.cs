﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.Service
{
  [ServiceContract(Namespace = Namespaces.Afx)]
  public interface IServiceHost
  {
    [OperationContract(IsOneWay = true)]
    void Terminate();
  }
}

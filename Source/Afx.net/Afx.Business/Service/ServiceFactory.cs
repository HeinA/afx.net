﻿using Afx.Business.ServiceModel;
using Microsoft.Practices.Unity.InterceptionExtension;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Configuration;
using System.ServiceModel.Description;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.Service
{
  public interface IDisposableWrapper<T> : IDisposable
  {
    T Instance { get; }
  }

  public class DisposableWrapper<T> : IDisposableWrapper<T> where T : class
  {
    protected T InternalInstance { get; private set; }
    public T Instance { get; private set; }
    protected ChannelFactory<T> Factory { get; set; }

    public DisposableWrapper(ChannelFactory<T> factory)
    {
      Factory = factory;
      CreateInstance();
    }

    void CreateInstance()
    {
      InternalInstance = Factory.CreateChannel();   
      IClientChannel channel = InternalInstance as IClientChannel;      
      if (ServiceFactory.InterceptionBehaviors.Count > 0) Instance = Intercept.ThroughProxy<T>(InternalInstance, new InterfaceInterceptor(), ServiceFactory.InterceptionBehaviors.ToArray());
      else Instance = InternalInstance;
    }

    protected virtual void Dispose(bool disposing)
    {
      if (disposing)
      {
        (InternalInstance as IDisposable).Dispose();
      }
    }

    public void Dispose()
    {
      try
      {
        Dispose(true);
        GC.SuppressFinalize(this);
      }
      catch
      {
      }
      InternalInstance = Instance = default(T);
    }
  }

  public class ClientWrapper<TProxy> : DisposableWrapper<TProxy>
      where TProxy : class
  {
    public ClientWrapper(ChannelFactory<TProxy> proxy) : base(proxy) { }
    protected override void Dispose(bool disposing)
    {
      if (disposing)
      {
        if (this.InternalInstance != null)
        {
          IClientChannel channel = this.InternalInstance as IClientChannel;
          if (channel.State == CommunicationState.Faulted)
          {
            channel.Abort();
          }
          else
          {
            channel.Close();
          }
        }
      }

      base.Dispose(disposing);
    }
  }

  static class WCFExtensions
  {
    public static IDisposableWrapper<TProxy> Wrap<TProxy>(
        this ChannelFactory<TProxy> proxy)
        where TProxy : class
    {

      return new ClientWrapper<TProxy>(proxy);
    }
  }

  public static class ServiceFactory
  {
    static object mLock = new object();

    ///// <summary>
    ///// Creates the service factory in a background thread and caches it.
    ///// </summary>
    ///// <typeparam name="T"></typeparam>
    //public static void SpinUp<T>()
    //  where T : class
    //{
    //  BackgroundWorker bw = new BackgroundWorker();
    //  bw.DoWork += (s, e) =>
    //  {
    //    using (var svc = ServiceFactory.GetService<T>())
    //    {
    //    }
    //  };
    //  bw.RunWorkerCompleted += (s, e) =>
    //  {
    //    bw.Dispose();
    //  };
    //  bw.RunWorkerAsync();
    //}

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
    public static IDisposableWrapper<T> GetService<T>(string serviceName = null)
      where T : class
    {
      lock (mLock)
      {
        string key = serviceName == null ? string.Format("{0}", typeof(T).FullName) : string.Format("{0}::{1}", serviceName, typeof(T).FullName);

        System.Configuration.Configuration config = null;
        if (System.ServiceModel.OperationContext.Current != null)
          config = System.Web.Configuration.WebConfigurationManager.OpenWebConfiguration("~");
        else
          config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

        ConfigurationSectionGroup csg = config.GetSectionGroup("system.serviceModel");
        if (csg != null)
        {
          ConfigurationSection css = csg.Sections["client"];
          if (css != null && css is ClientSection)
          {
            ClientSection cs = (ClientSection)csg.Sections["client"];

            //if (!cs.Endpoints.ContainsKey(key))
            //  return null;

            bool found = false;
            foreach (ChannelEndpointElement ep in cs.Endpoints)
            {
              if (ep.Name.Equals(key))
              {
                found = true;
                break;
              }
            }

            if (!found) 
              return null;
          }
        } 
        
        ChannelFactory<T> factory = null;
        if (serviceName == null) factory = new ChannelFactory<T>(typeof(T).FullName);
        else factory = new ChannelFactory<T>(key);
        factory.Open();
        IDisposableWrapper<T> wrapper = WCFExtensions.Wrap<T>(factory);
        return wrapper;
      }
    }

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
    public static IDisposableWrapper<T> GetDynamicService<T>(Binding binding, string address)
      where T : class
    {
      lock (mLock)
      {
        ChannelFactory<T> factory = null;
        var ep = new EndpointAddress(address);
        factory = new ChannelFactory<T>(binding, ep);
        factory.Endpoint.EndpointBehaviors.Add(new AfxCompressedEndpointBehavior());
        factory.Open();
        IDisposableWrapper<T> wrapper = WCFExtensions.Wrap<T>(factory);
        return wrapper;
      }
    }

    static Collection<IInterceptionBehavior> mInterceptionBehaviors = new Collection<IInterceptionBehavior>();
    public static Collection<IInterceptionBehavior> InterceptionBehaviors
    {
      get { return mInterceptionBehaviors; }
    }
  }
}

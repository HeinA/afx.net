﻿using Afx.Business.Data;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.Service
{
  [ServiceContract(Namespace = Namespace.Afx)]
  public interface IAfxReplicationService
  {
    [OperationContract(IsOneWay = true)]
    void Replicate(BusinessObject obj, Guid source, PersistanceFlags flags);
  }
}

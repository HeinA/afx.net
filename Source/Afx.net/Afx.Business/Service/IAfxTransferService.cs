﻿using Afx.Business.Documents;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.Service
{
  [ServiceContract(Namespace = Namespace.Afx, SessionMode=SessionMode.Required)]
  public interface IAfxTransferService : IDisposable
  {
    [OperationContract]
    [TransactionFlow(TransactionFlowOption.Mandatory)]
    Document TransferDocument(Guid document, Guid targetOU);
  }
}

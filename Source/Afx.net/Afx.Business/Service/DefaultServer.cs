﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Afx.Business.Service
{
  public static class DefaultServer
  {
    static DefaultServer()
    {
      try
      {
        XmlDocument xd = new XmlDocument();
        xd.Load("Server.xml");
        ServerName = xd.DocumentElement.Attributes["name"].Value;
      }
      catch { }
    }

    #region string ServerName

    static string mServerName;
    public static string ServerName
    {
      get { return mServerName; }
      private set { mServerName = value; }
    }

    #endregion
  }
}

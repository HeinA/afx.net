﻿using Afx.Business.Activities;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Documents;
using Afx.Business.Security;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.WCF;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.Service
{
  [ServiceContract(Namespace = Namespace.Afx)]
  public partial interface IAfxService : IDisposable
  {
    #region Authentication

    [OperationContract]
    AuthenticationResult Login(string username, string passwordHash, LoginOrganizationalUnit loginOU);

    #endregion

    #region Users

    [OperationContract]
    BasicCollection<User> LoadUsers();

    [OperationContract]
    BasicCollection<User> SaveUsers(BasicCollection<User> col);

    #endregion

    #region Cache

    [OperationContract]
    BasicCollection<BusinessObject> LoadCache();

    #endregion

    #region OrganizationalStructure

    [OperationContract]
    BasicCollection<OrganizationalStructureType> LoadOrganizationalStructure();

    [OperationContract]
    BasicCollection<OrganizationalStructureType> SaveOrganizationalStructure(BasicCollection<OrganizationalStructureType> col);

    #endregion

    #region OrganizationalUnits

    [OperationContract]
    BasicCollection<LoginOrganizationalUnit> LoadLoginOrganizationalUnits();

    [OperationContract]
    BasicCollection<OrganizationalUnit> LoadOrganizationalUnits();

    [OperationContract]
    BasicCollection<OrganizationalUnit> SaveOrganizationalUnits(BasicCollection<OrganizationalUnit> col);


    #endregion

    #region Operations

    [OperationContract]
    BasicCollection<OperationGroup> LoadOperationGroups();

    [OperationContract]
    BasicCollection<OperationGroup> SaveOperationGroups(BasicCollection<OperationGroup> col);

    #endregion

    #region Activities

    [OperationContract]
    BasicCollection<Activity> LoadActivities();

    [OperationContract]
    BasicCollection<Activity> SaveActivities(BasicCollection<Activity> col);

    #endregion

    #region DocumentTypes

    [OperationContract]
    BasicCollection<DocumentType> LoadDocumentTypes();

    [OperationContract]
    BasicCollection<DocumentType> SaveDocumentTypes(BasicCollection<DocumentType> col);

    #endregion

    #region ServerInstance

    [OperationContract]
    BasicCollection<ServerInstance> LoadServerInstances();

    [OperationContract]
    BasicCollection<ServerInstance> SaveServerInstances(BasicCollection<ServerInstance> col);

    #endregion

    #region Documents

    [OperationContract]
    Document TransferAllDocuments(Guid document, Guid targetOU);

    //[OperationContract]
    //Document TransferDocument(Guid document, Guid targetOU);

    [OperationContract]
    Document SetDocumentState(Document doc, string targetStateIdentifier);

    [OperationContract]
    Document SetDocumentStateWithScope(Document doc, string targetStateIdentifier, DocumentStateEventScope scope);

    [OperationContract]
    bool ValidateNewDocumentNumber(string documentNumber, Guid documentIdentifier);

    #endregion

    #region Search

    [OperationContract]
    SearchResults Search(SearchDatagraph sg);

    #endregion

    #region ImportExport

    [OperationContract]
    BasicCollection<BusinessObject> LoadExport();

    [OperationContract]
    void Import(BasicCollection<BusinessObject> col);
    
    #endregion

    #region Settings

    [OperationContract]
    BasicCollection<Setting> SaveSettings(BasicCollection<Setting> col, string machine);

    [OperationContract]
    BasicCollection<Setting> LoadSettings(string machine);

    #endregion

    [OperationContract]
    ReportCredentials GetReportCredentials();

    [OperationContract]
    BasicCollection<SyncHeader> GetDocumentSyncHeaders(int maxAge);

    [OperationContract]
    Document LoadDocument(Guid globalIdentifier);

  }
}

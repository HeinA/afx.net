﻿using Afx.Business.Activities;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Documents;
using Afx.Business.Security;
using Afx.Business.Service.Configuration;
using Afx.Business.ServiceModel;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.WCF;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using Microsoft.Practices.Unity;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Messaging;
using System.Runtime.Serialization;
using System.Security.Cryptography;
using System.ServiceModel;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;
using System.Web;

namespace Afx.Business.Service
{
  [ServiceBehavior(Namespace = Namespace.Afx, ConcurrencyMode = ConcurrencyMode.Multiple, InstanceContextMode = InstanceContextMode.PerCall)]
  [ExceptionShielding]
  public partial class AfxService : ServiceBase, IAfxService
  {
    public AfxService()
    {
    }

    public static void Initialize()
    {
      try
      {
        IPersistanceManager pm = ExtensibilityManager.PersistanceManager;

        //ThreadPool.SetMinThreads(20, 5);

        UnityProvider.Instance = new UnityContainer();
        UnityProvider.Instance.RegisterInstance<IPersistanceManager>(pm);

        pm.RegisterAssembly(typeof(BusinessObject).Assembly);
        foreach (Type s in ExtensibilityManager.GetExportedTypes<IService>())
        {
          pm.RegisterAssembly(s.Assembly);
        }

        foreach (IPersistanceEventManager em in ExtensibilityManager.CompositionContainer.GetExportedValues<IPersistanceEventManager>())
        {
          pm.RegisterEventManager(em);
        }

        foreach (IDocumentStateEventManager em in ExtensibilityManager.CompositionContainer.GetExportedValues<IDocumentStateEventManager>())
        {
          pm.RegisterDocumentStateEventManager(em);
        }

        foreach (IDocumentTransferManager tm in ExtensibilityManager.CompositionContainer.GetExportedValues<IDocumentTransferManager>())
        {
          pm.RegisterDocumentTransferManager(tm);
        }

        IdMapper.Create(pm);

        int retries = 0;
        SqlException sqlEx = null;
        while (retries++ < 10)
        {
          try
          {
            sqlEx = null;
            Cache.Instance.LoadCache(Cache.FetchCache(pm));
            break;
          }
          catch (SqlException ex)
          {
            sqlEx = ex;
            //Afx.Business.LogHelper.LogMessage(ex.ToString());
            Thread.Sleep(500);
          }
        }
        if (sqlEx != null) throw sqlEx;
      }
      catch
      {
        //Afx.Business.LogHelper.LogMessage(ex.ToString());
        throw;
      }
    }

    //public void Import(BusinessObject obj)
    //{
    //  try
    //  {
    //    using (new ConnectionScope())
    //    using (new SqlScope("Replication"))
    //    {
    //      IInternalObjectRepository or = ((IInternalPersistanceManager)PersistanceManager).GetRepository(obj.GetType());
    //      or.Persist(obj, PersistanceFlags.Import);
    //    }
    //  }
    //  catch (Exception)
    //  {
    //    throw;
    //  }
    //}

    #region Removed

    //static void TimedImport()
    //{
    //  try
    //  {
    //    Stopwatch sw = new Stopwatch();
    //    sw.Start();

    //    ServerInstance si = ServerInstance;
    //    ServerInstance sir = ServerInstance.Root;
    //    if (sir != null && !sir.Equals(si) && !BusinessObject.IsNull(si.Owner))
    //    {
    //      BasicCollection<BusinessObject> col = null;
    //      using (var svc = ProxyFactory.GetService<IAfxService>(sir.Name))
    //      {
    //        col = svc.LoadExport();
    //      }

    //      ImportInternal(col, ExtensibilityManager.PersistanceManager);

    //      sw.Stop();
    //      LogHelper.LogMessage("Cache Import: {0:#,##0}ms", sw.ElapsedMilliseconds);
    //    }
    //  }
    //  catch(Exception ex)
    //  {
    //    LogHelper.LogMessage("Cache Import Failed: {0}", ex.Message);
    //  }
    //}

    #endregion




    #region Authentication

    [SuppressAuthentication]
    public AuthenticationResult Login(string username, string passwordHash, LoginOrganizationalUnit loginOU)
    {
      try
      {
        using (new ConnectionScope())
        using (new SqlScope("Login"))
        {
          Collection<IDataFilter> filters = new Collection<IDataFilter>();
          filters.Add(new DataFilter<User>(User.UsernameProperty, FilterType.Equals, username));
          filters.Add(new DataFilter<User>(User.PasswordHashProperty, FilterType.Equals, passwordHash));
          User user = PersistanceManager.GetRepository<User>().GetInstance(filters);
          if (user != null)
          {
            AuthenticationResult result = new AuthenticationResult(loginOU.Server, loginOU.GlobalIdentifier, user);
            return result;
          }
        }

        return new AuthenticationResult("Invalid username or password.");
      }
      catch
      {
        throw;
      }
    }

    #endregion

    #region Users

    public BasicCollection<User> LoadUsers()
    {
      return GetAllInstances<User>();
    }

    public BasicCollection<User> SaveUsers(BasicCollection<User> col)
    {
      return Persist<User>(col);
    }

    #endregion

    #region Cache

    object mCacheLoadLock = new object();
    public BasicCollection<BusinessObject> LoadCache()
    {
      lock (mCacheLoadLock)
      {
        try
        {
          return Cache.FetchCache(PersistanceManager);
        }
        catch (Exception ex)
        {
          string s = ex.Message;
          if (Debugger.IsAttached) Debugger.Break();
          throw;
        }
      }
    }

    #endregion

    #region ImportExport

    [SuppressAuthentication]
    public BasicCollection<BusinessObject> LoadExport()
    {
      try
      {
        using (new ConnectionScope())
        using (new SqlScope("Load Export"))
        {
          IInternalPersistanceManager pm = (IInternalPersistanceManager)PersistanceManager;
          BasicCollection<BusinessObject> col = new BasicCollection<BusinessObject>();
          foreach (Type t in ObjectAttributeHelper.ReplicationTypes)
          {
            IInternalObjectRepository or = pm.GetRepository(t);
            foreach (var obj in or.GetAllInstances())
            {
              col.Add((BusinessObject)obj);
            }
          }
          return col;
        }
      }
      catch
      {
        throw;
      }
    }

    static void ImportInternal(BasicCollection<BusinessObject> col, IPersistanceManager pm)
    {
      using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
      using (new ConnectionScope())
      using (new SqlScope("Import"))
      {
        try
        {
          ObjectRepository<BusinessObject> or = pm.GetRepository<BusinessObject>();
          or.Persist(col, PersistanceFlags.Import | PersistanceFlags.IgnoreRevision);
        }
        catch
        {
          throw;
        }

        scope.Complete();
      }
    }

    public void Import(BasicCollection<BusinessObject> col)
    {
      ImportInternal(col, PersistanceManager);
    }

    #endregion

    #region OrganizationalStructure

    public BasicCollection<OrganizationalStructureType> LoadOrganizationalStructure()
    {
      return GetAllInstances<OrganizationalStructureType>();
    }

    public BasicCollection<OrganizationalStructureType> SaveOrganizationalStructure(BasicCollection<OrganizationalStructureType> col)
    {
      return Persist<OrganizationalStructureType>(col);
    }

    #endregion

    #region OrganizationalUnits

    [SuppressAuthentication]
    public BasicCollection<LoginOrganizationalUnit> LoadLoginOrganizationalUnits()
    {
      var ret = GetAllInstances<LoginOrganizationalUnit>();
      return ret;
    }

    public BasicCollection<OrganizationalUnit> LoadOrganizationalUnits()
    {
      return GetAllInstances<OrganizationalUnit>();
    }

    public BasicCollection<OrganizationalUnit> SaveOrganizationalUnits(BasicCollection<OrganizationalUnit> col)
    {
      return Persist<OrganizationalUnit>(col);
    }


    #endregion

    #region Operations

    public BasicCollection<OperationGroup> LoadOperationGroups()
    {
      try
      {
        return GetAllInstances<OperationGroup>();
      }
      catch
      {
        throw;
      }
    }

    public BasicCollection<OperationGroup> SaveOperationGroups(BasicCollection<OperationGroup> col)
    {
      try
      {
        return Persist<OperationGroup>(col);
      }
      catch
      {
        throw;
      }
    }

    #endregion

    #region Activities

    public BasicCollection<Activity> LoadActivities()
    {
      return GetAllInstances<Activity>();
    }

    public BasicCollection<Activity> SaveActivities(BasicCollection<Activity> col)
    {
      try
      {
        return Persist<Activity>(col);
      }
      catch
      {
        throw;
      }
    }

    #endregion

    #region DocumentTypes

    public BasicCollection<DocumentType> LoadDocumentTypes()
    {
      return GetAllInstances<DocumentType>();
    }

    public BasicCollection<DocumentType> SaveDocumentTypes(BasicCollection<DocumentType> col)
    {
      try
      {
        return Persist<DocumentType>(col);
      }
      catch
      {
        throw;
      }
    }

    #endregion

    #region ServerInstance

    public BasicCollection<ServerInstance> LoadServerInstances()
    {
      return GetAllInstances<ServerInstance>();
    }

    public BasicCollection<ServerInstance> SaveServerInstances(BasicCollection<ServerInstance> col)
    {
      try
      {
        return Persist<ServerInstance>(col);
      }
      catch
      {
        throw;
      }
    }

    #endregion

    #region Documents

    public Document TransferAllDocuments(Guid document, Guid targetOU)
    {
      try
      {
        using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
        using (new ConnectionScope())
        {
          ObjectRepository<Document> or = PersistanceManager.GetRepository<Document>();
          Document d = or.GetInstance(document);
          d = DocumentHelper.TransferAllDocuments(d, targetOU, PersistanceManager);
          Replicate(PersistanceFlags.IgnoreControllingOU | PersistanceFlags.IgnoreRevision);
          scope.Complete();
          return d;
        }
      }
      catch (Exception ex)
      {
        throw ex;
      }
    }

    //public Document TransferDocument(Guid document, Guid targetOU)
    //{
    //  try
    //  {
    //    using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
    //    using (new ConnectionScope())
    //    {
    //      Document d = null;
    //      ObjectRepository<Document> or = PersistanceManager.GetRepository<Document>();
    //      d = or.GetInstance(document);
    //      Document d2 = DocumentHelper.TransferDocument(d, targetOU, PersistanceManager);
    //      Replicate(PersistanceFlags.IgnoreControllingOU | PersistanceFlags.IgnoreRevision);
    //      scope.Complete();
    //      return d2;
    //    }
    //  }
    //  catch (Exception ex)
    //  {
    //    throw ex;
    //  }
    //}

    public Document SetDocumentState(Document doc, string targetStateIdentifier)
    {
      return SetDocumentStateWithScope(doc, targetStateIdentifier, new DocumentStateEventScope());
    }

    public Document SetDocumentStateWithScope(Document doc, string targetStateIdentifier, DocumentStateEventScope scope)
    {
      Stopwatch sw = new Stopwatch();
      sw.Start();
      int i = 0;
      try
      {
        using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required, new TimeSpan(0, 5, 0)))
        using (new ConnectionScope())
        {
          DocumentHelper.SetState(doc, Cache.Instance.GetObject<DocumentTypeState>(targetStateIdentifier), scope, PersistanceFlags.None); //, true);
          i = 1;
          Replicate();
          i = 2;
          ts.Complete();
          i = 3;
        }

        using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Suppress))
        using (new ConnectionScope())
        {
          IInternalPersistanceManager ipm = (IInternalPersistanceManager)PersistanceManager;
          i = 4;
          IInternalObjectRepository repo = ipm.GetRepository(doc.GetType());
          i = 5;
          Document obj1 = (Document)repo.GetInstance(doc.GlobalIdentifier);
          i = 6;
          ts.Complete();
          return obj1;
        }
      }
      catch (MessageException)
      {
        throw;
      }
      catch(Exception ex)
      {
        throw new Exception(string.Format("Timer value {0:#,##0} ms ({1})", sw.ElapsedMilliseconds, i), ex);
      }
    }

    public bool ValidateNewDocumentNumber(string documentNumber, Guid documentIdentifier)
    {
      try
      {
        using (new ConnectionScope())
        using (SqlCommand cmd = (SqlCommand)PersistanceManager.GetCommand())
        {
          cmd.CommandText = "SELECT COUNT(1) FROM [Afx].[Document] D WHERE D.DocumentNumber=@p1 AND D.GlobalIdentifier<>@p2";
          cmd.Parameters.AddWithValue("@p1", documentNumber);
          cmd.Parameters.AddWithValue("@p2", documentIdentifier);
          int i = (int)cmd.ExecuteScalar();
          if (i == 1) return false;
        }
        return true;
      }
      catch
      {
        throw;
      }
    }

    #endregion

    #region Search

    public SearchResults Search(SearchDatagraph sg)
    {
      using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Suppress))
      using (new ConnectionScope())
      using (new SqlScope("Search"))
      {
        try
        {
          return ExecuteSearch(sg);
          //return sg.ExecuteQuery((IInternalPersistanceManager)PersistanceManager);
        }
        catch
        {
          throw;
        }
      }
    }

    #endregion


    #region Settings

    public BasicCollection<Setting> SaveSettings(BasicCollection<Setting> col, string machine)
    {
      try
      {
        using (new SqlScope("SaveSettings"))
        {
          Persist<Setting>(col, PersistanceFlags.ValidateIds | PersistanceFlags.Import, false);
          return LoadSettings(machine);
        }
      }
      catch
      {
        throw;
      }
    }

    public BasicCollection<Setting> LoadSettings(string machine)
    {
      return SettingsHelper.LoadSettings(PersistanceManager, machine);
    }

    #endregion

    public ReportCredentials GetReportCredentials()
    {
      return new ReportCredentials() { Server = ConfigurationManager.AppSettings["Reporting.Server"], Database = ConfigurationManager.AppSettings["Reporting.Database"], UserName = ConfigurationManager.AppSettings["Reporting.User"], Password = ConfigurationManager.AppSettings["Reporting.Password"] };
    }

    public BasicCollection<SyncHeader> GetDocumentSyncHeaders(int maxAge)
    {
      BasicCollection<SyncHeader> list = new BasicCollection<SyncHeader>();

      try
      {
        using (new ConnectionScope())
        using (SqlCommand cmd = (SqlCommand)PersistanceManager.GetCommand())
        {
          if (maxAge == 0)
          {
            cmd.CommandText = "select * from Afx.Document order by id";
          }
          else
          {
            cmd.CommandText = "select * from Afx.Document where datediff(d, DocumentDate, getdate()) <= @d order by id";
            cmd.Parameters.AddWithValue("@d", maxAge);
          }
          DataSet ds = DataHelper.ExecuteDataSet(cmd);
          foreach (DataRow dr in ds.Tables[0].Rows)
          {
            list.Add(new SyncHeader() { Id = (int)dr["Id"], GlobalIdentifier = (Guid)dr["GlobalIdentifier"], DocumentNumber = (string)dr["DocumentNumber"], Revision = (int)dr["Revision"] });
          }
        }
      }
      catch
      {
        throw;
      }

      return list;
    }

    public Document LoadDocument(Guid globalIdentifier)
    {
      try
      {
        using (new ConnectionScope())
        {
          IInternalObjectRepository or = ((IInternalPersistanceManager)PersistanceManager).GetRepository(typeof(Document));
          return (Document)or.GetInstance(globalIdentifier);
        }
      }
      catch
      {
        throw;
      }
    }

    void IDisposable.Dispose()
    {
    }
  }
}
﻿using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Documents;
using Afx.Business.Security;
using Afx.Business.Service.Configuration;
using Afx.Business.ServiceModel;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace Afx.Business.Service
{
  public abstract class ServiceBase : IService
  {
    public ServiceBase()
    {
    }

    static ServerInstance mServerInstance;
    public static ServerInstance ServerInstance
    {
      get { return ServiceBase.mServerInstance ?? (ServiceBase.mServerInstance = ServiceBase.GetServerInstance(AfxServiceSection.Default.ServerInstance)); }
    }

    public static ServerInstance GetServerInstance(string identifier)
    {
      return Cache.Instance.GetObject<ServerInstance>(Guid.Parse(identifier));
    }

    protected SearchResults ExecuteSearch(SearchDatagraph sg)
    {
      return sg.ExecuteQuery((IInternalPersistanceManager)PersistanceManager);
    }

    [Dependency]
    public IPersistanceManager PersistanceManager { get; set; }

    protected BasicCollection<T> GetAllInstances<T>()
      where T : BusinessObject
    {
      using (new ConnectionScope())
      using (new SqlScope("GetAllInstances"))
      {
        return PersistanceManager.GetRepository<T>().GetAllInstances();
      }
    }

    protected BasicCollection<T> GetInstances<T>(Collection<IDataFilter> filters)
      where T : BusinessObject
    {
      using (new ConnectionScope())
      using (new SqlScope("GetInstances"))
      {
        return PersistanceManager.GetRepository<T>().GetInstances(filters);
      }
    }

    protected BasicCollection<T> GetInstances<T>(Collection<Guid> identifiers)
      where T : BusinessObject
    {
      using (new ConnectionScope())
      using (new SqlScope("GetInstances"))
      {
        if (identifiers.Count == 0) identifiers.Add(Guid.Empty);

        DataFilter df = new DataFilter(typeof(T), BusinessObject.GlobalIdentifierProperty, FilterType.In, identifiers);
        Collection<IDataFilter> filters = new Collection<IDataFilter>();
        filters.Add(df);
        return PersistanceManager.GetRepository<T>().GetInstances(filters);
      }
    }

    protected T GetInstance<T>(int id)
      where T : BusinessObject
    {
      using (new ConnectionScope())
      using (new SqlScope("GetInstance"))
      {
        return PersistanceManager.GetRepository<T>().GetInstance(id);
      }
    }

    protected T GetInstance<T>(Guid globalIdentifier)
      where T : BusinessObject
    {
      using (new ConnectionScope())
      using (new SqlScope("GetInstance"))
      {
        return PersistanceManager.GetRepository<T>().GetInstance(globalIdentifier);
      }
    }

    protected T Persist<T>(T obj, PersistanceFlags flags = PersistanceFlags.None, bool requery = true)
      where T : BusinessObject
    {
      try
      {
        using (new SqlScope("Persist"))
        {
          using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
          using (new ConnectionScope())
          {
            ObjectRepository<T> repo = PersistanceManager.GetRepository<T>();
            repo.Persist(obj, flags);
            Replicate();
            scope.Complete();
          }

          if (!requery) return null;
          using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Suppress))
          using (new ConnectionScope())
          {
            ObjectRepository<T> repo = PersistanceManager.GetRepository<T>();
            T obj1 = repo.GetInstance(obj.GlobalIdentifier);
            scope.Complete();
            return obj1;
          }
        }
      }
      catch (UniqueIndexViolationException ex)
      {
        //if (typeof(Document).IsAssignableFrom(ex.Type))
        //{
        //  throw new MessageException("The document number is not unique.");
        //}

        throw ex;
      }
      catch
      {
        throw;
      }
    }

    protected BasicCollection<T> Persist<T>(BasicCollection<T> col, PersistanceFlags flags = PersistanceFlags.None, bool requery = true)
      where T : BusinessObject
    {
      try
      {
        using (new SqlScope("Persist"))
        {
          using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
          using (new ConnectionScope())
          {
            ObjectRepository<T> repo = PersistanceManager.GetRepository<T>();
            repo.Persist(col, flags);
            Replicate();
            scope.Complete();
          }

          if (!requery) return null;
          using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Suppress))
          using (new ConnectionScope())
          {
            ObjectRepository<T> repo = PersistanceManager.GetRepository<T>();
            BasicCollection<T> ret = repo.GetAllInstances();
            scope.Complete();
            return ret;
          }
        }
      }
      catch
      {
        throw;
      }
    }

    protected void Replicate(PersistanceFlags flags = PersistanceFlags.None)
    {
      var guid = Guid.Empty;
      if (Transaction.Current != null) guid = Transaction.Current.TransactionInformation.DistributedIdentifier;

      using(new TransactionScope(TransactionScopeOption.Suppress))
      try
      {
        if (CachedObjectReplicationExtension.Objects != null)
        {
          foreach (BusinessObject bo in CachedObjectReplicationExtension.Objects)
          {
            foreach (ServerInstance si in ServiceBase.ServerInstance.Satellites.Where(si1 => si1.IsEnabled))
            {
              if (!ServiceBase.ServerInstance.Equals(si))
              {
                using (var svc = ServiceFactory.GetService<IAfxReplicationService>(si.Name))
                {
                  if (svc == null) return;
                  svc.Instance.Replicate(bo, ServiceBase.ServerInstance.GlobalIdentifier, flags);
                }
              }
            }
          }
          CachedObjectReplicationExtension.Clear();
        }

        if (ObjectReplicationExtension.Current != null && ObjectReplicationExtension.Current.ObjectQueue.Count > 0)
        {
          ServerInstance master = ServiceBase.ServerInstance.Root;

          while (ObjectReplicationExtension.Current.ObjectQueue.Count > 0)
          {
            BusinessObject obj = ObjectReplicationExtension.Current.ObjectQueue.Dequeue();

            using (var svc = ServiceFactory.GetService<IAfxReplicationService>(master.Name))
            {
              if (svc == null) return;
              svc.Instance.Replicate(obj, ServiceBase.ServerInstance.GlobalIdentifier, flags);
            }
          }
        }

        if (DocumentReplicationExtension.Current != null && DocumentReplicationExtension.Current.DocumentStack.Count > 0)
        {
          ServerInstance master = ServiceBase.ServerInstance.Root;

          while (DocumentReplicationExtension.Current.DocumentStack.Count > 0)
          {
            Document obj = DocumentReplicationExtension.Current.DocumentStack.Pop();

            //COU
            //if (obj.ControllingOrganizationalUnit.Server.Equals(ServiceBase.ServerInstance) || (flags & PersistanceFlags.IgnoreControllingOU) == PersistanceFlags.IgnoreControllingOU)
            {
              using (var svc = ServiceFactory.GetService<IAfxReplicationService>(master.Name))
              {
                if (svc == null) return;
                svc.Instance.Replicate(obj, ServiceBase.ServerInstance.GlobalIdentifier, flags);
              }
            }
          }
        }
      }
      catch
      {
        if (Debugger.IsAttached) Debugger.Break();
        throw;
      }
    }

    protected Collection<Guid> GetIdentifierCollection<T>(BasicCollection<T> col)
      where T : BusinessObject
    {
      Collection<Guid> guids = new Collection<Guid>();
      foreach (BusinessObject bo in col)
      {
        guids.Add(bo.GlobalIdentifier);
      }
      return guids;
    }

    protected Collection<Guid> GetIdentifierCollection(SearchResults srs)
    {
      Collection<Guid> guids = new Collection<Guid>();
      foreach (SearchResult sr in srs.Results)
      {
        guids.Add(sr.GlobalIdentifier);
      }
      return guids;
    }

    public void Dispose()
    {
      Dispose(true);
    }

    protected virtual void Dispose(bool disposing)
    {
    }
  }
}

﻿using Afx.Business.Collections;
using Afx.Business.Service;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.WCF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.Service
{
  public partial class AfxService
  {
    public BasicCollection<Afx.Business.ExternalSystem> LoadExternalSystemCollection()
    {
      try
      {
        return GetAllInstances<Afx.Business.ExternalSystem>();
      }
      catch
      {
        throw;
      }
    }

    public BasicCollection<Afx.Business.ExternalSystem> SaveExternalSystemCollection(BasicCollection<Afx.Business.ExternalSystem> col)
    {
      try
      {
        return Persist<Afx.Business.ExternalSystem>(col);
      }
      catch
      {
        throw;
      }
    }
  }
}
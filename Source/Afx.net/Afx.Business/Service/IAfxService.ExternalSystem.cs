﻿using Afx.Business.Collections;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.Service
{
  public partial interface IAfxService
  {
    [OperationContract]
    BasicCollection<Afx.Business.ExternalSystem> LoadExternalSystemCollection();

    [OperationContract]
    BasicCollection<Afx.Business.ExternalSystem> SaveExternalSystemCollection(BasicCollection<Afx.Business.ExternalSystem> col);
  }
}


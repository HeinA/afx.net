﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Dataforge.Business.Service
{
  [AttributeUsage(AttributeTargets.Method | AttributeTargets.Class |
                  AttributeTargets.Interface)]
  public class BusinessTypesAttribute : Attribute, IOperationBehavior, IServiceBehavior, IContractBehavior
  {
    void IOperationBehavior.AddBindingParameters(
            OperationDescription description,
            BindingParameterCollection parameters)
    {
    }

    void IOperationBehavior.ApplyClientBehavior(
            OperationDescription description,
            ClientOperation proxy)
    {
      ReplaceDataContractSerializerOperationBehavior(description);
    }

    void IOperationBehavior.ApplyDispatchBehavior(
            OperationDescription description,
            DispatchOperation dispatch)
    {
      ReplaceDataContractSerializerOperationBehavior(description);
    }

    void IOperationBehavior.Validate(OperationDescription description)
    {
    }

    void IServiceBehavior.AddBindingParameters(
          ServiceDescription serviceDescription,
          ServiceHostBase serviceHostBase,
          System.Collections.ObjectModel.Collection<ServiceEndpoint> endpoints,
          BindingParameterCollection bindingParameters)
    {
      ReplaceDataContractSerializerOperationBehavior(serviceDescription);
    }

    void IServiceBehavior.ApplyDispatchBehavior(
            ServiceDescription serviceDescription,
            ServiceHostBase serviceHostBase)
    {
      ReplaceDataContractSerializerOperationBehavior(serviceDescription);
    }

    void IServiceBehavior.Validate(ServiceDescription serviceDescription,
            ServiceHostBase serviceHostBase)
    {
    }

    void IContractBehavior.AddBindingParameters(
            ContractDescription contractDescription,
            ServiceEndpoint endpoint,
            BindingParameterCollection bindingParameters)
    {
    }

    void IContractBehavior.ApplyClientBehavior(
            ContractDescription contractDescription,
            ServiceEndpoint endpoint, ClientRuntime clientRuntime)
    {
      ReplaceDataContractSerializerOperationBehavior(contractDescription);
    }

    void IContractBehavior.ApplyDispatchBehavior(
            ContractDescription contractDescription,
            ServiceEndpoint endpoint, DispatchRuntime dispatchRuntime)
    {
      ReplaceDataContractSerializerOperationBehavior(contractDescription);
    }

    void IContractBehavior.Validate(ContractDescription contractDescription,
            ServiceEndpoint endpoint)
    {
    }

    private static void ReplaceDataContractSerializerOperationBehavior(
            ServiceDescription description)
    {
      foreach (var endpoint in description.Endpoints)
      {
        ReplaceDataContractSerializerOperationBehavior(endpoint);
      }
    }

    private static void ReplaceDataContractSerializerOperationBehavior(
            ContractDescription description)
    {
      foreach (var operation in description.Operations)
      {
        ReplaceDataContractSerializerOperationBehavior(operation);
      }
    }

    private static void ReplaceDataContractSerializerOperationBehavior(
            ServiceEndpoint endpoint)
    {
      // ignore mex
      if (endpoint.Contract.ContractType == typeof(IMetadataExchange))
      {
        return;
      }
      ReplaceDataContractSerializerOperationBehavior(endpoint.Contract);
    }

    private static void ReplaceDataContractSerializerOperationBehavior(
            OperationDescription description)
    {
      var behavior =
       description.Behaviors.Find<DataContractSerializerOperationBehavior>();
      if (behavior != null)
      {
        description.Behaviors.Remove(behavior);
        description.Behaviors.Add(
            new ShapeDataContractSerializerOperationBehavior(description));
      }
    }

    public class ShapeDataContractSerializerOperationBehavior
            : DataContractSerializerOperationBehavior
    {
      public ShapeDataContractSerializerOperationBehavior(
              OperationDescription description)
        : base(description) { }

      public override XmlObjectSerializer CreateSerializer(Type type,
              string name, string ns, IList<Type> knownTypes)
      {
        
        return new DataContractSerializer(type, name, ns, ServiceLocator.Instance.KnownTypes);
      }

      public override XmlObjectSerializer CreateSerializer(Type type,
              XmlDictionaryString name, XmlDictionaryString ns,
              IList<Type> knownTypes)
      {
        return new DataContractSerializer(type, name, ns, ServiceLocator.Instance.KnownTypes);
      }
    }
  }
}

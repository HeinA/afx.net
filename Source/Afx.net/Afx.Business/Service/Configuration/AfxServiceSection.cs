﻿using Afx.Business.Data;
using Afx.Business.Security;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.Service.Configuration
{
  public class AfxServiceSection : ConfigurationSection
  {
    public static AfxServiceSection Default
    {
      get { return ConfigurationManager.GetSection(AfxServiceSection.SectionName) as AfxServiceSection; }
    }

    public const string SectionName = "afxService";

    [ConfigurationProperty("serverInstance", IsRequired = true)]
    public string ServerInstance
    {
      get { return (string)this["serverInstance"]; }
      set { this["serverInstance"] = value; }
    }

    //[ConfigurationProperty("satelliteServices", IsDefaultCollection = false)]
    //public AfxSatelliteServiceCollection SatelliteServices
    //{
    //  get { return (AfxSatelliteServiceCollection)base["satelliteServices"]; }
    //}


    //public string GetSatelliteEndpointPrefix(OrganizationalUnit ou)
    //{
    //  foreach (AfxSatelliteServiceElement e in SatelliteServices)
    //  {
    //    if (e.OrganizationalUnit == ou.GlobalIdentifier.ToString()) return e.EndpointPrefix;
    //  }
    //  return null;
    //}
  }
}

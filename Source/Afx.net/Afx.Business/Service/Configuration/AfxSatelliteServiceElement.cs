﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.Service.Configuration
{
  public class AfxSatelliteServiceElement : ConfigurationElement
  {
    [ConfigurationProperty("organizationalUnit", IsKey = true, IsRequired = true)]
    public string OrganizationalUnit
    {
      get { return (string)this["organizationalUnit"]; }
      set { this["organizationalUnit"] = value; }
    }

    [ConfigurationProperty("endpointPrefix", IsRequired = true)]
    public string EndpointPrefix
    {
      get { return (string)this["endpointPrefix"]; }
      set { this["endpointPrefix"] = value; }
    }
  }
}

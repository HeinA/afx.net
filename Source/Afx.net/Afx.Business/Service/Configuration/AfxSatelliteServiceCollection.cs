﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.Service.Configuration
{
  public class AfxSatelliteServiceCollection : ConfigurationElementCollection
  {
    protected override ConfigurationElement CreateNewElement()
    {
      return new AfxSatelliteServiceElement();
    }

    protected override object GetElementKey(ConfigurationElement element)
    {
      return ((AfxSatelliteServiceElement)element).OrganizationalUnit;
    }
  }
}

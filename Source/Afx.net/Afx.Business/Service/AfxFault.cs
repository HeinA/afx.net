﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.Service
{
  [DataContract(Namespace = Namespace.Afx)]
  public abstract class AfxFault : BusinessObject
  {
    [DataMember]
    string mMessage = string.Empty;
    public string Message
    {
      get { return mMessage; }
      set { mMessage = value; }
    }
  }
}

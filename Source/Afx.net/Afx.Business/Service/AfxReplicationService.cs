﻿using Afx.Business.Data;
using Afx.Business.Documents;
using Afx.Business.Security;
using Afx.Business.Service.Configuration;
using Afx.Business.ServiceModel;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.WCF;
using Microsoft.Practices.Unity;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.Service
{
  [ServiceBehavior(Namespace = Namespace.Afx
    , ConcurrencyMode = ConcurrencyMode.Single
    , InstanceContextMode = InstanceContextMode.Single)]
  [ExceptionShielding]
  public class AfxReplicationService : IAfxReplicationService
  {
    [OperationBehavior(TransactionScopeRequired = true)]
    public void Replicate(BusinessObject obj, Guid source, PersistanceFlags flags)
    {
      IInternalPersistanceManager pm = (IInternalPersistanceManager)UnityProvider.Instance.Resolve<IPersistanceManager>();

      try
      {
        using (new ConnectionScope())
        using (new SqlScope("Replication"))
        {
          IInternalObjectRepository or = pm.GetRepository(obj.GetType());
          ServerInstance siSource = Cache.Instance.GetObject<ServerInstance>(source);

          ReplicateSatellites(obj, flags);

          //COU
          //Document d = obj as Document;
          //if (d != null)
          //{
          //  if (!d.ControllingOrganizationalUnit.Server.Equals(ServiceBase.ServerInstance))
          //  {
          //    or.Persist(obj, PersistanceFlags.Import | flags);
          //  }
          //}
          //else
          //{
          //  if (!ServiceBase.ServerInstance.Equals(siSource))
          //  {
          //    or.Persist(obj, PersistanceFlags.Import | flags);
          //  }
          //}

          if (!ServiceBase.ServerInstance.Equals(siSource))
          {
            or.Persist(obj, PersistanceFlags.Import | flags);
          }
        }
      }
      catch(Exception ex)
      {
        DirectoryInfo di = new DirectoryInfo(@"C:\Logs");
        if (!di.Exists) di.Create();
        using (StreamWriter sw = new StreamWriter(@"C:\Logs\ReplicationErrors.txt", true))
        {
          sw.WriteLine("{0:D} ******************************************************************");
          sw.WriteLine(ex.ToString());
        }
        ExceptionHelper.HandleServerException(ex);
        throw;
      }
    }

    #region Private

    void ReplicateSatellites(BusinessObject obj, PersistanceFlags flags)
    {
      foreach (ServerInstance si in ServiceBase.ServerInstance.Satellites.Where(si1 => si1.IsEnabled))
      {
        try
        {
          if (!ServiceBase.ServerInstance.Equals(si))
          {
            using (var svc = ServiceFactory.GetService<IAfxReplicationService>(si.Name))
            {
              svc.Instance.Replicate(obj, ServiceBase.ServerInstance.GlobalIdentifier, flags);
            }
          }
        }
        catch
        {
          throw;
        }
      }
    }

    #endregion
  }
}

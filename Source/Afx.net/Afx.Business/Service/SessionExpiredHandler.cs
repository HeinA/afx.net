﻿using Dataforge.Business.Security;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.Configuration;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Business.Service
{
  [ConfigurationElementType(typeof(CustomHandlerData))]
  public class SessionExpiredHandler : IExceptionHandler
  {
    public SessionExpiredHandler(NameValueCollection values)
    {
    }

    public Exception HandleException(Exception exception, Guid handlingInstanceId)
    {
      return null; // new SessionExpiredException();
    }
  }
}

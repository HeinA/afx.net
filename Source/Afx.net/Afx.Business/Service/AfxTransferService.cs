﻿using Afx.Business.Data;
using Afx.Business.Documents;
using Afx.Business.ServiceModel;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.WCF;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace Afx.Business.Service
{
  [ServiceBehavior(Namespace = Namespace.Afx
    , ConcurrencyMode = ConcurrencyMode.Single
    , InstanceContextMode = InstanceContextMode.Single)]
  [ExceptionShielding]
  public class AfxTransferService : ServiceBase, IAfxTransferService
  {
    [OperationBehavior(TransactionScopeRequired = true)]
    public Document TransferDocument(Guid document, Guid targetOU)
    {
      try
      {
        //using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
        using (new ConnectionScope())
        {
          string lid = System.Transactions.Transaction.Current.TransactionInformation.LocalIdentifier;
          Guid did = System.Transactions.Transaction.Current.TransactionInformation.DistributedIdentifier;

          Document d = null;
          IInternalPersistanceManager pm = (IInternalPersistanceManager)UnityProvider.Instance.Resolve<IPersistanceManager>();

          ObjectRepository<Document> or = pm.GetRepository<Document>();
          d = or.GetInstance(document);
          Document d2 = DocumentHelper.TransferDocument(d, targetOU, pm);
          Replicate(PersistanceFlags.IgnoreControllingOU | PersistanceFlags.IgnoreRevision);
          //scope.Complete();
          return d2;
        }
      }
      catch (Exception ex)
      {
        throw ex;
      }
    }
  }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.Service
{
  [ServiceContract(Namespace = Namespace.Afx)]
  public interface IAfxLogService
  {
    [OperationContract(IsOneWay = true)]
    void LogMessage(string source, DateTime timestamp, string message);

    [OperationContract(IsOneWay = true)]
    void LogSql(string source, string scope, DateTime timestamp, string message, SqlLogParameter[] parameters);
  }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business
{
  [AttributeUsage(AttributeTargets.Class)]
  public sealed class ExtensionAttribute : Attribute
  {
  }
}

﻿using Afx.Business;
using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Security;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace Afx.Business
{
  [PersistantObject(Schema = "Afx")]
  public abstract partial class UserSetting : Setting
  {
    #region Constructors

    public UserSetting()
    {
    }

    #endregion

    #region UserReference User

    public const string UserProperty = "User";
    [PersistantProperty]
    public UserReference User
    {
      get { return GetCachedObject<UserReference>(); }
      set { SetCachedObject<UserReference>(value); }
    }

    #endregion
  }
}

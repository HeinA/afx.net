﻿using Afx.Business;
using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Validation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.Runtime.Serialization;

namespace Afx.Business.Settings
{
  [PersistantObject(Schema = "Afx")]
  [Export(typeof(Setting))]
  [DataContract(IsReference = true, Namespace = Afx.Business.Namespace.Afx)]
  public class ReportingServerSetting : Setting 
  {
    #region Constructors

    public ReportingServerSetting()
    {
    }

    #endregion

    #region string Name

    public override string Name
    {
      get { return "Reporting"; }
    }

    #endregion

    #region string SettingGroupIdentifier

    protected override string SettingGroupIdentifier
    {
      get { return "46f1a0e8-072e-4eda-9217-56dbc928e9d5"; }
    }

    #endregion

    #region string URL
    
    public const string URLProperty = "URL";
    [DataMember(Name = URLProperty, EmitDefaultValue=false)]
    string mURL;
    [PersistantProperty]
    public string URL
    {
      get { return mURL; }
      set { SetProperty<string>(ref mURL, value); }
    }
    
    #endregion
  }
}

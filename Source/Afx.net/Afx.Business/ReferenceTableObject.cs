﻿using Dataforge.Business.Collections;
using Dataforge.Business.Constants;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Business
{
  [DataContract(IsReference = true, Namespace = Namespaces.Dataforge)]
  public abstract class ReferenceTableObject<TColumn, TRow, TCell> : BusinessObject, ITableObject
    where TColumn : BusinessObject
    where TRow : BusinessObject
    where TCell : TableCell<TColumn, TRow>, new()
  {
    protected override void OnOwnerChanged()
    {
      ((IInternalOwnedCollection)Cells).SetOwner(Owner);

      base.OnOwnerChanged();
    }

    #region Columns

    [DataMember(Name = "Columns")]
    BusinessObjectCollection<TColumn> _unorderedColumns;
    BusinessObjectCollection<TColumn> UnorderedColumns
    {
      get
      {
        if (_unorderedColumns == null) _unorderedColumns = new BusinessObjectCollection<TColumn>();
        return _unorderedColumns;
      }
    }

    protected abstract Func<TColumn, object> ColumnSorter
    {
      get;
    }

    [IgnoreDataMember]
    ReadOnlyCollection<TColumn> _columns;
    public ReadOnlyCollection<TColumn> Columns
    {
      get
      {
        if (_columns == null)
        {
          IList<TColumn> list = _unorderedColumns.Where(o => o.IsDeleted == false).OrderBy(ColumnSorter).ToList();
          _columns = new ReadOnlyCollection<TColumn>(list);
        }
        return _columns;
      }
      set
      {
        if (_columns != value)
        {
          _columns = value;
        }
      }
    }

    internal void AddColumn(BusinessObject column)
    {
      AddColumn((TColumn)column);
    }

    public void AddColumn(TColumn column)
    {
      try
      {
        if (UnorderedColumns.Contains(column)) return;
        UnorderedColumns.Add(column);
        foreach (TRow row in UnorderedRows)
        {
          TCell cell = Cells.FirstOrDefault(c => c.RowId == row.Id && c.ColumnId == column.Id);
          if (cell == null)
          {
            cell = new TCell();
            cell.Column = column;
            cell.Row = row;
            Cells.Add(cell);
          }
          else
          {
            cell.IsDeleted = false;
          }
        }
      }
      finally
      {
        Columns = null;
        OnPropertyChanged("Columns");
      }
    }

    public void RemoveColumn(TColumn col)
    {
      foreach (TCell cell in Cells.Where(c => c.ColumnId == col.Id))
      {
        cell.IsDeleted = true;
      }
      UnorderedColumns.Remove(col);
      Columns = null;
      OnPropertyChanged("Columns");
    }

    public void RemoveColumnAt(int i)
    {
      RemoveColumn(Columns[i]);
    }

    TColumn GetColumnAt(int i)
    {
      return Columns[i];
    }

    int GetColumnIndex(TColumn column)
    {
      return Columns.IndexOf(column);
    }

    #endregion

    #region Rows

    [DataMember(Name = "Rows")]
    BusinessObjectCollection<TRow> _unorderedRows;
    BusinessObjectCollection<TRow> UnorderedRows
    {
      get
      {
        if (_unorderedRows == null) _unorderedRows = new BusinessObjectCollection<TRow>();
        return _unorderedRows;
      }
    }

    protected abstract Func<TRow, object> RowSorter
    {
      get;
    }

    [IgnoreDataMember]
    ReadOnlyCollection<TRow> _rows;
    public ReadOnlyCollection<TRow> Rows
    {
      get
      {
        if (_rows == null)
        {
          IList<TRow> list = _unorderedRows.Where(o => o.IsDeleted == false).OrderBy(RowSorter).ToList();
          _rows = new ReadOnlyCollection<TRow>(list);
        }
        return _rows;
      }
      set
      {
        if (_rows != value)
        {
          _rows = value;
        }
      }
    }

    internal void AddRow(BusinessObject row)
    {
      AddRow((TRow)row);
    }

    public void AddRow(TRow row)
    {
      try
      {
        if (UnorderedRows.Contains(row)) return;
        UnorderedRows.Add(row);
        foreach (TColumn col in UnorderedColumns)
        {
          TCell cell = Cells.FirstOrDefault(c => c.RowId == row.Id && c.ColumnId == col.Id);
          if (cell == null)
          {
            cell = new TCell();
            cell.Column = col;
            cell.Row = row;
            Cells.Add(cell);
          }
          else
          {
            cell.IsDeleted = false;
          }
        }
      }
      finally
      {
        Rows = null;
        OnPropertyChanged("Rows");
      }
    }

    public void RemoveRow(TRow row)
    {
      foreach (TCell cell in Cells.Where(c => c.RowId == row.Id))
      {
        cell.IsDeleted = true;
      }
      UnorderedRows.Remove(row);
      Rows = null;
      OnPropertyChanged("Rows");
    }

    public void RemoveRowAt(int i)
    {
      RemoveRow(Rows[i]);
    }

    TRow GetRowAt(int i)
    {
      return Rows[i];
    }

    int GetRowIndex(TRow row)
    {
      return Rows.IndexOf(row);
    }

    #endregion

    #region Cells

    [DataMember(Name = "Cells")]
    BusinessObjectCollection<TCell> _cells;
    BusinessObjectCollection<TCell> Cells
    {
      get
      {
        if (_cells == null) _cells = new BusinessObjectCollection<TCell>(this);
        return _cells;
      }
    }

    public TCell this[TColumn col, TRow row]
    {
      get
      {
        return Cells.FirstOrDefault(c => c.ColumnId == col.Id && c.RowId == row.Id);
      }
    }

    public TCell this[int col, int row]
    {
      get
      {
        return Cells.FirstOrDefault(c => Columns.IndexOf((TColumn)c.Column) == col && Rows.IndexOf((TRow)c.Row) == row);
      }
    }

    void ITableObject.AddCell(ITableCell cell)
    {
      Cells.Add((TCell)cell);
      TColumn col = UnorderedColumns.FirstOrDefault(c => c.Id == cell.ColumnId);
      TRow row = UnorderedRows.FirstOrDefault(c => c.Id == cell.RowId);
      if (col == null)
        UnorderedColumns.Add((TColumn)cell.BusinessObjectColumn);
      //else
      //  cell.Column = col;
      if (row == null)
        UnorderedRows.Add((TRow)cell.BusinessObjectRow);
      //else
      //  cell.Row = row;
    }


    #region Serialization

    [OnDeserialized]
    void OnDeserialized(StreamingContext ctx)
    {
      ((IInternalOwnedCollection)UnorderedColumns).SetOwner(this);
      ((IInternalOwnedCollection)UnorderedRows).SetOwner(this);
      ((IInternalOwnedCollection)Cells).SetOwner(this);
    }

    #endregion

    #endregion
  }
}

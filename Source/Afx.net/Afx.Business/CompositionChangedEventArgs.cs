﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business
{
  public enum CompositionChangedType
  {
    PropertyChanged,
    ChildAdded,
    ChildRemoved
  }

  public class CompositionChangedEventArgs : EventArgs
  {
    public CompositionChangedEventArgs(CompositionChangedType compositionChangedType, string propertyName, object originalSource, BusinessObject item)
      : base()
    {
      CompositionChangedType = compositionChangedType;
      Item = item;
      OriginalSource = originalSource;
      PropertyName = propertyName;
    }

    public CompositionChangedType CompositionChangedType { get; private set; }
    public string PropertyName { get; private set; }
    public object OriginalSource { get; private set; }
    public BusinessObject Item { get; private set; }
  }
}

﻿using Afx.Business.Collections;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.Validation
{
  public class MandatoryAttribute : ValidationAttribute
  {
    public MandatoryAttribute(string message)
      :base(message)
    {
    }

    public override bool Validate(object value)
    {
      if (value == null) return false;
      if (value is string && string.IsNullOrWhiteSpace((string)value)) return false;
      if (value.GetType().IsValueType && value.Equals(Activator.CreateInstance(value.GetType()))) return false;
      if (value is BusinessObject && ((BusinessObject)value).GlobalIdentifier == Guid.Empty) return false;
      IInternalOwnedCollection oc = value as IInternalOwnedCollection;
      if (oc != null && oc.CountUndeleted == 0) return false;
      return true;
    }
  }
}

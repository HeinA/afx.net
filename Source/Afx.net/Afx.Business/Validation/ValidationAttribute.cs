﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.Validation
{
  [AttributeUsage(AttributeTargets.Property, AllowMultiple=true)]
  public abstract class ValidationAttribute : Attribute
  {
    protected ValidationAttribute(string message)
    {
      Message = message;
    }


    #region string Message

    string mMessage;
    public string Message
    {
      get { return mMessage; }
      protected set { mMessage = value; }
    }

    #endregion

    public abstract bool Validate(object value);
  }
}

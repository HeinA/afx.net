﻿using Afx.Business.Collections;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.Validation
{
  public class MinimumStringLengthAttribute : ValidationAttribute
  {
    public MinimumStringLengthAttribute(string message)
      : base(message)
    {
    }

    public MinimumStringLengthAttribute(string message, int minimumlength)
      : base(message)
    {
      MinimumLength = minimumlength;
    }

    #region int MinimumLength

    public const string MinimumLengthProperty = "MinimumLength";
    int mMinimumLength = 3;
    public int MinimumLength
    {
      get { return mMinimumLength; }
      private set { mMinimumLength = value; }
    }

    #endregion

    public override bool Validate(object value)
    {
      if (value == null) return false;
      if (!(value is string)) throw new InvalidOperationException("Property must be a STRING type.");
      if (value is string && string.IsNullOrWhiteSpace((string)value)) return false;
      if (((string)value).Length <= MinimumLength) return false;
      return true;
    }
  }
}

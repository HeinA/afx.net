﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business
{
  public class ObjectCatalog
  {
    public static List<WeakReference<BusinessObject>> mObjects = new List<WeakReference<BusinessObject>>();
    static object mLock = new object();

    public static void RegisterObject(BusinessObject bo)
    {
      lock (mLock)
      {
        mObjects.Add(new WeakReference<BusinessObject>(bo));
      }
    }

    public static void Purge()
    {
      lock (mLock)
      {
        Queue<WeakReference<BusinessObject>> purgeQueue = new Queue<WeakReference<BusinessObject>>();
        foreach (var wr in mObjects)
        {
          BusinessObject o = null;
          if (!wr.TryGetTarget(out o)) purgeQueue.Enqueue(wr);
        }
        while (purgeQueue.Count > 0) mObjects.Remove(purgeQueue.Dequeue());
      }
    }
  }
}

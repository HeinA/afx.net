﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business
{
  public interface IExternalReference
  {
    ExternalSystem ExternalSystem { get; set; }
    string Reference { get; set; }
  }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business
{
  public interface IAssociativeObject : IBusinessObject
  {
    Guid ReferenceId
    {
      get;
      //set;
    }

    BusinessObject Reference
    {
      get;
      set;
    }

    bool ReferenceIsOwned
    {
      get;
    }
  }
}

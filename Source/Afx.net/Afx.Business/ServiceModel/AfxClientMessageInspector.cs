﻿using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Business.Service;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Security.Cryptography;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Dispatcher;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.ServiceModel
{
  public class AfxClientMessageInspector : IClientMessageInspector
  {
    public const string SessionToken = "SessionToken";
    public const string User = "User";
    public const string OrganizationalUnit = "OrganizationalUnit";
    public const string Nonce = "Nonce";
    public const string PasswordDigest = "PasswordDigest";

    public void AfterReceiveReply(ref Message reply, object correlationState)
    {
      bool refreshCache = false;
      if (reply.Headers.FindHeader(AfxDispatchMessageInspector.RefreshCache, Namespace.Afx) >= 0) 
        refreshCache = reply.Headers.GetHeader<bool>(AfxDispatchMessageInspector.RefreshCache, Namespace.Afx);

      if (refreshCache)
      {
        Stopwatch sw = new Stopwatch();
        sw.Start();
        BasicCollection<BusinessObject> cache = null;
        BackgroundWorker bw = new BackgroundWorker();
        bw.DoWork += (sender, e) =>
        {
          try
          {
            using (var svc = ProxyFactory.GetService<IAfxService>(SecurityContext.MasterServer.Name))
            {
              cache = svc.LoadCache();
            }
          }
          catch { }
        };
        bw.RunWorkerCompleted += (sender, e) =>
        {
          try
          {
            if (e.Error == null)
            {
              Cache.Instance.LoadCache(cache);
              sw.Stop();
              //LogHelper.LogMessage("Client Cache Load: {0:#,##0}ms", sw.ElapsedMilliseconds);
#if DEBUG
              System.Media.SystemSounds.Exclamation.Play();
#endif
            }
            else
            {
#if DEBUG
              if (Debugger.IsAttached) Debugger.Break();
#endif
              //if (ExceptionHelper.HandleException(e.Error)) throw e.Error;
            }
            bw.Dispose();
          }
          catch
          {
            //throw;
          }
        };
        bw.RunWorkerAsync();
      }
    }

    public object BeforeSendRequest(ref Message request, IClientChannel channel)
    {
      //MessageHeader<Guid> customHeader = new MessageHeader<Guid>(SecurityContext.ImpersonationToken != Guid.Empty ? SecurityContext.ImpersonationToken : SecurityContext.SessionToken);
      //MessageHeader h = customHeader.GetUntypedHeader(SessionToken, Namespaces.Afx);
      //request.Headers.Add(h);

      if (SecurityContext.User != null)
      {
        MessageHeader<Guid> customHeader = new MessageHeader<Guid>(SecurityContext.User.GlobalIdentifier);
        MessageHeader h = customHeader.GetUntypedHeader(User, Namespace.Afx);
        request.Headers.Add(h);

        if (SecurityContext.OrganizationalUnit != null)
        {
          customHeader = new MessageHeader<Guid>(SecurityContext.OrganizationalUnit.GlobalIdentifier);
          h = customHeader.GetUntypedHeader(OrganizationalUnit, Namespace.Afx);
          request.Headers.Add(h);
        }
        else
        {
          customHeader = new MessageHeader<Guid>(Guid.Empty);
          h = customHeader.GetUntypedHeader(OrganizationalUnit, Namespace.Afx);
          request.Headers.Add(h);
        }

        Guid nonce = Guid.NewGuid();
        customHeader = new MessageHeader<Guid>(nonce);
        h = customHeader.GetUntypedHeader(Nonce, Namespace.Afx);
        request.Headers.Add(h);

        MD5 md5 = System.Security.Cryptography.MD5.Create();
        byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(string.Format("{0}{1}", nonce, SecurityContext.User.PasswordHash));
        byte[] hash = md5.ComputeHash(inputBytes);

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < hash.Length; i++)
        {
          sb.Append(hash[i].ToString("X2"));
        }

        MessageHeader<string> customHeader1 = new MessageHeader<string>(sb.ToString());
        h = customHeader1.GetUntypedHeader(PasswordDigest, Namespace.Afx);
        request.Headers.Add(h);
      }
//#if DEBUG
//      else
//      {
//        if (request.Headers.Action != "http://openafx.net/Business/IAfxService/Login"
//          && request.Headers.Action != "http://openafx.net/Business/IAfxLogService/LogMessage"
//          && request.Headers.Action != "http://openafx.net/Business/IAfxLogService/LogSql"
//          && request.Headers.Action != "http://openafx.net/Business/IAfxLogService/Exit")
//        {
//          Debugger.Break();
//        }
//      }
//#endif

      return Guid.NewGuid();
    }
  }
}

﻿using Afx.Business.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.ServiceModel
{
  public class AuthenticationExtension : IExtension<OperationContext>
  {
    OperationContext mOperationContext;
    OperationContext OperationContext
    {
      get { return mOperationContext; }
      set { mOperationContext = value; }
    }

    User mUser;
    public static User User
    {
      get
      {
        if (Current == null) return null;
        return Current.mUser;
      }
      internal set
      {
        if (Current != null) Current.mUser = value;
      }
    }

    OrganizationalUnit mOrganizationalUnit;
    public static OrganizationalUnit OrganizationalUnit
    {
      get
      {
        if (Current == null) return null;
        return Current.mOrganizationalUnit;
      }
      internal set
      {
        if (Current != null) Current.mOrganizationalUnit = value;
      }
    }

    static AuthenticationExtension Current
    {
      get
      {
        if (OperationContext.Current == null) return null;
        return OperationContext.Current.Extensions.Find<AuthenticationExtension>();
      }
    }

    public void Attach(OperationContext owner)
    {
      OperationContext = owner;
    }

    public void Detach(OperationContext owner)
    {
      if (OperationContext == owner)
      {
        OperationContext = null;
      }
      else throw new InvalidOperationException("Invalid OperationContext.");
    }
  }
}

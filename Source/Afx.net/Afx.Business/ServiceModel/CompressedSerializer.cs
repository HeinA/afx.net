﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.IO;
using System.IO.Compression;
using System.Diagnostics;
using System.Globalization;

namespace Afx.Business.ServiceModel
{
  public class CompressedSerializer : XmlObjectSerializer
  {
    NetDataContractSerializer mSerializer = new NetDataContractSerializer();

    public override bool IsStartObject(System.Xml.XmlDictionaryReader reader)
    {
      return mSerializer.IsStartObject(reader);
    }

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2202:Do not dispose objects multiple times")]
    public override object ReadObject(System.Xml.XmlDictionaryReader reader, bool verifyObjectName)
    {
      byte[] compressedData = (byte[])mSerializer.ReadObject(reader, verifyObjectName);
      using (MemoryStream memory = new MemoryStream(compressedData))
      {
        using (DeflateStream zip = new DeflateStream(memory, CompressionMode.Decompress, true))
        {
          var formatter = new NetDataContractSerializer();
          return formatter.ReadObject(zip);
        }
      }
    }

    public override void WriteEndObject(System.Xml.XmlDictionaryWriter writer)
    {
      mSerializer.WriteEndObject(writer);
    }

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2202:Do not dispose objects multiple times")]
    public override void WriteObjectContent(System.Xml.XmlDictionaryWriter writer, object graph)
    {
      using (MemoryStream memory = new MemoryStream())
      {
        using (DeflateStream zip = new DeflateStream(memory, CompressionMode.Compress, true))
        {
          var formatter = new NetDataContractSerializer();
          formatter.WriteObject(zip, graph);
        }
        byte[] compressedData = memory.ToArray();
        mSerializer.WriteObjectContent(writer, compressedData);
      }
    }

    public override void WriteStartObject(System.Xml.XmlDictionaryWriter writer, object graph)
    {
      mSerializer.WriteStartObject(writer, graph);
    }
  }
}

﻿using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.ServiceModel
{
  public static class UnityProvider
  {
    public static IUnityContainer Instance { get; set; }
  }
}

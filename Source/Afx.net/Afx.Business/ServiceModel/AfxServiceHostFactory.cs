﻿using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.ServiceModel
{
  public class AfxServiceHostFactory : ServiceHostFactory
  {
    static bool Intitialized = false;
    static object Lock = new object();
  
    public override ServiceHostBase CreateServiceHost(string constructorString, Uri[] baseAddresses)
    {
      Init();
      ServiceHostBase sh = base.CreateServiceHost(constructorString, baseAddresses);
      return sh;
    }

    protected override ServiceHost CreateServiceHost(Type serviceType, Uri[] baseAddresses)
    {
      Init();
      return base.CreateServiceHost(serviceType, baseAddresses);
    }

    void Init()
    {
      try
      {
        lock (Lock)
        {
          if (!Intitialized)
          {
            IConfigurationSource config = ConfigurationSourceFactory.Create();
            DatabaseFactory.SetDatabaseProviderFactory(new DatabaseProviderFactory(config));
            Logger.SetLogWriter(new LogWriterFactory(config).Create());
            ExceptionPolicyFactory factory = new ExceptionPolicyFactory(config);
            ExceptionManager exManager = factory.CreateManager(); 
            ExceptionPolicy.SetExceptionManager(exManager);

            //Cache.Instance.LoadCache(Cache.LoadCache());

            Intitialized = true;
          }
        }
      }
      catch
      {
        throw;
      }
    }
  }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Configuration;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.ServiceModel
{
  public class AfxEndpointBehaviorElement : BehaviorExtensionElement
  {
    protected override object CreateBehavior()
    {
      return new AfxEndpointBehavior();
    }

    public override Type BehaviorType
    {
      get
      {
        return typeof(AfxEndpointBehavior);
      }
    }
  }
}

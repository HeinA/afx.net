﻿using Afx.Business.Service;
using Afx.Business.ServiceModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.ServiceModel
{
  [DataContract(Namespace = Namespace.Afx)]
  public class AuthenticationFault : AfxFault
  {
  }
}

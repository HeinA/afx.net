﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.ServiceModel
{
  public class CacheExtension : IExtension<OperationContext>
  {
    OperationContext mOperationContext;
    OperationContext OperationContext
    {
      get { return mOperationContext; }
      set { mOperationContext = value; }
    }

    bool mRefreshCache = false;
    public static bool RefreshCache
    {
      get
      {
        if (Current == null) return false;
        return Current.mRefreshCache;
      }
      set
      {
        if (Current != null) Current.mRefreshCache = value;
      }
    }

    static CacheExtension Current
    {
      get
      {
        if (OperationContext.Current == null) return null;
        return OperationContext.Current.Extensions.Find<CacheExtension>();
      }
    }

    public void Attach(OperationContext owner)
    {
      OperationContext = owner;
    }

    public void Detach(OperationContext owner)
    {
      if (OperationContext == owner)
      {
        OperationContext = null;
      }
      else throw new InvalidOperationException("Invalid OperationContext.");
    }
  }
}

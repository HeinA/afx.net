﻿using Afx.Business.Documents;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.ServiceModel
{
  public class DocumentReplicationExtension : IExtension<OperationContext>
  {
    OperationContext mOperationContext;
    OperationContext OperationContext
    {
      get { return mOperationContext; }
      set { mOperationContext = value; }
    }

    Stack<Document> mDocumentStack = new Stack<Document>();
    public Stack<Document> DocumentStack
    {
      get { return mDocumentStack; }
    }

    public static DocumentReplicationExtension Current
    {
      get
      {
        if (OperationContext.Current == null) return null;
        return OperationContext.Current.Extensions.Find<DocumentReplicationExtension>();
      }
    }


    public void Attach(OperationContext owner)
    {
      OperationContext = owner;
    }

    public void Detach(OperationContext owner)
    {
      if (OperationContext == owner)
      {
        OperationContext = null;
      }
      else throw new InvalidOperationException("Invalid OperationContext.");
    }
  }
}

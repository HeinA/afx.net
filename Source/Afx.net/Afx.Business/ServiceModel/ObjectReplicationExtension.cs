﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.ServiceModel
{
  public class ObjectReplicationExtension : IExtension<OperationContext>
  {
    OperationContext mOperationContext;
    OperationContext OperationContext
    {
      get { return mOperationContext; }
      set { mOperationContext = value; }
    }

    Queue<BusinessObject> mObjectQueue = new Queue<BusinessObject>();
    public Queue<BusinessObject> ObjectQueue
    {
      get { return mObjectQueue; }
    }

    public static ObjectReplicationExtension Current
    {
      get
      {
        if (OperationContext.Current == null) return null;
        return OperationContext.Current.Extensions.Find<ObjectReplicationExtension>();
      }
    }

    public void Attach(OperationContext owner)
    {
      OperationContext = owner;
    }

    public void Detach(OperationContext owner)
    {
      if (OperationContext == owner)
      {
        OperationContext = null;
      }
      else throw new InvalidOperationException("Invalid OperationContext.");
    }
  }
}

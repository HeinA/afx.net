﻿using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Business.Service;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Dispatcher;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Afx.Business.ServiceModel
{
  public class AfxDispatchMessageInspector : IDispatchMessageInspector
  {
    public const string RefreshCache = "RefreshCache";

    static Dictionary<Guid, User> mUserDictionary;
    static Dictionary<Guid, User> UserDictionary
    {
      get { return AfxDispatchMessageInspector.mUserDictionary ?? (AfxDispatchMessageInspector.mUserDictionary = new Dictionary<Guid,User>()); }
    }

    object mLock = new object();

    public object AfterReceiveRequest(ref Message request, IClientChannel channel, InstanceContext instanceContext)
    {
      try
      {
        OperationContext.Current.Extensions.Add(new CacheExtension());
        OperationContext.Current.Extensions.Add(new CachedObjectReplicationExtension());
        OperationContext.Current.Extensions.Add(new DocumentReplicationExtension());
        OperationContext.Current.Extensions.Add(new ObjectReplicationExtension());
        OperationContext.Current.Extensions.Add(new AuthenticationExtension());

        if (!ServiceAuthenticationHelper.SuppressAuthentication)
        {
          try
          {
            using (new ConnectionScope())
            using (new SqlScope("Dispatch Message Inspector"))
            {
              IInternalPersistanceManager pm = (IInternalPersistanceManager)UnityProvider.Instance.Resolve<IPersistanceManager>();
              Guid userIdentifier = request.Headers.GetHeader<Guid>(AfxClientMessageInspector.User, Namespace.Afx);
              Guid ouIdentifier = request.Headers.GetHeader<Guid>(AfxClientMessageInspector.OrganizationalUnit, Namespace.Afx);
              Guid nonce = request.Headers.GetHeader<Guid>(AfxClientMessageInspector.Nonce, Namespace.Afx);
              string passwordDigest = request.Headers.GetHeader<string>(AfxClientMessageInspector.PasswordDigest, Namespace.Afx);

//              string sql = @"
//                SELECT U.PasswordHash
//                FROM Afx.{0} U
//                WHERE NOT EXISTS (SELECT 1 FROM Afx.UserNonce UN WHERE UN.{0}=U.Id AND UN.Nonce=@un AND U.GlobalIdentifier=@u)
//                AND U.GlobalIdentifier=@u";

              string sql = @"
                SELECT U.PasswordHash
                FROM Afx.{0} U
                WHERE U.GlobalIdentifier=@u";

              string passwordDigestCalulated = null;

              using (IDbCommand cmd = pm.GetCommand())
              {
                cmd.CommandText = string.Format(sql, pm.GetIdentifier("User"));
                pm.AddParameter("@un", nonce, cmd);
                pm.AddParameter("@u", userIdentifier, cmd);
                //LogHelper.LogSql(cmd);
                DataSet ds = Execute(cmd);
                if (ds.Tables[0].Rows.Count == 1)
                {
                  DataRow dr = ds.Tables[0].Rows[0];
                  passwordDigestCalulated = CalculateDigest(nonce, (string)dr["PasswordHash"]);
                }
              }

              if (passwordDigestCalulated != passwordDigest)
              {
                if (instanceContext.GetServiceInstance() is AfxReplicationService)
                {
                  //TODO: Try old passwords...
                }
                else
                {
                  throw new AuthenticationException();
                }
              }              

              lock (mLock)
              {
                if (!UserDictionary.ContainsKey(userIdentifier))
                {
                  ObjectRepository<User> oru = pm.GetRepository<User>();
                  User u = oru.GetInstance(userIdentifier);
                  UserDictionary.Add(userIdentifier, u);
                }
                AuthenticationExtension.User = UserDictionary[userIdentifier];
                AuthenticationExtension.OrganizationalUnit = Cache.Instance.GetObject<OrganizationalUnit>(ouIdentifier);
              }
            }
          }
          catch
          {
            throw new AuthenticationException();
          }
        }

        return instanceContext.GetServiceInstance();
      }
      catch
      {
        throw;
      }
    }

    string CalculateDigest(Guid nonce, string passwordHash)
    {
      MD5 md5 = System.Security.Cryptography.MD5.Create();
      byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(string.Format("{0}{1}", nonce, passwordHash));
      byte[] hash = md5.ComputeHash(inputBytes);

      StringBuilder sb = new StringBuilder();
      for (int i = 0; i < hash.Length; i++)
      {
        sb.Append(hash[i].ToString("X2"));
      }

      return sb.ToString();
    }

    public DataSet Execute(IDbCommand cmd)
    {
      try
      {
        System.Data.DataSet ds = new System.Data.DataSet();
        ds.Locale = CultureInfo.InvariantCulture;
        using (IDataReader r = cmd.ExecuteReader())
        {
          ds.Load(r, LoadOption.OverwriteChanges, string.Empty);
          r.Close();
        }
        return ds;
      }
      catch 
      {
        throw;
      }
    }


    public void BeforeSendReply(ref Message reply, object correlationState)
    {
      try
      {
        IPersistanceManager pm = UnityProvider.Instance.Resolve<IPersistanceManager>();
      //  if (OperationContext.Current.IncomingMessageHeaders.FindHeader(AfxClientMessageInspector.Nonce, Namespace.Afx) >= 0)
      //  {
      //    Guid nonce = OperationContext.Current.IncomingMessageHeaders.GetHeader<Guid>(AfxClientMessageInspector.Nonce, Namespace.Afx);
      //    using (new ConnectionScope())
      //    using (new SqlScope("Dispatch Message Inspector"))
      //    using (IDbCommand cmd = pm.GetCommand())
      //    {
      //      cmd.CommandText = string.Format("INSERT INTO Afx.UserNonce ({0}, Nonce) VALUES (@u, @un)", pm.GetIdentifier("User"));
      //      pm.AddParameter("@u", SecurityContext.User.Id, cmd);
      //      pm.AddParameter("@un", nonce, cmd);
      //      LogHelper.LogSql(cmd);
      //      cmd.ExecuteNonQuery();
      //    }
      //  }
      }
      catch
      {
        throw;
      }

      if (reply == null) return;

      if (CacheExtension.RefreshCache && !reply.IsFault)
      {
        MessageHeader<bool> customHeaderUserID = new MessageHeader<bool>(true);
        MessageHeader h = customHeaderUserID.GetUntypedHeader(RefreshCache, Namespace.Afx);
        reply.Headers.Add(h);

        try
        {
          IPersistanceManager pm = ((ServiceBase)correlationState).PersistanceManager; 
          Cache.Instance.LoadCache(Cache.FetchCache(pm));
        }
        catch
        {
#if DEBUG
          if (Debugger.IsAttached) Debugger.Break();
#endif
        }
      }
      else
      {
        MessageHeader<bool> customHeaderUserID = new MessageHeader<bool>(false);
        MessageHeader h = customHeaderUserID.GetUntypedHeader(RefreshCache, Namespace.Afx);
        reply.Headers.Add(h);
      }
    }
  }
}

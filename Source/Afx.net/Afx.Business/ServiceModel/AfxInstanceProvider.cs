﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Dispatcher;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.ServiceModel
{
  public class AfxInstanceProvider : IInstanceProvider
  {
    public object GetInstance(System.ServiceModel.InstanceContext instanceContext, System.ServiceModel.Channels.Message message)
    {
      return UnityProvider.Instance.Resolve(instanceContext.Host.Description.ServiceType, null);
    }

    public object GetInstance(System.ServiceModel.InstanceContext instanceContext)
    {
      return UnityProvider.Instance.Resolve(instanceContext.Host.Description.ServiceType, null);
    }

    public void ReleaseInstance(System.ServiceModel.InstanceContext instanceContext, object instance)
    {
      UnityProvider.Instance.Teardown(instance);
    }
  }
}

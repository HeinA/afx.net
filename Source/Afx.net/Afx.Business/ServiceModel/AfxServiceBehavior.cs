﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.ServiceModel
{
  public class AfxServiceBehavior : IServiceBehavior
  {
    public void AddBindingParameters(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase, Collection<ServiceEndpoint> endpoints, BindingParameterCollection bindingParameters)
    {
    }

    public void ApplyDispatchBehavior(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase)
    {
      if (UnityProvider.Instance != null)
      {
        foreach (ChannelDispatcher channelDispatcher in serviceHostBase.ChannelDispatchers)
        {
          foreach (EndpointDispatcher endpointDispatcher in channelDispatcher.Endpoints)
          {
            if (endpointDispatcher.ContractName != "IMetadataExchange")
            {
              string contractName = endpointDispatcher.ContractName;
              endpointDispatcher.DispatchRuntime.InstanceProvider = new AfxInstanceProvider();
            }
          }
        }
      }
    }


    public void Validate(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase)
    {
    }
  }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Configuration;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.ServiceModel
{
  public class AfxServiceBehaviorElement : BehaviorExtensionElement
  {
    public override Type BehaviorType
    {
      get { return typeof(AfxServiceBehavior); }
    }

    protected override object CreateBehavior()
    {
      return new AfxServiceBehavior();
    }
  }
}

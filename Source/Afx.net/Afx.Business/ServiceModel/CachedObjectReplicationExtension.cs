﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.ServiceModel
{
  public class CachedObjectReplicationExtension : IExtension<OperationContext>
  {
    OperationContext mOperationContext;
    OperationContext OperationContext
    {
      get { return mOperationContext; }
      set { mOperationContext = value; }
    }

    Collection<BusinessObject> mObjects = new Collection<BusinessObject>();
    public static IEnumerable<BusinessObject> Objects
    {
      get
      {
        if (Current == null) return null;
        return Current.mObjects;
      }
    }

    void ClearInternal()
    {
      mObjects.Clear();
    }

    public static void Clear()
    {
      if (Current == null) return;
      Current.ClearInternal();
    }

    public static int ObjectCount
    {
      get
      {
        if (Current == null) return 0;
        return Current.mObjects.Count;
      }
    }

    public static void AddObject(BusinessObject bo)
    {
      if (Current == null) return;
      if (bo == null) throw new ArgumentNullException("bo");

      BusinessObject root = GetRoot(bo);
      if (!Current.mObjects.Contains(root))
      {
        IRevisable r = root as IRevisable;
        if (r != null)
        {
          object ov = null;
          if (!root.IsNew && !root.GetOriginalValue("Revision", ref ov))
          {
            if (Debugger.IsAttached)
            {
              Debugger.Break();
            }
          }
        }
        Current.mObjects.Add(root);
      }
    }

    static BusinessObject GetRoot(BusinessObject bo)
    {
      IBusinessObject ibo = (IBusinessObject)bo;
      if (ibo.Owner == null) return bo;
      return GetRoot((BusinessObject)ibo.Owner);
    }

    public static CachedObjectReplicationExtension Current
    {
      get
      {
        if (OperationContext.Current == null) return null;
        return OperationContext.Current.Extensions.Find<CachedObjectReplicationExtension>();
      }
    }

    public void Attach(OperationContext owner)
    {
      OperationContext = owner;
    }

    public void Detach(OperationContext owner)
    {
      if (OperationContext == owner)
      {
        OperationContext = null;
      }
      else throw new InvalidOperationException("Invalid OperationContext.");
    }
  }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.ServiceModel
{
  class ServiceAuthenticationHelper
  {
    static object mLock = new object();
    static Dictionary<string, bool> mSuppressAuthentication = new Dictionary<string, bool>();

    public static bool SuppressAuthentication
    {
      get
      {
        lock (mLock)
        {
          if (!mSuppressAuthentication.ContainsKey(OperationContext.Current.IncomingMessageHeaders.Action))
          {
            var serviceType = OperationContext.Current.InstanceContext.Host.Description.ServiceType;
            var operationName = OperationContext.Current.IncomingMessageHeaders.Action;
            var methodName = operationName.Substring(operationName.LastIndexOf("/") + 1);

            var method = serviceType.GetMethods().Where(m => m.Name == methodName && m.IsPublic).SingleOrDefault();
            var attribute = method.GetCustomAttributes(true).FirstOrDefault(a => a is SuppressAuthenticationAttribute);
            if (attribute != null) mSuppressAuthentication.Add(OperationContext.Current.IncomingMessageHeaders.Action, true);
            else mSuppressAuthentication.Add(OperationContext.Current.IncomingMessageHeaders.Action, false);
          }
        }
        return mSuppressAuthentication[OperationContext.Current.IncomingMessageHeaders.Action];
      }
    }
  }
}

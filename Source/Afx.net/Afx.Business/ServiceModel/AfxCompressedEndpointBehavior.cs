﻿using Afx.Business.Attributes;
using Afx.Business.Collections;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.ServiceModel
{
  public class AfxCompressedEndpointBehavior : IEndpointBehavior
  {
    public void AddBindingParameters(ServiceEndpoint endpoint, BindingParameterCollection bindingParameters)
    {
    }

    public void ApplyClientBehavior(ServiceEndpoint endpoint, ClientRuntime clientRuntime)
    {
      if (clientRuntime.MessageInspectors.FirstOrDefault(o => o.GetType() == typeof(AfxClientMessageInspector)) == null) 
        clientRuntime.MessageInspectors.Add(new AfxClientMessageInspector());
    }

    public void ApplyDispatchBehavior(ServiceEndpoint endpoint, EndpointDispatcher endpointDispatcher)
    {
      if (endpointDispatcher.DispatchRuntime.MessageInspectors.FirstOrDefault(o => o.GetType() == typeof(AfxDispatchMessageInspector)) == null)
        endpointDispatcher.DispatchRuntime.MessageInspectors.Add(new AfxDispatchMessageInspector());
    }

    public void Validate(ServiceEndpoint endpoint)
    {
      #region Apply Known Types & Fault Contracts

      foreach (OperationDescription opDesc in endpoint.Contract.Operations)
      {
        foreach (Type t in KnownTypesProvider.Faults)
        {
          FaultDescription faultDescription = new FaultDescription(string.Format(CultureInfo.CurrentCulture, "{0}/{1}/{2}_{3}", opDesc.DeclaringContract.Namespace, opDesc.DeclaringContract.Name, opDesc.Name, t.Name));
          faultDescription.Namespace = KnownTypesProvider.GetFaultNamespace(t);
          faultDescription.DetailType = t;
          faultDescription.Name = t.Name;
          if (opDesc.Faults.FirstOrDefault(fd => fd.Name == t.Name) == null) opDesc.Faults.Add(faultDescription);
        }
      }

      #endregion

      foreach (OperationDescription desc in endpoint.Contract.Operations)
      {
        DataContractSerializerOperationBehavior dcsOperationBehavior = desc.Behaviors.Find<DataContractSerializerOperationBehavior>();
        if (dcsOperationBehavior != null && dcsOperationBehavior.GetType() != typeof(AfxCompressedSerializerOperationBehavior))
        {
          int idx = desc.Behaviors.IndexOf(dcsOperationBehavior);
          desc.Behaviors.Remove(dcsOperationBehavior);
          desc.Behaviors.Insert(idx, new AfxCompressedSerializerOperationBehavior(desc));
        }
      }

      endpoint.Binding.Namespace = Namespace.Afx;
    }
  }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.ServiceModel
{
  [Serializable]
  public class AuthenticationException : Exception
  {
    protected AuthenticationException(SerializationInfo info, StreamingContext context)
      :base(info, context)
    {
    }

    public AuthenticationException()
      : base()
    {
    }

    public AuthenticationException(string message)
      : base(message)
    {
    }

    public AuthenticationException(string message, Exception innerException)
      : base(message, innerException)
    {
    }  
  }
}

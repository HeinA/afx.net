﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Configuration;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.ServiceModel
{
  public class AfxNetTcpEndpointBehaviorElement : BehaviorExtensionElement
  {
    protected override object CreateBehavior()
    {
      return new AfxNetTcpEndpointBehavior();
    }

    public override Type BehaviorType
    {
      get
      {
        return typeof(AfxNetTcpEndpointBehavior);
      }
    }
  }
}

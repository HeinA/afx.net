﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.ServiceModel
{
  public class AfxObjectSerializer : XmlObjectSerializer
  {
    DataContractSerializer mDcs;
    DataContractSerializer Dcs
    {
      get { return mDcs; }
    }

    internal AfxObjectSerializer(DataContractSerializer dcs)
    {
      mDcs = dcs;
    }

    public override bool IsStartObject(System.Xml.XmlDictionaryReader reader)
    {
      return mDcs.IsStartObject(reader);
    }

    public override object ReadObject(System.Xml.XmlDictionaryReader reader, bool verifyObjectName)
    {
      return mDcs.ReadObject(reader, verifyObjectName);
    }

    public override void WriteEndObject(System.Xml.XmlDictionaryWriter writer)
    {
      mDcs.WriteEndObject(writer);
    }

    public override void WriteObjectContent(System.Xml.XmlDictionaryWriter writer, object graph)
    {
      try
      {
        int i = 1;
        nss = ExtensibilityManager.CompositionContainer.GetExportedValues<INamespace>();
        foreach (INamespace ns in nss)
        {
          string s = ns.GetUri();
          writer.WriteAttributeString("xmlns", string.Format("c{0}", i++), null, ns.GetUri());
        }
        writer.WriteAttributeString("xmlns", "a1", null, "http://www.w3.org/2001/XMLSchema");
        writer.WriteAttributeString("xmlns", "z", null, "http://schemas.microsoft.com/2003/10/Serialization/");

        mDcs.WriteObjectContent(writer, graph);
      }
      catch (Exception ex)
      {
        throw;
        //writer.WriteAttributeString("xmlns", "a1", null, "http://www.w3.org/2001/XMLSchema");
      }
    }

    IEnumerable<INamespace> nss;

    public override void WriteStartObject(System.Xml.XmlDictionaryWriter writer, object graph)
    {
      mDcs.WriteStartObject(writer, graph);

      int i = 1;
      nss = ExtensibilityManager.CompositionContainer.GetExportedValues<INamespace>();
      foreach (INamespace ns in nss)
      {
        writer.WriteAttributeString("xmlns", string.Format("c{0}", i++), null, ns.GetUri());
      }
      writer.WriteAttributeString("xmlns", "a1", null, "http://www.w3.org/2001/XMLSchema");
      writer.WriteAttributeString("xmlns", "z", null, "http://schemas.microsoft.com/2003/10/Serialization/");
    }
  }
}

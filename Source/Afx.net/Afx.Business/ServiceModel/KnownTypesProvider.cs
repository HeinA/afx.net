﻿using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Service;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.ServiceModel
{
  static class KnownTypesProvider
  {
    static object mLock;
    static object Lock
    {
      get
      {
        if (mLock == null) mLock = new object();
        return mLock;
      }
    }

    static Collection<Type> mFaults;
    static Dictionary<Type, string> mFaultNamespaces;
    static Collection<Type> mKnownTypes;

    static KnownTypesProvider()
    {
      try
      {
        lock (Lock)
        {
          ////DebugHelper.LogMessage("KnowTypesProvider starting...");
          Utilities.PreLoadDeployedAssemblies();

          mFaults = new Collection<Type>();
          mFaultNamespaces = new Dictionary<Type, string>();
          mKnownTypes = new Collection<Type>();
          Collection<Type> genericTypes = new Collection<Type>();

          foreach (Assembly a in AppDomain.CurrentDomain.GetAssemblies())
          {
            try
            {
              foreach (Type t in a.GetTypes().Where(t => t.IsSubclassOf(typeof(AfxFault))))
              {
                mFaults.Add(t);
                DataContractAttribute dca = t.GetCustomAttributes<DataContractAttribute>().FirstOrDefault();
                if (dca == null) throw new InvalidOperationException("Fault must have a DataContract attribute.");
                mFaultNamespaces.Add(t, dca.Namespace);
              }

              foreach (Type t in a.GetTypes().Where(t => !t.IsGenericTypeDefinition && typeof(BusinessObject).IsAssignableFrom(t) && t.IsDefined(typeof(DataContractAttribute))))
              {
                mKnownTypes.Add(t);

                Type ct = typeof(BasicCollection<>);
                Type ct1 = ct.MakeGenericType(new Type[] { t });
                mKnownTypes.Add(ct1);

                ct = typeof(BusinessObjectCollection<>);
                ct1 = ct.MakeGenericType(new Type[] { t });
                mKnownTypes.Add(ct1);

                ct = typeof(DataFilter<>);
                ct1 = ct.MakeGenericType(new Type[] { t });
                mKnownTypes.Add(ct1);
              }
            }
            catch
            {
              throw;
            }
          }
          mKnownTypes.Add(typeof(PropertyReference));
          mKnownTypes.Add(typeof(BasicCollection<PropertyReference>));
          mKnownTypes.Add(typeof(DataFilter));
          mKnownTypes.Add(typeof(SearchResult));
          mKnownTypes.Add(typeof(SearchResults));
          mKnownTypes.Add(typeof(SearchDatagraph));
          mKnownTypes.Add(typeof(SearchDatagraph.sdgResult));
          mKnownTypes.Add(typeof(SearchDatagraph.sdgWhere));

          foreach (IKnownTypesProvider kt in ExtensibilityManager.CompositionContainer.GetExportedValues<IKnownTypesProvider>())
          {
            foreach (Type t in kt.GetKnowTypes())
            {
              mKnownTypes.Add(t);
            }
          }
        }

        ////DebugHelper.LogMessage("KnowTypesProvider starting done.");
      }
      //catch (ReflectionTypeLoadException ex)
      //{
      //  using (StreamWriter sw = new StreamWriter("ReflectionErrors.txt", true))
      //  {
      //    sw.WriteLine(ex.ToString());
      //  }
      //}
      catch
      {
#if DEBUG
        if (Debugger.IsAttached) Debugger.Break();
#endif
        throw;
      }
    }

    public static Collection<Type> KnownTypes
    {
      get { return mKnownTypes; }
    }

    public static Collection<Type> Faults
    {
      get { return mFaults; }
    }

    public static string GetFaultNamespace(Type faultType)
    {
      return mFaultNamespaces[faultType];
    }
  }
}

﻿using Dataforge.Business.Attributes;
using Dataforge.Business.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Business
{
  [Table(FlattenToSubClass = true)]
  [DataContract(IsReference = true, Namespace = Namespaces.Dataforge)]
  public class IdentifiableObject : BusinessObject, IIdentifiableObject
  {
    #region DataMember Guid GlobalIdentifier

    public const string PN_GLOBALIDENTIFIER = "GlobalIdentifier";
    [DataMember(Name = PN_GLOBALIDENTIFIER, EmitDefaultValue = false)]
    Guid _globalIdentifier = Guid.NewGuid();
    public Guid GlobalIdentifier
    {
      get { return _globalIdentifier; }
      internal set { SetValue<Guid>(ref _globalIdentifier, value, PN_GLOBALIDENTIFIER); }
    }

    #endregion

    #region DataMember string StringIdentifier

    public const string PN_STRINGIDENTIFIER = "StringIdentifier";
    [DataMember(Name = PN_STRINGIDENTIFIER, EmitDefaultValue = false)]
    string _stringIdentifier;
    public string StringIdentifier
    {
      get { return !string.IsNullOrWhiteSpace(_stringIdentifier) ? _stringIdentifier : GlobalIdentifier.ToString("B").ToUpperInvariant(); }
      set { SetValue<string>(ref _stringIdentifier, value, PN_STRINGIDENTIFIER); }
    }

    #endregion

    #region Equality

    public override bool Equals(object obj)
    {
      if (obj != null && obj.GetType() == this.GetType())
      {
        IIdentifiableObject io = obj as IIdentifiableObject;
        return io.StringIdentifier == this.StringIdentifier;
      }
      return base.Equals(obj);
    }

    public override int GetHashCode()
    {
      return StringIdentifier.GetHashCode();
    }

    #endregion
  }
}

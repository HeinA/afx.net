﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.Configuration
{
  public class ExtensibilityConfiguration : ConfigurationSection
  {
    public const string SectionName = "extensibilityConfiguration";

    public static ExtensibilityConfiguration Default
    {
      get { return ConfigurationManager.GetSection(ExtensibilityConfiguration.SectionName) as ExtensibilityConfiguration; }
    }

    [ConfigurationProperty("allowedTypes", IsDefaultCollection = true)]
    [ConfigurationCollection(typeof(AllowedTypeCollection), AddItemName = "add")]
    public AllowedTypeCollection AllowedTypes
    {
      get { return (AllowedTypeCollection)base["allowedTypes"]; }
    }
  }
}

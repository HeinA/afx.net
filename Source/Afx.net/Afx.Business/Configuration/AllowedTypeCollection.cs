﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.Configuration
{
  public class AllowedTypeCollection : ConfigurationElementCollection
  {
    protected override ConfigurationElement CreateNewElement()
    {
      return new AllowedType();
    }

    protected override object GetElementKey(ConfigurationElement element)
    {
      return ((AllowedType)element).TypeName;
    }
  }
}

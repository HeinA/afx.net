﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.Configuration
{
  public class AllowedType : ConfigurationElement
  {
    [ConfigurationProperty("typeName", IsRequired = true)]
    public string TypeName
    {
      get { return (string)this["typeName"]; }
      set { this["typeName"] = value; }
    }
  }
}

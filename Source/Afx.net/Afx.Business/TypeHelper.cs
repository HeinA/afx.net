﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business
{
  public static class TypeHelper
  {
    public static string GetTypeName(Type type)
    {
      return string.Format("{0}, {1}", type.FullName, type.Assembly.GetName().Name);
    }

    public static Type GetGenericSubClass(Type generic, Type toCheck)
    {
      while (toCheck != null && toCheck != typeof(object))
      {
        var cur = toCheck.IsGenericType ? toCheck.GetGenericTypeDefinition() : toCheck;
        if (generic == cur)
        {
          return toCheck;
        }
        toCheck = toCheck.BaseType;
      }
      return null;
    }
  }
}

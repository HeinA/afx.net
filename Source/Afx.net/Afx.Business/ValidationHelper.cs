﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business
{
  public class ValidationHelper
  {
    static ValidationHelper()
    {
      foreach (ICollectionValidator v in ExtensibilityManager.CompositionContainer.GetExportedValues<ICollectionValidator>())
      {
        RegisterValidator(v);
      }
    }

    static object mLock = new object();
    static Dictionary<Type, ICollectionValidator> mValidationDictionary = new Dictionary<Type, ICollectionValidator>();
    public static void RegisterValidator(ICollectionValidator v)
    {
      lock (mLock)
      {
        mValidationDictionary.Add(v.ObjectType, v);
      }
    }

    public static string ValidateCollection<T>(ICollection<T> col)
      where T : BusinessObject
    {
      if (!mValidationDictionary.ContainsKey(typeof(T))) return null;
      Collection<string> errors = new Collection<string>();
      ValidateCollection<T>(col, errors);
      if (errors.Count == 0) return null;
      return string.Join("\n", errors);
    }

    public static void ValidateCollection<T>(ICollection<T> col, Collection<string> errors)
      where T : BusinessObject
    {
      if (!mValidationDictionary.ContainsKey(typeof(T))) return ;
      ICollectionValidator v = mValidationDictionary[typeof(T)];
      v.GetErrors((IList)col, errors);
    }
  }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business
{
  /// <summary>
  /// Will suppress PropertyChanged & CompositionChanged events, aswell as the Dirty flag being set.
  /// </summary>
  public class EventStateSuppressor : IDisposable
  {
    Collection<BusinessObject> mSuppressedObjects = new Collection<BusinessObject>();
    Collection<BusinessObject> SuppressedObjects
    {
      get { return mSuppressedObjects; }
    }

    /// <summary>
    /// Will suppress PropertyChanged &amp; CompositionChanged events of the specified BusinessObject and all it's children, aswell as their Dirty flag being set.
    /// </summary>
    /// <param name="bo">The targe BusinessObject</param>
    public EventStateSuppressor(BusinessObject bo)
    {
      if (bo == null) throw new ArgumentNullException("bo");

      if (!bo.SuppressEventState)
      {
        SuppressedObjects.Add(bo);
        bo.SuppressEventState = true;
      }
    }

    /// <summary>
    /// Will suppress PropertyChanged &amp; CompositionChanged events of the specified BusinessObject list and all it's children, aswell as their Dirty flag being set.
    /// </summary>
    /// <param name="bo">The targe BusinessObject</param>
    public EventStateSuppressor(IList list)
    {
      if (list == null) throw new ArgumentNullException("list");

      foreach (object o in list)
      {
        BusinessObject bo = o as BusinessObject;
        if (bo != null)
        {
          if (!bo.SuppressEventState)
          {
            SuppressedObjects.Add(bo);
            bo.SuppressEventState = true;
          }
        }
      }      
    }

    public void Dispose()
    {
      Dispose(true);
      GC.SuppressFinalize(this);
    }

    bool mDisposed;
    protected virtual void Dispose(bool disposing)
    {
      if (mDisposed) return;
      if (disposing)
      {
        foreach (BusinessObject bo in SuppressedObjects)
        {
          bo.SuppressEventState = false;
        }
      }
      mDisposed = true;
    }
  }
}

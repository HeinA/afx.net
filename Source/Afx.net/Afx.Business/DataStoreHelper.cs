﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business
{
  class DataStoreHelper
  {
    public static string DataStorePath
    {
      get
      {
        return String.Format(@"{0}\Afx.net\DataStore.sdf", Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData));
      }
    }

    public static string ConnectionString
    {
      get
      {
        return String.Format(@"Data Source={0}", DataStorePath);
      }
    }

    public static void CreateDataStore()
    {
      try
      {
        Stream s = Assembly.GetExecutingAssembly().GetManifestResourceStream("Afx.Business.DataStore.sdf");
        string path = String.Format(@"{0}\Afx.net", Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData));
        DirectoryInfo di = new DirectoryInfo(path);
        if (!di.Exists) di.Create();
        path = String.Format(@"{0}\DataStore.sdf", path);
        FileInfo fi = new FileInfo(path);
        if (!fi.Exists)
        {
          using (FileStream resourceFile = new FileStream(path, FileMode.Create))
          {
            byte[] b = new byte[s.Length + 1];
            s.Read(b, 0, Convert.ToInt32(s.Length));
            resourceFile.Write(b, 0, Convert.ToInt32(b.Length - 1));
          }
        }
      }
      catch (Exception ex)
      {
        if (ExceptionHelper.HandleException(ex)) throw;
      }
    }
  }
}

﻿using Afx.Business;
using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Security;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using Afx.Business.Service;
using System.ServiceModel;

namespace Afx.Business
{
  [PersistantObject(Schema = "Afx")]
  [Replicate]
  public abstract partial class Setting : BusinessObject, IRevisable
  {
    #region Constructors

    internal protected Setting()
    {
    }

    static Setting()
    {
      Refresh();
    }

    #endregion

    #region int Revision

    public const string RevisionProperty = "Revision";
    [DataMember(Name = RevisionProperty, EmitDefaultValue = false)]
    int mRevision = 1;
    [PersistantProperty]
    public int Revision
    {
      get { return mRevision; }
      set { SetProperty<int>(ref mRevision, value); }
    }

    #endregion

    #region string Name

    public const string NameProperty = "Name";
    [PersistantProperty]
    public abstract string Name
    {
      get;
    }

    #endregion


    #region Static

    #region void Refresh(...)

    public static void Refresh(bool fromMaster = false)
    {
      if (OperationContext.Current != null)
      {
        mSettings = SettingsHelper.LoadSettings(ExtensibilityManager.PersistanceManager, Environment.MachineName);
        ResetGroups();
        return;
      }

      //if (SecurityContext.User == null)
      //{
      //  return;
      //}

      string server = SecurityContext.ServerName;
      if (fromMaster) server = SecurityContext.MasterServer.Name;
      if (server == null) return;
      using (var svc = ProxyFactory.GetService<IAfxService>(server))
      {
        mSettings = svc.LoadSettings(Environment.MachineName);
      }
      ResetGroups();
    }

    public static void Refresh(BasicCollection<Setting> col)
    {
      mSettings = col;
      ResetGroups();
    }

    #endregion

    #region void ResetGroups()

    static void ResetGroups()
    {
      mGroups = new BasicCollection<SettingGroupView>();
      foreach (Setting s in mSettings)
      {
        SettingGroupView sgv = mGroups.Where(gv => gv.GlobalIdentifier.Equals(s.SettingGroup.GlobalIdentifier)).FirstOrDefault();
        if (sgv == null)
        {
          sgv = new SettingGroupView(s.SettingGroup.GlobalIdentifier, s.SettingGroup.Text);
          mGroups.Add(sgv);
        }
        sgv.mSettings.Add(s);
      }
    }

    #endregion

    #region IEnumerable<SettingGroupView> Groups

    static BasicCollection<SettingGroupView> mGroups;
    public static IEnumerable<SettingGroupView> Groups
    {
      get { return mGroups; }
    }

    #endregion

    #region IEnumerable<Setting> Settings

    static BasicCollection<Setting> mSettings;
    public static IEnumerable<Setting> Settings
    {
      get { return mSettings; }
    }

    #endregion

    #region T GetSetting<T>()

    public static T GetSetting<T>()
      where T : Setting, new()
    {
      return mSettings.OfType<T>().FirstOrDefault();
    }

    #endregion

    #endregion

    #region Private

    #region string SettingGroupIdentifier

    protected abstract string SettingGroupIdentifier
    {
      get;
    }

    #endregion

    #region SettingGroup SettingGroup

    SettingGroup mSettingGroup;
    private SettingGroup SettingGroup
    {
      get { return mSettingGroup ?? (mSettingGroup = Cache.Instance.GetObject<SettingGroup>(SettingGroupIdentifier)); }
    }

    #endregion

    #endregion
  }
}

﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;

namespace Afx.Business.Collections
{
  [CollectionDataContract(Namespace = Namespace.Afx)]
  public class BusinessObjectCollection<T> : BasicCollection<T>, IInternalOwnedCollection
    where T : BusinessObject
  {
    public BusinessObjectCollection()
      : base()
    {
    }

    public BusinessObjectCollection(BusinessObject owner, [CallerMemberName] string propertyName = null)
      : base()
    {
      Owner = owner;
      PropertyName = propertyName;
    }

    public int CountUndeleted
    {
      get
      {
        return this.Count(bo => !bo.IsDeleted);
      }
    }

    IInternalBusinessObject mOwner;
    internal IInternalBusinessObject Owner
    {
      get { return mOwner; }
      set { mOwner = value; }
    }

    string mPropertyName;
    string PropertyName
    {
      get { return mPropertyName; }
      set { mPropertyName = value; }
    }

    internal bool SuppressEvents
    {
      get
      {
        if (Owner == null) return false;
        return Owner.SuppressEventState;
      }
    }

    protected override void InsertItem(int index, T item)
    {
      if (OnBeforeItemInserted(item))
      {
        base.InsertItem(index, item);
      }
    }

    protected virtual bool OnBeforeItemInserted(T item)
    {
      if (Owner != null) Owner.AddChild(item);
      WeakEventManager<BusinessObject, CompositionChangedEventArgs>.AddHandler(item, "CompositionChanged", Item_CompositionChanged);

      return true;
      //item.CompositionChanged += Item_CompositionChanged;
    }

    protected virtual void OnBeforeItemRemoved(T item)
    {
      if (Owner != null) Owner.RemoveChild(item);
      //item.CompositionChanged -= Item_CompositionChanged;
    }

    protected override void ClearItems()
    {
      foreach (var bo in this.Items)
      {
        OnBeforeItemRemoved(bo);
      }
      base.ClearItems();
    }

    protected override void RemoveItem(int index)
    {
      OnBeforeItemRemoved(Items[index]);
      base.RemoveItem(index);
    }

    protected override void SetItem(int index, T item)
    {
      OnBeforeItemRemoved(Items[index]);
      OnBeforeItemInserted(item);
      base.SetItem(index, item);
    }

    /// <summary>
    /// Called by BusinessObject apon deserialization
    /// </summary>
    /// <param name="owner"></param>
    void IInternalOwnedCollection.SetOwner(BusinessObject owner)
    {
      this.Owner = owner;
      foreach (var bo in this.Items)
      {
        Owner.AddChild(bo);
      }

      OnSetOwner(owner);
    }

    protected virtual void OnSetOwner(BusinessObject owner)
    {
    }

    string IInternalOwnedCollection.PropertyName
    {
      get { return PropertyName; }
      set { PropertyName = value; }
    }

    protected void Item_CompositionChanged(object sender, CompositionChangedEventArgs e)
    {
      if (!SuppressEvents)
      {
        if (Owner != null) Owner.OnCompositionChanged(e.CompositionChangedType, e.PropertyName, e.OriginalSource, e.Item);
        //OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Replace, sender, sender));
      }
    }

    protected override void OnCollectionChanged(NotifyCollectionChangedEventArgs e)
    {
      if (!SuppressEvents && Owner != null)
      {
        if (e.NewItems != null)
        {
          foreach (BusinessObject bo in e.NewItems)
          {
            Owner.OnCompositionChanged(CompositionChangedType.ChildAdded, PropertyName, this, bo);
          }
        }

        if (e.OldItems != null)
        {
          foreach (BusinessObject bo in e.OldItems)
          {
            Owner.OnCompositionChanged(CompositionChangedType.ChildRemoved, PropertyName, this, bo);
          }
        }
      }

      base.OnCollectionChanged(e);
    }
  }
}

﻿using Dataforge.Business.Constants;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Business.Collections
{
  [CollectionDataContract(Namespace = Namespaces.Dataforge)]
  public class AssociativeObjectCollection<TAssociative, TReference> : ObservableCollection<TReference>, IInternalAssociativeObjectCollection
    where TAssociative : AssociativeObject, new()
    where TReference : BusinessObject
  {
    private AssociativeObjectCollection()
      : base()
    {
    }

    public AssociativeObjectCollection(BusinessObject owner)
      : base()
    {
      Owner = owner;
    }

    BusinessObject _owner;
    BusinessObject Owner
    {
      get { return _owner; }
      set { _owner = value; }
    }

    internal bool RaiseEvents
    {
      get
      {
        if (Owner == null) return true;
        return !Owner.SuppressEvents;
      }
    }

    protected override void InsertItem(int index, TReference item)
    {
      if (Owner != null)
      {
        TAssociative a = new TAssociative();
        a.Reference = item;
        Owner.SetAssociativeObject<TAssociative>(a);
      }
      base.InsertItem(index, item);
    }

    protected override void ClearItems()
    {
      foreach (var i in this.Items)
      {
        if (Owner != null) Owner.RemoveAssociativeObject<TAssociative>(i.Id);
      }
      base.ClearItems();
    }

    protected override void RemoveItem(int index)
    {
      if (Owner != null) Owner.RemoveAssociativeObject<TAssociative>(Items[index].Id);
      base.RemoveItem(index);
    }

    protected override void SetItem(int index, TReference item)
    {
      if (Owner != null)
      {
        Owner.RemoveAssociativeObject<TAssociative>(Items[index].Id);
        TAssociative a = new TAssociative();
        a.Reference = item;
        Owner.SetAssociativeObject<TAssociative>(a);
      }
      base.SetItem(index, item);
    }

    protected override void OnCollectionChanged(NotifyCollectionChangedEventArgs e)
    {
      if (RaiseEvents) base.OnCollectionChanged(e);
    }

    void IInternalOwnedCollection.SetOwner(BusinessObject owner)
    {
      Owner = owner;
    }

    void IInternalAssociativeObjectCollection.Repopulate()
    {
      using (new BusinessObjectEventSuppressor(this))
      {
        foreach (TAssociative ass in Owner.GetAssociativeObjects(typeof(TAssociative)))
        {
          Owner.SetAssociativeObject<TAssociative>(ass);
          base.InsertItem(this.Count, (TReference)ass.Reference);
        }
      }
    }
  }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.Collections
{
  public interface IAssociativeObjectCollection : ICollection
  {
    Type AssociativeType { get; }
    Type ReferenceType { get; }
  }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.Collections
{
  interface ICacheCollection
  {
    void Refresh();
  }
}

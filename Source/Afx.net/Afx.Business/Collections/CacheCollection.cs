﻿using Afx.Business.Data;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.Collections
{
  [CollectionDataContract(Namespace = Namespace.Afx)]
  public sealed class CacheCollection<T> : BasicCollection<T>, ICacheCollection
    where T : BusinessObject
  {
    #region Constructors

    internal CacheCollection(bool includeNull)
    {
      IncludeNull = includeNull;
    }

    internal CacheCollection(bool includeNull, Func<T, bool> predicate)
      : this(includeNull)
    {
      Predicate = predicate;
    }

    internal CacheCollection(bool includeNull, Func<T, object> sort, bool ascending)
      : this(includeNull)
    {
      Sort = sort;
      Ascending = ascending;
    }

    internal CacheCollection(bool includeNull, Func<T, bool> predicate, Func<T, object> sort, bool ascending)
      : this(includeNull)
    {
      Predicate = predicate;
      Sort = sort;
      Ascending = ascending;
    }

    #endregion

    bool mIncludeNull;
    bool IncludeNull
    {
      get { return mIncludeNull; }
      set { mIncludeNull = value; }
    }

    #region Properties

    Func<T, bool> mPredicate;
    Func<T, bool> Predicate
    {
      get { return mPredicate; }
      set { mPredicate = value; }
    }

    Func<T, object> mSort;
    Func<T, object> Sort
    {
      get { return mSort; }
      set { mSort = value; }
    }

    bool mAscending;
    bool Ascending
    {
      get { return mAscending; }
      set { mAscending = value; }
    }

    #endregion

    T GetNullObject()
    {
      ConstructorInfo ci = typeof(T).GetConstructor(new Type[] { typeof(bool) });
      if (ci == null) throw new InvalidOperationException("BusinessObject does not implement the nullable constructor.");
      return (T)ci.Invoke(new object[] { true });
    }

    #region ICacheCollection.Refresh()

    void ICacheCollection.Refresh()
    {
      try
      {
        mSuppressCollectionChanged = true;
        this.Clear();
        if (IncludeNull) this.Add(GetNullObject());

        if (Predicate == null && Sort == null)
        {
          foreach (T o in Cache.Instance.GetObjectsInternal<T>()) this.Add(o);
          return;
        }

        if (Predicate != null && Sort == null)
        {
          foreach (T o in Cache.Instance.GetObjectsInternal<T>(Predicate)) this.Add(o);
          return;
        }

        if (Predicate == null && Sort != null)
        {
          foreach (T o in Cache.Instance.GetObjectsInternal<T>(Sort, Ascending)) this.Add(o);
          return;
        }

        if (Predicate != null && Sort != null)
        {
          foreach (T o in Cache.Instance.GetObjectsInternal<T>(Predicate, Sort, Ascending)) this.Add(o);
          return;
        }
      }
      finally
      {
        mSuppressCollectionChanged = false;
        FireReset();
      }
    }

    bool mSuppressCollectionChanged = false;
    protected override void OnCollectionChanged(NotifyCollectionChangedEventArgs e)
    {
      if (!mSuppressCollectionChanged) base.OnCollectionChanged(e);
    }

    #endregion
  }
}

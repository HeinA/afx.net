﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.Collections
{
  interface IInternalOwnedCollection
  {
    void SetOwner(BusinessObject owner);
    string PropertyName { get; set; }
    int CountUndeleted { get; }
  }
}

﻿using Afx.Business.Data;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Afx.Business.Collections
{
  /// <summary>
  /// Do not add a [DataMember] attribute to Associative Object Collections!
  /// </summary>
  [CollectionDataContract(Namespace = Namespace.Afx)]
  public class AssociativeObjectCollection<TAssociative, TReference> : BusinessObjectCollection<TReference>, IAssociativeObjectCollection
    where TAssociative : IAssociativeObject, new()
    where TReference : BusinessObject
  {
    public AssociativeObjectCollection()
      : base()
    {
    }

    public AssociativeObjectCollection(BusinessObject owner, [CallerMemberName] string propertyName = null)
      : base(owner, propertyName)
    {
      try
      {
        using (new EventStateSuppressor(this))
        {
          Collection<TAssociative> col = new Collection<TAssociative>(Owner.GetAssociativeObjects(typeof(TAssociative)).OfType<TAssociative>().ToList());

          foreach (TAssociative a in col)// Owner.GetAssociativeObjects(typeof(TAssociative)))
          {
            BusinessObject bo = Owner.SetAssociativeObject<TAssociative>(a);
            if (!bo.IsDeleted)
            {
              WeakEventManager<BusinessObject, CompositionChangedEventArgs>.AddHandler(bo, "CompositionChanged", Item_CompositionChanged);
              TReference item = (TReference)a.Reference ?? Cache.Instance.GetObject<TReference>(a.ReferenceId);

              //TODO: Fix non included namspaces on query
              if (item != null)
              {
                if (!this.Contains(item)) base.InsertItem(this.Count, item);
                if (a.ReferenceIsOwned) 
                  item.mOwner = (BusinessObject)a.Owner;
              }
            }
          }
        }
      }
      catch
      {
        throw;
      }
    }

    #region IAssociativeObjectCollection

    Type IAssociativeObjectCollection.AssociativeType
    {
      get { return typeof(TAssociative); }
    }

    Type IAssociativeObjectCollection.ReferenceType
    {
      get { return typeof(TReference); }
    }

    #endregion

    protected override bool OnBeforeItemInserted(TReference item)
    {
      if (item == null)
        throw new ReferenceObjectNullException(typeof(TReference).Name);

      if (Owner != null)
      {
        if (this.Contains(item))
        {
          return false;
        }

        item.SuppressEventState = Owner.SuppressEventState;

        TAssociative a = new TAssociative();
        BusinessObject abo  = a as BusinessObject;
        abo.SuppressEventState = Owner.SuppressEventState;

        a.Reference = item;
        BusinessObject bo = a as BusinessObject;
        bo = Owner.SetAssociativeObject<TAssociative>(a);
        bo.IsDeleted = false;
        WeakEventManager<BusinessObject, CompositionChangedEventArgs>.AddHandler(bo, "CompositionChanged", Item_CompositionChanged);
      }

      return base.OnBeforeItemInserted(item);
    }

    protected override void OnBeforeItemRemoved(TReference item)
    {
      if (item == null) throw new ReferenceObjectNullException(typeof(TReference).Name);

      if (Owner != null)
      {
        TAssociative a = (TAssociative)Owner.GetAssociativeObject<TAssociative>(item.GlobalIdentifier);
        BusinessObject bo = a as BusinessObject;
        if (bo != null) bo.IsDeleted = true;
      }

      base.OnBeforeItemRemoved(item);
    }

    //protected override void OnSetOwner(BusinessObject owner)
    //{
    //  base.OnSetOwner(owner);
    //}
  }
}

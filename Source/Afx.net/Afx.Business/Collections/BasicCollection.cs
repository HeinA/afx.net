﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.Collections
{
  [CollectionDataContract(Namespace = Namespace.Afx)]
  public class BasicCollection<T> : ObservableCollection<T>
  {
    public BasicCollection()
    {
    }

    public BasicCollection(IList<T> list)
      : base(list)
    {
    }
    
    public BasicCollection(IEnumerable<T> collection)
      : base(collection)
    {
    }

    bool mIsDeserializing;
    protected bool IsDeserializing
    {
      get { return mIsDeserializing; }
      set { mIsDeserializing = value; }
    }

    [OnDeserializing]
    void OnDeserializing(StreamingContext context)
    {
      IsDeserializing = true;
    }

    [OnDeserialized]
    void OnDeserialized(StreamingContext ctx)
    {
      IsDeserializing = false;
    }

    public void FireReset()
    {
      OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
    }
  }
}

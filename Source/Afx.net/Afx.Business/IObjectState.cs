﻿using Afx.Business.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business
{
  public interface IObjectState
  {
    User User { get; }
    DateTime Timestamp { get; }
    string Text { get; }
  }
}

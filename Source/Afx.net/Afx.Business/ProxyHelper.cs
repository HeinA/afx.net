﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Afx.Business
{
  public static class ProxyHelper
  {
    public static void SetupProxy()
    {
      try
      {
        System.IO.FileInfo fi = new System.IO.FileInfo(Environment.ExpandEnvironmentVariables(@"%programdata%\Indilinga SDL\ProxySettings.xml"));
        if (fi.Exists)
        {
          XmlDocument doc = new XmlDocument();
          doc.Load(fi.FullName);
          Uri proxyAddress = new Uri(doc.DocumentElement.Attributes["server"].Value);
          string userName = doc.DocumentElement.Attributes["username"].Value;
          string password = doc.DocumentElement.Attributes["password"].Value;

          WebRequest.DefaultWebProxy = new WebProxy(proxyAddress)
          {
            UseDefaultCredentials = false,
            Credentials = new NetworkCredential(userName, password),
            BypassProxyOnLocal = true
          };

          System.Net.ServicePointManager.Expect100Continue = false;
        }
      }
      catch (Exception ex)
      {
        DirectoryInfo di = new DirectoryInfo(@"C:\Logs");
        if (di.Exists)
        {
          using (var sw = new StreamWriter(@"C:\Logs\ProxyLog.txt"))
          {
            sw.Write(ex.ToString());
          }
        }
        throw ex;
      }
    }
  }
}

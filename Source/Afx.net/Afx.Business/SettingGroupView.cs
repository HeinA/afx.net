﻿using Afx.Business.Collections;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business
{
  public class SettingGroupView : BusinessObject
  {
    #region Constructors

    internal SettingGroupView(Guid globalIdentifier, string text)
    {
      GlobalIdentifier = globalIdentifier;
      Text = text;
    }

    #endregion

    #region string Text

    string mText;
    public string Text
    {
      get { return mText; }
      internal set { mText = value; }
    }

    #endregion

    #region IEnumerable<Setting> Setting

    internal BasicCollection<Setting> mSettings = new BasicCollection<Setting>();
    public IEnumerable<Setting> Settings
    {
      get { return mSettings; }
    }

    #endregion
  }
}

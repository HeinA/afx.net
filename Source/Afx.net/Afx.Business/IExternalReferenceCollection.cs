﻿using Afx.Business.Collections;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business
{
  public interface IExternalReferenceCollection
  {
    IList ExternalReferences { get; }

    string ExternalReferenceText { get; }

    IExternalReference CreateReference(ExternalSystem es, string reference);
  }
}

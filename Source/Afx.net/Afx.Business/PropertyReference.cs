﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business
{
  [DataContract(IsReference = true, Namespace = Namespace.Afx, Name="PropertyReference")]
  class PropertyReference
  {
    public PropertyReference(string propertyName, object value)
    {
      mPropertyName = propertyName;
      mReference = value;
    }

    #region string PropertyName

    [DataMember(Name = "Property", EmitDefaultValue = false)]
    string mPropertyName;
    public string PropertyName
    {
      get { return mPropertyName; }
    }

    #endregion

    #region object Reference

    [DataMember(Name = "Reference", EmitDefaultValue = false)]
    object mReference;
    public object Reference
    {
      get { return mReference; }
      set { mReference = value; }
    }

    #endregion
  }
}

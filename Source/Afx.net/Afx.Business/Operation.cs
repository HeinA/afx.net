﻿using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Runtime.Serialization;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Afx.Business
{
  [DataContract(IsReference = true, Namespace = Afx.Business.Namespace.Afx)]
  [PersistantObject(Schema = "Afx", OwnerColumn = "Group")]
  public abstract class Operation : BusinessObject<OperationGroup>, INamespaceSensitive
  {
    #region Constructors

    protected Operation()
    {
    }

    protected Operation(bool isNull)
      : base(isNull)
    {
    }

    protected Operation(Operation op)
    {
      Text = op.Text;
      Description = op.Description;
      Tag = op.Tag;
      Base64Image = op.Base64Image;
      mIdentifier = op.Identifier;
    }

    #endregion

    public abstract Operation CreateReplicate();

    #region DataMember string Text

    public const string TextProperty = "Text";
    [DataMember(Name = TextProperty, EmitDefaultValue = false)]
    string mText;
    [PersistantProperty(SortOrder = 0)]
    public string Text
    {
      get { return mText; }
      set { SetProperty<string>(ref mText, value); }
    }

    #endregion

    #region DataMember string Description

    public const string DescriptionProperty = "Description";
    [DataMember(Name = DescriptionProperty, EmitDefaultValue = false)]
    string mDescription;
    [PersistantProperty]
    public string Description
    {
      get { return mDescription; }
      set { SetProperty<string>(ref mDescription, value); }
    }

    #endregion

    #region string Namespace

    public const string NamespaceProperty = "Namespace";
    [DataMember(Name = NamespaceProperty, EmitDefaultValue = false)]
    string mNamespace;
    [PersistantProperty]
    public string Namespace
    {
      get { return mNamespace; }
      set { SetProperty<string>(ref mNamespace, value); }
    }

    #endregion

    #region object Tag

    object mTag;
    public object Tag
    {
      get { return mTag; }
      set { mTag = value; }
    }

    #endregion

    #region string Base64Image

    #region DataMember string Base64Image

    const string Base64ImageProperty = "Base64Image";
    [DataMember(Name = Base64ImageProperty, EmitDefaultValue = false)]
    string mBase64Image;
    [PersistantProperty(IgnoreConcurrency = true)]
    public string Base64Image
    {
      get { return mBase64Image; }
      set { SetProperty<string>(ref mBase64Image, value, Base64ImageProperty); }
    }

    #endregion

    #region void SetImageFromFile(string filename)

    public void SetImageFromFile(string filename)
    {
      using (FileStream fs = new FileStream(filename, FileMode.Open, FileAccess.Read))
      {
        byte[] imgBytes = new byte[fs.Length];
        fs.Read(imgBytes, 0, Convert.ToInt32(fs.Length));
        Base64Image = Convert.ToBase64String(imgBytes, Base64FormattingOptions.InsertLineBreaks);
      }
      OnPropertyChanged(Base64ImageProperty);
    }

    #endregion

    #endregion

    string mIdentifier;
    public override string Identifier
    {
      get { return mIdentifier ?? base.Identifier; }
    }

    #region Validation

    protected override void GetErrors(System.Collections.ObjectModel.Collection<string> errors, string propertyName)
    {
      if (string.IsNullOrWhiteSpace(propertyName) || propertyName == "Text")
      {
        if (string.IsNullOrWhiteSpace(Text)) errors.Add("Text is a mandatory field");
      }

      if (string.IsNullOrWhiteSpace(propertyName) || propertyName == "Description")
      {
        if (string.IsNullOrWhiteSpace(Description)) errors.Add("Description is a mandatory field");
      }

      base.GetErrors(errors, propertyName);
    }

    #endregion
  }
}
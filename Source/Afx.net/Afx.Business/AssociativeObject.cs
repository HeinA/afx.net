﻿using Afx.Business.Attributes;
using Afx.Business.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business
{
  [PersistantObject(PersistToDerivedClass = true)]
  [DataContract(IsReference = true, Namespace = Namespace.Afx)]
  [NonCacheable]
  public abstract class AssociativeObject<TOwner, TReference> : BusinessObject<TOwner>, IAssociativeObject
    where TOwner : BusinessObject
    where TReference : BusinessObject
  {
    protected virtual bool ReferenceIsOwned
    {
      get { return false; }
    }

    #region DataMember TReference Reference

    public const string ReferenceProperty = "Reference";
    protected TReference mReference;
    [DataMember(Name = ReferenceProperty, EmitDefaultValue = false)]
    public TReference Reference
    {
      get
      {
        try
        {
          if (IsSerializing && !ReferenceIsOwned && ObjectAttributeHelper.IsCached(typeof(TReference))) // && mReference != null && Cache.Instance.Contains(mReference.GlobalIdentifier)) 
            return null;
          return mReference ?? (mReference = GetCachedObject<TReference>(ReferenceProperty));
        }
        catch
        {
          throw;
        }
      }
      internal set
      {
        if (SetProperty<TReference>(ref mReference, value, ReferenceProperty))
        {
          if (!ReferenceIsOwned && ObjectAttributeHelper.IsCached(typeof(TReference)))
          {
            SetCachedObject<TReference>(mReference, ReferenceProperty);
          }
          else if (!IsDeserializing)
          {
            mReference.mOwner = Owner;
          }

          if (!IsDeserializing && !SuppressEventState) OnReferenceChanged();
        }
      }
    }

    #endregion


    #region IAssociativeObject

    bool IAssociativeObject.ReferenceIsOwned
    {
      get { return ReferenceIsOwned; }
    }

    BusinessObject IAssociativeObject.Reference
    {
      get { return (BusinessObject)Reference; }
      set { Reference = (TReference)value; }
    }

    Guid IAssociativeObject.ReferenceId
    {
      get
      {
        if (ReferenceIsOwned || !ObjectAttributeHelper.IsCached(typeof(TReference)))
        {
          if (Reference != null) return Reference.GlobalIdentifier;
          return Guid.Empty;
        }
        return GetReferenceValue(ReferenceProperty);
      }
      //set
      //{
      //  if (ReferenceIsOwned || !ObjectAttributeHelper.IsCached(typeof(TReference)))
      //  {
      //    SetReferenceValue(value, ReferenceProperty);
      //  }
      //}
    }

    #endregion

    protected virtual void OnReferenceChanged()
    {
    }
  }
}

﻿using Afx.Business.Attributes;
using Afx.Business.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.Common
{
  [DataContract(IsReference = true, Namespace = Namespace.Afx)]
  [PersistantObject(Schema = "Afx")]
  [Cache]
  public class LookupType : BusinessObject
  {
    #region DataMember string Text

    public const string TextProperty = "Text";
    [DataMember(Name = TextProperty, EmitDefaultValue = false)]
    string mText;
    [PersistantProperty]
    public string Text
    {
      get { return mText; }
      set { SetProperty<string>(ref mText, value); }
    }

    #endregion
  }
}

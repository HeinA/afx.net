﻿//using System;
//using System.Collections.Generic;
//using System.Diagnostics;
//using System.IO;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;

//namespace Afx.Business
//{
//  public static class DebugHelper
//  {
//    static Stopwatch mSW = new Stopwatch();

//    public static void Start()
//    {
//      FileInfo fi = new FileInfo(@"c:\logs\debug.txt");
//      if (fi.Exists) fi.Delete();
//      mSW.Start();
//    }

//    static object mLock = new object();

//    public static void LogMessage(string message)
//    {
//      lock (mLock)
//      {
//        DirectoryInfo di = new DirectoryInfo(@"c:\logs");
//        if (!di.Exists) return;
//        using (var sw = new StreamWriter(@"c:\logs\debug.txt", true))
//        {
//          sw.WriteLine(string.Format("{0:00,000}: {1}", mSW.ElapsedMilliseconds, message));
//        }
//      }
//    }
//  }
//}

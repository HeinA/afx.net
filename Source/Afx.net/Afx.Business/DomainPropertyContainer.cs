﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Business
{
  public static class DomainPropertyContainer
  {
    static object _lock = new object();
    public static object Lock
    {
      get
      {
        if (DomainPropertyContainer._lock == null) DomainPropertyContainer._lock = new object();
        return DomainPropertyContainer._lock;
      }
    }

    static Dictionary<Type, Dictionary<Guid, Dictionary<string, object>>> _properties;
    static Dictionary<Type, Dictionary<Guid, Dictionary<string, object>>> Properties
    {
      get
      {
        if (DomainPropertyContainer._properties == null) DomainPropertyContainer._properties = new Dictionary<Type, Dictionary<Guid, Dictionary<string, object>>>();
        return DomainPropertyContainer._properties;
      }
    }

    public static T GetProperty<T>(BusinessObject bo, string propertyName)
    {
      if (bo == null) throw new ArgumentNullException("bo");
      if (string.IsNullOrWhiteSpace(propertyName)) throw new ArgumentException(Dataforge.Business.Properties.Resources.InvalidParameter, "propertyName");

      lock (Lock)
      {
        Type t = bo.GetType();
        if (!Properties.ContainsKey(t)) return (T)GetDefault(typeof(T));
        Dictionary<Guid, Dictionary<string, object>> typeDict = Properties[t];
        if (!typeDict.ContainsKey(bo.Guid)) return (T)GetDefault(typeof(T));
        Dictionary<string, object> objectDict = typeDict[bo.Guid];
        if (!objectDict.ContainsKey(propertyName)) return (T)GetDefault(typeof(T));
        return (T)objectDict[propertyName];
      }
    }

    public static void SetProperty(BusinessObject bo, string propertyName, object value)
    {
      if (bo == null) throw new ArgumentNullException("bo");
      if (string.IsNullOrWhiteSpace(propertyName)) throw new ArgumentException(Dataforge.Business.Properties.Resources.InvalidParameter, "propertyName");

      lock (Lock)
      {
        Type t = bo.GetType();
        if (!Properties.ContainsKey(t)) Properties.Add(t, new Dictionary<Guid, Dictionary<string, object>>());
        Dictionary<Guid, Dictionary<string, object>> typeDict = Properties[t];
        if (!typeDict.ContainsKey(bo.Guid)) typeDict.Add(bo.Guid, new Dictionary<string, object>());
        Dictionary<string, object> objectDict = typeDict[bo.Guid];
        if (!objectDict.ContainsKey(propertyName)) objectDict.Add(propertyName, value);
        else objectDict[propertyName] = value;
      }
    }

    static object GetDefault(Type type)
    {
      if (type.IsValueType) return Activator.CreateInstance(type);
      return null;
    }
  }
}

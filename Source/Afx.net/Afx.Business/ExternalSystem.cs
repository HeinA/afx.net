﻿using Afx.Business;
using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Validation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace Afx.Business
{
  [PersistantObject(Schema = "Afx", HasFlags = true)]
  [Cache]
  public partial class ExternalSystem : BusinessObject, IRevisable
  {
    #region Constructors

    public ExternalSystem()
    {
    }

    public ExternalSystem(bool isNull)
      : base(isNull)
    {
    }

    #endregion

    #region int Revision

    public const string RevisionProperty = "Revision";
    [DataMember(Name = RevisionProperty, EmitDefaultValue = false)]
    int mRevision = 1;
    [PersistantProperty]
    public int Revision
    {
      get { return mRevision; }
      set { SetProperty<int>(ref mRevision, value); }
    }

    #endregion

    #region string Name

    public const string NameProperty = "Name";
    [DataMember(Name = NameProperty, EmitDefaultValue = false)]
    string mName;
    [PersistantProperty]
    [Mandatory("Name is a mandatory field.")]
    public string Name
    {
      get { return mName; }
      set { SetProperty<string>(ref mName, value); }
    }

    #endregion

    #region string ConnectionString

    public const string ConnectionStringProperty = "ConnectionString";
    [DataMember(Name = ConnectionStringProperty, EmitDefaultValue = false)]
    string mConnectionString;
    [PersistantProperty]
    [Mandatory("ConnectionString is a mandatory field.")]
    public string ConnectionString
    {
      get { return mConnectionString; }
      set { SetProperty<string>(ref mConnectionString, value); }
    }

    #endregion
  }
}

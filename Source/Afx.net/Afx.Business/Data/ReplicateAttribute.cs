﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.Data
{
  /// <summary>
  /// The object or owned collection should be replicated.
  /// </summary>
  [AttributeUsage(AttributeTargets.Class)]
  public class ReplicateAttribute : Attribute
  {
  }
}

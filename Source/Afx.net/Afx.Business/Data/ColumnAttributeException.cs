﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.Data
{
  [Serializable]
  public class ColumnAttributeException : Exception
  {
    protected ColumnAttributeException(SerializationInfo info, StreamingContext context)
      :base(info, context)
    {
    }

    public ColumnAttributeException(string propertyName, string message)
      : base(string.Format("{0}: {1}", propertyName, message))
    {
    }

    public ColumnAttributeException(string propertyName, string message, Exception innerException)
      : base(string.Format("{0}: {1}", propertyName, message), innerException)
    {
    }
  }
}

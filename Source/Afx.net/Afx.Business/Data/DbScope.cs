﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.Data
{
  public class DbScope : IDisposable
  {
    [ThreadStatic]
    static Stack<String> mScopeStack;
    static Stack<String> ScopeStack
    {
      get { return mScopeStack ?? (mScopeStack = new Stack<string>()); }
    }

    public DbScope(string connectionName)
    {
      ScopeStack.Push(connectionName);
    }

    public static string ConnectionName
    {
      get
      {
        if (ScopeStack.Count == 0) return "Local";
        return ScopeStack.Peek();
      }
    }

    public void Dispose()
    {
      ScopeStack.Pop();
    }
  }
}

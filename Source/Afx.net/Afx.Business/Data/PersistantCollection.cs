﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Afx.Business.Data
{
  class PersistantCollection
  {
    public PersistantCollection(IInternalObjectRepository objectRepository, PropertyInfo pi, PersistantCollectionAttribute pca)
    {
      ObjectRepository = objectRepository;
      PropertyInfo = pi;

      CollectionType = pi.PropertyType.GetGenericArguments()[0];
      AssociativeType = pca.AssociativeType;
      //IsAggregate = pca.IsAggregate;
      
      PropertyName = pi.Name;
      PropertyType = pi.PropertyType;

      InitEmit();
    }

    #region Properties

    IInternalObjectRepository mObjectRepository;
    IInternalObjectRepository ObjectRepository
    {
      get { return mObjectRepository; }
      set { mObjectRepository = value; }
    }

    PropertyInfo mPropertyInfo;
    PropertyInfo PropertyInfo
    {
      get { return mPropertyInfo; }
      set { mPropertyInfo = value; }
    }

    Type mPropertyType;
    public Type PropertyType
    {
      get { return mPropertyType; }
      private set { mPropertyType = value; }
    }

    string mPropertyName;
    public string PropertyName
    {
      get { return mPropertyName; }
      private set { mPropertyName = value; }
    }

    Type mCollectionType;
    public Type CollectionType
    {
      get { return mCollectionType; }
      private set { mCollectionType = value; }
    }

    Type mAssociativeType;
    public Type AssociativeType
    {
      get { return mAssociativeType; }
      set { mAssociativeType = value; }
    }

    public bool IsAssociative
    {
      get { return AssociativeType != null; }
    }

    //bool mIsAggregate = false;
    //public bool IsAggregate
    //{
    //  get { return mIsAggregate; }
    //  set { mIsAggregate = value; }
    //}

    #endregion

    #region GetValue

    public IList GetValue(object target)
    {
      try
      {
        if (mEmittedPropertyAccessor != null && PropertyInfo.GetMethod.IsPublic)
          return (IList)GetValueEmit(target);
        else
          return (IList)PropertyInfo.GetValue(target);
      }
      catch
      {
        throw;
      }
    }

    #endregion

    #region Emit

    // ***********************************************************************************************************
    // Based on
    // http://www.codeproject.com/Articles/9927/Fast-Dynamic-Property-Access-with-C
    // ***********************************************************************************************************

    object GetValueEmit(object target)
    {
      if (PropertyInfo.CanRead) return this.mEmittedPropertyAccessor.Get(target);
      else throw new PersistantPropertyException(PropertyName, "Property does not have a get method.");
    }

    IPropertyAccessor mEmittedPropertyAccessor;
    //Hashtable mTypeHash;

    void InitEmit()
    {
      if (!ObjectRepository.ObjectType.IsPublic) return;
      //this.InitEmitTypes();
      Assembly assembly = EmitAssembly();
      mEmittedPropertyAccessor = assembly.CreateInstance("Property") as IPropertyAccessor;
      //mTypeHash = null;
    }

    ///// <summary>
    ///// Thanks to Ben Ratzlaff for this snippet of code
    ///// http://www.codeproject.com/cs/miscctrl/CustomPropGrid.asp
    ///// 
    ///// "Initialize a private hashtable with type-opCode pairs 
    ///// so i dont have to write a long if/else statement when outputting msil"
    ///// </summary>
    //void InitEmitTypes()
    //{
    //  mTypeHash = new Hashtable();
    //  mTypeHash[typeof(sbyte)] = OpCodes.Ldind_I1;
    //  mTypeHash[typeof(byte)] = OpCodes.Ldind_U1;
    //  mTypeHash[typeof(char)] = OpCodes.Ldind_U2;
    //  mTypeHash[typeof(short)] = OpCodes.Ldind_I2;
    //  mTypeHash[typeof(ushort)] = OpCodes.Ldind_U2;
    //  mTypeHash[typeof(int)] = OpCodes.Ldind_I4;
    //  mTypeHash[typeof(uint)] = OpCodes.Ldind_U4;
    //  mTypeHash[typeof(long)] = OpCodes.Ldind_I8;
    //  mTypeHash[typeof(ulong)] = OpCodes.Ldind_I8;
    //  mTypeHash[typeof(bool)] = OpCodes.Ldind_I1;
    //  mTypeHash[typeof(double)] = OpCodes.Ldind_R8;
    //  mTypeHash[typeof(float)] = OpCodes.Ldind_R4;
    //}

    /// <summary>
    /// Create an assembly that will provide the get and set methods.
    /// </summary>
    Assembly EmitAssembly()
    {
      //
      // Create an assembly name
      //
      AssemblyName assemblyName = new AssemblyName();
      assemblyName.Name = "PropertyAccessorAssembly";
      //
      // Create a new assembly with one module
      //
      AssemblyBuilder newAssembly = Thread.GetDomain().DefineDynamicAssembly(assemblyName, AssemblyBuilderAccess.Run);
      ModuleBuilder newModule = newAssembly.DefineDynamicModule("Module");
      //
      // Define a public class named "Property" in the assembly.
      //
      TypeBuilder myType = newModule.DefineType("Property", TypeAttributes.Public);
      //
      // Mark the class as implementing IPropertyAccessor. 
      //
      myType.AddInterfaceImplementation(typeof(IPropertyAccessor));
      // Add a constructor
      ConstructorBuilder constructor = myType.DefineDefaultConstructor(MethodAttributes.Public);
      //
      // Define a method for the get operation. 
      //
      Type[] getParamTypes = new Type[] { typeof(object) };
      Type getReturnType = typeof(object);
      MethodBuilder getMethod = myType.DefineMethod("Get", MethodAttributes.Public | MethodAttributes.Virtual, getReturnType, getParamTypes);
      //
      // From the method, get an ILGenerator. This is used to
      // emit the IL that we want.
      //
      ILGenerator getIL = getMethod.GetILGenerator();

      //
      // Emit the IL. 
      //
      MethodInfo targetGetMethod = this.ObjectRepository.ObjectType.GetMethod("get_" + this.PropertyName);
      if (targetGetMethod != null)
      {
        getIL.DeclareLocal(typeof(object));
        getIL.Emit(OpCodes.Ldarg_1); //Load the first argument
        //(target object)
        //Cast to the source type
        getIL.Emit(OpCodes.Castclass, this.ObjectRepository.ObjectType);
        //Get the property value
        getIL.EmitCall(OpCodes.Call, targetGetMethod, null);
        //if (targetGetMethod.ReturnType.IsValueType)
        //{
        //  getIL.Emit(OpCodes.Box, targetGetMethod.ReturnType);
        //  //Box if necessary
        //}
        getIL.Emit(OpCodes.Stloc_0); //Store it

        getIL.Emit(OpCodes.Ldloc_0);
      }
      else
      {
        getIL.ThrowException(typeof(MissingMethodException));
      }
      getIL.Emit(OpCodes.Ret);

      Type[] setParamTypes = new Type[] { typeof(object), typeof(object) };
      Type setReturnType = null;
      MethodBuilder setMethod = myType.DefineMethod("Set", MethodAttributes.Public | MethodAttributes.Virtual, setReturnType, setParamTypes);
      ILGenerator setIL = setMethod.GetILGenerator();
      MethodInfo targetSetMethod = this.ObjectRepository.ObjectType.GetMethod("set_" + this.PropertyName);
      setIL.ThrowException(typeof(MissingMethodException));
      setIL.Emit(OpCodes.Ret);

      //
      // Load the type
      //
      myType.CreateType();
      return newAssembly;
    }

    #endregion
  }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.Data
{
  class PersistanceCancelledException : Exception
  {
    protected PersistanceCancelledException(SerializationInfo info, StreamingContext context)
      : base(info, context)
    {
    }

    public PersistanceCancelledException()
      : base()
    {
    }
  }
}

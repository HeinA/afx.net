﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.Data
{
  class DeleteStatement : StatementBase
  {
    public DeleteStatement(IInternalObjectRepository objectRepository)
      :base(objectRepository.PersistanceManager)
    {
      ObjectRepository = objectRepository;
    }

    IInternalObjectRepository mObjectRepository;
    IInternalObjectRepository ObjectRepository
    {
      get { return mObjectRepository; }
      set { mObjectRepository = value; }
    }

    public void Execute(BusinessObject obj, PersistanceFlags flags)
    {
      string query = string.Format("DELETE FROM {0} WHERE {1} = {2}", ObjectRepository.GetFullTableName(), ObjectRepository.GetFullGlobalIdentifierColumn(), AddParameter(obj.GlobalIdentifier));
      if (typeof(IRevisable).IsAssignableFrom(ObjectRepository.ObjectType))
      {
        object oldValue = 0;
        obj.GetOriginalValue("Revision", ref oldValue);
        IRevisable r = obj as IRevisable;
        query += string.Format(" AND {0}.{1}={2}", ObjectRepository.GetFullTableName(), "Revision", AddParameter(oldValue));
      }
      Command.CommandText = query;
      PrintCommand();
      int rows = Command.ExecuteNonQuery();
      if (rows == 0 && (flags & PersistanceFlags.Import) == PersistanceFlags.Import)
        throw new MessageException(string.Format("Object of type {0} ({1}), was not found to be deleted.", ObjectRepository.ObjectType, obj.GlobalIdentifier));
    }
  }
}

﻿using Afx.Business.Security;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.Data
{
  class UpdateStatement : StatementBase
  {
    public UpdateStatement(IInternalObjectRepository objectRepository, PersistanceFlags flags)
      : base(objectRepository.PersistanceManager)
    {
      ObjectRepository = objectRepository;
      mFlags = flags;
    }

    IInternalObjectRepository mObjectRepository;
    IInternalObjectRepository ObjectRepository
    {
      get { return mObjectRepository; }
      set { mObjectRepository = value; }
    }

    string mIdColumn;
    string IdColumn
    {
      get { return mIdColumn; }
      set { mIdColumn = value; }
    }

    Collection<string> mSetClauses = new Collection<string>();
    public IEnumerable<string> SetClauses
    {
      get { return mSetClauses; }
    }

    Collection<string> mWhereClauses = new Collection<string>();
    public IEnumerable<string> WhereClauses
    {
      get { return mWhereClauses; }
    }

    public int Count
    {
      get { return mSetClauses.Count; }
    }

    PersistanceFlags mFlags = PersistanceFlags.None;
    public PersistanceFlags Flags
    {
      get { return mFlags; }
    }

    public void AddProperty(PersistantProperty pp, BusinessObject obj, Stack<BusinessObject> persisted, bool checkConcurrency = true)
    {
      if (pp.IsIdColumn || pp.IsGlobalIdentifierColumn || pp.IsOwnerColumn) return;

      if (typeof(BusinessObject).IsAssignableFrom(pp.PropertyType))
      {
        BusinessObject bo = (BusinessObject)pp.GetValue(obj);
        int refId = 0;
        if (bo != null)
        {
          refId = IdMapper.Instance.GetId(bo.GlobalIdentifier, bo.GetType());
          if (refId == 0)
          {
            IInternalObjectRepository or = PersistanceManager.GetRepository(bo.GetType());
            if (bo.IsOwnedBy(ObjectRepository.ObjectType))
            {
              bo.SetReferenceValue(obj.GlobalIdentifier, BusinessObject.OwnerProperty);
            }
            or.Persist(bo, persisted, Flags);
            refId = bo.Id;
          }
        }
        object oldRefId = refId;
        if (obj.GetOriginalValue(pp.PropertyName, ref oldRefId) || pp.IgnoreConcurrency || !checkConcurrency || (Flags & PersistanceFlags.Import) == PersistanceFlags.Import)
        {
          if (oldRefId is Guid) oldRefId = IdMapper.Instance.GetId((Guid)oldRefId, bo == null ? pp.PropertyType : bo.GetType());
          if (refId > 0) AddColumnValue(pp, refId, oldRefId, checkConcurrency);
          else if (!pp.IgnoreNull) AddColumnValue(pp, null, oldRefId, checkConcurrency);
        }
      }
      else
      {
        object value = pp.GetValue(obj);
        object oldValue = value;
        if (obj.GetOriginalValue(pp.PropertyName, ref oldValue) || pp.IgnoreConcurrency || !checkConcurrency || (Flags & PersistanceFlags.Import) == PersistanceFlags.Import)
        {
          if (value != null || (value == null && !pp.IgnoreNull)) AddColumnValue(pp, value, oldValue, checkConcurrency);
        }
      }
    }

    void AddColumnValue(PersistantProperty pp, object value, object oldValue, bool checkConcurrency)
    {
      if (value != null || !pp.IgnoreNull) AddColumnValue(pp, value);
      if ((!pp.IgnoreConcurrency && checkConcurrency && (Flags & PersistanceFlags.Import) == 0) || (typeof(IRevisable).IsAssignableFrom(pp.ObjectRepository.ObjectType) && pp.PropertyName == "Revision" && (Flags & PersistanceFlags.IgnoreRevision) == 0))
      {
        if ((typeof(IRevisable).IsAssignableFrom(pp.ObjectRepository.ObjectType) && pp.PropertyName == "Revision"))
        {
          if ((Flags & PersistanceFlags.IgnoreRevision) == 0) AddWhereValue(pp, oldValue);
        }
        else
        {
          //AddWhereValue(pp, value, oldValue);
        }
      }
    }

    void AddColumnValue(PersistantProperty pp, object value)
    {
      mSetClauses.Add(string.Format("{0} = {1}", pp.GetColumnName(), PersistanceManager.AddParameter(ParameterCount, value == null ? DBNull.Value : value, Command)));
    }

    void AddWhereValue(PersistantProperty pp, params object[] values)
    {
      if (values.Length == 1)
      {
        if (values[0] == null || (typeof(BusinessObject).IsAssignableFrom(pp.PropertyType) && (int)values[0] == 0)) mWhereClauses.Add(string.Format("{0} IS NULL", pp.GetColumnName()));
        else mWhereClauses.Add(string.Format("{0} = {1}", pp.GetColumnName(), PersistanceManager.AddParameter(ParameterCount, values[0], Command)));
      }
      else
      {
        string where = string.Empty;
        bool bFirst = true;
        foreach (object o in values)
        {
          if (!bFirst) where += " OR ";
          else bFirst = false;
          if (o == null || (typeof(BusinessObject).IsAssignableFrom(pp.PropertyType) && (int)o == 0)) where += string.Format("{0} IS NULL", pp.GetColumnName());
          else where += string.Format("{0} = {1}", pp.GetColumnName(), PersistanceManager.AddParameter(ParameterCount, o, Command));
        }
        mWhereClauses.Add(string.Format("({0})", where));
      }
    }

    void AddColumnValue(string columnName, object value)
    {
      mSetClauses.Add(string.Format("{0} = {1}", columnName, PersistanceManager.AddParameter(ParameterCount, value == null ? DBNull.Value : value, Command)));
    }

    void AddWhereValue(string columnName, params object[] values)
    {
      if (values.Length == 1)
      {
        mWhereClauses.Add(string.Format("{0} = {1}", columnName, PersistanceManager.AddParameter(ParameterCount, values[0], Command)));
      }
      else
      {
        string where = string.Empty;
        bool bFirst = true;
        foreach (object o in values)
        {
          if (!bFirst) where += " OR ";
          where += string.Format("{0} = {1}", columnName, PersistanceManager.AddParameter(ParameterCount, values[0], Command));
        }
        mWhereClauses.Add(string.Format("({0})", where));
      }
    }

    public void Execute(IBusinessObject obj)
    {
      if (mSetClauses.Count == 0) throw new QueryException("There are no columns to update.");

      try
      {
        AddWhereValue(ObjectRepository.GetIdColumn(), obj.Id);

        string query = string.Format("UPDATE {0} WITH (ROWLOCK) SET {1} WHERE {2}", ObjectRepository.GetFullTableName(), string.Join(", ", mSetClauses), string.Join(" AND ", mWhereClauses));
        Command.CommandText = query;
        PrintCommand();
        int rows = Command.ExecuteNonQuery();
        if (rows == 0 && (Flags & PersistanceFlags.Import) == 0)
        {
          PrintDebugCommand();
          throw new OptimisticConcurrencyException(string.Format("Could not update object of type {0} ({1}), it was updated from elsewhere.", obj.GetType(), obj.Id));
        }
      }
      catch (SqlException ex)
      {
        switch (ex.ErrorCode)
        {
          case -2146232060:
            if (ex.Message.Contains("insert duplicate key"))
            {
              throw new UniqueIndexViolationException("Primary key constraint violation", obj.GetType(), ex);
            }
            else
            {
              throw ex;
            }

          default:
            throw ex;
        }
      }
      catch (Exception ex)
      {
        throw ex;
      }
    }
  }
}

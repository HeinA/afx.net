﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.Data
{
  public enum FilterType
  {
    Equals,
    NotEquals,
    Like,
    GreaterThan,
    GreaterThanEqual,
    LessThan,
    LessThanEqual,
    IsNull,
    IsNotNull,
    In,
    NotIn
  }
}

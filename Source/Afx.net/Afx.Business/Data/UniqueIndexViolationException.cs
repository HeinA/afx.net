﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.Data
{
  [Serializable]
  public class UniqueIndexViolationException : Exception
  {
    protected UniqueIndexViolationException(SerializationInfo info, StreamingContext context)
      :base(info, context)
    {
    }

    public UniqueIndexViolationException(string message, Type type)
      : base(message)
    {
      this.Type = type;
    }

    public UniqueIndexViolationException(string message, Type type, Exception innerException)
      : base(message, innerException)
    {
      this.Type = type;
    }

    #region Type Type

    public const string TypeProperty = "Type";
    Type mType;
    public Type Type
    {
      get { return mType; }
      private set { mType = value; }
    }

    #endregion
  }
}

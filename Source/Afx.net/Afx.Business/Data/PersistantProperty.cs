﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Afx.Business.Data
{
  class PersistantProperty
  {
    public PersistantProperty(IInternalObjectRepository objectRepository, PropertyInfo pi, PersistantPropertyAttribute ca)
    {
      ObjectRepository = objectRepository;
      PropertyInfo = pi;

      if (typeof(INamespaceSensitive).IsAssignableFrom(ObjectRepository.ObjectType) && pi.Name == "Namespace")
      {
        IsNamespaceColumn = true;
        ObjectRepository.NamespaceProperty = this;
      }

      ColumnName = ca.Name ?? pi.Name;
      if (ColumnName == ObjectRepository.IdColumn) IsIdColumn = true;
      if (ColumnName == ObjectRepository.GlobalIdentifierColumn) IsGlobalIdentifierColumn = true;
      Read = ca.Read;
      Write = ca.Write;
      if (ColumnName == ObjectRepository.OwnerColumn) IsOwnerColumn = true;
      SortOrder = ca.SortOrder;
      SortDirection = ca.SortDirection;
      IgnoreNull = ca.IgnoreNull;
      PostProcess = ca.PostProcess;
      IgnoreConcurrency = ca.IgnoreConcurrency;
      ImportOverwrite = ca.ImportOverwrite;
      IsCached = ca.IsCached;
      ForceLoad = ca.ForceLoad;

      PropertyName = pi.Name;
      PropertyType = pi.PropertyType;

      //InitEmit();
    }

    #region Properties

    #region bool ImportOverwrite

    bool mImportOverwrite;
    public bool ImportOverwrite
    {
      get { return mImportOverwrite; }
      set { mImportOverwrite = value; }
    }

    #endregion

    #region bool IsCached

    public const string IsCachedProperty = "IsCached";
    bool mIsCached;
    public bool IsCached
    {
      get { return mIsCached; }
      set { mIsCached = value; }
    }

    #endregion

    #region bool ForceLoad

    public const string ForceLoadProperty = "ForceLoad";
    bool mForceLoad;
    public bool ForceLoad
    {
      get { return mForceLoad; }
      set { mForceLoad = value; }
    }

    #endregion

    IInternalObjectRepository mObjectRepository;
    internal IInternalObjectRepository ObjectRepository
    {
      get { return mObjectRepository; }
      set { mObjectRepository = value; }
    }

    PropertyInfo mPropertyInfo;
    public PropertyInfo PropertyInfo
    {
      get { return mPropertyInfo; }
      set { mPropertyInfo = value; }
    }

    bool mIsOwnerColumn = false;
    public bool IsOwnerColumn
    {
      get { return mIsOwnerColumn; }
      private set { mIsOwnerColumn = value; }
    }

    bool mIsNamespaceColumn = false;
    public bool IsNamespaceColumn
    {
      get { return mIsNamespaceColumn; }
      private set { mIsNamespaceColumn = value; }
    }

    bool mPostProcess = false;
    public bool PostProcess
    {
      get { return mPostProcess; }
      private set { mPostProcess = value; }
    }

    bool mIsIdColumn = false;
    public bool IsIdColumn
    {
      get { return mIsIdColumn; }
      private set { mIsIdColumn = value; }
    }

    bool mIsGlobalIdentifierColumn = false;
    public bool IsGlobalIdentifierColumn
    {
      get { return mIsGlobalIdentifierColumn; }
      private set { mIsGlobalIdentifierColumn = value; }
    }

    string mColumnName;
    public string ColumnName
    {
      get { return mColumnName; }
      private set { mColumnName = value; }
    }

    bool mRead = true;
    public bool Read
    {
      get { return mRead; }
      private set { mRead = value; }
    }

    bool mWrite = true;
    public bool Write
    {
      get { return mWrite; }
      private set { mWrite = value; }
    }

    int mSortOrder = -1;
    public int SortOrder
    {
      get { return mSortOrder; }
      private set { mSortOrder = value; }
    }

    SortDirection mSortDirection = SortDirection.Ascending;
    public SortDirection SortDirection
    {
      get { return mSortDirection; }
      private set { mSortDirection = value; }
    }

    bool mIgnoreNull = false;
    public bool IgnoreNull
    {
      get { return mIgnoreNull; }
      private set { mIgnoreNull = value; }
    }

    //bool mIsReference = false;
    //public bool IsReference
    //{
    //  get { return mIsReference; }
    //  private set { mIsReference = value; }
    //}

    bool mIgnoreConcurrency = false;
    public bool IgnoreConcurrency
    {
      get { return mIgnoreConcurrency; }
      private set { mIgnoreConcurrency = value; }
    }

    Type mPropertyType;
    public Type PropertyType
    {
      get { return mPropertyType; }
      private set { mPropertyType = value; }
    }

    string mPropertyName;
    public string PropertyName
    {
      get { return mPropertyName; }
      private set { mPropertyName = value; }
    }

    #endregion

    public string GetFullColumnName()
    {
      return string.Format("{0}.{1}", ObjectRepository.GetFullTableName(), ObjectRepository.PersistanceManager.GetIdentifier(ColumnName));
    }

    public string GetColumnName()
    {
      return string.Format("{0}", ObjectRepository.PersistanceManager.GetIdentifier(ColumnName));
    }

    #region Get & Set Values

    public void SetValue(object target, object value)
    {
      try
      {
        //if (mEmittedPropertyAccessor != null && PropertyInfo.SetMethod.IsPublic)
        //  SetValueEmit(target, value);
        //else
          PropertyInfo.SetValue(target, value);
      }
      catch
      {
        throw;
      }
    }

    public object GetValue(object target)
    {
      try
      {
        //if (mEmittedPropertyAccessor != null && PropertyInfo.GetMethod.IsPublic)
        //  return GetValueEmit(target);
        //else
          return PropertyInfo.GetValue(target);
      }
      catch
      {
        throw;
      }
    }

    //#region Emit

    //// ***********************************************************************************************************
    //// Based on
    //// http://www.codeproject.com/Articles/9927/Fast-Dynamic-Property-Access-with-C
    //// ***********************************************************************************************************

    //object GetValueEmit(object target)
    //{
    //  if (PropertyInfo.CanRead) return this.mEmittedPropertyAccessor.Get(target);
    //  else throw new PersistantPropertyException(PropertyName, "Property does not have a get method.");
    //}

    //void SetValueEmit(object target, object value)
    //{
    //  if (PropertyInfo.CanWrite) this.mEmittedPropertyAccessor.Set(target, value);
    //  else throw new PersistantPropertyException(PropertyName, "Property does not have a set method.");
    //}

    //IPropertyAccessor mEmittedPropertyAccessor;
    //Hashtable mTypeHash;

    //void InitEmit()
    //{
    //  if (!ObjectRepository.ObjectType.IsPublic) return;
    //  this.InitEmitTypes();
    //  Assembly assembly = EmitAssembly();
    //  mEmittedPropertyAccessor = assembly.CreateInstance("Property") as IPropertyAccessor;
    //  mTypeHash = null;
    //}

    ///// <summary>
    ///// Thanks to Ben Ratzlaff for this snippet of code
    ///// http://www.codeproject.com/cs/miscctrl/CustomPropGrid.asp
    ///// 
    ///// "Initialize a private hashtable with type-opCode pairs 
    ///// so i dont have to write a long if/else statement when outputting msil"
    ///// </summary>
    //void InitEmitTypes()
    //{
    //  mTypeHash = new Hashtable();
    //  mTypeHash[typeof(sbyte)] = OpCodes.Ldind_I1;
    //  mTypeHash[typeof(byte)] = OpCodes.Ldind_U1;
    //  mTypeHash[typeof(char)] = OpCodes.Ldind_U2;
    //  mTypeHash[typeof(short)] = OpCodes.Ldind_I2;
    //  mTypeHash[typeof(ushort)] = OpCodes.Ldind_U2;
    //  mTypeHash[typeof(int)] = OpCodes.Ldind_I4;
    //  mTypeHash[typeof(uint)] = OpCodes.Ldind_U4;
    //  mTypeHash[typeof(long)] = OpCodes.Ldind_I8;
    //  mTypeHash[typeof(ulong)] = OpCodes.Ldind_I8;
    //  mTypeHash[typeof(bool)] = OpCodes.Ldind_I1;
    //  mTypeHash[typeof(double)] = OpCodes.Ldind_R8;
    //  mTypeHash[typeof(float)] = OpCodes.Ldind_R4;
    //}

    ///// <summary>
    ///// Create an assembly that will provide the get and set methods.
    ///// </summary>
    //Assembly EmitAssembly()
    //{
    //  //
    //  // Create an assembly name
    //  //
    //  AssemblyName assemblyName = new AssemblyName();
    //  assemblyName.Name = "PropertyAccessorAssembly";
    //  //
    //  // Create a new assembly with one module
    //  //
    //  AssemblyBuilder newAssembly = Thread.GetDomain().DefineDynamicAssembly(assemblyName, AssemblyBuilderAccess.Run);
    //  ModuleBuilder newModule = newAssembly.DefineDynamicModule("Module");
    //  //
    //  // Define a public class named "Property" in the assembly.
    //  //
    //  TypeBuilder myType = newModule.DefineType("Property", TypeAttributes.Public);
    //  //
    //  // Mark the class as implementing IPropertyAccessor. 
    //  //
    //  myType.AddInterfaceImplementation(typeof(IPropertyAccessor));
    //  // Add a constructor
    //  ConstructorBuilder constructor = myType.DefineDefaultConstructor(MethodAttributes.Public);
    //  //
    //  // Define a method for the get operation. 
    //  //
    //  Type[] getParamTypes = new Type[] { typeof(object) };
    //  Type getReturnType = typeof(object);
    //  MethodBuilder getMethod = myType.DefineMethod("Get", MethodAttributes.Public | MethodAttributes.Virtual, getReturnType, getParamTypes);
    //  //
    //  // From the method, get an ILGenerator. This is used to
    //  // emit the IL that we want.
    //  //
    //  ILGenerator getIL = getMethod.GetILGenerator();

    //  //
    //  // Emit the IL. 
    //  //
    //  MethodInfo targetGetMethod = this.ObjectRepository.ObjectType.GetMethod("get_" + this.PropertyName);
    //  if (targetGetMethod != null)
    //  {
    //    getIL.DeclareLocal(typeof(object));
    //    getIL.Emit(OpCodes.Ldarg_1); //Load the first argument
    //    //(target object)
    //    //Cast to the source type
    //    getIL.Emit(OpCodes.Castclass, this.ObjectRepository.ObjectType);
    //    //Get the property value
    //    getIL.EmitCall(OpCodes.Call, targetGetMethod, null);
    //    if (targetGetMethod.ReturnType.IsValueType)
    //    {
    //      getIL.Emit(OpCodes.Box, targetGetMethod.ReturnType);
    //      //Box if necessary
    //    }
    //    getIL.Emit(OpCodes.Stloc_0); //Store it

    //    getIL.Emit(OpCodes.Ldloc_0);
    //  }
    //  else
    //  {
    //    getIL.ThrowException(typeof(MissingMethodException));
    //  }
    //  getIL.Emit(OpCodes.Ret);

    //  //
    //  // Define a method for the set operation.
    //  //
    //  Type[] setParamTypes = new Type[] { typeof(object), typeof(object) };
    //  Type setReturnType = null;
    //  MethodBuilder setMethod = myType.DefineMethod("Set", MethodAttributes.Public | MethodAttributes.Virtual, setReturnType, setParamTypes);
    //  //
    //  // From the method, get an ILGenerator. This is used to
    //  // emit the IL that we want.
    //  //
    //  ILGenerator setIL = setMethod.GetILGenerator();
    //  //
    //  // Emit the IL. 
    //  //
    //  MethodInfo targetSetMethod = this.ObjectRepository.ObjectType.GetMethod("set_" + this.PropertyName);
    //  if (targetSetMethod != null)
    //  {
    //    Type paramType = targetSetMethod.GetParameters()[0].ParameterType;
    //    setIL.DeclareLocal(paramType);
    //    setIL.Emit(OpCodes.Ldarg_1); //Load the first argument 
    //    //(target object)
    //    //Cast to the source type
    //    setIL.Emit(OpCodes.Castclass, this.ObjectRepository.ObjectType);
    //    setIL.Emit(OpCodes.Ldarg_2); //Load the second argument 
    //    //(value object)
    //    if (paramType.IsValueType)
    //    {
    //      setIL.Emit(OpCodes.Unbox, paramType); //Unbox it 
    //      if (mTypeHash[paramType] != null) //and load
    //      {
    //        OpCode load = (OpCode)mTypeHash[paramType];
    //        setIL.Emit(load);
    //      }
    //      else
    //      {
    //        setIL.Emit(OpCodes.Ldobj, paramType);
    //      }
    //    }
    //    else
    //    {
    //      setIL.Emit(OpCodes.Castclass, paramType); //Cast class
    //    }

    //    setIL.EmitCall(OpCodes.Callvirt, targetSetMethod, null); //Set the property value
    //  }
    //  else
    //  {
    //    setIL.ThrowException(typeof(MissingMethodException));
    //  }
    //  setIL.Emit(OpCodes.Ret);
    //  //
    //  // Load the type
    //  //
    //  myType.CreateType();
    //  return newAssembly;
    //}

    //#endregion

    #endregion
  }
}

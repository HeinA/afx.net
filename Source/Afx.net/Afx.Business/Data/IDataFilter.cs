﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.Data
{
  public interface IDataFilter
  {
    Type ObjectType { get; }
    string Property { get; }
    object Filter { get; }
    FilterType FilterType { get; }

    //bool IsCyclic { get; }
  }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.Data
{
  [Serializable]
  public class StatementException : Exception
  {
    protected StatementException(SerializationInfo info, StreamingContext context)
      :base(info, context)
    {
    }

    public StatementException(string message)
      : base(message)
    {
    }

    public StatementException(string message, Exception innerException)
      : base(message, innerException)
    {
    }
  }
}

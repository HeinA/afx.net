﻿using Afx.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.Data
{
  [DataContract(IsReference = true, Namespace = Namespace.Afx)]
  public class DataFilter<TObject> : IDataFilter
    where TObject : BusinessObject
  {
    public DataFilter(string property, FilterType filterType, object filter) 
    {
      mProperty = property;
      mFilterType = filterType;
      mFilter = filter;
    }

    Type IDataFilter.ObjectType
    {
      get { return typeof(TObject); }
    }

    [DataMember(Name = "Column", EmitDefaultValue = false)]
    string mProperty;
    public string Property
    {
      get { return mProperty; }
      private set { mProperty = value; }
    }

    [DataMember(Name = "Filter", EmitDefaultValue = false)]
    object mFilter;
    public object Filter
    {
      get { return mFilter; }
      private set { mFilter = value; }
    }

    [DataMember(Name = "FilterType", EmitDefaultValue = false)]
    FilterType mFilterType;
    public FilterType FilterType
    {
      get { return mFilterType; }
      private set { mFilterType = value; }
    }
  }

  [DataContract(IsReference = true, Namespace = Namespace.Afx)]
  class DataFilter : IDataFilter
  {
    public DataFilter(Type objectType, string property, FilterType filterType, object filter) 
    {
      ObjectType = objectType;
      mProperty = property;
      mFilterType = filterType;
      mFilter = filter;
    }

    [DataMember(Name = "ObjectType", EmitDefaultValue = false)]
    Type mObjectType;
    public Type ObjectType
    {
      get { return mObjectType; }
      private set { mObjectType = value; }
    }

    [DataMember(Name = "Column", EmitDefaultValue = false)]
    string mProperty;
    public string Property
    {
      get { return mProperty; }
      private set { mProperty = value; }
    }

    [DataMember(Name = "Filter", EmitDefaultValue = false)]
    object mFilter;
    public object Filter
    {
      get { return mFilter; }
      private set { mFilter = value; }
    }

    [DataMember(Name = "FilterType", EmitDefaultValue = false)]
    FilterType mFilterType;
    public FilterType FilterType
    {
      get { return mFilterType; }
      private set { mFilterType = value; }
    }
  }
}

﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.Data
{
  public class PersistanceEventManager<TObject> : IPersistanceEventManager
    where TObject : BusinessObject, IBusinessObject
  {
    public Type TargetType
    {
      get
      {
        return typeof(TObject);
      }
    }

    IPersistanceManager mPersistanceManager;
    public IPersistanceManager PersistanceManager
    {
      get { return mPersistanceManager; }
      internal set { mPersistanceManager = value; }
    }

    public virtual void BeforeDelete(TObject obj, PersistanceFlags flags)
    {
    }

    public virtual void AfterDelete(TObject obj, PersistanceFlags flags)
    {
    }

    public virtual bool BeforePersist(TObject obj, PersistanceFlags flags)
    {
      return true;
    }

    public virtual void AfterPersist(TObject obj, PersistanceFlags flags)
    {
    }

    #region IPersistanceEventManager

    void IPersistanceEventManager.BeforeDelete(IBusinessObject obj, PersistanceFlags flags)
    {
      BeforeDelete((TObject)obj, flags);
    }

    void IPersistanceEventManager.AfterDelete(IBusinessObject obj, PersistanceFlags flags)
    {
      AfterDelete((TObject)obj, flags);
    }

    bool IPersistanceEventManager.BeforePersist(IBusinessObject obj, PersistanceFlags flags)
    {
      return BeforePersist((TObject)obj, flags);
    }

    void IPersistanceEventManager.AfterPersist(IBusinessObject obj, PersistanceFlags flags)
    {
      AfterPersist((TObject)obj, flags);
    }

    IPersistanceManager IPersistanceEventManager.PersistanceManager
    {
      get { return PersistanceManager; }
      set { PersistanceManager = value; }
    }

    #endregion


    public DataSet ExecuteDataSet(IDbCommand cmd)
    {
      System.Data.DataSet ds = new System.Data.DataSet();
      ds.Locale = CultureInfo.InvariantCulture;
      using (IDataReader r = cmd.ExecuteReader())
      {
        ds.Load(r, LoadOption.OverwriteChanges, string.Empty);
        r.Close();
      }
      return ds;
    }

    protected void PrintCommand(IDbCommand cmd)
    {
      //LogHelper.LogSql(cmd);
    }

  }
}

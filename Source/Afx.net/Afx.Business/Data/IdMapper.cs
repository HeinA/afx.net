﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace Afx.Business.Data
{
  internal class IdMapper
  {
    IdMapper(IPersistanceManager pm)
    {
      mPersistanceManager = (IInternalPersistanceManager)pm;
    }

    internal void RefreshCachedIds()
    {
      lock (mLock)
      {
        mIdDictionary = new Dictionary<Type, Dictionary<Guid, int>>();
        mGuidDictionary = new Dictionary<Type, Dictionary<int, Guid>>();

        foreach (Type objectType in mPersistanceManager.BusinessClasses)
        {
          if (ObjectAttributeHelper.IsCached(objectType))
          {
            IInternalObjectRepository or = mPersistanceManager.GetRepository(objectType).RootRepository;
            LoadObjectTypeIds(objectType, or);
          }
        }
      }
    }

    object mLock1 = new object();
    void LoadObjectTypeIds(Type objectType, IInternalObjectRepository or)
    {
      lock (mLock1)
      {
        if (!mIdDictionary.ContainsKey(objectType)) mIdDictionary.Add(objectType, new Dictionary<Guid, int>());
        if (!mGuidDictionary.ContainsKey(objectType)) mGuidDictionary.Add(objectType, new Dictionary<int, Guid>());
      }
      Dictionary<Guid, int> guidDict = mIdDictionary[objectType];
      Dictionary<int, Guid> idDict = mGuidDictionary[objectType];

      using (IDbCommand cmd = mPersistanceManager.GetCommand())
      {
        cmd.CommandText = string.Format("SELECT {0}, {2} FROM {1}", or.GetFullIdColumn(), or.GetFullTableName(), or.GetFullGlobalIdentifierColumn());
        PrintCommand(cmd);
        foreach (DataRow dr in Execute(cmd).Tables[0].Rows)
        {
          int id = (int)dr[0];
          Guid gi = (Guid)dr[1];
          try
          {
            guidDict.Add(gi, id);
            idDict.Add(id, gi);
          }
          catch
          {
            throw;
          }
        }
      }
    }

    public DataSet Execute(IDbCommand cmd)
    {
      try
      {
        System.Data.DataSet ds = new System.Data.DataSet();
        ds.Locale = CultureInfo.InvariantCulture;
        using (IDataReader r = cmd.ExecuteReader())
        {
          ds.Load(r, LoadOption.OverwriteChanges, string.Empty);
          r.Close();
        }
        return ds;
      }
      catch
      {
        throw;
      }
    }

    public static void Create(IPersistanceManager pm)
    {
      mInstance = new IdMapper(pm);
    }

    IInternalPersistanceManager mPersistanceManager;
    object mLock = new object();
    Dictionary<Type, Dictionary<Guid, int>> mIdDictionary;
    Dictionary<Type, Dictionary<int, Guid>> mGuidDictionary;

    static IdMapper mInstance;
    public static IdMapper Instance
    {
      get { return mInstance; }
    }

    public int GetId(Guid globalIdentifier, Type objectType)
    {
      //lock (mLock)
      //{
      try
      {
        if (globalIdentifier == Guid.Empty) return 0;

        if (mIdDictionary.ContainsKey(objectType))
        {
          Dictionary<Guid, int> d = mIdDictionary[objectType];
          if (d.ContainsKey(globalIdentifier)) return d[globalIdentifier];
        }

        IInternalObjectRepository or = mPersistanceManager.GetRepository(objectType).RootRepository;
        using (IDbCommand cmd = mPersistanceManager.GetCommand())
        {
          cmd.CommandText = string.Format("SELECT {0} FROM {1} WITH (NOLOCK) WHERE {2}={3}", or.GetFullIdColumn(), or.GetFullTableName(), or.GetFullGlobalIdentifierColumn(), mPersistanceManager.AddParameter("guid", globalIdentifier, cmd));
          PrintCommand(cmd);
          object o = cmd.ExecuteScalar();
          if (o == null || o == DBNull.Value) return 0;
          return (int)o;
        }
      }
      catch
      {
        throw;
      }
      //}
    }

    public Guid GetIdentifier(int id, Type objectType)
    {
      //lock (mLock)
      //{
      if (id == 0) return Guid.Empty;

      if (mGuidDictionary.ContainsKey(objectType))
      {
        Dictionary<int, Guid> d = mGuidDictionary[objectType];
        if (d.ContainsKey(id)) return d[id];
      }

      IInternalObjectRepository or = mPersistanceManager.GetRepository(objectType).RootRepository;
      using (IDbCommand cmd = mPersistanceManager.GetCommand())
      {
        cmd.CommandText = string.Format("SELECT {0} FROM {1} WHERE {2}={3}", or.GetFullGlobalIdentifierColumn(), or.GetFullTableName(), or.GetFullIdColumn(), mPersistanceManager.AddParameter("id", id, cmd));
        PrintCommand(cmd);
        object o = cmd.ExecuteScalar();
        if (o == null || o == DBNull.Value) return Guid.Empty;
        return (Guid)o;
      }
      //}
    }


    protected void PrintCommand(IDbCommand cmd)
    {
      //LogHelper.LogSql(cmd);
    }
  }
}

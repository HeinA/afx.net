﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.Data
{
  /// <summary>
  /// The object is stored in a database table.
  /// </summary>
  [AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
  public sealed class PersistantObjectAttribute : Attribute
  {
    string mSchema;
    /// <summary>
    /// The database schema of the table.
    /// </summary>
    public string Schema
    {
      get { return mSchema; }
      set { mSchema = value; }
    }

    string mTableName;
    /// <summary>
    /// The database name of the table.
    /// Default is the name of class name.
    /// </summary>
    public string TableName
    {
      get { return mTableName; }
      set { mTableName = value; }
    }

    #region bool HasFlags

    bool mHasFlags = false;
    public bool HasFlags
    {
      get { return mHasFlags; }
      set { mHasFlags = value; }
    }

    #endregion

    string mIdColumn;
    /// <summary>
    /// The id column of the table
    /// Default is "Id"
    /// </summary>
    public string IdColumn
    {
      get { return mIdColumn; }
      set { mIdColumn = value; }
    }

    string mGlobalIdentifierColumn;
    /// <summary>
    /// The global identifier column of the table, if applicable.
    /// Default is "GlobalIdentifier".
    /// </summary>
    public string GlobalIdentifierColumn
    {
      get { return mGlobalIdentifierColumn; }
      set { mGlobalIdentifierColumn = value; }
    }

    string mOwnerColumn;
    /// <summary>
    /// The owner reference column of the table, if applicable.
    /// </summary>
    public string OwnerColumn
    {
      get { return mOwnerColumn; }
      set { mOwnerColumn = value; }
    }

    string mRowColumn;
    /// <summary>
    /// The row reference column of the TableObject
    /// </summary>
    public string RowColumn
    {
      get { return mRowColumn; }
      set { mRowColumn = value; }
    }

    string mColumnColumn;
    /// <summary>
    /// The column reference column of the TableObject
    /// </summary>
    public string ColumnColumn
    {
      get { return mColumnColumn; }
      set { mColumnColumn = value; }
    }

    bool mPersistToDerivedClass = false;
    /// <summary>
    /// The object will be stored in a descendant's table
    /// Default is false
    /// </summary>
    public bool PersistToDerivedClass
    {
      get { return mPersistToDerivedClass; }
      set { mPersistToDerivedClass = value; }
    }

    string mReferenceColumn;
    /// <summary>
    /// The reference column name of an AssociativeObject
    /// </summary>
    public string ReferenceColumn
    {
      get { return mReferenceColumn; }
      set { mReferenceColumn = value; }
    }

    #region bool IsReadOnly

    bool mIsReadOnly = false;
    public bool IsReadOnly
    {
      get { return mIsReadOnly; }
      set { mIsReadOnly = value; }
    }

    #endregion
  }
}

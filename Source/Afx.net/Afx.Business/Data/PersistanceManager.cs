﻿using Afx.Business;
using Afx.Business.Documents;
using Afx.Business.Security;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.Data
{
  public abstract class PersistanceManager : IInternalPersistanceManager
  {
    public PersistanceManager()
    {
      Container = new UnityContainer();
      Container.RegisterInstance<IPersistanceManager>(this);
      Container.RegisterType(typeof(ObjectRepository<>), new ContainerControlledLifetimeManager());

      RegisterEventManager<Document>(new DocumentEventManager());
      //RegisterEventManager<User>(new UserEventManager());
      //RegisterEventManager<UserSession>(new UserSessionEventManager());
      RegisterEventManager<BusinessObject>(new CacheBusinessObjectEventManager());
    }

    public ObjectRepository<T> GetRepository<T>() 
      where T : BusinessObject
    {
      return Container.Resolve<ObjectRepository<T>>();
    }

    public IEnumerable<PersistanceEventManager<T>> GetEventManagers<T>() 
      where T : BusinessObject
    {
      return Container.ResolveAll<PersistanceEventManager<T>>().Reverse();
    }

    public void RegisterEventManager<T>(PersistanceEventManager<T> manager)
      where T : BusinessObject
    {
      Container.RegisterInstance<PersistanceEventManager<T>>(Guid.NewGuid().ToString(), manager);
      manager.PersistanceManager = this;
    }

    public void RegisterEventManager(IPersistanceEventManager manager)
    {
      Container.RegisterInstance(manager.GetType().BaseType, Guid.NewGuid().ToString(), manager);
      manager.PersistanceManager = this;
    }


    public void RegisterDocumentStateEventManager<T>(DocumentStateEventManager<T> manager)
      where T : Document
    {
      Container.RegisterInstance<DocumentStateEventManager<T>>(Guid.NewGuid().ToString(), manager);
      manager.PersistanceManager = this;
    }

    public void RegisterDocumentStateEventManager(IDocumentStateEventManager manager)
    {
      Container.RegisterInstance(manager.GetType().BaseType, Guid.NewGuid().ToString(), manager);
      manager.PersistanceManager = this;
    }

    public void RegisterDocumentTransferManager(IDocumentTransferManager manager)
    {
      Container.RegisterInstance(typeof(IDocumentTransferManager), Guid.NewGuid().ToString(), manager);
      manager.PersistanceManager = this;
    }

    public void RegisterAssembly(Assembly assembly)
    {
      Queue<Type> queue = new Queue<Type>();

      foreach(var t in assembly.GetTypes().Where((t) => typeof(IBusinessObject).IsAssignableFrom(t) && t.IsClass
        && !(t.IsGenericType && t.GetGenericTypeDefinition() == typeof(BusinessObject<>))
        && !(t.IsGenericType && t.GetGenericTypeDefinition() == typeof(AssociativeObject<,>))
        //&& !(t.IsGenericType && t.GetGenericTypeDefinition() == typeof(TableObject<,,>))
        //&& !(t.IsGenericType && t.GetGenericTypeDefinition() == typeof(TableCell<,>))
        ))
      {
        BusinessClasses.Add(t);
        if (t.IsDefined(typeof(CacheAttribute)))
        {
          CacheAttribute ca = t.GetCustomAttributes<CacheAttribute>().First();
          if (ca.Priority >= 0)
          {
            if (ca.Priority > CachedClasses.Count) CachedClasses.Add(t);
            else CachedClasses.Insert(ca.Priority, t);
          }
          else queue.Enqueue(t); // CachedClasses.Add(t);
        }
      }

      while (queue.Count > 0)
      {
        CachedClasses.Add(queue.Dequeue());
      }
    }

    public void PreLoad()
    {
      foreach (Type t in BusinessClasses)
      {
        GetRepository(t);
      }
    }

    #region Protected

    protected virtual char IdentifierStart
    {
      get { return '"'; }
    }
    protected virtual char IdentifierEnd
    {
      get { return '"'; }
    }

    public string GetIdentifier(string name)
    {
      return string.Format("{0}{1}{2}", IdentifierStart, name, IdentifierEnd);
    }

    public IDbCommand GetCommand()
    {
      return GetCommand(DbScope.ConnectionName);
    }

    public abstract IDbCommand GetCommand(string dbName);

    public abstract string AddParameter(object name, object value, IDbCommand command);
    public abstract string AddOutputParameter(object name, IDbCommand command);

    public abstract IDbCommand GetSetFlagCommand(int id, string schema, string tableName, string flag);
    public abstract IDbCommand GetUnFlagCommand(int id, string schema, string tableName, string flag);
    public abstract IDbCommand GetQueryFlagsCommand(int id, string schema, string tableName);

    protected virtual object CastToDatabaseType(object value)
    {
      return value;
    }

    protected virtual object CastToObjectType(object value)
    {
      return value;
    }

    protected virtual string GetFilterText(FilterType filter)
    {
      switch (filter)
      {
        case FilterType.Equals:
          return "=";

        case FilterType.GreaterThan:
          return ">";

        case FilterType.GreaterThanEqual:
          return ">=";

        case FilterType.IsNotNull:
          return " IS NOT NULL";

        case FilterType.IsNull:
          return " IS NULL";

        case FilterType.LessThan:
          return "<";

        case FilterType.LessThanEqual:
          return "<=";

        case FilterType.Like:
          return " LIKE ";

        case FilterType.NotEquals:
          return "<>";

        case FilterType.In:
          return "IN";

        case FilterType.NotIn:
          return "NOT IN";
      }

      throw new StatementException("Invalid Filter.");
    }

    protected virtual string GetSortText(SortDirection sort)
    {
      switch (sort)
      {
        case SortDirection.Ascending:
          return "ASC";

        case SortDirection.Descending:
          return "DESC";
      }

      throw new StatementException("Invalid sort direction.");
    }

    #endregion

    #region Private

    IUnityContainer mContainer;
    IUnityContainer Container
    {
      get { return mContainer; }
      set { mContainer = value; }
    }

    Collection<Type> mBusinessClasses;
    Collection<Type> BusinessClasses
    {
      get { return mBusinessClasses ?? (mBusinessClasses = new Collection<Type>()); }
    }

    Collection<Type> mCachedClasses;
    Collection<Type> CachedClasses
    {
      get { return mCachedClasses ?? (mCachedClasses = new Collection<Type>()); }
    }

    #endregion

    IInternalObjectRepository GetRepository(Type objectType)
    {
      Type t = typeof(ObjectRepository<>);
      t = t.MakeGenericType(new Type[] { objectType });
      return (IInternalObjectRepository)Container.Resolve(t);
    }

    IEnumerable<IDocumentStateEventManager> GetDocumentStateEventManagers(Type objectType)
    {
      Type t = typeof(DocumentStateEventManager<>);
      t = t.MakeGenericType(new Type[] { objectType });
      return Container.ResolveAll(t).Cast<IDocumentStateEventManager>();
    }

    IEnumerable<IDocumentTransferManager> GetDocumentTransferManagers(Type objectType)
    {
      return Container.ResolveAll<IDocumentTransferManager>().Where(tm1 => tm1.DocumentType == objectType);
    }

    IEnumerable<IPersistanceEventManager> GetEventManagers(Type objectType)
    {
      Type t = typeof(PersistanceEventManager<>);
      t = t.MakeGenericType(new Type[] { objectType });
      return Container.ResolveAll(t).Cast<IPersistanceEventManager>().Reverse();
    }

    #region IInternalPersistanceManager

    IEnumerable<IDocumentStateEventManager> IInternalPersistanceManager.GetDocumentStateEventManagers(Type objectType)
    {
      return GetDocumentStateEventManagers(objectType);
    }

    IEnumerable<IDocumentTransferManager> IInternalPersistanceManager.GetDocumentTransferManagers(Type objectType)
    {
      return GetDocumentTransferManagers(objectType);
    }

    IEnumerable<IPersistanceEventManager> IInternalPersistanceManager.GetEventManagers(Type objectType)
    {
      return GetEventManagers(objectType);
    }

    IInternalObjectRepository IInternalPersistanceManager.GetRepository(Type objectType)
    {
      return GetRepository(objectType);
    }

    //string IInternalPersistanceManager.GetIdentifier(string name)
    //{
    //  return GetIdentifier(name);
    //}

    IEnumerable<Type> IInternalPersistanceManager.BusinessClasses
    {
      get { return BusinessClasses; }
    }

    IEnumerable<Type> IInternalPersistanceManager.CachedClasses
    {
      get { return CachedClasses; }
    }

    //IDbCommand IInternalPersistanceManager.GetCommand()
    //{
    //  return GetCommand();
    //}

    object IInternalPersistanceManager.CastToDatabaseType(object value)
    {
      return CastToDatabaseType(value);
    }

    object IInternalPersistanceManager.CastToObjectType(object value)
    {
      return CastToObjectType(value);
    }

    string IInternalPersistanceManager.GetFilterText(FilterType filter)
    {
      return GetFilterText(filter);
    }

    string IInternalPersistanceManager.GetSortText(SortDirection sort)
    {
      return GetSortText(sort);
    }

    //string IInternalPersistanceManager.AddParameter(object name, object value, IDbCommand command)
    //{
    //  return AddParameter(name, value, command);
    //}

    #endregion
  }
}

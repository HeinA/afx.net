﻿using Afx.Business.Activities;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.Data
{
  class QueryContext
  {
    [InjectionConstructor]
    public QueryContext(IPersistanceManager persistanceManager)
    {
      PersistanceManager = (IInternalPersistanceManager)persistanceManager;
    }

    IInternalPersistanceManager mPersistanceManager;
    IInternalPersistanceManager PersistanceManager
    {
      get { return mPersistanceManager; }
      set { mPersistanceManager = value; }
    }

    Collection<TypedDataRow> mDataSetCollection;
    Collection<TypedDataRow> DataSetCollection
    {
      get { return mDataSetCollection ?? (mDataSetCollection = new Collection<TypedDataRow>()); }
    }

    public IEnumerable<TypedDataRow> DataSet
    {
      get { return DataSetCollection; }
    }

    public IEnumerable<TypedDataRow> GetRootDataSet(Type objectType)
    {
      try
      {
        //Check only root objects (circular dependencies)
        IInternalObjectRepository or = PersistanceManager.GetRepository(objectType);
        if (or.IsOwned) return DataSetCollection.Where((r) => objectType.IsAssignableFrom(r.ObjectType) && DataSet.Count((r1) => objectType.IsAssignableFrom(r1.ObjectType) && r1.Id == (int)(r.Columns[or.OwnerColumn] ?? 0)) == 0);
        else return DataSet.Where((r) => objectType.IsAssignableFrom(r.ObjectType));
      }
      catch
      {
        throw;
      }
    }

    public IEnumerable<TypedDataRow> GetDataSet(Type objectType)
    {
      try
      {
        return DataSet.Where((r) => objectType.IsAssignableFrom(r.ObjectType));
      }
      catch
      {
        throw;
      }
    }

    public int Count(Type objectType)
    {
      return GetRootDataSet(objectType).Count((r) => true);
    }

    public Collection<int> GetReferences(Type objectType, PersistantProperty referenceProperty)
    {
      Collection<int> col = new Collection<int>();
      foreach (TypedDataRow tdr in DataSet.Where((r) => objectType.IsAssignableFrom(r.ObjectType)))
      {
        try
        {
          if (tdr[referenceProperty.ColumnName] != null) col.Add((int)tdr[referenceProperty.ColumnName]);
        }
        catch
        {
          throw;
        }
      }
      return col;
    }

    public Collection<int> GetIds(Type objectType)
    {
      Collection<int> col = new Collection<int>();
      foreach (TypedDataRow tdr in GetDataSet(objectType))
      {
        col.Add(tdr.Id);
      }
      return col;
    }

    public IEnumerable<TypedDataRow> GetCollection(Type objectType, int ownerId)
    {
      IInternalObjectRepository or = PersistanceManager.GetRepository(objectType);
      List<TypedDataRow> lst = null;
      try
      {
        lst = DataSet.Where((r) => objectType.IsAssignableFrom(r.ObjectType)).ToList();
        return DataSet.Where((r) => objectType.IsAssignableFrom(r.ObjectType) && ownerId == (int)(r.Columns[or.OwnerColumn] ?? 0)).ToList();
      }
      catch
      {
        throw;
      }
    }

    public int Query(Type objectType, QueryStatement qs)
    {
      try
      {
        IInternalObjectRepository or = PersistanceManager.GetRepository(objectType);

        if (or.DerivedClassCount > 0)
        {
          Collection<int> typeIds = GetIds(objectType);
          if (typeIds.Count > 0)
          {
            qs.ApplyFilter(new DataFilter(objectType, BusinessObject.IdProperty, FilterType.NotIn, typeIds), or.GetProperty(BusinessObject.IdProperty));
          }
        }

        try
        {
          using (System.Data.DataSet ds = qs.Execute(or))
          {
            foreach (System.Data.DataRow dr in ds.Tables[0].Rows)
            {
              TypedDataRow tdr = new TypedDataRow(this, objectType, or.IdColumn, or.GlobalIdentifierColumn, dr);
              TypedDataRow tdr1 = DataSet.Where((r) => objectType == r.ObjectType).FirstOrDefault((r) => r.Id == tdr.Id);
              if (tdr1 == null) // Add only once.
              {
                DataSetCollection.Add(tdr);
              }
            }
            return ds.Tables[0].Rows.Count;
          }
        }
        catch
        {
          throw;
        }
      }
      catch (Exception ex)
      {
        throw new QueryException(string.Format("Could not query object {0}.", TypeHelper.GetTypeName(objectType)), ex);
      }
    }
  }
}

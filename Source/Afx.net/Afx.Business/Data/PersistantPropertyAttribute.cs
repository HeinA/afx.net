﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.Data
{
  /// <summary>
  /// The property is involved in database operations.
  /// </summary>
  [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
  public sealed class PersistantPropertyAttribute : Attribute
  {
    string mName;
    /// <summary>
    /// The database name of the column.
    /// Default is the property name.
    /// </summary>
    public string Name
    {
      get { return mName; }
      set { mName = value; }
    }

    bool mRead = true;
    /// <summary>
    /// Coulumn must be selected during read operations.
    /// Default is true.
    /// </summary>
    public bool Read
    {
      get { return mRead; }
      set { mRead = value; }
    }

    #region bool ForceLoad

    public const string ForceLoadProperty = "ForceLoad";
    bool mForceLoad;
    public bool ForceLoad
    {
      get { return mForceLoad; }
      set { mForceLoad = value; }
    }

    #endregion

    bool mWrite = true;
    /// <summary>
    /// Column must be written during write operations.
    /// Default is true.
    /// </summary>
    public bool Write
    {
      get { return mWrite; }
      set { mWrite = value; }
    }

    int mSortOrder = -1;
    /// <summary>
    /// The sorting precedence of the column. (-1 to not sort by this column)
    /// Default is -1.
    /// </summary>
    public int SortOrder
    {
      get { return mSortOrder; }
      set { mSortOrder = value; }
    }

    SortDirection mSortDirection = SortDirection.Ascending;
    /// <summary>
    /// The direction of the sort.
    /// Default is Ascending.
    /// </summary>
    public SortDirection SortDirection
    {
      get { return mSortDirection; }
      set { mSortDirection = value; }
    }

    bool mIgnoreNull = false;
    /// <summary>
    /// Null values will be ignored during writing operations.
    /// Default is false.
    /// </summary>
    public bool IgnoreNull
    {
      get { return mIgnoreNull; }
      set { mIgnoreNull = value; }
    }

    /// <summary>
    /// The property is only persisted after the entire object tree is persisted
    /// </summary>
    bool mPostProcess = false;
    public bool PostProcess
    {
      get { return mPostProcess; }
      set { mPostProcess = value; }
    }

    bool mIgnoreConcurrency = false;
    public bool IgnoreConcurrency
    {
      get { return mIgnoreConcurrency; }
      set { mIgnoreConcurrency = value; }
    }

    #region bool ImportOverwrite

    bool mImportOverwrite = true;
    public bool ImportOverwrite
    {
      get { return mImportOverwrite; }
      set { mImportOverwrite = value; }
    }

    #endregion

    #region bool IsCached

    public const string IsCachedProperty = "IsCached";
    bool mIsCached;
    public bool IsCached
    {
      get { return mIsCached; }
      set { mIsCached = value; }
    }

    #endregion
  }
}

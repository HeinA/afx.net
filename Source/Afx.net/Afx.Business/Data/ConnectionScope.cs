﻿// Copyright (c) 2006, Microsoft Corporation
//
//  Author: Alazel Acheson

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Data.SqlClient;

namespace Afx.Business.Data
{
  // Allows almost-automated re-use of connections across multiple call levels
  //  while still controlling connection lifetimes.  Multiple connections are supported within a single scope.
  // To use:
  //  Create a new connection scope object in a using statement at the level within which you 
  //      want to scope connections.
  //  Use Current.AddConnection() and Current.GetConnection() to store/retrieve specific connections based on your
  //      own keys.
  //  Simpler alternative: Use Current.GetOpenConnection(factory, connection string) where you need to use the connection
  //
  // Example of simple case:
  //  void TopLevel() {
  //      using (DbConnectionScope scope = new DbConnectionScope()) {
  //          // Code that eventually calls LowerLevel a couple of times.
  //          // The first time LowerLevel is called, it will allocate and open the connection
  //          // Subsequent calls will use the already-opened connection, INCLUDING running in the same 
  //          //   System.Transactions transaction without using DTC (assuming only one connection string)!
  //      }
  //  }
  //
  //  void LowerLevel() {
  //      string connectionString = <...get connection string from config or somewhere...>;
  //      SqlCommand cmd = new SqlCommand("Some TSQL code");
  //      cmd.Connection = (SqlConnection) DbConnectionScope.Current.GetOpenConnection(SqlClientFactory.Instance, connectionString);
  //      ... finish setting up command and execute it
  //  }

  /// <summary>
  /// Class to assist in managing connection lifetimes inside scopes on a particular thread.
  /// </summary>
  sealed public class ConnectionScope : IDisposable
  {
    #region class fields
    [ThreadStatic]
    private static ConnectionScope mCurrentScope = null;      // Scope that is currently active on this thread
    private static Object mNullKey = new Object();   // used to allow null as a key
    #endregion

    #region instance fields
    private ConnectionScope mPriorScope;    // previous scope in stack of scopes on this thread
    private Dictionary<object, DbConnection> mConnections;   // set of connections contained by this scope.

    #endregion

    #region public class methods and properties
    /// <summary>
    /// Obtain the currently active connection scope
    /// </summary>
    public static ConnectionScope Current
    {
      get
      {
        return mCurrentScope;
      }
    }
    #endregion

    #region public instance methods and properties
    /// <summary>
    /// Constructor
    /// </summary>
    public ConnectionScope()
    {
      // Devnote:  Order of initial assignment is important in cases of failure!
      //  _priorScope first makes sure we know who we need to restore
      //  _connections second, to make sure we no-op dispose until we're as close to
      //      correct setup as possible
      //  __currentScope last, to make sure the thread static only holds validly set up objects
      mPriorScope = mCurrentScope;
      mConnections = new Dictionary<object, DbConnection>();
      mCurrentScope = this;
    }

    /// <summary>
    /// Convenience constructor to add an initial connection
    /// </summary>
    /// <param name="key">Key to associate with connection</param>
    /// <param name="connection">Connection to add</param>
    public ConnectionScope(object key, DbConnection connection)
      : this()
    {
      AddConnection(key, connection);
    }

    /// <summary>
    /// Add a connection and associate it with the given key
    /// </summary>
    /// <param name="key">Key to associate with the connection</param>
    /// <param name="connection">Connection to add</param>
    public void AddConnection(object key, DbConnection connection)
    {
      CheckDisposed();
      if (null == key)
      {
        key = mNullKey;
      }
      mConnections[key] = connection;
    }

    /// <summary>
    /// Check to see if there is a connection associated with this key
    /// </summary>
    /// <param name="key">Key to use for lookup</param>
    /// <returns>true if there is a connection, false otherwise</returns>
    public bool ContainsKey(object key)
    {
      CheckDisposed();
      return mConnections.ContainsKey(key);
    }

    /// <summary>
    /// Shut down this instance.  Disposes all connections it holds and restores the prior scope.
    /// </summary>
    public void Dispose()
    {
      if (!IsDisposed)
      {
        // Firstly, remove ourselves from the stack (but, only if we are the one on the stack)
        //  Note: Thread-local _currentScope, and requirement that scopes not be disposed on other threads
        //      means we can get away with not locking.
        if (mCurrentScope == this)
        {
          // In case the user called dispose out of order, skip up the chain until we find
          //  an undisposed scope.
          ConnectionScope prior = mPriorScope;
          while (null != prior && prior.IsDisposed)
          {
            prior = prior.mPriorScope;
          }
          mCurrentScope = prior;
        }

        // secondly, make sure our internal state is set to "Disposed"
        IDictionary<object, DbConnection> connections = mConnections;
        mConnections = null;

        // Lastly, clean up the connections we own
        foreach (DbConnection connection in connections.Values)
        {
          connection.Dispose();
        }
      }
    }

    /// <summary>
    /// Get the connection associated with this key. Throws if there is no entry for the key.
    /// </summary>
    /// <param name="key">Key to use for lookup</param>
    /// <returns>Associated connection</returns>
    public DbConnection GetConnection(object key)
    {
      CheckDisposed();

      // allow null-ref as key
      if (null == key)
      {
        key = mNullKey;
      }

      return mConnections[key];
    }

    /// <summary>
    /// This method gets the connection using the connection string as a key.  If no connection is
    /// associated with the string, the connection factory is used to create the connection.
    /// Finally, if the resulting connection is in the closed state, it is opened.
    /// </summary>
    /// <param name="connectionString">Configuration onnection string name to use</param>
    /// <returns>Connection in open state</returns>
    public SqlConnection SqlConnection(string dbName)
    {
      return (SqlConnection)GetOpenConnection(SqlClientFactory.Instance, dbName);
    }

    /// <summary>
    /// This method gets the connection using the connection string name as a key.  If no connection is
    /// associated with the string, the connection factory is used to create the connection.
    /// Finally, if the resulting connection is in the closed state, it is opened.
    /// </summary>
    /// <param name="factory">Factory to use to create connection if it is not already present</param>
    /// <param name="connectionString">Configuration onnection string name to use</param>
    /// <returns>Connection in open state</returns>
    DbConnection GetOpenConnection(DbProviderFactory factory, string dbname)
    {
      string connectionString = ConfigurationManager.ConnectionStrings[dbname].ConnectionString;

      CheckDisposed();
      object key;

      // allow null-ref as key
      if (null == connectionString)
      {
        key = mNullKey;
      }
      else
      {
        key = connectionString;
      }

      // go get the connection
      DbConnection result;
      if (!mConnections.TryGetValue(key, out result))
      {
        // didn't find it, so create it.
        result = factory.CreateConnection();
        result.ConnectionString = connectionString;
        mConnections[key] = result;
      }

      // however we got it, open it if it's closed.
      //  note: don't open unless state is unambiguous that it's ok to open
      if (ConnectionState.Closed == result.State)
      {
        result.Open();
      }

      return result;
    }
    #endregion

    #region private methods and properties
    /// <summary>
    /// Was this instance previously disposed?
    /// </summary>
    private bool IsDisposed
    {
      get
      {
        return null == mConnections;
      }
    }

    /// <summary>
    /// Handle calling API function after instance has been disposed
    /// </summary>
    private void CheckDisposed()
    {
      if (IsDisposed)
      {
        throw new ObjectDisposedException(Afx.Business.Properties.Resources.ConnectionScopeDisposed);
      }
    }

    #endregion
  }
}

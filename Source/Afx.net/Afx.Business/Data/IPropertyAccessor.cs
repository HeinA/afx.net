﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.Data
{
  /// <summary>
  /// Used by the emitted property accessors
  /// </summary>
  public interface IPropertyAccessor
  {
    object Get(object target);
    void Set(object target, object value);
  }
}

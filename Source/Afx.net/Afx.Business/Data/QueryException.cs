﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.Data
{
  [Serializable]
  public class QueryException : Exception
  {
    protected QueryException(SerializationInfo info, StreamingContext context)
      :base(info, context)
    {
    }

    public QueryException(string message)
      : base(message)
    {
    }

    public QueryException(string message, Exception innerException)
      : base(message, innerException)
    {
    }  
  }
}

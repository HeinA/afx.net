﻿using Afx.Business.Constants;
using Afx.Business.ServiceModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.Data
{
  [DataContract(Namespace = Namespaces.Afx)]
  public class UniqueIndexViolationFault : AfxFault
  {
  }
}

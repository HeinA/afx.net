﻿using Afx.Business.Security;
using Afx.Business.Service;
using Afx.Business.ServiceModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.Data
{
  public class CacheBusinessObjectEventManager : PersistanceEventManager<BusinessObject>
  {
    public override void AfterPersist(BusinessObject obj, PersistanceFlags flags)
    {
      if (((flags & PersistanceFlags.Import) != PersistanceFlags.Import) && ObjectAttributeHelper.MustReplicate(obj.GetType()))
      {
        CachedObjectReplicationExtension.AddObject(obj);
      }

      if (typeof(Setting).IsAssignableFrom(obj.GetType()))
      {
        ObjectReplicationExtension.Current.ObjectQueue.Enqueue(obj);
      }

      base.AfterPersist(obj, flags);
    }

    public override void AfterDelete(BusinessObject obj, PersistanceFlags flags)
    {
      if ((flags & PersistanceFlags.Import) != PersistanceFlags.Import && ObjectAttributeHelper.MustReplicate(obj.GetType()))
      {
        CachedObjectReplicationExtension.AddObject(obj);
      }

      base.AfterDelete(obj, flags);
    }
  }
}

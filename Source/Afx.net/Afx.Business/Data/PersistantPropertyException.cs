﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.Data
{
  [Serializable]
  public class PersistantPropertyException : Exception
  {
    protected PersistantPropertyException(SerializationInfo info, StreamingContext context)
      :base(info, context)
    {
    }

    public PersistantPropertyException(string propertyName, string message)
      : base(string.Format("{0}: {1}", propertyName, message))
    {
    }

    public PersistantPropertyException(string propertyName, string message, Exception innerException)
      : base(string.Format("{0}: {1}", propertyName, message), innerException)
    {
    }  
  }
}

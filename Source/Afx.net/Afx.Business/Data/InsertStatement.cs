﻿using Afx.Business.Security;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.Data
{
  class InsertStatement : StatementBase
  {
    public InsertStatement(IInternalObjectRepository objectRepository, PersistanceFlags flags)
      : base(objectRepository.PersistanceManager)
    {
      ObjectRepository = objectRepository;
      mFlags = flags;
    }

    IInternalObjectRepository mObjectRepository;
    IInternalObjectRepository ObjectRepository
    {
      get { return mObjectRepository; }
      set { mObjectRepository = value; }
    }

    PersistanceFlags mFlags = PersistanceFlags.None;
    public PersistanceFlags Flags
    {
      get { return mFlags; }
    }

    string mIdColumn;
    string IdColumn
    {
      get { return mIdColumn; }
      set { mIdColumn = value; }
    }

    string mGlobalIdentifierColumn;
    string GlobalIdentifierColumn
    {
      get { return mGlobalIdentifierColumn; }
      set { mGlobalIdentifierColumn = value; }
    }

    Guid mGlobalIdentifier;
    Guid GlobalIdentifier
    {
      get { return mGlobalIdentifier; }
      set { mGlobalIdentifier = value; }
    }

    Collection<string> mInsertColumns = new Collection<string>();
    public IEnumerable<string> InsertColumns
    {
      get { return mInsertColumns; }
    }

    public int Count
    {
      get { return mInsertColumns.Count; }
    }

    Collection<string> mInsertValues = new Collection<string>();
    public IEnumerable<string> InsertValues
    {
      get { return mInsertValues; }
    }

    public void AddProperty(PersistantProperty pp, BusinessObject obj, Stack<BusinessObject> persisted)
    {
      if (typeof(BusinessObject).IsAssignableFrom(pp.PropertyType))
      {
        BusinessObject bo = (BusinessObject)pp.GetValue(obj);
        if (bo == null)
        {
          //if (Debugger.IsAttached) Debugger.Break();
          return;
        }

        int refId = IdMapper.Instance.GetId(bo.GlobalIdentifier, bo.GetType());
        if (refId == 0 && !pp.PostProcess)
        {
          IInternalObjectRepository or = PersistanceManager.GetRepository(bo.GetType());
          if (bo.IsOwnedBy(ObjectRepository.ObjectType))
          {
            bo.SetReferenceValue(obj.GlobalIdentifier, BusinessObject.OwnerProperty);
          }
          or.Persist(bo, persisted, Flags);
          refId = bo.Id;
        }

        if (refId > 0)
        {
          AddColumnValue(pp, refId); //bo.Id);
        }
      }
      else
      {
        object value = pp.GetValue(obj);
        if (pp.IsIdColumn)
        {
          IdColumn = pp.GetColumnName();
        }
        else AddColumnValue(pp, value);

        if (pp.IsGlobalIdentifierColumn)
        {
          GlobalIdentifierColumn = pp.GetColumnName();
          GlobalIdentifier = (Guid)pp.GetValue(obj);
        }
      }
    }

    void AddColumnValue(PersistantProperty pp, object value)
    {
      if (value != null || !pp.IgnoreNull)
      {
        AddColumnValue(pp.GetColumnName(), value);
      }
    }

    void AddColumnValue(string columnName, object value)
    {
      mInsertColumns.Add(columnName);
      mInsertValues.Add(PersistanceManager.AddParameter(ParameterCount, value == null ? DBNull.Value : value, Command));
    }

    public int Execute(IBusinessObject obj)
    {
      //if (mInsertColumns.Count == 0) throw new QueryException("There are no columns to insert.");

      try
      {
        if (ObjectRepository.BaseRepository != null)
        {
          AddColumnValue(ObjectRepository.GetIdColumn(), obj.Id);
        }

        string query = string.Format("INSERT INTO {0} WITH (ROWLOCK) ({1}) VALUES ({2})", ObjectRepository.GetFullTableName(), string.Join(", ", mInsertColumns), string.Join(", ", mInsertValues));
        Command.CommandText = query;
        PrintCommand();
        int rows = 0;
        try
        {
          rows = Command.ExecuteNonQuery();
        }
        catch
        {
          throw;
        }
        if (rows == 0) throw new QueryException("Statement failed to insert any rows.");

        if (ObjectRepository.BaseRepository == null)
        {
          NewCommand();
          query = string.Format("SELECT {0} FROM {1} WITH (NOLOCK) WHERE {2} = {3}", IdColumn, ObjectRepository.GetFullTableName(), GlobalIdentifierColumn, ObjectRepository.PersistanceManager.AddParameter(ParameterCount, GlobalIdentifier, Command));
          Command.CommandText = query;
          PrintCommand();
          try
          {
            return (int)Command.ExecuteScalar();
          }
          catch
          {
            throw;
          }
        }
        else return obj.Id;
      }
      catch (SqlException ex)
      {
        switch (ex.ErrorCode)
        {
          case -2146232060:
            if (ex.Message.Contains("insert duplicate key"))
            {
              throw new UniqueIndexViolationException("Primary key constraint violation", obj.GetType(), ex);
            }
            else
            {
              throw ex;
            }

          default:
            throw ex;
        }
      }
      //}
      //catch (QueryException)
      //{
      //  throw;
      //}
      //catch (Exception ex)
      //{
      //  throw new QueryException("Could not execute the query.", ex);
      //}
    }
  }
}

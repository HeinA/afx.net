﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.Data
{
  public class DatabaseScope : IDisposable
  {
    public DatabaseScope()
    {
      mPreviousScope = DatabaseScope.Current;
      DbName = "Local";
    }

    public DatabaseScope(string dbName)
    {
      mPreviousScope = DatabaseScope.Current;
      DbName = dbName;
    }

    #region string DbName

    string mDbName;
    public string DbName
    {
      get { return mDbName; }
      private set { mDbName = value; }
    }

    #endregion

    DatabaseScope mPreviousScope;

    [ThreadStatic]
    static DatabaseScope mCurrentScope;
    public static DatabaseScope Current
    {
      get { return DatabaseScope.mCurrentScope; }
      private set { DatabaseScope.mCurrentScope = value; }
    }

    public void Dispose()
    {
      Dispose(true);
      GC.SuppressFinalize(this);
    }

    void Dispose(bool disposing)
    {
      if (disposing)
      {
        DatabaseScope.Current = mPreviousScope;
      }
    }
  }
}

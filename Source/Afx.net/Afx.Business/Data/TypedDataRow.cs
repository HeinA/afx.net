﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.Data
{
  class TypedDataRow
  {
    public TypedDataRow(QueryContext context, Type objectType, string idColumn, string globalIdentifierColumn, DataRow dataRow)
    {
      Context = context;
      ObjectType = objectType;
      Id = (int)dataRow[idColumn];
      GlobalIdentifier = (Guid)dataRow[globalIdentifierColumn];

      foreach (DataColumn column in dataRow.Table.Columns)
      {
        object value = dataRow[column.ColumnName];
        Columns.Add(column.ColumnName, value == DBNull.Value ? null : value);
      }
    }

    public object this[string columnName]
    {
      get { return Columns[columnName]; }
    }

    public QueryContext Context { get; private set; }
    public Type ObjectType { get; private set; }
    public int Id { get; private set; }
    public Guid GlobalIdentifier { get; private set; }

    Dictionary<string, object> mColumns;
    public Dictionary<string, object> Columns
    {
      get { return mColumns ?? (mColumns = new Dictionary<string,object>()); }
    }
  }
}

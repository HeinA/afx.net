﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.Data
{
  abstract class StatementBase
  {
    protected StatementBase(IInternalPersistanceManager persistanceManager)
    {
      PersistanceManager = persistanceManager;
    }

    IInternalPersistanceManager mPersistanceManager;
    internal IInternalPersistanceManager PersistanceManager
    {
      get { return mPersistanceManager; }
      private set { mPersistanceManager = value; }
    }

    IDbCommand mCommand;
    internal IDbCommand Command
    {
      get { return mCommand ?? (mCommand = PersistanceManager.GetCommand()); }
    }

    int mParameterCount = 0;
    protected int ParameterCount
    {
      get { return ++mParameterCount; }
      private set { mParameterCount = value; }
    }

    protected void NewCommand()
    {
      ParameterCount = 0;
      mCommand = null;
    }

    protected string AddParameter(object value)
    {
      return PersistanceManager.AddParameter(ParameterCount, value, Command);
    }

    protected void PrintCommand()
    {
      //LogHelper.LogSql(Command);
#if SQLDEBUG
      PrintDebugCommand();
#endif
    }

    protected void PrintDebugCommand()
    {
      string output = Command.CommandText;
      foreach (SqlParameter p in Command.Parameters)
      {
        output = output.Replace(p.ParameterName, p.Value.ToString());
      }
      Debug.WriteLine(output);
    }
  }
}

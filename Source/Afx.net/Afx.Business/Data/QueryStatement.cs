﻿using Afx.Business.Security;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.Data
{
  class QueryStatement : StatementBase
  {
    public QueryStatement(IInternalPersistanceManager persistanceManager)
      : base(persistanceManager)
    {
    }

    Collection<string> mQueryColumns;
    public Collection<string> QueryColumns
    {
      get { return mQueryColumns ?? (mQueryColumns = new Collection<string>()); }
    }

    Collection<string> mSortColumns;
    public Collection<string> SortColumns
    {
      get { return mSortColumns ?? (mSortColumns = new Collection<string>()); }
    }

    Collection<string> mJoinClauses;
    Collection<string> JoinClauses
    {
      get { return mJoinClauses ?? (mJoinClauses = new Collection<string>()); }
    }

    Collection<Type> mJoinedRepositories;
    Collection<Type> JoinedRepositories
    {
      get { return mJoinedRepositories ?? (mJoinedRepositories = new Collection<Type>()); }
    }

    Collection<string> mFilters;
    Collection<string> Filters
    {
      get { return mFilters ?? (mFilters = new Collection<string>()); }
    }

    public int AppliedFilters
    {
      get { return Filters.Count; }
    }

    string mTable;
    public string Table
    {
      get { return mTable; }
      set { mTable = value; }
    }

    public void AddQueryColumns(IInternalObjectRepository repository)
    {
      foreach (var pp in repository.ReadProperties)
      {
        QueryColumns.Add(pp.GetFullColumnName());
      }
    }

    public void AddSortColumns(IInternalObjectRepository repository)
    {
      foreach (var pp in repository.ReadProperties.Where((p) => p.SortOrder >= 0))
      {
        SortColumns.Add(string.Format("{0} {1}", pp.GetFullColumnName(), PersistanceManager.GetSortText(pp.SortDirection)));
      }
    }

    public void AddBaseJoinClause(IInternalObjectRepository baseRepository, IInternalObjectRepository repository)
    {
      if (JoinedRepositories.Contains(baseRepository.ObjectType)) return;
      JoinedRepositories.Add(baseRepository.ObjectType);
      string clause = string.Format("INNER JOIN {0} ON {1}={2}", baseRepository.GetFullTableName(), baseRepository.GetFullIdColumn(), repository.GetFullIdColumn());

      if (baseRepository.NamespaceProperty != null)
      {
        AppendNamespaceFilters(baseRepository);
      }

      JoinClauses.Add(clause);
    }

    void AppendNamespaceFilters(IInternalObjectRepository repo)
    {
      Collection<string> namespaces = new Collection<string>();
      foreach (INamespace ns in ExtensibilityManager.CompositionContainer.GetExportedValues<INamespace>())
      {
        namespaces.Add(ns.GetUri());
      }
      Collection<IDataFilter> filters = new Collection<IDataFilter>();
      filters.Add(new DataFilter(repo.ObjectType, string.Empty, FilterType.In, namespaces));
      //filters.Add(new DataFilter(repo.ObjectType, string.Empty, FilterType.IsNull, null));
      ApplyFilters(filters, repo.NamespaceProperty);
    }

    public void AddFilters(IInternalObjectRepository repository, ICollection<IDataFilter> conditions)
    {
      foreach (var pp in repository.Properties)
      {
        foreach (IDataFilter cnd in conditions)
        {
          if (repository.ApplyFilter(cnd, pp))
          {
            ApplyFilter(cnd, pp);
          }
        }
      }
    }

    public void ApplyFilter(IDataFilter condition, PersistantProperty property)
    {
      if (property == null) throw new ArgumentException("Persistant Property may not be null.", "property");

      if (condition.FilterType == FilterType.IsNotNull || condition.FilterType == FilterType.IsNull)
      {
        Filters.Add(string.Format("{0} {1}", property.GetFullColumnName(), PersistanceManager.GetFilterText(condition.FilterType)));
      }
      else if (condition.FilterType == FilterType.In || condition.FilterType == FilterType.NotIn)
      {
        if (property.PropertyType == typeof(int) || typeof(BusinessObject).IsAssignableFrom(property.PropertyType))
        {
          Collection<string> pc = new Collection<string>();
          foreach (object filter in (IList)condition.Filter)
          {
            pc.Add(filter.ToString());
          }
          Filters.Add(string.Format("{0} {1} ({2})", property.GetFullColumnName(), PersistanceManager.GetFilterText(condition.FilterType), string.Join(",", pc), Command));
        }
        else if (property.PropertyType == typeof(Guid))
        {
          Collection<string> pc = new Collection<string>();
          foreach (object filter in (IList)condition.Filter)
          {
            pc.Add(string.Format("'{0}'", filter));
          }
          Filters.Add(string.Format("{0} {1} ({2})", property.GetFullColumnName(), PersistanceManager.GetFilterText(condition.FilterType), string.Join(",", pc), Command));
        }
        else
        {
          //throw new InvalidOperationException("Property may only be of type int, string or Guid for In/NotIn filters");
          //}
          Collection<string> pc = new Collection<string>();
          foreach (object filter in (IList)condition.Filter)
          {
            pc.Add(PersistanceManager.AddParameter(ParameterCount, filter, Command));
          }
          Filters.Add(string.Format("{0} {1} ({2})", property.GetFullColumnName(), PersistanceManager.GetFilterText(condition.FilterType), string.Join(",", pc)));
        }
        //Collection<string> pc = new Collection<string>();
        //foreach (object filter in (IList)condition.Filter)
        //{
        //  pc.Add(PersistanceManager.AddParameter(ParameterCount, filter, Command));
        //}
        //Filters.Add(string.Format("{0} {1} ({2})", property.GetFullColumnName(), PersistanceManager.GetFilterText(condition.FilterType), string.Join(",", pc)));
      }
      else
      {
        Filters.Add(string.Format("{0} {1} {2}", property.GetFullColumnName(), PersistanceManager.GetFilterText(condition.FilterType), PersistanceManager.AddParameter(ParameterCount, condition.Filter, Command)));
      }
    }

    public void ApplyFilters(Collection<IDataFilter> conditions, PersistantProperty property)
    {
      if (property == null) throw new ArgumentException("Persistant Property may not be null.", "property");
      Collection<string> filters = new Collection<string>();

      foreach (IDataFilter condition in conditions)
      {
        if (condition.FilterType == FilterType.IsNotNull || condition.FilterType == FilterType.IsNull)
        {
          filters.Add(string.Format("{0} {1}", property.GetFullColumnName(), PersistanceManager.GetFilterText(condition.FilterType)));
        }
        else if (condition.FilterType == FilterType.In || condition.FilterType == FilterType.NotIn)
        {
          if (property.PropertyType == typeof(int) || typeof(BusinessObject).IsAssignableFrom(property.PropertyType))
          {
            Collection<string> pc = new Collection<string>();
            foreach (object filter in (IList)condition.Filter)
            {
              pc.Add(filter.ToString());
            }
            filters.Add(string.Format("{0} {1} ({2})", property.GetFullColumnName(), PersistanceManager.GetFilterText(condition.FilterType), string.Join(",", pc), Command));
          }
          else if (property.PropertyType == typeof(Guid))
          {
            Collection<string> pc = new Collection<string>();
            foreach (object filter in (IList)condition.Filter)
            {
              pc.Add(string.Format("'{0}'", filter));
            }
            filters.Add(string.Format("{0} {1} ({2})", property.GetFullColumnName(), PersistanceManager.GetFilterText(condition.FilterType), string.Join(",", pc), Command));
          }
          else
          {
            //throw new InvalidOperationException("Property may only be of type int, string or Guid for In/NotIn filters");
            //}
            Collection<string> pc = new Collection<string>();
            foreach (object filter in (IList)condition.Filter)
            {
              pc.Add(PersistanceManager.AddParameter(ParameterCount, filter, Command));
            }
            filters.Add(string.Format("{0} {1} ({2})", property.GetFullColumnName(), PersistanceManager.GetFilterText(condition.FilterType), string.Join(",", pc)));
          }
        }
        else
        {
          filters.Add(string.Format("{0} {1} {2}", property.GetFullColumnName(), PersistanceManager.GetFilterText(condition.FilterType), PersistanceManager.AddParameter(ParameterCount, condition.Filter, Command)));
        }
      }

      if (filters.Count > 0)
      {
        Filters.Add(string.Format("({0})", string.Join(" OR ", filters)));
      }
    }

    void ParseQuery(IInternalObjectRepository repository)
    {
      if (repository.NamespaceProperty != null)
      {
        AppendNamespaceFilters(repository);
      }

      string query = string.Format("SELECT {0} \nFROM {1}", string.Join("\n, ", QueryColumns), Table); // WITH (ROWLOCK)
      if (JoinClauses.Count > 0)
      {
        query = string.Format("{0}\n{1}", query, string.Join("\n", JoinClauses));
      }
      if (Filters.Count > 0)
      {
        query = string.Format("{0}\nWHERE {1}", query, string.Join("\nAND ", Filters));
      }
      if (SortColumns.Count > 0)
      {
        query = string.Format("{0}\nORDER BY {1}", query, string.Join(", ", SortColumns));
      }
      Command.CommandText = query;
    }

    public DataSet Execute(IInternalObjectRepository or)
    {
      try
      {
        ParseQuery(or);
        PrintCommand();
        System.Data.DataSet ds = new System.Data.DataSet();
        ds.Locale = CultureInfo.InvariantCulture;
        //ReadDataSet(ds);
        using (IDataReader r = Command.ExecuteReader())
        {
          ds.Load(r, LoadOption.OverwriteChanges, string.Empty);
          r.Close();
        }
        return ds;
      }
      catch (SqlException)
      {
        throw;
      }
      catch (Exception)
      {
        throw;
      }
    }

    //void ReadDataSet(DataSet ds)
    //{
    //  try
    //  {
    //    using (IDataReader r = Command.ExecuteReader())
    //    {
    //      ds.Load(r, LoadOption.OverwriteChanges, string.Empty);
    //    }
    //  }
    //  catch (SqlException ex)
    //  {
    //    if (ex.Number == 1205)
    //    {
    //      ReadDataSet(ds);
    //    }
    //    else
    //      throw;
    //  }
    //}
  }
}

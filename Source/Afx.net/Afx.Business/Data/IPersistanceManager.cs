﻿using Afx.Business;
using Afx.Business.Documents;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.Data
{
  public interface IPersistanceManager
  {
    void RegisterAssembly(Assembly assembly);
    void RegisterDocumentStateEventManager(IDocumentStateEventManager manager);
    void RegisterDocumentTransferManager(IDocumentTransferManager manager);
    void RegisterEventManager(IPersistanceEventManager manager);
    ObjectRepository<T> GetRepository<T>()
      where T : BusinessObject;

    IDbCommand GetCommand();
    IDbCommand GetCommand(string dbName);
    string AddParameter(object name, object value, IDbCommand command);
    string AddOutputParameter(object name, IDbCommand command);
    string GetIdentifier(string name);
    IDbCommand GetSetFlagCommand(int id, string schema, string tableName, string flag);
    IDbCommand GetUnFlagCommand(int id, string schema, string tableName, string flag);
    IDbCommand GetQueryFlagsCommand(int id, string schema, string tableName);
  }
}

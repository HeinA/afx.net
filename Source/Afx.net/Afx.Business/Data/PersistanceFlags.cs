﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.Data
{
  [Flags]
  public enum PersistanceFlags
  {
    None = 0,
    Import = 1,
    IgnoreRevision = 2,
    IgnoreControllingOU = 4,
    ValidateIds = 8
  }
}

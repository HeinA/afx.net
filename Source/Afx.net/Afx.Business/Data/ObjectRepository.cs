﻿using Afx.Business;
using Afx.Business.Collections;
using Afx.Business.Documents;
using Afx.Business.Security;
using Afx.Business.Service;
using Afx.Business.ServiceModel;
using Afx.Business.Tabular;
using Microsoft.Practices.Unity;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace Afx.Business.Data
{
  public sealed class ObjectRepository<TObject> : IInternalObjectRepository
    where TObject : BusinessObject, IBusinessObject
  {
    [InjectionConstructor]
    public ObjectRepository(IPersistanceManager persistanceManager, IUnityContainer container)
    {
      PersistanceManager = (IInternalPersistanceManager)persistanceManager;
      Container = container;
      Initialize();
    }

    #region Search

    public SearchResults Search(Collection<IDataFilter> filters)
    {
      return null;
    }

    #endregion

    #region GetInstance

    public TObject GetInstance(int id)
    {
      Collection<IDataFilter> filters = new Collection<IDataFilter>();
      filters.Add(new DataFilter<TObject>(BusinessObject.IdProperty, FilterType.Equals, id));
      return GetInstance(filters);
    }

    public TObject GetInstance(Guid globalIdentifier)
    {
      Collection<IDataFilter> filters = new Collection<IDataFilter>();
      filters.Add(new DataFilter<TObject>(BusinessObject.GlobalIdentifierProperty, FilterType.Equals, globalIdentifier));
      return GetInstance(filters);
    }

    public TObject GetInstance(Collection<IDataFilter> filters)
    {
      QueryContext qc = Container.Resolve<QueryContext>();
      FetchData(filters, qc);
      
      int count = qc.Count(ObjectType);
      
      if (count == 0) return null;
      if (count > 1) throw new QueryException("Query returned more than one row.");

      TypedDataRow tdr = qc.GetRootDataSet(ObjectType).FirstOrDefault((r) => true);
      if (tdr == null) throw new QueryException("Query returned an invalid result.");

      TObject obj = (TObject)Activator.CreateInstance(tdr.ObjectType);
      IInternalObjectRepository or = PersistanceManager.GetRepository(tdr.ObjectType);
      using (new EventStateSuppressor(obj))
      {
        or.ResolveObject(obj, tdr);
      }
      return obj;
    }

    public BasicCollection<TObject> GetInstances(Collection<IDataFilter> filters)
    {
      BasicCollection<TObject> col = new BasicCollection<TObject>();
      QueryContext qc = Container.Resolve<QueryContext>();
      FetchData(filters, qc);

      foreach (TypedDataRow tdr in qc.GetRootDataSet(ObjectType))
      {
        TObject obj = (TObject)Activator.CreateInstance(tdr.ObjectType);
        IInternalObjectRepository or = PersistanceManager.GetRepository(tdr.ObjectType);
        using (new EventStateSuppressor(obj))
        {
          or.ResolveObject(obj, tdr);
          col.Add(obj);
        }
      }

      return col;
    }

    public BasicCollection<TObject> GetAllInstances()
    {
      return GetInstances(new Collection<IDataFilter>());
    }

    #endregion

    #region Persistance

    public void Persist(TObject obj, PersistanceFlags flags = PersistanceFlags.None)
    {
      try
      {
        if ((flags & PersistanceFlags.Import) == 0)
        {
          //COU
          //if ((flags & PersistanceFlags.IgnoreControllingOU) == 0)
          //{
          //  Document doc = obj as Document;
          //  if (doc != null)
          //  {
          //    if (!doc.ControllingOrganizationalUnit.Server.Equals(SecurityContext.Server))
          //    {
          //      throw new DocumentControlException(string.Format("Document {0} is not controlled by this server.", doc.DocumentNumber));
          //    }
          //  }
          //}

          if (!obj.Validate()) throw new MessageException(string.Format("An object failed it's validation.\n({0})", obj.Error));
        }

        Stack<BusinessObject> persisted = new Stack<BusinessObject>();
        IInternalObjectRepository or = PersistanceManager.GetRepository(obj.GetType());
        or.Persist(obj, persisted, flags);
        while (persisted.Count > 0)
        {
          BusinessObject bo = persisted.Pop();
          if (bo.IsDeleted)
            AfterDelete(bo, flags);
          else
            AfterPersist(bo, flags);
        }
      }
      catch
      {
        throw;
      }
    }

    public void Persist(BasicCollection<TObject> col, PersistanceFlags flags = PersistanceFlags.None)
    {
      try
      {
        if ((flags & PersistanceFlags.Import) == 0)
        {
          if (!string.IsNullOrWhiteSpace(ValidationHelper.ValidateCollection<TObject>(col)))
              throw new MessageException("The collection failed it's validation.");

          foreach (TObject obj in col)
          {
            if (!obj.IsDeleted && !obj.Validate())
              throw new MessageException("An object failed it's validation.");
          } 
        }

        Stack<BusinessObject> persisted = new Stack<BusinessObject>();
        foreach (TObject obj in col)
        {
          IInternalObjectRepository or = PersistanceManager.GetRepository(obj.GetType());
          or.Persist(obj, persisted, flags);
          if (ObjectAttributeHelper.IsCached(obj.GetType()) && (flags & PersistanceFlags.Import) == PersistanceFlags.Import) 
            Cache.Instance.AppendObject(obj);
        }
        while (persisted.Count > 0)
        {
          BusinessObject bo = persisted.Pop();
          if (bo.IsDeleted)
            AfterDelete(bo, flags);
          else
            AfterPersist(bo, flags);
        }
      }
      catch
      {
        throw;
      }
    }

    void BeforeDelete(BusinessObject o, PersistanceFlags flags)
    {
      Type objectType = o.GetType();
      while (objectType != typeof(object))
      {
        foreach (IPersistanceEventManager em in PersistanceManager.GetEventManagers(objectType))
        {
          if (ObjectType == em.TargetType) em.BeforeDelete(o, flags);
        }
        objectType = objectType.BaseType;
      }
    }

    void AfterDelete(BusinessObject o, PersistanceFlags flags)
    {
      Type objectType = o.GetType();
      while (objectType != typeof(object))
      {
        foreach (IPersistanceEventManager em in PersistanceManager.GetEventManagers(objectType))
        {
          em.AfterDelete(o, flags);
        }
        objectType = objectType.BaseType;
      }
    }

    bool BeforePersist(BusinessObject o, PersistanceFlags flags)
    {
      Type objectType = o.GetType();
      while (objectType != typeof(object))
      {
        foreach (IPersistanceEventManager em in PersistanceManager.GetEventManagers(objectType))
        {
          //if (ObjectType == em.TargetType) 
          if (!em.BeforePersist(o, flags)) return false;
        }
        objectType = objectType.BaseType;
      }
      return true;
    }

    void AfterPersist(BusinessObject o, PersistanceFlags flags)
    {
      Type objectType = o.GetType();
      while (objectType != typeof(object))
      {
        foreach (IPersistanceEventManager em in PersistanceManager.GetEventManagers(objectType))
        {
          em.AfterPersist(o, flags);
        }
        objectType = objectType.BaseType;
      }
    }

    bool PersistInternal(TObject obj, Stack<BusinessObject> persisted, PersistanceFlags flags)
    {
      try
      {
        if (obj.GlobalIdentifier == Guid.Empty) return false;
        IAssociativeObject ao1 = obj as IAssociativeObject;

        bool isNew = false;
        int id = obj.Id;
        if ((flags & PersistanceFlags.Import) == PersistanceFlags.Import || (flags & PersistanceFlags.ValidateIds) == PersistanceFlags.ValidateIds)
        {
          if (ObjectType == obj.GetType())
          {
            id = IdMapper.Instance.GetId(obj.GlobalIdentifier, ObjectType);
            using (new EventStateSuppressor(obj))
            {
              obj.Id = id;
            }
          }
        }        
        if (obj.Id <= 0) isNew = true;

        if (BaseRepository != null)
        {
          if (!BaseRepository.Persist(obj, persisted, flags)) return false;
        }


        try
        {
          if (obj.IsDeleted || (ao1 != null && ao1.Reference != null && ao1.Reference.IsDeleted))
          {
            if (IsReadOnly) return true;
            if (persisted.Contains(obj)) return true;

            // Only delete if the repositry is the root or owner
            if (BaseRepository == null && !isNew)
            {
              BeforeDelete(obj, flags);
              Delete(obj, flags);
              if (ObjectAttributeHelper.IsCached(ObjectType)) CacheExtension.RefreshCache = true;
              if ((flags & PersistanceFlags.Import) != PersistanceFlags.Import)
              {
                persisted.Push(obj);
              }
            }
            return true;
          }
        }
        catch
        {
          throw;
        }

        if (obj.IsDirty || isNew || (flags & PersistanceFlags.Import) == PersistanceFlags.Import)
        {
          if (IsReadOnly) return true;
          if (ao1 != null && ao1.Reference == null) return true;
          if (!BeforePersist(obj, flags)) return false;
          if (isNew)
          {
            try
            {
              obj.Id = Insert(obj, persisted, flags);
              if (BaseRepository == null && (typeof(Setting).IsAssignableFrom(obj.GetType()) || (flags & PersistanceFlags.Import) != PersistanceFlags.Import))
              {
                persisted.Push(obj);
              }
            }
            catch (UniqueIndexViolationException)
            {
              if ((flags & PersistanceFlags.Import) == PersistanceFlags.Import) return true;
              throw;
            }
          }
          else
          {
            Update(obj, persisted, flags);
            if (BaseRepository == null && (typeof(Setting).IsAssignableFrom(obj.GetType()) || (flags & PersistanceFlags.Import) != PersistanceFlags.Import))
            {
              persisted.Push(obj);
            }
          }

          if (HasFlags)
          {
            foreach (var flag in obj.Flags)
            {
              IDbCommand cmd = PersistanceManager.GetSetFlagCommand(obj.Id, Schema, TableName, flag);
              cmd.ExecuteNonQuery();
            }

            foreach (var flag in obj.RemovedFlags)
            {
              IDbCommand cmd = PersistanceManager.GetUnFlagCommand(obj.Id, Schema, TableName, flag);
              cmd.ExecuteNonQuery();
            }
          }

          if (ObjectAttributeHelper.IsCached(ObjectType)) CacheExtension.RefreshCache = true;
        }

        if (ao1 != null && ao1.Reference != null && ao1.ReferenceIsOwned) // || !ObjectAttributeHelper.IsCached(ao1.Reference.GetType())))
        {
          if (ao1.Reference.IsDeleted)
          {
            if (BaseRepository == null && !isNew)
            {
              BeforeDelete(obj, flags);
              Delete(obj, flags);
              if (ObjectAttributeHelper.IsCached(ObjectType)) CacheExtension.RefreshCache = true;
              if ((flags & PersistanceFlags.Import) != PersistanceFlags.Import)
              {
                persisted.Push(obj);
              }
            }
          }

          IInternalObjectRepository or = PersistanceManager.GetRepository(ao1.Reference.GetType());
          or.Persist(ao1.Reference, flags);

          if (ao1.Reference.IsDeleted)
          {
            return true;
          }
        }

        foreach (var cp in PersistantCollections)
        {
          IInternalObjectRepository or = PersistanceManager.GetRepository(cp.CollectionType);
          if (or.OwnerType == ObjectType)
          {
            // Collection items belong to this object
            IList list = cp.GetValue(obj);
            foreach (IBusinessObject child in list)
            {
              or = PersistanceManager.GetRepository(child.GetType());
              //IAssociativeObject ao = null;
              //if (cp.IsAssociative) ao = obj.GetAssociativeObject(cp.CollectionType, child.GlobalIdentifier);
              or.Persist(child, persisted, flags);
              //if (ao != null) ao.ReferenceId = child.GlobalIdentifier;
            }
          }

          if (cp.IsAssociative)
          {
            foreach (IAssociativeObject ao in obj.GetAssociativeObjects(cp.AssociativeType))
            {
              or = PersistanceManager.GetRepository(ao.GetType());
              or.Persist(ao, persisted, flags);
            }
          }
        }

        foreach (IBusinessObject child in obj.ExtensionObjects)
        {
          if (child.Owner.GetType() == ObjectType)
          {
            IInternalObjectRepository or = PersistanceManager.GetRepository(child.GetType());
            or.Persist(child, persisted, flags);
          }
        }

        if (obj.IsDirty || isNew || (flags & PersistanceFlags.Import) == PersistanceFlags.Import)
        {
          if (IsReadOnly) return true;
          Update(obj, persisted, flags, true, !isNew);
        }
      }
      catch
      {
        throw;
      }

      return true;
    }

    int Insert(TObject obj, Stack<BusinessObject> persisted, PersistanceFlags flags)
    {
      try
      {
        InsertStatement i = new InsertStatement(this, flags);
        foreach (PersistantProperty pp in this.WriteProperties)
        {
          i.AddProperty(pp, obj, persisted);
        }

        using (new EventStateSuppressor(obj))
        {
          obj.Id = i.Execute(obj);
        }
        return obj.Id;
      }
      catch
      {
        throw;
      }
    }

    void Update(TObject obj, Stack<BusinessObject> persisted, PersistanceFlags flags, bool postProsess = false, bool checkConcurrency = true)
    {
      try
      {
        //if (typeof(IRevisable).IsAssignableFrom(ObjectType) && GetProperty("Revision") != null && (flags & PersistanceFlags.Import) == 0 && !postProsess)
        //{
        //  IRevisable r = obj as IRevisable;
        //  r.Revision++;
        //}

        UpdateStatement u = new UpdateStatement(this, flags);
        foreach (PersistantProperty pp in this.WriteProperties.Where(p => p.PostProcess == postProsess))
        {
          if ((flags & PersistanceFlags.Import) == PersistanceFlags.Import && !pp.ImportOverwrite) 
            continue;

          u.AddProperty(pp, obj, persisted, checkConcurrency);
        }
        if (u.Count > 0) u.Execute(obj);
      }
      catch
      {
        throw;
      }
    }

    void Delete(TObject obj, PersistanceFlags flags)
    {
      try
      {
        DeleteStatement ds = new DeleteStatement(this);
        ds.Execute(obj, flags);
      }
      catch
      {
        throw;
      }
    }

    #endregion

    #region Private

    #region Properties

    IUnityContainer mContainer;
    IUnityContainer Container
    {
      get { return mContainer; }
      set { mContainer = value; }
    }

    IInternalPersistanceManager mPersistanceManager;
    IInternalPersistanceManager PersistanceManager
    {
      get { return mPersistanceManager; }
      set { mPersistanceManager = value; }
    }

    Type mObjectType;
    Type ObjectType
    {
      get { return mObjectType; }
      set { mObjectType = value; }
    }

    bool mIsAbstract;
    bool IsAbstract
    {
      get { return mIsAbstract; }
      set { mIsAbstract = value; }
    }

    string mSchema;
    string Schema
    {
      get { return mSchema; }
      set { mSchema = value; }
    }

    string mTableName;
    string TableName
    {
      get { return mTableName; }
      set { mTableName = value; }
    }

    string mIdColumn;
    string IdColumn
    {
      get { return mIdColumn; }
      set { mIdColumn = value; }
    }

    string mGlobalIdentifierColumn;
    string GlobalIdentifierColumn
    {
      get { return mGlobalIdentifierColumn; }
      set { mGlobalIdentifierColumn = value; }
    }

    string mOwnerColumn;
    string OwnerColumn
    {
      get { return mOwnerColumn; }
      set { mOwnerColumn = value; }
    }

    Type mOwnerType;
    Type OwnerType
    {
      get { return mOwnerType; }
      set { mOwnerType = value; }
    }

    #region bool IsReadOnly

    bool mIsReadOnly;
    public bool IsReadOnly
    {
      get { return mIsReadOnly; }
      set { mIsReadOnly = value; }
    }

    #endregion

    #region bool HasFlags

    bool mHasFlags = false;
    bool HasFlags
    {
      get { return mHasFlags; }
      set { mHasFlags = value; }
    }

    #endregion

    string mRowColumn;
    string RowColumn
    {
      get { return mRowColumn; }
      set { mRowColumn = value; }
    }

    string mColumnColumn;
    string ColumnColumn
    {
      get { return mColumnColumn; }
      set { mColumnColumn = value; }
    }

    PersistantProperty mNamespaceProperty = null;
    PersistantProperty NamespaceProperty
    {
      get { return mNamespaceProperty; }
      set { mNamespaceProperty = value; }
    }

    bool mPersistToDerivedClass = false;
    bool PersistToDerivedClass
    {
      get { return mPersistToDerivedClass; }
      set { mPersistToDerivedClass = value; }
    }

    string mReferenceColumn;
    public string ReferenceColumn
    {
      get { return mReferenceColumn; }
      private set { mReferenceColumn = value; }
    }

    IInternalObjectRepository mBaseRepository;
    IInternalObjectRepository BaseRepository
    {
      get { return mBaseRepository; }
      set { mBaseRepository = value; }
    }

    bool IsOwned
    {
      get { return OwnerType != null; }
    }

    bool IsAssociative { get; set; }

    Collection<PersistantProperty> mProperties;
    Collection<PersistantProperty> Properties
    {
      get { return mProperties ?? (mProperties = new Collection<PersistantProperty>()); }
    }

    IEnumerable<PersistantProperty> ReadProperties
    {
      get { return Properties.Where(p => p.Read || p.IsOwnerColumn); }
    }

    IEnumerable<PersistantProperty> HeirarchyReadProperties
    {
      get
      {
        if (BaseRepository != null) return Properties.Where(p => p.Read).Union(BaseRepository.HeirarchyReadProperties);
        return Properties.Where(p => p.Read || p.IsOwnerColumn);
      }
    }

    IEnumerable<PersistantProperty> WriteProperties
    {
      get { return Properties.Where(p => p.Write || p.IsOwnerColumn); }
    }

    Collection<Type> mDerivedClasses;
    Collection<Type> DerivedClasses
    {
      get { return mDerivedClasses ?? (mDerivedClasses = new Collection<Type>(PersistanceManager.BusinessClasses.Where((t) => t.BaseType == ObjectType).ToList())); }
    }

    Collection<Type> mContainedBaseClasses;
    Collection<Type> ContainedBaseClasses
    {
      get { return mContainedBaseClasses ?? (mContainedBaseClasses = new Collection<Type>()); }
    }

    Collection<PersistantCollection> mPersistantCollections;
    Collection<PersistantCollection> PersistantCollections
    {
      get { return mPersistantCollections ?? (mPersistantCollections = new Collection<PersistantCollection>()); }
    }

    #endregion

    #region Initialization

    void GetOwnerColumn(PersistantObjectAttribute ta)
    {
      PropertyInfo piOwner = ObjectType.GetProperty(BusinessObject.OwnerProperty);
      if (piOwner != null && typeof(BusinessObject).IsAssignableFrom(piOwner.PropertyType))
      {
        OwnerType = piOwner.PropertyType;
        if (ta.OwnerColumn == null)
        {
          Type t = ObjectType.BaseType;
          while (t != typeof(BusinessObject))
          {
            PersistantObjectAttribute ta1 = GetTableAttribute(t);
            if (ta1.OwnerColumn != null)
            {
              OwnerColumn = ta1.OwnerColumn;
              break;
            }
            t = t.BaseType;
          }
        }

        if (OwnerColumn == null) OwnerColumn = ta.OwnerColumn ?? OwnerType.Name;
      }
    }

    void Initialize()
    {
      ObjectType = typeof(TObject);
      IsAbstract = ObjectType.IsAbstract;

      PersistantObjectAttribute ta = GetTableAttribute(ObjectType);
      GetOwnerColumn(ta);

      Type associative = GetGenericSubClass(typeof(AssociativeObject<,>), ObjectType);
      if (associative != null)
      {
        IsAssociative = true;
        Type reference = associative.GetGenericArguments()[1];
        ReferenceColumn = ta.ReferenceColumn ?? reference.Name;
      }

      Schema = ta.Schema;
      TableName = ta.TableName ?? ObjectType.Name;
      HasFlags = ta.HasFlags;
      IdColumn = ta.IdColumn ?? "id";
      GlobalIdentifierColumn = ta.GlobalIdentifierColumn ?? "GlobalIdentifier";
      RowColumn = ta.RowColumn ?? "Row";
      ColumnColumn = ta.ColumnColumn ?? "Column";
      PersistToDerivedClass = ta.PersistToDerivedClass;
      IsReadOnly = ta.IsReadOnly;

      if (string.IsNullOrWhiteSpace(OwnerColumn) && OwnerType != null && !PersistToDerivedClass) throw new PersistantObjectException(ObjectType, "OwnerColumn is mandatory for owned objects.");
      if (IsAssociative && string.IsNullOrWhiteSpace(ReferenceColumn)) throw new PersistantObjectException(ObjectType, "ReferenceColumn must be specified for an Associative Object.");

      try
      {
        InitializeProperties(ObjectType);
        //if (BaseRepository != null) OwnerColumn = BaseRepository.OwnerColumn;
      }
      catch (Exception ex)
      {
        throw new PersistantObjectException(ObjectType, "An error occured loading properties.", ex);
      }
    }

    Type GetGenericSubClass(Type generic, Type toCheck)
    {
      while (toCheck != null && toCheck != typeof(object))
      {
        var cur = toCheck.IsGenericType ? toCheck.GetGenericTypeDefinition() : toCheck;
        if (generic == cur)
        {
          return toCheck;
        }
        toCheck = toCheck.BaseType;
      }
      return null;
    }

    /// <summary>
    /// Adds properties of the repository object and all base classes that where PersistWithSubClass is true.
    /// </summary>
    /// <param name="objectType"></param>
    void InitializeProperties(Type objectType)
    {
      if (objectType == typeof(BusinessObject))
      {
        AddBusinessObjectProperties();
      }
      else if (objectType.IsGenericType && objectType.GetGenericTypeDefinition() == typeof(AssociativeObject<,>))
      {
        AddAssociativeObjectProperties();
      }
      else if (ObjectType == objectType)
      {
        AddProperties(objectType);
      }
      else if (objectType.IsGenericType && objectType.GetGenericTypeDefinition() == typeof(TabularBusinessObject<,,>)) // || objectType.GetGenericTypeDefinition() == typeof(TabularBusinessObject<,,,>)))
      {
        AddTableProperties(objectType);
      }
      else if (objectType.IsGenericType && objectType.GetGenericTypeDefinition() == typeof(TableCell<,,>))
      {
        AddCellProperties(objectType);
      }
      else
      {
        PersistantObjectAttribute ta = GetTableAttribute(objectType);
        if (ta.PersistToDerivedClass) AddProperties(objectType);
        else BaseRepository = PersistanceManager.GetRepository(objectType);
      }
    }

    void AddTableProperties(Type objectType)
    {
      InitializeProperties(objectType.BaseType);

      PropertyInfo pi = objectType.GetProperty("RowCollection", BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.DeclaredOnly);
      PersistantCollectionAttribute pca = GetPersistantCollectionAttribute(pi);
      PersistantCollection pc = new PersistantCollection(this, pi, pca);
      PersistantCollections.Add(pc);

      pi = objectType.GetProperty("ColumnCollection", BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.DeclaredOnly);
      pca = GetPersistantCollectionAttribute(pi);
      pc = new PersistantCollection(this, pi, pca);
      PersistantCollections.Add(pc);

      pi = objectType.GetProperty("Cells", BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.DeclaredOnly);
      pca = GetPersistantCollectionAttribute(pi);
      pc = new PersistantCollection(this, pi, pca);
      PersistantCollections.Add(pc);
    }

    void AddCellProperties(Type objectType)
    {
      InitializeProperties(objectType.BaseType);

      PropertyInfo pi = objectType.GetProperty("Row", BindingFlags.Instance | BindingFlags.Public | BindingFlags.DeclaredOnly);
      PersistantPropertyAttribute ca = GetColumnAttribute(pi);
      PersistantProperty pp = new PersistantProperty(this, pi, ca);
      Properties.Add(pp);

      pi = objectType.GetProperty("Column", BindingFlags.Instance | BindingFlags.Public | BindingFlags.DeclaredOnly);
      ca = GetColumnAttribute(pi);
      pp = new PersistantProperty(this, pi, ca);
      Properties.Add(pp);
    }

    void AddProperties(Type objectType)
    {
      InitializeProperties(objectType.BaseType);

      if (ObjectType != objectType)
      {
        ContainedBaseClasses.Add(objectType);
      }

      foreach (PropertyInfo pi in objectType.GetProperties(BindingFlags.Instance | BindingFlags.Public | BindingFlags.DeclaredOnly).Where(prop => prop.IsDefined(typeof(PersistantPropertyAttribute), false)))
      {
        PersistantPropertyAttribute ca = GetColumnAttribute(pi);
        PersistantProperty pp = new PersistantProperty(this, pi, ca);
        Properties.Add(pp);
      }

      foreach (PropertyInfo pi in objectType.GetProperties(BindingFlags.Instance | BindingFlags.Public | BindingFlags.DeclaredOnly).Where(prop => prop.IsDefined(typeof(PersistantCollectionAttribute), false)))
      {
        PersistantCollectionAttribute pca = GetPersistantCollectionAttribute(pi);
        PersistantCollection pc = new PersistantCollection(this, pi, pca);
        PersistantCollections.Add(pc);
      }
    }

    void AddBusinessObjectProperties()
    {
      PropertyInfo pi = ObjectType.GetProperty(BusinessObject.IdProperty);
      PersistantPropertyAttribute ca = new PersistantPropertyAttribute();
      ca.Name = IdColumn;
      PersistantProperty pp = new PersistantProperty(this, pi, ca);
      Properties.Add(pp);

      pi = ObjectType.GetProperty(BusinessObject.GlobalIdentifierProperty);
      ca = new PersistantPropertyAttribute();
      ca.Name = GlobalIdentifierColumn;
      pp = new PersistantProperty(this, pi, ca);
      Properties.Add(pp);

      if (IsOwned)
      {
        pi = ObjectType.GetProperty(BusinessObject.OwnerProperty);
        ca = new PersistantPropertyAttribute();
        ca.Name = OwnerColumn;
        pp = new PersistantProperty(this, pi, ca);
        Properties.Add(pp);
      }
    }

    const string ReferenceProperty = "Reference";
    void AddAssociativeObjectProperties()
    {
      AddBusinessObjectProperties();

      PropertyInfo pi = ObjectType.GetProperty(ReferenceProperty);
      PersistantPropertyAttribute ca = new PersistantPropertyAttribute();
      ca.Name = ReferenceColumn;
      PersistantProperty pp = new PersistantProperty(this, pi, ca);
      Properties.Add(pp);
    }

    #endregion

    #region Queries

    BusinessObject GetNullObject(Type type)
    {
      ConstructorInfo ci = type.GetConstructor(new Type[] { typeof(bool) });
      if (ci == null) throw new InvalidOperationException("BusinessObject does not implement the nullabe constructor.");
      return (BusinessObject)ci.Invoke(new object[] { true });
    }

    void ResolveObject(TObject obj, TypedDataRow dr)
    {
      try
      {
        if (BaseRepository != null) BaseRepository.ResolveObject(obj, dr);
        else
        {
          obj.Id = (int)dr[IdColumn];
          obj.IsNew = false;
        }

        if (HasFlags)
        {
          IDbCommand cmd = PersistanceManager.GetQueryFlagsCommand(obj.Id, Schema, TableName);
          DataSet ds = DataHelper.ExecuteDataSet(cmd);
          using (new EventStateSuppressor(obj))
          {
            foreach (DataRow dr1 in ds.Tables[0].Rows)
            {
              obj.SetFlag((string)dr1["Flag"]);
            }
          }
        }

        foreach (var pp in ReadProperties.Where((p) => !p.IsOwnerColumn && p.PropertyInfo.CanWrite))
        {
          if (pp.ColumnName == "PhysicalAddress" && obj.GetType().Name == "AccountReference")
          {
          }

          if (typeof(BusinessObject).IsAssignableFrom(pp.PropertyType))
          {
            object reference = PersistanceManager.CastToObjectType(dr[pp.ColumnName]);
            if (reference == null) continue;
            IAssociativeObject ao = obj as IAssociativeObject;
            if (pp.ForceLoad || (ao != null && ao.ReferenceIsOwned) || !ObjectAttributeHelper.IsCached(pp.PropertyType))
            {
              BusinessObject bo2 = null;
              if (pp.IsCached) // !(ao != null && ao.ReferenceIsOwned))
              {
                bo2 = Cache.Instance.GetObject(pp.PropertyType, (int)reference);
              }
              if (bo2 != null)
              {
                obj.SetReferenceValue(bo2.GlobalIdentifier, pp.PropertyName);
              }
              else
              {
                TypedDataRow tdr1 = null;
                try
                {
                  tdr1 = dr.Context.GetDataSet(pp.PropertyType).FirstOrDefault(r => r.Id == (int)reference);
                }
                catch
                {
                  throw;
                }
                if (tdr1 != null)
                {
                  BusinessObject bo = (BusinessObject)Activator.CreateInstance(tdr1.ObjectType);
                  using (new EventStateSuppressor(obj))
                  using (new EventStateSuppressor(bo))
                  {
                    IInternalObjectRepository or = PersistanceManager.GetRepository(tdr1.ObjectType);
                    pp.SetValue(obj, bo);
                    or.ResolveObject(bo, tdr1);
                  }
                }
              }
            }
            else
            {
              if (reference == null) obj.SetReferenceValue(Guid.Empty, pp.PropertyName);
              else obj.SetReferenceValue(IdMapper.Instance.GetIdentifier((int)reference, pp.PropertyType), pp.PropertyName);
            }
          }
          else
          {
            using (new EventStateSuppressor(obj))
            {
              try
              {
                pp.SetValue(obj, PersistanceManager.CastToObjectType(dr[pp.ColumnName]));
              }
              catch
              {
                throw;
              }
            }
          }

          //if (pp.IsGlobalIdentifierColumn)
          //{
          //  IdMapper.Instance.RegisterId(obj.GlobalIdentifier, obj.Id, obj.GetType());
          //}
        }

        foreach (var cp in PersistantCollections)
        {
          if (cp.IsAssociative)
          {
              try
              {
                foreach (TypedDataRow tdr in dr.Context.GetCollection(cp.AssociativeType, obj.Id))
                {
                  IInternalObjectRepository or = PersistanceManager.GetRepository(tdr.ObjectType);
                  BusinessObject obj1 = (BusinessObject)Activator.CreateInstance(tdr.ObjectType);
                  obj.SetAssociativeObject(cp.AssociativeType, (IAssociativeObject)obj1);
                  or.ResolveObject(obj1, tdr);
                }
              }
              catch
              {
                throw;
              }

            //if (!ObjectAttributeHelper.IsCached(cp.CollectionType))
            //{
            //  try
            //  {
            //    IList list = cp.GetValue(obj);
            //    list.Clear();
            //    foreach (TypedDataRow tdr in dr.Context.GetCollection(cp.CollectionType, obj.Id))
            //    {
            //      IInternalObjectRepository or = PersistanceManager.GetRepository(tdr.ObjectType);
            //      BusinessObject obj1 = (BusinessObject)Activator.CreateInstance(tdr.ObjectType);
            //      list.Add(obj1);
            //      or.ResolveObject(obj1, tdr);
            //    }
            //  }
            //  catch
            //  {
            //    throw;
            //  }
            //}
          }
          else
          {
            try
            {
              IList list = cp.GetValue(obj);
              list.Clear();
              foreach (TypedDataRow tdr in dr.Context.GetCollection(cp.CollectionType, obj.Id))
              {
                IInternalObjectRepository or = PersistanceManager.GetRepository(tdr.ObjectType);
                BusinessObject obj1 = (BusinessObject)Activator.CreateInstance(tdr.ObjectType);
                list.Add(obj1);
                or.ResolveObject(obj1, tdr);
              }
            }
            catch
            {
              throw;
            }
          }
        }

        foreach (Type extension in ObjectAttributeHelper.GetExtensionObjects(ObjectType))
        {
          foreach (TypedDataRow tdr in dr.Context.GetCollection(extension, obj.Id))
          {
            IInternalObjectRepository or = PersistanceManager.GetRepository(tdr.ObjectType);
            IExtensionObject obj1 = (IExtensionObject)Activator.CreateInstance(tdr.ObjectType);
            obj.AddExtensionObject(obj1);
            or.ResolveObject(obj1, tdr);
          }
        }
      }
      catch
      {
        throw;
      }
    }

    IEnumerable<PersistantCollection> HeirarchyPersistantCollections
    {
      get
      {
        if (BaseRepository == null) return PersistantCollections;
        return PersistantCollections.Union(BaseRepository.HeirarchyPersistantCollections);
      }
    }

    void FetchData(Collection<IDataFilter> filters, QueryContext context)
    {
      foreach (Type derivedType in DerivedClasses)
      {
        IInternalObjectRepository or = PersistanceManager.GetRepository(derivedType);
        or.FetchData(filters, context);
      }

      if (!PersistToDerivedClass)
      {
        QueryStatement qs = null;
        if (!IsAbstract)
        {
          qs = new QueryStatement(PersistanceManager);
          qs.Table = GetFullTableName();
          AddQueryColumns(qs);
          AddBaseJoinClauses(qs);
          AddFilters(qs, filters);
          AddSortColumns(qs);
        }
        if (qs == null || qs.AppliedFilters >= filters.Count)
        {
          if (qs != null && context.Query(ObjectType, qs) == 0) return;

          IAssociativeObject ao = null;
          if (typeof(IAssociativeObject).IsAssignableFrom(ObjectType)) ao = (IAssociativeObject)Activator.CreateInstance(ObjectType);
          IEnumerable<PersistantProperty> ppc = HeirarchyReadProperties.Where((p) => typeof(BusinessObject).IsAssignableFrom(p.PropertyType) && !p.IsCached && (p.ForceLoad || (ao != null && ao.ReferenceIsOwned) || !ObjectAttributeHelper.IsCached(p.PropertyType)) && !p.IsOwnerColumn);
          foreach (PersistantProperty pp in ppc)
          {
            if (pp.ColumnName == "PhysicalAddress") // && ObjectType.Name == "AccountReference")
            {
            }

            //Fetch Data for all Reference Properties where it is not cached
            IInternalObjectRepository or = PersistanceManager.GetRepository(pp.PropertyType);
            Collection<int> references = context.GetReferences(ObjectType, pp);
            Collection<IDataFilter> filter1 = new Collection<IDataFilter>();
            if (references.Count > 0)
            {
              filter1.Add(new DataFilter(pp.PropertyType, BusinessObject.IdProperty, FilterType.In, references));
              or.FetchData(filter1, context);
            }
            //or.FetchData(filter1, context);
          }

          Collection<int> ownerIds = context.GetIds(ObjectType);
          if (ownerIds.Count != 0)
          {
            foreach (PersistantCollection pc in HeirarchyPersistantCollections)
            {
              if (pc.IsAssociative)
              {
                IInternalObjectRepository or = PersistanceManager.GetRepository(pc.AssociativeType);
                Collection<IDataFilter> filter1 = new Collection<IDataFilter>();
                filter1.Add(new DataFilter(pc.AssociativeType, BusinessObject.OwnerProperty, FilterType.In, ownerIds));
                if (or.ObjectType == ObjectType)
                {
                  filter1.Add(new DataFilter(pc.AssociativeType, BusinessObject.IdProperty, FilterType.NotIn, ownerIds));
                }
                or.FetchData(filter1, context);

                //IAssociativeObject ao = null;
                //if (typeof(IAssociativeObject).IsAssignableFrom(pc.AssociativeType)) ao = (IAssociativeObject)Activator.CreateInstance(pc.AssociativeType);
                //if ((ao != null && ao.ReferenceIsOwned) || !ObjectAttributeHelper.IsCached(pc.CollectionType))
                //{
                //  or = PersistanceManager.GetRepository(pc.CollectionType);
                //  filter1 = new Collection<IDataFilter>();
                //  filter1.Add(new DataFilter(pc.CollectionType, BusinessObject.OwnerProperty, FilterType.In, ownerIds));
                //  if (or.ObjectType == ObjectType)
                //  {
                //    filter1.Add(new DataFilter(pc.CollectionType, BusinessObject.IdProperty, FilterType.NotIn, ownerIds));
                //  }
                //  or.FetchData(filter1, context);
                //}
              }
              else
              {
                IInternalObjectRepository or = PersistanceManager.GetRepository(pc.CollectionType);
                Collection<IDataFilter> filter1 = new Collection<IDataFilter>();
                filter1.Add(new DataFilter(pc.CollectionType, BusinessObject.OwnerProperty, FilterType.In, ownerIds));
                if (or.ObjectType == ObjectType)
                {
                  filter1.Add(new DataFilter(pc.CollectionType, BusinessObject.IdProperty, FilterType.NotIn, ownerIds));
                }
                or.FetchData(filter1, context);
              }
            }

            foreach (Type extension in GetHeirarchyExtensionObjects())
            {
              IInternalObjectRepository or = PersistanceManager.GetRepository(extension);
              Collection<IDataFilter> filter1 = new Collection<IDataFilter>();
              filter1.Add(new DataFilter(extension, BusinessObject.OwnerProperty, FilterType.In, ownerIds));
              or.FetchData(filter1, context);
            }
          }
        }
      }
    }


    IEnumerable<Type> GetHeirarchyExtensionObjects()
    {
      return ((IInternalObjectRepository)this).GetHeirarchyExtensionObjects();
    }
    
    IEnumerable<Type> IInternalObjectRepository.GetHeirarchyExtensionObjects()
    {
      if (BaseRepository != null) return BaseRepository.GetHeirarchyExtensionObjects().Union(ObjectAttributeHelper.GetExtensionObjects(ObjectType));
      return ObjectAttributeHelper.GetExtensionObjects(ObjectType);
    }

    void AddQueryColumns(QueryStatement qs)
    {
      if (BaseRepository != null) BaseRepository.AddQueryColumns(qs);
      qs.AddQueryColumns(this);
    }

    void AddSortColumns(QueryStatement qs)
    {
      if (BaseRepository != null) BaseRepository.AddSortColumns(qs);
      qs.AddSortColumns(this);
    }

    void  AddFilters(QueryStatement qs, ICollection<IDataFilter> conditions)
    {
      if (BaseRepository != null) BaseRepository.AddFilters(qs, conditions);
      qs.AddFilters(this, conditions);
    }

    void AddBaseJoinClauses(QueryStatement qs)
    {
      if (BaseRepository != null)
      {
        qs.AddBaseJoinClause(BaseRepository, this);
        BaseRepository.AddBaseJoinClauses(qs);
      }
    }

    #endregion

    #region Utilities

    string GetFullTableName()
    {
      if (string.IsNullOrWhiteSpace(Schema)) return string.Format("{0}", PersistanceManager.GetIdentifier(TableName));
      return string.Format("{0}.{1}", PersistanceManager.GetIdentifier(Schema), PersistanceManager.GetIdentifier(TableName));
    }

    string GetFullIdColumn()
    {
      return string.Format("{0}.{1}", GetFullTableName(), PersistanceManager.GetIdentifier(IdColumn));
    }

    string GetIdColumn()
    {
      return string.Format("{0}", PersistanceManager.GetIdentifier(IdColumn));
    }

    string GetFullGlobalIdentifierColumn()
    {
      return string.Format("{0}.{1}", GetFullTableName(), PersistanceManager.GetIdentifier(GlobalIdentifierColumn));
    }

    PersistantObjectAttribute GetTableAttribute(Type objectType)
    {
      var tableAttributes = (PersistantObjectAttribute[])objectType.GetCustomAttributes(typeof(PersistantObjectAttribute), false);
      if (tableAttributes.Length == 0)
      {
        //if (objectType.IsAbstract)
        //{
          PersistantObjectAttribute ta = new PersistantObjectAttribute() { PersistToDerivedClass = true };
          return ta;
        //}
        //throw new TableAttributeException(objectType, "TableAttribute is undefined");
      }
      return tableAttributes[0];
    }

    PersistantPropertyAttribute GetColumnAttribute(PropertyInfo pi)
    {
      var columnAttributes = (PersistantPropertyAttribute[])pi.GetCustomAttributes(typeof(PersistantPropertyAttribute), false);
      if (columnAttributes.Length == 0) throw new PersistantPropertyException(pi.Name, "PersistantPropertyAttribute is undefined.");
      return columnAttributes[0];
    }

    PersistantCollectionAttribute GetPersistantCollectionAttribute(PropertyInfo pi)
    {
      var attributes = (PersistantCollectionAttribute[])pi.GetCustomAttributes(typeof(PersistantCollectionAttribute), false);
      if (attributes.Length == 0) throw new PersistantCollectionException(pi.Name, "PersistantCollectionAttribute is undefined.");
      return attributes[0];
    }

    bool ApplyFilter(IDataFilter condition, PersistantProperty pp)
    {
      if (ObjectType == condition.ObjectType || condition.ObjectType.IsSubclassOf(ObjectType) || ContainedBaseClasses.Contains(condition.ObjectType)) // && pp != null && !PersistToDerivedClass
      {
        return pp.PropertyName == condition.Property;
      }
      return false;
    }

    PersistantProperty GetProperty(string name)
    {
      PersistantProperty pp = Properties.FirstOrDefault((p) => p.PropertyName == name);
      if (pp != null) return pp;
      if (BaseRepository != null) return BaseRepository.GetProperty(name);
      return null;
    }

    #endregion

    #endregion

    #region IInternalObjectRepository

    int IInternalObjectRepository.DerivedClassCount
    {
      get { return DerivedClasses.Count; }
    }

    //string IInternalObjectRepository.GetHashingString(IBusinessObject obj)
    //{
    //  return GetHashingString((TObject)obj);
    //}

    BusinessObject IInternalObjectRepository.GetInstance(int id)
    {
      return GetInstance(id);
    }

    PersistantProperty IInternalObjectRepository.NamespaceProperty
    {
      get { return NamespaceProperty; }
      set { NamespaceProperty = value; }
    }
    BusinessObject IInternalObjectRepository.GetInstance(Guid globalIdentifier)
    {
      return GetInstance(globalIdentifier);
    }

    string IInternalObjectRepository.GetIdColumn()
    {
      return GetIdColumn();
    }

    bool IInternalObjectRepository.Persist(IBusinessObject obj, Stack<BusinessObject> persisted, PersistanceFlags flags)
    {
      return PersistInternal((TObject)obj, persisted, flags);
    }

    void IInternalObjectRepository.Persist(IBusinessObject obj, PersistanceFlags flags)
    {
      Persist((TObject)obj, flags);
    }

    IEnumerable<PersistantCollection> IInternalObjectRepository.HeirarchyPersistantCollections
    {
      get { return HeirarchyPersistantCollections; }
    }

    void IInternalObjectRepository.Persist(IList col, PersistanceFlags flags)
    {
      Persist(new BasicCollection<TObject>(col.Cast<TObject>()));
    }

    void IInternalObjectRepository.AddSortColumns(QueryStatement qs)
    {
      AddSortColumns(qs);
    }

    IEnumerable IInternalObjectRepository.GetAllInstances()
    {
      return GetAllInstances();
    }
    bool IInternalObjectRepository.IsOwned
    {
      get { return IsOwned; }
    }

    string IInternalObjectRepository.IdColumn
    {
      get { return IdColumn; }
    }

    string IInternalObjectRepository.GlobalIdentifierColumn
    {
      get { return GlobalIdentifierColumn; }
    }

    string IInternalObjectRepository.OwnerColumn
    {
      get { return OwnerColumn; }
    }

    Type IInternalObjectRepository.OwnerType
    {
      get { return OwnerType; }
    }

    void IInternalObjectRepository.ResolveObject(IBusinessObject obj, TypedDataRow dr)
    {
      ResolveObject((TObject)obj, dr);
    }

    IEnumerable<PersistantProperty> IInternalObjectRepository.Properties
    {
      get { return Properties; }
    }

    bool IInternalObjectRepository.ApplyFilter(IDataFilter condition, PersistantProperty pp)
    {
      return ApplyFilter(condition, pp);
    }

    IInternalPersistanceManager IInternalObjectRepository.PersistanceManager
    {
      get { return PersistanceManager; }
    }
    Type IInternalObjectRepository.ObjectType
    {
      get { return mObjectType; }
    }

    void IInternalObjectRepository.AddBaseJoinClauses(QueryStatement qs)
    {
      AddBaseJoinClauses(qs);
    }

    IInternalObjectRepository IInternalObjectRepository.BaseRepository
    {
      get { return mBaseRepository; }
    }

    IInternalObjectRepository IInternalObjectRepository.RootRepository
    {
      get
      {
        if (mBaseRepository == null) return this;
        return mBaseRepository.RootRepository;
      }
    }

    string IInternalObjectRepository.GetFullTableName()
    {
      return GetFullTableName();
    }

    string IInternalObjectRepository.GetFullIdColumn()
    {
      return GetFullIdColumn();
    }

    string IInternalObjectRepository.GetFullGlobalIdentifierColumn()
    {
      return GetFullGlobalIdentifierColumn();
    }

    bool IInternalObjectRepository.IsAssociative
    {
      get { return IsAssociative; }
    }

    IEnumerable<PersistantProperty> IInternalObjectRepository.ReadProperties
    {
      get { return ReadProperties; }
    }

    IEnumerable<PersistantProperty> IInternalObjectRepository.HeirarchyReadProperties
    {
      get { return HeirarchyReadProperties; }
    }

    void IInternalObjectRepository.AddFilters(QueryStatement qs, ICollection<IDataFilter> conditions)
    {
      AddFilters(qs, conditions);
    }

    IEnumerable<PersistantProperty> IInternalObjectRepository.WriteProperties
    {
      get { return WriteProperties; }
    }

    PersistantProperty IInternalObjectRepository.GetProperty(string name)
    {
      return GetProperty(name);
    }

    //IEnumerable<PersistantCollection> IInternalObjectRepository.PersistantCollections
    //{
    //  get { return PersistantCollections; }
    //}

    void IInternalObjectRepository.FetchData(Collection<IDataFilter> filters, QueryContext context)
    {
      FetchData(filters, context);
    }

    void IInternalObjectRepository.AddQueryColumns(QueryStatement qs)
    {
      AddQueryColumns(qs);
    }

    #endregion
  }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.Data
{
  /// <summary>
  /// The object or owned collection should be cached.
  /// </summary>
  [AttributeUsage(AttributeTargets.Class)]
  public class CacheAttribute : Attribute
  {
    int mPriority = -1;
    public int Priority
    {
      get { return mPriority; }
      set { mPriority = value; }
    }

    #region bool Replicate

    bool mReplicate = true;
    public bool Replicate
    {
      get { return mReplicate; }
      set { mReplicate = value; }
    }

    #endregion

    #region bool SuppressRevision

    bool mSuppressRevision = false;
    public bool SuppressRevision
    {
      get { return mSuppressRevision; }
      set { mSuppressRevision = value; }
    }

    #endregion
  }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.Data
{
  public interface IPersistanceEventManager
  {
    Type TargetType { get; }
    IPersistanceManager PersistanceManager { get; set; }
    void BeforeDelete(IBusinessObject obj, PersistanceFlags flags);
    void AfterDelete(IBusinessObject obj, PersistanceFlags flags);
    bool BeforePersist(IBusinessObject obj, PersistanceFlags flags);
    void AfterPersist(IBusinessObject obj, PersistanceFlags flags);
  }
}

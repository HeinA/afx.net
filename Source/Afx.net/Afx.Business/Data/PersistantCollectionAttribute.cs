﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.Data
{
  [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
  public sealed class PersistantCollectionAttribute : Attribute
  {
    public PersistantCollectionAttribute()
    {
      //IsAggregate = true;
    }

    public Type AssociativeType { get; set; }


    //public bool IsAggregate { get; set; }
  }
}

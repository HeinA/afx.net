﻿using Afx.Business.Documents;
using Afx.Business.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.Data
{
  public interface IDocumentStateEventManager
  {
    Type TargetType { get; }
    DocumentTypeState TargetState { get; }
    void Process(Document doc, DocumentTypeState originalState, DocumentStateEventScope scope);

    IPersistanceManager PersistanceManager { get; set; }
  }
}

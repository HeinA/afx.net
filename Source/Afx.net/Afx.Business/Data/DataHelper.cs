﻿using ClosedXML.Excel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.Data
{
  public class DataHelper
  {
    public static DataSet ExecuteDataSet(IDbCommand cmd)
    {
      try
      {
        //LogHelper.LogSql(cmd);
        System.Data.DataSet ds = new System.Data.DataSet();
        ds.EnforceConstraints = false;
        ds.Locale = CultureInfo.InvariantCulture;
        using (IDataReader r = cmd.ExecuteReader())
        {
          ds.Load(r, LoadOption.OverwriteChanges, string.Empty);
          r.Close();
        }
        return ds;
      }
      catch
      {
        throw;
      }
    }

    public static byte[] ExecuteExcel(IDbCommand cmd)
    {
      DataSet ds = ExecuteDataSet(cmd);

      try
      {
        using (var wb = new XLWorkbook())
        {
          IXLWorksheet sheet = wb.Worksheets.Add("Export");
          int row = 1;
          int column = 1;
          foreach (DataColumn dc in ds.Tables[0].Columns)
          {
            sheet.Cell(row, column++).SetValue(dc.ColumnName).Style.Font.SetBold();
          }

          foreach (DataRow dr in ds.Tables[0].Rows)
          {
            row++;
            for (int i = 1; i <= ds.Tables[0].Columns.Count; i++)
            {
              sheet.Cell(row, i).SetValue(dr[i - 1]);
            }
          }

          using (var ms = new MemoryStream())
          {
            wb.SaveAs(ms);
            return ms.ToArray();
          }
        }
      }
      catch (Exception ex)
      {
        if (ExceptionHelper.HandleException(ex)) throw ex;
      }
      
      return null;
    }
  }
}

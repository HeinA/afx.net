﻿using Afx.Business.Documents;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.Data
{
  interface IInternalPersistanceManager : IPersistanceManager
  {
    //string GetIdentifier(string name);
    IInternalObjectRepository GetRepository(Type objectType);
    IEnumerable<IPersistanceEventManager> GetEventManagers(Type objectType);
    IEnumerable<IDocumentStateEventManager> GetDocumentStateEventManagers(Type objectType);
    IEnumerable<IDocumentTransferManager> GetDocumentTransferManagers(Type objectType);
    //IDbCommand GetCommand();
    object CastToDatabaseType(object value);
    object CastToObjectType(object value);
    string GetFilterText(FilterType filter);
    string GetSortText(SortDirection sort);
    //string AddParameter(object name, object value, IDbCommand command);
    IEnumerable<Type> BusinessClasses
    {
      get;
    }
    IEnumerable<Type> CachedClasses
    {
      get;
    }
  }
}

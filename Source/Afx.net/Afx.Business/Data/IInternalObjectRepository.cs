﻿using Afx.Business;
using Afx.Business.Collections;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.Data
{
  interface IInternalObjectRepository
  {
    SearchResults Search(Collection<IDataFilter> filters);
    IInternalPersistanceManager PersistanceManager { get; }
    IInternalObjectRepository BaseRepository { get; }
    IInternalObjectRepository RootRepository { get; }
    Type ObjectType { get; }
    IEnumerable<PersistantProperty> Properties { get; }
    IEnumerable<PersistantProperty> ReadProperties { get; }
    IEnumerable<PersistantProperty> HeirarchyReadProperties { get; }
    IEnumerable<PersistantCollection> HeirarchyPersistantCollections { get; }
    IEnumerable<PersistantProperty> WriteProperties { get; }
    IEnumerable<Type> GetHeirarchyExtensionObjects();
    int DerivedClassCount { get; }
    IEnumerable GetAllInstances();
    BusinessObject GetInstance(int id);
    BusinessObject GetInstance(Guid globalIdentifier);
    PersistantProperty GetProperty(string name);
    string IdColumn { get; }
    string GlobalIdentifierColumn { get; }
    string OwnerColumn { get; }
    string ReferenceColumn { get; }
    PersistantProperty NamespaceProperty { get; set; }
    bool IsOwned { get; }
    Type OwnerType { get; }
    bool IsAssociative { get; }
    void AddFilters(QueryStatement qs, ICollection<IDataFilter> conditions);
    string GetFullTableName();
    string GetFullIdColumn();
    string GetIdColumn();
    string GetFullGlobalIdentifierColumn();
    void ResolveObject(IBusinessObject obj, TypedDataRow dr);
    void FetchData(Collection<IDataFilter> filters, QueryContext context);
    bool ApplyFilter(IDataFilter condition, PersistantProperty pp);
    void AddQueryColumns(QueryStatement qs);
    void AddBaseJoinClauses(QueryStatement qs);
    void AddSortColumns(QueryStatement qs);
    bool Persist(IBusinessObject obj, Stack<BusinessObject> persisted, PersistanceFlags flags);
    void Persist(IBusinessObject obj, PersistanceFlags flags);
    void Persist(IList col, PersistanceFlags flags);
    //string GetHashingString(IBusinessObject obj);
  }
}

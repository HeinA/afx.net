﻿using Afx.Business.Collections;
using Afx.Business.Service;
using Afx.Business.ServiceModel;
using Microsoft.Practices.Unity;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.Data
{

  /// <summary>
  /// TODO:
  ///   Add multiple sorts (Nice to have)
  /// </summary>
  public class Cache
  {
    #region static Cache Instance

    static Cache mInstance;
    public static Cache Instance
    {
      get
      {
        if (mInstance == null) mInstance = new Cache();
        return mInstance;
      }
    }

    #endregion

    object mFetchLock = new object();

    #region BasicCollection<BusinessObject> FetchCache(IPersistanceManager persistanceManager)

    public static BasicCollection<BusinessObject> FetchCache(IPersistanceManager persistanceManager)
    {
      try
      {
        using (new ConnectionScope())
        using (new SqlScope("Cache Load"))
        {
          Stopwatch sw = new Stopwatch();
          sw.Start();

          IdMapper.Instance.RefreshCachedIds();

          IInternalPersistanceManager pm = (IInternalPersistanceManager)persistanceManager;
          BasicCollection<BusinessObject> col = new BasicCollection<BusinessObject>();
          foreach (Type t in pm.CachedClasses)
          {
            IInternalObjectRepository or = pm.GetRepository(t);
            foreach (var obj in or.GetAllInstances())
            {
              col.Add((BusinessObject)obj);
            }
          }

          sw.Stop();
          //LogHelper.LogMessage("Cache Load: {0:#,##0}ms", sw.ElapsedMilliseconds);
          return col;
        }
      }
      catch
      {
        throw;
      }
    }

    #endregion

    public bool Contains(Guid guid)
    {
      return mGuidCollection.Contains(guid);
    }

    #region void LoadCache(BasicCollection<BusinessObject> col)

    public void LoadCache(BasicCollection<BusinessObject> col)
    {
      lock (mLock)
      {
        mObjectDictionary = null;
        mGuidCollection = new Collection<Guid>();
        BasicCollection<BusinessObject> loaded = new BasicCollection<BusinessObject>();
        foreach (BusinessObject bo in col)
        {
          LoadObject(bo, loaded);
        }
      }
      RefreshCacheCollections();

      if (CacheUpdated != null) CacheUpdated(this, EventArgs.Empty);
    }

    public void AppendObject(BusinessObject bo)
    {
      lock (mLock)
      {
        BasicCollection<BusinessObject> loaded = new BasicCollection<BusinessObject>();
        LoadObject(bo, loaded);
      }
      RefreshCacheCollections();

      if (CacheUpdated != null) CacheUpdated(this, EventArgs.Empty);
    }

    void LoadObject(BusinessObject bo, BasicCollection<BusinessObject> loaded)
    {
      if (loaded.Contains(bo)) return;
      loaded.Add(bo);      
      mGuidCollection.Add(bo.GlobalIdentifier);

      Type t = bo.GetType();
      while (t != typeof(BusinessObject))
      {
        if (!ObjectDictionary.ContainsKey(t)) ObjectDictionary.Add(t, new CollectionDictionary());
        CollectionDictionary colDict = ObjectDictionary[t];

        if (colDict.Collection.Contains(bo)) colDict.Collection.Remove(bo);
        colDict.Collection.Add(bo);

        if (colDict.IdDictionary.ContainsKey(bo.Id)) colDict.IdDictionary.Remove(bo.Id);
        colDict.IdDictionary.Add(bo.Id, bo);

        if (colDict.IdentifierDictionary.ContainsKey(bo.GlobalIdentifier)) colDict.IdentifierDictionary.Remove(bo.GlobalIdentifier);
        colDict.IdentifierDictionary.Add(bo.GlobalIdentifier, bo);

        t = t.BaseType;
      }

      foreach (PropertyInfo pi in bo.GetType().GetProperties(BindingFlags.Instance | BindingFlags.Public))
      {
        if (typeof(IList).IsAssignableFrom(pi.PropertyType))
        {
          IList list = pi.GetValue(bo) as IList;
          if (list != null)
          {
            foreach (object oo in list)
            {
              BusinessObject bo2 = oo as BusinessObject;
              if (bo2 != null) LoadObject(bo2, loaded);
            }
          }
        }
      }
    }

    #endregion

    // UNDONE: Clone
    //#region Clone

    //BasicCollection<T> CloneCollection<T>(BasicCollection<T> source)
    //{
    //  using (MemoryStream stream = new MemoryStream())
    //  {
    //    NetDataContractSerializer dcs = new NetDataContractSerializer();
    //    dcs.WriteObject(stream, source);
    //    stream.Position = 0;
    //    return (BasicCollection<T>)dcs.ReadObject(stream);
    //  }
    //}

    //T Clone<T>(T source)
    //{
    //  using (MemoryStream stream = new MemoryStream())
    //  {
    //    NetDataContractSerializer dcs = new NetDataContractSerializer();
    //    dcs.WriteObject(stream, source);
    //    stream.Position = 0;
    //    return (T)dcs.ReadObject(stream);
    //  }
    //}

    //#endregion

    public event EventHandler<EventArgs> CacheUpdated;

    T GetNullObject<T>()
      where T : BusinessObject
    {
      ConstructorInfo ci = typeof(T).GetConstructor(new Type[] { typeof(bool) });
      if (ci == null) return null; 
      return (T)ci.Invoke(new object[] { true });
    }

    #region T GetObject<T>(...)

    //public T GetObject<T>(int id)
    //  where T : BusinessObject
    //{
    //  lock (_lock)
    //  {
    //    if (id == 0) return GetNullObject<T>();
    //    if (!ObjectDictionary.ContainsKey(typeof(T))) return null;
    //    CollectionDictionary colDict = ObjectDictionary[typeof(T)];
    //    if (!colDict.IdDictionary.ContainsKey(id)) return null;
    //    return Clone<T>((T)colDict.IdDictionary[id]);
    //  }
    //}

    internal BusinessObject GetObject(Type objectType, int id)
    {
      lock (mLock)
      {
        if (!ObjectDictionary.ContainsKey(objectType)) return null;
        CollectionDictionary colDict = ObjectDictionary[objectType];
        if (!colDict.IdDictionary.ContainsKey(id)) return null;
        return (BusinessObject)colDict.IdDictionary[id]; //.Clone(); // UNDONE: Clone
      }
    }

    public T GetObject<T>(string identifier)
      where T : BusinessObject
    {
      return GetObject<T>(Guid.Parse(identifier));
    }

    public T GetObject<T>(Guid identifier)
      where T : BusinessObject
    {
      lock (mLock)
      {
        if (identifier == Guid.Empty) return GetNullObject<T>();
        if (!ObjectDictionary.ContainsKey(typeof(T))) return GetLoadObject<T>(identifier);
        CollectionDictionary colDict = ObjectDictionary[typeof(T)];
        if (!colDict.IdentifierDictionary.ContainsKey(identifier)) return GetLoadObject<T>(identifier);
        return (T)colDict.IdentifierDictionary[identifier];
        //return Clone<T>((T)colDict.IdentifierDictionary[identifier]); // UNDONE: Clone
      }
    }

    T GetLoadObject<T>(Guid identifier)
      where T : BusinessObject
    {
      if (OperationContext.Current != null && ConnectionScope.Current != null)
      {
        IPersistanceManager pm = UnityProvider.Instance.Resolve<IPersistanceManager>();
        ObjectRepository<T> or = pm.GetRepository<T>();
        T obj = or.GetInstance(identifier);
        if (obj != null)
        {
          AppendObject(obj);
          return obj;
        }
      }
      else
      {
      }
      return null;
    }

    #endregion

    #region BasicCollection<T> GetObjects<T>()

    public BasicCollection<T> GetObjects<T>()
      where T : BusinessObject
    {
      return GetObjects<T>(false);
    }

    public BasicCollection<T> GetObjects<T>(bool includeNull)
      where T : BusinessObject
    {
      CacheCollection<T> cc = new CacheCollection<T>(includeNull);
      RegisterCacheCollection(cc);
      ((ICacheCollection)cc).Refresh();
      return cc;
    }

    internal BasicCollection<T> GetObjectsInternal<T>()
      where T : BusinessObject
    {
      lock (mLock)
      {
        if (ObjectDictionary.ContainsKey(typeof(T)))
        {
          return new BasicCollection<T>(ObjectDictionary[typeof(T)].Collection.Cast<T>().ToList<T>());
          //return CloneCollection<T>(new BasicCollection<T>(ObjectDictionary[typeof(T)].Collection.Cast<T>().ToList<T>())); // UNDONE: Clone
        }
        return new BasicCollection<T>();
      }
    }

    #endregion

    #region BasicCollection<T> GetObjects<T>(Func<T, bool> predicate)

    public BasicCollection<T> GetObjects<T>(Func<T, bool> predicate)
      where T : BusinessObject
    {
      return GetObjects<T>(false, predicate);
    }

    public BasicCollection<T> GetObjects<T>(bool includeNull, Func<T, bool> predicate)
      where T : BusinessObject
    {
      CacheCollection<T> cc = new CacheCollection<T>(includeNull, predicate);
      RegisterCacheCollection(cc);
      ((ICacheCollection)cc).Refresh();
      return cc;
    }

    internal BasicCollection<T> GetObjectsInternal<T>(Func<T, bool> predicate)
      where T : BusinessObject
    {
      lock (mLock)
      {
        if (ObjectDictionary.ContainsKey(typeof(T)))
        {
          return new BasicCollection<T>(ObjectDictionary[typeof(T)].Collection.Cast<T>().Where<T>(predicate).ToList<T>());
          // return CloneCollection<T>(new BasicCollection<T>(ObjectDictionary[typeof(T)].Collection.Cast<T>().Where<T>(predicate).ToList<T>())); // UNDONE: Clone
        }
        return new BasicCollection<T>();
      }
    }

    #endregion

    #region BasicCollection<T> GetObjects<T>(Func<T, object> sort, bool ascending)

    public BasicCollection<T> GetObjects<T>(Func<T, object> sort, bool ascending)
      where T : BusinessObject
    {
      return GetObjects<T>(false, sort, ascending);
    }

    public BasicCollection<T> GetObjects<T>(bool includeNull, Func<T, object> sort, bool ascending)
      where T : BusinessObject
    {
      CacheCollection<T> cc = new CacheCollection<T>(includeNull, sort, ascending);
      RegisterCacheCollection(cc);
      ((ICacheCollection)cc).Refresh();
      return cc;
    }

    internal BasicCollection<T> GetObjectsInternal<T>(Func<T, object> sort, bool ascending)
      where T : BusinessObject
    {
      lock (mLock)
      {
        if (ObjectDictionary.ContainsKey(typeof(T)))
        {
          // UNDONE: Clone
          //if (ascending) return CloneCollection<T>(new BasicCollection<T>(ObjectDictionary[typeof(T)].Collection.Cast<T>().OrderBy(sort).ToList<T>()));
          //else return CloneCollection<T>(new BasicCollection<T>(ObjectDictionary[typeof(T)].Collection.Cast<T>().OrderByDescending(sort).ToList<T>()));

          if (ascending) return new BasicCollection<T>(ObjectDictionary[typeof(T)].Collection.Cast<T>().OrderBy(sort).ToList<T>());
          else return new BasicCollection<T>(ObjectDictionary[typeof(T)].Collection.Cast<T>().OrderByDescending(sort).ToList<T>());
        }
        return new BasicCollection<T>();
      }
    }

    #endregion

    #region BasicCollection<T> GetObjects<T>(Func<T, bool> predicate, Func<T, object> sort, bool ascending)
    public BasicCollection<T> GetObjects<T>(Func<T, bool> predicate, Func<T, object> sort, bool ascending)
      where T : BusinessObject
    {
      return GetObjects<T>(false, predicate, sort, ascending);
    }

    public BasicCollection<T> GetObjects<T>(bool includeNull, Func<T, bool> predicate, Func<T, object> sort, bool ascending)
      where T : BusinessObject
    {
      CacheCollection<T> cc = new CacheCollection<T>(includeNull, predicate, sort, ascending);
      RegisterCacheCollection(cc);
      ((ICacheCollection)cc).Refresh();
      return cc;
    }

    internal BasicCollection<T> GetObjectsInternal<T>(Func<T, bool> predicate, Func<T, object> sort, bool ascending)
      where T : BusinessObject
    {
      lock (mLock)
      {
        if (ObjectDictionary.ContainsKey(typeof(T)))
        {
          // UNDONE: Clone
          //if (ascending) return CloneCollection<T>(new BasicCollection<T>(ObjectDictionary[typeof(T)].Collection.Cast<T>().Where<T>(predicate).OrderBy(sort).ToList<T>()));
          //else return CloneCollection<T>(new BasicCollection<T>(ObjectDictionary[typeof(T)].Collection.Cast<T>().Where<T>(predicate).OrderByDescending(sort).ToList<T>()));

          if (ascending) return new BasicCollection<T>(ObjectDictionary[typeof(T)].Collection.Cast<T>().Where<T>(predicate).OrderBy(sort).ToList<T>());
          else return new BasicCollection<T>(ObjectDictionary[typeof(T)].Collection.Cast<T>().Where<T>(predicate).OrderByDescending(sort).ToList<T>());
        }
        return new BasicCollection<T>();
      }
    }

    #endregion



    #region Private

    static readonly object mLock = new object();

    #region class CollectionDictionary

    class CollectionDictionary
    {
      BasicCollection<BusinessObject> mCollection;
      public BasicCollection<BusinessObject> Collection
      {
        get
        {
          if (mCollection == null) mCollection = new BasicCollection<BusinessObject>();
          return mCollection;
        }
      }

      Dictionary<int, BusinessObject> mIdDictionary;
      public Dictionary<int, BusinessObject> IdDictionary
      {
        get
        {
          if (mIdDictionary == null) mIdDictionary = new Dictionary<int, BusinessObject>();
          return mIdDictionary;
        }
      }

      Dictionary<Guid, BusinessObject> mIdentifierDictionary;
      public Dictionary<Guid, BusinessObject> IdentifierDictionary
      {
        get
        {
          if (mIdentifierDictionary == null) mIdentifierDictionary = new Dictionary<Guid, BusinessObject>();
          return mIdentifierDictionary;
        }
      }
    }

    #endregion

    #region Collection<WeakReference> CacheCollections

    Collection<WeakReference> mCacheCollections;
    Collection<WeakReference> CacheCollections
    {
      get { return mCacheCollections ?? (mCacheCollections = new Collection<WeakReference>()); }
    }

    #endregion

    #region RegisterCacheCollection(ICacheCollection cacheCollection)

    void RegisterCacheCollection(ICacheCollection cacheCollection)
    {
      lock (mLock)
      {
        CacheCollections.Add(new WeakReference(cacheCollection));
      }
    }

    #endregion

    #region void RefreshCacheCollections()

    void RefreshCacheCollections()
    {
      try
      {
        Stack<WeakReference> deadCollections = new Stack<WeakReference>();
        lock (mLock)
        {
          foreach (WeakReference wr in CacheCollections.ToArray())
          {
            if (!wr.IsAlive)
            {
              try
              {
                deadCollections.Push(wr);
              }
              catch
              {
                throw;
              }
            }
            else
            {
              try
              {
                ((ICacheCollection)wr.Target).Refresh();
              }
              catch
              {
                throw;
              }
            }
          }

          while (deadCollections.Count > 0)
          {
            WeakReference wr = deadCollections.Pop();
            CacheCollections.Remove(wr);
          }
        }
      }
      catch
      {
        if (Debugger.IsAttached) Debugger.Break();
        throw;
      }
    }

    #endregion

    Collection<Guid> mGuidCollection = new Collection<Guid>();

    #region Dictionary<Type, CollectionDictionary> ObjectDictionary

    Dictionary<Type, CollectionDictionary> mObjectDictionary;
    Dictionary<Type, CollectionDictionary> ObjectDictionary
    {
      get
      {
        if (mObjectDictionary == null) mObjectDictionary = new Dictionary<Type, CollectionDictionary>();
        return mObjectDictionary;
      }
    }

    #endregion

    #endregion
  }
}

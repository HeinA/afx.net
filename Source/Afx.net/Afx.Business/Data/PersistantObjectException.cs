﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.Data
{
  [Serializable]
  public class PersistantObjectException : Exception
  {
    protected PersistantObjectException(SerializationInfo info, StreamingContext context)
      :base(info, context)
    {
    }

    public PersistantObjectException(Type objectType, string message)
      : base(string.Format("{0}: {1}", objectType, message))
    {
    }

    public PersistantObjectException(Type objectType, string message, Exception innerException)
      : base(string.Format("{0}: {1}", objectType, message), innerException)
    {
    }
  }
}

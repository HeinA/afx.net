﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Threading.Tasks;
using Afx.Business.Collections;
using System.Collections.ObjectModel;
using Afx.Business.Configuration;

namespace Afx.Business.Data
{
  public static class ObjectAttributeHelper
  {
    static Dictionary<Type, bool> mIsCached = new Dictionary<Type, bool>();
    static Collection<Type> mEditableCachedTypes = new Collection<Type>();
    static Collection<Type> mReplicate = new Collection<Type>();
    static Dictionary<Type, Collection<Type>> mExtensionObjectDictionary = new Dictionary<Type, Collection<Type>>();
    static Collection<Type> mExtensionObjects = new Collection<Type>();

    static object mLock = new object();

    static ObjectAttributeHelper()
    {
      lock (mLock)
      {
        Queue<Type> queue = new Queue<Type>();
        //Utilities.PreLoadDeployedAssemblies();

        //Collection<Type> allowedTypes = new Collection<Type>();
        //foreach(AllowedType at in ExtensibilityConfiguration.Default.AllowedTypes)
        //{
        //  Type t = (Type)Type.GetType(at.TypeName);
        //  allowedTypes.Add(t);
        //}

        foreach (Assembly a in AppDomain.CurrentDomain.GetAssemblies())
        {
          try
          {
            foreach (Type type in a.GetTypes().Where(t => t.IsSubclassOf(typeof(BusinessObject))))
            {
              if (type.IsDefined(typeof(CacheAttribute))) AddCacheType(type, queue);
              if (type.IsDefined(typeof(ReplicateAttribute))) queue.Enqueue(type);
              if (type != typeof(ExtensionObject<>))
              {
                Type extension = GetGenericSubClass(typeof(ExtensionObject<>), type);
                if (extension != null) // && allowedTypes.Contains(type))
                {
                  Type owner = extension.GetGenericArguments()[0];
                  if (!mExtensionObjectDictionary.ContainsKey(owner)) mExtensionObjectDictionary.Add(owner, new Collection<Type>());
                  Collection<Type> col = mExtensionObjectDictionary[owner];
                  col.Add(type);
                  mExtensionObjects.Add(type);
                }
              }
            }
          }
          catch
          {
            throw;
          }
        }

        while (queue.Count > 0)
        {
          mReplicate.Add(queue.Dequeue());
        }
      }
    }

    public static IEnumerable<Type> GetExtensionObjects(Type owner)
    {
      if (!mExtensionObjectDictionary.ContainsKey(owner)) return new Collection<Type>();
      return mExtensionObjectDictionary[owner];
    }

    public static IEnumerable<Type> ExtensionObjects
    {
      get { return mExtensionObjects.OrderBy(eo => eo.FullName); }
    }

    static void AddCacheType(Type type, Queue<Type> replicationQueue)
    {
      //if (type == null) return;
      //if (type.GetCustomAttribute<NonCacheable>() != null) return;
      if (mIsCached.ContainsKey(type)) return;
      mIsCached.Add(type, true);

      //if (type.BaseType != typeof(BusinessObject))
      //{
      //  AddCacheType(type.BaseType, replicationQueue);
      //}

      CacheAttribute ca = type.GetCustomAttribute<CacheAttribute>();
      if (ca != null && ca.Replicate)
      {
        if (!typeof(IRevisable).IsAssignableFrom(type) && !ca.SuppressRevision) 
          throw new InvalidOperationException(string.Format("Object of type {0} must implement IRevisable", type));
        
        mEditableCachedTypes.Add(type);

        if (ca != null && ca.Priority >= 0)
        {
          if (ca.Priority > mReplicate.Count) mReplicate.Add(type);
          else mReplicate.Insert(ca.Priority, type);
        }
        else
        {
          replicationQueue.Enqueue(type);
        }
      }

      foreach (Assembly a in AppDomain.CurrentDomain.GetAssemblies())
      {
        try
        {
          foreach (Type t in a.GetTypes().Where(t1 => t1.IsSubclassOf(type)))
          {
            AddCacheType(t, replicationQueue);
          }
        }
        catch
        {
          throw;
        }
      }

      foreach (PropertyInfo pi in type.GetProperties(BindingFlags.Instance | BindingFlags.Public).Where(prop => prop.IsDefined(typeof(PersistantCollectionAttribute), false)))
      {
        PersistantCollectionAttribute pca = GetPersistantCollectionAttribute(pi);
        Type itemType = pi.PropertyType.GetGenericArguments()[0];        
        AddCacheType(itemType, replicationQueue);
        if (pca.AssociativeType != null)
        {
          AddCacheType(pca.AssociativeType, replicationQueue);
        }
      }
    }

    public static bool IsCached(Type type)
    {
      if (!mIsCached.ContainsKey(type)) return false;
      return mIsCached[type];      
    }

    public static bool MustReplicate(Type type)
    {
      return mReplicate.Contains(type);
    }

    public static IEnumerable<Type> ReplicationTypes
    {
      get { return mReplicate; }
    }

    public static IEnumerable<Type> EditableCachedTypes
    {
      get { return mEditableCachedTypes; }
    }

    static Type GetGenericSubClass(Type generic, Type toCheck)
    {
      while (toCheck != null && toCheck != typeof(object))
      {
        var cur = toCheck.IsGenericType ? toCheck.GetGenericTypeDefinition() : toCheck;
        if (generic == cur)
        {
          return toCheck;
        }
        toCheck = toCheck.BaseType;
      }
      return null;
    }

    static PersistantCollectionAttribute GetPersistantCollectionAttribute(PropertyInfo pi)
    {
      var attributes = (PersistantCollectionAttribute[])pi.GetCustomAttributes(typeof(PersistantCollectionAttribute), false);
      if (attributes.Length == 0) throw new PersistantCollectionException(pi.Name, "PersistantCollectionAttribute is undefined.");
      return attributes[0];
    }
  }
}

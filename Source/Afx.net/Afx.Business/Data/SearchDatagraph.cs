﻿using Afx.Business.Collections;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business.Data
{
  [DataContract(Namespace = Namespace.Afx, IsReference = true)]
  public class SearchDatagraph
  {
    [DataContract(Namespace = Namespace.Afx, IsReference = true)]
    internal class sdgResult
    {
      int mIndex;
      [DataMember]
      public int Index
      {
        get { return mIndex; }
        set { mIndex = value; }
      }

      string mObjectType;
      [DataMember]
      public string ObjectType
      {
        get { return mObjectType; }
        set { mObjectType = value; }
      }

      string mPropertyName;
      [DataMember]
      public string PropertyName
      {
        get { return mPropertyName; }
        set { mPropertyName = value; }
      }

      string mAlias;
      [DataMember]
      public string Alias
      {
        get { return mAlias; }
        set { mAlias = value; }
      }

      string mFormat;
      [DataMember]
      public string Format
      {
        get { return mFormat; }
        set { mFormat = value; }
      }
    }

    [DataContract(Namespace = Namespace.Afx, IsReference = true)]
    internal class sdgWhere
    {
      string mObjectType;
      [DataMember]
      public string ObjectType
      {
        get { return mObjectType; }
        set { mObjectType = value; }
      }

      string mAlias;
      [DataMember]
      public string Alias
      {
        get { return mAlias; }
        set { mAlias = value; }
      }

      string mPropertyName;
      [DataMember]
      public string PropertyName
      {
        get { return mPropertyName; }
        set { mPropertyName = value; }
      }

      FilterType mFilterType;
      [DataMember]
      public FilterType FilterType
      {
        get { return mFilterType; }
        set { mFilterType = value; }
      }

      object mFilter;
      [DataMember]
      public object Filter
      {
        get { return mFilter; }
        set { mFilter = value; }
      }
    }

    [DataMember]
    Collection<SearchDatagraph> mJoins;
    Collection<SearchDatagraph> Joins
    {
      get { return mJoins ?? (mJoins = new Collection<SearchDatagraph>()); }
    }

    [DataMember]
    Collection<SearchDatagraph> mBaseJoins;
    Collection<SearchDatagraph> BaseJoins
    {
      get { return mBaseJoins ?? (mBaseJoins = new Collection<SearchDatagraph>()); }
    }

    [DataMember]
    Collection<sdgResult> mResults;
    Collection<sdgResult> Results
    {
      get { return mResults ?? (mResults = new Collection<sdgResult>()); }
    }

    [DataMember]
    Collection<sdgWhere> mWhere;
    Collection<sdgWhere> WhereClauses
    {
      get { return mWhere ?? (mWhere = new Collection<sdgWhere>()); }
    }

    [DataMember]
    int mAliasCount = 1;
    string GetAlias()
    {
      return string.Format("A{0}", Root.mAliasCount++);
    }

    [DataMember]
    string mAlias;
    string Alias
    {
      get { return mAlias; }
    }

    [DataMember]
    string mJoinProperty;
    string JoinProperty
    {
      get { return mJoinProperty; }
    }

    public SearchDatagraph(Type objectType)
    {
      mObjectType = objectType;
      mObjectTypeName = TypeHelper.GetTypeName(objectType);
      mAlias = GetAlias();
      Select(BusinessObject.GlobalIdentifierProperty, "GlobalIdentifier");
    }

    SearchDatagraph(Type objectType, SearchDatagraph parent, string joinProperty)
    {
      mObjectType = objectType;
      mObjectTypeName = TypeHelper.GetTypeName(objectType);
      mParent = parent;
      mJoinProperty = joinProperty;
      mAlias = GetAlias();
    }

    SearchDatagraph(Type objectType, SearchDatagraph parent)
    {
      mObjectType = objectType;
      mObjectTypeName = TypeHelper.GetTypeName(objectType);
      mParent = parent;
      mAlias = GetAlias();
    }

    SearchDatagraph(Type objectType, Type associativeType, SearchDatagraph parent)
    {
      mObjectType = objectType;
      mAssociativeType = associativeType;
      mObjectTypeName = TypeHelper.GetTypeName(objectType);
      mAssociativeTypeName = TypeHelper.GetTypeName(associativeType);
      mList = true;
      mParent = parent;
      mAlias = GetAlias();
    }

    Type mObjectType;
    Type ObjectType
    {
      get { return mObjectType ?? (mObjectType = mObjectTypeName == null ? null : Type.GetType(mObjectTypeName)); }
    }

    [DataMember]
    string mObjectTypeName = null;


    Type mAssociativeType;
    Type AssociativeType
    {
      get { return mAssociativeType ?? (mAssociativeType = mAssociativeTypeName == null ? null : Type.GetType(mAssociativeTypeName)); }
    }

    [DataMember]
    string mAssociativeTypeName = null;


    [DataMember]
    int mResultIndex = 0;

    [DataMember]
    bool mList = false;

    int GetResultIndex()
    {
      return mResultIndex++;
    }

    [DataMember]
    SearchDatagraph mParent;
    SearchDatagraph Parent
    {
      get { return mParent; }
    }

    SearchDatagraph Root
    {
      get { return Parent == null ? this : Parent.Root; }
    }

    public SearchDatagraph EndJoin()
    {
      return Parent;
    }

    public SearchDatagraph Select(string propertyName, string alias = null, string format = null)
    {
      if (string.IsNullOrWhiteSpace(propertyName)) throw new ArgumentNullException("propertyName");
      PropertyInfo pi = ObjectType.GetProperty(propertyName);
      if (pi == null) throw new InvalidOperationException(string.Format("Property name '{0}' does not exist on object {1}", propertyName, ObjectType.Name));

      Results.Add(new sdgResult() { Index = Root.GetResultIndex(), ObjectType = ObjectType.FullName, PropertyName = propertyName, Alias = alias, Format = format });

      return this;
    }

    public SearchDatagraph Where(string propertyName, FilterType filterType, object filter, bool applyFilter = true)
    {
      if (!applyFilter) return this;
      if (string.IsNullOrWhiteSpace(propertyName)) throw new ArgumentNullException("propertyName");
      PropertyInfo pi = ObjectType.GetProperty(propertyName);
      if (pi == null) throw new InvalidOperationException(string.Format("Property name '{0}' does not exist on object {1}", propertyName, ObjectType.Name));

      if (filter is DateTime)
      {
        filter = ((DateTime)filter).Date;
        if (filterType == FilterType.Equals)
        {
          DateTime dt = ((DateTime)filter).Date.AddDays(1);
          WhereClauses.Add(new sdgWhere() { ObjectType = ObjectType.FullName, Alias = this.Alias, PropertyName = propertyName, FilterType = FilterType.GreaterThanEqual, Filter = filter });
          WhereClauses.Add(new sdgWhere() { ObjectType = ObjectType.FullName, Alias = this.Alias, PropertyName = propertyName, FilterType = FilterType.LessThan, Filter = dt });
        }
        else
        {
          WhereClauses.Add(new sdgWhere() { ObjectType = ObjectType.FullName, Alias = this.Alias, PropertyName = propertyName, FilterType = filterType, Filter = filter });
        }
      }
      else
      {
        WhereClauses.Add(new sdgWhere() { ObjectType = ObjectType.FullName, Alias = this.Alias, PropertyName = propertyName, FilterType = filterType, Filter = filter });
      }
      return this;
    }

    public SearchDatagraph WhereBetween(string propertyName, object startFilter, object endFilter, bool applyStartFilter = true, bool applyEndFilter = true)
    {
      if (!applyStartFilter && !applyEndFilter) return this;
      if (string.IsNullOrWhiteSpace(propertyName)) throw new ArgumentNullException("propertyName");
      PropertyInfo pi = ObjectType.GetProperty(propertyName);
      if (pi == null) throw new InvalidOperationException(string.Format("Property name '{0}' does not exist on object {1}", propertyName, ObjectType.Name));

      FilterType endFilterType = FilterType.LessThanEqual;

      //if (startFilter == null) applyStartFilter = false;
      //if (endFilter == null) applyEndFilter = false;

      if (startFilter is DateTime) startFilter = ((DateTime)startFilter).Date;
      if (endFilter is DateTime)
      {
        endFilter = ((DateTime)endFilter).Date.AddDays(1);
        endFilterType = FilterType.LessThan;
      }

      if (applyStartFilter && !applyEndFilter)
      {
        WhereClauses.Add(new sdgWhere() { ObjectType = ObjectType.FullName, Alias = this.Alias, PropertyName = propertyName, FilterType = FilterType.GreaterThanEqual, Filter = startFilter });
      }
      else if (!applyStartFilter && applyEndFilter)
      {
        WhereClauses.Add(new sdgWhere() { ObjectType = ObjectType.FullName, Alias = this.Alias, PropertyName = propertyName, FilterType = endFilterType, Filter = endFilter });
      }
      else
      {
        WhereClauses.Add(new sdgWhere() { ObjectType = ObjectType.FullName, Alias = this.Alias, PropertyName = propertyName, FilterType = FilterType.GreaterThanEqual, Filter = startFilter });
        WhereClauses.Add(new sdgWhere() { ObjectType = ObjectType.FullName, Alias = this.Alias, PropertyName = propertyName, FilterType = endFilterType, Filter = endFilter });
      }

      return this;
    }

    public SearchDatagraph Where(string propertyName, FilterType filterType, bool applyFilter = true)
    {
      if (!applyFilter) return this;
      if (string.IsNullOrWhiteSpace(propertyName)) throw new ArgumentNullException("propertyName");
      PropertyInfo pi = ObjectType.GetProperty(propertyName);
      if (pi == null) throw new InvalidOperationException(string.Format("Property name '{0}' does not exist on object {1}", propertyName, ObjectType.Name));

      WhereClauses.Add(new sdgWhere() { ObjectType = ObjectType.FullName, Alias = this.Alias, PropertyName = propertyName, FilterType = filterType });

      return this;
    }

    public SearchDatagraph Join(string propertyName)
    {
      if (string.IsNullOrWhiteSpace(propertyName)) throw new ArgumentNullException("propertyName");
      PropertyInfo pi = ObjectType.GetProperty(propertyName);
      if (pi == null) throw new InvalidOperationException(string.Format("Property name '{0}' does not exist on object {1}", propertyName, ObjectType.Name));

      SearchDatagraph dg = null;
      if (typeof(IList).IsAssignableFrom(pi.PropertyType))
      {
        PersistantCollectionAttribute pc = pi.GetCustomAttribute<PersistantCollectionAttribute>();
        if (pc != null)
        {
          if (pc.AssociativeType != null)
          {
            dg = new SearchDatagraph(pi.PropertyType.GetGenericArguments()[0], pc.AssociativeType, this);
          }
          else
          {
            dg = new SearchDatagraph(pi.PropertyType.GetGenericArguments()[0], this);
          }
        }
      }
      else
      {
        dg = new SearchDatagraph(pi.PropertyType, this, propertyName);
      }
      Joins.Add(dg);
      return dg;
    }

    public SearchDatagraph Join(Type ownedType)
    {
      if (ownedType == null) throw new ArgumentNullException("ownedType");

      SearchDatagraph dg = new SearchDatagraph(ownedType, this);
      Joins.Add(dg);
      return dg;
    }

    //bool ContainsResult(sdgResult result)
    //{
    //  sdgResult r = Results.FirstOrDefault(r1 => r1.Alias == result.Alias);
    //  return r != null;
    //}

    void AddBase(IInternalObjectRepository baseOr, SearchDatagraph gdo, Collection<string> columns, IInternalPersistanceManager pm)
    {
      SearchDatagraph dg = new SearchDatagraph(baseOr.ObjectType, this);
      SearchDatagraph sdg1 = GetBaseDatagram(baseOr.ObjectType);
      if (sdg1 == null) gdo.BaseJoins.Add(dg);

      foreach (sdgResult r in Results)
      {
        dg.Results.Add(r);
      }

      dg.RenderResultsColumns(columns, pm);

      if (baseOr.BaseRepository != null)
      {
        AddBase(baseOr.BaseRepository, gdo, columns, pm);
      }
    }

    void RenderResultsColumns(Collection<string> columns, IInternalPersistanceManager pm)
    {
      if (Results.Count > 0)
      {
        IInternalObjectRepository or = null;
        try
        {
          or = pm.GetRepository(ObjectType);
        }
        catch
        {
          throw;
        }

        if (or.BaseRepository != null)
        {
          AddBase(or.BaseRepository, this, columns, pm);
        }

        foreach (sdgResult r in Results)
        {
          PersistantProperty pp = or.GetProperty(r.PropertyName);
          if (pp == null) continue;
          if (pp.ObjectRepository.ObjectType == ObjectType)
          {
            try
            {
              string key = string.Format("{0}.{1}", r.ObjectType, pp.PropertyName);
              if (Root.PropertyAliasDictionary.ContainsKey(key)) continue;
              Root.PropertyAliasDictionary.Add(key, Alias ?? pp.PropertyName);
            }
            catch
            {
              throw;
            }
            if (columns.Count <= r.Index)
            {
              if (r.Alias != "GlobalIdentifier")
              {
                Root.Formats.Add(r.Format);
                if (r.Alias != null) Root.Headers.Add(r.Alias);
              }
              if (r.Alias != null) columns.Add(string.Format("{0}.{1} AS {2}", pm.GetIdentifier(Alias), pm.GetIdentifier(pp.ColumnName), pm.GetIdentifier(r.Alias)));
            }
            else
            {
              if (r.Alias != "GlobalIdentifier")
              {
                Root.Formats.Insert(r.Index - 1, r.Format);
                if (r.Alias != null) Root.Headers.Insert(r.Index - 1, r.Alias);
              }
              if (r.Alias != null) columns.Insert(r.Index, string.Format("{0}.{1} AS {2}", pm.GetIdentifier(Alias), pm.GetIdentifier(pp.ColumnName), pm.GetIdentifier(r.Alias)));
            }
          }
        }
      }

      foreach (SearchDatagraph sdg in Joins)
      {
        sdg.RenderResultsColumns(columns, pm);
      }
    }

    void RenderJoins(Collection<string> joins, IInternalPersistanceManager pm)
    {
      IInternalObjectRepository or = pm.GetRepository(ObjectType);
      foreach (SearchDatagraph sdg in BaseJoins)
      {
        IInternalObjectRepository orj = pm.GetRepository(sdg.ObjectType);
        joins.Add(string.Format("INNER JOIN {0} AS {1} ON {1}.{2}={3}.{4}", orj.GetFullTableName(), pm.GetIdentifier(sdg.Alias), pm.GetIdentifier(orj.IdColumn), pm.GetIdentifier(Alias), pm.GetIdentifier(or.IdColumn)));

        sdg.RenderJoins(joins, pm);
      }

      foreach (SearchDatagraph sdg in Joins)
      {
        IInternalObjectRepository orj = pm.GetRepository(sdg.ObjectType);

        if (sdg.mList)
        {
          if (sdg.AssociativeType != null)
          {
            string alias = GetAlias();
            IInternalObjectRepository ora = pm.GetRepository(sdg.AssociativeType);
            joins.Add(string.Format("LEFT OUTER JOIN {0} AS {1} ON {1}.{2}={3}.{4}", ora.GetFullTableName(), alias, pm.GetIdentifier(ora.OwnerColumn), pm.GetIdentifier(this.Alias), pm.GetIdentifier(or.IdColumn)));
            joins.Add(string.Format("LEFT OUTER JOIN {0} AS {1} ON {1}.{2}={3}.{4}", orj.GetFullTableName(), pm.GetIdentifier(sdg.Alias), pm.GetIdentifier(orj.IdColumn), alias, pm.GetIdentifier(ora.ReferenceColumn)));
          }
          else
          {
            joins.Add(string.Format("LEFT OUTER JOIN {0} AS {1} ON {1}.{2}={3}.{4}", orj.GetFullTableName(), pm.GetIdentifier(sdg.Alias), pm.GetIdentifier(orj.OwnerColumn), pm.GetIdentifier(this.Alias), pm.GetIdentifier(or.IdColumn)));
          }
        }
        else
        {
          if (sdg.JoinProperty != null)
          {
            PersistantProperty pp = or.GetProperty(sdg.JoinProperty);
            SearchDatagraph sdg1 = GetBaseDatagram(pp.ObjectRepository.ObjectType);
            joins.Add(string.Format("LEFT OUTER JOIN {0} AS {1} ON {1}.{2}={3}.{4}", orj.GetFullTableName(), pm.GetIdentifier(sdg.Alias), pm.GetIdentifier(orj.IdColumn), pm.GetIdentifier(sdg1.Alias), pm.GetIdentifier(pp.ColumnName)));
          }
          else
          {
            //PersistantProperty pp = or.GetProperty(BusinessObject.OwnerProperty);
            //SearchDatagraph sdg1 = GetBaseDatagram(sdg.ObjectType);
            joins.Add(string.Format("LEFT OUTER JOIN {0} AS {1} ON {1}.{2}={3}.{4}", orj.GetFullTableName(), pm.GetIdentifier(sdg.Alias), pm.GetIdentifier(orj.OwnerColumn), pm.GetIdentifier(this.Alias), pm.GetIdentifier(or.IdColumn)));
          }
        }

        sdg.RenderJoins(joins, pm);
      }
    }

    SearchDatagraph GetBaseDatagram(Type objectType)
    {
      if (ObjectType == objectType) return this;
      foreach (SearchDatagraph sdg in BaseJoins)
      {
        SearchDatagraph sdgr = sdg.GetBaseDatagram(objectType);
        if (sdgr != null) return sdgr;
      }
      return null;
    }

    void RenderWhere(Collection<string> where, IInternalPersistanceManager pm)
    {
      IInternalObjectRepository or = pm.GetRepository(ObjectType);
      foreach (sdgWhere w in WhereClauses)
      {
        string key = string.Format("{0}.{1}", w.ObjectType, w.PropertyName);
        if (w.FilterType == FilterType.IsNull)
        { 
          if (Root.PropertyAliasDictionary.ContainsKey(key)) where.Add(string.Format("{0}.{1} IS NULL", pm.GetIdentifier(Root.PropertyAliasDictionary[key]), pm.GetIdentifier(or.GetProperty(w.PropertyName).ColumnName)));
          else where.Add(string.Format("{0}.{1} IS NULL", pm.GetIdentifier(w.Alias), pm.GetIdentifier(or.GetProperty(w.PropertyName).ColumnName)));
        }
        else if (w.FilterType == FilterType.IsNotNull)
        {
          if (Root.PropertyAliasDictionary.ContainsKey(key)) where.Add(string.Format("{0}.{1} IS NOT NULL", pm.GetIdentifier(Root.PropertyAliasDictionary[key]), pm.GetIdentifier(or.GetProperty(w.PropertyName).ColumnName)));
          else where.Add(string.Format("{0}.{1} IS NOT NULL", pm.GetIdentifier(w.Alias), pm.GetIdentifier(or.GetProperty(w.PropertyName).ColumnName)));
        }
        else if (w.FilterType == FilterType.In)
        {
          Collection<string> sin = new Collection<string>();
          foreach(object o in (IList)w.Filter)
          {
            sin.Add(AddParameter(o, pm));
          }
          if (sin.Count > 0)
          {
            if (Root.PropertyAliasDictionary.ContainsKey(key)) where.Add(string.Format("{0}.{1} IN ({2})", pm.GetIdentifier(Root.PropertyAliasDictionary[key]), pm.GetIdentifier(or.GetProperty(w.PropertyName).ColumnName), string.Join(",", sin)));
            else if (sin.Count > 0) where.Add(string.Format("{0}.{1} IN ({2})", pm.GetIdentifier(w.Alias), pm.GetIdentifier(or.GetProperty(w.PropertyName).ColumnName), string.Join(",", sin)));
          }
        }
        else if ( w.FilterType == FilterType.NotIn)
        {
          Collection<string> sin = new Collection<string>();
          foreach (object o in (IList)w.Filter)
          {
            sin.Add(AddParameter(o, pm));
          }
          if (sin.Count > 0)
          {
            if (Root.PropertyAliasDictionary.ContainsKey(key)) where.Add(string.Format("{0}.{1} NOT IN ({2})", pm.GetIdentifier(Root.PropertyAliasDictionary[key]), pm.GetIdentifier(or.GetProperty(w.PropertyName).ColumnName), string.Join(",", sin)));
            else where.Add(string.Format("{0}.{1} NOT IN ({2})", pm.GetIdentifier(w.Alias), pm.GetIdentifier(or.GetProperty(w.PropertyName).ColumnName), string.Join(",", sin)));
          }
        }
        else
        {
          if (Root.PropertyAliasDictionary.ContainsKey(key)) where.Add(string.Format("{0}.{1}{2}{3}", pm.GetIdentifier(Root.PropertyAliasDictionary[key]), pm.GetIdentifier(or.GetProperty(w.PropertyName).ColumnName), pm.GetFilterText(w.FilterType), AddParameter(w.Filter, pm)));
          else where.Add(string.Format("{0}.{1}{2}{3}", pm.GetIdentifier(w.Alias), pm.GetIdentifier(or.GetProperty(w.PropertyName).ColumnName), pm.GetFilterText(w.FilterType), AddParameter(w.Filter, pm)));
        }
      }

      foreach (SearchDatagraph sdg in Joins)
      {
        sdg.RenderWhere(where, pm);
      }
    }

    Collection<string> mFormats;
    Collection<string> Formats
    {
      get { return mFormats ?? (mFormats = new Collection<string>()); }
    }

    Collection<string> mHeaders;
    Collection<string> Headers
    {
      get { return mHeaders ?? (mHeaders = new Collection<string>()); }
    }

    Dictionary<string, string> mPropertyAliasDictionary;
    Dictionary<string, string> PropertyAliasDictionary
    {
      get { return mPropertyAliasDictionary ?? (mPropertyAliasDictionary = new Dictionary<string, string>()); }
    }

    public SearchResults ExecuteQuery(IPersistanceManager pm)
    {
      return ExecuteQuery((IInternalPersistanceManager)pm);
    }

    internal SearchResults ExecuteQuery(IInternalPersistanceManager pm)
    {
      try
      {
        mPropertyAliasDictionary = null;
        Collection<string> columns = new Collection<string>();
        Collection<string> joins = new Collection<string>();
        Collection<string> where = new Collection<string>();

        mCommand = pm.GetCommand();

        RenderResultsColumns(columns, pm);
        RenderJoins(joins, pm);
        RenderWhere(where, pm);

        IInternalObjectRepository or = pm.GetRepository(ObjectType);
        string sql = null;
        if (where.Count == 0)
          sql = string.Format("SELECT DISTINCT TOP 500 {0}\nFROM {1} AS {2}\n{3}", string.Join("\n, ", columns), or.GetFullTableName(), pm.GetIdentifier(Alias), string.Join("\n", joins));
        else
          sql = string.Format("SELECT DISTINCT TOP 500 {0}\nFROM {1} AS {2}\n{3}\nWHERE {4}", string.Join("\n, ", columns), or.GetFullTableName(), pm.GetIdentifier(Alias), string.Join("\n", joins), string.Join("\nAND ", where));

        Command.CommandText = sql;
        //PrintCommand();

        SearchResults srs = new SearchResults(Headers, Formats);
        DataSet ds = Execute();
        foreach (DataRow dr in ds.Tables[0].Rows)
        {
          SearchResult sr = new SearchResult(dr);
          srs.AddResult(sr);
        }

        return srs;
      }
      catch
      {
        throw;
      }
    }


    DataSet Execute()
    {
      try
      {
        System.Data.DataSet ds = new System.Data.DataSet();
        ds.Locale = CultureInfo.InvariantCulture;
        using (IDataReader r = Command.ExecuteReader())
        {
          ds.Load(r, LoadOption.OverwriteChanges, string.Empty);
          r.Close();
        }
        return ds;
      }
      catch 
      {
        throw;
      }
    }


    IDbCommand mCommand;
    IDbCommand Command
    {
      get { return Root.mCommand; }
    }

    int mParameterCount = 0;
    int ParameterCount
    {
      get { return ++Root.mParameterCount; }
    }

    string AddParameter(object value, IPersistanceManager pm)
    {
      return pm.AddParameter(ParameterCount, value, Command);
    }

    void PrintCommand()
    {
      //LogHelper.LogSql(Command);
    }
  }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business
{
  [Serializable]
  public class ImportException : Exception
  {
    protected ImportException(SerializationInfo info, StreamingContext context)
      :base(info, context)
    {
    }

    public ImportException(string message)
      : base(message)
    {
    }

    public ImportException(string message, Exception innerException)
      : base(message, innerException)
    {
    }
  }
}

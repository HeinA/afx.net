﻿using Afx.Business.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business
{
  [DataContract(IsReference = true, Namespace = Afx.Business.Namespace.Afx)]
  [PersistantObject(Schema = "Afx")]
  public class ContextOperation : Operation
  {
    public ContextOperation()
    {
    }

    protected ContextOperation(ContextOperation op)
      :base(op)
    {
      ContextTypeName = op.ContextTypeName;
    }

    public override Operation CreateReplicate()
    {
      return new ContextOperation(this);
    }

    #region Type ContextType

    #region DataMember string ContextTypeName

    public const string ContextTypeNameProperty = "ContextTypeName";
    [DataMember(Name = ContextTypeNameProperty, EmitDefaultValue = false)]
    string mContextTypeName;
    [PersistantProperty]
    public string ContextTypeName
    {
      get { return mContextTypeName; }
      set { SetProperty<string>(ref mContextTypeName, value); }
    }

    #endregion

    Type mContextType;
    public Type ContextType
    {
      get
      {
        if (mContextType == null && !string.IsNullOrWhiteSpace(ContextTypeName)) mContextType = Type.GetType(ContextTypeName);
        return mContextType;
      }
      set
      {
        if (ContextType != value)
        {
          mContextType = value;
          if (value == null) ContextTypeName = null;
          else ContextTypeName = string.Format("{0}, {1}", value.FullName, value.Assembly.GetName().Name);
          OnPropertyChanged("ContextType");
        }
      }
    }

    #endregion
  }
}

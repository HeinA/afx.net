﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business
{
  public interface ITableCell
  {
    int RowId
    {
      get;
    }

    void SetRowId(int id);

    BusinessObject Row
    {
      get;
      set;
    }

    int ColumnId
    {
      get;
    }

    void SetColumnId(int id);

    BusinessObject Column
    {
      get;
      set;
    }
  }
}

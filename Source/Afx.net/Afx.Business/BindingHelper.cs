﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Afx.Business
{
  public class BindingHelper
  {
    #region INotifyPropertyChanged Source

    INotifyPropertyChanged mSource;
    public INotifyPropertyChanged Source
    {
      get { return mSource; }
      set { mSource = value; }
    }
    
    #endregion

    #region PropertyInfo SourceProperty

    PropertyInfo mSourceProperty;
    public PropertyInfo SourceProperty
    {
      get { return mSourceProperty; }
      set { mSourceProperty = value; }
    }

    #endregion


    #region INotifyPropertyChanged Target

    INotifyPropertyChanged mTarget;
    public INotifyPropertyChanged Target
    {
      get { return mTarget; }
      set { mTarget = value; }
    }

    #endregion

    #region PropertyInfo TargetProperty

    PropertyInfo mTargetProperty;
    public PropertyInfo TargetProperty
    {
      get { return mTargetProperty; }
      set { mTargetProperty = value; }
    }

    #endregion

    private BindingHelper(INotifyPropertyChanged source, string sourcePropertyName, INotifyPropertyChanged target, string targetPropertyName)
    {
      if (source == null) throw new ArgumentNullException("source");
      if (target == null) throw new ArgumentNullException("target");

      Source = source;
      Target = target;
      SourceProperty = Source.GetType().GetProperty(sourcePropertyName);
      TargetProperty = Target.GetType().GetProperty(targetPropertyName);

      if (SourceProperty == null) throw new ArgumentException("sourcePropertyName");
      if (TargetProperty == null) throw new ArgumentException("targetPropertyName");

      TargetProperty.SetValue(Target, SourceProperty.GetValue(Source));

      Source.PropertyChanged -= Source_PropertyChanged;
      Target.PropertyChanged -= Target_PropertyChanged;

      Source.PropertyChanged += Source_PropertyChanged;
      Target.PropertyChanged += Target_PropertyChanged;
    }

    bool mInternalUpdate = false;

    void Target_PropertyChanged(object sender, PropertyChangedEventArgs e)
    {
      if (mInternalUpdate) return;

      if (e.PropertyName == TargetProperty.Name)
      {
        try
        {
          mInternalUpdate = true;
          SourceProperty.SetValue(Source, TargetProperty.GetValue(Target));
        }
        finally
        {
          mInternalUpdate = false;
        }
      }
    }

    void Source_PropertyChanged(object sender, PropertyChangedEventArgs e)
    {
      if (mInternalUpdate) return;
      if (e.PropertyName == SourceProperty.Name)
      {
        try
        {
          mInternalUpdate = true;
          TargetProperty.SetValue(Target, SourceProperty.GetValue(Source));
        }
        finally
        {
          mInternalUpdate = false;
        }
      }
    }

    public static BindingHelper Bind(INotifyPropertyChanged source, string sourcePropertyName, INotifyPropertyChanged target, string targetPropertyName)
    {
      return new BindingHelper(source, sourcePropertyName, target, targetPropertyName);
    }
  }
}

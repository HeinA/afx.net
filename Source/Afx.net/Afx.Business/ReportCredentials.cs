﻿using Afx.Business;
using Afx.Business.Attributes;
using Afx.Business.Collections;
using Afx.Business.Data;
using Afx.Business.Validation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace Afx.Business
{
  [PersistantObject(Schema = "Afx")]
  public partial class ReportCredentials : BusinessObject 
  {
    #region Constructors

    public ReportCredentials()
    {
    }

    #endregion

    #region string Server

    public const string ServerProperty = "Server";
    [DataMember(Name = ServerProperty, EmitDefaultValue = false)]
    string mServer;
    [PersistantProperty]
    public string Server
    {
      get { return mServer; }
      set { SetProperty<string>(ref mServer, value); }
    }

    #endregion

    #region string Database

    public const string DatabaseProperty = "Database";
    [DataMember(Name = DatabaseProperty, EmitDefaultValue = false)]
    string mDatabase;
    [PersistantProperty]
    public string Database
    {
      get { return mDatabase; }
      set { SetProperty<string>(ref mDatabase, value); }
    }

    #endregion

    #region string UserName

    public const string UserNameProperty = "UserName";
    [DataMember(Name = UserNameProperty, EmitDefaultValue = false)]
    string mUserName;
    [PersistantProperty]
    public string UserName
    {
      get { return mUserName; }
      set { SetProperty<string>(ref mUserName, value); }
    }

    #endregion

    #region string Password

    public const string PasswordProperty = "Password";
    [DataMember(Name = PasswordProperty, EmitDefaultValue = false)]
    string mPassword;
    [PersistantProperty]
    public string Password
    {
      get { return mPassword; }
      set { SetProperty<string>(ref mPassword, value); }
    }

    #endregion
  }
}

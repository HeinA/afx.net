﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afx.Business
{
  [Export(typeof(INamespace))]
  public class Namespace : INamespace
  {
    public const string Afx = "http://openafx.net/Business";

    public string GetUri()
    {
      return Afx;
    }
  }
}

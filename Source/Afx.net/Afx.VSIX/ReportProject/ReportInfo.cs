﻿using Afx.Business.Security;
using Afx.Prism.RibbonMdi;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace $safeprojectname$
{
  [Export(typeof(ICrystalReportInfo))]
  [Authorization(Afx.Business.Security.Roles.Administrator)]
  public class ReportInfo : CrystalReportInfo<GetParametersDialog.GetParametersDialogController>
  {
    public override string GroupName { get { return ""; } }
    public override string ReportName { get { return ""; } }


    public override byte[] GetReport()
    {
      return ResourceHelper.GetResource(System.Reflection.Assembly.GetExecutingAssembly(), "$safeprojectname$.$safeprojectname$.rpt");
    }
  }
}

﻿using CrystalDecisions.CrystalReports.Engine;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace $safeprojectname$.GetParametersDialog
{
  public class GetParametersDialogController : MdiReportParameterController<GetParametersDialogContext, GetParametersDialogViewModel>
  {
    public const string GetParametersDialogControllerKey = "$safeprojectname$.GetParametersDialog.GetParametersDialogController";

    public GetParametersDialogController(IController controller)
      : base(GetParametersDialogControllerKey, controller)
    {
    }

    protected override void ApplyChanges()
    {
      //ReportDocument.SetParameterValue("@StartDate", DataContext.StartDate);
      //ReportDocument.SetParameterValue("@EndDate", DataContext.EndDate);
    }
  }
}

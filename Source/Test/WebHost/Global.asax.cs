﻿using Afx.Business;
using Afx.Business.Data;
using Afx.Business.ServiceModel;
using Afx.Data.Sql;
using FleetManagement.Business;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;

namespace WebHost
{
  public class Global : System.Web.HttpApplication
  {

    protected void Application_Start(object sender, EventArgs e)
    {
      //using (StreamWriter writer = new StreamWriter(@"c:\logs\debug.txt", false))
      //{
      //  writer.Write("Application_Start...");
      //}

      //PersistanceManager pm = new SqlPersistanceManager();
      //pm.RegisterAssembly(typeof(BusinessObject).Assembly);
      //pm.RegisterAssembly(typeof(TripSheet).Assembly);
      //pm.PreLoad();

      //UnityProvider.Instance = new UnityContainer();
      //UnityProvider.Instance.RegisterInstance<IPersistanceManager>(pm);

      //IConfigurationSource config = ConfigurationSourceFactory.Create();
      //DatabaseFactory.SetDatabaseProviderFactory(new DatabaseProviderFactory(config));
      //Logger.SetLogWriter(new LogWriterFactory(config).Create());
      //ExceptionPolicyFactory factory = new ExceptionPolicyFactory(config);
      //ExceptionManager exManager = factory.CreateManager();
      //ExceptionPolicy.SetExceptionManager(exManager);

      //Cache.Instance.LoadCache(Cache.FetchCache(pm));

      //using (StreamWriter writer = new StreamWriter(@"c:\logs\debug.txt", true))
      //{
      //  writer.WriteLine("done!");
      //}
    }

    protected void Session_Start(object sender, EventArgs e)
    {

    }

    protected void Application_BeginRequest(object sender, EventArgs e)
    {

    }

    protected void Application_AuthenticateRequest(object sender, EventArgs e)
    {

    }

    protected void Application_Error(object sender, EventArgs e)
    {
#if DEBUG
      Debugger.Break();
#endif
    }

    protected void Session_End(object sender, EventArgs e)
    {

    }

    protected void Application_End(object sender, EventArgs e)
    {
    }
  }
}
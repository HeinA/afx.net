﻿using Afx.Business;
using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Business.Service;
using Afx.Business.ServiceModel;
using Afx.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace WebHost.App_Code
{
  public class AppInit
  {
    public static void AppInitialize()
    {
      IConfigurationSource config = ConfigurationSourceFactory.Create();
      DatabaseFactory.SetDatabaseProviderFactory(new DatabaseProviderFactory(config));
      Logger.SetLogWriter(new LogWriterFactory(config).Create());
      ExceptionPolicyFactory factory = new ExceptionPolicyFactory(config);
      ExceptionManager exManager = factory.CreateManager();
      ExceptionPolicy.SetExceptionManager(exManager);

      AfxService.Initialize();
    }
  }
}
﻿using Afx.Business;
using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Business.Service;
using Afx.Business.ServiceModel;
using Afx.Data.Sql;
using FleetManagement.Business;
using FleetManagement.Business.Service;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TestHost
{
  [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
  public class Program
  {
    static Program mInstance;
    bool mRun = true;
    const Int32 SW_MINIMIZE = 6;

    [DllImport("Kernel32.dll", CallingConvention = CallingConvention.StdCall, SetLastError = true)]
    private static extern IntPtr GetConsoleWindow();

    [DllImport("User32.dll", CallingConvention = CallingConvention.StdCall, SetLastError = true)]
    [return: MarshalAs(UnmanagedType.Bool)]
    private static extern bool ShowWindow([In] IntPtr hWnd, [In] Int32 nCmdShow);

    private static void MinimizeConsoleWindow()
    {
      IntPtr hWndConsole = GetConsoleWindow();
      ShowWindow(hWndConsole, SW_MINIMIZE);
    }

    Program()
    {
      PersistanceManager pm = new SqlPersistanceManager();
      pm.RegisterAssembly(typeof(BusinessObject).Assembly);
      pm.RegisterAssembly(typeof(Tripsheet).Assembly);
      pm.PreLoad();

      AfxService.Initialize(pm);

      using (ServiceHost afxHost = new ServiceHost(typeof(AfxService)))
      using (ServiceHost fleetService = new ServiceHost(typeof(FleetManagementService)))
      {
        afxHost.Open();
        fleetService.Open();

        Console.WriteLine("The service is ready.");
        MinimizeConsoleWindow();
        while (mRun)
        {
          Thread.Sleep(100);
        }

        afxHost.Close();
        fleetService.Close();
      }
    }

    static void Main(string[] args)
    {
      mInstance = new Program();
    }

    public void Terminate()
    {
      mRun = false;
    }
  }
}

﻿using Afx.Business.Data;
using Afx.Business.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestHost
{
  class TestEventManager : PersistanceEventManager<OrganizationalStructureType>
  {
    public override bool BeforePersist(OrganizationalStructureType obj)
    {
      if (obj.Name == "zzz") return false;
      return base.BeforePersist(obj);
    }
  }
}

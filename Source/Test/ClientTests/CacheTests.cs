﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Afx.Business.Service;
using Afx.Business.Security;
using Afx.Business.Data;
using Afx.Business.Documents;
using Afx.Business.Activities;

namespace ClientTests
{
  [TestClass]
  public class CacheTests
  {
    [TestMethod]
    public void LoadCache()
    {
      using (var svc = ProxyFactory.GetService<IAfxService>("Elisenheim"))
      {
        Cache.Instance.LoadCache(svc.LoadCache());
      }
    }

    [TestMethod]
    public void SpotCheckCachedObjects()
    {
      DocumentTypeState ds = Cache.Instance.GetObject<DocumentTypeState>(new Guid("796316bb-a000-4f3f-8cf4-19b3f0a8fedd"));
      if (ds.Name != "Pod Received")
      {
        Assert.Fail("DocumentTypeState - Pod Received (796316bb-a000-4f3f-8cf4-19b3f0a8fedd) loaded incorrectly.");
      }

      DocumentType dt = Cache.Instance.GetObject<DocumentType>(new Guid("d174776f-18fb-4ee4-bef8-8753703634cd"));
      if (dt.Name != "Tripsheet")
      {
        Assert.Fail("DocumentType - Tripsheet (d174776f-18fb-4ee4-bef8-8753703634cd) loaded incorrectly.");
      }

      if (dt.InitialState.GlobalIdentifier != Guid.Parse("aed03677-ab54-4aa5-a024-774a6516c56e") || dt.InitialState.Name != "Created")
      {
        Assert.Fail("DocumentType - Tripsheet (d174776f-18fb-4ee4-bef8-8753703634cd) loaded incorrectly.  Incorrect Initial State");
      }

      Role r = Cache.Instance.GetObject<Role>(new Guid("e4e8636f-c7c4-444a-94e3-090ab8e71ecc"));
      if (r.Name != "Domain Administrator")
      {
        Assert.Fail("Role - Domain Administrator (e4e8636f-c7c4-444a-94e3-090ab8e71ecc) loaded incorrectly.");
      }

      Activity a = Cache.Instance.GetObject<Activity>(new Guid("8d92749e-d315-4ff3-8c02-720dbf1bb826"));
      if (a.Name != "Activity Management")
      {
        Assert.Fail("Activity - Activity Management (8d92749e-d315-4ff3-8c02-720dbf1bb826) loaded incorrectly.");
      }

      ActivityRole ar = a.GetAssociativeObject<ActivityRole>(r);
      if (ar == null)
      {
        Assert.Fail("Activity - Activity Management (8d92749e-d315-4ff3-8c02-720dbf1bb826) loaded incorrectly.  Domain Admin Role not found");
      }
    }
  }
}

﻿<?xml version="1.0" encoding="utf-8"?>
<configuration>
  
  <configSections>
    <section name="loggingConfiguration" type="Microsoft.Practices.EnterpriseLibrary.Logging.Configuration.LoggingSettings, Microsoft.Practices.EnterpriseLibrary.Logging" requirePermission="true" />
    <section name="exceptionHandling" type="Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.Configuration.ExceptionHandlingSettings, Microsoft.Practices.EnterpriseLibrary.ExceptionHandling" requirePermission="true" />
    <section name="applicationConfiguration" type="Afx.Prism.Configuration.ApplicationConfigurationSection, Afx.Prism" />
  </configSections>

  <applicationConfiguration title="Afx Ribbon Mdi Test Application" id="100" />

  <appSettings>
    <add key="LogSource" value="Client" />
    <add key="Servers" value="Master,Elisenheim,NPI" />
  </appSettings>

  <startup>
    <supportedRuntime version="v4.0" sku=".NETFramework,Version=v4.5.1" />
  </startup>

  <loggingConfiguration name="Logging Application Block" tracingEnabled="true" defaultCategory="General" logWarningsWhenNoCategoriesMatch="true">
    <listeners>
      <add name="Formatted EventLog TraceListener" type="Microsoft.Practices.EnterpriseLibrary.Logging.TraceListeners.FormattedEventLogTraceListener, Microsoft.Practices.EnterpriseLibrary.Logging" listenerDataType="Microsoft.Practices.EnterpriseLibrary.Logging.Configuration.FormattedEventLogTraceListenerData, Microsoft.Practices.EnterpriseLibrary.Logging" source="Afx" formatter="Text Formatter" log="Application" machineName="" traceOutputOptions="None" />
    </listeners>
    <formatters>
      <add type="Microsoft.Practices.EnterpriseLibrary.Logging.Formatters.TextFormatter, Microsoft.Practices.EnterpriseLibrary.Logging" template="Timestamp: {timestamp} Message: {message} Category: {category} Priority: {priority} EventId: {eventid} Severity: {severity} Title:{title} Machine: {machine} Application Domain: {appDomain} Process Id: {processId} Process Name: {processName} Win32 Thread Id: {win32ThreadId} Thread Name: {threadName} Extended Properties: {dictionary({key} - {value} )}" name="Text Formatter" />
      <add type="Microsoft.Practices.EnterpriseLibrary.Logging.Formatters.TextFormatter, Microsoft.Practices.EnterpriseLibrary.Logging" template="&lt;Event Timestamp=&quot;{timestamp}&quot;&gt;{message}&lt;/Event&gt;" name="XML Text Fomatter" />
    </formatters>
    <categorySources>
      <add switchValue="All" name="General">
        <listeners>
          <add name="Formatted EventLog TraceListener" />
        </listeners>
      </add>
    </categorySources>
    <specialSources>
      <allEvents switchValue="All" name="All Events" />
      <notProcessed switchValue="All" name="Unprocessed Category" />
      <errors switchValue="All" name="Logging Errors &amp; Warnings">
        <listeners>
          <add name="Formatted EventLog TraceListener" />
        </listeners>
      </errors>
    </specialSources>
  </loggingConfiguration>

  <exceptionHandling>
    <exceptionPolicies>
      <add name="Afx">
        <exceptionTypes>

          <add name="MessageFault" type="System.ServiceModel.FaultException`1[[Afx.Business.Service.MessageFault, Afx.Business]], System.ServiceModel, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" postHandlingAction="None">
            <exceptionHandlers>
              <add type="Afx.Prism.RibbonMdi.ExceptionHandlers.MessageExceptionHandler, Afx.Prism.RibbonMdi" name="DefaultHandler1" />
            </exceptionHandlers>
          </add>

          <add name="MessageException" type="Afx.Business.MessageException, Afx.Business" postHandlingAction="None">
            <exceptionHandlers>
              <add type="Afx.Prism.RibbonMdi.ExceptionHandlers.MessageExceptionHandler, Afx.Prism.RibbonMdi" name="DefaultHandler4" />
            </exceptionHandlers>
          </add>

          <add name="Exception" type="System.Exception, mscorlib" postHandlingAction="None">
            <exceptionHandlers>
              <add name="Logging Handler" type="Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.Logging.LoggingExceptionHandler, Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.Logging" logCategory="General" eventId="100" severity="Error" title="Enterprise Library Exception Handling" formatterType="Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.TextExceptionFormatter, Microsoft.Practices.EnterpriseLibrary.ExceptionHandling" priority="0" />
              <add type="Afx.Prism.RibbonMdi.ExceptionHandlers.ExceptionHandler, Afx.Prism.RibbonMdi" name="DefaultHandler5" />
            </exceptionHandlers>
          </add>

        </exceptionTypes>
      </add>
    </exceptionPolicies>
  </exceptionHandling>

  <system.diagnostics>
    <sources>
      <source name="System.ServiceModel" switchValue="Information, ActivityTracing" propagateActivity="true">
        <listeners>
          <add name="xml" />
        </listeners>
      </source>
      <source name="System.ServiceModel.MessageLogging">
        <listeners>
          <add name="xml" />
        </listeners>
      </source>
    </sources>
    <sharedListeners>
      <add name="xml" type="System.Diagnostics.XmlWriterTraceListener" initializeData="c:\Logs\AfxClient.svclog" />
    </sharedListeners>
  </system.diagnostics>

  <system.serviceModel>
    <diagnostics>
      <messageLogging logMessagesAtTransportLevel="true" logMessagesAtServiceLevel="false" logMalformedMessages="true" logEntireMessage="true" maxSizeOfMessageToLog="65535000" maxMessagesToLog="500" />
    </diagnostics>

    <bindings>
      <basicHttpBinding>
        <binding maxReceivedMessageSize="1048576">
          <security mode="None" />
        </binding>
      </basicHttpBinding>

      <netTcpBinding>
        <binding maxReceivedMessageSize="1048576">
          <security mode="None" />
        </binding>
      </netTcpBinding>

      <netMsmqBinding>
        <binding maxReceivedMessageSize="204800" exactlyOnce="true" receiveErrorHandling="Move" retryCycleDelay="00:10:00">
          <security mode="None">
            <!--<transport msmqAuthenticationMode="WindowsDomain"/>-->
          </security>
        </binding>
      </netMsmqBinding>

    </bindings>

    <extensions>
      <behaviorExtensions>
        <add name="afxEndpointBehaviorExtension" type="Afx.Business.ServiceModel.AfxCompressedEndpointBehaviorElement, Afx.Business" />
      </behaviorExtensions>
    </extensions>

    <behaviors>
      <endpointBehaviors>
        <behavior>
          <afxEndpointBehaviorExtension />
        </behavior>
      </endpointBehaviors>
    </behaviors>

    <client>
      <endpoint name="Master::Afx.Business.Service.IAfxService" contract="Afx.Business.Service.IAfxService" address="net.tcp://localhost:8010/WebHost.Master/AfxService.svc" binding="netTcpBinding" />

      <endpoint name="Master::FleetManagement.Business.Service.IFleetManagementService" contract="FleetManagement.Business.Service.IFleetManagementService" address="net.tcp://localhost:8010/WebHost.Master/FleetManagementService.svc" binding="netTcpBinding" />

      <endpoint name="Master::InventoryControl.Business.Service.IInventoryControlService" contract="InventoryControl.Business.Service.IInventoryControlService" address="net.tcp://localhost:8010/WebHost.Master/InventoryControlService.svc" binding="netTcpBinding" />

      <endpoint name="Elisenheim::Afx.Business.Service.IAfxService" contract="Afx.Business.Service.IAfxService" address="net.tcp://localhost:8010/WebHost/AfxService.svc" binding="netTcpBinding" />

      <endpoint name="Elisenheim::FleetManagement.Business.Service.IFleetManagementService" contract="FleetManagement.Business.Service.IFleetManagementService" address="net.tcp://localhost:8010/WebHost/FleetManagementService.svc" binding="netTcpBinding" />

      <endpoint name="Elisenheim::InventoryControl.Business.Service.IInventoryControlService" contract="InventoryControl.Business.Service.IInventoryControlService" address="net.tcp://localhost:8010/WebHost/InventoryControlService.svc" binding="netTcpBinding" />

      <endpoint name="NPI::Afx.Business.Service.IAfxService" contract="Afx.Business.Service.IAfxService" address="net.tcp://localhost:8010/WebHost.Npi/AfxService.svc" binding="netTcpBinding" />

      <endpoint name="NPI::FleetManagement.Business.Service.IFleetManagementService" contract="FleetManagement.Business.Service.IFleetManagementService" address="net.tcp://localhost:8010/WebHost.Npi/FleetManagementService.svc" binding="netTcpBinding" />

      <endpoint name="NPI::InventoryControl.Business.Service.IInventoryControlService" contract="InventoryControl.Business.Service.IInventoryControlService" address="net.tcp://localhost:8010/WebHost.Npi/InventoryControlService.svc" binding="netTcpBinding" />


      <!--<endpoint name="Master::Afx.Business.Service.IAfxService"
                contract="Afx.Business.Service.IAfxService"
                address="http://localhost/WebHost.Master/AfxService.svc"
                binding="basicHttpBinding"/>

      <endpoint name="Master::FleetManagement.Business.Service.IFleetManagementService"
                contract="FleetManagement.Business.Service.IFleetManagementService"
                address="http://localhost/WebHost.Master/FleetManagementService.svc"
                binding="basicHttpBinding"/>

      <endpoint name="Master::InventoryControl.Business.Service.IInventoryControlService"
                contract="InventoryControl.Business.Service.IInventoryControlService"
                address="http://localhost/WebHost.Master/InventoryControlService.svc"
                binding="basicHttpBinding"/>

      <endpoint name="Elisenheim::Afx.Business.Service.IAfxService"
                contract="Afx.Business.Service.IAfxService"
                address="http://localhost/WebHost/AfxService.svc"
                binding="basicHttpBinding"/>

      <endpoint name="Elisenheim::FleetManagement.Business.Service.IFleetManagementService"
                contract="FleetManagement.Business.Service.IFleetManagementService"
                address="http://localhost/WebHost/FleetManagementService.svc"
                binding="basicHttpBinding"/>

      <endpoint name="Elisenheim::InventoryControl.Business.Service.IInventoryControlService"
                contract="InventoryControl.Business.Service.IInventoryControlService"
                address="http://localhost/WebHost/InventoryControlService.svc"
                binding="basicHttpBinding"/>

      <endpoint name="NPI::Afx.Business.Service.IAfxService"
                contract="Afx.Business.Service.IAfxService"
                address="http://localhost/WebHost.Npi/AfxService.svc"
                binding="basicHttpBinding"/>

      <endpoint name="NPI::FleetManagement.Business.Service.IFleetManagementService"
                contract="FleetManagement.Business.Service.IFleetManagementService"
                address="http://localhost/WebHost.Npi/FleetManagementService.svc"
                binding="basicHttpBinding"/>

      <endpoint name="NPI::InventoryControl.Business.Service.IInventoryControlService"
                contract="InventoryControl.Business.Service.IInventoryControlService"
                address="http://localhost/WebHost.Npi/InventoryControlService.svc"
                binding="basicHttpBinding"/>-->


      <endpoint name="Afx.Business.Service.IAfxLogService" contract="Afx.Business.Service.IAfxLogService" address="net.msmq://localhost/private/AfxLogService.svc" binding="netMsmqBinding" />
    </client>
  </system.serviceModel>

  <runtime>
    <assemblyBinding xmlns="urn:schemas-microsoft-com:asm.v1">
      <dependentAssembly>
        <assemblyIdentity name="System.Windows.Interactivity" publicKeyToken="31bf3856ad364e35" culture="neutral" />
        <bindingRedirect oldVersion="0.0.0.0-4.5.0.0" newVersion="4.5.0.0" />
      </dependentAssembly>
      <!--<dependentAssembly>
        <assemblyIdentity name="Xceed.Wpf.AvalonDock"
                          publicKeyToken="3e4669d2f30244f4"
                          culture="neutral" />
        <bindingRedirect oldVersion="0.0.0.0-2.2.0.0"
                         newVersion="2.2.0.0" />
      </dependentAssembly>-->
      <dependentAssembly>
        <assemblyIdentity name="Microsoft.Practices.ServiceLocation" publicKeyToken="31bf3856ad364e35" culture="neutral" />
        <bindingRedirect oldVersion="0.0.0.0-1.3.0.0" newVersion="1.2.0.0" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="Microsoft.Practices.Unity" publicKeyToken="31bf3856ad364e35" culture="neutral" />
        <bindingRedirect oldVersion="0.0.0.0-3.5.1.0" newVersion="3.5.0.0" />
      </dependentAssembly>
      <!--<dependentAssembly>
        <assemblyIdentity name="Microsoft.Practices.Unity.Configuration" publicKeyToken="31bf3856ad364e35" culture="neutral" />
        <bindingRedirect oldVersion="0.0.0.0-3.5.1.0" newVersion="3.5.0.0" />
      </dependentAssembly>-->
      <dependentAssembly>
        <assemblyIdentity name="Xceed.Wpf.AvalonDock" publicKeyToken="3e4669d2f30244f4" culture="neutral" />
        <bindingRedirect oldVersion="0.0.0.0-2.0.0.0" newVersion="2.0.0.0" />
      </dependentAssembly>
    </assemblyBinding>
  </runtime>

</configuration>

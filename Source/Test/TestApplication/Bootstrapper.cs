﻿using Afx.Business.Security;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using Microsoft.Practices.Prism.Modularity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Resources;

namespace TestApplication
{
  class Bootstrapper : MdiBootstrapper
  {
    public override Stream GetDefaultDockingLayout()
    {
      try
      {
        StreamResourceInfo ri = Application.GetResourceStream(new Uri("DefaultLayout.config", UriKind.Relative));
        return ri.Stream;
      }
      catch
      {
        throw;
      }
    }

#if DEBUG
    protected override Afx.Business.Security.AuthenticationResult DefaultLoginDetails()
    {
      return SecurityContext.Authenticate("Elisenheim", "HeinA", "dred1234");
    }
#endif
  }
}

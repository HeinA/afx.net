﻿using Afx.Business;
using Afx.Business.Data;
using Afx.Business.Service;
using Afx.Business.ServiceModel;
using Afx.Data;
using Afx.Data.Sql;
using Afx.Prism;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace TestApplication
{
  /// <summary>
  /// Interaction logic for App.xaml
  /// </summary>
  public partial class App : Application
  {
    protected override void OnStartup(StartupEventArgs e)
    {
      Debug.Listeners.Clear();
      var listener = new FilteringListener();
      listener.IgnoredAssemblies.Add("Xceed.Wpf.AvalonDock");
      listener.IgnoredAssemblies.Add("Fluent");
      Debug.Listeners.Add(listener);

      Application.Current.DispatcherUnhandledException += Current_DispatcherUnhandledException;

      try
      {
        FileInfo fi = new FileInfo(@"c:\logs\AfxClient.svclog");
        if (fi.Exists) fi.Delete();
      }
      catch { }

      //ServiceFactory.SpinUp<IAfxService>();

      IConfigurationSource config = ConfigurationSourceFactory.Create();
      Logger.SetLogWriter(new LogWriterFactory(config).Create());
      ExceptionPolicyFactory factory = new ExceptionPolicyFactory(config);
      ExceptionManager exManager = factory.CreateManager();
      ExceptionPolicy.SetExceptionManager(exManager);

      base.OnStartup(e);
      Bootstrapper bootstrapper = new Bootstrapper();
      bootstrapper.Run(true);
    }

    void Current_DispatcherUnhandledException(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
    {
      try
      {
        ExceptionHelper.HandleException(e.Exception);
        //e.Handled = true;
      }
      catch
      {
#if DEBUG
        if (Debugger.IsAttached) Debugger.Break();
#endif
      }
    }
  }
}

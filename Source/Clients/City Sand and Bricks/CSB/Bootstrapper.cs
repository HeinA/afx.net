﻿using Afx.Business.Security;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using Microsoft.Practices.Prism.Modularity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Resources;

namespace CSB
{
  class Bootstrapper : MdiBootstrapper
  {
    public override Stream GetDefaultDockingLayout()
    {
      try
      {
        StreamResourceInfo ri = Application.GetResourceStream(new Uri("DefaultLayout.config", UriKind.Relative));
        return ri.Stream;
      }
      catch
      {
        throw;
      }
    }

    protected override MdiApplication CreateApplicationContext()
    {
      MdiApplication mdi = base.CreateApplicationContext();
      mdi.Title = "City Sand & Bricks";
      return mdi;
    }

  }
}

﻿using Afx.Business.Security;
using Afx.Business.Service;
using Afx.Prism;
using Afx.Prism.Touch;
using Microsoft.Practices.Prism.Modularity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Resources;

namespace CSB
{
  class Bootstrapper : TouchBootstrapper
  {
    protected override TouchApplication CreateApplicationContext()
    {
      TouchApplication mdi = base.CreateApplicationContext();
      mdi.Title = "City Sand & Bricks Weighbridge";
      return mdi;
    }

#if DEBUG
    protected override Afx.Business.Security.AuthenticationResult DefaultLoginDetails()
    {
      LoginOrganizationalUnit loginOU = null;
      using (var svc = ProxyFactory.GetService<IAfxService>(DefaultServer.ServerName))
      {
        //Depot
        loginOU = svc.LoadLoginOrganizationalUnits().Where(ou => ou.GlobalIdentifier.Equals(Guid.Parse("35263117-bc4a-476c-bdc8-3dd3ef41eaa5"))).FirstOrDefault();
      }
      return SecurityContext.Authenticate(loginOU, "HeinA", "dred1234");
    }
#endif

  }
}

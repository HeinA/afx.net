﻿using Afx.Business;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;

namespace CSB.WeighbridgeCredentials
{
  [Export(typeof(ICredentialProvider))]
  public class WeighbridgeCredentials : ICredentialProvider
  {
    public string Username
    {
      get { return "WeighbridgeOperator_Depot"; }
    }

    public string Password
    {
      get { return "WB123"; }
    }

    public Guid OrganizationalUnit
    {
      get { return Guid.Parse("35263117-bc4a-476c-bdc8-3dd3ef41eaa5"); }
    }
  }
}

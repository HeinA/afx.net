<?xml version="1.0"?>
<configuration>
  <configSections>
    <section name="loggingConfiguration"
             type="Microsoft.Practices.EnterpriseLibrary.Logging.Configuration.LoggingSettings, Microsoft.Practices.EnterpriseLibrary.Logging"
             requirePermission="true"/>
    <section name="exceptionHandling"
             type="Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.Configuration.ExceptionHandlingSettings, Microsoft.Practices.EnterpriseLibrary.ExceptionHandling"
             requirePermission="true"/>
    <section name="afxService"
             type="Afx.Business.Service.Configuration.AfxServiceSection, Afx.Business"/>
    <section name="extensibilityConfiguration"
             type="Afx.Business.Configuration.ExtensibilityConfiguration, Afx.Business"/>
  </configSections>
  
  <afxService serverInstance="fdea3092-76ff-4f47-8ef6-a363d84b580b"/>

  <extensibilityConfiguration>
    <allowedTypes>
      <add typeName="Weighbridge.Business.WeighbridgeTicketInventoryItem, Weighbridge.Business" />
    </allowedTypes>
  </extensibilityConfiguration>

  <appSettings>
    <add key="LogSource"
         value="CSB.Crusher"/>
  </appSettings>
  
  <loggingConfiguration name="Logging Application Block"
                        tracingEnabled="true"
                        defaultCategory="Afx Service Host"
                        logWarningsWhenNoCategoriesMatch="true">
    <listeners>
      <add name="Database Listener"
           type="Microsoft.Practices.EnterpriseLibrary.Logging.Database.FormattedDatabaseTraceListener, Microsoft.Practices.EnterpriseLibrary.Logging.Database"
           listenerDataType="Microsoft.Practices.EnterpriseLibrary.Logging.Database.Configuration.FormattedDatabaseTraceListenerData, Microsoft.Practices.EnterpriseLibrary.Logging.Database"
           databaseInstanceName="Local"
           writeLogStoredProcName="WriteLog"
           addCategoryStoredProcName="AddCategory"
           formatter="Text Formatter"
           traceOutputOptions="LogicalOperationStack, DateTime, Timestamp, ProcessId, ThreadId, Callstack"/>
    </listeners>
    <formatters>
      <add type="Microsoft.Practices.EnterpriseLibrary.Logging.Formatters.TextFormatter, Microsoft.Practices.EnterpriseLibrary.Logging"
           template="Timestamp: {timestamp}{newline} Message: {message}{newline} Category: {category}{newline} Priority: {priority}{newline} EventId: {eventid}{newline} Severity: {severity}{newline} Title:{title}{newline} Machine: {localMachine}{newline} App Domain: {localAppDomain}{newline} ProcessId: {localProcessId}{newline} Process Name: {localProcessName}{newline} Thread Name: {threadName}{newline} Win32 ThreadId:{win32ThreadId}{newline} Extended Properties: {dictionary({key} - {value}{newline})}"
           name="Text Formatter"/>
    </formatters>
    <categorySources>
      <add switchValue="All"
           name="Afx Service Host">
        <listeners>
          <add name="Database Listener"/>
        </listeners>
      </add>
    </categorySources>
    <specialSources>
      <allEvents switchValue="All"
                 name="All Events">
        <listeners>
          <add name="Database Listener"/>
        </listeners>
      </allEvents>
      <notProcessed switchValue="All"
                    name="Unprocessed Category">
        <listeners>
          <add name="Database Listener"/>
        </listeners>
      </notProcessed>
      <errors switchValue="All"
              name="Logging Errors &amp; Warnings">
        <listeners>
          <add name="Database Listener"/>
        </listeners>
      </errors>
    </specialSources>
  </loggingConfiguration>
  
  <exceptionHandling>
    <exceptionPolicies>
      <add name="WCF Exception Shielding">
        <exceptionTypes>
          <add name="Exception"
               type="System.Exception, mscorlib"
               postHandlingAction="None">
            <exceptionHandlers>
              <add name="LoggingExceptionHandler"
                   type="Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.Logging.LoggingExceptionHandler, Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.Logging"
                   logCategory="Afx Service Host"
                   eventId="100"
                   severity="Error"
                   title="Unhandled Exception"
                   formatterType="Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.TextExceptionFormatter, Microsoft.Practices.EnterpriseLibrary.ExceptionHandling"
                   priority="0"/>
            </exceptionHandlers>
          </add>
          <add name="AuthenticationException"
               type="Afx.Business.ServiceModel.AuthenticationException, Afx.Business"
               postHandlingAction="ThrowNewException">
            <exceptionHandlers>
              <add type="Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.WCF.FaultContractExceptionHandler, Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.WCF"
                   faultContractType="Afx.Business.ServiceModel.AuthenticationFault, Afx.Business"
                   name="AuthenticationFault"/>
            </exceptionHandlers>
          </add>
          <add name="AuthorizationException"
               type="Afx.Business.Security.AuthorizationException, Afx.Business"
               postHandlingAction="ThrowNewException">
            <exceptionHandlers>
              <add type="Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.WCF.FaultContractExceptionHandler, Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.WCF"
                   faultContractType="Afx.Business.Security.AuthorizationFault, Afx.Business"
                   name="AuthorizationFault"/>
            </exceptionHandlers>
          </add>
          <add name="OptimisticConcurrencyException"
               type="Afx.Business.Data.OptimisticConcurrencyException, Afx.Business"
               postHandlingAction="ThrowNewException">
            <exceptionHandlers>
              <add type="Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.WCF.FaultContractExceptionHandler, Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.WCF"
                   faultContractType="Afx.Business.Service.MessageFault, Afx.Business"
                   name="OptimisticConcurrencyFault"/>
            </exceptionHandlers>
          </add>
          <add name="ReferentialIntegrityViolationException"
               type="Afx.Business.Data.ReferentialIntegrityViolationException, Afx.Business"
               postHandlingAction="ThrowNewException">
            <exceptionHandlers>
              <add type="Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.WCF.FaultContractExceptionHandler, Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.WCF"
                   faultContractType="Afx.Business.Service.MessageFault, Afx.Business"
                   name="ReferentialIntegrityViolationFault"/>
            </exceptionHandlers>
          </add>
          <add name="UniqueIndexViolationException"
               type="Afx.Business.Data.UniqueIndexViolationException, Afx.Business"
               postHandlingAction="ThrowNewException">
            <exceptionHandlers>
              <add type="Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.WCF.FaultContractExceptionHandler, Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.WCF"
                   faultContractType="Afx.Business.Service.MessageFault, Afx.Business"
                   name="UniqueIndexViolationFault"/>
            </exceptionHandlers>
          </add>
          <add name="QueryException"
               type="Afx.Business.Data.QueryException, Afx.Business"
               postHandlingAction="ThrowNewException">
            <exceptionHandlers>
              <add type="Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.WCF.FaultContractExceptionHandler, Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.WCF"
                   faultContractType="Afx.Business.Service.MessageFault, Afx.Business"
                   name="QueryFault"/>
            </exceptionHandlers>
          </add>
          <add name="MessageException"
               type="Afx.Business.MessageException, Afx.Business"
               postHandlingAction="ThrowNewException">
            <exceptionHandlers>
              <add type="Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.WCF.FaultContractExceptionHandler, Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.WCF"
                   faultContractType="Afx.Business.Service.MessageFault, Afx.Business"
                   name="MessageException"/>
            </exceptionHandlers>
          </add>
          <add name="MessageException"
               type="Afx.Business.MessageException, Afx.Business"
               postHandlingAction="ThrowNewException">
            <exceptionHandlers>
              <add type="Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.WCF.FaultContractExceptionHandler, Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.WCF"
                   faultContractType="Afx.Business.Service.MessageFault, Afx.Business"
                   name="MessageException"/>
            </exceptionHandlers>
          </add>
          <add name="ImportException"
               type="Afx.Business.ImportException, Afx.Business"
               postHandlingAction="ThrowNewException">
            <exceptionHandlers>
              <add name="LoggingExceptionHandler"
                   type="Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.Logging.LoggingExceptionHandler, Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.Logging"
                   logCategory="Afx Service Host"
                   eventId="100"
                   severity="Error"
                   title="Import Exception"
                   formatterType="Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.TextExceptionFormatter, Microsoft.Practices.EnterpriseLibrary.ExceptionHandling"
                   priority="0"/>
              <add type="Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.WCF.FaultContractExceptionHandler, Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.WCF"
                   faultContractType="Afx.Business.Service.MessageFault, Afx.Business"
                   name="ImportException"/>
            </exceptionHandlers>
          </add>
        </exceptionTypes>
      </add>
    </exceptionPolicies>
  </exceptionHandling>

  <connectionStrings>
    <add name="Local"
         connectionString="data source=.;Initial Catalog=AfxCSBCrusher;Integrated Security=SSPI"
         providerName="System.Data.SqlClient"/>
  </connectionStrings>
  
    <system.serviceModel>
      <diagnostics>
        <messageLogging logEntireMessage="true"
                        logMalformedMessages="true"
                        logMessagesAtServiceLevel="true"
                        logMessagesAtTransportLevel="true"/>
      </diagnostics>
      <bindings>
        <basicHttpBinding>
          <binding maxReceivedMessageSize="204800"
                   receiveTimeout="00:10:00"
                   sendTimeout="00:10:00">
            <security mode="None"/>
          </binding>
        </basicHttpBinding>
        <netTcpBinding>
          <binding maxReceivedMessageSize="204800"
                   receiveTimeout="00:10:00"
                   sendTimeout="00:10:00">
            <security mode="None"/>
          </binding>
        </netTcpBinding>
        <netMsmqBinding>
          <binding maxReceivedMessageSize="204800"
                   exactlyOnce="true"
                   timeToLive="24.00:00:00"
                   receiveErrorHandling="Move"
                   retryCycleDelay="00:10:00"
                   receiveTimeout="00:10:00"
                   sendTimeout="00:10:00">
            <security mode="None"/>
          </binding>
        </netMsmqBinding>
      </bindings>
      <extensions>
        <behaviorExtensions>
          <add name="afxEndpointBehaviorExtension"
               type="Afx.Business.ServiceModel.AfxCompressedEndpointBehaviorElement, Afx.Business"/>
          <add name="afxEndpointBehaviorExtensionXml"
               type="Afx.Business.ServiceModel.AfxEndpointBehaviorElement, Afx.Business"/>
          <add name="afxServiceBehaviorExtension"
               type="Afx.Business.ServiceModel.AfxServiceBehaviorElement, Afx.Business"/>
        </behaviorExtensions>
      </extensions>
      <behaviors>
        <serviceBehaviors>
          <behavior>
            <!--<serviceThrottling maxConcurrentCalls="1000"/>-->
            <serviceMetadata httpGetEnabled="true"/>
            <serviceDebug includeExceptionDetailInFaults="false"/>
            <afxServiceBehaviorExtension/>
          </behavior>
        </serviceBehaviors>
        <endpointBehaviors>
          <behavior>
            <afxEndpointBehaviorExtension/>
          </behavior>
          <behavior name="afxEndpointBehaviorExtensionXml">
            <afxEndpointBehaviorExtensionXml/>
          </behavior>
        </endpointBehaviors>
      </behaviors>
      <services>
        <service name="Afx.Business.Service.AfxService">
          <host>
            <baseAddresses>
              <add baseAddress="AfxService.svc"/>
            </baseAddresses>
          </host>
          <endpoint address=""
                    binding="basicHttpBinding"
                    contract="Afx.Business.Service.IAfxService"/>
          <endpoint contract="IMetadataExchange"
                    binding="mexHttpBinding"
                    address="mex"/>
          <endpoint address=""
                    binding="netTcpBinding"
                    contract="Afx.Business.Service.IAfxService"/>
          <endpoint contract="IMetadataExchange"
                    binding="mexTcpBinding"
                    address="mex"/>
        </service>
        
        <service name="FleetManagement.Business.Service.FleetManagementService">
          <host>
            <baseAddresses>
              <add baseAddress="FleetManagementService.svc"/>
            </baseAddresses>
          </host>
          <endpoint address=""
                    binding="basicHttpBinding"
                    contract="FleetManagement.Business.Service.IFleetManagementService"/>
          <endpoint contract="IMetadataExchange"
                    binding="mexHttpBinding"
                    address="mex"/>
          <endpoint address=""
                    binding="netTcpBinding"
                    contract="FleetManagement.Business.Service.IFleetManagementService"/>
          <endpoint contract="IMetadataExchange"
                    binding="mexTcpBinding"
                    address="mex"/>
        </service>

        <service name="ContactManagement.Business.Service.ContactManagementService">
          <host>
            <baseAddresses>
              <add baseAddress="ContactManagementService.svc"/>
            </baseAddresses>
          </host>
          <endpoint address=""
                    binding="basicHttpBinding"
                    contract="ContactManagement.Business.Service.IContactManagementService"/>
          <endpoint contract="IMetadataExchange"
                    binding="mexHttpBinding"
                    address="mex"/>
          <endpoint address=""
                    binding="netTcpBinding"
                    contract="ContactManagement.Business.Service.IContactManagementService"/>
          <endpoint contract="IMetadataExchange"
                    binding="mexTcpBinding"
                    address="mex"/>
        </service>

        <service name="AccountManagement.Business.Service.AccountManagementService">
          <host>
            <baseAddresses>
              <add baseAddress="AccountManagementService.svc"/>
            </baseAddresses>
          </host>
          <endpoint address=""
                    binding="basicHttpBinding"
                    contract="AccountManagement.Business.Service.IAccountManagementService"/>
          <endpoint contract="IMetadataExchange"
                    binding="mexHttpBinding"
                    address="mex"/>
          <endpoint address=""
                    binding="netTcpBinding"
                    contract="AccountManagement.Business.Service.IAccountManagementService"/>
          <endpoint contract="IMetadataExchange"
                    binding="mexTcpBinding"
                    address="mex"/>
        </service>

        <service name="InventoryControl.Business.Service.InventoryControlService">
          <host>
            <baseAddresses>
              <add baseAddress="InventoryControlService.svc"/>
            </baseAddresses>
          </host>
          <endpoint address=""
                    binding="basicHttpBinding"
                    contract="InventoryControl.Business.Service.IInventoryControlService"/>
          <endpoint contract="IMetadataExchange"
                    binding="mexHttpBinding"
                    address="mex"/>
          <endpoint address=""
                    binding="netTcpBinding"
                    contract="InventoryControl.Business.Service.IInventoryControlService"/>
          <endpoint contract="IMetadataExchange"
                    binding="mexTcpBinding"
                    address="mex"/>
        </service>

        <service name="Weighbridge.Business.Service.WeighbridgeService">
          <host>
            <baseAddresses>
              <add baseAddress="WeighbridgeService.svc"/>
            </baseAddresses>
          </host>
          <endpoint address=""
                    binding="basicHttpBinding"
                    contract="Weighbridge.Business.Service.IWeighbridgeService"/>
          <endpoint contract="IMetadataExchange"
                    binding="mexHttpBinding"
                    address="mex"/>
          <endpoint address=""
                    binding="netTcpBinding"
                    contract="Weighbridge.Business.Service.IWeighbridgeService"/>
          <endpoint contract="IMetadataExchange"
                    binding="mexTcpBinding"
                    address="mex"/>
        </service>

        <service name="Development.Business.Service.DevelopmentService">
          <host>
            <baseAddresses>
              <add baseAddress="DevelopmentService.svc"/>
            </baseAddresses>
          </host>
          <endpoint address=""
                    binding="basicHttpBinding"
                    contract="Development.Business.Service.IDevelopmentService"/>
          <endpoint contract="IMetadataExchange"
                    binding="mexHttpBinding"
                    address="mex"/>
          <endpoint address=""
                    binding="netTcpBinding"
                    contract="Development.Business.Service.IDevelopmentService"/>
          <endpoint contract="IMetadataExchange"
                    binding="mexTcpBinding"
                    address="mex"/>
        </service>

        <service name="Afx.Business.Service.AfxReplicationService">
          <host>
            <baseAddresses>
              <add baseAddress="AfxReplicationService.svc"/>
            </baseAddresses>
          </host>
          <endpoint address=""
                    binding="netMsmqBinding"
                    contract="Afx.Business.Service.IAfxReplicationService"
                    behaviorConfiguration="afxEndpointBehaviorExtensionXml"/>
        </service>
      </services>

      <client>
        <!--<endpoint name="Depot::Afx.Business.Service.IAfxReplicationService"
                  contract="Afx.Business.Service.IAfxReplicationService"
                  address="net.msmq://citysand.dyndns.org/private/CSB.Master/AfxReplicationService.svc"
                  binding="netMsmqBinding"
                  behaviorConfiguration="afxEndpointBehaviorExtensionXml"/>
        
        <endpoint name="Crusher::Afx.Business.Service.IAfxReplicationService"
                  contract="Afx.Business.Service.IAfxReplicationService"
                  address="net.msmq://csb-crusher.ddns.net/private/CSB.Crusher/AfxReplicationService.svc"
                  binding="netMsmqBinding"
                  behaviorConfiguration="afxEndpointBehaviorExtensionXml"/>-->
        
        <endpoint name="Depot::Afx.Business.Service.IAfxReplicationService"
                  contract="Afx.Business.Service.IAfxReplicationService"
                  address="net.msmq://localhost/private/CSB.Master/AfxReplicationService.svc"
                  binding="netMsmqBinding"
                  behaviorConfiguration="afxEndpointBehaviorExtensionXml"/>

        <endpoint name="Crusher::Afx.Business.Service.IAfxReplicationService"
                  contract="Afx.Business.Service.IAfxReplicationService"
                  address="net.msmq://localhost/private/CSB.Crusher/AfxReplicationService.svc"
                  binding="netMsmqBinding"
                  behaviorConfiguration="afxEndpointBehaviorExtensionXml"/>

        <!--<endpoint name="Afx.Business.Service.IAfxLogService"
                  contract="Afx.Business.Service.IAfxLogService"
                  address="net.msmq://localhost/private/AfxLogService.svc"
                  binding="netMsmqBinding"/>-->
      </client>
    </system.serviceModel>
  
  <system.webServer>
    <directoryBrowse enabled="true"/>
    <httpProtocol allowKeepAlive="false"/>
  </system.webServer>

  <system.web>
    <httpRuntime targetFramework="4.5"/>
    <compilation debug="true"
                 targetFramework="4.5.1"/>
    <pages controlRenderingCompatibilityVersion="4.0"/>
  </system.web>

  <runtime>
    <generatePublisherEvidence enabled="false"/>
    <assemblyBinding xmlns="urn:schemas-microsoft-com:asm.v1">
      <dependentAssembly>
        <assemblyIdentity name="Microsoft.Practices.EnterpriseLibrary.Common"
                          publicKeyToken="31bf3856ad364e35"
                          culture="neutral"/>
        <bindingRedirect oldVersion="0.0.0.0-5.0.505.0"
                         newVersion="5.0.505.0"/>
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="Microsoft.Practices.EnterpriseLibrary.Logging"
                          publicKeyToken="31bf3856ad364e35"
                          culture="neutral"/>
        <bindingRedirect oldVersion="0.0.0.0-5.0.505.0"
                         newVersion="5.0.505.0"/>
      </dependentAssembly>
    </assemblyBinding>
  </runtime>
</configuration>
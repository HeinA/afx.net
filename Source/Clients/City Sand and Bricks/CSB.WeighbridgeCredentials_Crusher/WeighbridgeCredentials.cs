﻿using Afx.Business;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;

namespace CSB.WeighbridgeCredentials
{
  [Export(typeof(ICredentialProvider))]
  public class WeighbridgeCredentials : ICredentialProvider
  {
    public string Username
    {
      get { return "WeighbridgeOperator_Crusher"; }
    }

    public string Password
    {
      get { return "WB123"; }
    }

    public Guid OrganizationalUnit
    {
      get { return Guid.Parse("b2c64949-1a67-4bb7-b956-44f2f73686ec"); }
    }
  }
}

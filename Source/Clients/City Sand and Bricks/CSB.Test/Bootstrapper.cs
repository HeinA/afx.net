﻿using Afx.Business.Security;
using Afx.Business.Service;
using Afx.Prism;
using Afx.Prism.RibbonMdi;
using Microsoft.Practices.Prism.Modularity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Resources;

namespace CSB
{
  class Bootstrapper : MdiBootstrapper
  {
    public override Stream GetDefaultDockingLayout()
    {
      try
      {
        StreamResourceInfo ri = Application.GetResourceStream(new Uri("DefaultLayout.config", UriKind.Relative));
        return ri.Stream;
      }
      catch
      {
        throw;
      }
    }

    protected override MdiApplication CreateApplicationContext()
    {
      MdiApplication mdi = base.CreateApplicationContext();
      mdi.Title = "City Sand & Bricks";
      return mdi;
    }

#if DEBUG
    protected override Afx.Business.Security.AuthenticationResult DefaultLoginDetails()
    {
      LoginOrganizationalUnit loginOU = null;
      using (var svc = ProxyFactory.GetService<IAfxService>(DefaultServer.ServerName))
      {
        //Depot
        //loginOU = svc.LoadLoginOrganizationalUnits().Where(ou => ou.GlobalIdentifier.Equals(Guid.Parse("35263117-bc4a-476c-bdc8-3dd3ef41eaa5"))).FirstOrDefault();

        //Crusher
        loginOU = svc.LoadLoginOrganizationalUnits().Where(ou => ou.GlobalIdentifier.Equals(Guid.Parse("b2c64949-1a67-4bb7-b956-44f2f73686ec"))).FirstOrDefault();
      }
      return SecurityContext.Authenticate(loginOU, "HeinA", "dred1234");
    }
#endif
  }
}

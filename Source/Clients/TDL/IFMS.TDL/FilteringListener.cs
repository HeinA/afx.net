﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IFMS.TDL
{
  public class FilteringListener : TraceListener
  {
    public FilteringListener()
    {
      IgnoredAssemblies = new List<string>();
      mInner = new DefaultTraceListener();
    }

    private DefaultTraceListener mInner;

    public List<String> IgnoredAssemblies { get; private set; }

    public override void Write(string message)
    {
      if (ShouldFilter())
        mInner.Write(message);
    }

    private bool ShouldFilter()
    {
      var stack = new StackTrace();
      var stackFrame = stack.GetFrame(3);
      var method = stackFrame.GetMethod();
      var assembly = method.DeclaringType.Assembly;
      return !IgnoredAssemblies.Any(x => assembly.FullName.Contains(x));
    }

    public override void WriteLine(string message)
    {
      if (ShouldFilter())
        mInner.WriteLine(message);
    }
  }
}

﻿using Afx.Business;
using Afx.Business.Data;
using Afx.Business.Security;
using Afx.Business.Service;
using Afx.Business.ServiceModel;
using Afx.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.Web;

namespace WebHost.App_Code
{
  public class AppInit
  {
    public static void AppInitialize()
    {
      //try
      //{
        //using (var sw = new StreamWriter(@"c:\temp\iisservicestartup.txt", true))
        //{
        //  sw.WriteLine("Starting...");
        //}

        IConfigurationSource config = ConfigurationSourceFactory.Create();
        DatabaseFactory.SetDatabaseProviderFactory(new DatabaseProviderFactory(config));
        Logger.SetLogWriter(new LogWriterFactory(config).Create());
        ExceptionPolicyFactory factory = new ExceptionPolicyFactory(config);
        ExceptionManager exManager = factory.CreateManager();
        ExceptionPolicy.SetExceptionManager(exManager);

        AfxService.Initialize();


        //using (var sw = new StreamWriter(@"c:\temp\iisservicestartup.txt", true))
        //{
        //  sw.WriteLine("SUCESS");
        //}
      //}
      //catch (FaultException ex)
      //{
      //  using (var sw = new StreamWriter(@"c:\temp\iisservicestartup.txt", true))
      //  {
      //    sw.WriteLine(ex.ToString());
      //  }
      //}
    }
  }
}
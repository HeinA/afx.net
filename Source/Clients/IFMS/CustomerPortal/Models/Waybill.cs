﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CustomerPortal.Models
{
  public class Waybill
  {
    public string WaybillNumber { get; set; }
    public DateTime WaybillDate { get; set; }
    public string Origin { get; set; }
    public DateTime? CollectionDate { get; set; }
    public string Consigner { get; set; }
    public string Destination { get; set; }
    public DateTime? DeliveryDate { get; set; }
    public string Consignee { get; set; }

    List<CargoItem> mCargoItems = new List<CargoItem>();
    public List<CargoItem> CargoItems
    {
      get { return mCargoItems; }
    }

    List<WaybillTraceItem> mTraceItems = new List<WaybillTraceItem>();
    public List<WaybillTraceItem> TraceItems
    {
      get { return mTraceItems; }
    }

    List<ScannedDocument> mDocuments = new List<ScannedDocument>();
    public List<ScannedDocument> Documents
    {
      get { return mDocuments; }
    }
  }
}
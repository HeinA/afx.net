﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CustomerPortal.Models
{
  public class ScannedDocument
  {
    public Guid Id { get; set; }
    public string Description { get; set; }
  }
}
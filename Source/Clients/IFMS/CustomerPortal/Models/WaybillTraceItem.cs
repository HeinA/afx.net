﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CustomerPortal.Models
{
  public class WaybillTraceItem
  {
    public DateTime Timestamp { get; set; }
    public string Text { get; set; }
  }
}
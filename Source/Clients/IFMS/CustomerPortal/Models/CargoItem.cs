﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CustomerPortal.Models
{
  public class CargoItem
  {
    public int Quantity { get; set; }
    public string Description { get; set; }
    public decimal PhysicalWeight { get; set; }
    public decimal VolumetricWeight { get; set; }
    public decimal DeclaredValue { get; set; }
  }
}
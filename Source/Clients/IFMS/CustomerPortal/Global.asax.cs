﻿using CustomperPortal;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace CustomerPortal
{
  public class MvcApplication : System.Web.HttpApplication
  {
    protected void Application_Start()
    {
      AreaRegistration.RegisterAllAreas();
      RouteConfig.RegisterRoutes(RouteTable.Routes);
      BundleConfig.RegisterBundles(BundleTable.Bundles);
    }

    protected void Application_BeginRequest()
    {
      if (!Context.Request.IsSecureConnection && ConfigurationManager.AppSettings["Debug"] != "true")
        Response.Redirect(Context.Request.Url.ToString().Replace("http:", "https:"));
    }
  }
}

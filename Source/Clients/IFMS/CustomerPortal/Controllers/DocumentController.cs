﻿using Afx.Mvc.jqGrid;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace CustomerPortal.Controllers
{
  [System.Web.Mvc.Authorize]
  public class DocumentController : Controller
  {
    // GET: Document
    public ActionResult Index()
    {
      return View();
    }

    public JsonResult Waybills([FromUri] jqGridRequest request)
    {
      string sql = null;
      //      string sql = @"SELECT	D.id
      //,   '<a href=''/Waybill/View/' + cast(D.GlobalIdentifier as varchar(40)) + ''' target=''_blank''>'+D.DocumentNumber+'</a>' as DocumentNumber
      //,		Cast(D.DocumentDate as Date) as DocumentDate
      //,		DTS.Name as [State]
      //,		W.Consigner
      //,		W.Consignee
      //,		L.Name as DeliveryCity
      //,		W.PODDate
      //,		DL.Links
      //FROM Afx.Document D 
      //INNER JOIN FreightManagement.Waybill W ON D.id=W.id
      //inner join Afx.DocumentState DS on DS.id=D.[State]
      //inner join Afx.DocumentTypeState DTS on DTS.id=DS.DocumentTYpeState
      //inner join Geographics.Location L on L.id=W.DeliveryCity
      //left outer join (
      //	Select distinct DRE2.Document, 
      //			substring(replace(replace((
      //				Select ', <a href=''/Document/View/' + cast(SD.GlobalIdentifier as varchar(40)) + ''' target=''_blank''>'+isnull(isnull(DTST.Text, SD.Description), 'UNKNOWN')+'</a>' as [text()]
      //				From DocumentManagement.DocumentRepositoryExtension DRE
      //				inner join DocumentManagement.ScannedDocument SD on SD.DocumentRepositoryExtension=DRE.Id
      //				left outer join Afx.DocumentTypeScanType DTST on DTST.Id=SD.ScanType
      //				Where DRE.Document = DRE2.Document
      //				ORDER BY DRE.Document
      //				For XML PATH ('')
      //			), '&lt;', '<'), '&gt;', '>'), 2, 8000) [Links]
      //	From DocumentManagement.DocumentRepositoryExtension DRE2) DL on DL.Document=D.Id
      //  where W.Account in (select EUA.Account from FreightManagement.ExternalUser EU inner join FreightManagement.ExternalUserAccount EUA on EUA.ExternalUser=EU.id and EU.Username=@P1)";

      //if (User.IsInRole(Roles.Customer))
      //{
        sql = @"SELECT	D.id
,   '<a href=''/Waybill/View/' + cast(D.GlobalIdentifier as varchar(40)) + ''' target=''_blank''>'+D.DocumentNumber+'</a>' as DocumentNumber
,		Cast(D.DocumentDate as Date) as DocumentDate
,		DTS.Name as [State]
,		W.Consigner
,		W.Consignee
,		L.Name as DeliveryCity
,		W.PODDate
FROM Afx.Document D 
INNER JOIN FreightManagement.Waybill W ON D.id=W.id
inner join Afx.DocumentState DS on DS.id=D.[State]
inner join Afx.DocumentTypeState DTS on DTS.id=DS.DocumentTYpeState
inner join Geographics.Location L on L.id=W.DeliveryCity
where W.Account in (select EUA.Account from FreightManagement.ExternalUser EU inner join FreightManagement.ExternalUserAccount EUA on EUA.ExternalUser=EU.id and EU.Username=@P1)";
//      }

//      if (User.IsInRole(Roles.Agent))
//      {
//        if (!string.IsNullOrWhiteSpace(sql)) sql += "\nUNION ALL\n";

//        sql += @"SELECT	D.id
//,   '<a href=''/Waybill/View/' + cast(D.GlobalIdentifier as varchar(40)) + ''' target=''_blank''>'+D.DocumentNumber+'</a>' as DocumentNumber
//,		Cast(D.DocumentDate as Date) as DocumentDate
//,		DTS.Name as [State]
//,		W.Consigner
//,		W.Consignee
//,		L.Name as DeliveryCity
//,		W.PODDate
//FROM Afx.Document D 
//INNER JOIN FreightManagement.Waybill W ON D.id=W.id
//inner join Afx.DocumentState DS on DS.id=D.[State]
//inner join Afx.DocumentTypeState DTS on DTS.id=DS.DocumentTYpeState
//inner join Geographics.Location L on L.id=W.DeliveryCity
//where W.Account in (select EUA.Account from FreightManagement.ExternalUser EU inner join FreightManagement.ExternalUserAccount EUA on EUA.ExternalUser=EU.id and EU.Username=@P1)";
//      }

      if (string.IsNullOrWhiteSpace(sql)) return Json(null);

      string username = User.Identity.Name;

      using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["Local"].ConnectionString))
      {
        con.Open();

        var jr = Json(GridHelper.GetGridPage(sql, request, con, username), "text/html", JsonRequestBehavior.AllowGet);
        return jr;
      }
    }

    public FileContentResult View(Guid id)
    {
      string ext = null;
      using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["Local"].ConnectionString))
      {
        con.Open();
        using (var cmd = new SqlCommand("SELECT FileExtension FROM DocumentManagement.ScannedDocument WHERE GlobalIdentifier=@gi", con))
        {
          cmd.Parameters.AddWithValue("@gi", id);
          ext = (string)cmd.ExecuteScalar();
        }
      }

      if (string.IsNullOrEmpty(ext)) ext = ".pdf";
      if (ext[0] != '.') ext = string.Format(".{0}", ext);

      byte[] doc = null;
      using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["Documents"].ConnectionString))
      {
        con.Open();
        using (var cmd = new SqlCommand("SELECT Document FROM ScannedDocument WHERE id=@gi", con))
        {
          cmd.Parameters.AddWithValue("@gi", id);
          doc = (byte[])cmd.ExecuteScalar();
        }
      }

      Response.AppendHeader("Content-Disposition", string.Format("inline; filename=document{0}", ext));
      if (doc == null) return null;
      return File(doc, MimeTypes.MimeTypeMap.GetMimeType(ext));
    }
  }
}
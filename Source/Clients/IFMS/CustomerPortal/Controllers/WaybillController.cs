﻿using Afx.Mvc;
using CustomerPortal.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CustomerPortal.Controllers
{
  public class WaybillController : Controller
  {
    //// GET: Waybill
    //public ActionResult Index()
    //{
    //  return View();
    //}

    public ActionResult View(Guid id)
    {
      Waybill w = new Waybill();

      using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["Local"].ConnectionString))
      {
        con.Open();

        using (var cmdWaybill = new SqlCommand("SELECT * FROM Portal.WaybillDetails WHERE GlobalIdentifier=@gi", con))
        {
          cmdWaybill.Parameters.AddWithValue("@gi", id);
          DataSet ds = DataHelper.ExecuteDataSet(cmdWaybill);

          w.WaybillNumber = (string)ds.Tables[0].Rows[0]["DocumentNumber"];
          w.WaybillDate = (DateTime)ds.Tables[0].Rows[0]["DocumentDate"];
          w.CollectionDate = ds.Tables[0].Rows[0]["CollectionDate"] == DBNull.Value ? null : (DateTime?)ds.Tables[0].Rows[0]["CollectionDate"];
          w.DeliveryDate = ds.Tables[0].Rows[0]["DeliveryDate"] == DBNull.Value ? null : (DateTime?)ds.Tables[0].Rows[0]["DeliveryDate"];
          w.Origin = (string)ds.Tables[0].Rows[0]["Origin"];
          w.Destination = (string)ds.Tables[0].Rows[0]["Destination"];
          w.Consigner = (string)ds.Tables[0].Rows[0]["Consigner"];
          w.Consignee = (string)ds.Tables[0].Rows[0]["Consignee"];

          using (var cmdCI = new SqlCommand("SELECT * FROM FreightManagement.WaybillCargoItem WHERE Waybill=@id", con))
          {
            cmdCI.Parameters.AddWithValue("@id", ds.Tables[0].Rows[0]["id"]);
            DataSet ds1 = DataHelper.ExecuteDataSet(cmdCI);

            foreach (DataRow dr in ds1.Tables[0].Rows)
            {
              CargoItem ci = new CargoItem();
              ci.Quantity = (int)dr["Items"];
              ci.Description = dr["Description"] == DBNull.Value ? null : (string)dr["Description"];
              ci.PhysicalWeight = (decimal)dr["PhysicalWeight"];
              ci.VolumetricWeight = (decimal)dr["VolumetricWeight"];
              ci.DeclaredValue = (decimal)dr["DeclaredValue"];
              w.CargoItems.Add(ci);
            }
          }

          using (var cmdTrace = new SqlCommand("SELECT * FROM FreightManagement.WaybillTrace WHERE Waybill=@id", con))
          {
            cmdTrace.Parameters.AddWithValue("@id", ds.Tables[0].Rows[0]["id"]);
            DataSet ds1 = DataHelper.ExecuteDataSet(cmdTrace);

            foreach (DataRow dr in ds1.Tables[0].Rows)
            {
              WaybillTraceItem ti = new WaybillTraceItem();
              ti.Timestamp = (DateTime)dr["Timestamp"];
              ti.Text = (string)dr["Text"];
              w.TraceItems.Add(ti);
            }
          }

          string sql = @"Select SD.GlobalIdentifier, isnull(isnull(DTST.Text, SD.Description), 'UNKNOWN') as Description
From DocumentManagement.DocumentRepositoryExtension DRE
inner join DocumentManagement.ScannedDocument SD on SD.DocumentRepositoryExtension=DRE.Id
left outer join Afx.DocumentTypeScanType DTST on DTST.Id=SD.ScanType
where dre.Document=@id
ORDER BY DRE.Document";

          using (var cmdDocs = new SqlCommand(sql, con))
          {
            cmdDocs.Parameters.AddWithValue("@id", ds.Tables[0].Rows[0]["id"]);
            DataSet ds1 = DataHelper.ExecuteDataSet(cmdDocs);

            foreach (DataRow dr in ds1.Tables[0].Rows)
            {
              ScannedDocument sd = new ScannedDocument();
              sd.Id = (Guid)dr["GlobalIdentifier"];
              sd.Description = (string)dr["Description"];
              w.Documents.Add(sd);
            }
          }
        }
      }

      return View(w);
    }
  }
}
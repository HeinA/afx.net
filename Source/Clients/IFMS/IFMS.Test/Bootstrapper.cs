﻿using Afx.Business.Security;
using Afx.Business.Service;
using Afx.Prism.RibbonMdi;
using System;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Resources;

namespace IFMS.Test
{
  class Bootstrapper : MdiBootstrapper
  {
    public override Stream GetDefaultDockingLayout()
    {
      try
      {
        StreamResourceInfo ri = Application.GetResourceStream(new Uri("DefaultLayout.config", UriKind.Relative));
        return ri.Stream;
      }
      catch
      {
        throw;
      }
    }

    protected override MdiApplication CreateApplicationContext()
    {
      MdiApplication mdi = base.CreateApplicationContext();
      mdi.Title = "IFMS";
      return mdi;
    }
    
#if DEBUG
    protected override Afx.Business.Security.AuthenticationResult DefaultLoginDetails()
    {
      LoginOrganizationalUnit loginOU = null;
      using (var svc = ProxyFactory.GetService<IAfxService>(DefaultServer.ServerName))
      {
        //Cape Town
        //loginOU = svc.LoadLoginOrganizationalUnits().Where(ou => ou.GlobalIdentifier.Equals(Guid.Parse("f161697c-44d5-428e-96cb-129a151fdebc"))).FirstOrDefault();

        //Johannesburg
        //loginOU = svc.LoadLoginOrganizationalUnits().Where(ou => ou.GlobalIdentifier.Equals(Guid.Parse("6c93875b-a34c-4322-9956-4c86a088b247"))).FirstOrDefault();

        //Head Office
        loginOU = svc.LoadLoginOrganizationalUnits().Where(ou => ou.GlobalIdentifier.Equals(Guid.Parse("35263117-bc4a-476c-bdc8-3dd3ef41eaa5"))).FirstOrDefault();
      }


      return null;// SecurityContext.Authenticate(loginOU, "HeinA", "dred1234");
    }
#endif
  }
}

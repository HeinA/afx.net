﻿using Afx.Business;
using Afx.Business.Data;
using Afx.Business.Service;
using Afx.Business.ServiceModel;
using Afx.Data;
using Afx.Data.Sql;
using Afx.Prism;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows;

namespace IFMS.Test
{
  /// <summary>
  /// Interaction logic for App.xaml
  /// </summary>
  public partial class App : Application
  {
    public App()
    {
    }

    protected override void OnStartup(StartupEventArgs e)
    {
      Debug.Listeners.Clear();
      var listener = new FilteringListener();
      listener.IgnoredAssemblies.Add("Xceed.Wpf.AvalonDock");
      listener.IgnoredAssemblies.Add("Fluent");
      Debug.Listeners.Add(listener);

      Application.Current.DispatcherUnhandledException += Current_DispatcherUnhandledException;
      AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;

      try
      {
        FileInfo fi = new FileInfo(@"c:\logs\AfxClient.svclog");
        if (fi.Exists) fi.Delete();
      }
      catch { }

      //ServiceFactory.SpinUp<IAfxService>();

      IConfigurationSource config = ConfigurationSourceFactory.Create();
      Logger.SetLogWriter(new LogWriterFactory(config).Create());
      ExceptionPolicyFactory factory = new ExceptionPolicyFactory(config);
      ExceptionManager exManager = factory.CreateManager();
      ExceptionPolicy.SetExceptionManager(exManager);

      base.OnStartup(e);
      Bootstrapper bootstrapper = new Bootstrapper();
      bootstrapper.Run(true);
    }

    void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
    {
      string folder = Environment.GetFolderPath(System.Environment.SpecialFolder.ApplicationData);
      string file = string.Format(@"{0}\Afx.net\{1}.exceptions.txt", folder, Process.GetCurrentProcess().ProcessName);

      try
      {
        using (StreamWriter sw = new StreamWriter(file, false))
        {
          TypeLoadException tle = e.ExceptionObject as TypeLoadException;
          if (tle != null)
          {
            sw.WriteLine(tle.TypeName);
            sw.WriteLine("*****************************************************");

          }
          ReflectionTypeLoadException rtle = e.ExceptionObject as ReflectionTypeLoadException;
          if (rtle != null)
          {
            foreach (Exception exSub in rtle.LoaderExceptions)
            {
              sw.WriteLine(exSub.Message);
              FileNotFoundException exFileNotFound = exSub as FileNotFoundException;
              if (exFileNotFound != null)
              {
                if (!string.IsNullOrEmpty(exFileNotFound.FusionLog))
                {
                  sw.WriteLine("Fusion Log:");
                  sw.WriteLine(exFileNotFound.FusionLog);
                }
              }
            }
            sw.WriteLine("*****************************************************");
          }
          sw.WriteLine(e.ExceptionObject.ToString());
        }
        ExceptionHelper.HandleException((Exception)e.ExceptionObject);
        //e.Handled = true;
      }
      catch
      {
        using (StreamWriter sw = new StreamWriter(file, false))
        {
          sw.WriteLine("*****************************************************");
          sw.WriteLine("*****************************************************");
          TypeLoadException tle = e.ExceptionObject as TypeLoadException;
          if (tle != null)
          {
            sw.WriteLine(tle.TypeName);
            sw.WriteLine("*****************************************************");

          }
          ReflectionTypeLoadException rtle = e.ExceptionObject as ReflectionTypeLoadException;
          if (rtle != null)
          {
            foreach (Exception exSub in rtle.LoaderExceptions)
            {
              sw.WriteLine(exSub.Message);
              FileNotFoundException exFileNotFound = exSub as FileNotFoundException;
              if (exFileNotFound != null)
              {
                if (!string.IsNullOrEmpty(exFileNotFound.FusionLog))
                {
                  sw.WriteLine("Fusion Log:");
                  sw.WriteLine(exFileNotFound.FusionLog);
                }
              }
            }
            sw.WriteLine("*****************************************************");
          }
          sw.WriteLine(e.ExceptionObject.ToString());
        }
#if DEBUG
        if (Debugger.IsAttached) Debugger.Break();
#endif
        throw;
      }
    }

    void Current_DispatcherUnhandledException(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
    {
      string folder = Environment.GetFolderPath(System.Environment.SpecialFolder.ApplicationData);
      string file = string.Format(@"{0}\Afx.net\{1}.exceptions.txt", folder, Process.GetCurrentProcess().ProcessName);

      try
      {
        using (StreamWriter sw = new StreamWriter(file, false))
        {
          TypeLoadException tle = e.Exception as TypeLoadException;
          if (tle != null)
          {
            sw.WriteLine(tle.TypeName);
            sw.WriteLine("*****************************************************");

          }
          ReflectionTypeLoadException rtle = e.Exception as ReflectionTypeLoadException;
          if (rtle != null)
          {
            foreach (Exception exSub in rtle.LoaderExceptions)
            {
              sw.WriteLine(exSub.Message);
              FileNotFoundException exFileNotFound = exSub as FileNotFoundException;
              if (exFileNotFound != null)
              {
                if (!string.IsNullOrEmpty(exFileNotFound.FusionLog))
                {
                  sw.WriteLine("Fusion Log:");
                  sw.WriteLine(exFileNotFound.FusionLog);
                }
              }
            }
            sw.WriteLine("*****************************************************");
          }
          sw.WriteLine(e.Exception.ToString());
        }
        ExceptionHelper.HandleException(e.Exception);
        //e.Handled = true;
      }
      catch
      {
        using (StreamWriter sw = new StreamWriter(file, false))
        {
          sw.WriteLine("*****************************************************");
          sw.WriteLine("*****************************************************");
          TypeLoadException tle = e.Exception as TypeLoadException;
          if (tle != null)
          {
            sw.WriteLine(tle.TypeName);
            sw.WriteLine("*****************************************************");

          }
          ReflectionTypeLoadException rtle = e.Exception as ReflectionTypeLoadException;
          if (rtle != null)
          {
            foreach (Exception exSub in rtle.LoaderExceptions)
            {
              sw.WriteLine(exSub.Message);
              FileNotFoundException exFileNotFound = exSub as FileNotFoundException;
              if (exFileNotFound != null)
              {
                if (!string.IsNullOrEmpty(exFileNotFound.FusionLog))
                {
                  sw.WriteLine("Fusion Log:");
                  sw.WriteLine(exFileNotFound.FusionLog);
                }
              }
            }
            sw.WriteLine("*****************************************************");
          }
          sw.WriteLine(e.Exception.ToString());
        }
#if DEBUG
        if (Debugger.IsAttached) Debugger.Break();
#endif
        throw;
      }
    }
  }
}

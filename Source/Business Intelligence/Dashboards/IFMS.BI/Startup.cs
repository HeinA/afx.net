﻿#region Usings

using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security.Cookies;
using Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

#endregion

namespace IFMS.BI
{
  public class AppUser : IdentityUser, Afx.Business.Security.User
  {
  }

  public class AppDbContext : IdentityDbContext<AppUser>
  {
    public AppDbContext() : base("DefaultConnection")
    {
    }
  }
  public class Startup
  {
    public static Func<UserManager<AppUser>> UserManagerFactory { get; private set; }

    public void Configuration(IAppBuilder app)
    {
      app.MapSignalR();
      //app.UseCookieAuthentication(new CookieAuthenticationOptions());


      // configure the user manager
      UserManagerFactory = () =>
      {
        var usermanager = new UserManager<AppUser>(new UserStore<AppUser>(new AppDbContext()));
        // allow alphanumeric characters in username
        usermanager.UserValidator = new UserValidator<AppUser>(usermanager)
        {
          AllowOnlyAlphanumericUserNames = false
        };

        return usermanager;
      };
    }

    public void ConfigureAuth(IAppBuilder app)
    {
      //app.CreatePerOwinContext();
    }
  }
}
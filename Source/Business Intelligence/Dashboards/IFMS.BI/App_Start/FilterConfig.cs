﻿#region Usings

using System.Web;
using System.Web.Mvc;

#endregion

namespace IFMS.BI
{
  public static class FilterConfig
  {
    public static void RegisterGlobalFilters(GlobalFilterCollection filters)
    {
      filters.Add(new HandleErrorAttribute());
    }
  }
}

﻿#region Usings

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

#endregion

namespace IFMS.BI
{
  public static class RouteConfig
  {
    public static void RegisterRoutes(RouteCollection routes)
    {
      routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
      routes.LowercaseUrls = true;
      routes.MapRoute("Default", "{controller}/{action}/{id}", new
      {
        controller = "Dashboard",
        action = "Index",
        id = UrlParameter.Optional
      }).RouteHandler = new DashRouteHandler();
    }
  }
}

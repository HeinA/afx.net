﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;

namespace IFMS.BI.Hubs
{
  public class LiveDataHub : Hub
  {
    public void Hello()
    {
      Clients.All.hello();
    }

    public void SendWeeklyCPK()
    {
      Clients.OthersInGroup("Management").updateWeeklyCPK();
    }
  }
}
﻿#region Usings

using IFMS.BI.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

#endregion

namespace IFMS.BI.Controllers
{
  public class AuthController : Controller
  {
    private readonly UserManager<AppUser> userManager;

    public AuthController() : this(Startup.UserManagerFactory.Invoke())
    {
    }

    public AuthController(UserManager<AppUser> userManager)
    {
      this.userManager = userManager;
    }
    // GET: Auth
    public ActionResult Index()
    {
      return View();
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && userManager != null)
      {
        userManager.Dispose();
      }
      base.Dispose(disposing);
    }

    [HttpPost]
    public async Task<ActionResult> LogIn(AccountLoginModel model)
    {
      if (!ModelState.IsValid)
      {
        return View();
      }

      var user = await userManager.FindAsync(model.Email, model.Password);

      if (user != null)
      {
        var identity = await userManager.CreateIdentityAsync(
            user, DefaultAuthenticationTypes.ApplicationCookie);

        //GetAuthenticationManager().SignIn(identity);

        //return Redirect(GetRedirectUrl(model.ReturnUrl));
      }

      // user authN failed
      ModelState.AddModelError("", "Invalid email or password");
      return View();
    }

    private async Task SignIn(AppUser user)
    {
      var identity = await userManager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);
      //da
      //GetAuthenticationManager().SignIn(identity);
    }


    [HttpGet]
    public ActionResult Register()
    {
      return View();
    }

    [HttpPost]
    public async Task<ActionResult> Register(RegisterModel model)
    {
      if (!ModelState.IsValid)
      {
        return View();
      }

      var user = new AppUser
      {
        UserName = model.Email,
        //Country = model.Country
      };

      var result = await userManager.CreateAsync(user, model.Password);

      if (result.Succeeded)
      {
        await SignIn(user);
        return RedirectToAction("index", "home");
      }

      foreach (var error in result.Errors)
      {
        ModelState.AddModelError("", error);
      }

      return View();
    }
  }
}
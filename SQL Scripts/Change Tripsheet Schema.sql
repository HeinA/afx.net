ALTER SCHEMA FreightManagement TRANSFER ScheduleManagement.TripSheet
ALTER SCHEMA FreightManagement TRANSFER ScheduleManagement.TripsheetFee
ALTER SCHEMA FreightManagement TRANSFER ScheduleManagement.TripsheetManifest
ALTER SCHEMA FreightManagement TRANSFER ScheduleManagement.TripsheetVehicle

update Afx.DocumentType set [Namespace]='http://indilinga.com/FreightManagement/Business' where Name='Tripsheet'
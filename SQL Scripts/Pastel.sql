/*

select	W.id
,		DW.DocumentNumber as WaybillNumber
,		DA.DocumentNumber as AccountNumber
,		A.Name as AccountName
,		AE.ExternalSystem
,		AE.TaxType
from PastelPartnerIntegration.AccountExtension AE
inner join Accounts.Account A on A.id=AE.Account
inner join FreightManagement.Waybill W on W.Account=A.id
inner join Afx.Document DW on DW.id=W.id
inner join Afx.Document DA on DA.id=A.id
inner join Afx.DocumentState DS on DW.[State]=DS.id
inner join Afx.DocumentTypeState DTS on DTS.id=DS.DocumentTypeState
where DTS.Name = 'Pending Invoice'
and AE.ConsolidateInvoice = 0

select	WC.id
,		WC.GlobalIdentifier
,		W.id as Waybill
,		WC.Charge
,		WCT.[Text] as ChargeText
,		ERS.AllowAccountTaxTypeOverride
,		ER.Reference as PastelReference
from PastelPartnerIntegration.AccountExtension AE
inner join Accounts.Account A on A.id=AE.Account
inner join FreightManagement.Waybill W on W.Account=A.id
inner join Afx.Document DW on DW.id=W.id
inner join Afx.DocumentState DS on DW.[State]=DS.id
inner join Afx.DocumentTypeState DTS on DTS.id=DS.DocumentTypeState
inner join FreightManagement.WaybillCharge WC on W.id=WC.Waybill
inner join FreightManagement.WaybillChargeType WCT on WCT.id=WC.ChargeType
left outer join PastelPartnerIntegration.WaybillChargeTypeExternalReferences ERS on ERS.WaybillChargeType=WCT.Id
left outer join PastelPartnerIntegration.WaybillChargeTypeExternalReference ER on ER.WaybillChargeTypeExternalReferences=ERS.Id and ER.ExternalSystem=AE.ExternalSystem
where DTS.Name = 'Pending Invoice'
and AE.ConsolidateInvoice = 0

*/

alter view PastelPartnerIntegration.vPendingInvoice
as
select	W.id
,		DW.GlobalIdentifier
,		DW.DocumentNumber as WaybillNumber
,		A.id as Account
,		AE.ExternalSystem
,		AE.TaxType
,		isnull(AE.ExportAsOrder, 0) as ExportAsOrder
from  FreightManagement.Waybill W
inner join Afx.Document DW on DW.id=W.id
inner join Afx.DocumentState DS on DW.[State]=DS.id
inner join Afx.DocumentTypeState DTS on DTS.id=DS.DocumentTypeState
left outer join  Accounts.Account A on W.Account=A.id
left outer join  PastelPartnerIntegration.AccountExtension AE on A.id=AE.Account
where DTS.Name = 'Pending Invoice'

alter view PastelPartnerIntegration.vPendingInvoiceItem
as
select	WC.id
,		WC.GlobalIdentifier
,		W.id as PendingInvoice
,		WC.Charge
,		WC.ChargeType
--,		WCT.[Text] as ChargeText
--,		ERS.AllowAccountTaxTypeOverride
--,		ER.Reference as PastelReference
from FreightManagement.Waybill W 
inner join Afx.Document DW on DW.id=W.id
inner join Afx.DocumentState DS on DW.[State]=DS.id
inner join Afx.DocumentTypeState DTS on DTS.id=DS.DocumentTypeState
inner join FreightManagement.WaybillCharge WC on W.id=WC.Waybill
--inner join FreightManagement.WaybillChargeType WCT on WCT.id=WC.ChargeType
--left outer join Accounts.Account A on A.id=W.Account
--left outer join PastelPartnerIntegration.AccountExtension AE on W.Account=A.id
--left outer join PastelPartnerIntegration.WaybillChargeTypeExternalReferences ERS on ERS.WaybillChargeType=WCT.Id
--left outer join PastelPartnerIntegration.WaybillChargeTypeExternalReference ER on ER.WaybillChargeTypeExternalReferences=ERS.Id and ER.ExternalSystem=AE.ExternalSystem
where DTS.Name = 'Pending Invoice'

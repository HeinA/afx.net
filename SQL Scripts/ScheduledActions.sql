select	D.Id
,		D.GlobalIdentifier
,		D.DocumentType
,		D.DocumentNumber
,		'Load' as [Action]
,		FreightManagement.GetFullDateTime(TR.ScheduledLoadingDate, TR.ScheduledLoadingSlot) as [ActionDate]
from FreightManagement.TripsheetRoute TR
inner join Afx.Document D on D.Id=TR.Tripsheet
where	TR.ActualLoadingDate is null
and		TR.ScheduledLoadingDate is not null

union all

select	D.Id
,		D.GlobalIdentifier
,		D.DocumentType
,		D.DocumentNumber
,		'Depart' as [Action]
,		FreightManagement.GetFullDateTime(TR.ActualLoadingDate, TR.ActualLoadingSlot) as [ActionDate]
from FreightManagement.TripsheetRoute TR
inner join Afx.Document D on D.Id=TR.Tripsheet
where	TR.ScheduledLoadingDate is not null
and		TR.DepartureTime is null

union all

select	D.Id
,		D.GlobalIdentifier
,		D.DocumentType
,		D.DocumentNumber
,		'Offload' 
,		FreightManagement.GetFullDateTime(TR.ScheduledOffloadingDate, TR.ScheduledOffloadingSlot)
from FreightManagement.TripsheetRoute TR
inner join Afx.Document D on D.Id=TR.Tripsheet
where	TR.ActualOffloadingDate is null
and		TR.ScheduledOffloadingDate is not null

union all

select	D.Id
,		D.GlobalIdentifier
,		D.DocumentType
,		D.DocumentNumber
,		'Load' as Activity
,		FreightManagement.GetFullDateTime(LA.LoadingDate, LA.LoadingSlot) as [Date]
from ScheduleManagement.LoadAllocation LA
inner join Afx.Document D on D.Id=LA.Id
inner join Afx.DocumentState DS on D.[State]=DS.id
inner join Afx.DocumentTypeState DTS on DTS.Id=DS.DocumentTypeState
where DTS.GlobalIdentifier = '{7b72f718-2d0b-4247-8251-e483cad1c931}'

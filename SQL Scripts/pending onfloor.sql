select	WOF.DistributionCenter
,		DC.Name as DistributionCenterName
,		D.Id
,		D.DocumentDate
,		D.DocumentNumber
,		DTS.Name as DocumentTypeState
from Afx.Document D
inner join (
	select	distinct WR.DistributionCenter 
	,		WR.Waybill
	from FreightManagement.WaybillRoute WR
	where WR.DepartureTimestamp is null) as WOF on WOF.Waybill=D.id
inner join FreightManagement.DistributionCenter DC on WOF.DistributionCenter=DC.Id
inner join Afx.DocumentState DS on D.[State]=DS.id
inner join Afx.DocumentTypeState DTS on DTS.id=DS.DocumentTypeState
where DTS.GlobalIdentifier in ('' /*  */, '' /*  */, '' /*  */)

select * from Afx.DocumentTypeState where DocumentType=16

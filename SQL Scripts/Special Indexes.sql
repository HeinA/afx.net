CREATE UNIQUE NONCLUSTERED INDEX IDX_Document_DocumentNumber
ON [Afx].[Document](DocumentNumber)
WHERE DocumentNumber IS NOT NULL;
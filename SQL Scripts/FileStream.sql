--/Enable on properties of server in config manager

sp_configure 'filestream access level', 2
reconfigure with override

alter database AfxIFMSMasterDocuments
add filegroup ScannedDocumentsFilegroup contains filestream;
go

alter database AfxIFMSMasterDocuments
add file
  ( NAME = 'ScannedDocuments', FILENAME = 'C:\Data\ScannedDocuments'
   )
to filegroup ScannedDocumentsFilegroup;
go

CREATE TABLE dbo.ScannedDocument
(
	[Id] [uniqueidentifier] ROWGUIDCOL NOT NULL UNIQUE, 
	[Document] VARBINARY(MAX) FILESTREAM NULL
	[Replicated] [bit] NOT NULL CONSTRAINT [DF_ScannedDocument_Replicated]  DEFAULT ((0)),
)
GO